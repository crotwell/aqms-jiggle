package org.usgs.util;

/**
 * Support for Earthworm types. The types are used to create Earthworm "logos" that are
 * identifiers used in the headers of Earthworm messages.
 * In the Earthworm environment these are contained in
 * the files <it>earthworm_global.d</it> and <it>earthworm.d</it>. <p>
 *
 * The global codes are under the control of Earthworm Central and are under the
 * controll of  Barbara Bogaert (bogaert@usgs.gov).
 * @version 1.0
 */

public class EarthwormTypes {
/**
 <pre>
  #--------------------------------------------------------------------
  #                        Installation IDs
  #
  #   The character string <-> numerical value mapping for all
  #   installations are assigned and controlled by the Earthworm
  #   Development Team at the USGS in Golden, CO.  These mappings
  #   must remain globally unique in order for exchanged data
  #   to be properly attributed to the installation that created it.
  #
  #   0-255 are the only valid installation ids.
  #--------------------------------------------------------------------
  </pre>
    */
  public static final int   INST_WILDCARD  =  0 ;  // Sacred wildcard value - DO NOT CHANGE!!!
  public static final int   INST_FAIRBANKS =  1 ;
  public static final int   INST_UW        =  2 ;
  public static final int   INST_MENLO     =  3 ;
  public static final int   INST_CIT       =  4 ;
  public static final int   INST_UTAH      =  5 ;
  public static final int   INST_MEMPHIS   =  6 ;
  public static final int   INST_UNR       =  7 ;
  public static final int   INST_UCB       =  8 ;
  public static final int   INST_PTWC      =  9 ;
  public static final int   INST_IDA       = 10 ;
  public static final int   INST_NC        = 11 ;
  public static final int   INST_VT        = 12 ;
  public static final int   INST_USNSN     = 13 ;
  public static final int   INST_RICKS     = 14 ;
  public static final int   INST_HVO       = 15 ;
  public static final int   INST_ATWC      = 16 ;
  public static final int   INST_PGE       = 17 ;
  public static final int   INST_PNNL      = 18 ;
  public static final int   INST_PGC       = 19 ;
  public static final int   INST_AVO       = 20 ;
  public static final int   INST_BUTTE     = 21 ;
  public static final int   INST_LAMONT    = 22 ;
  public static final int   INST_SC_CHA    = 23 ;
  public static final int   INST_STLOUIS   = 24 ;
  public static final int   INST_NMT       = 25 ;
  public static final int   INST_CVO       = 26 ;
  public static final int   INST_SC_COL    = 27 ;
  public static final int   INST_PRSN      = 28 ;
  public static final int   INST_TEMP1     = 29;
  public static final int   INST_SOAPP     = 30;
  public static final int   INST_UO        = 31;
  public static final int   INST_USBR      = 32;
  public static final int   INST_UTIG      = 33;
  public static final int   INST_PSU       = 34;
  public static final int   INST_INEL      = 35;
  public static final int   INST_SISMALP   = 36;
  public static final int   INST_MIT       = 37;
  public static final int   INST_GSC       = 38;
  public static final int   INST_QUITO     = 100;
  public static final int   INST_INETER    = 101;
  public static final int   INST_CENAPRED  = 102;
  public static final int   INST_RABAUL    = 103;
/**
   <pre>
  #--------------------------------------------------------------------------
  #                          Message Types
  #
  #  Define all message name/message-type pairs that will be global
  #  to all Earthworm systems.
  #
  #  VALID numbers are:
  #
  #   0- 99 Message types 0-99 are defined in the file earthworm_global.d.
  #         These numbers are reserved by Earthworm Central to label types
  #         of messages which may be exchanged between installations. These
  #         string/value mappings must be global to all Earthworm systems
  #         in order for exchanged messages to be properly interpreted.
  #
  #
  #  OFF-LIMITS numbers:
  #
  # 100-255 Message types 100-255 are defined in each installation's
  #         earthworm.d file, under the control of each Earthworm
  #         installation. These values should be used to label messages
  #         which remain internal to an Earthworm system or installation.
  #
 #--------------------------------------------------------------------------
   </pre>
*/
   public static final int  TYPE_WILDCARD     =   0 ; // wildcard value - DO NOT CHANGE!!!
   public static final int  TYPE_ADBUF        =   1 ; // multiplexed waveforms from DOS adsend
   public static final int  TYPE_ERROR        =   2 ; // error public static final int
   public static final int  TYPE_HEARTBEAT    =   3 ; // heartbeat public static final int
   public static final int  TYPE_NANOBUF      =   5 ; // single-channel waveforms from nanometrics
   public static final int  TYPE_PICK2K       =  10 ; // P-wave arrival time (with 4 digit year)  from pick_ew
   public static final int  TYPE_CODA2K       =  11 ; // coda info (plus station code) from pick_ew
   public static final int  TYPE_HYP2000ARC   =  14 ; // hyp2000 (Y2K hypoinverse) event archive     msg from eqproc/eqprelim
   public static final int  TYPE_H71SUM2K     =  15 ; // hypo71-format hypocenter summary msg (with 4-digit year) from eqproc/eqprelim
   public static final int  TYPE_TRACEBUF     =  20 ; // single-channel waveforms from NT adsend, getdst2, nano2trace, rcv_ew, import_ida...
   public static final int  TYPE_LPTRIG       =  21 ; // single-channel long-period trigger from lptrig & evanstrig
   public static final int  TYPE_CUBIC        =  22 ; // cubic-format summary msg from cubic_msg
   public static final int  TYPE_CARLSTATRIG  =  23 ; // single-channel trigger from carlstatrig
   public static final int  TYPE_TRIGLIST2K   =  25 ; // trigger-list msg (with 4-digit year) used by tracesave modules from arc2trig,  trg_assoc, carlsubtrig
   public static final int  TYPE_TRACE_COMP_UA=  26 ; // compressed waveforms from compress_UA
   public static final int  TYPE_STRONGMOTION =  27 ; // single-instrument peak accel, peak velocity, peak displacement, spectral acceleration

   /**
<pre>
   #--------------------------------------------------------------------------
   #                           Module IDs
   #
   #  Define all module name/module id pairs for this installation
   #  Except for MOD_WILDCARD, all string/value mappings for module ids
   #  may be locally altered. The character strings themselves may also
   #  be changed to be more meaningful for your installation.
   #
   #  0-255 are the only valid module ids.
   #--------------------------------------------------------------------------
  </pre>
  * */
    public static final int   MOD_WILDCARD       = 0 ;  // Sacred wildcard value - DO NOT CHANGE!!!
    public static final int   MOD_ADSEND_A       = 1;
    public static final int   MOD_ADSEND_B       = 2;
    public static final int   MOD_ADSEND_C       = 3;
    public static final int   MOD_PICKER_A       = 4;
    public static final int   MOD_PICKER_B       = 5;
    public static final int   MOD_PICK_EW        = 6;
    public static final int   MOD_COAXTORING_A   = 7;
    public static final int   MOD_COAXTORING_B   = 8;
    public static final int   MOD_RINGTOCOAX     = 9;
    public static final int   MOD_BINDER_EW     = 10;
    public static final int   MOD_DISKMGR       = 11;
    public static final int   MOD_STATMGR       = 14;
    public static final int   MOD_REPORT        = 15;
    public static final int   MOD_STARTSTOP     = 18;
    public static final int   MOD_STATUS        = 19;
    public static final int   MOD_NANOBOX       = 20;
    public static final int   MOD_WAVESERVER    = 21;
    public static final int   MOD_PAGERFEEDER   = 23;
    public static final int   MOD_EQPROC        = 24;
    public static final int   MOD_TANKPLAYER    = 25;
    public static final int   MOD_EQALARM_EW    = 26;
    public static final int   MOD_EQPRELIM      = 27;
    public static final int   MOD_IMPORT_GENERIC= 28;
    public static final int   MOD_EXPORT_GENERIC= 29;
    public static final int   MOD_GETDST        = 31;
    public static final int   MOD_LPTRIG_A      = 32;
    public static final int   MOD_LPTRIG_B      = 33;
    public static final int   MOD_TRG_ASSOC     = 34;
    public static final int   MOD_AD_DEMUX_A    = 35;
    public static final int   MOD_AD_DEMUX_B    = 36;
    public static final int   MOD_VDL_EW        = 37;
    public static final int   MOD_CUBIC_MSG     = 38;
    public static final int   MOD_GAPLIST       = 39;
    public static final int   MOD_GETTERW       = 40;
    public static final int   MOD_WAVESERVERV   = 41;
    public static final int   MOD_NANO2TRACE    = 42;
    public static final int   MOD_GETDST2       = 43;
    public static final int   MOD_EXPORT_SCN    = 44;
    public static final int   MOD_ARC2TRIG      = 45;
    public static final int   MOD_RCV_EW        = 48;
    public static final int   MOD_HELI1         = 49;
    public static final int   MOD_SENDARC       = 50;
    public static final int   MOD_EXPORT_PTWC   = 51 ;
    public static final int   MOD_EXPORT_PTWC_HYPO   =52;
    public static final int   MOD_CARLSTATRIG_ANALOG =220;
    public static final int   MOD_CARLSTATRIG_AE     =221;
    public static final int   MOD_CARLSTATRIG_FJ     =222;
    public static final int   MOD_CARLSTATRIG_KO     =223;
    public static final int   MOD_CARLSTATRIG_PS     =224;
    public static final int   MOD_CARLSTATRIG_TZ     =225;
    public static final int   MOD_CARLSUBTRIG        =226;
    public static final int   MOD_A2M           = 243;
    public static final int   MOD_CS2EW_AE      = 227;
    public static final int   MOD_CS2EW_FJ      = 228;
    public static final int   MOD_CS2EW_KO      = 229;
    public static final int   MOD_CS2EW_PS      = 230;
    public static final int   MOD_CS2EW_TZ      = 231;
    public static final int   MOD_CS2EW_ANALOG  = 232;
    public static final int   MOD_PICK_EW_AE    = 233;
    public static final int   MOD_PICK_EW_FJ    = 234;
    public static final int   MOD_PICK_EW_KO    = 235;
    public static final int   MOD_PICK_EW_PS    = 236;
    public static final int   MOD_PICK_EW_TZ    = 237;
    public static final int   MOD_PICK_EW_ANALOG = 238;
    public static final int   MOD_HYP_PUB        = 240;
    public static final int   MOD_DATASOCK_IMPORT= 248;
    public static final int   MOD_FILTERPICK     = 249;
    public static final int   MOD_TRIG_PUB       = 250;
    // Do not exceed 255!

    // locally defined
    public static final int   MOD_GROMOPACKET    = 100;

//  public EarthwormTypes() {
//  }


}
/*

++++++++++++++++++++++++++++++++++++++++++++++
from file "earthworm_glogal.d"
++++++++++++++++++++++++++++++++++++++++++++++
#
#                    earthworm_global.d

#      !!!!   DO NOT make any changes to this file.  !!!!

#  The contents of this file are under the control of Earthworm Central.
#       Please direct any requested additions or changes to:
#
#             Barbara Bogaert    bogaert@usgs.gov

#  The values defined in earthworm_global.d are critical to proper
#  communication and data attribution among Earthworm systems around
#  the globe.

#  A copy of earthworm_global.d should be placed in your EW_PARAMS directory.
#
#  The master copy of earthworm_global.d resides in the vX.XX/environment
#  directory of this Earthworm distribution.



#--------------------------------------------------------------------
#                        Installation IDs
#
#   The character string <-> numerical value mapping for all
#   installations are assigned and controlled by the Earthworm
#   Development Team at the USGS in Golden, CO.  These mappings
#   must remain globally unique in order for exchanged data
#   to be properly attributed to the installation that created it.
#
#   0-255 are the only valid installation ids.
#--------------------------------------------------------------------

 Installation   INST_WILDCARD    0   # Sacred wildcard value - DO NOT CHANGE!!!
 Installation   INST_FAIRBANKS   1
 Installation   INST_UW          2
 Installation   INST_MENLO       3
 Installation   INST_CIT         4
 Installation   INST_UTAH        5
 Installation   INST_MEMPHIS     6
 Installation   INST_UNR         7
 Installation   INST_UCB         8
 Installation   INST_PTWC        9
 Installation   INST_IDA        10
 Installation   INST_NC         11
 Installation   INST_VT         12
 Installation   INST_USNSN      13
 Installation   INST_RICKS      14
 Installation   INST_HVO        15
 Installation   INST_ATWC       16
 Installation   INST_PGE        17
 Installation   INST_PNNL       18
 Installation   INST_PGC        19
 Installation   INST_AVO        20
 Installation   INST_BUTTE      21
 Installation   INST_LAMONT     22
 Installation   INST_SC_CHA     23
 Installation   INST_STLOUIS    24
 Installation   INST_NMT        25
 Installation   INST_CVO        26
 Installation   INST_SC_COL     27


#--------------------------------------------------------------------------
#                          Message Types
#
#  Define all message name/message-type pairs that will be global
#  to all Earthworm systems.
#
#  VALID numbers are:
#
#   0- 99 Message types 0-99 are defined in the file earthworm_global.d.
#         These numbers are reserved by Earthworm Central to label types
#         of messages which may be exchanged between installations. These
#         string/value mappings must be global to all Earthworm systems
#         in order for exchanged messages to be properly interpreted.
#
#
#  OFF-LIMITS numbers:
#
# 100-255 Message types 100-255 are defined in each installation's
#         earthworm.d file, under the control of each Earthworm
#         installation. These values should be used to label messages
#         which remain internal to an Earthworm system or installation.
#
#--------------------------------------------------------------------------

# Global Earthworm message-type mappings (0-99):
 Message  TYPE_WILDCARD        0  # wildcard value - DO NOT CHANGE!!!
 Message  TYPE_ADBUF           1  # multiplexed waveforms from DOS adsend
 Message  TYPE_ERROR           2  # error message
 Message  TYPE_HEARTBEAT       3  # heartbeat message
 Message  TYPE_NANOBUF         5  # single-channel waveforms from nanometrics
 Message  TYPE_PICK2K         10  # P-wave arrival time (with 4 digit year)
                                  #   from pick_ew
 Message  TYPE_CODA2K         11  # coda info (plus station code) from pick_ew
#Message  TYPE_PICK2          12  # P-wave arrival time from picker & pick_ew
#Message  TYPE_CODA2          13  # coda info from picker & pick_ew
 Message  TYPE_HYP2000ARC     14  # hyp2000 (Y2K hypoinverse) event archive
                                  #   msg from eqproc/eqprelim
 Message  TYPE_H71SUM2K       15  # hypo71-format hypocenter summary msg
                                  #   (with 4-digit year) from eqproc/eqprelim
#Message  TYPE_HINVARC        17  # hypoinverse event archive msg from
                                  #   eqproc/eqprelim
#Message  TYPE_H71SUM         18  # hypo71-format summary msg from
                                  #   eqproc/eqprelim
 Message  TYPE_TRACEBUF       20  # single-channel waveforms from NT adsend,
                                  #   getdst2, nano2trace, rcv_ew, import_ida...
 Message  TYPE_LPTRIG         21  # single-channel long-period trigger from
                                  #   lptrig & evanstrig
 Message  TYPE_CUBIC          22  # cubic-format summary msg from cubic_msg
 Message  TYPE_CARLSTATRIG    23  # single-channel trigger from carlstatrig
#Message  TYPE_TRIGLIST       24  # trigger-list msg (used by tracesave modules)
                                  #   from arc2trig, trg_assoc, carlsubtrig
 Message  TYPE_TRIGLIST2K     25  # trigger-list msg (with 4-digit year) used
                                  #   by tracesave modules from arc2trig,
                                  #   trg_assoc, carlsubtrig
 Message  TYPE_TRACE_COMP_UA  26  # compressed waveforms from compress_UA

++++++++++++++++++++++++++++++++++++++++++++++
from file "earthworm.d"
++++++++++++++++++++++++++++++++++++++++++++++

#--------------------------------------------------------------------------
#                           Module IDs
#
#  Define all module name/module id pairs for this installation
#  Except for MOD_WILDCARD, all string/value mappings for module ids
#  may be locally altered. The character strings themselves may also
#  be changed to be more meaningful for your installation.
#
#  0-255 are the only valid module ids.
#--------------------------------------------------------------------------

 Module   MOD_WILDCARD        0   # Sacred wildcard value - DO NOT CHANGE!!!
 Module   MOD_ADSEND_A        1
 Module   MOD_ADSEND_B        2
 Module   MOD_ADSEND_C        3
 Module   MOD_PICKER_A        4
 Module   MOD_PICKER_B        5
 Module   MOD_PICK_EW         6
 Module   MOD_COAXTORING_A    7
 Module   MOD_COAXTORING_B    8
 Module   MOD_RINGTOCOAX      9
 Module   MOD_BINDER_EW      10
 Module   MOD_DISKMGR        11
 Module   MOD_STATMGR        14
 Module   MOD_REPORT         15
 Module   MOD_STARTSTOP      18
 Module   MOD_STATUS         19
 Module   MOD_NANOBOX        20
 Module   MOD_WAVESERVER     21
 Module   MOD_PAGERFEEDER    23
 Module   MOD_EQPROC         24
 Module   MOD_TANKPLAYER     25
 Module   MOD_EQALARM_EW     26
 Module   MOD_EQPRELIM       27
 Module   MOD_IMPORT_GENERIC 28
 Module   MOD_EXPORT_GENERIC 29
 Module   MOD_GETDST         31
 Module   MOD_LPTRIG_A       32
 Module   MOD_LPTRIG_B       33
 Module   MOD_TRG_ASSOC      34
 Module   MOD_AD_DEMUX_A     35
 Module   MOD_AD_DEMUX_B     36
 Module   MOD_VDL_EW         37
 Module   MOD_CUBIC_MSG      38
 Module   MOD_GAPLIST        39
 Module   MOD_GETTERW        40
 Module   MOD_WAVESERVERV    41
 Module   MOD_NANO2TRACE     42
 Module   MOD_GETDST2        43
 Module   MOD_EXPORT_SCN     44
 Module   MOD_ARC2TRIG       45
 Module   MOD_RCV_EW         48
 Module   MOD_HELI1          49
 Module   MOD_SENDARC        50
 Module   MOD_EXPORT_PTWC    51
 Module   MOD_EXPORT_PTWC_HYPO   52
 Module   MOD_CARLSTATRIG_ANALOG 220
 Module   MOD_CARLSTATRIG_AE     221
 Module   MOD_CARLSTATRIG_FJ     222
 Module   MOD_CARLSTATRIG_KO     223
 Module   MOD_CARLSTATRIG_PS     224
 Module   MOD_CARLSTATRIG_TZ     225
 Module   MOD_CARLSUBTRIG        226
 Module   MOD_A2M            243
 Module   MOD_CS2EW_AE       227
 Module   MOD_CS2EW_FJ       228
 Module   MOD_CS2EW_KO       229
 Module   MOD_CS2EW_PS       230
 Module   MOD_CS2EW_TZ       231
 Module   MOD_CS2EW_ANALOG   232
 Module   MOD_PICK_EW_AE     233
 Module   MOD_PICK_EW_FJ     234
 Module   MOD_PICK_EW_KO     235
 Module   MOD_PICK_EW_PS     236
 Module   MOD_PICK_EW_TZ     237
 Module   MOD_PICK_EW_ANALOG 238
 Module   MOD_HYP_PUB         240
 Module   MOD_DATASOCK_IMPORT 248
 Module   MOD_FILTERPICK      249
 Module   MOD_TRIG_PUB        250



*/
