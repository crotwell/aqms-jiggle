package org.trinet.jdbc.datatypes;

public interface DataNumber extends DataState {
/** Returns an int primitive equivalent to the object's value attribute.*/
    public abstract int intValue() ;

/** Returns a long primitive equivalent to the object's value attribute.*/
    public abstract long longValue() ;

/** Returns a float primitive equivalent to the object's value attribute.*/
    public abstract float floatValue() ;

/** Returns a double primitive equivalent to the object's value attribute.*/
    public abstract double doubleValue() ;

/** Sets the object's value to the int input argument.
*/
    public abstract void setValue(int value) ;

/** Sets the object's value to the value of the long input argument.
*/
    public abstract void setValue(long value) ;

/** Sets the object's value to the value of the float input argument.
*/
    public abstract void setValue(float value) ;

/** Sets the object's value to the value of the double input argument.
*/
    public abstract void setValue(double value) ;

/** Returns 'true' if the object's value is ! isNull() and acceptable to DataSource constraints.*/
    public abstract boolean isValidNumber() ;
}
