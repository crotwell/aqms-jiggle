package org.trinet.jdbc.datatypes;
public interface DataObjectRangeIF {

/** Returns a reference to the object representing the range minimum. */
    DataObject getMin() ;

/** Returns a reference to the object representing the range maximum. */
    DataObject getMax() ;

/** Returns true if input object's bounds values are equivalent to this instance's. */
    boolean equalsRange(DataObjectRangeIF number) ;

/** Returns true if input object's value is less than this instance's minimum bound. */
    boolean after(DataObject number) ;

/** Returns true if input range's maximum bound is less than this instance's minimum bound.*/
    boolean after(DataObjectRange range) ;

/** Returns true if input object's value is greater than this instance's maximum bound.*/
    boolean before(DataObject number) ;

/** Returns true if input range's minimum bound is greater than this instance's maximum bound.*/
    boolean before(DataObjectRange range) ;

/** Returns true if input object's value is outside the bounds of this instance. */
    boolean excludes(DataObject number) ;

/** Returns true if input range is outside the bounds of this instance. */
    boolean excludes(DataObjectRange range) ;

/** Returns true if input range is outside the bounds of this instance. */
    boolean excludes(DataObject min, DataObject max) ;

/** Returns true if input range is within the bounds of this instance. */
    boolean contains(DataObjectRange range) ;

/** Returns true if input range is within the bounds of this instance. */
    boolean contains(DataObject min, DataObject max) ;

/** Returns true if input object's value is inside the bounds of this instance. */
    boolean contains(DataObject number) ;

/** Returns true if input range overlaps the bounds of this instance. */
    boolean overlaps(DataObjectRange range) ;

/** Returns true if input range overlaps the bounds of this instance. */
    boolean overlaps(DataObject min, DataObject max) ;

/** Returns true if input range encloses the bounds of this instance. */
    boolean within(DataObjectRange range) ;

/** Returns true if input range encloses the bounds of this instance. */
    boolean within(DataObject min, DataObject max) ;

/** Returns (long) (maxValue - minValue) for this instance. */
    long longExtent() ;

/** Returns (double) (maxValue - minValue) for this instance. */
    double doubleExtent() ;

/** Returns true if min > max for this instance. */
    boolean isReversed() ;

/** Returns false if (min == null || max == null || isReversed()) for this instance. */
    boolean isValid() ;

/** 
* Sets the minimum bound of the range to the input.
*/
    void setMin(DataObject min);

/** 
* Sets the maximum bound of the range to the input.
*/
    void setMax(DataObject max);

/** 
* Sets the minimum, maximum bounds of the range to the input range.
*/
    void setLimits(DataObject min, DataObject max);

/** 
* Sets the range bounds to those of the input object.
*/
    void setLimits(DataObjectRange range);

/**
* Sets the appropiate minimum or maximum bound to extend the range to include the input value.
* Does a no-op if contains(DataObject) == true.
*/
    void include(DataObject dataObject);

/**
* Sets the appropiate range bounds to include the input range extent.
* Does a no-op if contains(DataObjectRange) == true.
*/
    void include(DataObjectRange range);

/**
* Returns String concatenation of min and max bound values separated by ", ".
* Inserts "NULL" for value if bound == null or its values is considered undefined.
*/
    String toStringSQL();
}
