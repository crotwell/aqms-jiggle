package org.trinet.jdbc.datatypes;
/** This interface is implemented by any class desiring to define object data states of Update, Null, and Mutable.
* Classes DataTableRow, DataObject, and DataStnChl implement these methods as follows: update state implies the data value has
* been changed and is legitimate, null state implies that the actual value is to be ignored and the value returned is an SQL null,
* and mutable state implies that the value can be changed. The DataObject returned by set setXXX methods is the invoking instance
: (this) object.
*/
public interface DataState {
/** Sets flag to indicate whether the data is modified or ready for further processing.  */
    public DataObject setUpdate(boolean value);
/** Value indicates whether the data value is set or is ready for further processing.  */
    public boolean isUpdate();
/** Sets flag to indicate the data value is regarded as equivalent to a database Null. */
    public DataObject setNull(boolean value);
/** Value indicates whether data is set null or not.  */
    public boolean isNull();
/** Sets flag to indicate whether the data as mutable (non-constant) or not. */
    public DataObject setMutable(boolean value);
/** Value indicates whether data is mutable or not.  */
    public boolean isMutable();
}
