package org.trinet.jdbc.datatypes;
import java.util.*;
import org.trinet.jdbc.*;

/** Extends the DataObject class to implement a stateful String value. */
public class DataString extends DataObject implements DataTime, Comparable, Cloneable {
/** Data member holding value. */
    String value;

/** Constructor sets the object value to NullValueDb.NULL_LONG and the object state flags to their default settings
* (null: true; update: false; mutable: true)).
*/
    public DataString() {
        this.value = NullValueDb.NULL_STRING;
    }
 
/** Constructor sets the object value to input argument and isNull() == false and isUpdate() == true. */
    public DataString(int value) throws NumberFormatException {
        this(Integer.toString(value, 10));
    }

/** Constructor sets the object value to input argument and isNull() == false and isUpdate() == true. */
    public DataString(long value) throws NumberFormatException {
        this(Long.toString(value, 10));
    }

/** Constructor sets the object value to input argument and isNull() == false and isUpdate() == true. */
    public DataString(float value) throws NumberFormatException {
        this(Float.toString(value));
    }

/** Constructor sets the object value to input argument and isNull() == false and isUpdate() == true. */
    public DataString(double value) throws NumberFormatException {
        this(Double.toString(value));
    }

/** Constructor sets the object value to input argument and isNull() == false and isUpdate() == true. */
    public DataString(java.util.Date value) {
        this((value == null) ? (String) null : value.toString());
    }

/** Constructor sets the object value to input argument and isNull() == false and isUpdate() == true. */
    public DataString(java.sql.Timestamp value) {
        this((value == null) ? (String) null : value.toString());
    }

/** Constructor sets the object value to input argument and isNull() == false and isUpdate() == true. 
* Leading and trailing blanks are trimmed off the string.
* Sets value to null state if input is null.
*/
    public DataString(String value) {
        if (value == null) setNull(true);
        //else setValue(value.trim()); // removed 08/03/2005 -aww
        else setValue(value); // aww fix to address P. Lombard problem with "  " Channel Location Code
    }

/** Constructor copies the object value and state flag settings of the input argument object.
* Null state is propagated.
* */
    public DataString(DataString object) {
        if (object == null || object.isNull()) setNull(true);
        else {
            this.value = object.value;
            this.valueUpdate = object.valueUpdate;
            this.valueNull = object.valueNull;
            this.valueMutable = object.valueMutable;
        }
    }

/** Concatenate input String to this object's value. No-op if input is null. Returns this instance.*/
    public DataString concat(String str) {
        if (str == null || (str.length() == 0)) return this;
        if (value == null) setValue(str);
        else setValue(value.concat(str)); 
        return this;
    }

/** Returns a copy of the String representation of the object value.*/
    public String toString() {
        return value; 
    }

/** Returns String representation of the object value or the string "NULL" if isNull() == true. */
    public String toStringSQL() {
        if (valueNull) return "NULL";
        return StringSQL.valueOf(value, false);
    }

/** Returns a String of "label: value" pairs for the object value and its state flags. 
* If isNull() == true the string "NULL" is printed for the value.
* "Value: " + value.toString() + " Null: " + isNull() + " Update: " + isUpdate() + " Mutable: " + isMutable()
*/
    public String classToString() {
        StringBuffer sb = new StringBuffer(128);
        sb.append("                                           ");
        int maxlen = value.length();
        if (maxlen > 24) maxlen = 24;
        sb.insert(0, "Value:");
        if (isNull()) sb.insert(7, "NULL");
        else sb.insert(7, value);
        sb.insert(32, "Null:");
        sb.insert(37, valueNull);
        sb.insert(43, "Update:");
        sb.insert(50, valueUpdate);
        sb.insert(56, "Mutable:");
        sb.insert(64, valueMutable);
        String tmp = sb.toString().trim();
        if (value.length() < 25) return tmp;
        sb.delete(0,sb.length());
        sb.append(tmp);
        sb.append("\nEntireString: \"");
        sb.append(value);
        sb.append("\"");
        return sb.toString();
//        return  "Value: " + value + " Null: " + valueNull + " Update: " + valueUpdate + " Mutable: " + valueMutable;
    }

/** Returns hashcode for the object value. */
    public int hashCode() {
        return value.hashCode(); 
    }

/** Returns true if this object value and its state flags are equivalent to those of the input object.
* Returns false if the input object is null or is not of type DataString.
*/
    public boolean equals(Object object) {
        if (object == null || ! (object instanceof DataString)) return false;
        if (value.equals(((DataString) object).value) && 
           valueUpdate == ((DataString) object).valueUpdate && 
           valueMutable == ((DataString) object).valueMutable && 
           valueNull == ((DataString) object).valueNull) return true;
        else return false;
    }

/** Returns true if the object value is equivalent to that of the input object.
* Returns false if the input object is null or the input argument is not of class type DataString or String.
* The state flags are not compared.
*/
    public boolean equalsValue(Object object) {
        if (object == null) return false;
        if (object instanceof DataString) {
            return ( value.equals(((DataString) object).value) );
        }
        else if (object instanceof String) {
            return (value.equals((String) object) );
        }
        else return false; 
    }

/** Returns 0 if this.value == object.value; returns 1 if this.value > object.value;
 * returns -1 if this.value < object.value.
* Throws ClassCastException if input object is not of type DataString or String.
*/
    public int compareTo(Object object) throws ClassCastException {
        if (object instanceof String) {
            return this.value.compareTo((String)object);
        }
        else if (object instanceof DataString) {
            return compareTo((DataString) object);
        }
        else throw new ClassCastException("compareTo(object) argument must be a String or DataString class type: "
                                + object.getClass().getName());
    }

/** Returns 0 if this.value == object.value; returns 1 if this.value > object.value;
 * returns -1 if this.value < object.value.
*/
    public int compareTo(DataString object) {
        return this.value.compareTo(object.value);
    }

/** Overrides super.clone() method. */
    public Object clone() {
        DataString obj = null;
        obj = (DataString) super.clone();
        return obj;
    }

/** Returns the value parsed as an int. */
    public int intValue() throws NumberFormatException {
        return Integer.parseInt(value);
    }

/** Returns the value parsed as an long. */
    public long longValue() throws NumberFormatException {
        return Long.parseLong(value);
    }

/** Returns the value parsed as an float. */
    public float floatValue() throws NumberFormatException {
        if (value.equals("NaN")) return Float.NaN;
        if (value.equals("Infinity")) return Float.POSITIVE_INFINITY;
        if (value.equals("-Infinity")) return Float.NEGATIVE_INFINITY;
        return Float.parseFloat(value);
    }

/** Returns the value parsed as a double. */
    public double doubleValue() throws NumberFormatException {
        if (value.equals("NaN")) return Double.NaN;
        if (value.equals("Infinity")) return Double.POSITIVE_INFINITY;
        if (value.equals("-Infinity")) return Double.NEGATIVE_INFINITY;
        return Double.parseDouble(value);
    }

/** Returns a Date object if the string is of the form input to org.trinet.util.EpochTime.stringToDate(String).
* else if the string is numeric, the time millisecs value is set to the string parsed as a long value * 1000.
* Returns null if valid date cannot be parsed.
* @see java.util.Date
* @see java.sql.Date
*/
    public java.util.Date dateValue() {
        java.util.Date date = org.trinet.util.EpochTime.stringToDate(value);
        if (date == null) {
            try {
                date = new java.util.Date(Long.parseLong(value)*1000);
            }
            catch (NumberFormatException ex) {
               System.err.println(ex.toString());
               System.err.println("DataString.dateValue() cannot parse string as Date.");
            }
        }
        return date;
    }

/** Returns a Timestamp object where the time is set to the value, if it is an SQL Timestamp formatted string,
* if the string is numeric, the time millisecs value is set to the string parsed as a long value * 1000.
* Returns null if valid timestamp cannot be parsed.
* @see java.sql.Timestamp
*/
    public java.sql.Timestamp timestampValue() {
        java.sql.Timestamp ts = null;
        try {
            ts = java.sql.Timestamp.valueOf(value);
        }
        catch (IllegalArgumentException ex) {
            try {
                ts = new java.sql.Timestamp(Long.parseLong(value)*1000);
            }
            catch (NumberFormatException ex2) {
               System.err.println(ex2.toString());
               System.err.println("DataString.timestampValue() cannot parse string as Timestamp.");
            }
        }
        return ts;
    }

/** Sets the object value to the input formatted as a String.
* Does a no-op if isMutable() == false.
* Sets the state flags isNull() == false and isUpdate() == true .
*/
    public void setValue(int value) {
        setValue(Integer.toString(value, 10));
    }

/** Sets the object value to the input formatted as a String.
* Does a no-op if isMutable() == false.
* Sets the state flags isNull() == false and isUpdate() == true .
*/
    public void setValue(long value) {
        setValue(Long.toString(value, 10));
    }

/** Sets the object value to the input formatted as a String.
* Does a no-op if isMutable() == false.
* Sets the state flags isNull() == false and isUpdate() == true .
*/
    public void setValue(float value) {
        setValue(Float.toString(value));
    }

/** Sets the object value to the input formatted as a String.
* Does a no-op if isMutable() == false.
* Sets the state flags isNull() == false and isUpdate() == true .
*/
    public void setValue(double value) {
        setValue(Double.toString(value));
    }

/** Sets the object value to the input formatted as a String.
* Does a no-op if isMutable() == false.
* Sets the state flags isUpdate() == true and isNull() == false,
* unless input is null.
*/
    public void setValue(java.util.Date value) {
        if(! isMutable()) return;
        if (value == null) setNull(true);
        else setValue(value.toString());
    }

/** Sets the object value to the input formatted as a String.
* Does a no-op if isMutable() == false.
* Sets the state flags isUpdate() == true and isNull() == false,
* unless input is null.
*/
    public void setValue(java.sql.Timestamp value) {
        if(! isMutable()) return;
        if (value == null) setNull(true);
        else setValue(value.toString());
    }

/** Sets the object value to the input object value interpreted as a String. 
* Sets the value to string "NULL" if input object is null.
* Does a no-op if isMutable() == false.
* Sets the state flags isUpdate() == true and isNull() == false,
* unless input is null or if input is a DataObject, object.isNull().
*/
    public void setValue(Object object) {
        if(! isMutable()) return;
//        if (object == null)
//           throw new NullPointerException("setValue(Object) argument null" +
//           object.getClass().getName());
        if (object == null) {
          setNull(true);
          return;
        }
        // 08/01/03 aww force to null state
        if (object instanceof DataObject && ((DataObject) object).isNull() ) {
          setNull(true);
          return;
        }
        setValue((String) object.toString());
    }

/** Sets the object value to the input object value. 
* Sets the value to string "NULL" if input object is null.
* Does a no-op if isMutable() == false.
* Sets the state flags isNull() == false and isUpdate() == true .
*/
    public void setValue(String value) {
        if(! isMutable()) return;
        if (value == null) setNull(true);
        else {
          this.value = value;
          this.valueNull = false;
          this.valueUpdate = true;
        }
    }

/** Sets the object value to NullValueDb.NULL_STRING.
* Sets the state flags isNull() == tf and isUpdate() == true .
* Does a no-op if isMutable() == false.
*/
    public DataObject setNull(boolean tf) {
        if(! isMutable()) return this;
        this.value = NullValueDb.NULL_STRING;
        this.valueNull = tf;
        this.valueUpdate = true;
        return this;
    }


/**
* Returns true if a value can be parsed from input StringTokenizer.
* Does not set value and returns false if tokenizer.hasMoreTokens() == false.
*/
    public boolean parseValue(StringTokenizer tokenizer) {
        if (! tokenizer.hasMoreTokens()) return false;
        setValue(tokenizer.nextToken());
        return true;
    }

    /** Returns true if value is "NULL", "NaN", all blanks, or an empty string. */
    public boolean isBlankOrNull() {
        return NullValueDb.isBlank(value);
    }
}
