package org.trinet.jdbc.datatypes;
import java.util.*;
import org.trinet.jdbc.*;
import org.trinet.util.LeapSeconds;

/** Extends the DataObject class to implement a stateful long value. */
public class DataLong extends DataObject implements DataNumeric, DataTime, Comparable, Cloneable {
    long value;

/** Constructor sets the object value to NullValueDb.NULL_LONG and the object state flags to their default settings
* (null: true; update: false; mutable: true)).
*/
    public DataLong () {
        this.value = NullValueDb.NULL_LONG;
    }
 
/** Constructor sets the object value to input argument and isNull() == false and isUpdate() == true. */
    public DataLong (int value) {
        this((long) value);
    }

/** Constructor sets the object value to input argument and isNull() == false and isUpdate() == true. */
    public DataLong (long value) {
        this.value = value;
        this.valueNull = false;
        this.valueUpdate = true;
    }

/** Constructor sets the object value to input argument.
* Sets the state flags isUpdate() == true and isNull() == false, unless input satisifies Float.isNaN(value).
 * */
    public DataLong (float value) {
        setValue(value);
    }

/** Constructor sets the object value to input argument.
* Sets the state flags isUpdate() == true and isNull() == false, unless input satisifies Float.isNaN(value).
 * */
    public DataLong (double value) {
        setValue(value);
    }

/** Constructor sets the object value to input argument and isNull() == false and isUpdate() == true.
* Sets the state flags isUpdate() == true and isNull() == false, unless input is null.
 * */
    public DataLong (String value) throws NumberFormatException {
        this();
        setValue(value);
    }

/** Constructor copies the object value and state flag settings of the input argument object. 
* Null state is propagated.
*/
    public DataLong (DataLong object) {
        if (object == null) setNull(true);
        else {
            this.value = object.value;
            this.valueUpdate = object.valueUpdate;
            this.valueNull = object.valueNull;
            this.valueMutable = object.valueMutable;
        }
    }

/** Returns String representation of the object value.*/
    public String toString() {
        return String.valueOf(value); 
    }
/** Returns String representation of the object value or the string "NULL" if isNull() == true. */
    public String toStringSQL() {
        if (valueNull) return "NULL";
        return StringSQL.valueOf(value);
    }

/** Returns a String of "label: value" pairs for the object value and its state flags. 
* If isNull() == true the string "NULL" is printed for the value.
* "Value: " + value.toString() + " Null: " + isNull() + " Update: " + isUpdate() + " Mutable: " + isMutable()
*/
    public String classToString() {
        StringBuffer sb = new StringBuffer(128);
        sb.append("                                           ");
        sb.insert(0, "Value:");
        if (isNull()) sb.insert(7, "NULL");
        else sb.insert(7, value);
        sb.insert(32, "Null:");
        sb.insert(37, valueNull);
        sb.insert(43, "Update:");
        sb.insert(50, valueUpdate);
        sb.insert(56, "Mutable:");
        sb.insert(64, valueMutable);
        return sb.toString();
//        return  "Value: " + value + " Null: " + valueNull + " Update: " + valueUpdate + " Mutable: " + valueMutable;
    }

/** Returns hashcode for the object value. */
    public int hashCode() {
        return (int) (this.value^(this.value>>>32)) ; 
    }

/** Returns true if this object value and its state flags are equivalent to those of the input object.
* Returns false if the input object is null or is not of type DataLong.
*/
    public boolean equals(Object object) {
        if (object == null || ! (object instanceof DataLong)) return false;
        if (value == ((DataLong) object).value && 
           valueUpdate == ((DataLong) object).valueUpdate && 
           valueMutable == ((DataLong) object).valueMutable && 
           valueNull == ((DataLong) object).valueNull) return true;
        else return false;
    }

/** Returns true if the object value is equivalent to that of the input object.
* Returns false if the input object is null or the input argument is not of class type DataObject or Number.
* The state flags are not compared.
*/
    public boolean equalsValue(Object object) {
        if (object == null ) return false;
        if (object instanceof DataObject) {
            return ( value == ((DataObject) object).longValue() );
        }
        else if (object instanceof Number) {
            return ( value == ((Number) object).longValue() );
        }
        else return false; 
    }

/** Returns 0 if this.value == object.value; returns 1 if this.value > object.value;
 * returns -1 if this.value < object.value.
* Throws ClassCastException if input object is not of type DataLong or Long.
*/
    public int compareTo(Object object) throws ClassCastException {
        if (object instanceof Long) { 
            if (this.value == ((Long) object).longValue()) return 0;
            else if (this.value > ((Long) object).longValue()) return 1;
            else return -1;
        }
        else if (object instanceof DataLong) { 
            return compareTo((DataLong) object);
        }
        else throw new ClassCastException("compareTo(object) argument must be a Long or DataLong class type: "
                        + object.getClass().getName());
    }

/** Returns 0 if this.value == object.value; returns 1 if this.value > object.value;
 * returns -1 if this.value < object.value.
*/
    public int compareTo(DataLong object) {
        if (this.value == object.value) return 0;
        else if (this.value > object.value) return 1;
        else return -1;
    }


/** Returns the value cast as an int. */
    public int intValue() {
        return (int) value;
    }

/** Returns the value cast as an long. */
    public long longValue() {
        return (long) value;
    }

/** Returns the value cast as an float. */
    public float floatValue() {
        return (float) value;
    }

/** Returns the value cast as a double. */
    public double doubleValue() {
        return (double) value;
    }

/** Returns a Date object where the time is this true epoch seconds value converted to nominal millisseconds. */
    public java.util.Date dateValue() {
        return new java.util.Date(Math.round(LeapSeconds.trueToNominal((double)value)*1000.)); // for UTC seconds to date -aww 2008/02/12
    }

/** Returns a Timestamp object where time is this true epoch seconds value converted to nominal milliseconds. */
    public java.sql.Timestamp timestampValue() {
        return new java.sql.Timestamp(Math.round(LeapSeconds.trueToNominal((double)value)*1000.)); // for UTC seconds to timestamp -aww 2008/02/12
    }

/** Sets the object value to the input cast as a long.
* Does a no-op if isMutable() == false.
* Sets the state flags isNull() == false and isUpdate() == true .
*/
    public void setValue(int value) {
        setValue((long) value);
    }

/** Sets the object value to the input cast as a long.
* Does a no-op if isMutable() == false.
* Sets the state flags isNull() == false and isUpdate() == true .
*/
    public void setValue(long value) {
        if (! isMutable()) return;
        this.value = value;
        this.valueNull = false;
        this.valueUpdate = true;
    }

/** Sets the object value to the input "rounded" to a long.
* Does a no-op if isMutable() == false.
* Sets the state flags isUpdate() == true and isNull() == false,
* unless input satisifies Float.isNaN(value).
*/
    public void setValue(float value) {
        if (Float.isNaN(value)) setNull(true);
        else setValue((long) Math.round(value)); // used to truncate, now round -aww 20100507
    }

/** Sets the object value to the input "rounded" to a long.
* Does a no-op if isMutable() == false.
* Sets the state flags isUpdate() == true and isNull() == false,
* unless input satisifies Float.isNaN(value).
*/
    public void setValue(double value) {
        if (Double.isNaN(value)) setNull(true);
        setValue(Math.round(value)); // used to truncate, now round -aww 20100507
    }

/** Sets the object value to the input object value interpreted as a long. 
* Does a no-op if isMutable() == false.
* Sets the state flags isUpdate() == true and isNull() == false,
* unless input is null or if input is a DataObject, object.isNull().
* Throws a ClassCastException if input object is not of type DataObject, Number, or String.
* Throws a NumberFormatException if String object cannot be parsed as a number.
*/
    public void setValue(Object object) throws ClassCastException, NumberFormatException  {
        if (! isMutable()) return;
//        if (object == null) throw new NullPointerException("setValue(Object) argument null");
        if (object == null) {
            setNull(true);
        }
        else if (Number.class.isInstance(object)) {
            setValue(((Number) object).longValue());
        }
        else if (DataObject.class.isInstance(object)) {
            DataObject obj = (DataObject) object;
            if (obj.isNull()) setNull(true);
            else setValue(obj.longValue());
            //setValue(((DataObject) object).longValue());
        }
        else if (String.class.isInstance(object)) {
            setValue(Long.parseLong((String) object));
        }
        else throw new ClassCastException("setValue(Object) invalid object argument class type: " + object.getClass().getName());
    }

/** Sets the object value to NullValueDb.NULL_LONG.
* Sets the state flags isNull() == tf and isUpdate() == true.
* Does a no-op, if isMutable == false. 
*/
    public DataObject setNull(boolean tf) {
        if (! isMutable()) return this;
        this.value = NullValueDb.NULL_LONG;
        this.valueNull = tf;
        this.valueUpdate = true;
        return this;
    }


/**
* Returns true if a value can be parsed from input StringTokenizer.
* Does not set value and returns false if tokenizer.hasMoreTokens() == false
* or a float cannot be parsed from token.
*/
    public boolean parseValue(StringTokenizer tokenizer) {
        if (! tokenizer.hasMoreTokens()) return false;
        boolean retVal = false;
        try {
            setValue( Long.parseLong(tokenizer.nextToken()) );
            retVal = true;
        }
        catch (NumberFormatException ex) {
            System.err.println("DataLong parseValue()" + ex.getMessage());
        }
        return retVal;
    }

    public boolean isValidNumber() {
        return ! isNull();
    }
    public boolean isValid() {
        return isValidNumber();
    }
}
