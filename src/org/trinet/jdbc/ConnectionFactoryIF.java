package org.trinet.jdbc;
import org.trinet.jiggle.database.JiggleConnection;

import java.sql.*;
public interface ConnectionFactoryIF {
    public boolean onServer() ;
    public String getStatus() ;
    public Connection createConnection(String url, String driverName, String user, String passwd) ;
    public Connection createInternalDbServerConnection() ;

    public JiggleConnection getJiggleConnection(Connection conn); // Get Jiggle DB specific connection object
}
