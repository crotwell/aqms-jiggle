package org.trinet.jdbc;
import java.sql.*;
import org.trinet.jasi.*;

/**
 * Utility class to create the JDBC connections.
 * 
 */
public class JDBConn {
    private static ConnectionFactoryIF factory;

    static {
      initFactory();
    }

    // Null constructor
    private JDBConn() {}

    private static void initFactory() {
        initFactory(JasiObject.getDefault());
    }
    private static void initFactory(int type) {
        switch (type) {
          case JasiFactory.TRINET:
          case JasiFactory.EARTHWORM:
            factory = new ConnectionFactory();
            break;
          default:
           System.err.println("JDBConn unknown connection factory type id: " + type);
           factory = null;
        }
    }

    public static boolean onServer() {
        return (factory == null) ? false : factory.onServer();
    }

    /**
     * Creates new Connection instance using the String url and user data.
     * If input driverName is not null or empty, creates instance of class specified by driverName
     * and registers it with the DriverManager, otherwise, a default driver is used. 
     * DOES NOT set the static class Connection object returned by getConnection().
     * Use to create mutiple Connection objects with different URL's.
     * */
    public static Connection createConnection(String url, String driverName, String user, String passwd){
        return factory.createConnection(url, driverName, user, passwd);
    }

/** Returns the connection object for the server driver of the host machine database.
*   Closes existing non-null connection. This is for server-side use only.
*/
    public static Connection createInternalDbServerConnection() {
        return factory.createInternalDbServerConnection();
    }

    /** Return the status string of the last connection creation. */
    public static String getStatus() {
        return (factory == null) ? "Null connection" : factory.getStatus();
    }
}
