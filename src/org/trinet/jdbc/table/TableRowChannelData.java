package org.trinet.jdbc.table;
import org.trinet.jdbc.datatypes.DataClassIds;
/** Interface of static data constants defining the named table.
* @see ChannelData
*/
public interface TableRowChannelData extends DataClassIds {

/** Name of schema database table represented by this class.
*/
    public static final String DB_TABLE_NAME =  "CHANNEL_DATA";

/** Number of column data fields in a table row.
*/
    public static final int MAX_FIELDS =  24;

/** Id sequence name for primary key column
*/
    public static String SEQUENCE_NAME = "";


/**  CHANNEL_DATA table "net" column data object offset in collection stored by implementing class.
*/
    public static final int NET = 0;

/**  CHANNEL_DATA table "sta" column data object offset in collection stored by implementing class.
*/
    public static final int STA = 1;

/**  CHANNEL_DATA table "seedchan" column data object offset in collection stored by implementing class.
*/
    public static final int SEEDCHAN = 2;

/**  CHANNEL_DATA table "channel" column data object offset in collection stored by implementing class.
*/
    public static final int CHANNEL = 3;

/**  CHANNEL_DATA table "channelsrc" column data object offset in collection stored by implementing class.
*/
    public static final int CHANNELSRC = 4;

/**  CHANNEL_DATA table "location" column data object offset in collection stored by implementing class.
*/
    public static final int LOCATION = 5;

/**  CHANNEL_DATA table "inid" column data object offset in collection stored by implementing class.
*/
    public static final int INID = 6;

/**  CHANNEL_DATA table "remark" column data object offset in collection stored by implementing class.
*/
    public static final int REMARK = 7;

/**  CHANNEL_DATA table "unit_signal" column data object offset in collection stored by implementing class.
*/
    public static final int UNIT_SIGNAL = 8;

/**  CHANNEL_DATA table "unit_calib" column data object offset in collection stored by implementing class.
*/
    public static final int UNIT_CALIB = 9;

/**  CHANNEL_DATA table "lat" column data object offset in collection stored by implementing class.
*/
    public static final int LAT = 10;

/**  CHANNEL_DATA table "lon" column data object offset in collection stored by implementing class.
*/
    public static final int LON = 11;

/**  CHANNEL_DATA table "elev" column data object offset in collection stored by implementing class.
*/
    public static final int ELEV = 12;

/**  CHANNEL_DATA table "edepth" column data object offset in collection stored by implementing class.
*/
    public static final int EDEPTH = 13;

/**  CHANNEL_DATA table "azimuth" column data object offset in collection stored by implementing class.
*/
    public static final int AZIMUTH = 14;

/**  CHANNEL_DATA table "dip" column data object offset in collection stored by implementing class.
*/
    public static final int DIP = 15;

/**  CHANNEL_DATA table "format_id" column data object offset in collection stored by implementing class.
*/
    public static final int FORMAT_ID = 16;

/**  CHANNEL_DATA table "record_length" column data object offset in collection stored by implementing class.
*/
    public static final int RECORD_LENGTH = 17;

/**  CHANNEL_DATA table "samprate" column data object offset in collection stored by implementing class.
*/
    public static final int SAMPRATE = 18;

/**  CHANNEL_DATA table "clock_drift" column data object offset in collection stored by implementing class.
*/
    public static final int CLOCK_DRIFT = 19;

/**  CHANNEL_DATA table "flags" column data object offset in collection stored by implementing class.
*/
    public static final int FLAGS = 20;

/**  CHANNEL_DATA table "ondate" column data object offset in collection stored by implementing class.
*/
    public static final int ONDATE = 21;

/**  CHANNEL_DATA table "offdate" column data object offset in collection stored by implementing class.
*/
    public static final int OFFDATE = 22;

/**  CHANNEL_DATA table "lddate" column data object offset in collection stored by implementing class.
*/
    public static final int LDDATE = 23;
/** String of know column names delimited by ",".
*/
    public static final String COLUMN_NAMES =
   "NET,STA,SEEDCHAN,CHANNEL,CHANNELSRC,LOCATION,INID,REMARK,UNIT_SIGNAL,UNIT_CALIB,LAT,LON,ELEV,EDEPTH,AZIMUTH,DIP,FORMAT_ID,RECORD_LENGTH,SAMPRATE,CLOCK_DRIFT,FLAGS,ONDATE,OFFDATE,LDDATE";

/** String of table qualified column names delimited by ",".
*/
    public static final String QUALIFIED_COLUMN_NAMES = 
    "CHANNEL_DATA.NET,CHANNEL_DATA.STA,CHANNEL_DATA.SEEDCHAN,CHANNEL_DATA.CHANNEL,CHANNEL_DATA.CHANNELSRC,CHANNEL_DATA.LOCATION,CHANNEL_DATA.INID,CHANNEL_DATA.REMARK,CHANNEL_DATA.UNIT_SIGNAL,CHANNEL_DATA.UNIT_CALIB,CHANNEL_DATA.LAT,CHANNEL_DATA.LON,CHANNEL_DATA.ELEV,CHANNEL_DATA.EDEPTH,CHANNEL_DATA.AZIMUTH,CHANNEL_DATA.DIP,CHANNEL_DATA.FORMAT_ID,CHANNEL_DATA.RECORD_LENGTH,CHANNEL_DATA.SAMPRATE,CHANNEL_DATA.CLOCK_DRIFT,CHANNEL_DATA.FLAGS,CHANNEL_DATA.ONDATE,CHANNEL_DATA.OFFDATE,CHANNEL_DATA.LDDATE";

/**  Table column data field names.
*/
    public static final String [] FIELD_NAMES  = {
	"NET", "STA", "SEEDCHAN", "CHANNEL", "CHANNELSRC", 
	"LOCATION", "INID", "REMARK", "UNIT_SIGNAL", "UNIT_CALIB", 
	"LAT", "LON", "ELEV", "EDEPTH", "AZIMUTH", 
	"DIP", "FORMAT_ID", "RECORD_LENGTH", "SAMPRATE", "CLOCK_DRIFT", 
	"FLAGS", "ONDATE", "OFFDATE", "LDDATE"
    };

/** Nullable table column field.
*/
    public static final boolean [] FIELD_NULLS = {
	false, false, false, true, true, 
	false, true, true, false, false, 
	true, true, true, true, true, 
	true, false, true, false, true, 
	true, false, true, true
    };

/**  Table column data field object class identifiers.
* @see org.trinet.jdbc.datatypes.DataClassIds
* @see org.trinet.jdbc.datatypes.DataClasses
*/
    public static final int [] FIELD_CLASS_IDS = {
	DATASTRING, DATASTRING, DATASTRING, DATASTRING, DATASTRING, 
	DATASTRING, DATALONG, DATASTRING, DATALONG, DATALONG, 
	DATADOUBLE, DATADOUBLE, DATADOUBLE, DATADOUBLE, DATADOUBLE, 
	DATADOUBLE, DATALONG, DATALONG, DATADOUBLE, DATADOUBLE, 
	DATASTRING, DATADATE, DATADATE, DATADATE
    };

/**  Column indices of primary key table columns.
*/
    public static final int [] KEY_COLUMNS = {2, 5, 1, 21, 0};

/**  Number of decimal fraction digits in table column data fields.
*/
    public static final int [] FIELD_DECIMAL_DIGITS = {
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0
    };

/** Numeric sizes (width) of the table column data fields.
*/
    public static final int [] FIELD_SIZES = {
	8, 6, 3, 3, 8, 2, 8, 30, 8, 8, 126, 126, 126, 126, 126, 126, 8, 8, 126, 126, 
	27, 7, 7, 7
    };

/** Default table column field values.
*/
    public static final String [] FIELD_DEFAULTS = {
	null, null, null, null, null, null, null, null, null, null, 
	null, null, null, null, null, null, null, null, null, null, 
	null, null, null, "(SYS_EXTRACT_UTC(CURRENT_TIMESTAMP))"
    };
}
