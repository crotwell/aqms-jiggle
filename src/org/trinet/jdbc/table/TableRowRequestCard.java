package org.trinet.jdbc.table;

import org.trinet.jdbc.datatypes.DataClassIds;

/** Interface of static data constants defining the named table.
 *
 * Supercedes older TableRow WaveformRequest(WAVEFORM_REQUEST table)
* @see RequestCard
*
 <pre>
 SQL> describe request_card
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
 EVID                                               NUMBER(15)
 AUTH                                      NOT NULL VARCHAR2(15)
 SUBSOURCE                                 NOT NULL VARCHAR2(8)
 NET                                       NOT NULL VARCHAR2(8)
 STA                                       NOT NULL VARCHAR2(8)
 SEEDCHAN                                  NOT NULL VARCHAR2(8)
 STAAUTH                                   NOT NULL VARCHAR2(15)
 CHANNEL                                   NOT NULL VARCHAR2(8)
 DATETIME_ON                               NOT NULL NUMBER(25,10)
 DATETIME_OFF                              NOT NULL NUMBER(25,10)
 REQUEST_TYPE                              NOT NULL VARCHAR2(1)
 LDDATE                                             DATE
 RCID                                               NUMBER(15)
 LOCATION                                  NOT NULL VARCHAR2(2)
 RETRY                                              NUMBER(38)
 LASTRETRY                                          DATE
 PRIORITY                                           NUMBER

 CONSTRAINT_NAME                SEARCH_CONDITION
------------------------------ --------------------------------------------------------------------------------
SYS_C008252                    "AUTH" IS NOT NULL
SYS_C008253                    "SUBSOURCE" IS NOT NULL
SYS_C008254                    "NET" IS NOT NULL
SYS_C008255                    "STA" IS NOT NULL
SYS_C008256                    "SEEDCHAN" IS NOT NULL
SYS_C008257                    "STAAUTH" IS NOT NULL
SYS_C008258                    "CHANNEL" IS NOT NULL
SYS_C008259                    "DATETIME_ON" IS NOT NULL
SYS_C008260                    "DATETIME_OFF" IS NOT NULL
SYS_C008261                    "REQUEST_TYPE" IS NOT NULL
SYS_C008262                    "LOCATION" IS NOT NULL
REQ01                          request_type in('T','C')
REQ02                          retry >=0
REQUEST_CARDKEY01
 </pre>
 */
// DDG 6/25/03

public interface TableRowRequestCard extends DataClassIds {

/** Name of schema database table represented by this class. */
    public static final String DB_TABLE_NAME =  "REQUEST_CARD";

/** Number of column data fields in a table row. */
    public static final int MAX_FIELDS =  19;

/** Id sequence name for primary key column */
    public static String SEQUENCE_NAME = "REQSEQ";

/**  REQUEST_CARD table "evid" column data object offset in collection
stored by implementing class.  */
    public static final int EVID = 0;

/**  REQUEST_CARD table "auth" column data object offset in collection
stored by implementing class.  */
    public static final int AUTH = 1;

/**  REQUEST_CARD table "subsource" column data object offset in collection
stored by implementing class.  */
    public static final int SUBSOURCE = 2;

/**  REQUEST_CARD table "net" column data object offset in collection stored
by implementing class.  */
    public static final int NET = 3;

/**  REQUEST_CARD table "sta" column data object offset in collection stored
by implementing class.  */
    public static final int STA = 4;

/**  REQUEST_CARD table "seedchan" column data object offset in collection
stored by implementing class.  */
    public static final int SEEDCHAN = 5;

/**  REQUEST_CARD table "staauth" column data object offset in collection
stored by implementing class.  */
    public static final int STAAUTH = 6;

/**  REQUEST_CARD table "channel" column data object offset in collection
stored by implementing class.  */
    public static final int CHANNEL = 7;

/**  REQUEST_CARD table "datetime_on" column data object offset in
collection stored by implementing class.  */
    public static final int DATETIME_ON = 8;

/**  REQUEST_CARD table "datetime_off" column data object offset in
collection stored by implementing class.  */
    public static final int DATETIME_OFF = 9;

/**  REQUEST_CARD table "request_type" column data object offset in
collection stored by implementing class.  */
    public static final int REQUEST_TYPE = 10;

/**  REQUEST_CARD table "lddate" column data object offset in collection
stored by implementing class.  */
    public static final int LDDATE = 11;

/**  REQUEST_CARD table column data object offset in
collection stored by implementing class.  */
    public static final int RCID = 12;

/**  REQUEST_CARD table column data object offset in
collection stored by implementing class.  */
    public static final int LOCATION = 13;

/**  REQUEST_CARD table "retry" column data object offset in collection
stored by implementing class.  */
    public static final int RETRY = 14;

/**  REQUEST_CARD table "lastretry" column data object offset in collection
stored by implementing class.  */
    public static final int LASTRETRY = 15;

/**  REQUEST_CARD table column data object offset in collection
stored by implementing class.  */
    public static final int PRIORITY = 16;

    /** WAVEFORM ID */
    public static final int WFID = 17;

    /** PERCENT STORED */
    public static final int PCSTORED = 18;

/** String of know column names delimited by ",".
*/
    public static final String COLUMN_NAMES =  // for db to UTC conversion -aww 2008/02/12
   "EVID,AUTH,SUBSOURCE,NET,STA,SEEDCHAN,STAAUTH,CHANNEL,truetime.getEpoch(DATETIME_ON,'UTC'),truetime.getEpoch(DATETIME_OFF,'UTC'),REQUEST_TYPE,LDDATE,RCID,LOCATION,RETRY,LASTRETRY,PRIORITY,WFID,PCSTORED";

/** String of table qualified column names delimited by ",".
*/
    public static final String QUALIFIED_COLUMN_NAMES =  // for db to UTC conversion -aww 2008/02/12
    "REQUEST_CARD.EVID,REQUEST_CARD.AUTH,REQUEST_CARD.SUBSOURCE,REQUEST_CARD.NET,REQUEST_CARD.STA,REQUEST_CARD.SEEDCHAN,REQUEST_CARD.STAAUTH,REQUEST_CARD.CHANNEL,truetime.getEpoch(REQUEST_CARD.DATETIME_ON,'UTC'),truetime.getEpoch(REQUEST_CARD.DATETIME_OFF,'UTC'),REQUEST_CARD.REQUEST_TYPE,REQUEST_CARD.LDDATE,REQUEST_CARD.RCID,REQUEST_CARD.LOCATION,REQUEST_CARD.RETRY,REQUEST_CARD.LASTRETRY,REQUEST_CARD.PRIORITY,REQUEST_CARD.WFID,REQUEST_CARD.PCSTORED";

/**  Table column data field names.
*/
    public static final String [] FIELD_NAMES  = {
        "EVID", "AUTH", "SUBSOURCE", "NET", "STA",
        "SEEDCHAN", "STAAUTH", "CHANNEL", "DATETIME_ON", "DATETIME_OFF",
        "REQUEST_TYPE", "LDDATE", "RCID", "LOCATION", "RETRY",
        "LASTRETRY", "PRIORITY","WFID","PCSTORED"
    };

/** Nullable table column field.
*/
    public static final boolean [] FIELD_NULLS = {
        true, false, false, false, false,
        false, false, false, false, false,
        false, true, true, false, true,
        true, true, true, true
    };

/**  Table column data field object class identifiers.
* @see org.trinet.jdbc.datatypes.DataClassIds
* @see org.trinet.jdbc.datatypes.DataClasses
*/
    public static final int [] FIELD_CLASS_IDS = {
        DATALONG, DATASTRING, DATASTRING, DATASTRING, DATASTRING,
        DATASTRING, DATASTRING, DATASTRING, DATADOUBLE, DATADOUBLE,
        DATASTRING, DATADATE, DATALONG, DATASTRING, DATAINT,
        DATADATE, DATAINT, DATALONG, DATADOUBLE
    };

/**  Column indices of primary key table columns.
    * NOTE: these are ARRAY indexes (0-n) and NOT table COLUMN indexes
*/
    //public static final int [] KEY_COLUMNS = {1, 6, 4, 8, 2, 10, 3, 5, 9, 13};
    //public static final int [] KEY_COLUMNS = {1, 2, 3, 4, 5 ,6 ,7 ,8, 9, 10, 13};
    public static final int [] KEY_COLUMNS = {13};  // notice that table is declared with this column as primary key -aww 2008/08/01

/**  Number of decimal fraction digits in table column data fields.
*/
    public static final int [] FIELD_DECIMAL_DIGITS = {
        0, 0, 0, 0, 0, 0, 0, 0, 10, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0
    };

/** Numeric sizes (width) of the table column data fields.
*/
    public static final int [] FIELD_SIZES = {
        15, 15, 8, 8, 8, 8, 15, 8, 25, 25, 1, 7, 15, 2, 38, 7, 3, 15, 3
    };

/** Default table column field values.
*/
    public static final String [] FIELD_DEFAULTS = {
        null, null, null, null, null, null, null, null, null, null,
        null, "(SYS_EXTRACT_UTC(CURRENT_TIMESTAMP))", null, null, null, null, null, null, null
    };
}

