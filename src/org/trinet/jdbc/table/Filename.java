package org.trinet.jdbc.table;
import org.trinet.jdbc.datatypes.*;
import org.trinet.jdbc.*;
import java.sql.Connection;

/** Constructor uses static data members defined by the TableRowFilename interface to initialize the base class with 
* the parameters necessary to describe the schema table named by the interface String parameter DB_TABLE_NAME.
* The class implements several convenience methods to provides class specific static arguments to the argument
* list of the DataTableRow base class methods. Base class methods are used to set or modify the values and states
* of the contained DataObjects. Because the base class uses a JDBC connection class to access the database containing
* the table described by this object, a connection object must be instantiated before using any of the database enabled
* methods of this class.
* Object states refering to data update, nullness, and mutability are inhereited from the DataTableRow base class
* implementation of the DataState interface. These state conditions are used to control the methods access to the
* object and its contained DataObjects, which also implement the DataState interface.
* The default constructor sets the default states to: setUpdate(false), setNull(true), setMutable(true), and 
* setProcessing(NONE).
*/

public class Filename extends DataTableRow implements TableRowFilename {
    public Filename() {
        super(DB_TABLE_NAME, SEQUENCE_NAME, MAX_FIELDS, KEY_COLUMNS, FIELD_NAMES, FIELD_NULLS, FIELD_CLASS_IDS);
    }

/** Copy constructor invokes the default constructor, then clones all the DataObject classes of the input argument.
* The newly instantiated object state values are set to those of the input object.
*/
    public Filename(Filename object) {
        this();
        for (int index = 0; index < MAX_FIELDS; index++) {
            fields.set(index, ((DataObject) object.fields.get(index)).clone());
        }
        this.valueUpdate = object.valueUpdate;
        this.valueNull = object.valueNull;
        this.valueMutable = object.valueMutable;
    }

/** Constructor invokes default constructor, then sets the default Connection object to the handle of the input Connection argument.
* The newly instantiated object state values are those of the default constructor.
*/
    public Filename(Connection conn) {
        this();
        setConnection(conn);
    }

/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
    public Filename(long fileid) {
        this();
        fields.set(FILEID, new DataLong(fileid));
        valueUpdate = true;
        valueNull = false;
    }

/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
    public Filename(long fileid, String dfile) {
        this(fileid);
        fields.set(DFILE, new DataString(dfile));
    }

/** Returns table row count.
*/
    public int getRowCount() {
        return ExecuteSQL.getRowCount(connDB, getTableName());
    }

/** Returns table row count corresponding to the specified input event id.
*/
    public int getRowCountByEventId(long evid) {
        String whereString =
                "WHERE FILEID IN (SELECT FILEID FROM WAVEFORM WHERE WFID IN (SELECT WFID FROM ASSOCWAE WHERE EVID = " 
                + evid + " ))";
        return ExecuteSQL.getRowCount(connDB, getTableName(), "*", whereString);
    }

/** Returns table row count corresponding to the specified input event id and station channel.
*/
    public int getRowCountByEventIdStnChl(long evid, DataStnChl sc) {
        String whereString =
                "WHERE FILEID IN (SELECT FILEID FROM WAVEFORM WHERE WFID IN (SELECT WFID FROM ASSOCWAE WHERE EVID = " + evid + " )"
                + "  AND WFID IN (SELECT WFID FROM WAVEFORM WHERE " + sc.toStringSQLWhereCondition() + " ))";
        return ExecuteSQL.getRowCount(connDB, getTableName(), "*", whereString);
    }

/** Returns table row count corresponding to the specified input origin id.
*/
    public int getRowCountByOriginId(long orid) {
        String whereString =
                "WHERE FILEID IN (SELECT FILEID FROM WAVEFORM WHERE WFID IN (SELECT WFID FROM ASSOCWAE WHERE EVID IN " +
        " ( SELECT EVID FROM ORIGIN WHERE ORID = " + orid + " )))";
        return ExecuteSQL.getRowCount(connDB, getTableName(), "*", whereString);
    }

/** Returns table row count corresponding to the specified input origin id and station channel.
*/
    public int getRowCountByOriginIdStnChl(long orid, DataStnChl sc) {
        String whereString =
                "WHERE FILEID IN (SELECT FILEID FROM WAVEFORM WHERE WFID IN (SELECT WFID FROM ASSOCWAE WHERE EVID IN " +
        " ( SELECT EVID FROM ORIGIN WHERE ORID = " + orid + " ))" +
                "  AND WFID IN (SELECT WFID FROM WAVEFORM WHERE " + sc.toStringSQLWhereCondition() + " ))";
        return ExecuteSQL.getRowCount(connDB, getTableName(), "*", whereString);
    }

/** Returns an array where each element contains the data from a single table row parsed from an SQL query
* WHERE the DATETIME column is BETWEEN the specified input times and the SUBSOURCE column has the specified input value.
* A return value of null indicates no data or an error condition.
*/
// Perhaps needs to qualify AUTH too?
    public Filename [] getRowsByDateTimeRange(double tStart, double tEnd, String subsource) {
        StringBuffer sb = new StringBuffer(132);
        /* // replace for leap secs -aww 2008/01/24
        sb.append("WHERE ( (DATETIME_ON BETWEEN ");
        sb.append(StringSQL.valueOf(tStart) );
        sb.append(" AND ");
        sb.append(StringSQL.valueOf(tEnd));
        sb.append(") OR (DATETIME_OFF BETWEEN ");
        sb.append(StringSQL.valueOf(tStart));
        sb.append(" AND ");
        sb.append(StringSQL.valueOf(tEnd));
        sb.append(") )");
        */
        // below added for leap secs -aww 2008/01/24
        sb.append("WHERE ( (DATETIME_ON BETWEEN TRUETIME.putEpoch(");
        sb.append(StringSQL.valueOf(tStart) );
        sb.append(", 'UTC') AND TRUETIME.putEpoch(");
        sb.append(StringSQL.valueOf(tEnd));
        sb.append(", 'UTC)) OR (DATETIME_OFF BETWEEN TRUETIME.putEpoch(");
        sb.append(StringSQL.valueOf(tStart));
        sb.append(", 'UTC') AND TRUETIME.putEpoch(");
        sb.append(StringSQL.valueOf(tEnd));
        sb.append(", 'UTC)) )");
        if (! NullValueDb.isEmpty(subsource)) {
            sb.append(" AND SUBSOURCE = ");
            sb.append(StringSQL.valueOf(subsource)); 
        }
        return (Filename []) getRowsEquals(sb.toString());
    }

/** Returns an array where each element contains the data from a single table row parsed from an SQL query
* for rows associated with the specified event id (evid). 
* A return value of null indicates no data or an error condition.
*/
    public Filename [] getRowsByEventId(long evid) {
        String whereString =
                "WHERE FILEID IN (SELECT FILEID FROM WAVEFORM WHERE WFID IN (SELECT WFID FROM ASSOCWAE WHERE EVID = " 
                + evid + " ))";
        return (Filename []) getRowsEquals(whereString);
    }
    
/** Returns an array where each element contains the data from a single table row parsed from an SQL query
* for rows associated with the specified event id (evid) and station channel data inputs. 
* A return value of null indicates no data or an error condition.
*/
    public Filename [] getRowsByEventIdStnChl(long evid, DataStnChl sc) {
        String whereString =
                "WHERE FILEID IN (SELECT FILEID FROM WAVEFORM WHERE WFID IN (SELECT WFID FROM ASSOCWAE WHERE EVID = " + evid + " )"
                + "  AND WFID IN (SELECT WFID FROM WAVEFORM WHERE " + sc.toStringSQLWhereCondition() + " ))";
        return (Filename []) getRowsEquals(whereString);
    }

/** Returns an array where each element contains the data from a single table row parsed from an SQL query
* for rows associated with the specified origin id (orid). 
* Method uses the JDBC Connection object assigned with setConnection().
* Returns null if no rows satisfy query or an error condition occurs.
*/
    public Filename [] getRowsByOriginId(long orid) {
        String whereString =
                "WHERE FILEID IN (SELECT FILEID FROM WAVEFORM WHERE WFID IN (SELECT WFID FROM ASSOCWAE WHERE EVID IN " +
        " ( SELECT EVID FROM ORIGIN WHERE ORID = " + orid + " )))";
        return (Filename []) getRowsEquals(whereString);
    }

/** Returns an array where each element contains the data from a single table row parsed from an SQL query
* for rows associated with the specified origin id (orid) and station channel data inputs. 
* Method uses the JDBC Connection object assigned with setConnection().
* Returns null if no rows satisfy query or an error condition occurs.
*/
    public Filename [] getRowsByOriginIdStnChl(long orid, DataStnChl sc) {
        String whereString =
                "WHERE FILEID IN (SELECT FILEID FROM WAVEFORM WHERE WFID IN (SELECT WFID FROM ASSOCWAE WHERE EVID IN " +
        " ( SELECT EVID FROM ORIGIN WHERE ORID = " + orid + " ))" +
                "  AND WFID IN (SELECT WFID FROM WAVEFORM WHERE " + sc.toStringSQLWhereCondition() + " ))";
        return (Filename []) getRowsEquals(whereString);
    }

/*
* Deletes rows from the database table defined by tableName associated with the specified event id (evid).
* Returns number of rows deleted for specified id. A return value of -1 indicates an error condition.
    public int deleteRowsByEvent(long evid) {
        String whereCondition = " WHERE EVID = " + evid;
        return ExecuteSQL.deleteRowsWhere(connDB, getTableName(), whereCondition);
    }

/** Deletes rows associated with the specified origin id (orid).
* Returns number of rows deleted for specified id. A return value of -1 indicates an error condition.
    public int deleteRowsByOrigin(long orid) {
        return ExecuteSQL.deleteRowsEquals(connDB, getTableName(), "ORID", orid);
    }
*/

}
