package org.trinet.jdbc.table;
import org.trinet.jdbc.datatypes.DataClassIds;
/** Interface of static data constants defining the named table.
* @see AssocCoM
*/
public interface TableRowAssocCoM extends DataClassIds {

/** Name of schema database table represented by this class.
*/
    public static final String DB_TABLE_NAME =  "ASSOCCOM";

/** Number of column data fields in a table row.
*/
    public static final int MAX_FIELDS =  12;

/** Id sequence name for primary key column
*/
    public static String SEQUENCE_NAME = "";


/**  ASSOCCOM table "magid" column data object offset in collection stored by implementing class.
*/
    public static final int MAGID = 0;

/**  ASSOCCOM table "coid" column data object offset in collection stored by implementing class.
*/
    public static final int COID = 1;

/**  ASSOCCOM table "commid" column data object offset in collection stored by implementing class.
*/
    public static final int COMMID = 2;

/**  ASSOCCOM table "auth" column data object offset in collection stored by implementing class.
*/
    public static final int AUTH = 3;

/**  ASSOCCOM table "subsource" column data object offset in collection stored by implementing class.
*/
    public static final int SUBSOURCE = 4;

/**  ASSOCCOM table "weight" column data object offset in collection stored by implementing class.
*/
    public static final int WEIGHT = 5;

/**  ASSOCCOM table "in_wgt" column data object offset in collection stored by implementing class.
*/
    public static final int IN_WGT = 6;

/**  ASSOCCOM table "rflag" column data object offset in collection stored by implementing class.
*/
    public static final int RFLAG = 7;

/**  ASSOCCOM table "lddate" column data object offset in collection stored by implementing class.
*/
    public static final int LDDATE = 8;

/**  ASSOCCOM table "mag" column data object offset in collection stored by implementing class.
*/
    public static final int MAG = 9;

/**  ASSOCCOM table "magres" column data object offset in collection stored by implementing class.
*/
    public static final int MAGRES = 10;

/**  ASSOCCOM table "magcorr" column data object offset in collection stored by implementing class.
*/
    public static final int MAGCORR = 11;
/** String of know column names delimited by ",".
*/
    public static final String COLUMN_NAMES =
   "MAGID,COID,COMMID,AUTH,SUBSOURCE,WEIGHT,IN_WGT,RFLAG,LDDATE,MAG,MAGRES,MAGCORR";

/** String of table qualified column names delimited by ",".
*/
    public static final String QUALIFIED_COLUMN_NAMES = 
    "ASSOCCOM.MAGID,ASSOCCOM.COID,ASSOCCOM.COMMID,ASSOCCOM.AUTH,ASSOCCOM.SUBSOURCE,ASSOCCOM.WEIGHT,ASSOCCOM.IN_WGT,ASSOCCOM.RFLAG,ASSOCCOM.LDDATE,ASSOCCOM.MAG,ASSOCCOM.MAGRES,ASSOCCOM.MAGCORR";

/**  Table column data field names.
*/
    public static final String [] FIELD_NAMES  = {
	"MAGID", "COID", "COMMID", "AUTH", "SUBSOURCE", 
	"WEIGHT", "IN_WGT", "RFLAG", "LDDATE", "MAG", 
	"MAGRES", "MAGCORR"
    };

/** Nullable table column field.
*/
    public static final boolean [] FIELD_NULLS = {
	false, false, true, false, true, 
	true, true, true, true, true, 
	true, true
    };

/**  Table column data field object class identifiers.
* @see org.trinet.jdbc.datatypes.DataClassIds
* @see org.trinet.jdbc.datatypes.DataClasses
*/
    public static final int [] FIELD_CLASS_IDS = {
	DATALONG, DATALONG, DATALONG, DATASTRING, DATASTRING, 
	DATADOUBLE, DATADOUBLE, DATASTRING, DATADATE, DATADOUBLE, 
	DATADOUBLE, DATADOUBLE
    };

/**  Column indices of primary key table columns.
*/
    public static final int [] KEY_COLUMNS = {0, 1};

/**  Number of decimal fraction digits in table column data fields.
*/
    public static final int [] FIELD_DECIMAL_DIGITS = {
	0, 0, 0, 0, 0, 3, 3, 0, 0, 2, 2, 2
    };

/** Numeric sizes (width) of the table column data fields.
*/
    public static final int [] FIELD_SIZES = {
	15, 15, 15, 15, 8, 4, 4, 2, 7, 5, 5, 5
    };

/** Default table column field values.
*/
    public static final String [] FIELD_DEFAULTS = {
	null, null, null, null, null, null, null, null, "(SYS_EXTRACT_UTC(CURRENT_TIMESTAMP))", null, 
	null, null
    };
}
