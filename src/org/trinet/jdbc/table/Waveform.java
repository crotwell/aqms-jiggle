package org.trinet.jdbc.table;
import org.trinet.jdbc.datatypes.*;
import org.trinet.jdbc.*;
import java.sql.Connection;
import java.util.List;

/** Constructor uses static data members defined by the TableRowWaveform interface to initialize the base class with 
* the parameters necessary to describe the schema table named by the interface String parameter DB_TABLE_NAME.
* The class implements several convenience methods to provides class specific static arguments to the argument
* list of the DataTableRow base class methods. Base class methods are used to set or modify the values and states
* of the contained DataObjects. Because the base class uses a JDBC connection class to access the database containing
* the table described by this object, a connection object must be instantiated before using any of the database enabled
* methods of this class.
* Object states refering to data update, nullness, and mutability are inhereited from the DataTableRow base class
* implementation of the DataState interface. These state conditions are used to control the methods access to the
* object and its contained DataObjects, which also implement the DataState interface.
* The default constructor sets the default states to: setUpdate(false), setNull(true), setMutable(true), and 
* setProcessing(NONE).
*/

public class Waveform extends DataTableRow implements TableRowWaveform {
    public Waveform() {
        super(DB_TABLE_NAME, SEQUENCE_NAME, MAX_FIELDS, KEY_COLUMNS, FIELD_NAMES, FIELD_NULLS, FIELD_CLASS_IDS);
    }

/** Copy constructor invokes the default constructor, then clones all the DataObject classes of the input argument.
* The newly instantiated object state values are set to those of the input object.
*/
    public Waveform(Waveform object) {
        this();
        for (int index = 0; index < MAX_FIELDS; index++) {
            fields.set(index, ((DataObject) object.fields.get(index)).clone());
        }
        this.valueUpdate = object.valueUpdate;
        this.valueNull = object.valueNull;
        this.valueMutable = object.valueMutable;
    }

/** Constructor invokes default constructor, then sets the default Connection object to the handle of the input Connection argument.
* The newly instantiated object state values are those of the default constructor.
*/
    public Waveform(Connection conn) {
        this();
        setConnection(conn);
    }

/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
    public Waveform(long wfid) {
        this();
        fields.set(WFID, new DataLong(wfid));
        valueUpdate = true;
        valueNull = false;
    }

/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
    public Waveform(long wfid, DataStnChl sc) {
        this(wfid);
        setDataStnChl(sc);        
    }

/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
    public Waveform(long wfid, DataStnChl sc, String archive, long fileid) {
        this(wfid);
        setDataStnChl(sc);        
        fields.set(ARCHIVE, new DataString(archive));
        fields.set(FILEID, new DataLong(fileid));
    }

/** Returns a DataStnChl object derived from the station channel data stored in this object instance, else returns null.
*/
    public DataStnChl getDataStnChl() {
        return new DataStnChl(
                (DataString) fields.get(findFieldIndex("STA")),
                (DataString) fields.get(findFieldIndex("NET")),
                (DataString) fields.get(findFieldIndex("AUTH")),
                (DataString) fields.get(findFieldIndex("SUBSOURCE")),
                (DataString) fields.get(findFieldIndex("CHANNEL")),
                (DataString) fields.get(findFieldIndex("CHANNELSRC")),
                (DataString) fields.get(findFieldIndex("SEEDCHAN")),
                (DataString) fields.get(findFieldIndex("LOCATION")));
    }
/** Sets station channel data for this object instance to the DataObjects members of the input DataStnChl object;
* a null input results in a no-op.
*/
    public void setDataStnChl(DataStnChl obj) {
        if (obj == null) return;
        fields.set(findFieldIndex("STA"), ((DataString) obj.getDataObject(DataStnChl.STA)).clone());
        fields.set(findFieldIndex("NET"), ((DataString) obj.getDataObject(DataStnChl.NET)).clone());
        fields.set(findFieldIndex("AUTH"), ((DataString) obj.getDataObject(DataStnChl.AUTH)).clone());
        fields.set(findFieldIndex("SUBSOURCE"), ((DataString) obj.getDataObject(DataStnChl.SUBSOURCE)).clone());
        fields.set(findFieldIndex("CHANNEL"), ((DataString) obj.getDataObject(DataStnChl.CHANNEL)).clone());
        fields.set(findFieldIndex("CHANNELSRC"), ((DataString) obj.getDataObject(DataStnChl.CHANNELSRC)).clone());
        fields.set(findFieldIndex("SEEDCHAN"), ((DataString) obj.getDataObject(DataStnChl.SEEDCHAN)).clone());
        fields.set(findFieldIndex("LOCATION"), ((DataString) obj.getDataObject(DataStnChl.LOCATION)).clone());
    }
/** Returns List object containing station channel DataObject fields for this object instance.
*/
    public List getDataStnChlList() {
        return fields.subList(findFieldIndex("STA"), findFieldIndex("LOCATION"));
    }
/** Sets station data fields in this object instance to the values in input List; a null input results in a no-op.
*/
    public void setDataStnChlList(List list) {
        if (list == null) return;
        fields.addAll(findFieldIndex("STA"), list);
    }

/** Returns table row count.
*/
    public int getRowCount() {
        return ExecuteSQL.getRowCount(connDB, getTableName());
    }

/** Returns table row count corresponding to the specified input event id.
*/
    public int getRowCountByEventId(long evid) {
        String whereString = "WHERE WFID IN (SELECT WFID FROM ASSOCWAE WHERE EVID = " + evid + " )" ;
        return ExecuteSQL.getRowCount(connDB, getTableName(), "*", whereString);
    }

/** Returns table row count corresponding to the specified input origin id.
*/
    public int getRowCountByOriginId(long orid) {
        String whereString =
            "WHERE WFID IN ( SELECT WFID FROM ASSOCWAE WHERE EVID IN ( SELECT EVID FROM ORIGIN WHERE ORID = " + orid + " ))";
        return ExecuteSQL.getRowCount(connDB, getTableName(), "*", whereString);
    }

/** Returns table row count corresponding to the specified input event id and station channel.
*/
    public int getRowCountByEventIdStnChl(long evid, DataStnChl sc) {
       String whereString = "WHERE WFID IN ( SELECT WFID FROM ASSOCWAE WHERE EVID = " + evid + " ) AND " +
                         sc.toStringSQLWhereCondition();
        return ExecuteSQL.getRowCount(connDB, getTableName(), "*", whereString);
    }

/** Returns table row count corresponding to the specified input origin id and station channel.
*/
    public int getRowCountByOriginIdStnChl(long orid, DataStnChl sc) {
        String whereString =
                "WHERE WFID IN ( SELECT WFID FROM ASSOCWAE WHERE EVID IN ( SELECT EVID FROM ORIGIN WHERE ORID = " + orid + " ))"
                + " AND " + sc.toStringSQLWhereCondition();
        return ExecuteSQL.getRowCount(connDB, getTableName(), "*", whereString);
    }

/** Returns an array where each element contains the data from a single table row parsed from an SQL query ResultSet.
* WHERE the DATETIME column is BETWEEN the specified input times and the SUBSOURCE column has the specified input value.
* A return value of null indicates no data or an error condition.
*/
// Perhaps needs to qualify AUTH too?
    public Waveform [] getRowsByDateTimeRange(double tStart, double tEnd, String subsource) {
        //String whereString = "WHERE ( DATETIME_ON BETWEEN " + StringSQL.valueOf(tStart) + " AND " + StringSQL.valueOf(tEnd) + " ) "
        //                     + " OR DATETIME_OFF BETWEEN " + StringSQL.valueOf(tStart) + " AND " + StringSQL.valueOf(tEnd) + " ))";
        // replaced above with below for leap secs -aww 2008/01/24
        String whereString = "WHERE ( (DATETIME_ON BETWEEN TRUETIME.putEpoch(" + StringSQL.valueOf(tStart) + 
            ", 'UTC') AND TRUETIME.putEpoch(" + StringSQL.valueOf(tEnd) + ", 'UTC')) OR (DATETIME_OFF BETWEEN TRUETIME.putEpoch(" +
            StringSQL.valueOf(tStart) + ", 'UTC) AND TRUETIME.putEpoch(" + StringSQL.valueOf(tEnd) + ", 'UTC')) )";
        if (! NullValueDb.isEmpty(subsource))
            whereString = whereString + " AND SUBSOURCE = " + StringSQL.valueOf(subsource); 
        return (Waveform []) getRowsEquals(whereString);
    }

/** Returns an Waveform object containing the data from a single table row parsed from an SQL query ResultSet.
* Returns the row matching the specified waveform id (wfid). 
* A return value of null indicates no data or an error condition.
*/
    public Waveform getRowByWaveformId(long wfid) {
        Waveform [] wf = (Waveform []) getRowsEquals("WFID", wfid);
        if (wf == null) return null;
        else return wf[0];
    }
    
/** Returns an array where each element contains the data from a single table row parsed from an SQL query ResultSet.
* Returns all rows associated with the specified event id (evid). 
* A return value of null indicates no data or an error condition.
*/
    public Waveform [] getRowsByEventId(long evid) {
        String whereString = "WHERE WFID IN ( SELECT WFID FROM ASSOCWAE WHERE EVID = " + evid + " )";
        return (Waveform []) getRowsEquals(whereString);
    }
    
/** Returns an array where each element contains the data from a single table row parsed from an SQL query ResultSet.
* Returns all rows associated with the specified event id (evid) and station channel data inputs. 
* A return value of null indicates no data or an error condition.
*/
    public Waveform [] getRowsByEventIdStnChl(long evid, DataStnChl sc) {
       String whereString = "WHERE WFID IN ( SELECT WFID FROM ASSOCWAE WHERE EVID = " + evid + " ) AND " +
                         sc.toStringSQLWhereCondition();
        return (Waveform []) getRowsEquals(whereString);
    }

/** Returns an array where each element contains the data from a single table row parsed from an SQL query ResultSet.
* Returns all rows associated with the specified origin id (orid). 
* Method uses the JDBC Connection object assigned with setConnection().
* Returns null if no rows satisfy query or an error condition occurs.
*/
    public Waveform [] getRowsByOriginId(long orid) {
        String whereString =
                "WHERE WFID IN ( SELECT WFID FROM ASSOCWAE WHERE EVID IN ( SELECT EVID FROM ORIGIN WHERE ORID = " + orid + " ))";
        return (Waveform []) getRowsEquals(whereString);
    }

/** Returns an array where each element contains the data from a single table row parsed from an SQL query ResultSet.
* Returns all rows associated with the specified origin id (orid) and station channel data inputs. 
* Method uses the JDBC Connection object assigned with setConnection().
* Returns null if no rows satisfy query or an error condition occurs.
*/
    public Waveform [] getRowsByOriginIdStnChl(long orid, DataStnChl sc) {
        String whereString =
                "WHERE WFID IN ( SELECT WFID FROM ASSOCWAE WHERE EVID IN ( SELECT EVID FROM ORIGIN WHERE ORID = " + orid + " ))"
                + " AND " + sc.toStringSQLWhereCondition();
        return (Waveform []) getRowsEquals(whereString);
    }

}
