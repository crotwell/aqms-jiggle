package org.trinet.jdbc.table;
import org.trinet.jdbc.datatypes.DataClassIds;
/** Interface of static data constants defining the named table.
* @see Origin
*/
public interface TableRowOrigin extends DataClassIds {

/** Name of schema database table represented by this class.
*/
    public static final String DB_TABLE_NAME =  "ORIGIN";

/** Number of column data fields in a table row.
*/
    public static final int MAX_FIELDS =  43;  // added 4 cols 2015/03/18 -aww

/** Id sequence name for primary key column
*/
    public static String SEQUENCE_NAME = "ORSEQ";

/**  ORIGIN table "orid" column data object offset in collection stored by implementing class.
*/
    public static final int ORID = 0;

/**  ORIGIN table "evid" column data object offset in collection stored by implementing class.
*/
    public static final int EVID = 1;

/**  ORIGIN table "prefmag" column data object offset in collection stored by implementing class.
*/
    public static final int PREFMAG = 2;

/**  ORIGIN table "prefmec" column data object offset in collection stored by implementing class.
*/
    public static final int PREFMEC = 3;

/**  ORIGIN table "commid" column data object offset in collection stored by implementing class.
*/
    public static final int COMMID = 4;

/**  ORIGIN table "bogusflag" column data object offset in collection stored by implementing class.
*/
    public static final int BOGUSFLAG = 5;

/**  ORIGIN table "datetime" column data object offset in collection stored by implementing class.
*/
    public static final int DATETIME = 6;

/**  ORIGIN table "lat" column data object offset in collection stored by implementing class.
*/
    public static final int LAT = 7;

/**  ORIGIN table "lon" column data object offset in collection stored by implementing class.
*/
    public static final int LON = 8;

/**  ORIGIN table "depth" column data object offset in collection stored by implementing class.
*/
    public static final int DEPTH = 9;

/**  ORIGIN table "type" column data object offset in collection stored by implementing class.
*/
    public static final int TYPE = 10;

/**  ORIGIN table "algorithm" column data object offset in collection stored by implementing class.
*/
    public static final int ALGORITHM = 11;

/**  ORIGIN table "algo_assoc" column data object offset in collection stored by implementing class.
*/
    public static final int ALGO_ASSOC = 12;

/**  ORIGIN table "auth" column data object offset in collection stored by implementing class.
*/
    public static final int AUTH = 13;

/**  ORIGIN table "subsource" column data object offset in collection stored by implementing class.
*/
    public static final int SUBSOURCE = 14;

/**  ORIGIN table "datumhor" column data object offset in collection stored by implementing class.
*/
    public static final int DATUMHOR = 15;

/**  ORIGIN table "datumver" column data object offset in collection stored by implementing class.
*/
    public static final int DATUMVER = 16;

/**  ORIGIN table "gap" column data object offset in collection stored by implementing class.
*/
    public static final int GAP = 17;

/**  ORIGIN table "distance" column data object offset in collection stored by implementing class.
*/
    public static final int DISTANCE = 18;

/**  ORIGIN table "wrms" column data object offset in collection stored by implementing class.
*/
    public static final int WRMS = 19;

/**  ORIGIN table "stime" column data object offset in collection stored by implementing class.
*/
    public static final int STIME = 20;

/**  ORIGIN table "erhor" column data object offset in collection stored by implementing class.
*/
    public static final int ERHOR = 21;

/**  ORIGIN table "sdep" column data object offset in collection stored by implementing class.
*/
    public static final int SDEP = 22;

/**  ORIGIN table "erlat" column data object offset in collection stored by implementing class.
*/
    public static final int ERLAT = 23;

/**  ORIGIN table "erlon" column data object offset in collection stored by implementing class.
*/
    public static final int ERLON = 24;

/**  ORIGIN table "totalarr" column data object offset in collection stored by implementing class.
*/
    public static final int TOTALARR = 25;

/**  ORIGIN table "totalamp" column data object offset in collection stored by implementing class.
*/
    public static final int TOTALAMP = 26;

/**  ORIGIN table "ndef" column data object offset in collection stored by implementing class.
*/
    public static final int NDEF = 27;

/**  ORIGIN table "nbs" column data object offset in collection stored by implementing class.
*/
    public static final int NBS = 28;

/**  ORIGIN table "nbfm" column data object offset in collection stored by implementing class.
*/
    public static final int NBFM = 29;

/**  ORIGIN table "locevid" column data object offset in collection stored by implementing class.
*/
    public static final int LOCEVID = 30;

/**  ORIGIN table "quality" column data object offset in collection stored by implementing class.
*/
    public static final int QUALITY = 31;

/**  ORIGIN table "fdepth" column data object offset in collection stored by implementing class.
*/
    public static final int FDEPTH = 32;

/**  ORIGIN table "fepi" column data object offset in collection stored by implementing class.
*/
    public static final int FEPI = 33;

/**  ORIGIN table "ftime" column data object offset in collection stored by implementing class.
*/
    public static final int FTIME = 34;

/**  ORIGIN table "vmodelid" column data object offset in collection stored by implementing class.
*/
    public static final int VMODELID = 35; // datatype changed from int to 2 char

/**  ORIGIN table "cmodelid" column data object offset in collection stored by implementing class.
*/
    public static final int CMODELID = 36;// datatype changed from int to 2 char

/**  ORIGIN table "lddate" column data object offset in collection stored by implementing class.
*/
    public static final int LDDATE = 37;

/**  ORIGIN table "rflag" column data object offset in collection stored by implementing class.
*/
    public static final int RFLAG = 38;

/** Origin table " mdepth" **/
    public static final int MDEPTH  = 39; // model depth (depending on model used, could be same as geoid)

/** Origin table " CRUST_TYPE" **/
    public static final int CRUST_TYPE  = 40; //'H','T','E','L','V','U' 

/** Origin table  "CRUST_MODEL" name acronym for crust velocity delay model.**/
    public static final int CRUST_MODEL  = 41; // ? is the solution solve date going here -aww 11/08/2006 ?

/** Origin table  "GTYPE" name acronym for geographic type.**/
    public static final int GTYPE  = 42; // ? is the solution solve date going here -aww 11/08/2006 ?

/** String of know column names delimited by ",".
*/
    public static final String COLUMN_NAMES = // for db to UTC conversion -aww 2008/02/12
   "ORID,EVID,PREFMAG,PREFMEC,COMMID,BOGUSFLAG,truetime.getEpoch(DATETIME,'UTC'),LAT,LON,DEPTH,TYPE,ALGORITHM,ALGO_ASSOC,AUTH,SUBSOURCE,DATUMHOR,DATUMVER,GAP,DISTANCE,WRMS,STIME,ERHOR,SDEP,ERLAT,ERLON,TOTALARR,TOTALAMP,NDEF,NBS,NBFM,LOCEVID,QUALITY,FDEPTH,FEPI,FTIME,VMODELID,CMODELID,LDDATE,RFLAG,MDEPTH,CRUST_TYPE,CRUST_MODEL,GTYPE";

/** String of table qualified column names delimited by ",".
*/
    public static final String QUALIFIED_COLUMN_NAMES =  // for db to UTC conversion -aww 2008/02/12
    "ORIGIN.ORID,ORIGIN.EVID,ORIGIN.PREFMAG,ORIGIN.PREFMEC,ORIGIN.COMMID,ORIGIN.BOGUSFLAG,truetime.getEpoch(ORIGIN.DATETIME,'UTC'),ORIGIN.LAT,ORIGIN.LON,ORIGIN.DEPTH,ORIGIN.TYPE,ORIGIN.ALGORITHM,ORIGIN.ALGO_ASSOC,ORIGIN.AUTH,ORIGIN.SUBSOURCE,ORIGIN.DATUMHOR,ORIGIN.DATUMVER,ORIGIN.GAP,ORIGIN.DISTANCE,ORIGIN.WRMS,ORIGIN.STIME,ORIGIN.ERHOR,ORIGIN.SDEP,ORIGIN.ERLAT,ORIGIN.ERLON,ORIGIN.TOTALARR,ORIGIN.TOTALAMP,ORIGIN.NDEF,ORIGIN.NBS,ORIGIN.NBFM,ORIGIN.LOCEVID,ORIGIN.QUALITY,ORIGIN.FDEPTH,ORIGIN.FEPI,ORIGIN.FTIME,ORIGIN.VMODELID,ORIGIN.CMODELID,ORIGIN.LDDATE,ORIGIN.RFLAG,ORIGIN.MDEPTH,ORIGIN.CRUST_TYPE,ORIGIN.CRUST_MODEL,GTYPE";

/**  Table column data field names.
*/
    public static final String [] FIELD_NAMES  = {
        "ORID", "EVID", "PREFMAG", "PREFMEC", "COMMID", 
        "BOGUSFLAG", "DATETIME", "LAT", "LON", "DEPTH", 
        "TYPE", "ALGORITHM", "ALGO_ASSOC", "AUTH", "SUBSOURCE", 
        "DATUMHOR", "DATUMVER", "GAP", "DISTANCE", "WRMS", 
        "STIME", "ERHOR", "SDEP", "ERLAT", "ERLON", 
        "TOTALARR", "TOTALAMP", "NDEF", "NBS", "NBFM", 
        "LOCEVID", "QUALITY", "FDEPTH", "FEPI", "FTIME", 
        "VMODELID", "CMODELID", "LDDATE", "RFLAG","MDEPTH",
        "CRUST_TYPE", "CRUST_MODEL", "GTYPE"
    };

/** Nullable table column field.
*/
    public static final boolean [] FIELD_NULLS = {
        false, false, true, true, true, 
        true, false, false, false, true, 
        true, true, true, false, true, 
        true, true, true, true, true, 
        true, true, true, true, true, 
        true, true, true, true, true, 
        true, true, true, true, true, 
        true, true, true, true, true,
        true, true, true
    };

/**  Table column data field object class identifiers.
* @see org.trinet.jdbc.datatypes.DataClassIds
* @see org.trinet.jdbc.datatypes.DataClasses
*/
    public static final int [] FIELD_CLASS_IDS = {
        DATALONG, DATALONG, DATALONG, DATALONG, DATALONG, 
        DATALONG, DATADOUBLE, DATADOUBLE, DATADOUBLE, DATADOUBLE, 
        DATASTRING, DATASTRING, DATASTRING, DATASTRING, DATASTRING, 
        DATASTRING, DATASTRING, DATADOUBLE, DATADOUBLE, DATADOUBLE, 
        DATADOUBLE, DATADOUBLE, DATADOUBLE, DATADOUBLE, DATADOUBLE, 
        DATALONG, DATALONG, DATALONG, DATALONG, DATALONG, 
        DATASTRING, DATADOUBLE, DATASTRING, DATASTRING, DATASTRING, 
        DATASTRING, DATASTRING, DATADATE, DATASTRING, DATADOUBLE,
        DATASTRING, DATASTRING, DATASTRING
    };

/**  Column indices of primary key table columns.
*/
    public static final int [] KEY_COLUMNS = {0};

/**  Number of decimal fraction digits in table column data fields.
*/
    public static final int [] FIELD_DECIMAL_DIGITS = {
        0, 0, 0, 0, 0, 0, 10, 7, 7, 3, 0, 0, 0, 0, 0, 0, 0, 1, 3, 3, 
        3, 3, 3, 3, 3, 0,  0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 3,
        0, 0, 0
    };

/** Numeric sizes (width) of the table column data fields.
*/
    public static final int [] FIELD_SIZES = {
        15, 15, 15, 15, 15, 1, 25, 9, 10, 7,
        2, 15, 80, 15, 8, 8, 8, 4, 7, 5, 
        6, 7, 7, 7, 7, 5, 6, 5, 4, 4,
        12, 2, 1, 1, 1, 2, 2, 7, 2, 7,
        1, 3, 2
    };

/** Default table column field values.
*/
    public static final String [] FIELD_DEFAULTS = {
        null, null, null, null, null, null, null, null, null, null, 
        null, null, null, null, null, null, null, null, null, null, 
        null, null, null, null, null, null, null, null, null, null, 
        null, null, null, null, null, null, null, "(SYS_EXTRACT_UTC(CURRENT_TIMESTAMP))", null, null,
        null, null, null
    };
}
