package org.trinet.jdbc.table;
import org.trinet.jdbc.datatypes.DataClassIds;
/** Interface of static data constants defining the named table.
* @see StationData
*/
public interface TableRowStationData extends DataClassIds {

/** Name of schema database table represented by this class.
*/
    public static final String DB_TABLE_NAME =  "STATION_DATA";

/** Number of column data fields in a table row.
*/
    public static final int MAX_FIELDS =  12;

/** Id sequence name for primary key column
*/
    public static String SEQUENCE_NAME = "";


/**  STATION_DATA table "net" column data object offset in collection stored by implementing class.
*/
    public static final int NET = 0;

/**  STATION_DATA table "sta" column data object offset in collection stored by implementing class.
*/
    public static final int STA = 1;

/**  STATION_DATA table "lat" column data object offset in collection stored by implementing class.
*/
    public static final int LAT = 2;

/**  STATION_DATA table "lon" column data object offset in collection stored by implementing class.
*/
    public static final int LON = 3;

/**  STATION_DATA table "elev" column data object offset in collection stored by implementing class.
*/
    public static final int ELEV = 4;

/**  STATION_DATA table "staname" column data object offset in collection stored by implementing class.
*/
    public static final int STANAME = 5;

/**  STATION_DATA table "net_id" column data object offset in collection stored by implementing class.
*/
    public static final int NET_ID = 6;

/**  STATION_DATA table "word_32" column data object offset in collection stored by implementing class.
*/
    public static final int WORD_32 = 7;

/**  STATION_DATA table "word_16" column data object offset in collection stored by implementing class.
*/
    public static final int WORD_16 = 8;

/**  STATION_DATA table "ondate" column data object offset in collection stored by implementing class.
*/
    public static final int ONDATE = 9;

/**  STATION_DATA table "offdate" column data object offset in collection stored by implementing class.
*/
    public static final int OFFDATE = 10;

/**  STATION_DATA table "lddate" column data object offset in collection stored by implementing class.
*/
    public static final int LDDATE = 11;
/** String of know column names delimited by ",".
*/
    public static final String COLUMN_NAMES =
   "NET,STA,LAT,LON,ELEV,STANAME,NET_ID,WORD_32,WORD_16,ONDATE,OFFDATE,LDDATE";

/** String of table qualified column names delimited by ",".
*/
    public static final String QUALIFIED_COLUMN_NAMES = 
    "STATION_DATA.NET,STATION_DATA.STA,STATION_DATA.LAT,STATION_DATA.LON,STATION_DATA.ELEV,STATION_DATA.STANAME,STATION_DATA.NET_ID,STATION_DATA.WORD_32,STATION_DATA.WORD_16,STATION_DATA.ONDATE,STATION_DATA.OFFDATE,STATION_DATA.LDDATE";

/**  Table column data field names.
*/
    public static final String [] FIELD_NAMES  = {
	"NET", "STA", "LAT", "LON", "ELEV", 
	"STANAME", "NET_ID", "WORD_32", "WORD_16", "ONDATE", 
	"OFFDATE", "LDDATE"
    };

/** Nullable table column field.
*/
    public static final boolean [] FIELD_NULLS = {
	false, false, true, true, true, 
	true, true, false, false, false, 
	true, true
    };

/**  Table column data field object class identifiers.
* @see org.trinet.jdbc.datatypes.DataClassIds
* @see org.trinet.jdbc.datatypes.DataClasses
*/
    public static final int [] FIELD_CLASS_IDS = {
	DATASTRING, DATASTRING, DATADOUBLE, DATADOUBLE, DATADOUBLE, 
	DATASTRING, DATALONG, DATALONG, DATALONG, DATADATE, 
	DATADATE, DATADATE
    };

/**  Column indices of primary key table columns.
*/
    public static final int [] KEY_COLUMNS = {0, 1, 9};

/**  Number of decimal fraction digits in table column data fields.
*/
    public static final int [] FIELD_DECIMAL_DIGITS = {
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    };

/** Numeric sizes (width) of the table column data fields.
*/
    public static final int [] FIELD_SIZES = {
	8, 6, 126, 126, 126, 50, 8, 8, 8, 7, 7, 7
    };

/** Default table column field values.
*/
    public static final String [] FIELD_DEFAULTS = {
	null, null, null, null, null, null, null, null, null, null, 
	null, "(SYS_EXTRACT_UTC(CURRENT_TIMESTAMP))"
    };
}
