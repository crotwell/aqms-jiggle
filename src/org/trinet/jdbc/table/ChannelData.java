package org.trinet.jdbc.table;

import java.sql.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.util.*;

/** Constructor uses static data members defined by the TableRowChannelData interface to initialize the base class with 
* the parameters necessary to describe the schema table named by the interface String parameter DB_TABLE_NAME. 
* The class implements several convenience methods to provides class specific static arguments to the argument
* list of the DataTableRow base class methods. Base class methods are used to set or modify the values and states
* of the contained DataObjects. Because the base class uses a JDBC connection class to access the database containing
* the table described by this object, a connection object must be instantiated before using any of the database enabled
* methods of this class.
* Object states refering to data update, nullness, and mutability are inhereited from the DataTableRow base class
* implementation of the DataState interface. These state conditions are used to control the methods access to the
* object and its contained DataObjects, which also implement the DataState interface.
* The default constructor sets the default states to: setUpdate(false), setNull(true), setMutable(true), and 
* setProcessing(NONE).
*/

public class ChannelData extends ResponseStnChlTableRow implements TableRowChannelData {
    public ChannelData () {
	super(DB_TABLE_NAME, SEQUENCE_NAME, MAX_FIELDS, KEY_COLUMNS, FIELD_NAMES, FIELD_NULLS, FIELD_CLASS_IDS);
    }
    
/** Copy constructor invokes the default constructor, then clones all the DataObject classes of the input argument.
* The newly instantiated object state values are set to those of the input object.
*/ 
    public ChannelData(ChannelData object) {
	this();
	for (int index = 0; index < MAX_FIELDS; index++) {
	    fields.set(index, ((DataObject) object.fields.get(index)).clone());
	}
	this.valueUpdate = object.valueUpdate;
	this.valueNull = object.valueNull;
	this.valueMutable = object.valueMutable;
    }

/** Constructor invokes default constructor, then sets the default Connection object to the handle of the input Connection argument.
* The newly instantiated object state values are those of the default constructor.
*/
    public ChannelData(Connection conn) {
	this();
        setConnection(conn);
    }

/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
    public ChannelData(String net, String sta, String seedchan, String location, Date ondate) {
	this();
        fields.set(NET, new DataString(net));
        fields.set(STA, new DataString(sta));
        fields.set(SEEDCHAN, new DataString(seedchan));
        fields.set(LOCATION, new DataString(location));
        fields.set(ONDATE, new DataDate(ondate));
	valueUpdate = true;
	valueNull = false;
    }

/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
    public ChannelData(String net, String sta, String seedchan, String location, Date ondate, int unit_signal, int unit_calib,
		int format_id, double samprate) {
	this(net, sta, seedchan, location, ondate);
        fields.set(UNIT_SIGNAL, new DataLong(unit_signal));
        fields.set(UNIT_CALIB, new DataLong(unit_calib));
        fields.set(FORMAT_ID, new DataLong(format_id));
        fields.set(SAMPRATE, new DataDouble(samprate));
    }

/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
    public ChannelData(DataStnChl sc, Date ondate) {
	this();
        fields.set(NET, new DataString(sc.getStringValue(NET)));
        fields.set(STA, new DataString(sc.getStringValue(STA)));
        fields.set(SEEDCHAN, new DataString(sc.getStringValue(SEEDCHAN)));
        fields.set(LOCATION, new DataString(sc.getStringValue(LOCATION)));
        fields.set(CHANNEL, new DataString(sc.getStringValue(CHANNEL)));
        fields.set(CHANNELSRC, new DataString(sc.getStringValue(CHANNELSRC)));
        fields.set(ONDATE, new DataDate(ondate));
	valueUpdate = true;
	valueNull = false;
    }

/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
    public ChannelData(DataStnChl sc, Date ondate, int unit_signal, int unit_calib, int format_id, double samprate) {
	this(sc, ondate);
        fields.set(UNIT_SIGNAL, new DataLong(unit_signal));
        fields.set(UNIT_CALIB, new DataLong(unit_calib));
        fields.set(FORMAT_ID, new DataLong(format_id));
        fields.set(SAMPRATE, new DataDouble(samprate));
    }

/** Returns an array of ChannelData objects initialized with columns: NET,STA,SEEDCHAN,LAT,LON,ELEV,ONDATE,OFFDATE
*/
    public ChannelData [] getLocationsByDate(java.util.Date date) {
        if (this.connDB == null) { 
	    System.err.println("ChannelData.getLocationsByDate(date) JDBC connection null;" + 
		" application must first instantiate a connection class" +
		"; see DataSource");
	    return null;
        }
        StringBuffer sb = new StringBuffer(256);
        sb.append("SELECT NET, STA, SEEDCHAN, LAT, LON, ELEV, ONDATE, OFFDATE FROM CHANNEL_DATA WHERE ");
        sb.append(DataTableRowUtil.toDateConstraintSQLWhereClause("CHANNEL_DATA", date));

        Statement sm = null;
        try {
            sm = connDB.createStatement();
        }
        catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }

        ResultSet rs = ExecuteSQL.rowQuery(sm, sb.toString());
        java.util.ArrayList list = new java.util.ArrayList(512);

        try {
          if (rs != null) {
            while (rs.next()) {
                ChannelData cd = new ChannelData();
                cd.setValue(ChannelData.NET, rs.getString(1));
                cd.setValue(ChannelData.STA, rs.getString(2));
                cd.setValue(ChannelData.SEEDCHAN, rs.getString(3));

                cd.setValue(ChannelData.LAT, rs.getDouble(4));
                cd.setValue(ChannelData.LON, rs.getDouble(5));
                cd.setValue(ChannelData.ELEV, rs.getDouble(6));

                // 03/29/2005 converted to timestamp since Date truncates hr:mm to 00:00 local time -aww
                //cd.setValue(ChannelData.ONDATE, rs.getTimestamp(7));
                //cd.setValue(ChannelData.OFFDATE, rs.getTimestamp(8));
                // 2005/04/05 -removed above aww
                // String to UTC Date because jdbc times are shifted to local tz millisecs (PST)
                String dstr = rs.getString(7);
                if (dstr != null && dstr.indexOf('.',dstr.length()-4) < 0) dstr += ".0";
                cd.setValue(ChannelData.ONDATE,
                        (dstr == null) ? null : EpochTime.stringToDate(dstr));
                dstr = rs.getString(8);
                if (dstr != null && dstr.indexOf('.',dstr.length()-4) < 0) dstr += ".0";
                cd.setValue(ChannelData.OFFDATE,
                        (dstr == null) ? null : EpochTime.stringToDate(dstr));

                list.add(cd);
            }
          }
        }
        catch (SQLException ex) {
            ex.printStackTrace();
            //return null;
        }
        finally {
          try {
            if (rs != null) rs.close();
            if (sm != null) sm.close();
          } catch (SQLException ex) {}
        }
        return (rs == null) ? null : (ChannelData []) list.toArray(new ChannelData [list.size()]);
    }
}
