package org.trinet.jdbc.table;
import org.trinet.jdbc.datatypes.*;
import org.trinet.jdbc.*;
import java.util.*;
import java.sql.*;
import java.math.BigDecimal;
import org.trinet.jasi.ChannelIdIF;
import org.trinet.jasi.AuthChannelIdIF;

/** Base class for all schema table classes.
* Implements methods used by class extensions to retrieve and modify database table rows.
* Class data members are initialized by the implementations that extend this class.
* The state of the object instance and its data members can be modified/checked by set/get methods in this class.
* A JDBC database Connection member has to be initialized before using any method which accesses the database.
* @see #setConnection(Connection)
* @see #setConnection(Connection, DataTableRow [])
*/
public abstract class DataTableRow implements DataTableRowStates, DataClassIds, DataClasses, Cloneable {
/** Name of database table described by this object instance. */
    private String tableName; // must have correspondingly name java Class
/** Table key column identifier sequence name. */
    private String sequenceName;
/** Number of columns in table row. */
    private int maxFields;
/** Number of index columns in table row. */
    private int keyIndexLength;
/** Column indices of table keys. */
    private int [] keyColumnIndex;
/** Names of table columns. */
    private String [] fieldNames;
/** Flags indicate table columns as nullable. */
    private boolean [] fieldNulls;
/** Column data field classes. */
    private int [] fieldClassIds;
/** Appended to query if isSelectForUpdate() == true. */
    static final String SELECT_FOR_UPDATE = " FOR UPDATE";
/** Lock table mode if isLockTableForUpdate() == true. */
    static final String ROW_EXCLUSIVE_MODE_NOWAIT = " IN ROW EXCLUSIVE MODE NOWAIT";

/** Flag set by lockTableForUpdate(boolean). */
    private boolean lockTableFlag = false;
/** Flag set by selectTableForUpdate(boolean). */
    private boolean selectForUpdateTableFlag = false;

/** Contains a DataObject element for each table column data field. */ 
    protected Vector fields;
/** DataTableRow object processing mode.  */
    protected int processingStatus = NONE;
/** Flag object null status. */
    protected boolean valueNull = true;
/** Flag object update status. */
    protected boolean valueUpdate = false;
/** Flag object mutable status. */
    protected boolean valueMutable = true;

/** Handle of the default JDBC connnection object used by methods when no connection argument is specified. 
* @see #setConnection(Connection)
* @see #getConnection()
*/
    protected Connection connDB = null;

/** Constructor is invoked by the implementions of extending classes to define table parameters.
* Throws an IndexOutOfBoundsException if the input arguments violate any of these conditions:
*        maxFields <= 0, fieldNames.length != maxFields, fieldClassIds.length != maxFields,
*        keyColumnIndex.length < 1 or (keyColumnIndex.length > 1 && !(sequenceName==""))
*/
    protected DataTableRow(String tableName, String sequenceName, int maxFields, int [] keyColumnIndex, String[] fieldNames,
                        boolean [] fieldNulls, int[] fieldClassIds) throws NullPointerException, IndexOutOfBoundsException {
        this.tableName = tableName;
        if (tableName == null) throw new NullPointerException("DataTableRow Constructor - tableName == null.");
        this.maxFields = maxFields;
        if (maxFields <= 0) {
           throw new IndexOutOfBoundsException(tableName + " DataTableRow initialize(...)  maxFields must be > 0");
        }
        if (fieldNames.length != maxFields) {
           throw new IndexOutOfBoundsException(tableName + " DataTableRow initialize(...) fieldNames:" +
                         fieldNames.length  + " != "  + " maxFields:" + maxFields);
        }
        if (fieldNulls.length != maxFields) {
           throw new IndexOutOfBoundsException(tableName + " DataTableRow initialize(...) fieldNulls:" +
                         fieldNulls.length  + " != "  + " maxFields:" + maxFields);
        } 
        if (fieldClassIds.length != maxFields) {
           throw new IndexOutOfBoundsException(tableName + " DataTableRow initialize(...) fieldClassIds:" +
                         fieldClassIds.length  + " != "  + " maxFields:" + maxFields);
        } 
        if (keyColumnIndex.length < 1 ) {
           throw new IndexOutOfBoundsException(tableName + " DataTableRow initialize(...) keyColumnIndex.length:" +
                 keyColumnIndex.length  + " must be > 0");
        }
/*        if (keyColumnIndex.length > 1 && ! NullValueDb.isEmpty(sequenceName)) {
           throw new IndexOutOfBoundsException(tableName +
                 " DataTableRow initialize(...) keyColumnIndex length must equal 1 if a sequenceName string is specified.");
        }
*/
        this.sequenceName = sequenceName;
        this.keyColumnIndex = keyColumnIndex;
        this.keyIndexLength = keyColumnIndex.length;
        this.fieldNames = fieldNames;
        this.fieldNulls = fieldNulls;
        this.fieldClassIds = fieldClassIds;

        this.fields = new Vector(maxFields);
        this.fields.setSize(maxFields); // this is the equvalent of the loop below
//        for (int index = 0; index < maxFields; index++) {
//            this.fields.add(null); // can't fields.set(int,Object) if no elements are added.
//        }
    }


/** Implement the cloneable interface, clones the data field members 
* after invoking super.clone().
*/
    public Object clone() {
        DataTableRow obj = null;
        try {
            obj = (DataTableRow) super.clone();
        }
        catch (CloneNotSupportedException ex) {
            System.out.println(tableName + " DataTableRow Cloneable not implemented for class.");
            ex.printStackTrace();
        }
        for (int index = 0; index < maxFields; index++) {
            DataObject dataObj = (DataObject) obj.fields.get(index);
            if (dataObj != null) obj.fields.set(index, dataObj.clone());
            else obj.fields.set(index, null);
        }
        return obj;
    }

/** Resets the state flags of this object instance and those of its contained column data to the default values:
* update == false, null == true, mutable == true.
*/
    public void setDefaultStates() {
        this.setUpdate(false).setNull(true).setMutable(true);
        for (int index = 0; index < maxFields; index++) {
            DataObject obj = (DataObject) fields.get(index);
            if (obj != null) obj.setUpdate(false).setNull(true).setMutable(true);
        }
    }

/** Enables/disables table locking in ROW EXCLUSIVE MODE before table modification statement execution.
* Returns a handle to this object instance.
*/
    public DataTableRow setLockTableForUpdate(boolean value) {
        lockTableFlag = value;
        return this;
    }

/** Returns table locking status, whether it is in ROW EXCLUSIVE MODE before table modification statement execution.
*/
    public boolean isLockTableForUpdate() {
        return lockTableFlag;
    }

/** Enables FOR UPDATE option for all table queries with getRowXXXX methods.
* Returns a handle to this object instance.
*/
    public DataTableRow setSelectForUpdate(boolean value) {
        selectForUpdateTableFlag = value;
        return this;
    }

/** Returns FOR UPDATE option status for all table queries using getRowXXXX methods.
*/
    public boolean isSelectForUpdate() {
        return selectForUpdateTableFlag;
    }

/** Returns the table name string.
*/
    public String getTableName() {
        return tableName;
    }

/** Returns the table sequence name string.
*/
    public String getSequenceName() {
        return sequenceName;
    }

/** Returns the number of column fields in a single table row.
*/
    public int getMaxFields() {
        return maxFields;
    }

/** Returns an array of table row key field indices whose values are the key column position.
*/
    public int [] getKeyIndex() {
        return keyColumnIndex;
    }

/** Returns an array of table column name strings indexed by column position.
*/
    public String [] getFieldNames() {
        return fieldNames;
    }

/** Returns an array of booleans indicating which table columns are nullable indexed by column position.
*/
    public boolean [] getFieldNullables() {
        return fieldNulls;
    }

/** Returns an array of table column data Class identifiers indexed by column position.
* @see org.trinet.jdbc.datatypes.DataClassIds
* @see org.trinet.jdbc.datatypes.DataClasses
*/
    public int [] getFieldClassIds() {
        return fieldClassIds;
    }

/** Aliases the JDBC Connection for this instance to the specified Connection input argument.
* All database operation methods create, execute, and close a java.sql.Statement using this connection object. 
* @see #setConnection(Connection, DataTableRow [])
* @see #getConnection()
*/
   public void setConnection(Connection conn) {
        this.connDB = conn;
        return;
   }

/** Aliases the JDBC Connection of each of the DataTableRow objects in the input array
* to the Connection input argument.
* All database operation methods create, execute, and close a java.sql.Statement using this connection object. 
* @see #setConnection(Connection)
* @see #getConnection()
*/
   public static void setConnection(Connection conn, DataTableRow [] array) {
        for ( int index = 0; index < array.length; index++) {
            array[index].setConnection(conn);
        }
        return;
   }

/** Returns the handle of the JDBC Connection stored in this instance.
* @see #setConnection(Connection)
* @see #setConnection(Connection, DataTableRow [])
*/
   public Connection getConnection() {
        return this.connDB;
   }

/** Returns current primary key sequence number for table from the sequence named by the implementing class.
* Requires an active non-null JDBC database Connection reference.
* Method uses the JDBC Connection object assigned with setConnection().
* Oracle requires a prior call to sequenceName.NEXTVAL. 
* Returns -1 if sequence is not defined or error.
* @see #setConnection(Connection)
*/
    public long getCurrentSeq() {
        return getCurrentSeq(connDB);
    }

/** Returns current primary key sequence number for table from the sequence named by the implementing class.
* Requires an active non-null JDBC database Connection reference.
* Oracle requires a prior call to sequenceName.NEXTVAL. 
* Returns -1 if sequence is not defined or error.
* @see #setConnection(Connection)
*/
    public long getCurrentSeq(Connection conn) {
        if (NullValueDb.isEmpty(sequenceName)) return -1;
        if (conn == null) {
            System.err.println(tableName + " DataTableRow getCurrentSeq: JDBC connection input argument null;" +
                " Application must first instantiate a connection class" +
                "; see DataSource");
            return -1l;
        }
        return SeqIds.getCurrSeq(conn, sequenceName);
    }

/** Returns next primary key sequence number for table from the sequence named by the implementing class.
* Requires an active non-null JDBC database Connection reference.
* Method uses the JDBC Connection object assigned with setConnection().
* Returns -1 if sequence is not defined or error.
* @see #setConnection(Connection)
*/
    public long getNextSeq() {
        return getNextSeq(connDB);
    }

/** Returns next primary key sequence number for table from the sequence named by the implementing class.
* Requires an active non-null JDBC database Connection reference.
* Returns -1 if sequence is not defined or error.
* @see #setConnection(Connection)
*/
    public long getNextSeq(Connection conn) {
        if (NullValueDb.isEmpty(sequenceName)) return -1;
        if (conn == null) {
            System.err.println(tableName + " DataTableRow getNextSeq: JDBC connection input argument null;" + 
                " Application must first instantiate class;" +
                " see DataSource(String url, String driverName, String user, String passwd)");
            return -1l;
        }
        return SeqIds.getNextSeq(conn, sequenceName);
    }

/** Returns boolean true if the field at the specified column position is nullable in the database table.
*/
    public boolean isNullable(int index) throws IndexOutOfBoundsException {
        if (index < 0 || index > fields.size())
            throw new IndexOutOfBoundsException(tableName +
                 " DataTableRow isNullable(int index) field index out of bounds:" + index);
        return fieldNulls[index];
    }

/** Returns boolean true if the named column field is nullable in the database table.
*/
    public boolean isNullable(String name) throws NoSuchFieldException {
        int index = findFieldIndex(name);
        if (index < 0)
                throw new NoSuchFieldException(tableName + " DataTableRow isNullable(String name) unknown field name:" + name);
        return isNullable(index);
    }

// Methods that implement a state control for a DataTableRow
/** Enable/Disable this DataTableRow object as updated or ready for processing.
* Returns a handle to this object instance.
*/
    public DataTableRow setUpdate(boolean value) {
        this.valueUpdate = value;
        return this;
    }

/** Returns update status of this object instance.
*/
    public boolean isUpdate() {
        return valueUpdate;
    }

/** Enable/Disable this DataTableRow object as flagged null.
* Returns a handle to this object instance.
*/
    public DataTableRow setNull(boolean value) {
        this.valueNull = value;
        return this;
    }

/** Returns null status of this object instance.
*/
    public boolean isNull() {
        return valueNull;
    }

/** Enable/Disable this DataTableRow as mutabile.
* Returns a handle to this object instance.
*/
    public DataTableRow setMutable(boolean value) {
        this.valueMutable = value;
        return this;
    }

/** Returns the mutability status of this object instance; is it editable or not. 
*/
    public boolean isMutable() {
        return valueMutable;
    }

/** Sets the processing state of this DataTableRow object.
* Returns a handle to this object instance.
* Throws IndexOutOfBounds exception if input argument does not match know value range.
* @see DataTableRowStates
*/
    public DataTableRow setProcessing(int value) {
        if (value < 0 || value > MAX_PROCESSING_STATES) 
                throw new IndexOutOfBoundsException(tableName + " DataTableRow setProcessing argument value not in range.");
        else this.processingStatus = value;
        return this;
    }

/** Get the processing state of this DataTableRow, must be one of the constants defined in DataTableRowStates interface. 
* @see DataTableRowStates
*/
    public int getProcessing() {
        return processingStatus;
    }

/** Enable/disable the update state of the data value of every column field.
* Returns a handle to this object instance.
*/
    public DataTableRow setUpdateAllValues(boolean value) {
        for (int index = 0; index < fields.size(); index++) {
            DataState ds = (DataState) fields.get(index);
            if (ds != null) ds.setUpdate(value);
        }
        return this;
    }

/** Enable/disable the update state of every key column field.
* Returns a handle to this object instance.
* Note - keyColumnObject.isUpdate() must return true for a key column value to be included in the WHERE condition of a database
* modification SQL statement.
* Invoking setUpdateAllKeyValues(false) results in getWhereKeyString() returning null, thus results in a no-op for a
* insertXXX(), updateXXX() or deleteXXX methods.
* @see #getWhereKeyString()
*/
    public DataTableRow setUpdateAllKeyValues(boolean value) {
        for (int index = 0; index < keyIndexLength; index++) {
            DataState ds = (DataState) fields.get(keyColumnIndex[index]);
            if (ds != null) ds.setUpdate(value);
        }
        return this;
    }

/** Enable/disable the null state for the data value of every column field.
* Returns a handle to this object instance.
*/
    public DataTableRow setNullAllValues(boolean value) {
        for (int index = 0; index < fields.size(); index++) {
            DataState ds = (DataState) fields.get(index);
            if (ds != null) ds.setNull(value);
        }
        return this;
    }

/** Enable/disable the editable state of the data value of every column field.
* Returns a handle to this object instance.
*/
    public DataTableRow setMutableAllValues(boolean value) {
        for (int index = 0; index < fields.size(); index++) {
            DataState ds = (DataState) fields.get(index);
            if (ds != null) ds.setMutable(value);
        }
        return this;
    }

/** Returns the table column index of the specified table column name.
*/
    public int findFieldIndex(String name) {
        for (int index = 0; index < fields.size(); index++) {
            if (fieldNames[index].equals(name.trim().toUpperCase())) return index;
        }
        return -1;
    }

/** Returns data value null status by specified column field index.
*/
    public boolean isNullValue(int index) throws IndexOutOfBoundsException {
        if (index < 0 || index > fields.size())
                throw new IndexOutOfBoundsException(tableName + " DataTableRow isNullValue(int) index out of bounds:" + index);
        DataState ds = (DataState) fields.get(index);
        if (ds != null) return ds.isNull();
        else return true;
    }

/** Returns data value null status by specified column name.
*/
    public boolean isNullValue(String name) throws NoSuchFieldException {
        int index = findFieldIndex(name);
        if (index < 0) throw new NoSuchFieldException(tableName + " DataTableRow isNullValue(name) unknown field name:" + name);
        return isNullValue(index);
    }

/** Returns data value update status by specified column field index.
*/
    public boolean isUpdateValue(int index) throws IndexOutOfBoundsException {
        if (index < 0 || index > fields.size())
                throw new IndexOutOfBoundsException(tableName +
                         " DataTableRow isUpdateValue(int) field index out of bounds:" + index);
        DataState ds = (DataState) fields.get(index);
        if (ds != null) return ds.isUpdate();
        else return false;
    }

/** Returns data value update status by specified column name.
*/
    public boolean isUpdateValue(String name) throws NoSuchFieldException {
        int index = findFieldIndex(name);
        if (index < 0) throw new NoSuchFieldException(tableName + " DataTableRow isUpdateValue(name) unknown field name:" + name);
        return isUpdateValue(index);
    }

/** Returns data value mutability status by specified column field index.
*/
    public boolean isMutableValue(int index) throws IndexOutOfBoundsException {
        if (index < 0 || index > fields.size())
                throw new IndexOutOfBoundsException(tableName +
                         " DataTableRow isMutableValue(int) field index out of bounds:" + index);
        DataState ds = (DataState) fields.get(index);
        if (ds != null) return ds.isMutable();
        else return true;
    }

/** Returns data value mutability status by specified column name.
*/
    public boolean isMutableValue(String name) throws NoSuchFieldException {
        int index = findFieldIndex(name);
        if (index < 0) throw new NoSuchFieldException(tableName + " DataTableRow isMutableValue(name) unknown field name:" + name);
        return isMutableValue(index);
    }

/** Set data value mutability state by specified column name.
*/
    public DataTableRow setMutableValue(String name, boolean value) throws NoSuchFieldException {
        int index = findFieldIndex(name);
        if (index < 0) throw new NoSuchFieldException(tableName + " DataTableRow setMutableValue(name) unknown field name:" + name);
        return setMutableValue(index, value);
    }

/** Set data value mutability state by specified column field index.
*/
    public DataTableRow setMutableValue(int index, boolean value) throws IndexOutOfBoundsException {
        if (index < 0 || index > fields.size())
                throw new IndexOutOfBoundsException(tableName +
                         " DataTableRow setMutableValue(int) field index out of bounds:" + index);
        DataState ds = (DataState) fields.get(index);
        if (ds != null) ds.setMutable(value);
        return this;
    }

/** Set data value update state for all fields.
*/
    public DataTableRow setUpdateValues(boolean value) {
        DataState ds = null;
        for (int index = 0; index< fields.size(); index++) {
          ds = (DataState) fields.get(index);
          if (ds != null) ds.setUpdate(value);
        }
        return this;
    }

/** Set data value update state by specified column name.
*/
    public DataTableRow setUpdateValue(String name, boolean value) throws NoSuchFieldException {
        int index = findFieldIndex(name);
        if (index < 0) throw new NoSuchFieldException(tableName + " DataTableRow setUpdateValue(name) unknown field name:" + name);
        return setUpdateValue(index, value);
    }

/** Set data value update state by specified column field index.
*/
    public DataTableRow setUpdateValue(int index, boolean value) throws IndexOutOfBoundsException {
        if (index < 0 || index > fields.size())
                throw new IndexOutOfBoundsException(tableName +
                         " DataTableRow setUpdateValue(int) field index out of bounds:" + index);
        DataState ds = (DataState) fields.get(index);
        if (ds != null) ds.setUpdate(value);
        return this;
    }

/** Set data value null state by specified column name.
*/
    public DataTableRow setNullValue(String name, boolean value) throws NoSuchFieldException {
        int index = findFieldIndex(name);
        if (index < 0) throw new NoSuchFieldException(tableName + " DataTableRow setNullValue(name) unknown field name:" + name);
        return setNullValue(index, value);
    }

/** Set data value null state by specified column field index.
*/
    public DataTableRow setNullValue(int index, boolean value) throws IndexOutOfBoundsException {
        if (index < 0 || index > fields.size())
                throw new IndexOutOfBoundsException(tableName +
                         " DataTableRow setNullValue(int) field index out of bounds:" + index);
        if (value == true && ! fieldNulls[index]) return this;
        DataState ds = (DataState) fields.get(index);
        if (ds != null) ds.setNull(value);
        return this;
    }

/** Returns the DataObject for the specified table column name from the data collection.
*/
    public DataObject getDataObject(String name) throws NoSuchFieldException {
        int index = findFieldIndex(name);
        if (index < 0) throw new NoSuchFieldException(tableName +" DataTableRow getObject(String name) unknown field name:" + name);
        DataObject dataObj = getDataObject(index);
        if (dataObj == null) return null;
        else return (DataObject) dataObj.clone();
    }

/** Returns the DataObject for the specified table column index from the data collection.
*/
    public DataObject getDataObject(int index) throws IndexOutOfBoundsException {
        if (index < 0 || index > fields.size())
                throw new IndexOutOfBoundsException(tableName +
                         " DataTableRow getDataObject(index) field index out of bounds:" + index);
        return (DataObject) fields.get(index);
    }

/** Returns a primitive equivalent to the value of the DataObject for the specified table column name.
*/
    public int getIntValue(String name) throws NoSuchFieldException {
        int index = findFieldIndex(name);
        if (index < 0) throw new NoSuchFieldException(tableName +
                         " DataTableRow getIntValue(String name) unknown field name:" + name);
        return getIntValue(index);
    }

/** Returns a primitive equivalent to the value of the DataObject for the specified table column index.
*/
    public int getIntValue(int index) throws IndexOutOfBoundsException, NullPointerException {
        if (index < 0 || index > fields.size())
                throw new IndexOutOfBoundsException(tableName +
                         " DataTableRow getIntValue(index) field index out of bounds:" + index);
        DataObject dataObj = (DataObject) fields.get(index);
        if (dataObj == null)
                 throw new NullPointerException(tableName + " DataTableRow getIntValue: field object null at index: " + index);
        return dataObj.intValue();
    }

/** Returns a primitive equivalent to the value of the DataObject for the specified table column name.
*/
    public long getLongValue(String name) throws NoSuchFieldException {
        int index = findFieldIndex(name);
        if (index < 0) throw new NoSuchFieldException(tableName +
                         " DataTableRow getLongValue(String name) unknown field name:" + name);
        return getLongValue(index);
    }

/** Returns a primitive equivalent to the value of the DataObject for the specified table column index.
*/
    public long getLongValue(int index) throws IndexOutOfBoundsException, NullPointerException {
        if (index < 0 || index > fields.size())
                throw new IndexOutOfBoundsException(tableName +
                         " DataTableRow getLongValue(index) field index out of bounds:" + index);
        DataObject dataObj = (DataObject) fields.get(index);
        if (dataObj == null)
                throw new NullPointerException(tableName + "DataTableRow getLongValue: field object null at index: " + index);
        return dataObj.longValue();
    }

/** Returns a primitive equivalent to the value of the DataObject for the specified table column name.
*/
    public float getFloatValue(String name) throws NoSuchFieldException {
        int index = findFieldIndex(name);
        if (index < 0) throw new NoSuchFieldException(tableName +
                         " DataTableRow getFloatValue(String name) unknown field name:" + name);
        return getFloatValue(index);
    }

/** Returns a primitive equivalent to the value of the DataObject for the specified table column index.
*/
    public float getFloatValue(int index) throws IndexOutOfBoundsException, NullPointerException {
        if (index < 0 || index > fields.size())
                throw new IndexOutOfBoundsException(tableName +
                         " DataTableRow getFloatValue(index) field index out of bounds:" + index);
        DataObject dataObj = (DataObject) fields.get(index);
        if (dataObj == null)
                 throw new NullPointerException(tableName + " DataTableRow getFloatValue: field object null at index: " + index);
        return dataObj.floatValue();
    }

/** Returns a primitive equivalent to the value of the DataObject for the specified table column name.
*/
    public double getDoubleValue(String name) throws NoSuchFieldException {
        int index = findFieldIndex(name);
        if (index < 0) throw new NoSuchFieldException(tableName +
                         " DataTableRow getDoubleValue(String name) unknown field name:" + name);
        return getDoubleValue(index);
    }

/** Returns a primitive equivalent to the value of the DataObject for the specified table column index.
*/
    public double getDoubleValue(int index) throws IndexOutOfBoundsException, NullPointerException {
        if (index < 0 || index > fields.size())
                throw new IndexOutOfBoundsException(tableName +
                         " DataTableRow getDoubleValue(index) field index out of bounds:" + index);
        DataObject dataObj = (DataObject) fields.get(index);
        if (dataObj == null)
                throw new NullPointerException(tableName + " DataTableRow getDoubleValue: field object null at index: " + index);
        return dataObj.doubleValue();
    }

/** Returns a date object from the DataObject for the specified table column name.
* The table column field DataObject must implement the DataTime interface.
*/
    public java.sql.Date getDateValue(String name) throws NoSuchFieldException {
        int index = findFieldIndex(name);
        if (index < 0) throw new NoSuchFieldException(tableName +
                         " DataTableRow getDateValue(String name) unknown field name:" + name);
        return getDateValue(index);
    }

/** Returns a date object from the DataObject for the specified table column index.
* The table column field DataObject must implement the DataTime interface.
*/
    public java.sql.Date getDateValue(int index) throws IndexOutOfBoundsException, NullPointerException {
        if (index < 0 || index > fields.size())
                throw new IndexOutOfBoundsException(tableName +
                         " DataTableRow getDateValue(index) field index out of bounds:" + index);
        DataTime dataObj = (DataTime) fields.get(index);
        if (dataObj == null) 
                throw new NullPointerException(tableName + " DataTableRow getDateValue: field object null at index: " + index);
        return new java.sql.Date(dataObj.dateValue().getTime());
    }

/** Returns a timestamp object from the DataObject for the specified table column name.
* The table column field DataObject must implement the DataTime interface.
*/
    public java.sql.Timestamp getTimestampValue(String name) throws NoSuchFieldException {
        int index = findFieldIndex(name);
        if (index < 0) throw new NoSuchFieldException(tableName +
                 " DataTableRow getTimestampValue(String name) unknown field name:" + name);
        return getTimestampValue(index);
    }

/** Returns a timestamp object from the DataObject for the specified table column index.
* The table column field DataObject must implement the DataTime interface.
*/
    public java.sql.Timestamp getTimestampValue(int index) throws IndexOutOfBoundsException, NullPointerException {
        if (index < 0 || index > fields.size())
                throw new IndexOutOfBoundsException(tableName +
                 " DataTableRow getTimestampValue(index) field index out of bounds:" + index);
        DataTime dataObj = (DataTime) fields.get(index);
        if (dataObj == null)
                throw new NullPointerException(tableName + " DataTableRow getTimestampValue: field object null at index: " + index);
        return dataObj.timestampValue();
    }

/** Returns SQL syntax string value of getStringValue() for the specified table column name
* (non-numeric strings are surrounded by single quotes).
*/
    public String getStringValueSQL(String name) throws NoSuchFieldException {
        return StringSQL.valueOf(getStringValue(name));
    }

/** Returns SQL syntax string value of getStringValue() for the specified table column index
* (non-numeric strings are surrounded by single quotes).
*/
    public String getStringValueSQL(int index) throws IndexOutOfBoundsException, NullPointerException {
        return StringSQL.valueOf(getStringValue(index));
    }

/** Returns a string value derived from the DataObject for the specified table column name.
*/
    public String getStringValue(String name) throws NoSuchFieldException {
        int index = findFieldIndex(name);
        if (index < 0) throw new NoSuchFieldException(tableName +
                 " DataTableRow getStringValue(String name) unknown field name:" + name);
        return getStringValue(index);
    }

/** Returns a string value derived from the DataObject for the specified table column index.
*/
    public String getStringValue(int index) throws IndexOutOfBoundsException, NullPointerException {
        if (index < 0 || index > fields.size())
                throw new IndexOutOfBoundsException(tableName +
                 " DataTableRow getStringValue(index) field index out of bounds:" + index);
        DataObject dataObj = (DataObject) fields.get(index);

        if (dataObj == null) 
                throw new NullPointerException(tableName + " DataTableRow getStringValue: field object null at index: " +
                        index + " name:" + fieldNames[index]);
//        if (dataObj == null) return "NULL";
        return dataObj.toString();
    }

/** Creates for the specified table column index a newInstance of a DataObject of the same type as the column class.
* Invoked by data field value modification methods (setValue(xxx, xxx)) whenever the DataObject in the data collection is null.
* Returns the handle of the newly created DataObject.
*/
    private DataObject initializeDataField(int index) {
        DataObject dataObj = null;
        try {
            dataObj = (DataObject) DATA_CLASSES[fieldClassIds[index]].newInstance();
        }
        catch (IllegalAccessException ex) {
            System.err.println(tableName +
                 " DataTableRow initializeField(int index): class/initializer not accessible: " +
                DATA_CLASSES[fieldClassIds[index]]);
//            throw ex.fillInStackTrace();
        }
        catch (InstantiationException ex) {
            System.err.println(tableName +
                 " DataTableRow initializeField(int index): cannot instantiate class of type:" +
                DATA_CLASSES[fieldClassIds[index]]);
//            throw ex.fillInStackTrace();
        }
        return dataObj; 
    }

/** Sets the DataObject value for the specified table column name.
* Does a no-op if isMutable() == false or if a field is null and the required initializeDataField() fails.
* Sets the column field update status flag, isUpdate() == true.
* Sets the column field null status flag, isNull() == false.
* Sets the row null status flag, isNull() == false.
* Returns a handle to this object instance.
*/
    public DataTableRow setValue(String name, int value) throws NoSuchFieldException {
        if (! isMutable()) return this;
        int index = findFieldIndex(name);
        if (index < 0)
                throw new NoSuchFieldException(tableName +
                 " DataTableRow setValue(String name, int value) unknown field name:" + name);
        return setValue(index, value);
    }

/** Sets the DataObject value for the specified table column index.
* Does a no-op if isMutable() == false or if a field is null and the required initializeDataField() fails.
* Sets the column field update status flag, isUpdate() == true.
* Sets the column field null status flag, isNull() == false.
* Sets the row null status flag, isNull() == false.
* Returns a handle to this object instance.
*/
    public DataTableRow setValue(int index, int value) throws IndexOutOfBoundsException {
        if (! isMutable()) return this;
        if (index < 0 || index > fields.size())
            throw new IndexOutOfBoundsException(tableName +
                 " DataTableRow setValue(int index, int value) field index out of bounds:" + index);
        DataObject dataObj = (DataObject) fields.get(index);
        if (dataObj == null) {
            dataObj = initializeDataField(index);
            dataObj.setValue(value);
            fields.set(index, dataObj);
        }
        else dataObj.setValue(value);
        return setUpdate(true).setNull(false);
    }

/** Sets the DataObject value for the specified table column name.
* Does a no-op if isMutable() == false or if a field is null and the required initializeDataField() fails.
* Sets the column field update status flag, isUpdate() == true.
* Sets the column field null status flag, isNull() == false.
* Sets the row null status flag, isNull() == false.
* Returns a handle to this object instance.
*/
    public DataTableRow setValue(String name, long value) throws NoSuchFieldException {
        if (! isMutable()) return this;
        int index = findFieldIndex(name);
        if (index < 0)
                throw new NoSuchFieldException(tableName +
                 " DataTableRow setValue(String name, long value) unknown field name:" + name);
        return setValue(index, value);
    }

/** Sets the DataObject value for the specified table column index.
* Does a no-op if isMutable() == false or if a field is null and the required initializeDataField() fails.
* Sets the column field update status flag, isUpdate() == true.
* Sets the column field null status flag, isNull() == false.
* Sets the row null status flag, isNull() == false.
* Returns a handle to this object instance.
*/
    public DataTableRow setValue(int index, long value) throws IndexOutOfBoundsException {
        if (! isMutable()) return this;
        if (index < 0 || index > fields.size())
            throw new IndexOutOfBoundsException(tableName +
                 " DataTableRow setValue(int index, long value) field index out of bounds:" + index);
        DataObject dataObj = (DataObject) fields.get(index);
        if (dataObj == null) {
            dataObj = initializeDataField(index);
            dataObj.setValue(value);
            fields.set(index, dataObj);
        }
        else dataObj.setValue(value);
        return setUpdate(true).setNull(false);
    }

/** Sets the DataObject value for the specified table column name.
* Does a no-op if isMutable() == false or if a field is null and the required initializeDataField() fails.
* Sets the column field update status flag, isUpdate() == true.
* Sets the column field null status flag, isNull() == false.
* Sets the row null status flag, isNull() == false.
* Returns a handle to this object instance.
*/
    public DataTableRow setValue(String name, float value) throws NoSuchFieldException {
        if (! isMutable()) return this;
        int index = findFieldIndex(name);
        if (index < 0)
                throw new NoSuchFieldException(tableName +
                 " DataTableRow setValue(String name, float value) unknown field name:" + name);
        return setValue(index, value);
    }

/** Sets the DataObject value for the specified table column index.
* Does a no-op if isMutable() == false or if a field is null and the required initializeDataField() fails.
* Sets the column field update status flag, isUpdate() == true.
* Sets the column field null status flag, isNull() == false.
* Sets the row null status flag, isNull() == false.
* Returns a handle to this object instance.
*/
    public DataTableRow setValue(int index, float value) throws IndexOutOfBoundsException {
        if (! isMutable()) return this;
        if (index < 0 || index > fields.size())
            throw new IndexOutOfBoundsException(tableName +
                 " DataTableRow setValue(int index, float value) field index out of bounds:" + index);
        DataObject dataObj = (DataObject) fields.get(index);
        if (dataObj == null) {
            dataObj = initializeDataField(index);
            dataObj.setValue(value);
            fields.set(index, dataObj);
        }
        else dataObj.setValue(value);
        return setUpdate(true).setNull(false);
    }

/** Sets the DataObject value for the specified table column name.
* Does a no-op if isMutable() == false or if a field is null and the required initializeDataField() fails.
* Sets the column field update status flag, isUpdate() == true.
* Sets the column field null status flag, isNull() == false.
* Sets the row null status flag, isNull() == false.
* Returns a handle to this object instance.
*/
    public DataTableRow setValue(String name, double value) throws NoSuchFieldException {
        if (! isMutable()) return this;
        int index = findFieldIndex(name);
        if (index < 0)
                throw new NoSuchFieldException(tableName +
                 " DataTableRow setValue(String name, double value) unknown field name:" + name);
        return setValue(index, value);
    }

/** Sets the DataObject value for the specified table column index.
* Does a no-op if isMutable() == false or if a field is null and the required initializeDataField() fails.
* Sets the column field update status flag, isUpdate() == true.
* Sets the column field null status flag, isNull() == false.
* Sets the row null status flag, isNull() == false.
* Returns a handle to this object instance.
*/
    public DataTableRow setValue(int index, double value) throws IndexOutOfBoundsException {
        if (! isMutable()) return this;
        if (index < 0 || index > fields.size())
            throw new IndexOutOfBoundsException(tableName +
                 " DataTableRow setValue(int index, double value) field index out of bounds:" + index);
        DataObject dataObj = (DataObject) fields.get(index);
        if (dataObj == null) {
            dataObj = initializeDataField(index);
            dataObj.setValue(value);
            fields.set(index, dataObj);
        }
        else dataObj.setValue(value);
        return setUpdate(true).setNull(false);
    }

/** Sets the DataObject value for the specified table column name.
* Does a no-op if isMutable() == false or if a field is null and the required initializeDataField() fails.
* Sets the column field update status flag, isUpdate() == true.
* Sets the column field null status flag, isNull() == false.
* Sets the row null status flag, isNull() == false.
* Returns a handle to this object instance.
*/
    public DataTableRow setValue(String name, String value) throws NoSuchFieldException {
        if (! isMutable()) return this;
        int index = findFieldIndex(name);
        if (index < 0)
                throw new NoSuchFieldException(tableName +
                 " DataTableRow setValue(String name, String value) unknown field name:" + name);
        return setValue(index, value);
    }

/** Sets the DataObject value for the specified table column index.
* Does a no-op if isMutable() == false or if a field is null and the required initializeDataField() fails.
* Sets the column field update status flag, isUpdate() == true.
* Sets the column field null status flag, isNull() == false.
* Sets the row null status flag, isNull() == false.
* Returns a handle to this object instance.
*/
    public DataTableRow setValue(int index, String value) throws IndexOutOfBoundsException {
        //System.out.println("***DTR setValue(int,String) b4 Mutable return .");
        if (! isMutable()) return this;
        if (index < 0 || index > fields.size())
            throw new IndexOutOfBoundsException(tableName +
                 " DataTableRow setValue(int index, String value) field index out of bounds:" + index);
        DataObject dataObj = (DataObject) fields.get(index);
        if (dataObj == null) {
            dataObj = initializeDataField(index);
            dataObj.setValue(value);
            fields.set(index, dataObj);
        }
        else dataObj.setValue(value);
        return setUpdate(true).setNull(false);
    }

/** Sets the DataObject value for the specified table column name.
* Does a no-op if isMutable() == false or if a field is null and the required initializeDataField() fails.
* Sets the column field update status flag, isUpdate() == true.
* Sets the column field null status flag, isNull() == false.
* Sets the row null status flag, isNull() == false.
* Returns a handle to this object instance.
*/
    public DataTableRow setValue(String name, Object value) throws NoSuchFieldException {
        if (! isMutable()) return this;
        int index = findFieldIndex(name);
        if (index < 0)
                throw new NoSuchFieldException(tableName +
                 " DataTableRow setValue(String name, Object value) unknown field name:" + name);
        return setValue(index, value);
    }

/** Sets the DataObject value for the specified table column index.
* Does a no-op if isMutable() == false or if a field is null and the required initializeDataField() fails.
* Sets the column field update status flag, isUpdate() == true.
* Sets the column field null status flag, isNull() == false.
* Sets the row null status flag, isNull() == false.
* Returns a handle to this object instance.
*/
    public DataTableRow setValue(int index, Object value) throws IndexOutOfBoundsException {
        if (! isMutable()) return this;
//        if (value == null) throw new NullPointerException(tableName + " DataTableRow setValue(Object) argument null");
        if (index < 0 || index > fields.size())
            throw new IndexOutOfBoundsException(tableName +
                 " DataTableRow setValue(int index, Object value) field index out of bounds:" + index);
        DataObject dataObj = (DataObject) fields.get(index);
        if (dataObj == null) {
            dataObj = initializeDataField(index);
            dataObj.setValue(value);
            fields.set(index, dataObj);
        }
        else dataObj.setValue(value);
        return setUpdate(true).setNull(false);
    }

/** Returns a new array object containing the results of parsing the input ResultSet rows into instances of the implementing class. 
* A return == null indicates an error condition.
* Sets the mutability state of each DataTableRow instance to false if isSelectForUpdate == false.
* Sets the mutability state for the DataObject in any declared table key column to false.
*/
    protected Object parseResults(ResultSetDb rsdb) {
        return parseResults(rsdb, 0);
    }

/** Returns a new array object containing the results of parsing the input ResultSet rows into instances of the implementing class. 
* Starts each row parse at the column at the specified offset.
* Iterates through the ResultSet calling ResultSet.next().
* Closes the ResultSet when upon reaching the end of the results.
* Sets the mutability state of each DataTableRow instance to false if isSelectForUpdate == false.
* Sets the mutability state for the DataObject in any declared table key column to false.
*/
    public Object parseResults(ResultSetDb rsdb, int offset) {
        if (rsdb == null) {
            System.err.println(tableName + " DataTableRow parseResults: null ResultSetDb object.");
            return null;
        }
        if (offset < 0) {
            System.err.println(tableName + " DataTableRow parseResults: invalid offset argument: " + offset);
            return null;
        }
        ResultSet rs = rsdb.getResultSet();
        if (rs == null) {
            System.err.println(tableName + " DataTableRow parseResults: null ResultSet object.");
            return null;
        }

        try {
            ResultSetMetaData rsmd = rs.getMetaData();
            if (rsmd.getColumnCount() < offset + maxFields ) {
                System.err.println(tableName +
                        " DataTableRow parseResults: offset+totalRowColumns exceeds the total columns in ResultSet: " +
                        " offset+rowColumns: " + (offset+maxFields)  + " resultSetColumns: " + rsmd.getColumnCount());
                rs.close();
                return null;
            }
        }
        catch (SQLException ex) {
          SQLExceptionHandler.prtSQLException(ex);
          System.err.println(tableName + " DataTableRow parseResults: cannot get ResultSetMetaData");
          try {
            if (rs != null) rs.close();
          }
          catch (SQLException ex2) { }
          return null;
        }

        int nrows = 0;
        Vector vtr = new Vector();
        try {
            while (rs.next() ) {
                vtr.add(parseOneRow(rsdb, offset));
                nrows++;
            }
        }
        catch (SQLException ex) {
            System.err.println(tableName + " DataTableRow: parseResults ResultSet.next() SQLException");
            SQLExceptionHandler.prtSQLException(ex);
            vtr = null;
        }
        finally {
          try {
            rs.close();
          }
          catch (SQLException ex) {
            System.err.println(tableName + " DataTableRow: parseResults ResultSet.close() SQLException");
            SQLExceptionHandler.prtSQLException(ex);
          }
        }
        return recast(vtr);
    }

/** Returns a DataTableRow object resulting from parsing one row of the input ResultSetDb into instance of implementing class. 
* Starts row parse at the column at the specified offset. After row parse, does no other JDBC operations on ResultSetDb.
* Sets the mutability state of this DataTableRow instance to false if isSelectForUpdate == false.
* Each row column value is  represented by a DataObject class.
* For a row key column, DataObject.setUpdate() == true && DataObject.isMutable() == false.
* For a row non-key column, DataObject.isUpdate() == false && DataObject.isMutable() == true.
* Returns null if input ResultSetDb is null or input offset < 0.
* Returns null if input offset+table_row_columns (i.e. TableRowXXX.MAX_FIELDS) > number of columns returned in the ResultSetDb row.
* Returns null if an SQLException occurs during processing.
* @see ExecuteSQL#parseOneRow(ResultSet, int, Class, Connection)
*/
    public DataTableRow parseOneRow(ResultSetDb rsdb, int offset) {
        if (rsdb == null ) {
            System.err.println(tableName + " DataTableRow parseOneRow: null ResultSetDb input argument.");
            return null;
        }
        if (offset < 0) {
            System.err.println(tableName + " DataTableRow parseOneRow: invalid offset input argument.");
            return null;
        }
        ResultSet rs =  rsdb.getResultSet();
        if (rs == null) {
            System.err.println(tableName + " DataTableRow parseOneRow: null ResultSet object.");
            return null;
        }
        try {
            ResultSetMetaData rsmd = rs.getMetaData(); 
            if (rsmd.getColumnCount() < offset + maxFields ) {
                System.err.println(tableName +
                 " DataTableRow parseResults: offset+totalRowColumns exceeds the total columns in ResultSet: " +
                        " offset+rowColumns: " + (offset+maxFields)  + " resultSetColumns: " + rsmd.getColumnCount()); 
                //rs.close(); // let caller method close
                return null;
            }
        }
        catch (SQLException ex) {
          SQLExceptionHandler.prtSQLException(ex);
          System.err.println(tableName + " DataTableRow parseOneRow: cannot get ResultSetMetaData");
          //try { // let caller method close
          //  if (rs != null) rs.close();
          //}
          //catch (SQLException ex2) { }
          return null;
        }

        DataTableRow row = null;
        try {
            row = (DataTableRow) this.getClass().newInstance(); // create new instance of tableName class
        }
        catch (IllegalAccessException ex) {
            System.err.println(tableName + " DataTableRow parseOneRow(ResultSetDb): class or initializer not accessible.");
            return null;
        }
        catch (InstantiationException ex) {
            System.err.println(tableName + " DataTableRow parseOneRow(ResultSetDb): cannot instantiate class check type.");
            return null;
        }
        if (connDB != null) row.connDB = this.connDB;

// Remember ResultSet column indexes start at 1, but arrays start at 0.
        for (int index = 0; index < maxFields; index++) {
            try {
//                row.fields.set(index, rsdb.getDataObject( index + 1 + offset, this.fieldNames[index], this.DATA_CLASSES[fieldClassIds[index]] ) );
                row.fields.set(index, rsdb.getDataObject(index + 1 + offset, this.DATA_CLASSES[fieldClassIds[index]] ) );
// instead of setValue() used fields.set since ResultSetDb.getDataObject(...) does a setUpdate(false) for the new DataObject.
            }
            catch ( SQLException ex) {
                SQLExceptionHandler.prtSQLException(ex);
                System.err.println(tableName + " DataTableRow parseOneRow at column index value: " + index + " fieldName: " 
                    + fieldNames[index] + " fieldClass: " + DATA_CLASSES[fieldClassIds[index]] + " offset: " + offset);
                return null;
            }
            catch ( NoSuchFieldException ex) {
                ex.printStackTrace();
                return null;
            }
            catch ( IndexOutOfBoundsException ex) {
                    ex.printStackTrace();
                return null;
            }
        }
// key fields are not modifiable and are set as a valid update by default to be included in the WHERE cause on SQL update.
        for (int index = 0; index < keyIndexLength; index++) {
            ((DataObject) row.fields.get(keyColumnIndex[index])).setMutable(false).setUpdate(true); // ok since all fields != null
        }
        if (! isSelectForUpdate()) row.setMutable(false).setNull(false); // read-only mode, do not modify row contents
        else row.setNull(false);
        return row;
    }

/** Returns a new array of this class instance type containing the contents of the input List object.
* Returns null if the List object is null or empty.
*/
    public Object recast(List list) { 
        Object retVal = null;
        if (list == null) return null;
        if ( list.size() > 0) {
            retVal = java.lang.reflect.Array.newInstance(this.getClass(), list.size());
            for (int index = 0; index < list.size(); index++) {
                java.lang.reflect.Array.set(retVal, index,  list.get(index));
            }
        }
        return retVal;
    }

/** Returns a new array of this class instance type containing the contents of the input array object.
* Returns null if the input array is null or empty.
*/
    public Object recast(DataTableRow [] dtr) { 
        if (dtr == null) return null;
        if (dtr.length < 1) return null;
        Object retVal = java.lang.reflect.Array.newInstance(this.getClass(), dtr.length);
        for (int index = 0; index < dtr.length; index++) {
            java.lang.reflect.Array.set(retVal, index,  dtr[index]);
        }
        return retVal;
    }
 
/** Returns the count of database table rows satisfying: "SELECT COUNT( countExpression ) WHERE whereCondition". 
* If countExpression string is null, returns COUNT(*), the whereCondition input string can be null.
* The countExpression string is usually "*" or a combination of "DISTINCT or ALL" and a column name.
* Except for "*" expression, null column values are not included in the count unless the NVL function is used
* in the COUNT expression argument to specify an alternative value string.
* Thus, getRowCount("*", null) and getRowCount("*", "") both return a count of all rows in the table.
* The queried table name is that initialized by the implementing class instance.
* Returns -1 if an error occurs while executing the JDBC query.
* Assumes an existing JDBC Connection class instantiation for the connection to database.
*/
    public int getRowCount(String countExpression, String whereCondition) {
        return ExecuteSQL.getRowCount(connDB, tableName, countExpression, whereCondition);
    }
/** Returns a count of all rows in the table named by this object instance.
*/
    public int getRowCount() {
        return getRowCount("*", "");
    }

    /** Returns true if a row with same key exists the table of the database of default connection. */
    public boolean rowExists() {
        return rowExists(connDB);
    }

    /** Returns true if a row with same key exists the table of the database of input connection. */
    public boolean rowExists(Connection conn) {
        return ( ExecuteSQL.getRowCount(conn, tableName, "*", getWhereKeyString()) > 0 );
    }

/** Returns the array of DataTableRow objects of the invoking class type. 
* parses the resultSet returned by the database SQL query specified by the input string argument.
* Returns null if no data satifies specified string query or an error occurs while executing.
* Assumes an existing JDBC Connection class instantiation for the connection to database.
* Does not set the update state flag of the parsed database column data objects (object.isUpdate() == false).
*/
    protected  Object rowQuery(String sql) {
        return rowQuery(connDB, sql);
    }

    protected Object rowQuery(Connection conn, String sql) {
        if (conn == null) {
            System.err.println(tableName + " DataTableRow rowQuery: JDBC connection null;" + 
                " application must first instantiate a connection class" +
                "; see DataSource");
            return null;
        }
        Object rows = null;
        Statement sm = null;
        ExecuteSQL.setSelectForUpdate(isSelectForUpdate());        

        ResultSetDb rsdb = null;
        try {
            sm = conn.createStatement();
//                System.out.println("**DataTableRow rowQuery sql:\n" + sql);
            rsdb = new ResultSetDb(ExecuteSQL.rowQuery(sm, sql));
            rows = parseResults(rsdb);
        }
        catch (SQLException ex) {
            System.err.println(tableName + " DataTableRow: rowQuery() SQLException");
            SQLExceptionHandler.prtSQLException(ex);
            rows = null;
        }
        finally {
            try {
                if (rsdb.getResultSet() != null) rsdb.getResultSet().close();
                if (sm != null) sm.close(); // this should also close any open ResultSet
            }
            catch (SQLException exc) {
                SQLExceptionHandler.prtSQLException(exc);
            }
        }
        return rows;
    }

/** Returns String of know column names delimited by ",". */
    public String columnNames() {
        StringBuffer sb = new StringBuffer(512);
        int len = fieldNames.length;
        for (int index = 0; index < len; index++) {
            sb.append(fieldNames[index]).append(",");
        }
        return sb.substring(0, sb.length()-1);
    }

/** Returns String of table qualified known column names ("tableName.columnName") delimited by ",". */
    public String qualifiedColumnNames() {
        StringBuffer sb = new StringBuffer(1024);
        int len = fieldNames.length;
        for (int index = 0; index < len; index++) {
            sb.append(tableName).append(".").append(fieldNames[index]).append(",");
        }
        return sb.substring(0, sb.length()-1);
    }

/** Returns DataTableRow "keyFieldName=value AND ..." string for use in SQL WHERE clause.
* Invoked by methods which need to access database record with corresponding key.
* Return string contains only those key column fields where isUpdate() == true.
* Returns a null, if no key fields have isUpdate() == true.
* @see DataObject#setUpdate(boolean)
* @see DataTableRow#setUpdateAllValues(boolean)
* @see DataObject#isUpdate()
* @see DataTableRow#isUpdate()
*/
    public String getWhereKeyString() {
        StringBuffer retVal = new StringBuffer(512);
        retVal.append(" ( ");
        int length = retVal.length();
        for (int index = 0; index < keyIndexLength; index++) {
            DataObject dataObj = (DataObject) fields.get(keyColumnIndex[index]);
            if (dataObj == null ) continue;
            if (! dataObj.isUpdate() ) continue; // only append if update set for key DataObject
            if (isColumnEpochSecsUTC(fieldNames[keyColumnIndex[index]])) { // for UTC - aww 2008/02/19, changed to use new method 2010/02/26
                retVal.append(fieldNames[keyColumnIndex[index]]).append(" = ");
                retVal.append("truetime.putEpoch("); // for UTC - aww 2008/02/19
                retVal.append(dataObj.toStringSQL());
                retVal.append(",'UTC')").append(" AND "); // for UTC - aww 2008/02/19
            }
            else {
                retVal.append(fieldNames[keyColumnIndex[index]]).append(" = ").append(dataObj.toStringSQL()).append(" AND ");
            }
        }
        if (length >= retVal.length()) return null;
        retVal.replace(retVal.length()-5, retVal.length(),  " )");
        return retVal.toString(); 
    }

/** GetRows a protected wrapper for the private rowQuery(sql) method.
* Used by extending class implementations.
* Returns a new array object of the invoking class type containing the results of the query.
* Returns null if no data or error.
* Does not set the update state flag of the parsed database column data objects (object.isUpdate() == false).
* Requires an active non-null JDBC database Connection reference.
* Method uses the JDBC Connection object assigned with setConnection().
* Appends "FOR UPDATE" to query if isSelectForUpdate() == true.
*/
    protected Object getRows(String sql) {
        return getRows(connDB, sql);
    }

/** GetRows a protected wrapper for the private rowQuery(sql) method.
* Used by extending class implementations.
* Returns a new array object of the invoking class type containing the results of the query.
* Returns null if no data or an error occurs.
* Does not set the update state flag of the parsed database column data objects (object.isUpdate() == false).
* Requires an active non-null JDBC database Connection reference.
* Appends "FOR UPDATE" to query if isSelectForUpdate() == true.
*/
    protected Object getRows(Connection conn, String sql) {
        String sqltmp = sql;
        if (isSelectForUpdate()) {
          if (sql.toUpperCase().indexOf(SELECT_FOR_UPDATE) < 0) {
              StringBuffer sb = new StringBuffer(2048);
              sb.append(sql).append(SELECT_FOR_UPDATE);
              sqltmp = sb.toString();
          }
        }
        return rowQuery(conn, sqltmp);
    }

/** Returns a new DataTableRow object of the invoking class type containing the results of the query,
* created using the key fields values of this object instance.
* If a non-unique key in the query results in  more than one table row, only the first row is returned.
* Does not set the update state flag of the parsed database column data objects (isUpdate() == false).
* Returns null if no data satisfy query or an JDBC error occurs.
* Requires an active non-null JDBC database Connection reference.
* Requires getProcessing == SELECT or NONE for the input argument DataTableRow object.
* Method uses the JDBC Connection object assigned with setConnection().
*/
    public DataTableRow getRow() {
        DataTableRow [] array = (DataTableRow []) getRows();
        if (array != null)
            return array[0];
        else
            return null ;
    }

/** Returns a new DataTableRow object of the invoking class type containing the results of the query,
* created using the key fields values of input DataTableRow argument.
* However, does a no-op and returns null, if the input row null state is set, row.isNull() == true.
* If a non-unique key in the query results in  more than one table row, only the first row is returned.
* Returns null if no data satisfy query, or an JDBC error occurs.
* If the input DataTableRow key is not unique, representing multiple rows,
* only the object for the first of the matching rows is returned.
* Does not set the update state flag of the parsed database column data objects (isUpdate() == false).
* Requires an active non-null JDBC database Connection reference.
* Requires getProcessing == SELECT or NONE for the input argument DataTableRow object.
* Method uses the JDBC Connection object assigned with setConnection().
*/
    public DataTableRow getRow(DataTableRow row) {
        if (row.isNull()) return null;
        DataTableRow [] array = (DataTableRow []) getRows(row);
        if (array != null)
            return array[0];
        else
            return null ;
    }

/** Returns a new array object of the invoking class type containing the results of the query
* created using the key fields values of this object instance.
* Returns null if no data satisfy query or an JDBC error occurs.
* Does not set the update state flag of the parsed database column data objects (isUpdate() == false).
* If the input DataTableRow key is not unique, representing multiple rows,
* DataTableRow objects are created for all matching rows.
* Requires an active non-null JDBC database Connection reference.
* Requires getProcessing == SELECT or NONE for this instance object.
* Method uses the JDBC Connection object assigned with setConnection().
*/
    public Object getRows() {
        String whereCondition = getWhereKeyString();
        if (whereCondition == null) return null;
        if (getProcessing() != SELECT && getProcessing() != NONE) return null;
        StringBuffer sb = new StringBuffer(2048);
        sb.append("SELECT ").append(columnNames()).append(" FROM ").append(tableName);
        sb.append(" WHERE ").append(whereCondition);
        return getRows(connDB, sb.toString());
    }

/** Returns a new array object of the invoking class type containing the results of the query
* created using the key fields values of input DataTableRow object.
* However, does a no-op and returns null, if the input row null state is set, row.isNull() == true.
* Returns null if no data satisfy query or an JDBC error occurs.
* If the input DataTableRow key is not unique, representing multiple rows,
* DataTableRow objects are created for all matching rows.
* Does not set the update state flag of the parsed database column data objects (isUpdate() == false).
* Requires an active non-null JDBC database Connection reference.
* Requires getProcessing == SELECT or NONE for the input argument DataTableRow object.
* Method uses the JDBC Connection object assigned with setConnection().
*/
    public Object getRows(DataTableRow row) {
        if (row.isNull()) return null;
        String whereCondition = row.getWhereKeyString();
        if (whereCondition == null) return null;
        if (row.getProcessing() != SELECT && row.getProcessing() != NONE) return null;
        StringBuffer sb = new StringBuffer(2048);
        sb.append("SELECT ").append(columnNames()).append(" FROM ").append(tableName);
        sb.append(" WHERE ").append(whereCondition);
        return getRows(row.connDB, sb.toString());
    }


/** Returns a new array object of the invoking class type containing the results of calling getRows(DataTableRow) for
* each input array element, returns null if an error occurs or no data is found in the database.
* Does a no-op on input rows where, row.isNull() == true.
* If an input element key is not unique, representing multiple rows, all matching DataTableRow objects are created.
* Does not set the update state flag of the parsed database column data objects (isUpdate() == false).
* Requires getProcessing == SELECT or NONE for the for each element of the input argument array.
* Requires an active non-null JDBC database Connection reference.
* Method uses the JDBC Connection object assigned with setConnection() in each array object instance.
*/
    public Object getRows(DataTableRow [] array) {
        Vector vtr = new Vector();
        for ( int idx = 0; idx < array.length; idx++) {
            DataTableRow [] results = (DataTableRow []) getRows(array[idx]);
            if (results == null) continue;
            for (int index = 0; index < results.length; index++) {
                vtr.add(results[index]);
            }
        }
/*
        if ( vtr.size() > 0) {
            DataTableRow [] newArray = new DataTableRow [vtr.size()];
            vtr.toArray(newArray);
            return newArray;
        }
        else {
            return null;
        }
*/
        return recast(vtr);
    }
    

/** Returns a new array object of the invoking class type satisfying an SQL table query
* "SELECT DISTINCT + "columnNames()" + FROM" with specified WHERE clause. 
* The query table name is that initialized by the implementing class instance.
* Does not set the update state flag of the parsed database column data objects (isUpdate() == false).
* Requires an active non-null JDBC database Connection reference.
* Method uses the JDBC Connection object assigned with setConnection().
* Returns null if no rows satisfy query or an error condition occurs.
*/
    public Object getRowsEquals(String whereCondition) {
        StringBuffer sb = new StringBuffer(1024);
        sb.append("SELECT DISTINCT ").append(columnNames());
        sb.append(" FROM ").append(tableName).append(" ");
        sb.append(whereCondition);
        return getRows(connDB, sb.toString());
    }

/** A protected wrapper for the getRows(Connection, String) method.
* Used by extending class implementations.
* Returns a new array object of the invoking class type satisfying an SQL table query
* "SELECT * FROM ... WHERE columnName = longValue. 
* The query table name is that initialized by the implementing class instance.
* Does not set the update state flag of the parsed database column data objects (isUpdate() == false).
* Requires an active non-null JDBC database Connection reference.
* Method uses the JDBC Connection object assigned with setConnection().
* Returns null if no rows satisfy query or an error condition occurs.
*/
    protected Object getRowsEquals(String columnName, long longValue) {
        StringBuffer sb = new StringBuffer(2048);
        sb.append("SELECT ").append(columnNames()).append(" FROM ").append(tableName);
        sb.append(" WHERE ").append(columnName).append(" = ").append(longValue);
        return getRows(connDB, sb.toString());
    }

// Methods to modify database table rows 
/** Executes JDBC update statement for SQL statements of type INSERT, UPDATE, DELETE.
* Requires an active non-null JDBC database Connection reference.
* Method uses the JDBC Connection object assigned with setConnection().
* Returns number of rows modified, a return value of -1 indicates an error condition.
* @see #setConnection(Connection)
* @see #isLockTableForUpdate()
* @see #setLockTableForUpdate(boolean)
* @see ExecuteSQL#lockTableForUpdate(Connection, String, String)
*/
    int rowUpdate(String sql) {
        return rowUpdate(connDB, sql);
    }

/** Executes JDBC update statement for SQL statements of type INSERT, UPDATE, DELETE.
* Requires an active non-null JDBC database Connection reference.
* Returns number of rows modified, a return value of -1 indicates an error condition.
* @see #setConnection(Connection)
* @see #isLockTableForUpdate()
* @see #setLockTableForUpdate(boolean)
* @see ExecuteSQL#lockTableForUpdate(Connection, String, String)
*/
    int rowUpdate(Connection conn, String sql) {
        if (conn == null) {
            System.err.println(tableName + " DataTableRow rowUpdate: JDBC connection null;" +
                " application must first instantiate a connection class" + 
                " (see DataSource(String url, String driverName, String user, String passwd)");
            return -1;
        }
        if (isLockTableForUpdate()) {
            if (! ExecuteSQL.lockTableForUpdate(conn, tableName, ROW_EXCLUSIVE_MODE_NOWAIT)) return -1;
        }
//            System.out.println("DEBUG DataTableRow rowUpdate sql:\n" + sql);
        return ExecuteSQL.rowUpdate(conn, sql);
    }
    
/** Creates a database table row from the DataTableRow input argument object.
* Composes an SQL INSERT INTO table statement from the object's field names and values using the database table name
* initialized by the implementing class instance.
* For tables with the single primary key column specified by the sequence name initialized by the implementing class,
* if this key column value is not set, it is generated from the named sequence.
* Requires an active non-null JDBC database Connection reference.
* Requires for all key columns, column.isUpdate() == true, else the routine returns -1.
* Requires isUpdate() == true isNull() == false, and getProcessing() == INSERT for the input DataTableRow object, 
* else the method does a no-op.
* Returns 1 for success; returns 0, for a no-op.
* A return value of -1 indicates an error condition.
*/
    private int insertRow(DataTableRow row, Connection conn) {
        if (! row.isUpdate() || row.isNull()) return 0;
        if (row.getProcessing() != INSERT) return 0;
        boolean keySet = true;
        StringBuffer sql = new StringBuffer(1024);

    // scan the key to insure all values are set
        for (int index = 0; index < keyIndexLength; index++) {
            DataObject dataObj = (DataObject) row.fields.get(keyColumnIndex[index]);
            if (dataObj == null) keySet = false; 
            else if (! dataObj.isUpdate() ) keySet = false; 

      // if key is not set, try to set it using the sequencer
            if (! keySet) {
                if ( NullValueDb.isEmpty(row.getSequenceName()) ) {
                    System.out.println(tableName +
                         " DataTableRow insertRow - no id sequence name => all key column fields must be set.");
                    return -1;
                } else {
                    if (keyIndexLength > 1) {
                        System.out.println(tableName +
                         " DataTableRow insertRow: table must have only one key field if a sequenceName is specified."); 
                        return -1;
                    }
                    long ikey = getNextSeq(conn);
                    if (ikey < 1l) return -1; // returns error condition
                    DataLong dl = new DataLong(ikey);
                    dl.setMutable(false);
                    row.fields.set(keyColumnIndex[index], dl); // sets the value of the column for the key index field 
                    keySet = true;
                }
            }
        }

        sql.append("INSERT INTO ").append(tableName).append(" ( ");

    // NOTE: the tables are set to set LDDATE=SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) by default on an insert
    // where LDDATE is null. Therefore, the INSERT statement does not set LDDATE.
        try {
            DataDate dataObj = (DataDate) row.getDataObject("LDDATE");
            if (dataObj != null) {
//                dataObj.setValue(System.currentTimeMillis()); // the value set to current time
                dataObj.setUpdate(false); // the value defaults to table default
            }
        }
        catch (NoSuchFieldException ex) {
        }

        for (int index = 0 ; index < row.maxFields; index++) {
            DataObject dataObj = (DataObject) row.fields.get(index);
            if (dataObj == null) continue;
            if (! dataObj.isUpdate()) {
                for (int idx = 0; idx < keyIndexLength; idx++) { // include all key values
                    if (keyColumnIndex[idx] == index) {
                        sql.append(row.fieldNames[index]).append(", ");
                        break; // found key value, loop done 
                    }
                }
            }
            else sql.append(row.fieldNames[index]).append(", ");
        }

        sql.replace(sql.length()-2, sql.length(), " ) VALUES ( ");
        
        for (int index = 0; index < row.maxFields; index++) {
            DataObject dataObj = (DataObject) row.fields.get(index);
            if (dataObj == null) continue;
            if (! dataObj.isUpdate()) {
                for (int idx = 0; idx < keyIndexLength; idx++) { // include all key values
                    if (keyColumnIndex[idx] == index) {
                      if (isColumnEpochSecsUTC(fieldNames[index])) { // for UTC - aww 2008/02/19, changed to use new method 2010/02/26
                          sql.append("truetime.putEpoch(").append(dataObj.toStringSQL()).append(",'UTC'), ");  // for UTC - aww 2008/02/19
                      }
                      else sql.append(dataObj.toStringSQL()).append(", ");
                      break; //  found key value, loop done
                    }
                }
            }
            else { // field has updated value
                if (isColumnEpochSecsUTC(fieldNames[index])) { // for UTC - aww 2008/02/19, changed to use new method 2010/02/26
                    sql.append("truetime.putEpoch(").append(dataObj.toStringSQL()).append(",'UTC'), ");  // for UTC - aww 2008/02/19
                }
                else sql.append(dataObj.toStringSQL()).append(", "); // for UTC -aww 2008/02/19
            }
        }
        sql.replace(sql.length()-2, sql.length(), " )");
        return rowUpdate(conn, sql.toString());
    }
    
    /*
     * Returns true if input column field name contains string "DATETIME", false otherwise,
     * subclasses should override this method to add column names whose values are
     * UTC epoch seconds.
     */
    protected boolean isColumnEpochSecsUTC(String columnName) {
        // Added method to allow subclasses handle non-standard names like WSTART, SAVESTART, TIME1 etc. -aww 2010/02/26
        return (columnName.indexOf("DATETIME") >= 0 ); // for UTC - aww 2010/02/26
    }

/** Creates a database table row from the DataTableRow input argument object.
* Composes an SQL INSERT INTO table statement from the object's field names and values using the database table name
* initialized by the implementing class instance.
* All of the key column fields must have isUpdate() == true or the routine returns -1.
* For tables with the single primary key column specified by the sequence name initialized by the implementing class,
* if this key column value does not have isUpdate() == true, it is generated from the named sequence.
* Requires getProcessing == INSERT for this object instance.
* Requires an active non-null JDBC database Connection reference.
* Method uses the JDBC Connection object assigned with setConnection().
* Returns 1 for success; returns 0, a no-op, if this object instance is not set for update (isUpdate() == false),
* a return value of -1 indicates an error condition.
*/
    public int insertRow() {
        return insertRow(this, this.connDB);
    }

/** Creates a database table row from the DataTableRow input argument object.
* Composes an SQL INSERT INTO table statement from the object's field names and values using the database table name
* initialized by the implementing class instance.
* All key column fields must have isUpdate() == true or the routine returns -1.
* For tables with the single primary key column specified by the sequence name initialized by the implementing class,
* if this key column does not have isUpdate() == true, it is generated from the named sequence.
* Requires getProcessing == INSERT for this object instance.
* Requires an active non-null JDBC database Connection reference.
* Method uses the JDBC Connection object assigned with setConnection().
* Returns 1 for success; returns 0, a no-op, if this object instance is not set for update (isUpdate() == false),
* a return value of -1 indicates an error condition.
*/
    public int insertRow(Connection conn) {
        return insertRow(this, conn);
    }

/** Creates a database table row for each DataTableRow array object; primary key is generated from the table sequence.
* Does a no-op if the input DataTableRow object is not set for update (isUpdate() == false).
* Requires getProcessing == INSERT for the for each element of the input argument array.
* Requires an active non-null JDBC database Connection reference.
* Method uses the JDBC Connection object assigned with setConnection().
* Returns number of rows inserted, a return value of -1 indicates an error condition.
*/
    public int insertRows(DataTableRow [] array) {
        int nrows = 0;
        for (int index = 0; index < array.length; index++) {
            int retVal = insertRow(array[index], array[index].connDB);
            if (retVal < 0) break;
            nrows += retVal;;
        }
        return nrows;
    }

/** Creates a database table row for each DataTableRow array object; primary key is generated from the table sequence.
* Does a no-op if the input DataTableRow object is not set for update (isUpdate() == false).
* Requires getProcessing == INSERT for the for each element of the input argument array.
* Requires an active non-null JDBC database Connection reference.
* Returns number of rows inserted, a return value of -1 indicates an error condition.
*/
    public int insertRows(DataTableRow [] array, Connection conn) {
        int nrows = 0;
        for (int index = 0; index < array.length; index++) {
            int retVal = insertRow(array[index], conn);
            if (retVal < 0) break;
            nrows += retVal;;
        }
        return nrows;
    }
    
/** Deletes a database table row if its key fields match those of this object instance and 
* the row also satifies the input whereCondition criteria
* The whereCondition can be null or empty string.
* The table name acted upon is that initialized by the implementing class instance.
* Requires an active non-null JDBC database Connection reference.
* Requires isUpdate() == true isNull() == false, and getProcessing == DELETE for the input DataTableRow object,
* else the method does a no-op.
* Returns 1 for success; returns 0, a no-op.
* A return value of -1 indicates an error condition.
*/
    private int deleteRow(DataTableRow row, Connection conn, String whereCondition) {
        if (! row.isUpdate() || row.isNull()) return 0;
        if (row.getProcessing() != DELETE) return 0;
        StringBuffer sql = new StringBuffer(1024);
        String whereKeyCondition = row.getWhereKeyString();
        if (whereKeyCondition == null) return -1;
        sql.append("DELETE FROM ").append(row.tableName).append(" WHERE ").append(whereKeyCondition);
        if (! NullValueDb.isEmpty(whereCondition)) {
            int index = whereCondition.toUpperCase().indexOf("WHERE");
            sql.append(" AND ").append(whereCondition.substring(index+5));
        }
        return rowUpdate(conn, sql.toString());
    }

/** Deletes the row from the database which corresponds to this object's key field names and values.
* The table name acted upon is that initialized by the implementing class instance.
* Requires getProcessing == DELETE for this object instance.
* Requires an active non-null JDBC database Connection reference.
* Method uses the JDBC Connection object assigned with setConnection().
* Returns 1 for success; returns 0, a no-op, if this object instance is not set for update (isUpdate() == false).
* A return value of -1 indicates an error condition.
*/
    public int deleteRow() {
        return deleteRow(this, this.connDB, "");
    }

/** Deletes the row from the database which corresponds to this object's key field names and values.
* The table name acted upon is that initialized by the implementing class instance.
* Requires getProcessing == DELETE for this object instance.
* Requires an active non-null JDBC database Connection reference.
* Returns 1 for success; returns 0, a no-op, if this object instance is not set for update (isUpdate() == false).
* A return value of -1 indicates an error condition.
*/
    public int deleteRow(Connection conn) {
        return deleteRow(this, conn, "");
    }

/** Deletes the row from the database which corresponds to this object's key field names and values.
* The rows must match the values of the this object's key fields and the input whereCondition string.
* The whereCondition can be null or empty string.
* The table name acted upon is that initialized by the implementing class instance.
* Requires getProcessing == DELETE for this object instance.
* Requires an active non-null JDBC database Connection reference.
* Method uses the JDBC Connection object assigned with setConnection().
* Returns the number of rows deleted, if successful, returns 0 if this object's update and DELETE processing flags are not set.
* A return value of -1 indicates a null row key value or a JDBC execution error. 
*/
    public int deleteRow(String whereCondition) {
        return deleteRow(this, this.connDB, whereCondition);
    }

/** Deletes the row from the database which corresponds to this object's key field names and values.
* The rows must match the values of the this object's key fields and the input whereCondition string.
* The whereCondition can be null or empty string.
* The table name acted upon is that initialized by the implementing class instance.
* Requires getProcessing == DELETE for this object instance.
* Requires an active non-null JDBC database Connection reference.
* Returns the number of rows deleted, if successful, returns 0 if this object's update and DELETE processing flags are not set.
* A return value of -1 indicates a null row key value or a JDBC execution error. 
*/
    public int deleteRow(Connection conn, String whereCondition) {
        return deleteRow(this, conn, whereCondition);
    }

/** Deletes a database table row if its key matches that of the DataTableRow array object element.
* The table name acted upon is is that initialized by the implementing class instance.
* Requires getProcessing == DELETE for each element of the input argument array.
* Requires an active non-null JDBC database Connection reference.
* Method uses the JDBC Connection object assigned with setConnection() in each array element.
* Does a no-op if the input DataTableRow object is not set for update (isUpdate() == false).
* Returns number of rows deleted, a return value of -1 indicates an error condition.
*/
    public int deleteRows(DataTableRow [] array) {
        int nrows = 0;
        for (int index = 0; index < array.length; index++) {
            int retVal = deleteRow(array[index], array[index].connDB, "");
            if (retVal < 0) break;
            nrows += retVal;
        }
        return nrows;
    }

/** Deletes a database table row if its key matches that of the DataTableRow array object element.
* The table name acted upon is is that initialized by the implementing class instance.
* Requires getProcessing == DELETE for each element of the input argument array.
* Requires an active non-null JDBC database Connection reference.
* Does a no-op if the DataTableRow is not set for update (isUpdate() == false).
* Returns number of rows deleted, a return value of -1 indicates an error condition.
*/
    public int deleteRows(DataTableRow [] array, Connection conn) {
        int nrows = 0;
        for (int index = 0; index < array.length; index++) {
            int retVal = deleteRow(array[index], conn, "");
            if (retVal < 0) break;
            nrows += retVal;
        }
        return nrows;
    }

/** Deletes a database table row if its key matches that of the DataTableRow array object element and 
* the row also satifies the input whereCondition criteria. The whereCondition can be null or empty string.
* The table name acted upon is that initialized by the implementing class instance.
* Requires getProcessing == DELETE for each element of the input argument array.
* Requires an active non-null JDBC database Connection reference.
* Method uses the JDBC Connection object assigned with setConnection().
* Returns the number of rows deleted, if successful, else returns 0 if the row update and DELETE processing flags are not set.
* A return value of -1 indicates an error condition. 
*/
    public int deleteRows(DataTableRow [] array, String whereCondition) {
        int nrows = 0;
        for ( int idx = 0 ; idx < array.length ; idx++) {
            int retVal = deleteRow(array[idx], array[idx].connDB, whereCondition);
            if (retVal < 0) break;
            nrows += retVal;
        }
        return nrows;
    }

/** Deletes a database table row if its key matches that of the DataTableRow array object element and 
* the row also satifies the input whereCondition criteria. The whereCondition can be null or empty string.
* The table name acted upon is that initialized by the implementing class instance.
* Requires getProcessing == DELETE for each element of the input argument array.
* Requires an active non-null JDBC database Connection reference.
* Returns the number of rows deleted, if successful, else returns 0 if the row update and DELETE processing flags are not set.
* A return value of -1 indicates an error condition. 
*/
    public int deleteRows(DataTableRow [] array, Connection conn, String whereCondition) {
        int nrows = 0;
        for ( int idx = 0 ; idx < array.length ; idx++) {
            int retVal = deleteRow(array[idx], conn, whereCondition);
            if (retVal < 0) break;
            nrows += retVal;
        }
        return nrows;
    }

/** Updates a database row with the values where isUpdate() == true in the input DataTableRow object.
* The rows must match the values of the the input DataTableRow argument's key fields and the input whereCondition string.
* The table name acted upon is that initialized by the implementing class instance.
* Forces LDDATE to null value to allow database constraint to use default SYS_EXTRACT_UTC(CURRENT_TIMESTAMP).
* Requires an active non-null JDBC database Connection reference.
* Requires isUpdate() == true isNull() == false, and getProcessing == UPDATE for the input DataTableRow object,
* else the method does a no-op.
* Returns the number of rows updated, if successful; returns 0 for a no-op.
* A return value of -1 indicates a null row key value or a JDBC execution error. 
*/
    private int updateRow(DataTableRow row, Connection conn, String whereCondition) {
        if (! row.isUpdate() || row.isNull()) return 0;
        if (row.getProcessing() != UPDATE) return 0;
        StringBuffer sql = new StringBuffer(1024);

        int index = 0;
        /* Logic change next block below 06/22/2006 -aww <- DISABLED on 02/22/2007 -aww
        index = findFieldIndex("LDDATE");
        if (index > 0) {
            DataDate dataObj = (DataDate) row.getDataObject(index);
            if (dataObj != null) {
//            dataObj.setUpdate(false); // 09/19/00
//            dataObj.setUpdate(true); // force update to SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) function 6/22/2006 <- DISABLED 02/22/2007 -aww
            }
            // Must set value to enable update to SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) below - aww 06/22/2006 
//          else row.setValue(index, new DataDate()); <- DISABLED 02/22/2007 -aww
        }
        */

        sql.append("UPDATE ").append(row.tableName).append(" SET "); 
        int length = sql.length();
        FIELD_LOOP:
        for (index = 0; index < maxFields; index++) {
            for (int idx = 0; idx < keyColumnIndex.length; idx++) {
                if (keyColumnIndex[idx] == index) continue FIELD_LOOP;
            }
            DataObject dataObj = (DataObject) row.fields.get(index);
            if (dataObj == null) continue;
            if (! dataObj.isUpdate()) continue;
            String fieldName = row.fieldNames[index];
            if (isColumnEpochSecsUTC(fieldName)) { // for UTC - aww 2008/02/19, changed to use new method 2010/02/26
                sql.append(fieldName).append(" = truetime.putEpoch(").append(dataObj.toStringSQL()).append(",'UTC'), ");  // for UTC - aww 2008/02/19
            }
            else if (fieldName.equals("LDDATE") ) { // don't change LDDATE only set when row created -aww 2008/02/19
                //sql.append(fieldName).append(" =  SYS_EXTRACT_UTC(CURRENT_TIMESTAMP), "); 
                continue; // don't reset LDDATE 2008/05/30 -aww
            }
            else {
                sql.append(fieldName).append(" = ").append(dataObj.toStringSQL()).append(", "); 
            }
        }
        if (sql.length() <= length) return 0;        
        sql.replace(sql.length()-2,sql.length(), " ");
        String whereKeyCondition = row.getWhereKeyString();

        if (whereKeyCondition == null) return -1;
        sql.append("WHERE ").append(whereKeyCondition);

        if (! NullValueDb.isEmpty(whereCondition)) {
            index = whereCondition.toUpperCase().indexOf("WHERE");
            sql.append(" AND ").append(whereCondition.substring(index+5));
        }
        // if (row instanceof XXX) System.out.println(sql.toString());
        return rowUpdate(conn, sql.toString());
    }
    
/** Updates a single database row whose key values are the same as those of this object instance. 
* The values of the this object's key fields are used to construct the WHERE clause condition.
* The table name acted upon is that initialized by the implementing class instance.
* Requires getProcessing == UPDATE for this object instance.
* Requires an active non-null JDBC database Connection reference.
* Method uses the JDBC Connection object assigned with setConnection().
* Returns the number of rows updated, if successful.
* Returns 0 if the row update flag or none of its data values have update flags set (isUpdate()==true).
* A return value of -1 indicates an error condition. 
* @see DataTableRow#isUpdate()
* @see DataTableRow#setUpdate(boolean)
*/
    public int updateRow() {
        return updateRow(this, this.connDB, "");
    }

/** Updates a database with the values that have set update (isUpdate()==true) in this DataTableRow object instance. 
* The values of the this object's key fields are used to construct the WHERE clause condition for the update.
* The table name acted upon is that initialized by the implementing class instance.
* Requires getProcessing == UPDATE for this object instance.
* Requires an active non-null JDBC database Connection reference.
* Returns the number of rows updated, if successful.
* Returns 0 if the row update flag or none of its data values have update flags set (isUpdate()==true).
* A return value of -1 indicates an error condition. 
* @see DataTableRow#isUpdate()
* @see DataTableRow#setUpdate(boolean)
*/
    public int updateRow(Connection conn) {
        return updateRow(this, conn, "");
    }
    
/** Updates a database row with the values set in this object.
* The row must match the values of the this object's key fields and the input whereCondition string.
* The table name acted upon is that initialized by the implementing class instance.
* Requires getProcessing == UPDATE for this object instance.
* Requires an active non-null JDBC database Connection reference.
* Method uses the JDBC Connection object assigned with setConnection().
* Returns the number of rows updated, if successful.
* Returns 0 if the row update flag or none of its data values have update flags set (isUpdate()==true).
* data fields have set update (isUpdate() == true).
* A return value of -1 indicates a null row key value or a JDBC execution error. 
*/
    public int updateRow(String whereCondition) {
        return updateRow(this, this.connDB, whereCondition);
    }

/** Updates a database row with the values set in this object.
* The row must match the values of the this object's key fields and the input whereCondition string.
* The whereCondition can be null or empty string.
* The table name acted upon is that initialized by the implementing class instance.
* Requires getProcessing == UPDATE for this object instance.
* Requires an active non-null JDBC database Connection reference.
* Returns the number of rows updated, if successful.
* Returns 0 if the row update flag or none of its data values have update flags set (isUpdate()==true).
* A return value of -1 indicates a null row key value or a JDBC execution error. 
*/
    public int updateRow(Connection conn, String whereCondition) {
        return updateRow(this, conn, whereCondition);
    }

/** Update a database row for each member of the input array argument. 
* The values of a DataTableRow object's key fields are used to construct the WHERE clause condition.
* The table name acted upon is that initialized by the implementing class instance.
* Requires getProcessing == UPDATE for each element of the input argument array.
* Requires an active non-null JDBC database Connection reference.
* Method uses the JDBC Connection object assigned with setConnection().
* Returns the number of rows updated, if successful.
* Returns 0 if none of the row update flags or none of the data values have update flags set (isUpdate()==true).
* A return value of -1 indicates an error condition. 
*/
    public int updateRows(DataTableRow [] array) {
        int nrows = 0;
        for ( int idx = 0 ; idx < array.length ; idx++) {
            int retVal = updateRow(array[idx], array[idx].connDB, "");
            if (retVal < 0) break;
            nrows += retVal;
        }
        return nrows;
    }
    
/** Update a database row for each member of the input array argument. 
* The values of a DataTableRow object's key fields are used to construct the WHERE clause condition.
* The table name acted upon is that initialized by the implementing class instance.
* Requires getProcessing == UPDATE for each element of the input argument array.
* Requires an active non-null JDBC database Connection reference.
* Returns the number of rows updated, if successful.
* Returns 0 if none of the row update flags or none of the data values have update flags set (isUpdate()==true).
* A return value of -1 indicates an error condition. 
*/
    public int updateRows(DataTableRow [] array, Connection conn) {
        int nrows = 0;
        for ( int idx = 0 ; idx < array.length ; idx++) {
            int retVal = updateRow(array[idx], conn, "");
            if (retVal < 0) break;
            nrows += retVal;
        }
        return nrows;
    }

/** Updates a database row for each member of the input array argument. 
* The rows must match the values of the the input DataTableRow argument's key fields and the input whereCondition string.
* The table name acted upon is that initialized by the implementing class instance.
* Requires getProcessing == UPDATE for each element of the input argument array.
* Requires an active non-null JDBC database Connection reference.
* Method uses the JDBC Connection object assigned with setConnection().
* Returns the number of rows updated, if successful.
* Returns 0 if none of the row update flags or none of the data values have update flags set (isUpdate()==true).
* A return value of -1 indicates an error condition. 
*/
    public int updateRows(DataTableRow [] array, String whereCondition) {
        int nrows = 0;
        for ( int idx = 0 ; idx < array.length ; idx++) {
            int retVal = updateRow(array[idx], array[idx].connDB, whereCondition);
            if (retVal < 0) break;
            nrows += retVal;
        }
        return nrows;
    }

/** Update a database row for each member of the input array argument. 
* The rows must match the values of the the input DataTableRow argument's key fields and the input whereCondition string.
* The table name acted upon is that initialized by the implementing class instance.
* Requires getProcessing == UPDATE for each element of the input argument array.
* Requires an active non-null JDBC database Connection reference.
* Returns the number of rows updated, if successful.
* Returns 0 if none of the row update flags or none of the data values have update flags set (isUpdate()==true).
* A return value of -1 indicates an error condition. 
*/
    public int updateRows(DataTableRow [] array, Connection conn, String whereCondition) {
        int nrows = 0;
        for ( int idx = 0 ; idx < array.length ; idx++) {
            int retVal = updateRow(array[idx], conn, whereCondition);
            if (retVal < 0) break;
            nrows += retVal;
        }
        return nrows;
    }

/** Returns a String containing any remark associated with this instance.
* Returns an empty string if no remark is found.
*/
    public String getRemark() {
        int commid;
        try {
           commid = getIntValue("COMMID");
        }
        catch (NoSuchFieldException ex) {
            return "";
        }
        Remark rmk = new Remark();
        Remark [] rmkArray = (Remark []) rmk.setValue(Remark.COMMID, commid).getRows();
        if (rmkArray == null) return "";
        StringBuffer strbuf = new StringBuffer(rmkArray.length*80);
        for (int index = 0; index < rmkArray.length; index++) {
            strbuf.append(rmkArray[index].toStringRemark());
            if (index < rmkArray.length - 1) strbuf.append("\n");
        }
        return strbuf.toString();
    }

/** Returns string concatenating DataTableRow flags with one line per DataObject.classToString() for this instance.
*  Followed by a line repeating the DataTableRow state flag settings for this instance.
*/
    public String toString() {
        StringBuffer str = new StringBuffer(256);
        StringBuffer strTotal = new StringBuffer(8192);
        strTotal.append(tableName).append(" DataTableRow");
        strTotal.append("    Null:").append(valueNull);
        strTotal.append("    Update: ").append(valueUpdate);
        strTotal.append("    Mutable: ").append( valueMutable).append("\n");
        for (int index = 0; index < maxFields; index++) {
            DataObject dataObj = (DataObject) fields.get(index);
            if (dataObj == null) continue;
            str.append(tableName);
            str.append("                  ");
            str.insert(18,fieldNames[index]);
            int len = str.length();
            str.insert(len-1, "                  ");
            str.insert(35,":");
            str.insert(37, dataObj.classToString()).append("\n");
            String tmpstr = str.toString().trim();
            strTotal.append(tmpstr).append("\n");
            str.delete(0, str.length());
        }
        strTotal.append(tableName).append(" DataTableRow");
        strTotal.append("    Null:").append(valueNull);
        strTotal.append("    Update: ").append(valueUpdate);
        strTotal.append("    Mutable: ").append( valueMutable).append("\n");
        return strTotal.toString().trim();
    }
    
/** Returns true if argument object is of like class type and all of its DataObject column fields are equivalent to this instance. 
* The value and the state flag settings of each column must be equivalent or the method returns false.
* However, compared DataTableRow objects may have different connection object members and/or row-level state flag settings.
*/
    public boolean equals(Object x) {
        if (x == null || ! this.getClass().isInstance(x)) return false;
        for (int index = 0; index < maxFields; index++) {
            DataObject dataObj = (DataObject) fields.get(index);
            DataObject dataObjArg = (DataObject) ((DataTableRow) x).fields.get(index);
            if (dataObj == null && dataObjArg == null) continue;
            else if (dataObj == null) {
//                System.out.println("dataObj null at index:" + index);
                return false;
            }
            if (! dataObj.equals(dataObjArg) ) {
//                System.out.println("dataObj notEqual at index:" + index + "\n" + dataObj.classToString() + "\n" + dataObjArg.classToString()) ;
                return false;
            }
        }
        return true;             
    }

/** Returns true if argument object is of like class type and all of its DataObject field values are equivalent to this instance. 
* The flag settings of each column's DataObject are ignored. As with equals(Object) the compared DataTableRow objects
* may have different connection object members and/or row-level state flag settings.
*/
    public boolean equalsValue(Object x) {
        if (x == null || ! this.getClass().isInstance(x)) return false;
        for (int index = 0; index < maxFields; index++) {
            DataObject dataObj = (DataObject) fields.get(index);
            DataObject dataObjArg = (DataObject) ((DataTableRow) x).fields.get(index);
            if (dataObj == null && dataObjArg == null) continue;
            else if (dataObj == null) {
                System.out.println("dataObj null at index:" + index);
                return false;
            }
            if (! dataObj.equalsValue(dataObjArg) ) {
                System.out.println("dataObj notEqual at index:" + index + "\n" + dataObj.classToString() + "\n" + dataObjArg.classToString()) ;
                return false;
            }
        }
        return true;             
    }

/** Returns true if the input argument is of like class type and the URL string of its JDBC connection equals
* that of this instance. 
*/
    public boolean equalsConnection(Object x) {
        if (x == null || ! this.getClass().isInstance(x)) return false;
        try {
            if (connDB.getMetaData().getURL().equals( ((DataTableRow) x).getConnection().getMetaData().getURL()) ) return true;
        }
        catch (SQLException ex) {
            SQLExceptionHandler.prtSQLException(ex);
        }
        return false;             
    }

/** Aliases the column data field objects in this instance to the DataObject elements of the input array argument. 
* Returns the handle of this instance.
* Throws an IndexOutOfBoundsException if input array length < TableRowXXX.MAX_FIELDS,
* the the number of columns in this DataTableRow instance.
*/
    public DataTableRow dataObjectToFields(DataObject[] objArray) throws IndexOutOfBoundsException {
        if (objArray.length < maxFields) throw new IndexOutOfBoundsException(tableName +
                         " DataTableRow dataObjectToFields length<maxFields.");
        for (int index = 0; index < maxFields; index++) {
            fields.set(index, objArray[index]);
        }
        return this;
    }
    
/** Returns a new DataObject array whose elements are copied from the column DataObject fields of this instance. 
*/
    public DataObject[] fieldsToDataObject() {
        DataObject[] objArray = new DataObject[maxFields];
        return (DataObject []) fields.toArray(objArray);
    }
    
/** Returns a new Vector whose elements are copied from the DataObject fields of this instance. 
*/
    public Vector fieldsToVector() {
            return new Vector(fields);
    }
    
/** Aliases the column data fields of this instance to the values of the DataObject elements of the Vector argument. 
* Returns the handle of this instance.
* Throws a ClassCastException if vector elements are not all of type DataObject.
* Throws an IndexOutOfBoundsException if input object array length < TableRowXXX.MAX_FIELDS,
* the the number of columns in this DataTableRow instance.
*/
    public DataTableRow vectorToFields(Vector vtr) throws IndexOutOfBoundsException {
        if (vtr.size() < maxFields) throw new IndexOutOfBoundsException(tableName +
                         " DataTableRow vectorToFields vecotr.size() < maxFields.");
        for (int index = 0; index < maxFields; index++) {
            fields.set(index, (DataObject) vtr.elementAt(index));
        }
        return this;
    }

/** Returns an array of DataObject types for this instance derived from the data values found in an input object array 
* containing the java object types BigDecimal, String, and Timestamp.
* Starts the parsing of the input array at index specified by input offset argument.
* Returns null if input array is null.
* Returns null if any input object type is not one of the above listed Java class types.
* Returns null if the length of input array is less than offset+TableRowXXX.MAX_FIELDS, 
* The returned array has TableRowXXX.MAX_FIELDS length, the total number of columns in the table row represented by this instance,
* and can be used as input to the dataObjectToFields(Object[]) method to reset the data fields in this instance.
* @see #dataObjectToFields(DataObject[])
* @see ExecuteSQL#parseResults(ResultSet,int,int)
*/
    public DataObject [] objectToDataObject(Object [] object, int offset) {
        if (object == null) return null;
        if ( (object.length + offset) > maxFields) return null;
        DataObject [] retVal = new DataObject[maxFields];
        DataObject dataObj = null;
        for (int index = 0; index < maxFields; index++ ) {
            int idx = index + offset;
            if (BigDecimal.class.isInstance(object[idx])) {
                try {
                    dataObj = (DataObject) DATA_CLASSES[fieldClassIds[index]].newInstance();
                    dataObj.setValue(((DataObject) object[idx]).doubleValue());
                } 
                catch (IllegalAccessException ex) {
                    System.err.println(tableName +
                         " DataTableRow objectToDataObject(): class/initializer not accessible:" +
                        DATA_CLASSES[fieldClassIds[index]]);
                    return null;
                }
                catch (InstantiationException ex) {
                        System.err.println(tableName +
                                 " DataTableRow objectToDataObject(): cannot instantiate class of type:" +
                                DATA_CLASSES[fieldClassIds[index]]);
                    return null;
                }
            }
            else if (String.class.isInstance(object[idx])) {
                dataObj = new DataString((String) object[idx]);
             }
            else if (java.sql.Timestamp.class.isInstance(object[idx])) {
                dataObj = new DataTimestamp((java.sql.Timestamp) object[idx]);
            }
            else {
                System.err.println(tableName + " DataTableRow objectToDataObject(): input object class not recognized: " +
                        object[idx].getClass().getName());
                return null;
            }
            retVal[index] = dataObj;
        } 
        return retVal;
    }

} // end of DataTableRow class
