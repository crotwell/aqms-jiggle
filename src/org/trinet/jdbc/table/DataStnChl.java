package org.trinet.jdbc.table;
import org.trinet.jdbc.datatypes.*;
import org.trinet.jdbc.*;
import java.util.*;
import org.trinet.jasi.AuthChannelIdIF;
import org.trinet.jasi.ChannelIdIF;

/** Data class stores string data describing station channel data as defined by NCEDC database schema tables.
* Primarily used by the DataTableRow class the descendent database table classes (Amp, Arrival, etc.) containing station data.
* Static data define the table column names and their relative positions in the database tables.
* Each station channel data element is stored as a DataString data object in the fields data vector member.
* The class implements the state interface as do each of the stored DataString objects.
* @see DataString
* @see DataObject
* @see DataTableRow
*/
public class DataStnChl implements AuthChannelIdIF, DataClasses, DataClassIds, Cloneable {

/** Total number of data member fields in a station channel description
*/
    public static final int MAX_FIELDS = 8;

/** DataObject type used to stored string elements of station channel description.
* Types are defined by the DataClassIds and DataClasses static data interfaces.
* @see DataClassIds
* @see DataClasses
*/
    public static final int [] FIELD_CLASS_IDS =
        {
          DATASTRING, DATASTRING, DATASTRING, DATASTRING,
          DATASTRING, DATASTRING, DATASTRING, DATASTRING
        };

/** Names of the table columns used to describe a station channel in their relative index order.
*/
    public static final String [] FIELD_NAMES =
       {
        "STA", "NET", "AUTH", "SUBSOURCE", "CHANNEL", "CHANNELSRC",
        "SEEDCHAN", "LOCATION"
       };

/** Constant declares relative position index of column field in station channel/table description.
*/
    public static final int STA = 0;

/** Constant declares relative position index of column field in station channel/table description.
*/
    public static final int NET = 1;

/** Constant declares relative position index of column field in station channel/table description.
*/
    public static final int AUTH = 2;

/** Constant declares relative position index of column field in station channel/table description.
*/
    public static final int SUBSOURCE = 3;

/** Constant declares relative position index of column field in station channel/table description.
*/
    public static final int CHANNEL = 4;

/** Constant declares relative position index of column field in station channel/table description.
*/
    public static final int CHANNELSRC = 5;

/** Constant declares relative position index of column field in station channel/table description.
*/
    public static final int SEEDCHAN = 6;

/** Constant declares relative position index of column field in station channel/table description.
*/
    public static final int LOCATION = 7;

/** Delimiters for parsing input string tokens */
    public static final String FIELD_DELIMITERS = " _\t,";

/** Character "*" flags a null token field in input strings. */
    public static final String STAR_NULL_FIELD = "*";

    protected Integer hashCache;
/*
    public DataString sta;
    public DataString net;
    public DataString auth;
    public DataString subsource;
    public DataString channel;
    public DataString channelsrc;
    public DataString seedchan;
    public DataString location;
*/

/** List object used to store the DataObject data types of the declared station channel description.
*/
    Vector fields = new Vector(MAX_FIELDS);

/** Flag indicates whether to consider this object's data value changed.
*/
    boolean valueUpdate = false;

/** Flag indicates whether to consider this object NULL.
*/
    boolean valueNull = true;

/** Flag indicates whether to consider this object's data members changeable.
*/
    boolean valueMutable = true;

/** Default constructor creates new DataString data members that are set to empty string values.
* Default state flag settings are Update:false, Null:true, Mutable:true.
*/
    public DataStnChl () {
      for (int index = 0; index < MAX_FIELDS; index++) {
          fields.add(new DataString(""));
      }
    }

/** Copy constructor clones the input argument object data.
* Sets the data members to clones of the input object's data; sets state flags to the input object values.
*/
    public DataStnChl (DataStnChl x) {
      for (int index = 0; index < MAX_FIELDS; index++) {
          fields.add(((DataString) x.fields.get(index)).clone());
      }
      valueNull = x.valueNull;
      valueUpdate = x.valueUpdate;
      valueMutable = x.valueMutable;
    }

/** Constructs new data members from input string arguments.
* Both null and empty strings are set to an empty String value.
* Sets the state flags to Update:true, Null:false, Mutable:true.
* @see DataString#DataString()
*/
    public DataStnChl (String sta, String net, String auth, String subsource,
       String channel, String channelsrc, String seedchan, String location) {
      fields.add(new DataString(sta));
      fields.add(new DataString(net));
      fields.add(new DataString(auth));
      fields.add(new DataString(subsource));
      fields.add(new DataString(channel));
      fields.add(new DataString(channelsrc));
      fields.add(new DataString(seedchan));
      fields.add(new DataString(location));
      valueUpdate = true;
      valueNull = false;
    }

/** Constructs new data members from input string arguments.
* Invokes the complete string field constructor with null values for the missing fields.
* Sets the state flags to Update:true, Null:false, Mutable:true.
*/
    public DataStnChl (String sta, String net, String auth, String subsource,
       String channel) {
      this(sta, net, auth, subsource, channel, null, null, null);
      valueUpdate = true;
      valueNull = false;
    }

/** Constructs new data members from clones of the input DataString arguments.
* Null argument values invoke the DataString() (empty string) constructor for the respective data member.
* Sets the state flags to Update:true, Null:false, Mutable:true.
*/
    public DataStnChl (DataString sta, DataString net, DataString auth, DataString subsource,
       DataString channel, DataString channelsrc, DataString seedchan, DataString location) {
      if (sta == null) fields.add(new DataString());
      else fields.add(sta.clone());
      if (net == null) fields.add(new DataString());
      else fields.add(net.clone());
      if (auth == null) fields.add(new DataString());
      else fields.add(auth.clone());
      if (subsource == null) fields.add(new DataString());
      else fields.add(subsource.clone());
      if (channel == null) fields.add(new DataString());
      else fields.add(channel.clone());
      if (channelsrc == null) fields.add(new DataString());
      else fields.add(channelsrc.clone());
      if (seedchan == null) fields.add(new DataString());
      else fields.add(seedchan.clone());
      if (location == null) fields.add(new DataString());
      else fields.add(location.clone());
      valueUpdate = true;
      valueNull = false;
    }

/** Constructs new data members from clones of the input DataString arguments.
* Invokes the complete DataString constructor with null arguments for the missing fields.
* Sets the state flags to Update:true, Null:false, Mutable:true.
*/
    public DataStnChl (DataString sta, DataString net, DataString auth, DataString subsource,
       DataString channel) {
      this(sta, net, auth, subsource, channel, null, null, null);
      valueUpdate = true;
      valueNull = false;
    }

/** Constructs new data members by parsing a input string in which the data fields are delimited by
* a space, underline, tab, or comma.
* Invokes the default constructor.
* Parses the input string tokens replacing each of the defaults fields in order;
* Missing or null fields can be indicated by a delimited "*" character and are stored as empty strings.
* Throws IndexOutOfBounds exception if the number of token fields is greater than MAX_FIELDS.
* Sets the state flags to Update:true, Null:false, Mutable:true.
*/
    public DataStnChl (String scString) throws IndexOutOfBoundsException {
      this();
      StringTokenizer st = new StringTokenizer(scString, FIELD_DELIMITERS);
      if (st.countTokens() > MAX_FIELDS)
        throw new IndexOutOfBoundsException("DataStnChl stringToDataStnChl(string) string:" + scString);
      for (int index = 0; index < MAX_FIELDS; index++) {
          if (! st.hasMoreTokens()) break;
          try {
            String tmpString = st.nextToken();
            if (tmpString.equals(STAR_NULL_FIELD)) continue;
            else fields.set(index, new DataString(tmpString));
          }
          catch (NoSuchElementException ex) { break;}
      }
      valueUpdate = true;
      valueNull = false;
    }

/** Sets the update flags of all the DataString data members to the specified input argument value.
* Sets the update flag of this object instance to the input value.
* Returns a handle to this object instance.
*/
    public DataStnChl setUpdateAllValues(boolean value) {
      for (int index = 0; index < MAX_FIELDS; index++) {
          ((DataString) fields.get(index)).setUpdate(value);
      }
      valueUpdate = value;
      return this;
    }

/** Sets the null data flags of all the DataString data members to the specified input argument value.
* Sets the null flag of this object instance to input value.
* Returns a handle to this object instance.
*/
    public DataStnChl setNullAllValues(boolean value) {
      for (int index = 0; index < MAX_FIELDS; index++) {
          ((DataString) fields.get(index)).setNull(value);
      }
      valueNull = value;
      return this;
    }

/** Sets the mutability flags of all the DataString data members to the specified input argument value.
* Sets the mutability flag of this object instance to input value.
* Returns a handle to this object instance.
*/
    public DataStnChl setMutableAllValues(boolean value) {
      for (int index = 0; index < MAX_FIELDS; index++) {
          ((DataString) fields.get(index)).setMutable(value);
      }
      valueMutable = value;
      return this;
    }

/** Sets the update flag to the specified argument value. Determines data legitimacy in implemented class extensions.
* An input value of true == "set", false == "not set". Returns the handle (this) of the invoking class instance.
*/
    public DataStnChl setUpdate(boolean value) {
      this.valueUpdate = value;
      return this;
    }

/** Returns the boolean value of the data update flag for the invoking instance.
*/
    public boolean isUpdate() {
      return valueUpdate;
    }

/** Sets the null flag to the specified argument value. Determines whether the data value is considered undefined or NULL.
* An input value of true == "NULL", false == "not NULL". Returns the handle (this) of the invoking class instance.
*/
    public DataStnChl setNull(boolean value) {
      this.valueNull = value;
      return this;
    }

/** Returns the boolean value of the data null flag for the invoking instance.
*/
    public boolean isNull() {
      return valueNull;
    }

/** Sets the mutability flag to the specified argument value. Determines whether the data value can be modified.
* An input value of true == "mutable", false == "not mutable". Returns the handle (this) of the invoking class instance.
*/
    public DataStnChl setMutable(boolean value) {
      this.valueMutable = value;
      return this;
    }

/** Returns the boolean value of the data mutability flag for the invoking instance.
*/
    public boolean isMutable() {
      return valueMutable;
    }

/** Return the relative postion of named data member field in the station channel description.
* Returns -1 if no names matches the input argument value.
*/
    public int findFieldIndex(String name) {
      for (int index = 0; index < MAX_FIELDS; index++) {
        if (FIELD_NAMES[index].equals(name.trim().toUpperCase())) return index;
      }
      return -1;
    }

/** Returns the null status of the data field at the specified input position index.
*/
    public boolean isNullValue(int index) throws IndexOutOfBoundsException {
      if (index < 0 || index > MAX_FIELDS)
         throw new IndexOutOfBoundsException("DataStnChl isNullValue(int) index out of bounds:" + index);
      return ((DataState) fields.get(index)).isNull();
    }

/** Returns the null status of the data field with the name specified in the input argument.
*/
    public boolean isNullValue(String name) throws NoSuchFieldException {
      int index = findFieldIndex(name);
      if (index < 0) throw new NoSuchFieldException("DataStnChl isNullValue(name) unknown field name:" + name);
      return isNullValue(index);
    }

/** Returns the update status of the data field at the specified input position index.
*/
    public boolean isUpdateValue(int index) throws IndexOutOfBoundsException {
      if (index < 0 || index > MAX_FIELDS)
        throw new IndexOutOfBoundsException("DataStnChl isUpdateValue(int) field index out of bounds:" + index);
      return ((DataState) fields.get(index)).isUpdate();
    }

/** Returns the update status of the data field with the name specified in the input argument.
*/
    public boolean isUpdateValue(String name) throws NoSuchFieldException {
      int index = findFieldIndex(name);
      if (index < 0) throw new NoSuchFieldException("DataStnChl isUpdateValue(name) unknown field name:" + name);
      return isUpdateValue(index);
    }

/** Returns the mutability status of the data field at the specified input position index.
*/
    public boolean isMutableValue(int index) throws IndexOutOfBoundsException {
      if (index < 0 || index > MAX_FIELDS)
        throw new IndexOutOfBoundsException("DataStnChl isMutableValue(int) field index out of bounds:" + index);
      return ((DataState) fields.get(index)).isMutable();
    }

/** Returns the mutability status of the data field with the name specified in the input argument.
*/
    public boolean isMutableValue(String name) throws NoSuchFieldException {
      int index = findFieldIndex(name);
      if (index < 0) throw new NoSuchFieldException("DataStnChl isMutableValue(name) unknown field name:" + name);
      return isMutableValue(index);
    }

/** Returns the handle of the DataString object for the data field with the name specified in the input argument.
*/
    public DataObject getDataObject(String name) throws NoSuchFieldException {
        int index = findFieldIndex(name);
        if (index < 0) throw new NoSuchFieldException("DataStnChl getDataObject(String name) unknown field name:" + name);
        return getDataObject(index);
    }

/** Returns the handle of the DataString object for the data field at the specified input position index.
*/
    public DataObject getDataObject(int index) throws IndexOutOfBoundsException {
        if (index < 0 || index > fields.size())
                throw new IndexOutOfBoundsException("DataStnChl getDataObject(index) field index out of bounds:" + index);
        return (DataObject) fields.get(index);
    }

/** Returns the string value of the data field with the name specified in the input argument.
*/
    public String getStringValue(String name) throws NoSuchFieldException {
        int index = findFieldIndex(name);
        if (index < 0) throw new NoSuchFieldException("DataStnChl getStringValue(String name) unknown field name:" + name);
        return getStringValue(index);
    }

/** Returns the string value of the data field at the specified input position index.
*/
    public String getStringValue(int index) throws IndexOutOfBoundsException, NullPointerException, ClassCastException  {
        if (index < 0 || index > fields.size())
                throw new IndexOutOfBoundsException("DataStnChl getStringValue(index) field index out of bounds:" + index);
        if (fields.get(index) == null)
                throw new NullPointerException("DataStnChl getStringValue: field object null at index: " + index);
        String retVal = "";
        if (fields.get(index) instanceof DataObject) {
            retVal = ((DataObject) fields.get(index)).toString();
        }
        else throw new ClassCastException("DataStnChl getStringValue(index) fields[index] must be a DataObject class type: "
                                         + fields.get(index).getClass().getName());
        return retVal;
    }

/** Sets the string value of the data field with the name specified in the input argument to the input value argument.
* Does a no-op if isMutable() == false.
* Returns a handle to this object instance.
*/
    public DataStnChl setValue(String name, String value) throws NoSuchFieldException {
      if(! isMutable()) return this;
      int index = findFieldIndex(name);
      if (index < 0)
        throw new NoSuchFieldException("DataStnChl setValue(String name, String value) unknown field name:" + name);
      return setValue(index, value);
    }

/** Sets the string value of the data field at the specified input position index to the input value argument.
* Does a no-op if isMutable() == false.
* Returns a handle to this object instance.
*/
    public DataStnChl setValue(int index, String value) throws IndexOutOfBoundsException {
      if (! isMutable()) return this;
      if (index < 0 || index > fields.size())
          throw new IndexOutOfBoundsException("DataStnChl setValue(int index, String value) field index out of bounds:"
        + index);
      if (fields.get(index) == null) {
          try {
            fields.set(index, DATA_CLASSES[FIELD_CLASS_IDS[index]].newInstance());
          }
          catch (IllegalAccessException ex) {
            System.out.println("DataStnChl setValue(int,String): class or initializer not accessible." +
              DATA_CLASSES[FIELD_CLASS_IDS[index]]);
          }
          catch (InstantiationException ex) {
            System.out.println("DataStnChl setValue(int,String): cannot instantiate class check type." +
              DATA_CLASSES[FIELD_CLASS_IDS[index]]);
          }
      }
      ((DataObject) fields.get(index)).setValue(value);
      hashCache = null;
      return this;
    }

/** Sets the string value of the data field with the name specified in the input argument.
* Set value to the string value of the input Object argument (for example class of input object == DataString).
* Does a no-op if isMutable() == false.
* Returns a handle to this object instance.
*/
    public DataStnChl setValue(String name, Object value) throws NoSuchFieldException {
      if(! isMutable()) return this;
      int index = findFieldIndex(name);
      if (index < 0)
        throw new NoSuchFieldException("DataStnChl setValue(String name, Object value) unknown field name:" + name);
        return setValue(index, value);
      }

/** Sets the string value of the data field at the specified input position index.
* Set value to the string value of the input Object argument (for example class of input object == DataString).
* Does a no-op if isMutable() == false.
* Returns a handle to this object instance.
*/
    public DataStnChl setValue(int index, Object value) throws NullPointerException, IndexOutOfBoundsException {
      if(! isMutable()) return this;
      if (value == null) throw new NullPointerException("DataStnChl setValue(Object) argument null");
      if (index < 0 || index > fields.size())
          throw new IndexOutOfBoundsException("DataStnChl setValue(int index, Object value) field index out of bounds:"
    + index);
      if (fields.get(index) == null) {
        try {
          fields.set(index, DATA_CLASSES[FIELD_CLASS_IDS[index]].newInstance());
        }
        catch (IllegalAccessException ex) {
          System.out.println("DataStnChl setValue(int,String): class or initializer not accessible." +
            DATA_CLASSES[FIELD_CLASS_IDS[index]]);
        }
        catch (InstantiationException ex) {
              System.out.println("DataStnChl setValue(int,String): cannot instantiate class check type." +
            DATA_CLASSES[FIELD_CLASS_IDS[index]]);
        }
      }
      ((DataObject) fields.get(index)).setValue(value);
      hashCache = null;
      return this;
    }

/** Overrides the Object.clone() method.
* Invokes Object.clone(), does a deep copy of its data member objects.
* Returns a handle to the new object instance of this type.
*/
    public Object clone() {
      DataStnChl obj = null;
      try {
          obj = (DataStnChl) super.clone();
      }
      catch (CloneNotSupportedException ex) {
          System.out.println("Cloneable not implemented for class: " + this.getClass().getName());
          ex.printStackTrace();
      }

      for (int index = 0; index < MAX_FIELDS; index++) {
          obj.fields.set(index, ((DataObject) fields.get(index)).clone());
      }
      return obj;
    }

/** Returns true if all data values and state flags of this instance equal those of the input argument object.
* Returns false if input object is null or not an instanceof DataStnChl.
* The data field string comparison is case sensitive.
*/
    public boolean equals(Object x) {
      if (x == null || ! ( x instanceof DataStnChl) ) return false;
          //throw new ClassCastException("equals(object) argument must be a DataStnChl class type: " + x.getClass().getName());

      //  if (x == null || ! (x instanceof DataStnChl)) return false;
      for (int index = 0; index < MAX_FIELDS; index++) {
          if (! ((DataObject) fields.get(index)).equals((DataObject) ((DataStnChl) x).fields.get(index)) ) return false;
      }
      if (this.valueNull == ((DataStnChl) x).valueNull && this.valueUpdate == ((DataStnChl) x).valueUpdate &&
            this.valueMutable == ((DataStnChl) x).valueMutable) return true;
      return false;

    }

/** Returns true if STA, NET, and SEEDCHAN string values of this instance equal those of the input object.
* Returns false if input object is null or not an instanceof DataStnChl.
* No state flags are checked. The data field string comparison is case insensitive.
*/
    public boolean sameAs(Object x) {
        if (x == null || ! ( x instanceof DataStnChl) ) return false;
        //throw new ClassCastException("sameAs(object) argument must be a DataStnChl class type: " + x.getClass().getName());

        String seedchan = this.getStringValue(SEEDCHAN).toUpperCase();
        String channel = this.getStringValue(CHANNEL).toUpperCase();
        String testSeedchan = ((DataStnChl) x).getStringValue(SEEDCHAN).toUpperCase();
        String testChannel = ((DataStnChl) x).getStringValue(CHANNEL).toUpperCase();

        return  ( this.getStringValue(STA).equalsIgnoreCase( ((DataStnChl) x).getStringValue(STA)) &&
                  this.getStringValue(NET).equalsIgnoreCase( ((DataStnChl) x).getStringValue(NET)) &&
                  ( seedchan.equals(testSeedchan) || seedchan.equals(testChannel)  ||
                    channel.equals(testSeedchan)  || channel.equals(testChannel) ) );
    }

/** Returns true if "SEED" name of this instance is equivalent to the input object.
 * That is the net,sta,seedchan and location attributes are equivalent.
* Returns false if input object is null.
* The data field string comparison is case insensitive.
*/
    public boolean equalsNameIgnoreCase(ChannelIdIF id) {
        return (id == null) ?  false :
          (
            getStringValue(STA).equalsIgnoreCase(id.getSta())  &&
            getStringValue(SEEDCHAN).equalsIgnoreCase(id.getSta()) &&
            getStringValue(NET).equalsIgnoreCase(id.getSta())
            && getStringValue(LOCATION).equalsIgnoreCase(id.getSta())
          );
    }
    public boolean equalsName(ChannelIdIF id) {
        return (id == null) ?  false :
          (
            getStringValue(STA).equals(id.getSta())  &&
            getStringValue(SEEDCHAN).equals(id.getSta()) &&
            getStringValue(NET).equals(id.getSta())
            && getStringValue(LOCATION).equals(id.getSta())
          );
    }
    public boolean equalsIgnoreCase(Object x) {
      if (x == null || ! ( x instanceof DataStnChl) ) return false;
      //throw new ClassCastException("equalsIgnoreCase(object) argument must be a DataStnChl class type: " + x.getClass().getName());

      for (int index = 0 ; index < MAX_FIELDS; index++) {
          if (! this.getStringValue(index).equalsIgnoreCase( ((DataStnChl) x).getStringValue(index)) ) return false;
      }
      return true;
    }

/** Returns the value of this.toString().hashCode()
*/
    public int hashCode() {
      if (hashCache == null) hashCache = new Integer(toString().hashCode());
      return hashCache.intValue();
    }

/** Returns the result of comparing the strings generated by invoking toString() for this instance and the input object.
* Throws ClassCastException if input object is not an instanceof DataStnChl.
* A return of 0 == value equals, <0 == value less than, >0 == value greater than, the string value of the input argument.
*/
    public int compareTo(Object x) {
      if (x == null || ! ( x instanceof DataStnChl) )
         throw new ClassCastException("compareTo(object) argument must be a DataStnChl class type: "
             + x.getClass().getName());

      return this.toString().compareTo(((DataStnChl) x).toString());
    }

/** Converts the string values of all station channel data fields to uppercase.
* Does a no-op if isMutable == false.
* Returns a handle to this object instance.
*/
    public DataStnChl toUpperCase() {
      if(! isMutable()) return this;
      for (int index = 0 ; index < MAX_FIELDS; index++) {
          setValue(index, getStringValue(index).toUpperCase());
      }
      return this;
    }

/** Converts the string values of all station channel data fields to lowercase.
* Does a no-op if isMutable == false.
* Returns a handle to this object instance.
*/
    public DataStnChl toLowerCase() {
      if (! isMutable()) return this;
      for (int index = 0 ; index < MAX_FIELDS; index++) {
        setValue(index, getStringValue(index).toLowerCase());
      }
      return this;
    }

/** Returns a string of data field name and value pairs and for this object instance.
* Includes the station channel fields and the state flag fields.
* String is of the form:
* sta: "stavalue.toString()" net: "netvalue.toString()" ...  Null: isNull() Update: isUpdate() Mutable: isMutable()
*/
    public String classToString(){
      StringBuffer sb = new StringBuffer(512);
      sb.append("sta: \"").append(fields.get(STA).toString());
      sb.append("\" net: \"").append(fields.get(NET).toString());
      sb.append("\" auth: \"").append(fields.get(AUTH).toString());
      sb.append("\" subsrc: \"").append(fields.get(SUBSOURCE).toString());
      sb.append("\" chl: \"").append(fields.get(CHANNEL).toString());
      sb.append("\" chlsrc: \"").append(fields.get(CHANNELSRC).toString());
      sb.append("\" seedchl: \"").append(fields.get(SEEDCHAN).toString());
      sb.append("\" loc: \"").append(fields.get(LOCATION).toString());
      sb.append("\" Null: ").append(valueNull);
      sb.append(" Update: ").append(valueUpdate);
      sb.append(" Mutable: ").append(valueMutable);
      return sb.toString();
    }

/** Returns the concatenated string values of only the STA, AUTH, SUBSOURCE, SEEDCHAN, and LOCATION fields.
* The fields are delimited by _ and empty fields are represented by an "*" character.
* If the internal data member object is null,  a null pointer exception is thrown; this is an error.
*/
    public String toStnChlString() {
      StringBuffer sb = new StringBuffer(64);
      String delimiter = "_";
      if (fields.get(STA).toString().length() > 0) sb.append(fields.get(STA).toString());
      else sb.append("*");
      if (fields.get(AUTH).toString().length() > 0) sb.append(delimiter).append(fields.get(AUTH).toString());
      else sb.append(delimiter).append("*");
      if (fields.get(SUBSOURCE).toString().length() > 0) sb.append(delimiter).append(fields.get(SUBSOURCE).toString());
      else sb.append(delimiter).append("*");
      if (fields.get(SEEDCHAN).toString().length() > 0) sb.append(delimiter).append(fields.get(SEEDCHAN).toString());
      else sb.append(delimiter).append("*");
      if (fields.get(LOCATION).toString().length() > 0) sb.append(delimiter).append(fields.get(LOCATION).toString());
      else sb.append(delimiter).append("*");
      return sb.toString();
    }

/** Returns the concatenated string values of only the STA, AUTH, and SEEDCHAN fields.
* The fields are delimited by _ and empty fields are represented by an "*" character.
* If the internal data member object is null,  a null pointer exception is thrown; this is an error.
*/
    public String toAbbrvStnChlString() {
      StringBuffer sb = new StringBuffer(64);
      String delimiter = "_";
      if (fields.get(STA).toString().length() > 0) sb.append(fields.get(STA).toString());
      else sb.append("*");
      if (fields.get(AUTH).toString().length() > 0) sb.append(delimiter).append(fields.get(AUTH).toString());
      else sb.append(delimiter).append("*");
      if (fields.get(SEEDCHAN).toString().length() > 0) sb.append(delimiter).append(fields.get(SEEDCHAN).toString());
      else sb.append(delimiter).append("*");
      return sb.toString();
    }

/** Returns the concatenated string values of all the station channel data fields.
* The fields are delimited by _ and empty fields are represented by an "*" character.
* If the internal data member object is null,  a null pointer exception is thrown; this is an error.
*/
    public String toString() {
      return toDelimitedNameString('_');
    }

    public String toDelimitedSeedNameString(char delimiter) {
      return toDelimitedSeedNameString(String.valueOf(delimiter));
    }
    public String toDelimitedSeedNameString(String delimiter) {
      StringBuffer sb = new StringBuffer(64);
      if (fields.get(NET).toString().length() > 0) sb.append(delimiter).append(fields.get(NET).toString());
      else sb.append(delimiter).append("*");
      if (fields.get(STA).toString().length() > 0) sb.append(fields.get(STA).toString());
      else sb.append("*");
      if (fields.get(SEEDCHAN).toString().length() > 0) sb.append(delimiter).append(fields.get(SEEDCHAN).toString());
      else sb.append(delimiter).append("*");
      if (fields.get(LOCATION).toString().length() > 0) sb.append(delimiter).append(fields.get(LOCATION).toString());
      else sb.append(delimiter).append("*");

      return sb.toString();
    }
    public String toDelimitedNameString(char delimiter) {
      return toDelimitedNameString(String.valueOf(delimiter));
    }
    public String toDelimitedNameString(String delimiter) {
      StringBuffer sb = new StringBuffer(64);
      if (fields.get(STA).toString().length() > 0) sb.append(fields.get(STA).toString());
      else sb.append("*");
      if (fields.get(NET).toString().length() > 0) sb.append(delimiter).append(fields.get(NET).toString());
      else sb.append(delimiter).append("*");
      if (fields.get(AUTH).toString().length() > 0) sb.append(delimiter).append(fields.get(AUTH).toString());
      else sb.append(delimiter).append("*");
      if (fields.get(SUBSOURCE).toString().length() > 0) sb.append(delimiter).append(fields.get(SUBSOURCE).toString());
      else sb.append(delimiter).append("*");
      if (fields.get(CHANNEL).toString().length() > 0) sb.append(delimiter).append(fields.get(CHANNEL).toString());
      else sb.append(delimiter).append("*");
      if (fields.get(CHANNELSRC).toString().length() > 0) sb.append(delimiter).append(fields.get(CHANNELSRC).toString());
      else sb.append(delimiter).append("*");
      if (fields.get(SEEDCHAN).toString().length() > 0) sb.append(delimiter).append(fields.get(SEEDCHAN).toString());
      else sb.append(delimiter).append("*");
      if (fields.get(LOCATION).toString().length() > 0) sb.append(delimiter).append(fields.get(LOCATION).toString());
      else sb.append(delimiter).append("*");

      return sb.toString();
    }

/** Returns a concatenated string of updated (isUpdate == true) station channel field name and value pairs.
* The form is that of an SQL WHERE clause condition: sta = 'stavalue' AND net = 'netvalue' AND ...
* The WHERE is not included at the beginning of the string.
* Only those fields where isUpdate() == true are used to build the string.
* Empty string values are represented by SQL NULL.
*/
    public String toStringSQLWhereCondition() {
      StringBuffer retVal = new StringBuffer(512);
      retVal.append(" ");
      for (int index = 0; index < MAX_FIELDS; index++) {
        if ( ((DataState) fields.get(index)).isUpdate() ) {
          retVal.append(FIELD_NAMES[index]).append(" = ");
          retVal.append(((DataObject) fields.get(index)).toStringSQL()).append(" AND ");
        }
      }
      if (retVal.length() >= 5) retVal.replace(retVal.length()-5, retVal.length(),  " ");
      return retVal.toString();
    }

/** Returns a concatenated string of updated (isUpdate == true) station channel field values.
*  The form is that of an SQL INSERT statement value expression: " 'stavalue', 'netvalue' ..."
* Only those fields where isUpdate() == true are used to build the string.
* Empty string values are represented by SQL NULL.
*/
    public String toStringSQLInsertValues() {
      StringBuffer retVal = new StringBuffer(512);
      retVal.append(" ");
      for (int index = 0; index < MAX_FIELDS; index++) {
        if ( ((DataState) fields.get(index)).isUpdate() )
            retVal.append(((DataObject) fields.get(index)).toStringSQL()).append(", ");
      }
      if (retVal.length() >= 2) retVal.replace(retVal.length()-2, retVal.length(),  " ");
      return retVal.toString();
    }

/** Returns a concatenated string of updated (isUpdate == true) station channel field name and value pairs.
* The form is that of an SQL UPDATE SET statement: sta = 'stavalue', net = 'netvalue', ...
* Only those fields where isUpdate() == true are used to build the string.
* Empty string values are represented by SQL NULL.
*/
    public String toStringSQLUpdateSet() {
      StringBuffer retVal = new StringBuffer(512);
      retVal.append(" ");
      for (int index = 0; index < MAX_FIELDS; index++) {
        if ( ((DataState) fields.get(index)).isUpdate() ) {
          retVal.append(FIELD_NAMES[index]).append(" = ");
          retVal.append(((DataObject) fields.get(index)).toStringSQL()).append(", ");
        }
      }
      if (retVal.length() >= 2) retVal.replace(retVal.length()-2, retVal.length(),  " ");
      return retVal.toString();
    }

/** Returns a DataStnChl object resulting from parsing a string composed of station channel tokens.
* Invokes the default constructor.
* Parses the input string tokens replacing each of the default station channel field in order;
* Data fields are delimited by a space, underline, tab, or comma.
* Missing or null fields can be indicated by a delimited "*" character and are stored as the default empty strings.
* Throws an IndexOutOfBounds exception if the number of token fields is greater than MAX_FIELDS.
* Sets the object state flags to Update:true, Null:false, Mutable:true.
*/
    static public DataStnChl stringToDataStnChl(String scString) throws IndexOutOfBoundsException {
      DataStnChl dsc = new DataStnChl();
      StringTokenizer st = new StringTokenizer(scString, FIELD_DELIMITERS);
      if (st.countTokens() > MAX_FIELDS)
          throw new IndexOutOfBoundsException("DataStnChl stringToDataStnChl(string) string:" + scString);
      for (int index = 0; index < MAX_FIELDS; index++) {
        if (! st.hasMoreTokens()) break;
        try {
          String tmpString = st.nextToken();
          if (tmpString.equals(STAR_NULL_FIELD)) continue;
          else dsc.fields.set(index, new DataString(tmpString));
        }
        catch (NoSuchElementException ex) { break;}
      }
      dsc.valueUpdate = true;
      dsc.valueNull = false;
      return dsc;
    }

/**
* Sets the input ChannelIdIF instance's data member values to the equivalents of this instance.
* Returns the input ChannelId parameter instance reference.
*/
    public ChannelIdIF parseToChannelId(ChannelIdIF chanId) {
        chanId.setSta(getSta());
        chanId.setNet(getNet());
        chanId.setSeedchan(getSeedchan());
        chanId.setLocation(getLocation());
        chanId.setChannel(getChannel());
        chanId.setChannelsrc(getChannelsrc());
        if (chanId instanceof AuthChannelIdIF) {
            ((AuthChannelIdIF) chanId).setAuth(getAuth());
            ((AuthChannelIdIF) chanId).setSubsource(getSubsource());
        }
        return chanId;
    }

/**
* Sets this instance's data member field values, if object isMutable() == true, to those of the input ChannelId parameter.
* Returns reference to this DataStnChl instance.
*/
    public DataStnChl setValues(ChannelIdIF chanId) {
        if (! isMutable()) return this;
        setSta(chanId.getSta());
        setNet(chanId.getNet());
        setSeedchan(chanId.getSeedchan());
        setLocation(chanId.getLocation());
        setChannel(chanId.getChannel());
        setChannelsrc(chanId.getChannelsrc());
        if (chanId instanceof AuthChannelIdIF) {
            setAuth(((AuthChannelIdIF) chanId).getAuth());
            setSubsource(((AuthChannelIdIF) chanId).getSubsource());
        }
        else {
            setAuth(null);
            setSubsource(null);

        }
        return this;
    }

// Implement AuthChannelIdIF
    public String getSta() {
        return getStringValue(STA);
    }
    public String getNet() {
        return getStringValue(NET);
    }
    public String getSeedchan() {
        return getStringValue(SEEDCHAN);
    }
    public String getLocation() {
        return getStringValue(LOCATION);
    }
    public String getChannel() {
        return getStringValue(CHANNEL);
    }
    public String getChannelsrc() {
        return getStringValue(CHANNELSRC);
    }
    public String getAuth() {
        return getStringValue(AUTH);
    }
    public String getSubsource() {
        return getStringValue(SUBSOURCE);
    }

    public void setSta(String sta) {
        setValue(STA, sta);
    }
    public void setNet(String net) {
        setValue(NET, net);
    }
    public void setSeedchan(String seedchan) {
        setValue(SEEDCHAN, seedchan);
    }
    public void setLocation(String location) {
        setValue(LOCATION, location);
    }
    public void setChannel(String channel) {
        setValue(CHANNEL, channel);
    }
    public void setChannelsrc(String channelsrc) {
        setValue(CHANNELSRC, channelsrc);
    }
    public void setAuth(String auth) {
        setValue(AUTH, auth);
    }
    public void setSubsource(String subsource) {
        setValue(SUBSOURCE, subsource);
    }
}
