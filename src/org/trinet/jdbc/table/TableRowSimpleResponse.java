package org.trinet.jdbc.table;
import org.trinet.jdbc.datatypes.DataClassIds;
/** Interface of static data constants defining the named table.
* @see org.trinet.jasi.SimpleResponse
*/
public interface TableRowSimpleResponse extends DataClassIds {

/** Name of schema database table represented by this class.
*/
    public static final String DB_TABLE_NAME =  "SIMPLE_RESPONSE";

/** Number of column data fields in a table row.
*/
    public static final int MAX_FIELDS =  16;

/** Id sequence name for primary key column
*/
    public static String SEQUENCE_NAME = "";


/**  SIMPLE_RESPONSE table "net" column data object offset in collection stored by implementing class.
*/
    public static final int NET = 0;

/**  SIMPLE_RESPONSE table "sta" column data object offset in collection stored by implementing class.
*/
    public static final int STA = 1;

/**  SIMPLE_RESPONSE table "seedchan" column data object offset in collection stored by implementing class.
*/
    public static final int SEEDCHAN = 2;

/**  SIMPLE_RESPONSE table "channel" column data object offset in collection stored by implementing class.
*/
    public static final int CHANNEL = 3;

/**  SIMPLE_RESPONSE table "channelsrc" column data object offset in collection stored by implementing class.
*/
    public static final int CHANNELSRC = 4;

/**  SIMPLE_RESPONSE table "location" column data object offset in collection stored by implementing class.
*/
    public static final int LOCATION = 5;

/**  SIMPLE_RESPONSE table "natural_frequency" column data object offset in collection stored by implementing class.
*/
    public static final int NATURAL_FREQUENCY = 6;

/**  SIMPLE_RESPONSE table "damping_constant" column data object offset in collection stored by implementing class.
*/
    public static final int DAMPING_CONSTANT = 7;

/**  SIMPLE_RESPONSE table "gain" column data object offset in collection stored by implementing class.
*/
    public static final int GAIN = 8;

/**  SIMPLE_RESPONSE table "gain_units" column data object offset in collection stored by implementing class.
*/
    public static final int GAIN_UNITS = 9;

/**  SIMPLE_RESPONSE table "low_freq_corner" column data object offset in collection stored by implementing class.
*/
    public static final int LOW_FREQ_CORNER = 10;

/**  SIMPLE_RESPONSE table "high_freq_corner" column data object offset in collection stored by implementing class.
*/
    public static final int HIGH_FREQ_CORNER = 11;

/**  SIMPLE_RESPONSE table "dlogsens" column data object offset in collection stored by implementing class.
*/
    public static final int DLOGSENS = 12;

/**  SIMPLE_RESPONSE table "ondate" column data object offset in collection stored by implementing class.
*/
    public static final int ONDATE = 13;

/**  SIMPLE_RESPONSE table "offdate" column data object offset in collection stored by implementing class.
*/
    public static final int OFFDATE = 14;

/**  SIMPLE_RESPONSE table "lddate" column data object offset in collection stored by implementing class.
*/
    public static final int LDDATE = 15;

/** String of know column names delimited by ",".
*/
    public static final String COLUMN_NAMES =
   "NET,STA,SEEDCHAN,CHANNEL,CHANNELSRC,LOCATION,NATURAL_FREQUENCY,DAMPING_CONSTANT,GAIN,GAIN_UNITS,LOW_FREQ_CORNER,HIGH_FREQ_CORNER,DLOGSENS,ONDATE,OFFDATE,LDDATE";

/** String of table qualified column names delimited by ",".
*/
    public static final String QUALIFIED_COLUMN_NAMES = 
    "SIMPLE_RESPONSE.NET,SIMPLE_RESPONSE.STA,SIMPLE_RESPONSE.SEEDCHAN,SIMPLE_RESPONSE.CHANNEL,SIMPLE_RESPONSE.CHANNELSRC,SIMPLE_RESPONSE.LOCATION,SIMPLE_RESPONSE.NATURAL_FREQUENCY,SIMPLE_RESPONSE.DAMPING_CONSTANT,SIMPLE_RESPONSE.GAIN,SIMPLE_RESPONSE.GAIN_UNITS,SIMPLE_RESPONSE.LOW_FREQ_CORNER,SIMPLE_RESPONSE.HIGH_FREQ_CORNER,SIMPLE_RESPONSE.DLOGSENS,SIMPLE_RESPONSE.ONDATE,SIMPLE_RESPONSE.OFFDATE,SIMPLE_RESPONSE.LDDATE";

/**  Table column data field names.
*/
    public static final String [] FIELD_NAMES  = {
	"NET", "STA", "SEEDCHAN", "CHANNEL", "CHANNELSRC", 
	"LOCATION", "NATURAL_FREQUENCY","DAMPING_CONSTANT", "GAIN", "GAIN_UNITS",
        "LOW_FREQ_CORNER", "HIGH_FREQ_CORNER", "DLOGSENS", "ONDATE", "OFFDATE",
        "LDDATE" 
    };

/** Nullable table column field.
*/
    public static final boolean [] FIELD_NULLS = {
	false, false, false, true, true, 
	false, true, true, true, true, 
	true, true, true, false, true, 
	true
    };

/**  Table column data field object class identifiers.
* @see org.trinet.jdbc.datatypes.DataClassIds
* @see org.trinet.jdbc.datatypes.DataClasses
*/
    public static final int [] FIELD_CLASS_IDS = {
	DATASTRING, DATASTRING, DATASTRING, DATASTRING, DATASTRING, 
	DATASTRING, DATADOUBLE, DATADOUBLE, DATADOUBLE, DATASTRING, 
	DATADOUBLE, DATADOUBLE, DATADOUBLE, DATADATE, DATADATE, 
	DATADATE
    };

/**  Column indices of primary key table columns.
*/
    public static final int [] KEY_COLUMNS = {0, 1, 2, 5, 13};

/**  Number of decimal fraction digits in table column data fields.
*/
    public static final int [] FIELD_DECIMAL_DIGITS = {
	0, 0, 0, 0, 0,
        0, 3, 3, 0, 0,
        3, 3, 8, 0, 0,
        0 
    };

/** Numeric sizes (width) of the table column data fields.
*/
    public static final int [] FIELD_SIZES = {
	8, 6, 3, 3, 8,
        2, 6, 6, 10, 10,
        6, 6, 9, 10, 10,
        10
    };

/** Default table column field values.
*/
    public static final String [] FIELD_DEFAULTS = {
	null, null, null, null, null,
        "'01' ", null, null, null, null, 
	null, null, null, null, null,
        "(SYS_EXTRACT_UTC(CURRENT_TIMESTAMP))" 
    };
}
