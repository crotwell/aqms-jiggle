package org.trinet.jdbc.table;
import java.util.Date;
import org.trinet.jdbc.*;
import org.trinet.util.EpochTime;

public abstract class ResponseStnTableRow extends DataTableRow {

    public ResponseStnTableRow (String tableName, String sequenceName, int maxFields, int[] keyColumnIndex,
                String [] fieldNames, boolean [] fieldNulls, int [] fieldClassIds ) {
        super(tableName, sequenceName, maxFields, keyColumnIndex, fieldNames, fieldNulls, fieldClassIds);
    }

/** Overrides DataTableRow.clone()
*/
    public Object clone() {
        ResponseStnTableRow obj = null;
        obj = (ResponseStnTableRow) super.clone();
        return obj;
    }

/** Returns table row count.
*/
    public int getRowCount() {
	return ExecuteSQL.getRowCount(connDB, getTableName());
    }

/** Returns table row count of the active stations for the specified date.
* Input date string must be of the form 'yyyy-MM-dd HH:mm:ss'.
* If date == null the date SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) function is used for the active date.
*/
    public int getRowCountByDate(String date) {
	return getRowCountByDate(EpochTime.stringToDate(date,"yyyy-MM-dd HH:mm:ss")); // DATE nominal GMT in db -aww 2008/02/12
    }

/** Returns table row count of the active stations for the specified date.
* If date == null the date SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) function is used for the active date.
*/
    public int getRowCountByDate(java.util.Date date) { // DATE nominal GMT in db -aww 2008/02/12

        String datestr = (date != null) ? StringSQL.toDATE(date) : "SYS_EXTRACT_UTC(CURRENT_TIMESTAMP)";
        String whereString = " WHERE (OFFDATE > " + datestr + " OR OFFDATE = NULL) AND ONDATE <= " + datestr;

	return ExecuteSQL.getRowCount(connDB, getTableName(), "*", whereString);
    }

/** Returns table row count corresponding to the specified date and net.
* Input date string must be of the form 'yyyy-MM-dd HH:mm:ss'.
* If the input date == null, the date returned by the SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) function is used.
*/
    public int getRowCountByDate(String date, String net) {
	return getRowCountByDate(EpochTime.stringToDate(date,"yyyy-MM-dd HH:mm:ss"), net); // DATE nominal GMT in db -aww 2008/02/12
    }

/** Returns table row count corresponding to the specified date and net.
* If the input date == null, the date returned by the SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) function is used.
*/
    public int getRowCountByDate(java.util.Date date, String net) { // DATE nominal GMT in db -aww 2008/02/12
	String whereString = "WHERE NET = " + StringSQL.valueOf(net);

        String datestr = (date != null) ? StringSQL.toDATE(date) : "SYS_EXTRACT_UTC(CURRENT_TIMESTAMP)";
        whereString += " AND (OFFDATE > " + datestr + " OR OFFDATE = NULL) AND ONDATE <= " + datestr;

	return ExecuteSQL.getRowCount(connDB, getTableName(), "*", whereString);
    }

/** Returns table row count corresponding to the specified date, net, and station.
* Input date string must be of the form 'yyyy-MM-dd HH:mm:ss'.
* If the input date == null, the date returned by the SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) function is used.
*/
    public int getRowCountByDate(String date, String net, String sta) {
	return getRowCountByDate(EpochTime.stringToDate(date,"yyyy-MM-dd HH:mm:ss"), net, sta); // DATE nominal GMT in db -aww 2008/02/12
    }

/** Returns table row count corresponding to the specified date, net, and station.
* If the input date == null, the date returned by the SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) function is used.
*/
    public int getRowCountByDate(java.util.Date date, String net, String sta) { // DATE nominal GMT in db -aww 2008/02/12
	String whereString = "WHERE NET = " + StringSQL.valueOf(net) +
				" AND STA = " + StringSQL.valueOf(sta);

        String datestr = (date != null) ? StringSQL.toDATE(date) : "SYS_EXTRACT_UTC(CURRENT_TIMESTAMP)";
        whereString += " AND (OFFDATE > " + datestr + " OR OFFDATE = NULL) AND ONDATE <= " + datestr;

	return ExecuteSQL.getRowCount(connDB, getTableName(), "*", whereString);
    }

/** Returns an array where each element contains the data from a single table row parsed from an SQL query
* WHERE the station was active on the specified input date, network, and station.
* Input date string must be of the form 'yyyy-MM-dd HH:mm:ss'.
* If the input date == null, the date returned by the SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) function is used.
* A return value of null indicates no data or an error condition.
*/
    public Object getRowsByDate(String date, String net, String sta) {
	return getRowsByDate(EpochTime.stringToDate(date,"yyyy-MM-dd HH:mm:ss"), net, sta); // DATE nominal GMT in db -aww 2008/02/12
    }

/** Returns an array where each element contains the data from a single table row parsed from an SQL query
* WHERE the station was active on the specified input date, network, and station.
* If the input date == null, the date returned by the SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) function is used.
* A return value of null indicates no data or an error condition.
*/
    public Object getRowsByDate(java.util.Date date, String net, String sta) { // DATE nominal GMT in db -aww 2008/02/12
	String whereString = "WHERE NET = " + StringSQL.valueOf(net) +
				" AND STA = " + StringSQL.valueOf(sta) ;

        String datestr = (date != null) ? StringSQL.toDATE(date) : "SYS_EXTRACT_UTC(CURRENT_TIMESTAMP)";
        whereString += " AND (OFFDATE > " + datestr + " OR OFFDATE = NULL) AND ONDATE <= " + datestr;

	return getRowsEquals(whereString);
    }

/** Returns an array where each element contains the data from a single table row parsed from an SQL query
* WHERE the station was active on the specified input date and network.
* Input date string must be of the form 'yyyy-MM-dd HH:mm:ss'.
* If the input date == null, the date returned by the SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) function is used.
* A return value of null indicates no data or an error condition.
*/
    public Object getRowsByDate(String date, String net) {
	return getRowsByDate(EpochTime.stringToDate(date,"yyyy-MM-dd HH:mm:ss"), net); // DATE nominal GMT in db -aww 2008/02/12
    }

/** Returns an array where each element contains the data from a single table row parsed from an SQL query
* WHERE the station was active on the specified input date and network.
* If the input date == null, the date returned by the SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) function is used.
* A return value of null indicates no data or an error condition.
*/
    public Object getRowsByDate(java.util.Date date, String net) { // DATE nominal GMT in db -aww 2008/02/12
	String whereString = "WHERE NET = " + StringSQL.valueOf(net) ;

        String datestr = (date != null) ? StringSQL.toDATE(date) : "SYS_EXTRACT_UTC(CURRENT_TIMESTAMP)";
        whereString += " AND (OFFDATE > " + datestr + " OR OFFDATE = NULL) AND ONDATE <= " + datestr;

	return getRowsEquals(whereString);
    }

/** Returns an array where each element contains the data from a single table row parsed from an SQL query
* WHERE the station was active on the specified input date.
* Input date string must be of the form 'yyyy-MM-dd HH:mm:ss'.
* If the input date == null, the date returned by the SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) function is used.
* A return value of null indicates no data or an error condition.
*/
    public Object getRowsByDate(String date) {
	return getRowsByDate(EpochTime.stringToDate(date,"yyyy-MM-dd HH:mm:ss")); // DATE nominal GMT in db -aww 2008/02/12
    }

/** Returns an array where each element contains the data from a single table row parsed from an SQL query
* WHERE the station was active on the specified input date.
* If the input date == null, the date returned by the SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) function is used.
* A return value of null indicates no data or an error condition.
*/
    public Object getRowsByDate(java.util.Date date) { // DATE nominal GMT in db -aww 2008/02/12
        String datestr = (date != null) ? StringSQL.toDATE(date) : "SYS_EXTRACT_UTC(CURRENT_TIMESTAMP)";
        String whereString = " WHERE (OFFDATE > " + datestr + " OR OFFDATE = NULL) AND ONDATE <= " + datestr;

	return getRowsEquals(whereString);
    }

}

