package org.trinet.jdbc.table;
import org.trinet.jdbc.datatypes.DataClassIds;
/** Interface of static data constants defining the named table.
* @see Remark
*/
public interface TableRowRemark extends DataClassIds {

/** Name of schema database table represented by this class.
*/
    public static final String DB_TABLE_NAME =  "REMARK";

/** Number of column data fields in a table row.
*/
    public static final int MAX_FIELDS =  4;

/** Id sequence name for primary key column
*/
    public static String SEQUENCE_NAME = "COMMSEQ";


/**  REMARK table "commid" column data object offset in collection stored by implementing class.
*/
    public static final int COMMID = 0;

/**  REMARK table "lineno" column data object offset in collection stored by implementing class.
*/
    public static final int LINENO = 1;

/**  REMARK table "remark" column data object offset in collection stored by implementing class.
*/
    public static final int REMARK = 2;

/**  REMARK table "lddate" column data object offset in collection stored by implementing class.
*/
    public static final int LDDATE = 3;
/** String of know column names delimited by ",".
*/
    public static final String COLUMN_NAMES =
   "COMMID,LINENO,REMARK,LDDATE";

/** String of table qualified column names delimited by ",".
*/
    public static final String QUALIFIED_COLUMN_NAMES = 
    "REMARK.COMMID,REMARK.LINENO,REMARK.REMARK,REMARK.LDDATE";

/**  Table column data field names.
*/
    public static final String [] FIELD_NAMES  = {
	"COMMID", "LINENO", "REMARK", "LDDATE"
    };

/** Nullable table column field.
*/
    public static final boolean [] FIELD_NULLS = {
	false, false, true, true
    };

/**  Table column data field object class identifiers.
* @see org.trinet.jdbc.datatypes.DataClassIds
* @see org.trinet.jdbc.datatypes.DataClasses
*/
    public static final int [] FIELD_CLASS_IDS = {
	DATALONG, DATALONG, DATASTRING, DATADATE
    };

/**  Column indices of primary key table columns.
*/
    public static final int [] KEY_COLUMNS = {0, 1};

/**  Number of decimal fraction digits in table column data fields.
*/
    public static final int [] FIELD_DECIMAL_DIGITS = {
	0, 0, 0, 0
    };

/** Numeric sizes (width) of the table column data fields.
*/
    public static final int [] FIELD_SIZES = {
	15, 15, 80, 7
    };

/** Default table column field values.
*/
    public static final String [] FIELD_DEFAULTS = {
	null, null, null, "(SYS_EXTRACT_UTC(CURRENT_TIMESTAMP))"
    };
}
