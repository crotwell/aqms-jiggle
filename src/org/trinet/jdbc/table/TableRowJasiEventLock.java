package org.trinet.jdbc.table;

import org.trinet.jdbc.datatypes.DataClassIds;
/** Interface of static data constants defining the named table.
* @see JasiEventLock
*/
public interface TableRowJasiEventLock extends DataClassIds {

/** Name of schema database table represented by this class.
*/
    public static final String DB_TABLE_NAME =  "JASIEVENTLOCK";

/** Number of column data fields in a table row.
*/
    public static final int MAX_FIELDS =  5;

/** Id sequence name for primary key column
*/
    public static String SEQUENCE_NAME = "";


/**  JASIEVENTLOCK table "evid" column data object offset in collection stored
by implementing class.  */
    public static final int EVID = 0;

/**  JASIEVENTLOCK table "hostname" column data object offset in collection
stored by implementing class.  */
    public static final int HOSTNAME = 1;

/**  JASIEVENTLOCK table "application" column data object offset in collection
stored by implementing class.  */
    public static final int APPLICATION = 2;

/**  JASIEVENTLOCK table "username" column data object offset in collection
stored by implementing class.  */
    public static final int USERNAME = 3;

/**  JASIEVENTLOCK table "lddate" column data object offset in collection stored
by implementing class.  */
    public static final int LDDATE = 4;
/** String of know column names delimited by ",".
*/
    public static final String COLUMN_NAMES =
   "EVID,HOSTNAME,APPLICATION,USERNAME,LDDATE";

/** String of table qualified column names delimited by ",".
*/
    public static final String QUALIFIED_COLUMN_NAMES = 
    "JASIEVENTLOCK.EVID,JASIEVENTLOCK.HOSTNAME,JASIEVENTLOCK.APPLICATION,JASIEVENTLOCK.USERNAME,JASIEVENTLOCK.LDDATE";

/**  Table column data field names.
*/
    public static final String [] FIELD_NAMES  = {
	"EVID", "HOSTNAME", "APPLICATION", "USERNAME", "LDDATE"
    };

/** Nullable table column field.
*/
    public static final boolean [] FIELD_NULLS = {
	false, true, true, true, true
    };

/**  Table column data field object class identifiers.
* @see org.trinet.jdbc.datatypes.DataClassIds
* @see org.trinet.jdbc.datatypes.DataClasses
*/
    public static final int [] FIELD_CLASS_IDS = {
	DATALONG, DATASTRING, DATASTRING, DATASTRING, DATADATE
    };

/**  Column indices of primary key table columns.
*/
    public static final int [] KEY_COLUMNS = {0};

/**  Number of decimal fraction digits in table column data fields.
*/
    public static final int [] FIELD_DECIMAL_DIGITS = {
	0, 0, 0, 0, 0
    };

/** Numeric sizes (width) of the table column data fields.
*/
    public static final int [] FIELD_SIZES = {
	22, 40, 20, 20, 7
    };

/** Default table column field values.
*/
    public static final String [] FIELD_DEFAULTS = {
	null, null, null, null, "(SYS_EXTRACT_UTC(CURRENT_TIMESTAMP))"
    };
}
