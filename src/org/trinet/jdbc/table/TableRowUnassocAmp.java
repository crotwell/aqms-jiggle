package org.trinet.jdbc.table;
import org.trinet.jdbc.datatypes.DataClassIds;
/** Interface of static data constants defining the named table.
* @see Amp
* @see TableRowAmp
*/
public interface TableRowUnassocAmp extends DataClassIds { 
//public interface TableRowUnassocAmp extends TableRowGenericAmp {
//
    public static final int MAX_FIELDS =  28;   // 0 - 27

/** Name of schema database table represented by this class.  */
    public static final String DB_TABLE_NAME =  "UNASSOCAMP";


    /** Id sequence name for primary key column */
    public static String SEQUENCE_NAME = "AMPSEQ";

    /**  AMP table "ampid" column data object offset in collection stored by implementing class.  */
    public static final int AMPID = 0;

    /**  AMP table "commid" column data object offset in collection stored by implementing class.  */
    public static final int COMMID = 1;

    /**  AMP table "datetime" column data object offset in collection stored by implementing class.  */
    public static final int DATETIME = 2;

    /**  AMP table "sta" column data object offset in collection stored by implementing class.  */
    public static final int STA = 3;

    /**  AMP table "net" column data object offset in collection stored by implementing class.  */
    public static final int NET = 4;

    /**  AMP table "auth" column data object offset in collection stored by implementing class.  */
    public static final int AUTH = 5;

    /**  AMP table "subsource" column data object offset in collection stored by implementing class.  */
    public static final int SUBSOURCE = 6;

    /**  AMP table "channel" column data object offset in collection stored by implementing class.  */
    public static final int CHANNEL = 7;

    /**  AMP table "channelsrc" column data object offset in collection stored by implementing class.  */
    public static final int CHANNELSRC = 8;

    /**  AMP table "seedchan" column data object offset in collection stored by implementing class.  */
    public static final int SEEDCHAN = 9;

    /**  AMP table "location" column data object offset in collection stored by implementing class.  */
    public static final int LOCATION = 10;

    /**  AMP table "iphase" column data object offset in collection stored by implementing class.  */
    public static final int IPHASE = 11;

    /**  AMP table "amplitude" column data object offset in collection stored by implementing class.  */
    public static final int AMPLITUDE = 12;

    /**  AMP table "amptype" column data object offset in collection stored by implementing class.  */
    public static final int AMPTYPE = 13;

    /**  AMP table "units" column data object offset in collection stored by implementing class.  */
    public static final int UNITS = 14;

    /**  AMP table "ampmeas" column data object offset in collection stored by implementing class.  */
    public static final int AMPMEAS = 15;

    /**  AMP table "eramp" column data object offset in collection stored by implementing class.  */
    public static final int ERAMP = 16;

    /**  AMP table "flagamp" column data object offset in collection stored by implementing class.  */
    public static final int FLAGAMP = 17;

    /**  AMP table "per" column data object offset in collection stored by implementing class.  */
    public static final int PER = 18;

    /**  AMP table "snr" column data object offset in collection stored by implementing class.  */
    public static final int SNR = 19;

    /**  AMP table "tau" column data object offset in collection stored by implementing class.  */
    public static final int TAU = 20;

    /**  AMP table "quality" column data object offset in collection stored by implementing class.  */
    public static final int QUALITY = 21;

    /**  AMP table "rflag" column data object offset in collection stored by implementing class.  */
    public static final int RFLAG = 22;

    /**  AMP table "cflag" column data object offset in collection stored by implementing class.  */
    public static final int CFLAG = 23;

    /**  AMP table "wstart" column data object offset in collection stored by implementing class.  */
    public static final int WSTART = 24;

    /**  AMP table "duration" column data object offset in collection stored by implementing class.  */
    public static final int DURATION = 25;

    /**  AMP table "lddate" column data object offset in collection stored by implementing class.  */
    public static final int LDDATE = 26;

    /**  AMP table "lddate" column data object offset in collection stored by implementing class.  */
    public static final int FILEID = 27;

    /** Table column data field names. This is the only place the names are defined all else is derived from this.  */
    public static final String [] FIELD_NAMES  = {
        "AMPID", "COMMID", "DATETIME", "STA", "NET",
        "AUTH", "SUBSOURCE", "CHANNEL", "CHANNELSRC", "SEEDCHAN",
        "LOCATION", "IPHASE", "AMPLITUDE", "AMPTYPE", "UNITS",
        "AMPMEAS", "ERAMP", "FLAGAMP", "PER", "SNR",
        "TAU", "QUALITY", "RFLAG", "CFLAG", "WSTART",
        "DURATION", "LDDATE", "FILEID"
    };

    /** String of know column names delimited by ",".  */
    public static final String COLUMN_NAMES =
        FIELD_NAMES[0]+","+
        FIELD_NAMES[1]+","+
        "truetime.getEpoch("+FIELD_NAMES[2]+",'UTC'),"+ // for db to UTC conversion -aww 2008/02/12
        FIELD_NAMES[3]+","+
        FIELD_NAMES[4]+","+
        FIELD_NAMES[5]+","+
        FIELD_NAMES[6]+","+
        FIELD_NAMES[7]+","+
        FIELD_NAMES[8]+","+
        FIELD_NAMES[9]+","+
        FIELD_NAMES[10]+","+
        FIELD_NAMES[11]+","+
        FIELD_NAMES[12]+","+
        FIELD_NAMES[13]+","+
        FIELD_NAMES[14]+","+
        FIELD_NAMES[15]+","+
        FIELD_NAMES[16]+","+
        FIELD_NAMES[17]+","+
        FIELD_NAMES[18]+","+
        FIELD_NAMES[19]+","+
        FIELD_NAMES[20]+","+
        FIELD_NAMES[21]+","+
        FIELD_NAMES[22]+","+
        FIELD_NAMES[23]+","+
        "truetime.getEpoch("+FIELD_NAMES[24]+",'UTC'),"+ // for db to UTC conversion -aww 2010/02/25
        FIELD_NAMES[25]+","+
        FIELD_NAMES[26]+","+
        FIELD_NAMES[27];

    public static final String QUALIFIED_COLUMN_NAMES =
        TableRowUnassocAmp.DB_TABLE_NAME+"."+FIELD_NAMES[0]+","+
        TableRowUnassocAmp.DB_TABLE_NAME+"."+FIELD_NAMES[1]+","+
        "truetime.getEpoch("+TableRowUnassocAmp.DB_TABLE_NAME+"."+FIELD_NAMES[2]+",'UTC'),"+// for db to UTC conversion -aww 2008/02/12
        TableRowUnassocAmp.DB_TABLE_NAME+"."+FIELD_NAMES[3]+","+
        TableRowUnassocAmp.DB_TABLE_NAME+"."+FIELD_NAMES[4]+","+
        TableRowUnassocAmp.DB_TABLE_NAME+"."+FIELD_NAMES[5]+","+
        TableRowUnassocAmp.DB_TABLE_NAME+"."+FIELD_NAMES[6]+","+
        TableRowUnassocAmp.DB_TABLE_NAME+"."+FIELD_NAMES[7]+","+
        TableRowUnassocAmp.DB_TABLE_NAME+"."+FIELD_NAMES[8]+","+
        TableRowUnassocAmp.DB_TABLE_NAME+"."+FIELD_NAMES[9]+","+
        TableRowUnassocAmp.DB_TABLE_NAME+"."+FIELD_NAMES[10]+","+
        TableRowUnassocAmp.DB_TABLE_NAME+"."+FIELD_NAMES[11]+","+
        TableRowUnassocAmp.DB_TABLE_NAME+"."+FIELD_NAMES[12]+","+
        TableRowUnassocAmp.DB_TABLE_NAME+"."+FIELD_NAMES[13]+","+
        TableRowUnassocAmp.DB_TABLE_NAME+"."+FIELD_NAMES[14]+","+
        TableRowUnassocAmp.DB_TABLE_NAME+"."+FIELD_NAMES[15]+","+
        TableRowUnassocAmp.DB_TABLE_NAME+"."+FIELD_NAMES[16]+","+
        TableRowUnassocAmp.DB_TABLE_NAME+"."+FIELD_NAMES[17]+","+
        TableRowUnassocAmp.DB_TABLE_NAME+"."+FIELD_NAMES[18]+","+
        TableRowUnassocAmp.DB_TABLE_NAME+"."+FIELD_NAMES[19]+","+
        TableRowUnassocAmp.DB_TABLE_NAME+"."+FIELD_NAMES[20]+","+
        TableRowUnassocAmp.DB_TABLE_NAME+"."+FIELD_NAMES[21]+","+
        TableRowUnassocAmp.DB_TABLE_NAME+"."+FIELD_NAMES[22]+","+
        TableRowUnassocAmp.DB_TABLE_NAME+"."+FIELD_NAMES[23]+","+
        "truetime.getEpoch("+TableRowUnassocAmp.DB_TABLE_NAME+"."+FIELD_NAMES[24]+",'UTC'),"+// for db to UTC conversion -aww 2008/02/25
        TableRowUnassocAmp.DB_TABLE_NAME+"."+FIELD_NAMES[25]+","+
        TableRowUnassocAmp.DB_TABLE_NAME+"."+FIELD_NAMES[26]+","+
        TableRowUnassocAmp.DB_TABLE_NAME+"."+FIELD_NAMES[27];

    /** Nullable table column field.  */
    public static final boolean [] FIELD_NULLS = {
        false, true, false, false, true,
        false, true, true, true, true,
        true, true, false, true, false,
        true, true, true, true, true,
        true, true, true, true, false,
        false, true, true
    };

    /**  Table column data field object class identifiers.
    * @see org.trinet.jdbc.datatypes.DataClassIds
    * @see org.trinet.jdbc.datatypes.DataClasses
    */
    public static final int [] FIELD_CLASS_IDS = {
        DATALONG, DATALONG, DATADOUBLE, DATASTRING, DATASTRING,
        DATASTRING, DATASTRING, DATASTRING, DATASTRING, DATASTRING,
        DATASTRING, DATASTRING, DATADOUBLE, DATASTRING, DATASTRING,
        DATASTRING, DATADOUBLE, DATASTRING, DATADOUBLE, DATADOUBLE,
        DATADOUBLE, DATADOUBLE, DATASTRING, DATASTRING, DATADOUBLE,
        DATADOUBLE, DATADATE, DATALONG
    };

    /**  Column indices of primary key table columns.  */
    public static final int [] KEY_COLUMNS = {0};

    /**  Number of decimal fraction digits in table column data fields.  */
    public static final int [] FIELD_DECIMAL_DIGITS = {
        0, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 4, 0,
        4, 1, 0, 0, 0, 0, 0, 0
    };

    /** Numeric sizes (width) of the table column data fields.  * For use in JTables, etc.  */
    public static final int [] FIELD_SIZES = {
        15, 15, 25, 6, 8, 15, 8, 3, 8, 3, 2, 8, 126, 8, 4, 1, 5, 4, 10, 126,
        7, 2, 2, 2, 126, 126, 7, 15
    };

    /** Default table column field values.  */
    public static final String [] FIELD_DEFAULTS = {
        null, null, null, null, null, null, null, null, null, null,
        null, null, null, null, null, null, null, null, null, null,
        null, null, null, null, null, null, "(SYS_EXTRACT_UTC(CURRENT_TIMESTAMP))", null
    };

}

