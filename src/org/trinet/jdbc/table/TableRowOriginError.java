package org.trinet.jdbc.table;
import org.trinet.jdbc.datatypes.DataClassIds;
/** Interface of static data constants defining the named table.
* @see OriginError
*/
public interface TableRowOriginError extends DataClassIds {

/** Name of schema database table represented by this class.
*/
    public static final String DB_TABLE_NAME =  "ORIGIN_ERROR";

/** Number of column data fields in a table row.
*/
    public static final int MAX_FIELDS =  21;

/** Id sequence name for primary key column
*/
    public static String SEQUENCE_NAME = "";


/**  ORIGIN_ERROR table "orid" column data object offset in collection stored by implementing class.
*/
    public static final int ORID = 0;

/**  ORIGIN_ERROR table "sxx" column data object offset in collection stored by implementing class.
*/
    public static final int SXX = 1;

/**  ORIGIN_ERROR table "syy" column data object offset in collection stored by implementing class.
*/
    public static final int SYY = 2;

/**  ORIGIN_ERROR table "szz" column data object offset in collection stored by implementing class.
*/
    public static final int SZZ = 3;

/**  ORIGIN_ERROR table "stt" column data object offset in collection stored by implementing class.
*/
    public static final int STT = 4;

/**  ORIGIN_ERROR table "sxy" column data object offset in collection stored by implementing class.
*/
    public static final int SXY = 5;

/**  ORIGIN_ERROR table "sxz" column data object offset in collection stored by implementing class.
*/
    public static final int SXZ = 6;

/**  ORIGIN_ERROR table "syz" column data object offset in collection stored by implementing class.
*/
    public static final int SYZ = 7;

/**  ORIGIN_ERROR table "stx" column data object offset in collection stored by implementing class.
*/
    public static final int STX = 8;

/**  ORIGIN_ERROR table "sty" column data object offset in collection stored by implementing class.
*/
    public static final int STY = 9;

/**  ORIGIN_ERROR table "stz" column data object offset in collection stored by implementing class.
*/
    public static final int STZ = 10;

/**  ORIGIN_ERROR table "azismall" column data object offset in collection stored by implementing class.
*/
    public static final int AZISMALL = 11;

/**  ORIGIN_ERROR table "dipsmall" column data object offset in collection stored by implementing class.
*/
    public static final int DIPSMALL = 12;

/**  ORIGIN_ERROR table "magsmall" column data object offset in collection stored by implementing class.
*/
    public static final int MAGSMALL = 13;

/**  ORIGIN_ERROR table "aziinter" column data object offset in collection stored by implementing class.
*/
    public static final int AZIINTER = 14;

/**  ORIGIN_ERROR table "dipinter" column data object offset in collection stored by implementing class.
*/
    public static final int DIPINTER = 15;

/**  ORIGIN_ERROR table "maginter" column data object offset in collection stored by implementing class.
*/
    public static final int MAGINTER = 16;

/**  ORIGIN_ERROR table "azilarge" column data object offset in collection stored by implementing class.
*/
    public static final int AZILARGE = 17;

/**  ORIGIN_ERROR table "diplarge" column data object offset in collection stored by implementing class.
*/
    public static final int DIPLARGE = 18;

/**  ORIGIN_ERROR table "maglarge" column data object offset in collection stored by implementing class.
*/
    public static final int MAGLARGE = 19;

/**  ORIGIN_ERROR table "lddate" column data object offset in collection stored by implementing class.
*/
    public static final int LDDATE = 20;
/** String of know column names delimited by ",".
*/
    public static final String COLUMN_NAMES =
   "ORID,SXX,SYY,SZZ,STT,SXY,SXZ,SYZ,STX,STY,STZ,AZISMALL,DIPSMALL,MAGSMALL,AZIINTER,DIPINTER,MAGINTER,AZILARGE,DIPLARGE,MAGLARGE,LDDATE";

/** String of table qualified column names delimited by ",".
*/
    public static final String QUALIFIED_COLUMN_NAMES = 
    "ORIGIN_ERROR.ORID,ORIGIN_ERROR.SXX,ORIGIN_ERROR.SYY,ORIGIN_ERROR.SZZ,ORIGIN_ERROR.STT,ORIGIN_ERROR.SXY,ORIGIN_ERROR.SXZ,ORIGIN_ERROR.SYZ,ORIGIN_ERROR.STX,ORIGIN_ERROR.STY,ORIGIN_ERROR.STZ,ORIGIN_ERROR.AZISMALL,ORIGIN_ERROR.DIPSMALL,ORIGIN_ERROR.MAGSMALL,ORIGIN_ERROR.AZIINTER,ORIGIN_ERROR.DIPINTER,ORIGIN_ERROR.MAGINTER,ORIGIN_ERROR.AZILARGE,ORIGIN_ERROR.DIPLARGE,ORIGIN_ERROR.MAGLARGE,ORIGIN_ERROR.LDDATE";

/**  Table column data field names.
*/
    public static final String [] FIELD_NAMES  = {
	"ORID", "SXX", "SYY", "SZZ", "STT", 
	"SXY", "SXZ", "SYZ", "STX", "STY", 
	"STZ", "AZISMALL", "DIPSMALL", "MAGSMALL", "AZIINTER", 
	"DIPINTER", "MAGINTER", "AZILARGE", "DIPLARGE", "MAGLARGE", 
	"LDDATE"
    };

/** Nullable table column field.
*/
    public static final boolean [] FIELD_NULLS = {
	false, true, true, true, true, 
	true, true, true, true, true, 
	true, true, true, true, true, 
	true, true, true, true, true, 
	true
    };

/**  Table column data field object class identifiers.
* @see org.trinet.jdbc.datatypes.DataClassIds
* @see org.trinet.jdbc.datatypes.DataClasses
*/
    public static final int [] FIELD_CLASS_IDS = {
	DATALONG, DATADOUBLE, DATADOUBLE, DATADOUBLE, DATADOUBLE, 
	DATADOUBLE, DATADOUBLE, DATADOUBLE, DATADOUBLE, DATADOUBLE, 
	DATADOUBLE, DATADOUBLE, DATADOUBLE, DATADOUBLE, DATADOUBLE, 
	DATADOUBLE, DATADOUBLE, DATADOUBLE, DATADOUBLE, DATADOUBLE, 
	DATADATE
    };

/**  Column indices of primary key table columns.
*/
    public static final int [] KEY_COLUMNS = {0};

/**  Number of decimal fraction digits in table column data fields.
*/
    public static final int [] FIELD_DECIMAL_DIGITS = {
	0, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 2, 2, 5, 2, 2, 5, 2, 2, 5, 
	0
    };

/** Numeric sizes (width) of the table column data fields.
*/
    public static final int [] FIELD_SIZES = {
	15, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 4, 7, 5, 4, 7, 5, 4, 7, 
	7
    };

/** Default table column field values.
*/
    public static final String [] FIELD_DEFAULTS = {
	null, null, null, null, null, null, null, null, null, null, 
	null, null, null, null, null, null, null, null, null, null, 
	"(SYS_EXTRACT_UTC(CURRENT_TIMESTAMP))"
    };
}
