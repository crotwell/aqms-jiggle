package org.trinet.jdbc.table;

import org.trinet.jdbc.*;
import org.trinet.jdbc.datatypes.*;
import java.sql.Connection;

/** 
Class representing a row in the JasiEventLock table of the Oracle dbase. This is
used by the TriNet concrete implementation of org.trinet.jasi.SolutionLock which
allows event locking. <p>

Constructor uses static data members defined by the TableRowJasiEventLock
interface to initialize the base class with the parameters necessary to describe
the schema table named by the interface String parameter DB_TABLE_NAME. The
class implements several convenience methods to provides class specific static
arguments to the argument list of the DataTableRow base class methods. Base
class methods are used to set or modify the values and states of the contained
DataObjects. Because the base class uses a JDBC connection class
to access the database containing the table described by this object,
a connection object must be instantiated before using
any of the database enabled methods of this class.  Object states refering to
data updated, nullness, and mutability are inhereited from the DataTableRow base
class implementation of the DataState interface. These state conditions are used
to control the methods access to the object and its contained DataObjects, which
also implement the DataState interface.  The default constructor sets the
default states to: setUpdate(false), setNull(true), setMutable(true), and
setProcessing(NONE).  

@see: org.trinet.jasi.SolutionLock
*/


public class JasiEventLock extends StnChlTableRow implements TableRowJasiEventLock {

    public JasiEventLock() {
        super(DB_TABLE_NAME, SEQUENCE_NAME, MAX_FIELDS, KEY_COLUMNS, 
	      FIELD_NAMES, FIELD_NULLS, FIELD_CLASS_IDS);
    }

/** Constructor invokes default constructor, then sets the column data members
* to the input values.  The newly instantiated object state values are
* isUpdate() == true and isNull == false.  */
    public JasiEventLock(Connection conn, 
			 long evid, String host, String app, String user) {
	this(conn);
        fields.set(EVID,	new DataLong(evid));
        fields.set(HOSTNAME,	new DataString(host));
        fields.set(APPLICATION, new DataString(app));
	fields.set(USERNAME,	new DataString(user));

	//	"EVID", "HOSTNAME", "APPLICATION", "USERNAME", "LDDATE"
    }

/** Copy constructor invokes the default constructor, then clones all the
* DataObject classes of the input argument.  The newly instantiated object state
* values are set to those of the input object.  */
    public JasiEventLock(JasiEventLock object) {
        this();
        for (int index = 0; index < MAX_FIELDS; index++) {
            fields.set(index, ((DataObject) object.fields.get(index)).clone());
        }
        this.valueUpdate  = object.valueUpdate;
        this.valueNull    = object.valueNull;
        this.valueMutable = object.valueMutable;
    }

/** Constructor invokes default constructor, then sets the default Connection
* object to the handle of the input Connection argument.  The newly instantiated
* object state values are those of the default constructor.  */
    public JasiEventLock(Connection conn) {
        this();
        setConnection(conn);
    }

/** Constructor invokes default constructor, then sets the column data members
* to the input values.  The newly instantiated object state values are
* isUpdate() == true and isNull() == false.  */
    public JasiEventLock(String dummy) {
        this();
        fields.set(0, new DataString(dummy));
    }

/** Returns table row count.
*/
   public int getRowCount() {
        return ExecuteSQL.getRowCount(connDB, getTableName());
    }

}
