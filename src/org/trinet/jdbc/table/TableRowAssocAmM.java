package org.trinet.jdbc.table;
import org.trinet.jdbc.datatypes.DataClassIds;
/** Interface of static data constants defining the named table.
* @see AssocAmM
*/
public interface TableRowAssocAmM extends DataClassIds {

/** Name of schema database table represented by this class.
*/
    public static final String DB_TABLE_NAME =  "ASSOCAMM";

/** Number of column data fields in a table row.
*/
    public static final int MAX_FIELDS =  13;

/** Id sequence name for primary key column
*/
    public static String SEQUENCE_NAME = "";


/**  ASSOCAMM table "magid" column data object offset in collection stored by implementing class.
*/
    public static final int MAGID = 0;

/**  ASSOCAMM table "ampid" column data object offset in collection stored by implementing class.
*/
    public static final int AMPID = 1;

/**  ASSOCAMM table "commid" column data object offset in collection stored by implementing class.
*/
    public static final int COMMID = 2;

/**  ASSOCAMM table "auth" column data object offset in collection stored by implementing class.
*/
    public static final int AUTH = 3;

/**  ASSOCAMM table "subsource" column data object offset in collection stored by implementing class.
*/
    public static final int SUBSOURCE = 4;

/**  ASSOCAMM table "weight" column data object offset in collection stored by implementing class.
*/
    public static final int WEIGHT = 5;

/**  ASSOCAMM table "in_wgt" column data object offset in collection stored by implementing class.
*/
    public static final int IN_WGT = 6;

/**  ASSOCAMM table "mag" column data object offset in collection stored by implementing class.
*/
    public static final int MAG = 7;

/**  ASSOCAMM table "magres" column data object offset in collection stored by implementing class.
*/
    public static final int MAGRES = 8;

/**  ASSOCAMM table "magcorr" column data object offset in collection stored by implementing class.
*/
    public static final int MAGCORR = 9;

/**  ASSOCAMM table "importance" column data object offset in collection stored by implementing class.
*/
    public static final int IMPORTANCE = 10;

/**  ASSOCAMM table "rflag" column data object offset in collection stored by implementing class.
*/
    public static final int RFLAG = 11;

/**  ASSOCAMM table "lddate" column data object offset in collection stored by implementing class.
*/
    public static final int LDDATE = 12;
/** String of know column names delimited by ",".
*/
    public static final String COLUMN_NAMES =
   "MAGID,AMPID,COMMID,AUTH,SUBSOURCE,WEIGHT,IN_WGT,MAG,MAGRES,MAGCORR,IMPORTANCE,RFLAG,LDDATE";

/** String of table qualified column names delimited by ",".
*/
    public static final String QUALIFIED_COLUMN_NAMES = 
    "ASSOCAMM.MAGID,ASSOCAMM.AMPID,ASSOCAMM.COMMID,ASSOCAMM.AUTH,ASSOCAMM.SUBSOURCE,ASSOCAMM.WEIGHT,ASSOCAMM.IN_WGT,ASSOCAMM.MAG,ASSOCAMM.MAGRES,ASSOCAMM.MAGCORR,ASSOCAMM.IMPORTANCE,ASSOCAMM.RFLAG,ASSOCAMM.LDDATE";

/**  Table column data field names.
*/
    public static final String [] FIELD_NAMES  = {
	"MAGID", "AMPID", "COMMID", "AUTH", "SUBSOURCE", 
	"WEIGHT", "IN_WGT", "MAG", "MAGRES", "MAGCORR", 
	"IMPORTANCE", "RFLAG", "LDDATE"
    };

/** Nullable table column field.
*/
    public static final boolean [] FIELD_NULLS = {
	false, false, true, false, true, 
	true, true, true, true, true, 
	true, true, true
    };

/**  Table column data field object class identifiers.
* @see org.trinet.jdbc.datatypes.DataClassIds
* @see org.trinet.jdbc.datatypes.DataClasses
*/
    public static final int [] FIELD_CLASS_IDS = {
	DATALONG, DATALONG, DATALONG, DATASTRING, DATASTRING, 
	DATADOUBLE, DATADOUBLE, DATADOUBLE, DATADOUBLE, DATADOUBLE, 
	DATADOUBLE, DATASTRING, DATADATE
    };

/**  Column indices of primary key table columns.
*/
    public static final int [] KEY_COLUMNS = {0, 1};

/**  Number of decimal fraction digits in table column data fields.
*/
    public static final int [] FIELD_DECIMAL_DIGITS = {
	0, 0, 0, 0, 0, 3, 3, 2, 2, 2, 1, 0, 0
    };

/** Numeric sizes (width) of the table column data fields.
*/
    public static final int [] FIELD_SIZES = {
	15, 15, 15, 15, 8, 4, 4, 5, 5, 5, 2, 2, 7
    };

/** Default table column field values.
*/
    public static final String [] FIELD_DEFAULTS = {
	null, null, null, null, null, null, null, null, null, null, 
	null, null, "(SYS_EXTRACT_UTC(CURRENT_TIMESTAMP))"
    };
}
