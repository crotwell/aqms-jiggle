package org.trinet.jdbc.table;
import org.trinet.jdbc.datatypes.DataClassIds;
/** Interface of static data constants defining the named table.
* @see SignificantEvent
*/
public interface TableRowSignificantEvent extends DataClassIds {

/** Name of schema database table represented by this class.
*/
    public static final String DB_TABLE_NAME =  "SIGNIFICANT_EVENT";

/** Number of column data fields in a table row.
*/
    public static final int MAX_FIELDS =  7;

/** Id sequence name for primary key column
*/
    public static String SEQUENCE_NAME = "";


/**  SIGNIFICANT_EVENT table "evid" column data object offset in collection stored by implementing class.
*/
    public static final int EVID = 0;

/**  SIGNIFICANT_EVENT table "evname" column data object offset in collection stored by implementing class.
*/
    public static final int EVNAME = 1;

/**  SIGNIFICANT_EVENT table "remarks" column data object offset in collection stored by implementing class.
*/
    public static final int REMARKS = 2;

/**  SIGNIFICANT_EVENT table "nfelt" column data object offset in collection stored by implementing class.
*/
    public static final int NFELT = 3;

/**  SIGNIFICANT_EVENT table "mmi" column data object offset in collection stored by implementing class.
*/
    public static final int MMI = 4;

/**  SIGNIFICANT_EVENT table "pga" column data object offset in collection stored by implementing class.
*/
    public static final int PGA = 5;

/**  SIGNIFICANT_EVENT table "lddate" column data object offset in collection stored by implementing class.
*/
    public static final int LDDATE = 6;
/** String of know column names delimited by ",".
*/
    public static final String COLUMN_NAMES =
   "EVID,EVNAME,REMARKS,NFELT,MMI,PGA,LDDATE";

/** String of table qualified column names delimited by ",".
*/
    public static final String QUALIFIED_COLUMN_NAMES = 
    "SIGNIFICANT_EVENT.EVID,SIGNIFICANT_EVENT.EVNAME,SIGNIFICANT_EVENT.REMARKS,SIGNIFICANT_EVENT.NFELT,SIGNIFICANT_EVENT.MMI,SIGNIFICANT_EVENT.PGA,SIGNIFICANT_EVENT.LDDATE";

/**  Table column data field names.
*/
    public static final String [] FIELD_NAMES  = {
	"EVID", "EVNAME", "REMARKS", "NFELT", "MMI", 
	"PGA", "LDDATE"
    };

/** Nullable table column field.
*/
    public static final boolean [] FIELD_NULLS = {
	false, true, true, true, true, 
	true, true
    };

/**  Table column data field object class identifiers.
* @see org.trinet.jdbc.datatypes.DataClassIds
* @see org.trinet.jdbc.datatypes.DataClasses
*/
    public static final int [] FIELD_CLASS_IDS = {
	DATALONG, DATASTRING, DATASTRING, DATALONG, DATALONG, 
	DATADOUBLE, DATADATE
    };

/**  Column indices of primary key table columns.
*/
    public static final int [] KEY_COLUMNS = {0};

/**  Number of decimal fraction digits in table column data fields.
*/
    public static final int [] FIELD_DECIMAL_DIGITS = {
	0, 0, 0, 0, 0, 4, 0
    };

/** Numeric sizes (width) of the table column data fields.
*/
    public static final int [] FIELD_SIZES = {
	15, 80, 2, 8, 2, 6, 7
    };

/** Default table column field values.
*/
    public static final String [] FIELD_DEFAULTS = {
	null, null, null, null, null, null, "(SYS_EXTRACT_UTC(CURRENT_TIMESTAMP))"
    };
}
