package org.trinet.jdbc;

import java.sql.*;
import java.util.StringTokenizer;
import org.trinet.jdbc.datatypes.*;
import org.trinet.util.*;

/** Class converts table column input data to SQL string syntax appropiate to build SQL statements.
* @see NullValueDb
* @see org.trinet.jdbc.datatypes.DataObject
*/
public class StringSQL {

    private static final char APOSTROPHE = '\'';
    public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    
    /* Use this rather than SYSDATE to insure current UTC time
     * SYSDATE = the OS system time zone
     * LOCALTIMESTAMP = dbase time zone which can be set differently  
     */
    public static final String NOW_UTC = "SYS_EXTRACT_UTC(CURRENT_TIMESTAMP)";  //DDG 5/23/08
    
/** Returns String.valueOf(value) or returns "NULL", if NullValueDb.isNull(value). */
    public static String valueOf(int value) {
    return (NullValueDb.isNull(value)) ? "NULL" : String.valueOf(value);
    }

/** Returns String.valueOf(value) or returns "NULL", if NullValueDb.isNull(value). */
    public static String valueOf(long value) {
    return (NullValueDb.isNull(value)) ? "NULL" : String.valueOf(value);
    }

/** Returns String.valueOf(value) or returns "NULL", if NullValueDb.isNull(value). */
    public static String valueOf(float value) {
    return (NullValueDb.isNull(value)) ? "NULL" : String.valueOf(value);
    }

/** Returns String.valueOf(value) or returns "NULL", if NullValueDb.isNull(value). */
     public static String valueOf(double value) {
    return (NullValueDb.isNull(value)) ? "NULL" : String.valueOf(value);
    }

/** Returns String.valueOf(value.intValue()) or returns "NULL", if NullValueDb.isNull(value). */
    public static String valueOf(Integer value) {
    return (NullValueDb.isNull(value.intValue())) ? "NULL" : String.valueOf(value.intValue());
    }

/** Returns String.valueOf(value.longValue()) or returns "NULL", if NullValueDb.isNull(value). */
    public static String valueOf(Long value) {
    return (NullValueDb.isNull(value.longValue())) ? "NULL" : String.valueOf(value.longValue());
    }

/** Returns String.valueOf(value.floatValue()) or returns "NULL", if NullValueDb.isNull(value). */
    public static String valueOf(Float value) {
    return (NullValueDb.isNull(value.floatValue())) ? "NULL" : String.valueOf(value.floatValue());
    }

/** Returns String.valueOf(value.doubleValue()) or returns "NULL", if NullValueDb.isNull(value). */
    public static String valueOf(Double value) {
    return (NullValueDb.isNull(value.doubleValue())) ? "NULL" : String.valueOf(value.doubleValue());
    }

/** Returns "'value'" with blanks trimmed, or returns "NULL" if isBlank(value)==true.
*   Checks for embedded apostrophes in input value and doubles them up (' => '').
*/
    public static String valueOf(String value) {
        return valueOf(value, true);
    }

/** Returns "'value'" with blanks trimmed, or returns "NULL" if
*   NullValueDb.isEmpty(value) or 'blankNull' is <i>true </i> and value.trim().length() is 0.
*   Checks for embedded apostrophes in input value and doubles them up (' => '').
*/
    public static String valueOf(String value, boolean blankNull) {
        if ( NullValueDb.isEmpty(value) ||
             (blankNull && value.trim().length() == 0)) return "NULL";
        return toStringSQL(toSafeSqlString(value));
    }

/** Encapsulates input String with apostrophes for SQL literal strings. */
    private static String toStringSQL(String value) {
        StringBuffer sb = new StringBuffer(value.length()+2);
        //sb.append(APOSTROPHE).append(value.trim()).append(APOSTROPHE); // removed trim() - aww 07/14/2005
        // Must accept input as is (no trim, channel location codes may be "  " 2-blanks.
        sb.append(APOSTROPHE).append(value).append(APOSTROPHE);
        return sb.toString();
    }

/** Make a string safe by escaping single-quotes. A single quote is ruin the syntax of an SQL
 * statement. For example: "What's up?"  becomes INSERT INTO REMARK ( REMSTR ) VALUES ('What's up?');
 * It must be: INSERT INTO REMARK ( REMSTR ) VALUES ('What''s up?');
 * Does not trim.
*/

    public static String toSafeSqlString (String value) {
      if (value.indexOf(APOSTROPHE) == -1) return value;  // no 's return input
      StringBuffer sb = new StringBuffer(value.length()+1);
      int len = value.length();
      for (int i = 0; i < len; i++) {
        char ch = value.charAt(i);
        sb.append(ch);
        if (ch == APOSTROPHE) sb.append(APOSTROPHE);  // need 2 of them
      }
      return sb.toString();
    }

/** Returns "'date-string-value'" or returns "NULL" if NullValueDb.isNull(value).
* Example: '2002-03-12 01:32:54', no fractional seconds. */
    public static String valueOf(java.util.Date value) {
        return (NullValueDb.isNull(value)) ?
          "NULL" : toStringSQL(EpochTime.dateToString(value, "yyyy-MM-dd HH:mm:ss")); // ORACLE DATE whole nominal not UTC seconds -aww 2008/02/11
    }

/*
//Returns "'date-string-value'" or returns "NULL" if NullValueDb.isNull(value) -aww removed method on 2008/02/11 
//because for case where input is true leap second value, ORACLE SQL string to DATE conversion doesn't allow seconds > 59
    public static String valueOf(org.trinet.util.DateTime value) { 
        return (NullValueDb.isNull(value)) ?
          "NULL" : toStringSQL(value.toDateString(DEFAULT_DATE_FORMAT));
    }
*/

/** Returns "'date-string-value'" or returns "NULL" if NullValueDb.isNull(value). */
    public static String valueOf(java.sql.Date value) {
        return (NullValueDb.isNull(value)) ? "NULL" : toStringSQL(value.toString());
    }

/** Returns "'time-string-value'" or returns "NULL" if NullValueDb.isNull(value). */
    public static String valueOf(java.sql.Time value) {
        return (NullValueDb.isNull(value)) ? "NULL" : toStringSQL(value.toString());
    }

/** Returns "'timestamp-string-value'" or returns "NULL" if NullValueDb.isNull(value). */
    public static String valueOf(java.sql.Timestamp value) {
        return (NullValueDb.isNull(value)) ? "NULL" : toStringSQL(value.toString());
    }

/** Returns value.toStringSQL or returns "NULL" if NullValueDb.isNull(value). */
    public static String valueOf(DataObject value) {
    return (NullValueDb.isNull(value)) ? "NULL" : value.toStringSQL();
    }

/** Returns valueOf(value) from this object, else returns "'String.valueOf(value)'"
* or returns "NULL" if NullValueDb.isNull(value).
*/
    public static String valueOf(Object value) {
    if (NullValueDb.isNull(value)) return "NULL";
    if (value instanceof Integer) return valueOf((Integer) value);
    if (value instanceof Double) return valueOf((Double) value);
    if (value instanceof Float) return valueOf((Float) value);
    if (value instanceof Long) return valueOf((Long) value);
    if (value instanceof String) return valueOf((String) value);
    if (value instanceof Time) return valueOf((Time) value);
    if (value instanceof Timestamp) return valueOf((Timestamp) value);
    if (value instanceof java.sql.Date) return valueOf((java.sql.Date) value);
    // above here are java subclasses of java.util.Date with different String formats 
    if (value instanceof java.util.Date) return valueOf((java.util.Date) value);
    if (value instanceof DataObject) return valueOf((DataObject) value);
        return valueOf(String.valueOf(value)); // default for unknown type
    }

/** Returns "NULL" if isBlank(value)==true, else returns the input value. */
    public static String EmptyToNULL(String value) {
    return (NullValueDb.isBlank(value)) ? "NULL" : value;
    }

/** Returns "NULL" if the input value == null, else returns the input value. */
    public static String NullToNULL(String value) {
    return (value == null) ? "NULL" : value;
    }

/** Returns "" if the input value == null, else returns the input value. */
    public static String NullToEmpty(String value) {
    return (value == null) ? "" : value;
    }

/** Returns String wrapper of Database function to convert input Date to datebase DATE type.
 *  of the form TO_DATE(dateString, "YYYY-MM-DD HH24:MI:SS.SS")
 *  @see #valueOf(Date)
 */
    public static String toDATE(java.util.Date date) {
        StringBuffer sb = new StringBuffer(64);
        sb.append("TO_DATE(").append(valueOf(date)).append(", 'YYYY-MM-DD HH24:MI:SS')");
        return sb.toString();
    }
    public static String toDATE(java.sql.Time date) { // Time java format is hh:mm:ss
        StringBuffer sb = new StringBuffer(64);
        sb.append("TO_DATE(").append(valueOf(date)).append(", 'HH24:MI:SS')");
        return sb.toString();
    }
    public static String toDATE(java.sql.Timestamp date) { // Timestamp java format has .fffffffff at end of string
        StringBuffer sb = new StringBuffer(64);
        sb.append("TO_DATE(").append(valueOf(date).substring(0,19)).append(", 'YYYY-MM-DD HH24:MI:SS')");
        return sb.toString();
    }
/**
* Returns canonical empty string "", if input string is "empty",
* that is: (str == null || str.trim() == 0) .
* Otherwise returns "intern()" lookup of trimmed input value.
* */
  public static String trimToCanonical(String value) {
    if (value == null) return "";
    // value is new String reference pointing offset,len to old internal character array values
    value = value.trim();
    return (value.length() == 0) ? "" : value.intern(); // for duplicate objects use new String(value);
  }

/*
  public final static class Tester {
      public static void main(String[] args) {
//      String str = "This is a string !@#$%^&*()_+=-][}{';><";
//      System.out.println(StringSQL.valueOf(str));
        String str = "This is a string'y thing'y some doubles '' and a triple ''' ";
        System.out.println(StringSQL.valueOf(str));
        System.out.println(StringSQL.valueOf(new java.util.Date()));
        System.out.println(StringSQL.toDATE(new java.util.Date()));
      }
  }
*/
}
