package org.trinet.jasi;
public class UnitsAmp extends UnitsDouble {
     { units = Units.COUNTS; }
     public UnitsAmp() { }
     public UnitsAmp(UnitsAmp data) {
       super(data);
     }
     public UnitsAmp(double value, int units) { 
       super(value, units);
     }
}
