package org.trinet.jasi;
import org.trinet.util.*;
//import java.util.regex.*;
import jregex.Pattern; // aww 02/15/2005 try this pkg

public class SeedChanMatcher extends AbstractChannelNameMatcher {

    public SeedChanMatcher() { }

    public SeedChanMatcher(String [] regex) {
      this(regex, 0);
    }
    public SeedChanMatcher(String [] regex, int flags) {
      super(regex, flags);
    }

    public boolean matches(ChannelIdIF cid) {
      return matches(cid.getSeedchan()); 
    }

  /*
  static public final class Tester {  
    public static final void main(String [] args) {
      SeedChanMatcher sm = new SeedChanMatcher(new String [] {"HHZ","HLZ","HLE"});
      ChannelName cn = new ChannelName("CI","PAS","Z","HHZ");
      System.out.println("HHZ sm.matches(HHZ) t: " + sm.matches(cn));
      cn.setSeedchan("ABC");
      System.out.println("HHZ sm.matches(ABC) f: " + sm.matches(cn));
      System.out.println();
      sm.createPatternList(new String [] {"H_Z","BLZ","ABC"});
      cn.setSeedchan("HHZ");
      System.out.println("H_Z sm.matches(HHZ) t: " + sm.matches(cn));
      cn.setSeedchan("HLZ");
      System.out.println("H_Z sm.matches(HLZ) t: " + sm.matches(cn));
      System.out.println();
      sm.createPatternList(new String [] {"DEF","BLZ","H%"});
      cn.setSeedchan("HLN");
      System.out.println("H%  sm.matches(HLN) t: " + sm.matches(cn));
      cn.setSeedchan("HLE");
      System.out.println("H%  sm.matches(HLE) t: " + sm.matches(cn));
      cn.setSeedchan("ABC");
      System.out.println("H%  sm.matches(ABC) f: " + sm.matches(cn));
      System.out.println();
      //sm.createPatternList(new String [] {"DEF","BLZ","H%"}, Pattern.CASE_INSENSITIVE); //jdk 1.4
      sm.createPatternList(new String [] {"DEF","BLZ","H%"}, Pattern.IGNORE_CASE); // jregex
      cn.setSeedchan("hhz");
      System.out.println("H% nocase sm.matches(hhz) t: " + sm.matches(cn));
      cn.setSeedchan("hlz");
      System.out.println("H% nocase sm.matches(hlz) t: " + sm.matches(cn));
      cn.setSeedchan("abc");
      System.out.println("H% nocase sm.matches(abc) f: " + sm.matches(cn));
    }
  }
  */
}
