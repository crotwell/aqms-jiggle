package org.trinet.jasi;
import java.util.*;
import org.trinet.jdbc.datatypes.DataLong;
import org.trinet.jdbc.datatypes.DataString;
/**
 * A list of Amplitude objects
 * @See: Amplitude()
 */
public class AmpList extends AbstractMagnitudeAssocList {
    // Class which list elements must be instanceof
    //protected static final Class elementClass = Amplitude.class;

    //Only set ampset elements when batch processing event associated amplist, e.g. strong groundmotions, needing a single commit call
    protected DataString myAmpSetType = new DataString(); // here? or element of a subclass like AmpListSet
    protected DataLong myAmpSetId = new DataLong();       // here? or element of a subclass like AmpListSet

    /** No-op default constructor, empty list. */
    public AmpList() { }
    /** Create empty list with specified input capacity.*/
    public AmpList(int capacity) { super(capacity); }
    /** Create list and add input collection elements to list.*/
    public AmpList(Collection col) { super(col); }

    public static final AmpList create() {
        return create(JasiObject.DEFAULT);
    }

    private static final AmpList create(int schemaType) {
        return create(JasiFactory.suffix[schemaType]);
    }

    private static final AmpList create(String suffix) {
        return (AmpList) JasiObject.newInstance("org.trinet.jasi.AmpList", suffix);
    }

    // Event AmpSet Assoc Code added here but would it be better to instead create new subclass AmpListSet, AmpListSetTN?
    public long getAmpSetId() {
        return (myAmpSetId.isNull()) ? 0l : myAmpSetId.longValue();
    }
    public void setAmpSetId(long setid) {
        myAmpSetId.setValue(setid);
    }
    public String getAmpSetType() {
        return (myAmpSetType.isNull()) ? "" : myAmpSetType.toString();
    }
    public void setAmpSetType(String type) {
        myAmpSetType.setValue(type);
    }
    public boolean invalidateAmpSet() {
        return false;
    }
    public boolean validateAmpSet() {
        return false;
    }
    public boolean invalidateAmpSets(String source) {
        return false;
    }

    public boolean commit(boolean newSeqAmpid) {
        return commit();
    }


    /*
    public int commitAmpSet(Solution sol) {
        return commitAmpSet(sol.getId().longValue());
    }
    public int commitAmpSet(long evid) {
        // no-op, let subclass do this
    }
    */
    // End of added AmpSet Assoc Code

    /** Returns header string describing data returned by toNeatString(). */
    public String getNeatHeader() {
      return Amplitude.getNeatStringHeader();
    }

    /**
    * Return the first amp of the given type in the list.
    * Returns null if none found.
    * @see: AmpType
    */
    public Amplitude getType(int type) {
      if (!AmpType.isLegal(type)) return null;
      Amplitude amp = null;
      int count = size();
      for (int i = 0; i<count; i++) {
        amp = (Amplitude) get(i);
        if (amp.type == type) return amp;
      }
      return null;
    }

    /**
     * Convenience method that casts the object array returned by
     * Collection.toArray() as an array of type Amplitude.
     * */
    public Amplitude[] getArray() {
        return (Amplitude[]) toArray(new Amplitude[this.size()]);
    }
    /**
     * Convenience wrapper for ArrayList.get() that casts the returned object
     * as an Amplitude.
     * */
    public Amplitude getAmp(int idx) {
        return (Amplitude) get(idx);
    }
}
