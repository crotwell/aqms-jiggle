package org.trinet.jasi;

//import java.lang.*;
//import java.io.File;
import java.util.*;

import org.trinet.jdbc.datatypes.*;

import org.trinet.util.*;
import org.trinet.util.gazetteer.*;

/**
   A JasiTimeSeries is a collection of Segments. The type of segment will depend
   on the type of time series.
*/
public abstract class JasiTimeSeries extends JasiTimeBoundedData
           implements JasiTimeSeriesIF {

// statics
   /** Pseudo null value for primatives. */
    private static final int NULL = (int) Math.pow(2.0, 31.0);

    // Enumeration of data types. These are from the NCDC Schema v1.5 doc
    public static final int UNKNOWN          = 0;
    public static final int RAW_FORMAT      = 1;
    public static final int SEED_FORMAT     = 2;
    public static final int PASSCAL_SEGY_FORMAT = 3;
    public static final int UCSB_SEGY_FORMAT    = 4;

    /** Data format as defined in NCDN schema */
    //public DataLong    format     = new DataLong();
    public DataLong    format       = new DataLong(SEED_FORMAT);

    /** The sample rate in units of samples/second. */
    protected  DataDouble samplesPerSecond = new DataDouble();

    double bytesPerSample;

    /** Channel information. */
    protected Channel chan  = Channel.create();

    /** List of Time-series Segments containing data for this channel/window */
    protected List segList = new ArrayList();  // must instantiate so we can use size()

    /** List of TimeSpans containing the original data packet boundaries */
    //protected TimeSpan[] packetSpans = null;
    protected List packetSpans = new ArrayList();

    /** Rectified background noise level with bias removed. It may be derived
    * from some outside source (say a longterm average from the external datasource.)
    * If calculated by this class it is
    * the median of the peaks between zero-crossings for a rectified time-series
    * with the bias removed. The time window for which it is calculated is set by
    * a call to scanForNoiseLevel(start, end). */
    float noiseLevel = Float.NaN;

    /** True if timeseries has been filtered. */
    boolean isFiltered = false;

    // Channelable interface
    /**
     * Return the channel object associated with this reading.
     */
    public Channel getChannelObj() {
      return chan;
    }
    public void setChannelObj(Channel chan) {
      // throws null pointer exception if input is null
      if (this.chan == null) this.chan = (Channel) chan.clone();
      else this.chan.copy((Channelable)chan);
    }

    public void setChannelObjData(Channelable chan) { // Channelable interface ??
      // throws null pointer exception if input is null
      if (this.chan == null) this.chan = (Channel) chan.getChannelObj().clone();
      else this.chan.setChannelObjData(chan.getChannelObj()); // set instance data values to input's
    }

    public boolean equalsChannelId(Channelable chan) {
      return (this == chan) ? true : getChannelObj().equalsChannelId(chan);
    }
    public boolean sameNetworkAs(Channelable chan) {
      return (this == chan) ? true : getChannelObj().sameNetworkAs(chan);
    }
    public boolean sameStationAs(Channelable chan) {
      return (this == chan) ? true : getChannelObj().sameStationAs(chan);
    }
    public boolean sameSeedChanAs(Channelable chan) {
      return (this == chan) ? true : getChannelObj().sameSeedChanAs(chan);
    }
    public boolean sameTriaxialAs(Channelable chan) {
      return (this == chan) ? true : getChannelObj().sameTriaxialAs(chan);
    }

    // see ChannelIdIF and ChannelIdentifiable interfaces
    public String toDelimitedSeedNameString(String delimiter) {
        return getChannelObj().toDelimitedSeedNameString(delimiter);
    }
    // see ChannelIdIF and ChannelIdentifiable interfaces
    public String toDelimitedNameString(String delimiter) {
        return getChannelObj().toDelimitedNameString(delimiter);
    }

    public void setDistance(double distance, double horizontalDistance) {
      setDistance(distance);
      setHorizontalDistance(distance);
    }
    public void setDistance(double distance) {
      getChannelObj().setDistance(distance);
    }
    public void setHorizontalDistance(double distance) {
      getChannelObj().setHorizontalDistance(distance);
    }

    /** Calculate and set both epicentral (horizontal) distance and true (hypocentral)
     * distance of this channel from the given location in km.
     * Note: this is better than setDistance() or setHorizontalDistance() because
     * it does both and insures they are consistent. */
    public double calcDistance(GeoidalLatLonZ loc) { // aww changed input arg type 06/11/2004
      return chan.calcDistance(loc);
    }

    // RE: channel dist etc. could put IF methods below
    // into a DistAzDepth type containing entity then
    // use getDistAzDepth() to return an object with data.
    //
    public double getDistance() {
      return getChannelObj().getDistance();
    }
    public double getHorizontalDistance() {
      return getChannelObj().getHorizontalDistance();
    }
    public double getVerticalDistance() {
      return getChannelObj().getVerticalDistance();
    }
    public void setAzimuth(double az) {
        getChannelObj().setAzimuth(az);
    }
    public double getAzimuth() {
        return getChannelObj().getAzimuth();
    }
    //

    public boolean setChannelObjFromMasterList(java.util.Date aDate) {
      return getChannelObj().setChannelObjFromMasterList(aDate);
    }
    public boolean setChannelObjFromList(ChannelList chanList, java.util.Date aDate) {
      return getChannelObj().setChannelObjFromList(chanList, aDate);
    }
    public boolean matchChannelWithDataSource(java.util.Date aDate) {
      return getChannelObj().matchChannelWithDataSource(aDate);
    }

// Convenience methods - wrappers around interfaces methods:
    public java.util.Date getLookUpDate() {
      return ( timeStart.isValidNumber() ) ?  // use for lookUp- aww
          new DateTime(getEpochStart(), true) : new java.util.Date(); // for UTC seconds -aww 2008/02/11
    }
    public boolean setChannelObjFromMasterList() {
      return setChannelObjFromMasterList(getLookUpDate());
    }
    public boolean setChannelObjFromList(ChannelList aList) {
      return setChannelObjFromList(aList, getLookUpDate());
    }
    public boolean matchChannelWithDataSource() {
      return matchChannelWithDataSource(getLookUpDate());
    }

    /** Set the timeseries sample rate in samples/second. */
    public void setSampleRate(double samplesPerSecond) {
       try {
         if (samplesPerSecond <= 0. || Double.isNaN(samplesPerSecond)) {
           throw new IllegalArgumentException("JasiTimeSeries WARNING for " +chan.getChannelObj()+
                   " invalid samplesPerSecond : " + samplesPerSecond);
         }
       } catch(IllegalArgumentException ex) {
           System.err.println(ex.getMessage()); // ex.printStackTrace();
       }
       this.samplesPerSecond.setValue(samplesPerSecond);

    }
    /** Return the timeseries sample rate in samples/second. */
    public double getSampleRate() {
       return (samplesPerSecond.isNull()) ? 0. : samplesPerSecond.doubleValue();
    }

    /** Save the start & stop times of the segment boundaries.
     *  These are sometimes useful in understanding the original data. */
    public void savePacketSpans() {
      // Only do this once
      if (packetSpans.isEmpty() ) {
      //if (packetSpans == null) {
        //packetSpans = new TimeSpan[segList.size()];
        //for (int i = 0; i < packetSpans.length ; i++) {
        //    packetSpans[i] = ((TimeSeriesSegment)segList.get(i)).getTimeSpan();
        //}
        for (int i = 0; i < segList.size() ; i++) {
            packetSpans.add( ((TimeSeriesSegment)segList.get(i)).getTimeSpan() );
        }
      }
    }

    /*
    public String packetSpansToString() {
        int cnt = packetSpans.size();
        if (cnt == 0) return "";
        StringBuffer sb = new StringBuffer(1024);
        for (int i = 0; i < cnt; i++) {
            sb.append(packetSpans.get(i).toString());
            sb.append("\n");
        }
        return sb.toString();
    }
    */

    /** Return the start & stop times of the segment boundaries.
     *  These are sometimes useful in understanding the original data. */
    public TimeSpan[] getPacketSpans() {
        //return packetSpans;
        return (TimeSpan []) packetSpans.toArray(new TimeSpan[packetSpans.size()]);
    }

    public List getPacketSpanList() {
        return packetSpans;
    }

    public void setPacketSpans(List timeSpanList) {
        //packetSpans =  new ArrayList(timeSpanList);
        packetSpans.clear();
        packetSpans.addAll(timeSpanList);
    }

    public void setPacketSpans(TimeSpan[] spanArray) {
        //packetSpans = spanArray;
        setPacketSpans( Arrays.asList(spanArray) );
    }

  /** Return the number of segments in the timeseries.*/
    public int getSegmentCount() {
        return segList.size();
    }
    /** Return the collection of WFSegments. */
    public List getSegmentList() {
        return segList;
    }

    /*
    public String segmentsToString() {
        int cnt = segList.size();
        if (cnt == 0) return "";
        StringBuffer sb = new StringBuffer(2048);
        for (int i = 0; i < cnt; i++) {
            sb.append(segList.get(i).toString());
            sb.append("\n");
        }
        return sb.toString();
    }
    */

  /** Sets the list of WFSegments; does not set PacketSpans to match the new list.*/
  public void setSegmentList(Collection list) {
      // Stopped saving spans by default because it overwrote original miniseed (raw) packetSpans array DDG - 2/2003
      setSegmentList(list, false);
  }
  public void setSegmentList(Collection list, boolean resetPacketSpans) {
      segList = (list == null) ? new ArrayList() : (ArrayList) list;
      if (resetPacketSpans) {
        packetSpans.clear();
        savePacketSpans();
      }
      noiseLevel = Float.NaN;
      scanSegListForTimeQuality();
  }

  /** Scan the timeQuality of each segment and set the whole waveform's
   *  timeQuality equal to the WORST (least value) in the list.  */
  public void scanSegListForTimeQuality() {
      if (segList.size() < 1) return;
      DataDouble worst = ((TimeSeriesSegment)segList.get(0)).getTimeQuality();
      DataDouble next;
      for (int i = 1; i < segList.size() ; i++) {
          next = ((TimeSeriesSegment)segList.get(i)).getTimeQuality();
          if (worst.compareTo(next) == 1) {
              worst = next;
          }
      }
      setTimeQuality(worst);
  }
  /** Set value of the noise level. */
  public void setNoiseLevel(float value) {
      noiseLevel = value;
  }
  /** Return value of the noise level. Returns Float.NaN if it is unknown. */
  public float getNoiseLevel() {
    if (noiseLevel == Float.NaN) scanForNoiseLevel();
    return noiseLevel;
  }

  // ABSTRACT METHODS
  /** Scan time series for the noise level. Different time series may calculate
   *  this in different ways. */
  public abstract float scanForNoiseLevel();

  public void setIsFiltered(boolean tf) {
    isFiltered = tf;
  }

  public boolean isFiltered() {
    return isFiltered;
  }

  public long getFormat() {
      return format.longValue(); 
  }
  public void setFormat(long value) {
      format.setValue(value); 
  }
}
