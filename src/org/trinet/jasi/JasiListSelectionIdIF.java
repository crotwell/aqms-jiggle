package org.trinet.jasi;
// requires concept of long "id", could work with sol assoc types like readings, mag:
public interface JasiListSelectionIdIF extends JasiListSelectionIF {
    public int getIndex(long id);     // convenience wrapper, checks only event id match not object equality?
    public JasiCommitableIF getNext(long id); // uses id only used to be Solution?
    public boolean contains(long id); // uses id only?
    public boolean setSelected(long id);
    public boolean setSelected(long id, boolean notify);
    public long getSelectedId();
 }
