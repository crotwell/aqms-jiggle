package org.trinet.jasi;

/**
 * Rflag.java
 *
 *
 * Created: Thu Dec  9 14:24:45 1999
 *
 * @author Doug Given
 * @version
 */
import org.trinet.jdbc.datatypes.*;

/**
 * NCDN 'rflag' field object. The purpose is to map a char in the NCDC field to
 * an in enumeration; */

public class Rflag  {

    public static String toString(int val) {
        return (val == JasiProcessingConstants.STATE_UNKNOWN) ? "" : ProcessingState.getTag(val);
    }

    public static int toInt(String str) {
        return ProcessingState.tagToId(str);
    }  

    public static int toInt(DataObject obj) {
        return Rflag.toInt(obj.toString());
    }  

} // Rflag
