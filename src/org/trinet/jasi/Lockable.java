package org.trinet.jasi;

/**
 * Lockable.java
 *
 *
 * Created: Tue Aug 15 16:43:00 2000
 *
 * @author Doug Given
 * @version
 */

public interface Lockable  {
    
    /** Lock this object, returns true on success, false on failure.
    * If locked, information on the current lock holder is in the data members. */
    public boolean lock();

    /** Release the lock on this object. Returns true even if the lock was not held
    * in the first place. */
    public boolean unlock();

    /** Returns true if this object is locked by anyone, the caller included. */
    public boolean isLocked();

    /** Returns true if this object is locked by the caller. */
    public boolean lockIsMine();

    
} // Lockable
