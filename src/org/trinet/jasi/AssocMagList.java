package org.trinet.jasi;
import java.util.*;
import org.trinet.util.*;
public class AssocMagList extends MagList {
    protected Solution assocSol;  // solution associated with mags in list.
    public AssocMagList() {super();}
    public AssocMagList(int capacity) {super(capacity);}
    public AssocMagList(Solution sol) {
        this();
        assign(sol);
    }
    public AssocMagList(Solution sol, Collection c) {
        this();
        assign(sol);
        addAll(c, false);
    }
    public void assign(Solution sol) { assocSol = sol;}
    public Solution getAssociatedSolution() { return assocSol;}

    public SolutionAssociatedListIF getAssociatedWith(boolean undeletedOnly) {
        return getAssociatedWith(assocSol, undeletedOnly);
    }
    public SolutionAssociatedListIF getAssociatedWith() {
        return getAssociatedWith(assocSol);
    }
    public void associateAllWith() {
        associateAllWith(assocSol);
    }
    public void unassociateAllFrom() {
        unassociateAllFrom(assocSol);
    }
    public void replaceAllOfTypeWith(Magnitude mag) {
        replaceAllOfTypeWith(assocSol, mag);
    }
    public JasiCommitableListIF getListByType(Object type) {
        return getListByType(assocSol, type);
    }
    public Magnitude getPreferredMagnitude() {
        return getPreferredMagnitude(assocSol);
    }
/** Returns preferred magnitude for associated sol, sets null if null. */ 
    public void setPreferredMagnitude(Magnitude mag) {
        if (assocSol == null) return;
        if (mag.isDeleted()) mag.setDeleteFlag(false);
        assocSol.setPreferredMagnitude(mag);
        if (mag != null) {
           // add it back to this list if not the sol.altMagList
           this.add(mag);
        }
    }
    public MagList getAlternateMagList() {
        return getAlternateMagList(assocSol);
    }
    public int deleteAll() {
        return deleteAll(assocSol);
    }
    public int removeAll() {
        return removeAll(assocSol);
    }
    public int stripByResidual(double val) {
        return stripByResidual(val, assocSol);
    }
    public int stripByDistance(double val) {
        return stripByDistance(val, assocSol); 
    }
    public boolean add(Object obj, boolean notify) {
            //if (obj == null) return false;
            Magnitude mag = (Magnitude) obj;
            Solution sol = mag.getAssociatedSolution();
            if (sol == null)  mag.assign(assocSol);
            if (! mag.isAssignedTo(assocSol)) {
                    System.err.print("AssocMagList.add(mag) mag.sol != list.assocSol");
                    System.err.print("Sol:\n" + sol.toString());
                    System.err.println("AssocSol:\n" + assocSol.toString());
                    return false;
            }
            return super.add(mag,notify);
    }
    public void add(int index, Object obj)  {
            //if (obj == null) return;
            Magnitude mag = (Magnitude) obj;
            Solution sol = mag.getAssociatedSolution();
            if (sol == null)  mag.assign(assocSol);
            if (! mag.isAssignedTo(assocSol)) {
                    System.err.print("AssocMagList.add(mag) mag.sol != list.assocSol");
                    System.err.print("Sol:\n" + sol.toString());
                    System.err.println("AssocSol:\n" + assocSol.toString());
                    return;
            }
            super.add(index,obj);
    }
} // end AssocMagList
