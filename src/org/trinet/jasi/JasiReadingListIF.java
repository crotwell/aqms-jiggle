package org.trinet.jasi;
import java.util.*;
public interface JasiReadingListIF extends SolutionAssociatedListIF, ChannelableListIF {
    /** Order by matching net,sta,seedchan; no component secondary sort.*/
    public static final int FULL_NAME_SORT     = 0;
    /** Order by site, seedchan, time in default order defined by ComponentSorter. */
    public static final int SITE_SEEDCHAN_TIME_SORT = 1;
    /** Order by site, seedchan by time. */
    public static final int SITE_TIME_SORT     = 2;
  // TODO:
  // Instead of variant methods for iterative loop filtering have single method
  // do loop and use filter mask to cascade through tests for acceptable match.
  // Test selection filter mask (bitwise "&") for match of binary flag settings
  // public static final int DELETED         = -1; 
  // public static final int ALL             =  0;
  // public static final int UNDELETED       =  1; 
  // 
  // public static final int ASSOC           = 1; 
  // public static final int ALL_DATA        = 0;
  // public static final int UNASSOC         = -1; 
  // 
  // public static final int MATCH_ANY       =  0;  
  // public static final int MATCH_INSTANCE  =  1; 
  // public static final int MATCH_ID        =  2;
  // public static final int MATCH_CHANNEL   =  4;
  // public static final int MATCH_TYPE      =  8;
  // public static final int MATCH_ASSOC     = 16;
  // public static final int MATCH_TIME      = 32;
  //
  //public JasiReadingIF addOrReplace(JasiReadingIF jr, int filterByMask);
  //public JasiReadingIF getSame(JasiReadingIF jr, int filterByMask, int delete_state);
  //
  // See notes on addOrReplace in JasiCommitableListIF code
  //
  public JasiReadingIF addOrReplace(JasiReadingIF ar);
  public void addOrReplaceAllOfSameChanType(JasiReadingIF jr);
  public void addOrReplaceAllOfSameStaType(JasiReadingIF jr);
  public void addOrReplaceAllOfSameChanType(JasiReadingIF jr, boolean autoOnly);
  public void addOrReplaceAllOfSameStaType(JasiReadingIF jr, boolean autoOnly);
  public JasiReadingIF addOrReplaceWithSameTime(JasiReadingIF jr);
  public JasiReadingIF addOrReplaceLikeWithSameTime(JasiReadingIF jr);
  public JasiReadingIF getSameChanType(JasiReadingIF jr);
  public JasiReadingIF getSameChanType(JasiReadingIF jr, boolean undeletedOnly);
  public JasiReadingIF getLikeChanType(JasiReadingIF jr);
  public JasiReadingIF getLikeChanType(JasiReadingIF jr, boolean undeletedOnly);
  public JasiReadingIF getLikeWithSameTime(JasiReadingIF jr);
  public JasiReadingIF getLikeWithSameTime(JasiReadingIF jr, boolean undeletedOnly);

  public String getNeatHeader();
  public JasiReadingIF getJasiReading(int idx);
  public JasiReadingListIF getAutomaticReadings();
  public int eraseAllAutomatic();

  public ChannelableListIF getAssociatedByChannel(Channel chan);
  public int matchChannelsWithList(ChannelList channelList);
  //implement to use input list to set channel in this list's elements

  public int getChannelUsedCount(); 
  public int getStationUsedCount(); 

  public void timeSort(boolean ascends);
  public void channelSort(boolean ascends, int type);
  public void distanceSort(boolean ascends, int type);

  public JasiReadingListIF getTimeSortedList(boolean ascends);
  public JasiReadingListIF getDistanceSortedList(boolean ascends);
  public JasiReadingListIF getContributingReadings(); // undeleted, wt > 0.

  public JasiReadingIF getNearestToTime(double time);
  public JasiReadingIF getNearestToTime(double time, Channel chan);
  public JasiReadingIF getNearestToTime(double time, Solution sol);

  public int rejectByResidual(double val, Solution sol);
  public int rejectByDistance(double val, Solution sol);
  public int rejectByResidual(double val);
  public int rejectByDistance(double val);
}
