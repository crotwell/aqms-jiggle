package org.trinet.jasi;

import java.util.*;
/**
 * Provides comparator() method to sort a list of Channelable objects
 * alphabetically (lexically) by network, name with component order
 * as found by ComponentSorter() order, if components are identical, then
 * in lexical location order. <p>
 * @see: ComponentSorter
 */

public class ChannelNameSorter implements Comparator {

    /** Comparator for component sort. */
    ComponentSorter componentSorter = new ComponentSorter();
    String st1 = null;
    String st2 = null;
    int diff = 0;

    /** Comparator function. */
    public int compare(Object o1, Object o2) {

      if ((o1 instanceof Channelable) && (o2 instanceof Channelable)) {

        Channel ch1 = ((Channelable) o1).getChannelObj();
        Channel ch2 = ((Channelable) o2).getChannelObj();

        // Sort on net/sta concatenated, e.g. "NETSTA"
        st1 = ch1.getNet().trim() + ch1.getSta().trim();
        st2 = ch2.getNet().trim() + ch2.getSta().trim();

        // "lexical" sort by NETSTA
        diff =  st1.compareTo(st2);
        if (diff != 0) return diff;

        // NOTE order by LOCATION before SEEDCHAN, else interleaved - aww 2011/08/11
        // same NETSTA, now try location
        diff = ch1.getLocation().compareTo(ch2.getLocation());
        if (diff != 0) return diff;

        // same location, now try component
        diff = componentSorter.compare(ch1.getSeedchan(), ch2.getSeedchan());

        return diff;

      }

      return 0; // wrong object types

    }
} // end of ChannelNameSorter

