package org.trinet.jasi;
public interface JasiCommitableIF extends ParseableJasiObjectIF, JasiSourceAssociationIF {
  //public JasiObject parseData(Object data); // in super interface
  // convenience used to toggle delete/undelete state:
  public boolean setDeleteFlag(boolean tf);
  public boolean isDeleted();
  public boolean delete(); // virtual no side-effects, except stale summary
  public boolean undelete(); // setDeleteFlag(false), stale summary state
  public boolean commit() throws JasiCommitException;
  public boolean getNeedsCommit();
  // specific network subclasses implement a validity, integrity test ?
  // public boolean isCommitable() or isValid() // true => needed fields set ok
  public boolean hasChanged();
  public void setNeedsCommit(boolean tf);
  public Object getIdentifier(); // like long id from sequence
  public boolean idEquals(JasiCommitableIF jc);
  public boolean idEquals(Object id);
  public boolean equivalent(Object id); // object is descriptive of similar data entity
  public void setIdentifier(Object id); // like long id from sequence
  public JasiCommitableIF getById(Object id); // ? need to add implementation
  public Object getTypeQualifier(); // unknown? null?, descriptive string or object?
  public boolean isSameType(Object type); // use type qualifier?
  public boolean isSameType(JasiCommitableIF jc); // use type qualifier?
  public boolean isFromDataSource(); // data exists in datasource (not new or altered)
  public JasiDataReaderIF getDataReader();
  //public JasiDataWriterIF getDataWriter();
  // no equivalent "writer" object in the TN subclasses yet commit()
  // does the writing invoking dbaseInsert, dbaseAssocxxx which could
  // defined as part of a writer IF heirarchy
}
