package org.trinet.jasi.EW;

import java.sql.*;
import java.util.*;
import org.trinet.jasi.*;
import org.trinet.jasi.TN.ChannelTN;
import org.trinet.util.gazetteer.LatLonZ;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

//
// For now convenience to compile just extend TN class
// still need to implement abstract Channel methods here -aww
//
public class ChannelEW extends ChannelTN {

    public long idChan;
    EWChannel ewChannel;

    public ChannelEW() { }

    /**@todo: implement all org.trinet.jasi.Channel abstract methods*/

    public ChannelList readList(Connection conn, java.util.Date date)
    {
        // DK Cleanup do something with conn
        return(readList(date));
    }

    public static Channel lookUp(long idChan)
    {
        ChannelEW chan = new ChannelEW();
        chan.idChan = idChan;
        return(((Channel)chan).lookUp(((Channel)chan)));
    }

    public Channel lookUp(Channelable parm1) {
        return lookUp(parm1, new java.util.Date());
    }
    public Channel lookUp(Channelable parm1, java.util.Date date) {
        Channel chTemp;

        this.setChannelObj(parm1.getChannelObj());

        EWChannel ewChannel = new EWChannel();

        ewChannel.idChan = this.idChan;
        ewChannel.tReqTime = (int) (date.getTime()/1000); //msec to sec
        ewChannel.Read();
        chTemp = EWChannel_2_ChannelEW(ewChannel);
        //
        //parm1.copy((Channelable) chTemp); // ddg removed // aww
        //
        return chTemp;
    }

    public ChannelList readList()
    {
        return readList(new java.util.Date());
    }

    public ChannelList readList(java.util.Date date)
    {
        if(ewChannel == null) ewChannel = new EWChannel();

        ewChannel.tReqTime = (int)((date.getTime())/1000 /*msec to sec */);
        Vector vEWChannelList = ewChannel.ReadList();

        ChannelList clChannels = new ChannelList(vEWChannelList.size());

        ChannelEW channelEW;
        for(int i=0; i<vEWChannelList.size(); i++)
        {
          channelEW = EWChannel_2_ChannelEW((EWChannel)(vEWChannelList.get(i)));
          if(channelEW != null) clChannels.add((Object)channelEW, false);
        }
        return(clChannels);
    }


    protected static ChannelEW EWChannel_2_ChannelEW(EWChannel ewChannel)
    {
        ChannelEW channelEW = new ChannelEW();

        channelEW.setChannelName(ewChannel.sNet, ewChannel.sSta, ewChannel.sComp, null, null, null, ewChannel.sComp, null);
        channelEW.idChan = ewChannel.idChan;
        channelEW.setLocation(ewChannel.sLoc);
        //channelEW.latlonz = new LatLonZ(ewChannel.dLat, ewChannel.dLon, ewChannel.dElev); // aww 06/11/2004
        // below chang elevation sign to use depth-axis convention, positive down
        channelEW.latlonz = new LatLonZ(ewChannel.dLat, ewChannel.dLon, -ewChannel.dElev); // aww 06/11/2004

        return(channelEW);
    }

    public boolean equals(Object x)
    {
        if (this == x) return(true);
        else if ( x == null) return(false);
        if(!(x instanceof ChannelEW)) return super.equals(x);
        return(this.idChan == ((ChannelEW)x).idChan);
    }

    public Object clone() {
        ChannelEW chan = (ChannelEW)(super.clone());
        chan.idChan = this.idChan;
        return chan;
    }

    public Channel copy(Channelable chan)
    {
        super.copy((Channelable) chan);
        ChannelEW chanEW = (ChannelEW) chan.getChannelObj();
        this.idChan = chanEW.idChan;
        this.ewChannel = chanEW.ewChannel;
        return this;
    }

}
