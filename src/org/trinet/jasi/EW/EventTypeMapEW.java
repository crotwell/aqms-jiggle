package org.trinet.jasi.EW;

import org.trinet.jasi.EventTypeMap;

/** Concrete class that defines how event types are mapped in the TriNet schema. */
public class EventTypeMapEW extends EventTypeMap {


    public static final int EW_FLAG_ET_ALL       =     -1;
    public static final int EW_FLAG_ET_QUAKE     =      2;
    public static final int EW_FLAG_ET_QUARRY    =      3;
    public static final int EW_FLAG_ET_REGIONAL  =      4;
    public static final int EW_FLAG_ET_TELESEISM =      5;
    public static final int EW_FLAG_ET_SONIC     =      6;
    public static final int EW_FLAG_ET_NUCLEAR   =      7;
    public static final int EW_FLAG_ET_TRIGGER   =      8;
    public static final int EW_FLAG_ET_TREMOR    =      9;
    public static final int EW_FLAG_ET_MANUAL    =      10;
    public static final int EW_FLAG_ET_UNKOWN    =      11;
    public static final int EW_FLAG_ET_BOGUS     =      12;

    /** Define EWDB event type numbers. */
    static int EWEventTypes[] = {EW_FLAG_ET_QUAKE, EW_FLAG_ET_QUARRY,
                                 EW_FLAG_ET_REGIONAL, EW_FLAG_ET_TELESEISM,
                                 EW_FLAG_ET_SONIC, EW_FLAG_ET_TRIGGER,
                                 EW_FLAG_ET_UNKOWN
				};


    static int iDefaultType = EW_FLAG_ET_QUAKE;

    public EventTypeMapEW()  {}
    
    public void loadEventTypes() { }
}
