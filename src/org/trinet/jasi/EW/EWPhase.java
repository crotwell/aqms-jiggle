
// *** PACKAGE NAME *** \
package org.trinet.jasi.EW;


// *** IMPORTS *** \
import java.util.*;
import java.sql.Connection;
import org.trinet.jasi.EW.SQL.EWSQLStatement;

// individual SQL calls
import org.trinet.jasi.EW.SQL.GetArrivalsStatement;
import org.trinet.jasi.EW.SQL.CreatePickStatement;
import org.trinet.jasi.EW.SQL.CreateOriginPickStatement;

/**
 * <!-- Class Description>
 *
 *
 * Created:  2002/06/19
 *
 * @author   ew_oci2java Tool / DK
 * @version  0.99
 **/



public class EWPhase
{

  // *** CLASS ATTRIBUTES *** \

  // *** CLASS ATTRIBUTES *** \
  public long idPick; // Description
  public String sSource; // Description
  public String sSourcePickID; // Description
  public long idChan; // Description
  public String sPhase; // Description
  public double tPhase; // Description
  public String cMotion; // Description
  public String cOnset; // Description
  public double dSigma; // Description
  public long idOriginPick; // Description
  public long idOrigin; // Description
  public String sCalcPhase; // Description
  public double tCalcPhase; // Description
  public float dWeight; // Description
  public float dDist; // Description
  public float dAzm; // Description
  public float dTakeoff; // Description
  public double tResPick; // Description

  // bind to event
  public int bBindToEvent; // Description
  public long idEvent; // Description


  // *** CLASS SQL STATEMENTS *** \
  EWSQLStatement es1;
  EWSQLStatement es2;
  EWSQLStatement es3;


  // *** CONSTRUCTORS *** \
  public EWPhase()
  {
    es1 = new GetArrivalsStatement(org.trinet.jasi.DataSource.getConnection());
    es2 = new CreatePickStatement(org.trinet.jasi.DataSource.getConnection());
    es3 = new CreateOriginPickStatement(org.trinet.jasi.DataSource.getConnection());
  }


  // *** CLASS METHODS *** \
  public void SetConnection(Connection IN_conn)
  {
    es1.SetConnection(IN_conn);
    es2.SetConnection(IN_conn);
    es3.SetConnection(IN_conn);
  }


  public Vector ReadList()
  {
    Vector ResultList = new Vector();
     int    iRetCode;

    es1.CallQueryStatement((Object)this, ResultList);
    return(ResultList);
  }  // end EWPhase:ReadList()

  public EWPhase Write()
  {

    if(es2.CallStatement((Object)this))
      return(this);
    else
      return(null);
  }  // end EWPhase:Write()

  public EWPhase Associate()
  {

    if(es3.CallStatement((Object)this))
      return(this);
    else
      return(null);
  }  // end EWPhase:Associate()



}  // end class EWPhase
