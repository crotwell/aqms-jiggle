package org.trinet.jasi.EW;

import org.trinet.jasi.*;
import java.util.Collection;
import org.usgs.util.ByteStreamParser;
import org.usgs.util.TraceBuf;
import java.util.ArrayList;
import org.trinet.jasi.EW.EWWaveform;
import org.trinet.util.gazetteer.LatLonZ;
import java.util.Vector;
import org.trinet.jdbc.datatypes.DataLong;
import org.trinet.jdbc.datatypes.DataDouble;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

public class WaveformEW extends Wavelet
{

  EWWaveform ewWaveform;

  public WaveformEW()
  {
  }

  public String getFileRoot()
  {
    /** we don't use files*/
    return(null);
  }

  protected boolean EWWaveform_2_WaveformEW(EWWaveform IN_ewWaveform)
  {
    ChannelEW chan;

    if(IN_ewWaveform == null)
      return(false);

    // Waveform Info
    // tStart, tEnd               : Data range
    // iDataFormat                : Binary Data Type
    // iByteLen                   : Binary Length
    this.ewWaveform = IN_ewWaveform;
    this.wfid = new DataLong(ewWaveform.idWaveform);
    this.setStart(new DataDouble(IN_ewWaveform.tStart));
    this.setEnd(new DataDouble(IN_ewWaveform.tEnd));
    // keep all of the binary stuff in the EWWaveform object.

    //channel info
    //  tOff, tOn                 : validity range
    //  sSta, sComp, sNet, sLoc   : name
    //  dLat, dLon, dElev         : LatLonZ
    //  dAzm, dDip                : Component Orientation
    chan = new ChannelEW();
    chan.setChannelName(ewWaveform.sNet, ewWaveform.sSta, ewWaveform.sComp, null, null, null, ewWaveform.sComp, null);
    chan.idChan = ewWaveform.idChan;
    chan.setLocation(ewWaveform.sLoc);
    //chan.setLatLonZ(new LatLonZ(ewWaveform.dLat, ewWaveform.dLon, ewWaveform.dElev)); // aww 06/11/2004
    chan.setLatLonZ(new LatLonZ(ewWaveform.dLat, ewWaveform.dLon, -ewWaveform.dElev)); // change sign to depth-axis convention aww 06/11/2004
    // no component azimuth and dip in jiggle for now (I think) DK CLEANUP

    this.setChannelObj((Channel)chan);

    return(true);
  }

  //public boolean makeRequest(long evid, String archiveSrc, double start, double end) { return false; }

  public Collection getByChannel(Channelable ch, long evid) {
      return null;
  }

  public int getCountBySolution(Solution sol) {
    return (sol == null) ? 0 : getCountBySolution(sol.getId().longValue());
  }

  public int getCountBySolution(long id) {
      // need to implement in ewWaveform class a prepared count query!
      //KLUDGE: 
      Collection col = getBySolution(id);
      return (col == null) ? 0 : col.size();
  }

  public Collection getBySolution(long id)
  {
    Vector vEWWaveformList;
    if(ewWaveform == null)
    {
      ewWaveform = new EWWaveform();
    }
    ewWaveform.idEvent = id;
    vEWWaveformList = ewWaveform.ReadListwCompInfo();
    if(vEWWaveformList == null)
      return(null);

    Vector vWaveformEWList = new Vector(vEWWaveformList.size());
    WaveformEW wfEW;

    for(int i=0; i < vEWWaveformList.size(); i++)
    {
      wfEW = (WaveformEW)(Wavelet.create());
      if(!wfEW.EWWaveform_2_WaveformEW((EWWaveform)(vEWWaveformList.get(i))))
        continue;
      vWaveformEWList.add((Object)wfEW);
    }

    return((Collection)vWaveformEWList);
  }

  public Collection getBySolution(Solution sol)
  {
    if(sol == null)
      return(null);
    else
      return(getBySolution(sol.id.longValue()));
  }

  public boolean loadTimeSeries(double startTime, double endTime)
  {
    // DK Cleanup:  Just load the whole thing for now
    return(loadTimeSeries());
  }

  public boolean loadTimeSeries()
  {
    int iRetCode;

    /**@todo: implement this org.trinet.jasi.Waveform abstract method*/
    if(this.wfid.longValue() == 0 || this.ewWaveform == null)
      return(false);

    synchronized (this)
    {      // synchronize for thread safty

      ewWaveform.ReadSnippet();
      iRetCode = ba_2_TimeSeries(ewWaveform.RawSnippet);

      if(iRetCode == 0)
      {

        // concatinate contiguous segments
        collapse();

        // set max, min, bias values
        scanForAmps();
        scanForBias();

        // Notify any listeners that Waveform state has changed
        // (changeListener logic is in jasiObject())
        fireStateChanged();
      }
    }

    return(iRetCode == 0);
  }  // end WaveformEW.loadTimeSeries();


  protected int ba_2_TimeSeries(byte[] baSnippet)
  {

    int iRetCode = 0;
    TraceBuf tb;
    WFSegment wfseg;

    segList = new ArrayList();  // must instantiate so we can use size()
    ByteStreamParser bspStream = new ByteStreamParser(baSnippet);

    while(true)
    {
      tb = new TraceBuf();
      tb.SetChannel(this.getChannelObj());
      iRetCode = tb.ParseHeader(bspStream);
      if(iRetCode != 0)
      {
        if(iRetCode == 1)
          iRetCode = 0;
        break;
      }
      if(this.samplesPerSecond.equals(new DataDouble()) )
        this.samplesPerSecond = new DataDouble(tb.dSampleRate);

      iRetCode = tb.ParseSampleData(bspStream);
      if(iRetCode != 0)
        break;

      wfseg = tb.toWFSegment();
      if(wfseg == null)
      {
        iRetCode = -11;
        break;
      }

      segList.add((Object)wfseg);

      if(this.getChannelObj() == null)
        this.setChannelObj(tb.GetChannel());
    }  // end while

    return(iRetCode);
  }  // end WaveformEW::ba_2_TimeSeries

  public Wavelet getByWaveformId(long wfid)
  {
    if(ewWaveform == null)
    {
      ewWaveform = new EWWaveform();
    }
    ewWaveform.idWaveform = wfid;
    ewWaveform.Read();
    EWWaveform_2_WaveformEW(this.ewWaveform);
    return(null);
  }
  public boolean commit(Solution sol)
  {
    /**@todo: implement this org.trinet.jasi.Waveform abstract method*/
    return(false);
  }
  public String getFileRoot(long eventId)
  {
    /**@todo: implement this org.trinet.jasi.Waveform abstract method*/
    return(null);
  }
}
