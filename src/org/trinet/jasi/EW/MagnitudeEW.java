package org.trinet.jasi.EW;

import java.util.*;
import java.sql.*;
import org.trinet.jasi.*;
import org.trinet.jasi.TN.*;
import org.trinet.jdbc.*;
import org.trinet.jdbc.table.*;
import org.trinet.jdbc.datatypes.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

//
// For now convenience to compile just extend TN class
// still need to implement abstract Magnitude methods here
//
public class MagnitudeEW extends MagnitudeTN { // -aww

    public static final int MagType_Undefined       = 0;
    public static final int MagType_Local_Peak2Peak = 1;
    public static final int MagType_Moment          = 2;
    public static final int MagType_Body_Wave       = 3;
    public static final int MagType_Surface_Wave    = 4;
    public static final int MagType_ScalarMoment    = 5;
    public static final int MagType_Duration        = 6;
    public static final int MagType_Local_Zero2Peak = 7;
    public static final int MagType_Mblg            = 8;


    EWMagnitude ewMag;
    int iMagType;

    public MagnitudeEW()
    {
        this.magid = new DataLong();
    }
    public boolean hasChanged() {
        /**@todo: implement this org.trinet.jasi.Magnitude abstract method*/
        return (false);
    }

    public Collection getBySolution(Connection conn, Solution sol) {
      /**@todo: implement this org.trinet.jasi.Magnitude abstract method*/
      return null; // not defined yet
    }

    public boolean commit() throws org.trinet.jasi.JasiCommitException
    {
      /**@todo: implement this org.trinet.jasi.Magnitude abstract method*/
      if(magid.equals(new DataLong()))
      {
      // we have a new magnitude, write it.
        toEWMag();
        ewMag.Write();
        if(ewMag.idMag <= 0) return(false);
        else magid = new DataLong(ewMag.idMag);
        // Now we have to deal with the stationmags
        if(this.iMagType == this.MagType_Duration)
        {
          // loop through the codalist and save every coda term
          // that doesn't have an idTCoda
          org.trinet.jasi.Coda[] codaArray = this.codaList.getArray();
          for(int i=0; i < codaArray.length; i++)
          {
            codaArray[i].commit();
          }

          /*****************************
           * NO Md CODE (no coda member in Phase)
          // 2) loop through the phaselist for the solution and save
          //    every coda/p-phase pair, where the coda does not contain
          //    an idDurCoda.  phases/codas are paired via phase.coda
          Phase[] phaseArray = this.sol.phaseList.getArray();
          for(int i=0; i < phaseArray.length; i++)
          {
            if(phaseArray[i].coda != null && (phaseArray[i].coda instanceof CodaEW))
            {
              ((CodaEW)phaseArray[i].coda).commitCodaDur(phaseArray[i]);
            }
          }
          **************************/
        }
        else
        {
        }
        this.setStale(false);
        return(true);
      }
      return false;
    }

    static DataString MagType_2_Subscript(int IN_iMagType)
    {
      switch(IN_iMagType)
      {
        case MagType_Local_Peak2Peak:
        case MagType_Local_Zero2Peak:
          return(new DataString("L"));
        case MagType_Moment:
          return(new DataString("w"));
        case MagType_Body_Wave:
          return(new DataString("b"));
        case MagType_Surface_Wave:
          return(new DataString("s"));
        case MagType_ScalarMoment:
          return(new DataString("wp"));
        case MagType_Duration:
          return(new DataString("d"));
        case MagType_Mblg:
          return(new DataString("blg"));
        default:
          return(new DataString(""));
      }  // end switch(MagType)
    }  // end MagType_2_Subscript()

    static int Subscript_2_MagType(String IN_sMagType)
    {
      int OUT_iMagType;
      if(IN_sMagType.equalsIgnoreCase("d"))
        OUT_iMagType = MagType_Duration;
      else if(IN_sMagType.equalsIgnoreCase("L"))
        OUT_iMagType = MagType_Local_Peak2Peak;
      else if(IN_sMagType.equalsIgnoreCase("w"))
        OUT_iMagType = MagType_Moment;
      else if(IN_sMagType.equalsIgnoreCase("b"))
        OUT_iMagType = MagType_Body_Wave;
      else if(IN_sMagType.equalsIgnoreCase("s"))
        OUT_iMagType = MagType_Surface_Wave;
      else if(IN_sMagType.equalsIgnoreCase("wp"))
        OUT_iMagType = MagType_ScalarMoment;
      else if(IN_sMagType.equalsIgnoreCase("blg"))
        OUT_iMagType = MagType_Mblg;
      else
        OUT_iMagType = MagType_Undefined;
      return(OUT_iMagType);
    }

    EWMagnitude toEWMag()
    {
        ewMag = new EWMagnitude();
        ewMag.dMagAvg = this.value.floatValue();
        ewMag.dMagErr = this.error.floatValue();
        ewMag.iMagType = this.iMagType = this.Subscript_2_MagType(this.subScript.toString());
        ewMag.sSource = this.source.toString();
        ewMag.iNumMags = this.usedStations.intValue();
        ewMag.idOrigin = ((DataLong)this.sol.getPreferredOriginId()).longValue();
        ewMag.idEvent = this.sol.id.longValue();
        return(ewMag);
    }

    static MagnitudeEW EWMag2MagEW(EWMagnitude ewMag)
    {
        MagnitudeEW magEW = new MagnitudeEW();
        magEW.value = new DataDouble(ewMag.dMagAvg);
        magEW.error = new DataDouble(ewMag.dMagErr);
        magEW.subScript = MagType_2_Subscript(ewMag.iMagType);
        magEW.magid = new DataLong(ewMag.idMag);
        magEW.source = new DataString(ewMag.sSource);
        magEW.usedStations = new DataLong(ewMag.iNumMags);
        // DK CLEANUP  Are we doing all of the conversions we're supposed to
        return(magEW);
    }
}
//
// end class MagnitudeEW
//
