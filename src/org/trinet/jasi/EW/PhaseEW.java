package org.trinet.jasi.EW;

import java.util.*;
import org.trinet.jasi.*;
import org.trinet.jasi.TN.PhaseTN;
import org.trinet.jdbc.datatypes.*;
/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

//
// For now convenience to compile just extend TN class
// still need to implement abstract Phase methods here -aww
//
public class PhaseEW extends PhaseTN
{

    EWPhase ewPhase;
    long idOrigin;
    long idOriginPick;
    long idPick;
    public CodaEW codaEW;
    String xidPick;
  
    public PhaseEW()
    {
      long Temp_xidPick = System.currentTimeMillis();
      xidPick = String.valueOf(Temp_xidPick);
      this.idPick = 0;
      this.source = new DataString("Jiggle1");
    }
  
    public boolean replace(Phase parm1)
    {
      /**@todo: implement this org.trinet.jasi.Phase abstract method*/
      /* overwrite the existing phase with the new one. keep links */
      return(false);
    }

    public Collection getBySolution(Solution sol)
    {
      Collection phases;
  
      if(sol == null)
        return(null);
  
      if(ewPhase == null)
        ewPhase = new EWPhase();
  
      ewPhase.idOrigin = ((SolutionEW)sol).idOrigin;
  
      if(ewPhase.idOrigin <= 0)
        return(null);
  
      Vector vEWPhaseList;
      PhaseEW phaseEW;
  
      vEWPhaseList = ewPhase.ReadList();
      if(vEWPhaseList == null)
        return(null);
  
      Vector vPhaseEWList = new Vector(vEWPhaseList.size());
  
      for(int i=0; i < vEWPhaseList.size(); i++)
      {
        phaseEW = PhaseEW.EWPhase_2_PhaseEW((EWPhase)(vEWPhaseList.get(i)));
        if(phaseEW != null)
        {
          phaseEW.sol = sol;
          vPhaseEWList.add((Object)phaseEW);
        }
      }
  
      return((Collection)vPhaseEWList);
    }  // end PhaseEW::getBySolution()
  
  
    protected static PhaseEW EWPhase_2_PhaseEW(EWPhase ewPhase)
    {
      PhaseEW phaseEW = new PhaseEW();
  
      if(ewPhase == null)
        return(null);
  
  
      int iPickWt;
      if(ewPhase.dSigma <= .02)
       iPickWt = 0;
      else  if(ewPhase.dSigma <= .03)
       iPickWt = 1;
      else  if(ewPhase.dSigma <= .05)
       iPickWt = 2;
      else  if(ewPhase.dSigma <= .08)
       iPickWt = 3;
      else
       iPickWt = 4;
  
      phaseEW.changeDescription(ewPhase.sPhase,ewPhase.cOnset, ewPhase.cMotion,
                                Integer.toString(iPickWt));
      phaseEW.idOriginPick = ewPhase.idOriginPick;
      phaseEW.idPick = ewPhase.idPick;
      phaseEW.idOrigin = ewPhase.idOrigin;
      phaseEW.datetime = new DataDouble(ewPhase.tPhase);
      phaseEW.residual = new DataDouble(ewPhase.tResPick);
      phaseEW.description.setWeight((int)(ewPhase.dWeight));
      phaseEW.setHorizontalDistance(ewPhase.dDist);
      phaseEW.setAzimuth(ewPhase.dAzm);
      phaseEW.weightIn = new DataDouble(ewPhase.dSigma);
      phaseEW.emergenceAngle = new DataDouble(ewPhase.dTakeoff);
      phaseEW.setChannelObj(ChannelEW.lookUp(ewPhase.idChan));
      phaseEW.source = new DataString(ewPhase.sSource);
      phaseEW.xidPick = ewPhase.sSourcePickID;
  
  
      return(phaseEW);
    }
  
  
    protected boolean toEWPhase()
    {
      ewPhase = new EWPhase();
  
      ewPhase.sPhase = this.description.iphase;
      ewPhase.cOnset = this.description.ei;
      ewPhase.cMotion = new String(this.description.fm.substring(0,1));
  
      double dQual = this.description.getQuality();
      if(dQual > 0.75)
        ewPhase.dSigma = .02;
      else if(dQual > 0.5)
        ewPhase.dSigma = .03;
      else if(dQual > 0.25)
        ewPhase.dSigma = .05;
      else if(dQual > 0)
        ewPhase.dSigma = .08;
      else
        ewPhase.dSigma = 99.99;
  
      if(this.sol != null)
      {
        ewPhase.idEvent = this.sol.id.longValue();
        if(this.sol instanceof SolutionEW)
          ewPhase.idOrigin = (((SolutionEW)this.sol).GetidOrigin());
      }
  
      ewPhase.tPhase = datetime.doubleValue();
      ewPhase.tResPick = residual.doubleValue();
      ewPhase.dDist = (float)(this.getHorizontalDistance());
      ewPhase.dAzm = (float)this.getAzimuth();
      ewPhase.dTakeoff = emergenceAngle.floatValue();
      ewPhase.sSource = this.source.toString();
      ewPhase.idChan = ((ChannelEW)this.getChannelObj()).idChan;
      ewPhase.dWeight = (float)this.weightOut.doubleValue();
      ewPhase.sSourcePickID = this.xidPick;
  
      ewPhase.idPick = this.idPick;
      return(true);
    }
  
  
    public boolean commit()
    {
      /**@todo: implement this org.trinet.jasi.JasiReading abstract method*/
      if(this.idPick <= 0)
      {
        if(ewPhase == null)
          ewPhase = new EWPhase();
  
        toEWPhase();
        ewPhase.Write();
        if(ewPhase.idPick <= 0)
        {
          return(false);
        }
        else
        {
          // copy the idPick from ewPhase back to this object
          this.idPick = ewPhase.idPick;
          return(true);
        }
      }
      return(true);
    }  // end PhaseEW::commit()
  
    public Collection getBySolution(long id)
    {
      Solution sol = Solution.create().getById(id);
      return(getBySolution(sol));
    }  //end PhaseEW::getBySolution()
  
    boolean associate()
    {
      if(this.idOriginPick <= 0)
      {
        toEWPhase();
        ewPhase.Associate();
      }
  
      if(ewPhase.idOriginPick <= 0)
      {
        return(false);
      }
      else
      {
        // copy the idPick from ewPhase back to this object
        this.idOriginPick = ewPhase.idOriginPick;
        return(true);
      }
    }
  
    public boolean copySolutionDependentData ( Phase newPhase)
    {
      this.idOrigin=0;
      this.idOriginPick=0;
  /*****************************
   * NO Md CODE (no coda member in Phase)
      if((this.coda != null)  && (this.coda instanceof CodaEW))
      {
        ((CodaEW)this.coda).idCodaDur = 0;
        ((CodaEW)this.coda).idMagLink = 0;
      }
   ********************************/
          if (isLikeChanType(newPhase)) { // no association check
              residual  = newPhase.residual;
              weightIn  = newPhase.weightIn;
              weightOut = newPhase.weightOut;
              emergenceAngle = newPhase.emergenceAngle;
  
              setDistance(newPhase.getDistance());
              setHorizontalDistance(newPhase.getHorizontalDistance()); //aww
              setAzimuth(newPhase.getAzimuth());
  
              return true;
          } else {
              return false;
          }
    }
}
//
// end class PhaseEW
//
