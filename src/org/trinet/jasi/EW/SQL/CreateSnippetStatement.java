// *** PACKAGE NAME *** \
package org.trinet.jasi.EW.SQL;


// *** IMPORTS *** \
import java.util.*;
import java.sql.Connection;
import java.sql.SQLException;
import org.trinet.jasi.EW.EWWaveform;


/**
 * <!-- Class Description>
 *
 *
 * Created:  2002/06/17
 *
 * @author   ew_oci2java Tool / DK
 * @version  0.99
 **/

public class CreateSnippetStatement extends EWSQLStatement
{

  // *** CLASS ATTRIBUTES *** \

  // *** CONSTRUCTORS *** \
  public CreateSnippetStatement()
  {
    sSQLStatement = new String("insert into Waveform(idWaveform,binSnippet) values(:IN_idWaveform,:IN_RawSnippet)");
    bIsQuery = false;
    init();
  }


  public CreateSnippetStatement(Connection IN_conn)
  {
    this();
    SetConnection(IN_conn);
  }


  // *** CLASS METHODS *** \

  protected boolean SetInputParams(Object obj)
  {

    EWWaveform objEWWaveform = (EWWaveform)obj;

    try
    {
      cs.setLong(1, objEWWaveform.idWaveform);
      cs.setBinaryStream(2, isRawSnippet, objEWWaveform.iByteLen);
    }
    catch (SQLException ex)
    {
      System.err.println("Exception in CreateSnippetStatement:SetInputParams()");
      System.err.println(ex);
      ex.printStackTrace();
      return(false);
    }

    return(true);
  }   // end CreateSnippetStatement::SetInputParams()


  boolean ConfigureLongRaw(EWWaveform ewObject)
	 {
	   isRawSnippet = new java.io.ByteArrayInputStream(ewObject.RawSnippet);
	   return(true);
	 }


  java.io.InputStream isRawSnippet;

} // end class CreateSnippetStatement


//             <EOF>
