// *** PACKAGE NAME *** \                                     
package org.trinet.jasi.EW.SQL;                                
                                                               
                                                               
// *** IMPORTS *** \                                          
import java.util.*;                                            
import java.sql.Connection;                                    
import java.sql.SQLException;                                  
import org.trinet.jasi.EW.EWWaveform;                                  
                                                               
                                                               
/**                                    
 * <!-- Class Description>             
 *                                     
 *                                     
 * Created:  2002/06/17                        
 *                                     
 * @author   ew_oci2java Tool / DK                        
 * @version  0.99                        
 **/                                   
                                       
public class GetWaveformDescStatement extends EWSQLStatement 
{                                      
                                       
  // *** CLASS ATTRIBUTES *** \       
                                       
  // *** CONSTRUCTORS *** \           
  public GetWaveformDescStatement()                          
  {                                    
    sSQLStatement = new String("Begin Get_WaveformDesc(OUT_RetCode => :OUT_RetCode, IN_idWaveform => :IN_idWaveform, OUT_idChan => :OUT_idChan, OUT_tStart => :OUT_tStart,  OUT_tEnd => :OUT_tEnd, OUT_iDataFormat => :OUT_iDataFormat,  OUT_iByteLen => :OUT_iByteLen); End;");
    bIsQuery = false;                
    init();                            
  }                                    
                                       
                                       
  public GetWaveformDescStatement(Connection IN_conn)        
  {                                    
    this();                            
    SetConnection(IN_conn);            
  }                                    
                                       
                                       
  // *** CLASS METHODS *** \          
                                       
  protected boolean SetInputParams(Object obj)    
  {                                  
                                     
    EWWaveform objEWWaveform = (EWWaveform)obj;              
                                     
    try                              
    {                                
      cs.setLong(2, objEWWaveform.idWaveform);    
    }                                
    catch (SQLException ex)          
    {                                
      System.err.println("Exception in GetWaveformDescStatement:SetInputParams()"); 
      System.err.println(ex);        
      ex.printStackTrace();          
      return(false);                 
    }                                
                                     
    return(true);                    
  }   // end GetWaveformDescStatement::SetInputParams()    
                                     
                                     
  protected boolean RegisterOutputParams()    
  {                                  
    try                              
    {                                
      cs.registerOutParameter(1, java.sql.Types.INTEGER); 
      cs.registerOutParameter(3, java.sql.Types.BIGINT); 
      cs.registerOutParameter(4, java.sql.Types.DOUBLE); 
      cs.registerOutParameter(5, java.sql.Types.DOUBLE); 
      cs.registerOutParameter(6, java.sql.Types.INTEGER); 
      cs.registerOutParameter(7, java.sql.Types.INTEGER); 
    }                                
    catch (SQLException ex)          
    {                                
      System.err.println("Exception in GetWaveformDescStatement:RegisterOutputParams()"); 
      System.err.println(ex);        
      ex.printStackTrace();          
      return(false);                 
    }                                
    return(true);                    
  }  // end GetWaveformDescStatement:RegisterOutputParams()
                                     
                                     
  protected boolean RetrieveOutputParams(Object obj)
  {                                  
    EWWaveform objEWWaveform = (EWWaveform)obj;             
                                     
    try                              
    {                                
      objEWWaveform.RetCode=cs.getInt(1);     
      objEWWaveform.idChan=cs.getLong(3);     
      objEWWaveform.tStart=cs.getDouble(4);     
      objEWWaveform.tEnd=cs.getDouble(5);     
      objEWWaveform.iDataFormat=cs.getInt(6);     
      objEWWaveform.iByteLen=cs.getInt(7);     
    }                                
    catch (SQLException ex)          
    {                                
      System.err.println("Exception in GetWaveformDescStatement:RetrieveOutputParams()"); 
      System.err.println(ex);        
      ex.printStackTrace();          
      return(false);                 
    }                                
                                     
    return(true);                    
  }   // end GetWaveformDescStatement::RetrieveOutputParams()
                                     
                                     
} // end class GetWaveformDescStatement                      
                                       
                                       
//             <EOF>                   
