// *** PACKAGE NAME *** \
package org.trinet.jasi.EW.SQL;


// *** IMPORTS *** \
import java.util.*;
import java.sql.Connection;
import java.sql.SQLException;
import org.trinet.jasi.EW.EWPhase;


/**
 * <!-- Class Description>
 *
 *
 * Created:  2002/08/28
 *
 * @author   ew_oci2java Tool / DK
 * @version  0.99
 **/

public class CreateOriginPickStatement extends EWSQLStatement
{

  // *** CLASS ATTRIBUTES *** \

  // *** CONSTRUCTORS *** \
  public CreateOriginPickStatement()
  {
    sSQLStatement = new String("Begin Create_OriginPick(OUT_idOP => :OUT_idOP,OUT_idPick => :OUT_idPick,IN_idOrigin => :IN_idOrigin,IN_sSource => :IN_sSource,IN_sSourcePickID => :IN_sSourcePickID,IN_sPhase => :IN_sPhase,IN_tPhase => :IN_tPhase,IN_dWeight => :IN_dWeight,IN_dDist => :IN_dDist,IN_dAzm => :IN_dAzm,IN_dTakeoff => :IN_dTakeoff,IN_tResPick => :IN_tResPick); End;");
    bIsQuery = false;
    init();
  }


  public CreateOriginPickStatement(Connection IN_conn)
  {
    this();
    SetConnection(IN_conn);
  }


  // *** CLASS METHODS *** \

  protected boolean SetInputParams(Object obj)
  {

    EWPhase objEWPhase = (EWPhase)obj;

    try
    {
      cs.setLong(2, objEWPhase.idPick);
      cs.setLong(3, objEWPhase.idOrigin);
      cs.setString(4, objEWPhase.sSource);
      cs.setString(5, objEWPhase.sSourcePickID);
      cs.setString(6, objEWPhase.sPhase);
      cs.setDouble(7, objEWPhase.tPhase);
      cs.setDouble(8, objEWPhase.dWeight);
      cs.setDouble(9, objEWPhase.dDist);
      cs.setDouble(10, objEWPhase.dAzm);
      cs.setDouble(11, objEWPhase.dTakeoff);
      cs.setDouble(12, objEWPhase.tResPick);
    }
    catch (SQLException ex)
    {
      System.err.println("Exception in CreateOriginPickStatement:SetInputParams()");
      System.err.println(ex);
      ex.printStackTrace();
      return(false);
    }

    return(true);
  }   // end CreateOriginPickStatement::SetInputParams()


  protected boolean RegisterOutputParams()
  {
    try
    {
      cs.registerOutParameter(1, java.sql.Types.BIGINT);
      cs.registerOutParameter(2, java.sql.Types.BIGINT);
    }
    catch (SQLException ex)
    {
      System.err.println("Exception in CreateOriginPickStatement:RegisterOutputParams()");
      System.err.println(ex);
      ex.printStackTrace();
      return(false);
    }
    return(true);
  }  // end CreateOriginPickStatement:RegisterOutputParams()


  protected boolean RetrieveOutputParams(Object obj)
  {
    EWPhase objEWPhase = (EWPhase)obj;

    try
    {
      objEWPhase.idOriginPick=cs.getLong(1);     // DK was idOP
      objEWPhase.idPick=cs.getLong(2);
    }
    catch (SQLException ex)
    {
      System.err.println("Exception in CreateOriginPickStatement:RetrieveOutputParams()");
      System.err.println(ex);
      ex.printStackTrace();
      return(false);
    }

    return(true);
  }   // end CreateOriginPickStatement::RetrieveOutputParams()


} // end class CreateOriginPickStatement


//             <EOF>
