// *** PACKAGE NAME *** \                                     
package org.trinet.jasi.EW.SQL;                                
                                                               
                                                               
// *** IMPORTS *** \                                          
import java.util.*;                                            
import java.sql.Connection;                                    
import java.sql.SQLException;                                  
import org.trinet.jasi.EW.EWCoda;                                  
                                                               
                                                               
/**                                    
 * <!-- Class Description>             
 *                                     
 *                                     
 * Created:  2002/07/19                        
 *                                     
 * @author   ew_oci2java Tool / DK                        
 * @version  0.99                        
 **/                                   
                                       
public class GetStaMagCodaStatement extends EWSQLStatement 
{                                      
                                       
  // *** CLASS ATTRIBUTES *** \       
                                       
  // *** CONSTRUCTORS *** \           
  public GetStaMagCodaStatement()                          
  {                                    
    sSQLStatement = new String("select idMagLink, idMeasurement idCodaDur, dMag, dWeight, idChan,   tCodaDurObs, tCodaDurXtp, tCodaTermObs, tCodaTermXtp,   idTCoda, iMagType  from StaMag_CodaDur_Info where idMag=:idMagnitude");
    bIsQuery = true;                 
    init();                            
  }                                    
                                       
                                       
  public GetStaMagCodaStatement(Connection IN_conn)        
  {                                    
    this();                            
    SetConnection(IN_conn);            
  }                                    
                                       
                                       
  // *** CLASS METHODS *** \          
                                       
  protected boolean SetInputParams(Object obj)    
  {                                  
                                     
    EWCoda objEWCoda = (EWCoda)obj;              
                                     
    try                              
    {                                
      cs.setLong(1, objEWCoda.idMagnitude);    
    }                                
    catch (SQLException ex)          
    {                                
      System.err.println("Exception in GetStaMagCodaStatement:SetInputParams()"); 
      System.err.println(ex);        
      ex.printStackTrace();          
      return(false);                 
    }                                
                                     
    return(true);                    
  }   // end GetStaMagCodaStatement::SetInputParams()    
                                     
                                     
  protected boolean RetrieveOutputParamsList(Vector OutputObjectList) 
  {                                                      
                                                         
     EWCoda objEWCoda;                                           
                                                         
    try                              
    {                                
    while(rs.next())                                     
    {                                                    
      objEWCoda = new EWCoda();                                  
                                                         
      objEWCoda.idMagLink=rs.getLong(1);                      
      objEWCoda.idCodaDur=rs.getLong(2);                      
      objEWCoda.dMag=rs.getFloat(3);                      
      objEWCoda.dWeight=rs.getFloat(4);                      
      objEWCoda.idChan=rs.getLong(5);                      
      objEWCoda.tCodaDurObs=rs.getDouble(6);                      
      objEWCoda.tCodaDurXtp=rs.getDouble(7);                      
      objEWCoda.tCodaTermObs=rs.getDouble(8);                      
      objEWCoda.tCodaTermXtp=rs.getDouble(9);                      
      objEWCoda.idTCoda=rs.getLong(10);                      
      objEWCoda.iMagType=rs.getInt(11);                      
      OutputObjectList.addElement(objEWCoda);                
    }  // end while(rs.next)                             
    }                                
    catch (SQLException ex)          
    {                                
      System.err.println("Exception in GetStaMagCodaStatement:RetrieveOutputParamsList()"); 
      System.err.println(ex);        
      ex.printStackTrace();          
      return(false);                 
    }                                
                                                         
    return(true);                                        
  }   // end GetStaMagCodaStatement::RetrieveOutputParamsList()              
                                                         
                                                         
} // end class GetStaMagCodaStatement                      
                                       
                                       
//             <EOF>                   
