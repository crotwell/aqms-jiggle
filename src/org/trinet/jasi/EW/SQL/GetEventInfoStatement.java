// *** PACKAGE NAME *** \                                     
package org.trinet.jasi.EW.SQL;                                
                                                               
                                                               
// *** IMPORTS *** \                                          
import java.util.*;                                            
import java.sql.Connection;                                    
import java.sql.SQLException;                                  
import org.trinet.jasi.EW.EWSolution;                                  
                                                               
                                                               
/**                                    
 * <!-- Class Description>             
 *                                     
 *                                     
 * Created:  2002/06/12                        
 *                                     
 * @author   ew_oci2java Tool / DK                        
 * @version  0.99                        
 **/                                   
                                       
public class GetEventInfoStatement extends EWSQLStatement 
{                                      
                                       
  // *** CLASS ATTRIBUTES *** \       
                                       
  // *** CONSTRUCTORS *** \           
  public GetEventInfoStatement()                          
  {                                    
    sSQLStatement = new String("Begin Get_Event_Info(OUT_Retcode => :OUT_Retcode, IN_idEvent => :IN_idEvent,  OUT_tiEventType => :OUT_tiEventType,  OUT_iDubiocity => :OUT_iDubiocity, OUT_bArchived => :OUT_bArchived,  OUT_sComment => :OUT_sComment); End;");
    bIsQuery = false;                
    init();                            
  }                                    
                                       
                                       
  public GetEventInfoStatement(Connection IN_conn)        
  {                                    
    this();                            
    SetConnection(IN_conn);            
  }                                    
                                       
                                       
  // *** CLASS METHODS *** \          
                                       
  protected boolean SetInputParams(Object obj)    
  {                                  
                                     
    EWSolution objEWSolution = (EWSolution)obj;              
                                     
    try                              
    {                                
      cs.setLong(2, objEWSolution.idEvent);    
    }                                
    catch (SQLException ex)          
    {                                
      System.err.println("Exception in GetEventInfoStatement:SetInputParams()"); 
      System.err.println(ex);        
      ex.printStackTrace();          
      return(false);                 
    }                                
                                     
    return(true);                    
  }   // end GetEventInfoStatement::SetInputParams()    
                                     
                                     
  protected boolean RegisterOutputParams()    
  {                                  
    try                              
    {                                
      cs.registerOutParameter(1, java.sql.Types.INTEGER); 
      cs.registerOutParameter(3, java.sql.Types.INTEGER); 
      cs.registerOutParameter(4, java.sql.Types.INTEGER); 
      cs.registerOutParameter(5, java.sql.Types.INTEGER); 
      cs.registerOutParameter(6, java.sql.Types.VARCHAR); 
    }                                
    catch (SQLException ex)          
    {                                
      System.err.println("Exception in GetEventInfoStatement:RegisterOutputParams()"); 
      System.err.println(ex);        
      ex.printStackTrace();          
      return(false);                 
    }                                
    return(true);                    
  }  // end GetEventInfoStatement:RegisterOutputParams()
                                     
                                     
  protected boolean RetrieveOutputParams(Object obj)
  {                                  
    EWSolution objEWSolution = (EWSolution)obj;             
                                     
    try                              
    {                                
      objEWSolution.Retcode=cs.getInt(1);     
      objEWSolution.tiEventType=cs.getInt(3);     
      objEWSolution.iDubiocity=cs.getInt(4);     
      objEWSolution.bArchived=cs.getInt(5);     
      objEWSolution.sComment=cs.getString(6);     
    }                                
    catch (SQLException ex)          
    {                                
      System.err.println("Exception in GetEventInfoStatement:RetrieveOutputParams()"); 
      System.err.println(ex);        
      ex.printStackTrace();          
      return(false);                 
    }                                
                                     
    return(true);                    
  }   // end GetEventInfoStatement::RetrieveOutputParams()
                                     
                                     
} // end class GetEventInfoStatement                      
                                       
                                       
//             <EOF>                   
