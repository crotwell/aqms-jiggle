// *** PACKAGE NAME *** \                                     
package org.trinet.jasi.EW.SQL;                                
                                                               
                                                               
// *** IMPORTS *** \                                          
import java.util.*;                                            
import java.sql.Connection;                                    
import java.sql.SQLException;                                  
import org.trinet.jasi.EW.EWMagnitude;                                  
                                                               
                                                               
/**                                    
 * <!-- Class Description>             
 *                                     
 *                                     
 * Created:  2002/06/12                        
 *                                     
 * @author   ew_oci2java Tool / DK                        
 * @version  0.99                        
 **/                                   
                                       
public class GetMagListStatement extends EWSQLStatement 
{                                      
                                       
  // *** CLASS ATTRIBUTES *** \       
                                       
  // *** CONSTRUCTORS *** \           
  public GetMagListStatement()                          
  {                                    
    sSQLStatement = new String("select idMag, idOrigin, dMagAvg, iNumMags, dMagErr, iMagType from Magnitude where idOrigin = :idOrigin");
    bIsQuery = true;                 
    init();                            
  }                                    
                                       
                                       
  public GetMagListStatement(Connection IN_conn)        
  {                                    
    this();                            
    SetConnection(IN_conn);            
  }                                    
                                       
                                       
  // *** CLASS METHODS *** \          
                                       
  protected boolean SetInputParams(Object obj)    
  {                                  
                                     
    EWMagnitude objEWMagnitude = (EWMagnitude)obj;              
                                     
    try                              
    {                                
      cs.setLong(1, objEWMagnitude.idOrigin);    
    }                                
    catch (SQLException ex)          
    {                                
      System.err.println("Exception in GetMagListStatement:SetInputParams()"); 
      System.err.println(ex);        
      ex.printStackTrace();          
      return(false);                 
    }                                
                                     
    return(true);                    
  }   // end GetMagListStatement::SetInputParams()    
                                     
                                     
  protected boolean RetrieveOutputParamsList(Vector OutputObjectList) 
  {                                                      
                                                         
     EWMagnitude objEWMagnitude;                                           
                                                         
    try                              
    {                                
    while(rs.next())                                     
    {                                                    
      objEWMagnitude = new EWMagnitude();                                  
                                                         
      objEWMagnitude.idMag=rs.getLong(1);                      
      objEWMagnitude.idOrigin=rs.getLong(2);                      
      objEWMagnitude.dMagAvg=rs.getFloat(3);                      
      objEWMagnitude.iNumMags=rs.getInt(4);                      
      objEWMagnitude.dMagErr=rs.getFloat(5);                      
      objEWMagnitude.iMagType=rs.getInt(6);                      
      OutputObjectList.addElement(objEWMagnitude);                
    }  // end while(rs.next)                             
    }                                
    catch (SQLException ex)          
    {                                
      System.err.println("Exception in GetMagListStatement:RetrieveOutputParamsList()"); 
      System.err.println(ex);        
      ex.printStackTrace();          
      return(false);                 
    }                                
                                                         
    return(true);                                        
  }   // end GetMagListStatement::RetrieveOutputParamsList()              
                                                         
                                                         
} // end class GetMagListStatement                      
                                       
                                       
//             <EOF>                   
