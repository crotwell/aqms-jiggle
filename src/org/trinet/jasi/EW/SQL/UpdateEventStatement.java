// *** PACKAGE NAME *** \
package org.trinet.jasi.EW.SQL;


// *** IMPORTS *** \
import java.util.*;
import java.sql.Connection;
import java.sql.SQLException;
import org.trinet.jasi.EW.EWSolution;


/**
 * <!-- Class Description>
 *
 *
 * Created:  2002/06/12
 *
 * @author   ew_oci2java Tool / DK
 * @version  0.99
 **/

public class UpdateEventStatement extends EWSQLStatement
{

  // *** CLASS ATTRIBUTES *** \

  // *** CONSTRUCTORS *** \
  public UpdateEventStatement()
  {
    sSQLStatement = new String("Begin Update_Event(OUT_RetCode => :OUT_RetCode,   IN_idEvent => :IN_idEvent, IN_iEventType => :IN_iEventType,   IN_iDubiocity => :IN_iDubiocity, IN_bArchived => :IN_bArchived,   IN_bSetEventType => :IN_bSetEventType, IN_bSetDubiocity => :IN_bSetDubiocity,   IN_bSetArchived => :IN_bSetArchived); End;");
    bIsQuery = false;
    init();
  }


  public UpdateEventStatement(Connection IN_conn)
  {
    this();
    SetConnection(IN_conn);
  }


  // *** CLASS METHODS *** \

  protected boolean SetInputParams(Object obj)
  {

    EWSolution objEWSolution = (EWSolution)obj;

    try
    {
      cs.setLong(2, objEWSolution.idEvent);
      cs.setInt(3, objEWSolution.tiEventType);
      cs.setInt(4, objEWSolution.iDubiocity);
      cs.setInt(5, objEWSolution.bArchived);
      cs.setInt(6, objEWSolution.bSetEventType);
      cs.setInt(7, objEWSolution.bSetDubiocity);
      cs.setInt(8, objEWSolution.bSetArchived);
    }
    catch (SQLException ex)
    {
      System.err.println("Exception in UpdateEventStatement:SetInputParams()");
      System.err.println(ex);
      ex.printStackTrace();
      return(false);
    }

    return(true);
  }   // end UpdateEventStatement::SetInputParams()


  protected boolean RegisterOutputParams()
  {
    try
    {
      cs.registerOutParameter(1, java.sql.Types.INTEGER);
    }
    catch (SQLException ex)
    {
      System.err.println("Exception in UpdateEventStatement:RegisterOutputParams()");
      System.err.println(ex);
      ex.printStackTrace();
      return(false);
    }
    return(true);
  }  // end UpdateEventStatement:RegisterOutputParams()


  protected boolean RetrieveOutputParams(Object obj)
  {
    EWSolution objEWSolution = (EWSolution)obj;

    try
    {
      objEWSolution.Retcode=cs.getInt(1);
    }
    catch (SQLException ex)
    {
      System.err.println("Exception in UpdateEventStatement:RetrieveOutputParams()");
      System.err.println(ex);
      ex.printStackTrace();
      return(false);
    }

    return(true);
  }   // end UpdateEventStatement::RetrieveOutputParams()


} // end class UpdateEventStatement


//             <EOF>
