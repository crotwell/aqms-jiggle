// *** PACKAGE NAME *** \                                     
package org.trinet.jasi.EW.SQL;                                
                                                               
                                                               
// *** IMPORTS *** \                                          
import java.util.*;                                            
import java.sql.Connection;                                    
import java.sql.SQLException;                                  
import org.trinet.jasi.EW.EWChannel;                                  
                                                               
                                                               
/**                                    
 * <!-- Class Description>             
 *                                     
 *                                     
 * Created:  2002/06/21                        
 *                                     
 * @author   ew_oci2java Tool / DK                        
 * @version  0.99                        
 **/                                   
                                       
public class GetStationListStatement extends EWSQLStatement 
{                                      
                                       
  // *** CLASS ATTRIBUTES *** \       
                                       
  // *** CONSTRUCTORS *** \           
  public GetStationListStatement()                          
  {                                    
    sSQLStatement = new String("select idChan, idComp, sSta, sComp, sNet, sLoc, dAzm, dDip,  dLat, dLon, dElev from ALL_STATION_INFO where tOn <= :ReqTime AND tOff >= :ReqTime ORDER BY idChan");
    bIsQuery = true;                 
    init();                            
  }                                    
                                       
                                       
  public GetStationListStatement(Connection IN_conn)        
  {                                    
    this();                            
    SetConnection(IN_conn);            
  }                                    
                                       
                                       
  // *** CLASS METHODS *** \          
                                       
  protected boolean SetInputParams(Object obj)    
  {                                  
                                     
    EWChannel objEWChannel = (EWChannel)obj;              
                                     
    try                              
    {                                
      cs.setDouble(1, objEWChannel.tReqTime);  // DK was ReqTime  
    }                                
    catch (SQLException ex)          
    {                                
      System.err.println("Exception in GetStationListStatement:SetInputParams()"); 
      System.err.println(ex);        
      ex.printStackTrace();          
      return(false);                 
    }                                
                                     
    return(true);                    
  }   // end GetStationListStatement::SetInputParams()    
                                     
                                     
  protected boolean RetrieveOutputParamsList(Vector OutputObjectList) 
  {                                                      
                                                         
     EWChannel objEWChannel;                                           
                                                         
    try                              
    {                                
    while(rs.next())                                     
    {                                                    
      objEWChannel = new EWChannel();                                  
                                                         
      objEWChannel.idChan=rs.getLong(1);                      
      objEWChannel.idComp=rs.getLong(2);                      
      objEWChannel.sSta=rs.getString(3);                      
      objEWChannel.sComp=rs.getString(4);                      
      objEWChannel.sNet=rs.getString(5);                      
      objEWChannel.sLoc=rs.getString(6);                      
      objEWChannel.dAzm=rs.getFloat(7);                      
      objEWChannel.dDip=rs.getFloat(8);                      
      objEWChannel.dLat=rs.getDouble(9);                      
      objEWChannel.dLon=rs.getDouble(10);                      
      objEWChannel.dElev=rs.getDouble(11);                      
      OutputObjectList.addElement(objEWChannel);                
    }  // end while(rs.next)                             
    }                                
    catch (SQLException ex)          
    {                                
      System.err.println("Exception in GetStationListStatement:RetrieveOutputParamsList()"); 
      System.err.println(ex);        
      ex.printStackTrace();          
      return(false);                 
    }                                
                                                         
    return(true);                                        
  }   // end GetStationListStatement::RetrieveOutputParamsList()              
                                                         
                                                         
} // end class GetStationListStatement                      
                                       
                                       
//             <EOF>                   
