package org.trinet.jasi;
import java.util.*;
import org.trinet.util.velocitymodel.UniformFlatLayerVelocityModel;
import org.trinet.util.velocitymodel.VelocityModelIF;

/**
* ActiveList of UniformFlatLayerVelocityModel objects
*/

public class VelocityModelList extends ActiveList {

  public VelocityModelList() { }

  /** Return a model from the list with this class name.
   * Returns null if string is null or no such class is in the list.*/
  public UniformFlatLayerVelocityModel getByName(String modelName) {
    if (modelName == null) return null;
    UniformFlatLayerVelocityModel model = null;
    for (Iterator it=this.iterator(); it.hasNext(); ) {
      model = (UniformFlatLayerVelocityModel) it.next();
      if (model.getName().equals(modelName)) return model;
    }
    return null;
  }

  /** Return a UniformFlatLayerVelocityModel model from the list with this class name.
   * Returns null if string is null or no such class is in the list.*/
  public UniformFlatLayerVelocityModel getByClassname(String classname) {
    if (classname == null) return null;
    UniformFlatLayerVelocityModel model = null;
    for (Iterator it=this.iterator(); it.hasNext(); ) {
      model = (UniformFlatLayerVelocityModel) it.next();
      if (model.getClass().getName().equals(classname)) return model;
    }
    return null;
  }

  public boolean setSelected(String modelName) {
      return setSelected(getByName(modelName));
  }

  /** Adds a UniformFlatLayerVelocityModel model if not in the list. */
  public boolean addObject(Object obj) {
    if ( obj == null || ! (obj instanceof UniformFlatLayerVelocityModel)) return false;
    return super.add(obj);
  }

  /** Add or replace by name a UniformFlatLayerVelocityModel model in the list. */
  public Object addOrReplace(Object obj) {

    if ( obj == null || ! (obj instanceof UniformFlatLayerVelocityModel)) return null;
    UniformFlatLayerVelocityModel model = (UniformFlatLayerVelocityModel) obj;
    String name = model.getName();

    for (int idx = 0; idx < size(); idx++) {
        model = (UniformFlatLayerVelocityModel) get(idx);
        if (model.getName().equals(name)) {
            model = (UniformFlatLayerVelocityModel) super.set(idx, obj);
            if (model != null && model == selected) setSelected(obj);  // added selection synch -aww 2008/03/24
            notifyObs(obj);
            return model;
        }
    }

    super.add(obj);
    notifyObs(obj);
    return null;

  }

  /** Instantiate a UniformFlatLayerVelocityModel matching input classname and add it to the list, if not present.*/
  public boolean addByClassname(String modelClassName) {
      if (modelClassName == null) return false;  // bad input
      return add(makeNewModelFromClassName(modelClassName)); 
  }

  /** Instantiate a UniformFlatLayerVelocityModel matching input classname replace like model in the list, otherwise add it.*/
  public Object addOrReplaceByClassname(String modelClassName) {
      if (modelClassName == null ) return null;  // bad input
      return addOrReplace(makeNewModelFromClassName(modelClassName)); 
  }

  private UniformFlatLayerVelocityModel makeNewModelFromClassName(String modelClassName) {
      UniformFlatLayerVelocityModel model = null; 
      try { // model with a zero-arg default constructor
          model = (UniformFlatLayerVelocityModel) Class.forName(modelClassName).newInstance();
      }
      catch (ClassNotFoundException ex) {
          ex.printStackTrace();
      }
      catch (InstantiationException ex) {
          ex.printStackTrace();
      }
      catch (IllegalAccessException ex) {
          ex.printStackTrace();
      }
      return model;
  }

  public String [] getModelNames() {
      String [] names = new String [size()]; 
      for (int idx = 0; idx < size(); idx++) {
          names[idx] = ((UniformFlatLayerVelocityModel) get(idx)).getName();
      }
      return names;
  }

  public final String toString() {
      StringBuffer sb = new StringBuffer(1024);
      for (int idx = 0; idx < size(); idx++ ) {
          sb.append(get(idx).toString()).append("\n");
      }
      return sb.toString();
  }

  public final String [] toStringArray() {
      String [] str = new String[size()];
      for (int idx = 0; idx < size(); idx++ ) {
          str[idx] = (String) get(idx).toString();
      }
      return str;
  }

  public final VelocityModelIF getSelectedModel() {
      return (VelocityModelIF) super.getSelected();
  }

}
