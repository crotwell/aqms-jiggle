package org.trinet.jasi;
import org.trinet.util.Format;


/**
 * The gain of a single channel. Gain has both a value and units.
 * The gain of a time-series (actually of the
 * seismometer that produced it) is the relationship between the values of the
 * time-series (e.g. counts) and actual ground motion (e.g. cm/sec). <p>
 * For example, CI_CBK_EHZ is a short-period velocity transducer with a gain of
 * 8966788 and units of 'Units.CntCMS' ("counts/cm/sec"). So one cm/sec equals
 * 8966788 digital counts or one count = 1/8966788 cm/sec. Thus, this is a
 * very high-gain station (i.e. it magnifies the ground motion alot).<p>
 * CI_CTC_HLZ is an accelerometer. It has a gain of 5356 and units of
 * "counts/(cm/sec2)". It's a strong motion instrument (low-gain).<p>
 * Note that the units of the gain can be used to determine the sensor type: velocity
 * transducer vs. accelerometer.
 * */
public class ChannelGain extends UnitsDataDouble  { // implements TimeBoundedObjectIF

     { units = Units.CntCMS; } // initialize counts/(cm/sec)

     // protected TimeSpan timespan = new TimeSpan(); // to implement TimeBoundedObjectIF

     public ChannelGain() { }

     public ChannelGain(ChannelGain cg) {
         this.setValue(cg);
     }

     public ChannelGain(double value, int units) {
         super(value, units);
     }

     public Object clone() {
         ChannelGain cg = (ChannelGain) super.clone();
         // cg.timespan = (TimeSpan) this.timespan.clone();
         return cg;
     }

     /* Belcw methods of interface TimeBoundedObjectIF
     //Returns 'true' if input true UTC time seconds is in the time span for which this object is valid.
     public boolean isValidAt(double trueSecs) {
       return timespan.contains(trueSecs);
     }
     // Return the timespan for for which this object is valid.
     public TimeSpan getTimeSpan() {
       return timespan;
     }
     // Set the timespan for for which this object is valid.
     public void setTimespan(TimeSpan timespan) {
       timespan.set(timespan);
     }
     */

    public boolean isVelocity() {
        return Units.isVelocity(getUnits());
    }
    public boolean isAcceleration() {
        return Units.isAcceleration(getUnits());
    }

    /** Return the Units type that will result when you convert a waveform
     * from counts to physical units by dividing by this gain.
     * Note this may be nonsense for some gain types, if so this returns Units.UNKNOWN.
     */
     static public int getResultingUnits(ChannelGain gain) {
         int units = gain.getUnits();
         // set input gain units to known standard physical units
         if (units == Units.CntCMS) {   // || what about Units.CntCMSx ? aww
           return Units.CMS;
         } else if (units == Units.CntCMSS) {   // || what about Units.CntCMSSx ? aww
           return Units.CMSS;
         } else if (units == Units.DuMS) {
           return Units.MS;   // BK - DigitalUnits/Meter/Sec
         } else if (units == Units.DuMSS) {
           return Units.MSS;  // BK - DigitalUnits/Meter/Sec^2
         } else if (units == Units.CntMS) {
           return Units.MS;   // SIS
         } else if (units == Units.CntMSS) {
           return Units.MSS;  // SIS
         }
         return Units.UNKNOWN;
     }

     public String toString() {
       StringBuffer sb = new StringBuffer(32);
       String str = (isNull()) ? "NULL" : String.valueOf(Math.round(doubleValue()));
       sb.append(str).append(" ").append(getUnitsString());
       return sb.toString();
     }



}
