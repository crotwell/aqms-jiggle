package org.trinet.jasi;
public class AssociatedSolutionMismatchException extends RuntimeException {
    public AssociatedSolutionMismatchException() {
        super();
    }
    public AssociatedSolutionMismatchException(String message) {
        super(message);
    }
}
