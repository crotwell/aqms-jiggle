package org.trinet.jasi;
public interface MagnitudeCalibrationIF extends ChannelCorrectionDataIF {
    public boolean hasMagCorr(MagnitudeAssocJasiReadingIF jr) ;
    public Double getMagCorr(MagnitudeAssocJasiReadingIF jr) ;
    public Double getSummaryWt(MagnitudeAssocJasiReadingIF jr) ;
    public Double getClipAmp(MagnitudeAssocJasiReadingIF jr) ;
}
