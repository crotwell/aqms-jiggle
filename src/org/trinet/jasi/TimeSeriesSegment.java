package org.trinet.jasi;

import org.trinet.util.*;

public abstract class TimeSeriesSegment extends JasiTimeBoundedData {

    /** Multiply this value (default=1.5) by the sample interval to define
     * the time-tear interval. If two samples or segments are separated by
     * more than the time-tear interval it indicates that data is missing or
     * that there has been a timebase shift.*/
    static float timeTearTolerance = 1.5f;

    /** Channel description */
    Channel chan = Channel.create();

    /** Data sample interval (sec/sample) */
    double dt;

    /** Size of a single sample value in bytes. */
    int bytesPerSample;

    /** True if timeseries has been filtered. */
    boolean isFiltered = false;

    /** Array holding the time series values.
     * Float rather than double to save memory and no known seismic time-series
     * exceeds 32-bits. */
    float ts[];

    /** Set the channel */
    public void setChannelObj(Channel channel) {
        //logic below mirrors that of other classes with Channel members
        //this.chan = channel; // don't alias here , unless its necessary - aww
        if (channel == null) this.chan = null; // Do you want to allow null?
        else if (this.chan == null) this.chan = (Channel) channel.clone();
        else this.chan.copy((Channelable)channel);
    }
    /** Get the channel */
    public Channel getChannelObj()  {
        return chan;
    }

/** Return the time-tear tolerance factor. It is usually a value like 1.5, meaning
* that the gap between two segments can be upto 1.5 times the sample interval
* without being declared a time-tear.
*/
   public static float getTimeTearTolerance() {
          return timeTearTolerance;
   }
/** Return the time-tear tolerance factor. It is usually a value like 1.5, meaning
* that the gap between two segments can be upto 1.5 times the sample interval
* without being declared a time-tear.
*/
   public static void setTimeTearTolerance(float val) {
          timeTearTolerance = val;
   }

   /** Returns seconds between samples. */
   public double getSampleInterval() {
          return dt;
   }
   /** Set seconds between samples. */
   public void setSampleInterval(double interval) {
          dt = interval;
   }
    /**
     * Returns 'true' if two Segments have no time gap between them.
     */
    /* The tricky part here is knowing how much time there should be between
     * the last sample of the preceding seg and the first sample of the next.
     * It should be exactly one sample interval but due to storage and rounding jitter
     * you can't test with "=" :. We test that the interval between the segs
     * is within getTimeTearTolerance() times the sample interval.
     */
    public static boolean areContiguous(TimeSeriesSegment seg1, TimeSeriesSegment seg2) {
        // Note that this will return true if there is a data overlap too.
        return ((seg2.getEpochStart() - seg1.getEpochEnd()) < (seg1.getSampleInterval() * getTimeTearTolerance())) ? true : false;
    }


    /** Set the time series array. Scan it to set bias, max, min, etc.*/
    public void setTimeSeries(float timeseries[]) {
       setTimeSeries(timeseries, true);
    }

    /** Set the time series array. If 'scanIt' is true the new time series
    * will be scanned to set bias, max, min, etc. You should only set 'scanIt' false
    * if you are resetting the timeseries repeatedly and don't care about
    * teh results of the scan. An example is when WFSegments are concatinated.*/
    public void setTimeSeries(float timeseries[], boolean scanIt) {
       ts = timeseries;
       if (scanIt) scanTimeSeries();
    }

    /** Called when a new time series is set. Can set max, min, bias, etc.*/
    abstract void scanTimeSeries();

    /** Return the time series array. */
    public float[] getTimeSeries() {
       return ts;
    }

    /** Return the size of the time series array. */
//    public int getTimeSeriesLength() {
//       return ts.length;
//    }

    public int size() {
       if (ts != null) {
          return ts.length;
       } else {
          return 0;
       }
    }

    /** Return true if there is time series. */
    public boolean hasTimeSeries() {
           return (size() > 0);
    }

    /** Return a copy of the time series array. */
    public float[] getTimeSeriesCopy() {
      float newArray[] = new float[ts.length] ;
      System.arraycopy(ts, 0, newArray, 0, ts.length);
      return newArray;
    }

 /**
 * Allocate an array of this size to hold the time series. This will
 * destroy any previous time series.
 */
  public void allocateTimeSeriesArray(int nsamp) {
        ts = new float[nsamp];
  }
/**
 * Delete the time series array
 */
  public void deallocateTimeSeriesArray() {
        ts = null;
  }

  /** Multiply this value to all samples in the time series. Used to scale. */
  public void scaleAmp(float val) {
    for (int i = 0; i<ts.length; i++) {
       ts[i] *= val;
    }
    scanTimeSeries();
  }

  /** Add this value to all samples in the time series. Used to demean, etc. */
  public void addAmpOffset(float val) {
    for (int i = 0; i<ts.length; i++) {
       ts[i] += val;
    }
    scanTimeSeries();
  }

  /** Filter this time series in place.  */
  public void filter(FilterIF filter) {
      if (!hasTimeSeries()) return;

      filter.filter(ts);
  }

  public void setIsFiltered(boolean tf) {
    isFiltered = tf;
  }

  public boolean isFiltered() {
    return isFiltered;
  }
}
