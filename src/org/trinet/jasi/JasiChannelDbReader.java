package org.trinet.jasi;
import java.util.*;
import java.sql.*;
import org.trinet.jdbc.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.jdbc.table.*;
import org.trinet.util.EpochTime;
import org.trinet.util.Utils;
import org.trinet.util.DateRange;

public class JasiChannelDbReader extends JasiDbReader {

    public static final int SEEDCHAN   = DataTableRowUtil.SEEDCHAN;
    public static final int LOCATION   = DataTableRowUtil.LOCATION;
    public static final int CHANNELSRC = DataTableRowUtil.CHANNELSRC;
    public static final int AUTHSUBSRC = DataTableRowUtil.AUTHSUBSRC;

    // table debug option:
    public static int defaultMatchMode = LOCATION; // hopefully no overlapping data!
    //data integrity problem when channels with overlapping date range for same location:
    //public static defaultMatchMode = CHANNELSRC; // use channel channelsrc temporary as tie-breaker

    public JasiChannelDbReader() { }

    public JasiChannelDbReader(DbReadableJasiObjectIF jo) {
        super(jo);
    }

    public JasiChannelDbReader(String className) {
        super(className);
    }

    /**
     * Returns the instance with the most recent ondate that is valid for the specified input.
     * Returns null if no data are found satisfying input criteria.
    */
    // Should return single row
    public static Object getByChannelId(Connection conn, String tableName, ChannelIdIF id, java.util.Date date, DbReadableJasiObjectIF jo) {
        if (id == null || NullValueDb.isBlank(id.getSta()))
           throw new IllegalArgumentException("ChannelId sta name must be defined.");

        StringBuffer sb = new StringBuffer(128);
        sb.append(((DbReadableJasiChannelObjectIF) jo).toChannelSQLSelectPrefix());
        whereOrAndAppend(sb, true);
        sb.append(DataTableRowUtil.toPreparedChannelIdDateClause(tableName, id, date));
        sb.append(" ORDER BY ").append((tableName != null) ? tableName+"." : "").append("ondate DESC"); // most recent first

        PreparedStatement ps = null;
        ResultSet rs = null;
        Collection aList = null;
        int offset = 1;
        String sql = null;
        try {
            if ( conn == null || conn.isClosed() ) {
              System.err.println ("JasiChannelDbReader getByPrepared connection c is closed");
              return null;
            }
            sql = sb.toString();
            ps = conn.prepareStatement(sql);
            ps.setString(offset++, id.getSta());
            ps.setString(offset++, id.getSeedchan());
            ps.setString(offset++, id.getNet());
            ps.setString(offset++, id.getLocation());

            //if (date != null) ps.setDate(offset++, new java.sql.Date(date.getTime()));
            if (date != null) ps.setString(offset++, EpochTime.dateToString(date,"yyyy-MM-dd HH:mm:ss"));
            ResultSetDb rsdb = new ResultSetDb(ps.executeQuery());
            rs = rsdb.getResultSet();
            if (rs != null) {
              aList = new ArrayList();
              while ( rs.next() ) {
                aList.add(jo.parseData(rsdb));
              }
            }
        }
        catch (SQLException ex) {
            System.err.println(ex);
            ex.printStackTrace();
        }
        finally {
          JasiDatabasePropertyList.debugSQL(sql, ps, debug);
          Utils.closeQuietly(rs, ps);
        }

        if (aList == null || aList.size() == 0) {
            if (debug || JasiDatabasePropertyList.debugSQL) System.out.println("JasiChannelDbReader  getByChannelId QUERY RETURNED EMPTY RESULTSET");
            return null;
        }
        Object [] obj = aList.toArray();
        if (debug && obj.length > 1) { // dump when debugging, multiple rows indicate duplicates 
          System.err.println("WARNING! JasiChannelDbReader.getByChannelId by date > 1 count: " +
              obj.length);
         // + " table: " + tableName + " channelId: " + id.toString() + " date: " + date);
          System.err.println(sb.toString()); // debug
          // test trace : throw new NullPointerException();
        }

        return obj[0];
    }
    //

    /*
    public static Object getByChannelId(Connection conn, String tableName, ChannelIdIF id, java.util.Date date, DbReadableJasiObjectIF jo) {
        if (id == null || NullValueDb.isBlank(id.getSta()))
           throw new IllegalArgumentException("ChannelId sta name must be defined.");
        StringBuffer sb = new StringBuffer(128);
        sb.append(((DbReadableJasiChannelObjectIF) jo).toChannelSQLSelectPrefix(date));
        whereOrAndAppend(sb, true);
        // removed line below until location code overlap for dates resolved - aww 06/04
        //sb.append(DataTableRowUtil.toChannelLocationSQLWhereClause(tableName, id));
        // code below compares "channel" elements: 
        //sb.append(DataTableRowUtil.toChannelSrcSQLWhereClause(tableName, id));
        // use option below to toggle matching level for table data debugging:
        sb.append(DataTableRowUtil.toChannelIdSQLString(tableName, id, defaultMatchMode));
        if (tableName != null)
          sb.append(" ORDER BY ").append(tableName).append(".ondate DESC"); // most recent first
        Collection list = getBySQL(conn, sb.toString(), jo);
        if (list == null || list.size() == 0) return null;
        Object [] obj = list.toArray();
        if (debug && obj.length > 1) { // dump when debugging
          System.err.println("WARNING! JasiChannelDbReader.getByChannelId by date > 1 count: " +
              obj.length);
         // + " table: " + tableName + " channelId: " + id.toString() + " date: " + date);
          System.err.println(sb.toString()); // debug
          // test trace : throw new NullPointerException();
        }
        return obj[0];
    }
    */

    public static Object getByChannelId(String tableName, ChannelIdIF id, java.util.Date date, DbReadableJasiObjectIF jo) {
        return getByChannelId(DataSource.getConnection(), tableName, id, date, jo);
    }
    public Object getByChannelId(String tableName, ChannelIdIF id, java.util.Date date) {
        return getByChannelId(DataSource.getConnection(), tableName, id, date, jo);
    }

    /**
     * Returns Collection whose data match the input ChannelId and whose valid
     * date range overlaps or is completely within the input date range.
     * Returns null if no data are found satisfying input criteria.
    */
    public static Collection getByChannelId(String tableName, ChannelIdIF id, DateRange dr, DbReadableJasiObjectIF jo) {
        if (id == null || NullValueDb.isBlank(id.getSta()))
           throw new IllegalArgumentException("ChannelId sta name must be defined.");
        StringBuffer sb = new StringBuffer(128);
        sb.append(((DbReadableJasiChannelObjectIF) jo).toChannelSQLSelectPrefix());
        whereOrAndAppend(sb, true);
        // Table row ondates less than input offdate
        sb.append(DataTableRowUtil.toOnDateConstraintSQLWhereClause(tableName, dr.getMaxDate()));
        whereOrAndAppend(sb, true);
        // Table row offdates greater than input ondate
        sb.append(DataTableRowUtil.toOffDateConstraintSQLWhereClause(tableName, dr.getMinDate()));
        whereOrAndAppend(sb, true);
        sb.append(DataTableRowUtil.toChannelIdSQLString(tableName, id, defaultMatchMode));
        if (tableName != null)
          sb.append(" ORDER BY ").append(tableName).append(".ondate DESC"); // most recent first
        return getBySQL(DataSource.getConnection(), sb.toString(), jo);
    }
    public Collection getByChannelId(String tableName, ChannelIdIF id, DateRange dr) {
        return getByChannelId(tableName, id, dr, jo);
    }

    public Collection loadAll(Connection conn) {
        return getBySQL(conn, ((DbReadableJasiChannelObjectIF) jo).toChannelSQLSelectPrefix(), jo);
    }

    public Collection loadAll(Connection conn, java.util.Date date) {
        return loadAll(conn, date, jo);
    }

    public Collection loadAll(String tableName, Connection conn, java.util.Date date,
                    String [] prefNet, String [] prefSeedchan,
                    String [] prefChannel, String [] prefLocations) {
        return loadAll(tableName, conn, date, prefNet, prefSeedchan, prefChannel, prefLocations, jo);
    }

    public static Collection loadAll(Connection conn, java.util.Date date, DbReadableJasiObjectIF jo) {
        return loadAll(null, conn, date, null, null, null, null, jo);
    }

    // NOTE input tableName cannot be null if any of the input pref... arguments are not null !
    public static Collection loadAll(String tableName, Connection conn, java.util.Date date,
                    String [] prefNet, String [] prefSeedchan,
                    String [] prefChannel, String [] prefLocations,
                    DbReadableJasiObjectIF jo) {

      StringBuffer sb = new StringBuffer(256);

      sb.append(((DbReadableJasiChannelObjectIF) jo).toChannelSQLSelectPrefix(date));


      /* could replace code implementation below instead with:
       whereOrAndAppend(sb, true);
       sb.append(toMatchingNetListSQL(tableName, LIKE, prefNet));
       sb.append(" AND ");
       sb.append(toMatchingSeedchanListSQL(tableName, LIKE, prefSeedchan));
       sb.append(" AND ");
       sb.append(toMatchingChannelListSQL(tableName, LIKE, prefChannel));
       sb.append(" AND ");
       sb.append(toMatchingLocationListSQL(tableName, LIKE, prefLocations));
       return getBySQL(conn, sb.toString(), jo);
       */
      boolean checkWhere = true;
      if (prefNet != null && prefNet.length > 0) {
        checkWhere = whereOrAndAppend(sb, checkWhere);
        sb.append("(");
        for (int idx = 0; idx < prefNet.length; idx++) {
          if (idx > 0) sb.append(" OR ");
          sb.append(tableName).append(".net LIKE ").append(StringSQL.valueOf(prefNet[idx]));
        }
        sb.append(")");
      }
      if (prefSeedchan != null && prefSeedchan.length > 0) {
        checkWhere = whereOrAndAppend(sb, checkWhere);
        sb.append("(");
        for (int idx = 0; idx < prefSeedchan.length; idx++) {
          if (idx > 0) sb.append( " OR ");
          sb.append(tableName).append(".seedchan LIKE ").append(StringSQL.valueOf(prefSeedchan[idx]));
        }
        sb.append(")");
      }
      // channel may be needed for odd historical data maps?
      if (prefChannel != null && prefChannel.length > 0) {
        checkWhere = whereOrAndAppend(sb, checkWhere);
        sb.append("(");
        for (int idx = 0; idx < prefChannel.length; idx++) {
          if (idx > 0) sb.append( " OR ");
          sb.append(tableName).append(".channel LIKE ").append(StringSQL.valueOf(prefChannel[idx]));
        }
        sb.append(")");
      }
      // location attribute now required for typical channel lookup
      if (prefLocations != null && prefLocations.length > 0) {
        checkWhere = whereOrAndAppend(sb, checkWhere);
        sb.append("(");
        for (int idx = 0; idx < prefLocations.length; idx++) {
          if (idx > 0) sb.append( " OR ");
          sb.append(tableName).append(".location LIKE ").append(StringSQL.valueOf(prefLocations[idx]));
        }
        sb.append(")");
      }
      return getBySQL(conn, sb.toString(), jo);
    }

    private static String toOrderChannelString(String tableName) {
      StringBuffer sb = new StringBuffer(132);
      sb.append(" ORDER BY ");
      sb.append(tableName).append("net, ");
      sb.append(tableName).append(".sta, ");
      sb.append(tableName).append(".seedchan, ");
      sb.append(tableName).append(".location");
      return sb.toString();
    }

    public static ChannelIdIF parseChannelIdKeyByName(ChannelIdIF id, ResultSet rs) {
      // by default matches only seedchan location, ignoring channel,channelsrc attributes
      // since they are not part of db key and may be inconsistent in db across data tables
      return parseChannelIdKeyByName(id, rs, LOCATION);
    }
    public static ChannelIdIF parseChannelIdKeyByName(ChannelIdIF id, ResultSet rs, int parseType) {
      if (parseType < 0 || parseType > AUTHSUBSRC) throw new IllegalArgumentException("Unknown parse type.");
      if (id  == null) id = new ChannelName();
      try {
        String tmp = rs.getString("NET");
        id.setNet((tmp == null) ? "" : tmp);
        tmp = rs.getString("STA");
        id.setSta((tmp == null) ? "" : tmp);
        tmp = rs.getString("SEEDCHAN");
        id.setSeedchan((tmp == null) ? "" : tmp);

        if (parseType > SEEDCHAN) {
          tmp = rs.getString("LOCATION");
          id.setLocation((tmp == null) ? "" : tmp);
        }

        if (parseType > LOCATION) {
          tmp = rs.getString("CHANNEL");
          id.setChannel((tmp == null) ? "" : tmp);
          tmp = rs.getString("CHANNELSRC");
          id.setChannelsrc((tmp == null) ? "" : tmp);
        }
        if ( (parseType > CHANNELSRC) && (id instanceof AuthChannelIdIF) ) {
          AuthChannelIdIF aid = (AuthChannelIdIF) id;
          tmp = rs.getString("AUTH");
          aid.setAuth((tmp == null) ? "" : tmp);
          tmp = rs.getString("SUBSOURCE");
          aid.setSubsource((tmp == null) ? "" : tmp);
        }
      }
      catch (SQLException ex) {
        ex.printStackTrace();
      }
      return id;
    }
    public static ChannelIdIF parseChannelIdKeyByOffset(ChannelIdIF id, int offset, ResultSet rs) {
      if (id == null) id = new ChannelName();
      try {
        String tmp = rs.getString(++offset);
        id.setNet((tmp == null) ? "" : tmp);
        tmp = rs.getString(++offset);
        id.setSta((tmp == null) ? "" : tmp);
        tmp = rs.getString(++offset);
        id.setSeedchan((tmp == null) ? "" : tmp);
        tmp = rs.getString(++offset);
        id.setLocation((tmp == null) ? "" : tmp);
      }
      catch (SQLException ex) {
        ex.printStackTrace();
      }
      return id;
    }

    public static StringBuffer toMatchingNetListSQL(String tableName, int likeFlag, String [] netList) {
      return toMatchingColumnSQL(tableName, "net", likeFlag, netList);
    }
    public static StringBuffer toMatchingStaListSQL(String tableName, int likeFlag, String [] staList) {
      return toMatchingColumnSQL(tableName, "sta", likeFlag, staList);
    }
    public static StringBuffer toMatchingSeedchanListSQL(String tableName, int likeFlag, String [] seedchanList) {
      return toMatchingColumnSQL(tableName, "seedchan", likeFlag, seedchanList);
    }
    public static StringBuffer toMatchingLocationListSQL(String tableName, int likeFlag, String [] locationList) {
      return toMatchingColumnSQL(tableName, "location", likeFlag, locationList);
    }
    public static StringBuffer toMatchingChannelListSQL(String tableName, int likeFlag, String [] channelList) {
      return toMatchingColumnSQL(tableName, "channel", likeFlag, channelList);
    }
    //
    // could add "auth, subsource, or channelsrc" static equivalents too.
    //
} // JasiChannelDbReader
