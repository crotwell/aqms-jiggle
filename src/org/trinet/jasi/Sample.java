package org.trinet.jasi;

import org.trinet.util.LeapSeconds;
import org.trinet.util.Format;

/**
 *  A single sample. Has a time (epoch time) and a value (amplitude). The
 * units of the ampltude are unspecified and could be counts, cm/sec, etc.
 */

public class Sample implements SampleIF {  // should be serializable -aww

    public double datetime = 0.0;
    public double value    = 0.0;

    protected boolean isNull = true;

    protected static final Format ff = new Format("%6.3f");
    protected static final Format fd = new Format("%13d");
    protected static final Format fe = new Format("%13.7e");

    public Sample() { }

    public Sample(double time, double val) {
        set(time, val);
    }

    public Sample(double time, int val) {
        this(time, (double) val);
    }

    public Sample(Sample samp) {
        set(samp.datetime, samp.value);
    }

    public void set(double time, double val) {
        setDatetime(time);
        setValue(val);
    }

    public double getDatetime() {
        return datetime;
    }
    public void setDatetime(double time ) {
        datetime = time;
        setNotNull();
    }

    public double getValue() {
        return value;
    }
    public void setValue(double val ) {
      value = val;
      setNotNull();
    }

    /** Returns true if no value or time has been set for this Sample. */
    public boolean isNull() {
        return isNull;
    }

    private void setNotNull() {
        isNull = false;
    }

    public String toUnitsString(String units) {
        //return String.format("%5.2f %s at %s", value, units, LeapSeconds.trueToString(datetime));
        Format f = (units.equalsIgnoreCase("counts")) ? fd : fe;
        return LeapSeconds.trueToString(datetime) + " " + units + " " + f.form(value);
    }

    public String toString() {
        //return String.format("%5.2f at %s", value, LeapSeconds.trueToString(datetime));
        return LeapSeconds.trueToString(datetime) + " " + fe.form(value);
    }

    /** Return the time difference between samples. */
    public double timeDiff(Sample samp) {
        return datetime - samp.datetime;
    }
/*
    public boolean equalsValue(Sample s) {
        return (value == s.value);
    }

    public boolean equals(Sample s) {
        return (value == s.value && datetime == s.datetime && isNull == s.isNull);
    }

    public boolean equals(Object obj) {
        if (! (obj instanceof Sample)) return false;
        return equals((Sample) obj);
    }
*/
    /** Return a copy of oneself. */
    public Sample copy() {
      return (Sample) clone();
    }

    /** Return the latest time Sample with the maximum value. Returns null if BOTH Samples
    * are null.  If one Sample is null returns the non-null Sample. */
    public static Sample max(Sample s1, Sample s2) {
      if (s1 == null) return s2;
      if (s2 == null) return s1;

      if (s1.value > s2.value) return s1;
      return s2;
    }
    /** Return the latest time Sample with the minimum value. Returns null if BOTH Samples
    * are null. If one Sample is null returns the non-null Sample.*/
    public static Sample min(Sample s1, Sample s2) {
      if (s1 == null) return s2;
      if (s2 == null) return s1;

      if (s1.value < s2.value) return s1;
      return s2;
    }
    /** Return the latest time Sample with the maximum absolute value. Returns null if BOTH Samples
    * are null.  If one Sample is null returns the non-null Sample. */
    public static Sample absMax(Sample s1, Sample s2) {
      if (s1 == null) return s2;
      if (s2 == null) return s1;

      if (Math.abs(s1.value) > Math.abs(s2.value)) return s1;
      return s2;
    }

    public Object clone() {
        Sample samp = null;
        try {
          samp = (Sample) super.clone();
        }
        catch (CloneNotSupportedException ex) {
          ex.printStackTrace();
        }
        return samp;
    }

} // end of class
