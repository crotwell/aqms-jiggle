package org.trinet.jasi;
import java.util.List;
public interface JasiCommitableAssociatorIF {
  public boolean associate(JasiCommitableIF jc);
  public boolean unassociate(JasiCommitableIF jc);
  public boolean erase(JasiCommitableIF jc);
  public boolean add(JasiCommitableIF jc);
  public boolean addOrReplace(JasiCommitableIF jc);
  public boolean remove(JasiCommitableIF jc);
  public boolean delete(JasiCommitableIF jc);
  public boolean undelete(JasiCommitableIF jc);
  public boolean contains(JasiCommitableIF jc);
  public JasiCommitableListIF getListFor(JasiCommitableIF jc);
  public JasiCommitableListIF getListFor(JasiCommitableListIF jcList);
  public int addAssociatedData(JasiCommitableListIF jcList);
  public void clearDataLists(boolean verbose);

  public void associate(List aList);

}
