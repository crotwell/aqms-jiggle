package org.trinet.jasi;

import java.util.Collection;
import org.trinet.util.*;

/** Lock a Solution so multiple data applications that access the data will not
 * conflict. The concrete class must determine how this is implements. However,
 * it must be done in such a way that all sessions sharing a datasource will
 * also share locks. This can be done by using the source database or with files
 * on the data host.  <p>
 *
 * If locking is not supported the create() method returns null, and all methods
 * return 'true'.
 */
public abstract class SolutionLock
    extends ResourceLock implements Lockable {

    /** The ID number of the solution to lock. We assume it is unique. */
    protected long id;

    /** Flag that is set true if locking works. This is a static so that you
don't have to do an expensive dbase operation every time you ask the question
"Is locking supported?". You determine that once, and set this flag.  If locking
does not work then always return true from all boolean methods and empty
collections from all others. */
    static protected boolean lockingWorks = false;

    /** Constructor is protected so it can't be called. Use 'create()' to instantiate. */
    protected SolutionLock() {}

// ////////////////////////////////////////////////////////////////////////////
    /**
     * Instantiate an object of this type. You do
     * NOT use a constructor. This "factory" method creates various
     * concrete implementations. Creates a Solution of the DEFAULT type.
     * @See: JasiObject
     */
     public static final SolutionLock create() {
        return create(DEFAULT);
     }

    /**
     * Instantiate an object of this type. You do
     * NOT use a constructor. This "factory" method creates various
     * concrete implementations. The argument is an integer implementation type.
     * @See: JasiObject
     */
     public static final SolutionLock create(int schemaType) {
        return create(JasiFactory.suffix[schemaType]);
     }

    /**
     * Instantiate an object of this type. You do
     * NOT use a constructor. This "factory" method creates various
     * concrete implementations. The argument is as 2-char implementation suffix.
     * @See: JasiObject
     */
     public static final SolutionLock create(String suffix) {
        return (SolutionLock) JasiObject.newInstance("org.trinet.jasi.SolutionLock", suffix);
     }
// ////////////////////////////////////////////////////////////////////////////
    /** Override this if locking is supported or if support is dependent on
    some underlying condition (like presence of a dbase, file, or other resource).*/
    public boolean isSupported() {
        return lockingWorks;
    }
    /** Checks the DataSource to see if locking is supported. Sets flag 'lockingWorks'
        to proper value. */
    abstract public boolean checkLockingWorks();

    public void setSolution(Solution sol) {
        setId(sol.id.longValue());
    }

    /** Set whether or not locking works. */
    public static void setLockingWorks(boolean tf) {
        lockingWorks = tf;
    }

    public long getId() {
        return id;
    }

    /** Lock this solution, returns true on success, false on failure.*/
    public void setId(long id) {
        this.id = id;
    }

    /** Lock this solution, returns true on success, false on failure.
    * If locked, information on the current lock holder is in the data members. */
    public abstract boolean lock() ;

    /** Release the lock on this solution. Returns true even if the lock was not held
    * in the first place. */
    public abstract boolean unlock() ;

    /** Returns true if this solution is locked by anyone, the caller included. */
    public abstract boolean isLocked() ;

    /** Returns true if this solution is locked by the caller. */
    public abstract boolean lockIsMine() ;

    /** Return an array of SolutionLock objects that represent ALL locks currently
    * held by this user. */
    public abstract Collection getLocksByUser(String usename);

    /** Return an array of SolutionLock objects that represent ALL locks currently
    * held by this host. */
    public abstract Collection getLocksByHost(String host) ;

    /** Return an array of SolutionLock objects that represent ALL locks currently
    * held by this application. */
    public abstract Collection getLocksByApplication(String application) ;

    /** Return an array of SolutionLock objects that represent ALL locks currently
    * held locks on all events. */
    public abstract Collection getAllLocks() ;

    /** Return an array of SolutionLock objects that represent ALL locks currently
    * held by this host/user/application. */
    public abstract Collection getAllMyLocks() ;

    /** Unlock ALL currently held locks on all events. */
    public abstract boolean unlockAll() ;

    /** Release ALL locks currently held by this host/user/application. */
    public abstract boolean unlockAllMyLocks();

    /**  */
    public String toString() {
        return "ID: "+id+" Host: "+host+" Username: "+username+
        " Application: "+application+
        " Time: "+LeapSeconds.trueToString(datetime);
    }

    public String requestorToString() {
        return "ID: "+id+" Host: "+requestorHost+
            " Username: "+requestorUsername+
        " Application: "+requestorApplication+
            " Time: "+LeapSeconds.trueToString(requestorDateTime);
    }

    /** Produce an output header string to match output from toFormattedString()
     * Has the following form:<p>
    <tt>
Event-ID    Host                          Username  Application    Date/Time
</tt>
    */
    public static String getHeaderString() {
        StringBuffer sb = new StringBuffer(132);
        sb = appendWithPad(sb, "Event-ID", 12);
        sb = appendWithPad(sb, "Host", 30);
        sb = appendWithPad(sb, "Username", 10);
        sb = appendWithPad(sb, "Application", 15);
        sb = appendWithPad(sb, "Date/Time", 19);
        return sb.toString().trim();
    }
    /** Produce output with the following form:<p>
    <tt>
1234001     serverbo.gps.caltech.edu         doug      LockEvent      2000-08-16 17:56:09
1234003     serverbt.gps.caltech.edu          stan      Jiggle         2000-08-16 21:47:53

</tt>
Use getHeaderString() to put a properly aligned header at the top of this output.
    */
    public String toFormattedString() {
        StringBuffer sb = new StringBuffer(132);
        sb = appendWithPad(sb, ""+id, 12);
        sb = appendWithPad(sb, host, 30);
        sb = appendWithPad(sb, username, 10);
        sb = appendWithPad(sb, application, 15);
        sb = appendWithPad(sb, LeapSeconds.trueToString(datetime), 19);  //"2000-08-16 17:56:09"
        return sb.toString().trim();
    }
    
        /** Pad right side of string with spaces to maintain fixed spacing. Insures that
        substring call doesn't fail because original string is too short.  */
    private static StringBuffer appendWithPad(StringBuffer sb, String str, int size) {
        String padder = //add right padding
            str + "                                                                 ";
        try {
            sb = sb.append(padder.substring(0, size));
        }
        catch (StringIndexOutOfBoundsException e) {}
        return sb;
    }
    
}
