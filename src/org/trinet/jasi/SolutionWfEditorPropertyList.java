package org.trinet.jasi;
import java.util.*;
import org.trinet.jasi.seed.SeedReader;
import org.trinet.jiggle.WaveServerGroup;
import org.trinet.util.WaveClient;

public class SolutionWfEditorPropertyList extends SolutionEditorPropertyList {

    /** List of WaveServer groups. */
    private ActiveList wsgList = new ActiveList();

    /** List of ChannelTimeWindowModels. This is a list of String objects. Each
     *  String is a Class name for a ChannelTimeWindowModel that can be instantiated
     *  with ChannelTimeWindowModel.getCurrentChannelTimeWindowModelInstance(list).
     *  Strings have the form: "org.trinet.jasi.SpecialChannelTimeWindowModel".
     *  The class must extend ChannelTimeWindowModel and be in the Classpath to
     *  be instantiated. The list is stored in the property file as a comma delimited
     *  string in the "channelTimeWindowModelList" property.
     *  The currently selected model is stored in the property
     *  "currentChannelTimeWindowModel".
     *   */
    //ActiveList ctwModelList = new ActiveList();
    private ChannelTimeWindowModelList ctwModelList = new ChannelTimeWindowModelList();
    private ChannelTimeWindowModel currentCTW = null;

    private boolean waveServerInit = false;

    public static boolean waveServerDebug = false;
    public static boolean seedReaderVerbose = false;

/**
 *  Constructor: reads no property file empty list.
 *  @see #setRequiredProperties()
 */
    public SolutionWfEditorPropertyList() { }

/**
 *  Constructor: Makes a COPY of a property list. Doesn't reread the properties files,
 * assumes that was already done at instantiation of passed input object.
 * @see: JasiPropertyList(String fileName)
 */
    public SolutionWfEditorPropertyList(SolutionWfEditorPropertyList props) {
        super(props);
        privateSetup();
    }

/**
 *  Constructor: input file is a name relative to the default user and system paths
 *  which are defined by System properties appending a default subdirectory of type
 *  defined by the lowercase form of the application name or "jasi".
 */
    public SolutionWfEditorPropertyList(String fileName)
    {
        super(fileName);  // does a reset()
        privateSetup();
    }

/**
*  Constructor: input filenames are not appended to constructed paths, but rather are
*  interpreted as specified (either relative to current working directory or a complete path). 
*/
    public SolutionWfEditorPropertyList(String userPropFileName, String defaultPropFileName)
    {
        super(userPropFileName, defaultPropFileName);
        privateSetup();
    }

    public Object clone() {
        SolutionWfEditorPropertyList wfpList = (SolutionWfEditorPropertyList) super.clone();
        wfpList.wsgList = (ActiveList) this.wsgList.clone();
        wfpList.ctwModelList  = (ChannelTimeWindowModelList) this.ctwModelList.clone(); // 1st clone the list
        //wfp.currentCTW = (ChannelTimeWindowModel) this.currentCTW.clone(); // no, set it from list elements -aww 
        wfpList.currentCTW = wfpList.ctwModelList.getByClassname(wfpList.getCurrentChannelTimeWindowModelClassName());
        return wfpList;
    }

/** Returns the waveform source. This can be passed to AbstractWaveform.setWaveDataSource() to
 *  affect the change in wavesource. */

    public Object getWaveSource() {
        if (getInt("waveformReadMode") == AbstractWaveform.LoadFromDataSource) {
          //return DataSource.getConnection();
          return DataSource.getSource();
        } else if (getInt("waveformReadMode") == AbstractWaveform.LoadFromWaveServer) {
          return getWaveServerGroup();
        } else {
          return null;
        }
    }


/** Sets the waveform source property and the static Waveform WaveSource.
 *  The 'flag' value is an integer as defined in Waveform.
 *  @see: AbstractWaveform.LoadFromDataSource
 *  @see: AbstractWaveform.LoadFromWaveServer */
    public void setWaveSource(int source, Object waveDataSource) {
         setProperty("waveformReadMode", source);
         AbstractWaveform.setWaveDataSource(waveDataSource);

    }
// WaveServerGroup
    public void setWaveServerGroupList(Collection list) {
        wsgList =  (ActiveList) list;
        waveServerInit = true;
        synchProperties();
    }

    public Collection getWaveServerGroupList() {
        return wsgList;
    }
    public void setWaveServerGroup(WaveServerGroup wsg) {
        WaveServerGroup.setSelected(wsg, wsgList);
        synchProperties();
    }

    /** Returns the currently selected WaveServerGroup. */
    public WaveServerGroup getWaveServerGroup() {
        return WaveServerGroup.getSelected(wsgList);
    }

    /** Return a Collection of ChannelTimeWindowModels.  */
    public ChannelTimeWindowModelList getChannelTimeWindowModelList() {
        return  ctwModelList;
    }

    /** Return current selected ChannelTimeWindowModel.  */
    public ChannelTimeWindowModel getCurrentChannelTimeWindowModel() {
        return currentCTW;
    }

    /** Return instance of currently selected ChannelTimeWindowModel.  */
    public ChannelTimeWindowModel getCurrentChannelTimeWindowModelInstance() {
        ChannelTimeWindowModel ctw =  ChannelTimeWindowModel.getInstance(getCurrentChannelTimeWindowModelClassName());
        // model class specific prefix properties could be defined, so added setProperties - aww 06/16/2005
        if (ctw != null) ctw.setProperties(this);
        return ctw;
    }

    /** Return classname string of currently selected ChannelTimeWindowModel.  */
    public String getCurrentChannelTimeWindowModelClassName() {
        return getProperty("currentChannelTimeWindowModel", "");
    }

    /** Set this model as the selected one.
     *  An instance of the model must be in the ChannelTimeWindowModelList */
    public boolean setChannelTimeWindowModel(String modelClass) {

      ChannelTimeWindowModel model = ctwModelList.getByClassname(modelClass);
      if (model == null)  return false;  // not in the list or null string

      model.setProperties(this); // reset any model properties to current settings in this object

      setProperty("currentChannelTimeWindowModel", model.getClassname());
      currentCTW = model;

      return true ;

    }

    /** Returns true if any of the wavesource properties were changed. */
    public boolean waveSourceEquals(SolutionWfEditorPropertyList otherProps) {
      if (
        !getProperty("waveServerConnectTimeout", "").equals(otherProps.getProperty("waveServerConnectTimeout", "")) ||
        !getProperty("waveformReadMode", "").equals(otherProps.getProperty("waveformReadMode", "")) ||
        !getProperty("waveServerGroupList", "").equals(otherProps.getProperty("waveServerGroupList", "")) ||
        !getProperty("waveServerGroupDefaultClient", "").equals(otherProps.getProperty("waveServerGroupDefaultClient","")) ||
        !getProperty("currentWaveServerGroup", "").equals(otherProps.getProperty("currentWaveServerGroup", ""))
        ) return false;

      return true;
    }

    /** Returns true if the ChannelTimeWindowModel changed. */
    public boolean channelTimeWindowModelEquals(SolutionWfEditorPropertyList otherProps) {
        if (! getCurrentChannelTimeWindowModelClassName().equals(otherProps.getCurrentChannelTimeWindowModelClassName())) return false;
        if (currentCTW == null && otherProps.currentCTW == null) return true;
        if (currentCTW == null || otherProps.currentCTW == null) return false;
        return currentCTW.getParameterDescriptionString().equals(otherProps.currentCTW.getParameterDescriptionString());
    }

    /**
     * Setup class static, instance, and/or environment variables from current properties.
     * This method should be called after the pertinent properties are initially read
     * or changed through updates.
     */
    public boolean setup() {
      boolean status = super.setup();
      return (privateSetup() && status);
    }

    public void setupWaveServerGroup() {
      if (isSpecified("waveServerGroupList")) {

        WaveServerGroup.doPopupOnAddServerError = getBoolean("waveServerPopupOnAddServerError", false);

        // Make WaveServerGroup connectTimeout static, parse below creates the WaveServerGroup instance and connectTimeout not in the string
        // TODO: implement WaveServerGroup.parseFromProperties(props) where properties are more like:
        //waveClientServerGroupNames=rts
        //wcsg.rts.retries=1
        //wcsg.rts.maxTimeoutMillis=20000
        //wcsg.rts.connectTimeoutMs=5000
        //wcsg.rts.verifyWaveforms=false
        //wcsg.rts.trucateAtTimeGap=false
        //wcsg.rts.hostPortList= wave1.gps.caltech.edu\:6509  wave2.gps.caltech.edu\:6509

        WaveServerGroup.setConnectTimeout(getInt("waveServerConnectTimeout", WaveClient.DEFAULT_CONNECT_TIMEOUT));

        String str = getProperty("waveServerGroupList");
        // NOTE: below parse setups the group as well as the WaveClient and does server connection
        wsgList = (ActiveList) WaveServerGroup.parsePropertyString(str);

        // set current group, if not set or not in list default to 1st in list
        WaveServerGroup wsg = null;
        if (isSpecified("currentWaveServerGroup")) {
          str = getProperty("currentWaveServerGroup");
          wsg = WaveServerGroup.getByName(wsgList, str);
          if (wsg == null && wsgList != null && !wsgList.isEmpty()) {   // not defined, default
            System.err.println("Warning: 'currentWaveServerGroup' is not in 'waveServerGroupList'");
            wsg = (WaveServerGroup) wsgList.get(0);
          }
          if (wsg != null && wsgList != null) WaveServerGroup.setSelected(wsg, wsgList);
        }
        if (wsgList != null ) {
            wsg = WaveServerGroup.getSelected(wsgList);
            WaveClient wc = null;
            if (wsg != null) wc = wsg.getWaveClient();
            if (wc != null)  wc.setDebug(getBoolean("waveServerDebug"));
        }
      }
      waveServerInit = true;
    }

    private boolean privateSetup() {

      boolean status = true;
      if ( isSpecified("waveServerDebug") ) 
          waveServerDebug = getBoolean("waveServerDebug");

      if ( isSpecified("seedReaderDebug") ) 
          SeedReader.debug = getBoolean("seedReaderDebug");

      if ( isSpecified("seedReaderVerbose") ) 
          seedReaderVerbose = getBoolean("seedReaderVerbose");

      // primary database WAVEROOTS table wcopy column value to be used in query for waveform file path lookup
      if ( isSpecified("waverootsCopy") ) 
          Wavelet.wcopy = getInt("waverootsCopy");
      else
          Wavelet.wcopy = 1;

      if ( isSpecified("scanNoiseType") ) 
          WFSegment.scanNoiseType = getInt("scanNoiseType"); // used to be test case set in Jiggle only -aww

      // Multiples of the S-P time to add to the P-time for end time of the energy scan window
      if ( isSpecified("wfSmPWindowMultiplier") ) {
          double d = getDouble("wfSmPWindowMultiplier");
          // Force to be at least 1.5
          if (d >= 1.5) AbstractWaveform.smpWindowMultiplier = d;
      }

      // get the waveServer group list even if we aren't using it 
      String str = null;
      if (isSpecified("waveServerGroupDefaultClient")) {
          str = getProperty("waveServerGroupDefaultClient");
          WaveServerGroup.setDefaultWaveClient(str);
      }

      //setupWaveServerGroup();

      // get the ChannelTimeWindowModel list.
      if (isSpecified("channelTimeWindowModelList")) {
        str = getProperty("channelTimeWindowModelList");
        ActiveList ctwModelNameList = (ActiveList) ChannelTimeWindowModel.parsePropertyString(str);
        for (Iterator it=ctwModelNameList.iterator(); it.hasNext(); ) {
          String name = (String) it.next();
          ctwModelList.add(name, this);    // create and setup models
        }
        if (isSpecified("currentChannelTimeWindowModel") ) { // added set current model 06/16/2005 -aww
          if (!setChannelTimeWindowModel(getProperty("currentChannelTimeWindowModel")))
            System.err.println("Warning: 'currentChannelTimeWindowModel' is not in 'channelTimeWindowModelList'");
        }
      }

      if (isSpecified("currentChannelTimeWindowModel") ) {
        status = checkForRuntimeClassOf("currentChannelTimeWindowModel");
      }

      setupWaveDataSource(); // shouldn't we enable this by default ? - aww 09/08/2006

      //if (debug) ChannelTimeWindowModel.debug = true;

      return status;
    }

    public void setupWaveDataSource() {
      // set the waveform source by property
      if (isSpecified("waveformReadMode")) {
        int waveSrc = getInt("waveformReadMode");
        if ( waveSrc == AbstractWaveform.LoadFromDataSource) {
          //setWaveSource(waveSrc, new DataSource(getDbaseDescription()));
          setWaveSource( waveSrc, DataSource.getSource() );
        } else if (waveSrc == AbstractWaveform.LoadFromWaveServer) {
          setupWaveServerGroup();
          setWaveSource( waveSrc, WaveServerGroup.getSelected(wsgList) );
        }
      }
    }

/**
 * Save the properties with a custom header.
 */
    public boolean saveProperties(String header) {
       // make sure all values are up to date
       synchProperties();
       // save user properties
       remove("auxPropFile"); // since all properties input are now written to parent
       return super.saveProperties(header);
    }

    /** Set property values for things that are stored internally as objects other than
     *  raw properties. For example, the "currentWaveServerGroup" is stored as a
     *  Collection. If that collection changes, the property, which is a string, must
     *  also change. */
    public void synchProperties() {
      // the currentChannelTimeWindowModel should always be correct
      String str = ChannelTimeWindowModel.toModelListPropertyString(ctwModelList);
      if (str != null) setProperty("channelTimeWindowModelList", str );

      // added test here to force setup for case where DataSource mode bypassed the input waveserver init
      if (!waveServerInit && (wsgList == null || wsgList.isEmpty())) setupWaveServerGroup();

      str = WaveServerGroup.toGroupListPropertyString(wsgList);
      if (str != null) setProperty("waveServerGroupList", str);

      if ( WaveServerGroup.getSelected(wsgList) != null ) {
        str = WaveServerGroup.getSelected(wsgList).getName();
        if (str != null) setProperty ("currentWaveServerGroup",  str);
      }

    }


    /** Return a List of property names used
     * by application classes utilizing this class.
     * */
    public List getKnownPropertyNames() {
        List aList = super.getKnownPropertyNames();
        List myList = classDeclaredPropertyNames();
        if (aList instanceof ArrayList)
            ((ArrayList)aList).ensureCapacity(aList.size() + myList.size());
        aList.addAll(myList);
        Collections.sort(aList);
        return aList;
    }

    public static List classDeclaredPropertyNames() {

        ArrayList myList = new ArrayList(10);

        myList.add("channelTimeWindowModelList");
        myList.add("currentChannelTimeWindowModel");
        myList.add("currentWaveServerGroup");
        myList.add("scanNoiseType");
        myList.add("seedReaderVerbose");
        myList.add("seedReaderDebug");
        myList.add("waveformReadMode");
        myList.add("waveServerGroupList");
        myList.add("waveServerGroupDefaultClient");
        myList.add("waverootsCopy");
        myList.add("waveServerDebug");

        Collections.sort(myList);

        return myList;
    }

} // end of class

