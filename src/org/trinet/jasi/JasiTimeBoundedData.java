package org.trinet.jasi;
import java.util.*;

import org.trinet.jdbc.datatypes.*;

import org.trinet.util.*;
/**
 * An object that is time bounded, that is, has a start and stop time.
 * Also allows specification of a value from 0.0 -> 1.0 describing the
 * quality of the time.
*/
public abstract class JasiTimeBoundedData extends JasiObject implements TimeBoundedDataIF {

    public static boolean debug = false;

    /** Start time of the window */
    protected DataDouble  timeStart = new DataDouble();  // NaN
    /** End time of the window */
    protected DataDouble  timeEnd   = new DataDouble();  // NaN

    /** A value from 0.0 -> 1.0 describing the quality of the time. 1.0 is best.*/
    protected DataDouble timeQuality = new DataDouble();  // NaN

    public DataDouble getStart() { return timeStart; }
    public double getEpochStart() { return timeStart.doubleValue(); }

    public void setStart(DataDouble start) {
           timeStart.setValue(start);
    }
    public void setStart (double start) {
           timeStart.setValue(start);
    }

    public DataDouble getEnd() { return timeEnd; }
    public double getEpochEnd() { return timeEnd.doubleValue(); }
    public void setEnd(DataDouble end) {
           timeEnd.setValue(end);
    }
    public void setEnd (double end) {
           timeEnd.setValue(end);
    }

    /** Return the time window's length in seconds. Beware of precision jitter.*/
    public double getDuration() {
           return getEpochEnd() - getEpochStart();
    }
    /** Return the time window's length as a TimeSpan object. */
    public TimeSpan getTimeSpan() {
           return new TimeSpan(getEpochStart(), getEpochEnd());
    }
    /** Set the time window's TimeSpan object. */
    public void setTimeSpan(TimeSpan ts) {
           setStart(ts.getStart());
           setEnd(ts.getEnd());
    }
    /** Set the time quality value, a value from 0.0 -> 1.0 describing the
     *  quality of the time. 1.0 is best.*/
    public void setTimeQuality (double qual) {
      timeQuality.setValue(qual);
    }
    /** Set the time quality value, a value from 0.0 -> 1.0 describing the
     *  quality of the time. 1.0 is best.*/
    public void setTimeQuality (DataDouble qual) {
      timeQuality = qual;
    }
    /** Get the time quality value, a value from 0.0 -> 1.0 describing the
     *  quality of the time. 1.0 is best. Returns null if unknown. */
    public DataDouble getTimeQuality () {
      return timeQuality;
    }
}
