/**
* <h1> Overview </h1>
* <p>
* Java Abstract Seismic Interface (jasi) is a set of classes that represent 
* generic seismological objects such as Solution, Phase, Coda, etc.
* </p>
*
*  <h1>Creating Concrete JASI Version Implementations</h1>
* 
*  
* <p> 
* Jiggle (and any applications built from its component classes) interacts with 
* the underlying database via a set of Java classes called "jasi" (Java Abstract Seismic Interface). 
* It is a set of classes that represent generic seismological objects; e.g. Solution, Phase, Coda, etc.
* </p>
* <p> 
* Any of the classes that touch the dbase or do anything site-specific are declared as 
* "abstract". An abstract class or method defines what the inputs, outputs and behavior 
* are but doesn't actually do anything. To manipulate the Earthworm dbase you will 
* need to write a "concrete" method for each abstract method in the jasi library. 
* Our concrete classes use JDBC (Java DataBase Connectivity) which is a standard Java/SQL interface; 
* there is nothing Oracle specific in any of the code. You could elect to use another Java-to-dbase 
* interface like SQLJ or a commercial product. You could even instantiate a jasi implementation 
* to read/write from flat files.
* </p>
* 
* <p> 
* Most classes in the distribution are NOT abstract and do not need schema-specific 
* implementations. Even in the abstract classes only some of the methods are abstract. 
* You can, however, override any of the existing concrete methods to customize the 
* class behavior for your site as long as you don't change the fundamental behavior 
* as described in the documentation.
* </p>
* 
*  
* 
*<b>The classes that will need concrete implementations are:</b>
* 
* <ul> 
* 
* <li>Amplitude.java</li>
* 
* <li>Comment.java</li>
* 
* <li>EventTypeMap.java</li>
* 
* <li>Magnitude.java</li>
* 
* <li>MagnitudeMethod.java</li>
* 
* <li>Phase.java</li>
* 
* <li>QualityControlModel.java</li>
* 
* <li>Solution.java</li>
* 
* <li>SolutionLock.java</li>
* 
* <li>Waveform.java</li>
*
* </ul> 
*  
* 
* <p>
* Most of the classes in the table involve reading/writing to the site-specific 
* schema. Some, like SolutionLock.java, would need no work at all if you adopt 
* the JasiEventLock table schema that is used by the TriNet implementation. 
* Others are wrappers around site-specific procedures. For example, the EventTypeMap.java 
* class allows you to customize the strings you use to describe different types 
* of events; "blast", "quarry", "qb" and map from those strings in your data set 
* to the ones used by jasi. MagnitudeMethod.java is a base class that is extended 
* to support calculation of various magnitude types like ML or MCA. By extending 
* the MagnitudeMethod class you can adapt any magnitude calculation technique to 
* work with the jasi MagnitudeEngine class.
* </p> 
*  
* 
* <p>
* Here's a simple "concrete" example:
* </p>
* 
*  
* <p> 
* In Solution.java you want to read in all dbase events (Solutions) that are in a given time window.
* </p>
* 
*  
* <pre> 
* THE ABSTRACT METHOD
* 
* 
*      * Returns array of Solutions within this time window. Times are
* 
*      * seconds in UNIX epoch time. Returns null if no event is found.
* 
*      * No other criteria or flags are checked.
* 
*      
* 
*     abstract public Solution[] getByTime(double start, double stop);
* </pre>
*    
* <pre> 
* THE CONCRETE METHOD (in class SolutionTN.java)
* 
*  
* 
*     
* 
*      * Returns array of Solutions within this time window.
* 
*      * Returns null if no event is found.
* 
*      
* 
*     public Solution[] getByTime(double start, double stop) {
* 
*       // must do a join of Event/Origin/NetMag to get all the
* 
*       // info we need   
* 
*       String sql = sqlJoin + getTimeSpanSQL(start, stop) +
* 
*           " order by Origin.Datetime";
* 
*  
* 
*       return getBySQL(sql);
* 
*     }
* </pre>
*  
* <p> 
* This is a little bit deceptive because the hard work is done in the 
* method helper method called getBySQL() which gets the data from the dbase and 
* parses it into the Solution object. This helper method is NOT part of the jasi. 
* You are free to create private members, methods or classes within your concrete 
* implementation.
* </p>
*  
* <p> 
* The TriNet jdbc library contains a set of classes that corresponds to the NCDC 
* schema tables. The TriNet jdbc library is a lower level interface to the NCDC 
* schema. This made it simpler to interact with that schema while developing the
* TriNet concrete classes. A similar approach would probably make interfacing to 
* other schemas simpler.
* </p>
* 
*/
package org.trinet.jasi;
