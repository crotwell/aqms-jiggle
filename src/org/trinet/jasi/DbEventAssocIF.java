package org.trinet.jasi;
// could implement in  Solution, Magnitude, JasiReading?
public interface DbEventAssocIF {
    public long getEvidValue();
    public long getCommidValue();
}
