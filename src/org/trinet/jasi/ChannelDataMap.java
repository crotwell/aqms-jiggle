package org.trinet.jasi;
import java.io.*;
import java.util.*;
import org.trinet.util.*;
import org.trinet.jdbc.table.*;

public class ChannelDataMap extends Object implements ChannelDataMapListIF, Cloneable, Serializable {
/**
* Store in a hashmap keyed by the ChannelIdIF a set of valid dates ranges for the ChannelIdIF
* If an input Date lies within a DateRange (ondate offdate) of the set, cached data object is returned.
* Otherwise, object if a new ChannelData object can be created from the database it is added to the cache
* and returned. Returns null if the data is not recoverable.
* The cached ChannelData object's DateRange and ChannelId are used to compose the key that is mapped to the
* ChannelData object in the hashmap.
*/
    //protected static final int DEFAULT_CAPACITY = 1019;
    protected static final int DEFAULT_CAPACITY = 89;
    protected static final float DEFAULT_MAP_LOAD_FACTOR = .75f;

    protected HashMap chanParmMap;
    protected HashMap chanDateMap;
    protected ChannelDataIF defaultData;

    public ChannelDataMap() {
      this((ChannelDataIF) null);
    }
    public ChannelDataMap(ChannelDataIF defaultDataFactory) {
      this(DEFAULT_CAPACITY, DEFAULT_MAP_LOAD_FACTOR, defaultDataFactory);
    }
    public ChannelDataMap(int capacity, ChannelDataIF defaultDataFactory) {
      this(capacity, DEFAULT_MAP_LOAD_FACTOR, defaultDataFactory);
    }
    public ChannelDataMap(int capacity, float loadFactor, ChannelDataIF defaultDataFactory) {
      chanParmMap = new HashMap(capacity, loadFactor);
      chanDateMap = new HashMap(capacity, loadFactor);
      this.defaultData = defaultDataFactory;
    }
    public ChannelDataMap(Collection data) {
      this(data, DEFAULT_MAP_LOAD_FACTOR);
    }
    public ChannelDataMap(Collection data, float loadFactor) {
      int count = data.size();
      // allow for 25% more data b4 rehash required? 
      int capacity = (int) Math.round(count/loadFactor + 0.25*count);
      if (capacity%2 == 0) capacity += 1; // make size an odd number, it's more likely to be a prime
      if (capacity < 5) capacity = 5;
      chanParmMap = new HashMap(capacity, loadFactor);
      chanDateMap = new HashMap(capacity, loadFactor);
      ChannelDataIF [] input = (ChannelDataIF []) data.toArray(new ChannelDataIF [count]);
      // Do we assume all in collection are of same factory class type
      // Since first element may be "large" due to associated data creation
      // it may be better to make user HAVE to invoke setDefaultDataFactory
      // using preferably to new object instance of element subtype as input.
      //this.defaultData = (ChannelDataIF) input[0].clone();
      defaultData = (ChannelDataIF) JasiObject.newInstance(input[0].getClass().getName());
      addAll(input);
    }
    public ChannelDataMap(ChannelDataMap data) {
      defaultData = (ChannelDataIF) data.defaultData.clone();
      putAll(data);
    }

    public void setDefaultDataFactory(ChannelDataIF defaultDataFactory) {
      this.defaultData = defaultDataFactory;
    }

    private ChannelDataIF setDefaultData(ChannelIdIF id, DateRange dr) {
      defaultData.setChannelId(id);
      defaultData.setDateRange(dr);
      return defaultData;
    }
    private void nullDefaultData() {
      defaultData.setChannelId(null);
      defaultData.setDateRange(null);
    }

/**
* Writes the list data to the default data archive.
*/
    public boolean commit() {
        if (chanParmMap == null) return false;
        Iterator iter = chanParmMap.values().iterator();
        boolean status = true;
        Object data = null;
        while (iter.hasNext()) {
            data = iter.next();
            if (data instanceof Commitable) {
              status = ((Commitable) data).commit();
            }
            if (! status) break;
        }
        return status;
    }

/**
*  Initializes the list with new elements of default type using data from data source.  
*  valid for current date.
*/
    public boolean load() {
        return load((java.util.Date) null);
    }

/**
*  Initializes the list with new elements of default type using data from data source
*  for specified date.
*/
    public boolean load(java.util.Date date) {
        Collection newCollection = defaultData.loadAll(date);  // retrieve all data from data source
        if (newCollection == null) return false;
        int count = newCollection.size();
        if (count < 1) return false;
        chanParmMap = new HashMap(count);
        chanDateMap = new HashMap(count);
        addAll(newCollection);
        return true;
    }

/**
*  Adds to list a new elements of default type using data from data source
*  for specified channel and date.
*  Returns data instance added to list, null if no data added. 
*/
    public ChannelDataIF load(ChannelIdIF id, java.util.Date date) {
        if (id == null) return null;
        ChannelDataIF data = defaultData.load(id, date);
        if (data != null) add(data, false);
        return data;
    }

    public void printValues() {
        System.out.println(getValuesString());
    }

    public String getValuesString() {
        if (chanParmMap == null || chanParmMap.isEmpty()) {
            return "null or empty";
        }
        Collection list = chanParmMap.values();
        Iterator iter = list.iterator();
        StringBuffer sb = new StringBuffer(list.size() * 132);
        while (iter.hasNext()) {
            sb.append(iter.next().toString());
            sb.append("\n");
        }
        return sb.toString();
    }

    public boolean isEmpty() {
        if (chanParmMap.isEmpty()) {
           clear();
           return true;
        }
        else return false;
    }

    public int size() {
      return chanParmMap.size();
    }
    public Collection values() {
      return chanParmMap.values();
    }
    public void clear() {
      chanParmMap.clear();
      chanDateMap.clear();
    }

    public boolean contains(Channelable chan, java.util.Date date) {
      return (chan == null) ? false : contains(chan.getChannelObj().getChannelId(), date);
    }
    public boolean contains(ChannelIdIF id, java.util.Date date) {
        if (id == null || date == null) return false;
        Set aDateRangeSet = (Set) chanDateMap.get( makeDateMapHashKey(id) );
        if (aDateRangeSet == null) return false;
        Iterator iter = aDateRangeSet.iterator();
        boolean retVal = false;
        while (iter.hasNext()) {
          DateRange dr = ((DateRange) iter.next());
          if (dr.contains(date))  {
            retVal = contains( setDefaultData(id, dr) );
            nullDefaultData(); // cleanup reset 
            break;
          }
        }
        return retVal;
    }

    public boolean contains(ChannelDataIF cd) {
      return (cd == null) ? false : chanParmMap.containsKey( makeParmMapHashKey(cd) ); 
    }

    public Object remove(ChannelDataIF cd) {
      if (cd == null) return null;
      DateRange dateRange = cd.getDateRange();
      Set aDateRangeSet = (Set) chanDateMap.get( makeDateMapHashKey(cd) );
      if (aDateRangeSet != null) {
        Iterator iter = aDateRangeSet.iterator();
        while (iter.hasNext()) {
          if ( ((DateRange) iter.next()).equals(dateRange) ) {
            iter.remove();
            break;
          }
        }
      }
      return chanParmMap.remove( makeParmMapHashKey(cd) );
    }

    public boolean addAll(Collection data) {
      return (data == null) ? false : addAll((ChannelDataIF [])data.toArray(new ChannelDataIF[data.size()]));
    }

    public boolean addAll(ChannelDataIF [] data) {
        if (data == null) return false;
        int count = data.length;
        if ( count == 0) return false; 
        // If default not set, for convenience set to first element, all in list must be same type
        if (defaultData == null) defaultData = data[0];
        //check first element against default type, assumes remainder of array elements are of same class.
        else if ( ! defaultData.getClass().isInstance(data[0]) ) {
          System.err.println("ChannelDataMap.add input mismatch with default factory data type!");
          System.err.println(" default: "+defaultData.getClass().getName()+" input: "+data[0].getClass().getName());
          throw new IllegalArgumentException("ERROR - input not instance type of default factory.");
        }
        boolean retVal = false;
        ChannelDataIF newValue = null;
        for (int idx = 0; idx < data.length; idx++) {
          newValue = data[idx];
          if (add(newValue, false) != newValue) retVal = true; // true only if not already in map
        }
        return retVal;
    }

    public Object add(ChannelDataIF cd) {
      return add(cd, true);
    }
    protected Object add(ChannelDataIF cd, boolean checkType) {
      if (cd == null) return null;
      if ( checkType && ! defaultData.getClass().isInstance(cd) ) {
        System.err.println("ChannelDataMap.add input mismatch with default factory data type!");
        System.err.println(" default: "+defaultData.getClass().getName()+" input: "+cd.getClass().getName());
        throw new IllegalArgumentException("ERROR - input not instance type of default factory.");
      }
      DateRange dr = cd.getDateRange();
      Object dateKey = makeDateMapHashKey(cd);
      Set aDateRangeSet = (Set) chanDateMap.get(dateKey);
      if (aDateRangeSet != null) {
        aDateRangeSet.add(dr);
      }
      else {
        aDateRangeSet = new HashSet(7);
        aDateRangeSet.add(dr);
        chanDateMap.put(dateKey, aDateRangeSet);
      }
      return chanParmMap.put( makeParmMapHashKey(cd), cd );
    }

    public void putAll(ChannelDataMap cdl) {
       if (cdl == null) return;
       if (chanDateMap == null) chanDateMap = (HashMap) cdl.chanDateMap.clone();
       else chanParmMap.putAll(cdl.chanDateMap);
       if (chanParmMap == null) chanParmMap = (HashMap) cdl.chanParmMap.clone();
       else chanParmMap.putAll(cdl.chanParmMap);
    }

    //find or load the current active data 
    public ChannelDataIF lookUp(ChannelDataIF cd) {
       return lookUp(cd, null);
    }
    public ChannelDataIF lookUp(ChannelDataIF cd, java.util.Date date) {
       return (cd == null) ? null : lookUp(cd.getChannelId(), date);
    }
    public ChannelDataIF lookUp(Channelable chan, java.util.Date date) {
       return (chan == null) ? null : lookUp(chan.getChannelObj().getChannelId(), date);
    }
    public ChannelDataIF lookUp(ChannelIdIF id, java.util.Date date) {
        if (id == null) return null;
        ChannelDataIF data = get(id, date);
        return (data != null) ?  data : load(id, date) ;
    }

    public ChannelDataIF get(Channelable chan, java.util.Date date) {
        return (chan == null) ? null : get(chan.getChannelObj().getChannelId(), date);
    }
    public ChannelDataIF get(ChannelDataIF cd, java.util.Date date) {
        // Note input may be a different ChannelData "type" than those stored in list 
        // but have like ChannelId
        return (cd == null) ? null : get(cd.getChannelId(), date);
    }
    public ChannelDataIF get(ChannelIdIF id, java.util.Date date) {
        if (id == null) return null;
        Set aDateRangeSet = (Set) chanDateMap.get( makeDateMapHashKey(setDefaultData(id, null)) );
        if(aDateRangeSet == null) return null;
        Object retVal = null;
        Iterator iter = aDateRangeSet.iterator();
        while (iter.hasNext()) {
          DateRange dateRange = ((DateRange) iter.next());
          if (dateRange.contains(date)) {
            // Update defaultData dateRange to go with input channelId for key object
            // use defaultFactory type for lookup:
            retVal =
              chanParmMap.get( makeParmMapHashKey(setDefaultData(id, dateRange)) );
            break;
          }
        }
        nullDefaultData(); // cleanup reset 
        //System.out.println("DEBUG chanParmMap returned: " +((Channel)retVal).toDumpString());
        return (ChannelDataIF) retVal; // or return input cd if null //??
    }

    // Return data instance from map exactly matching the input 
    public ChannelDataIF get(ChannelDataIF cd) {
      return (ChannelDataIF) chanParmMap.get(makeParmMapHashKey(cd));
    }

    public boolean equals(Object object) {
        if (object == this) return true;
        if (object == null || object.getClass() != this.getClass()) return false;
        ChannelDataMap list = (ChannelDataMap) object;
        return ( chanDateMap.equals(list.chanDateMap) && chanParmMap.equals(list.chanParmMap) );
    }

    public Object clone() {
        ChannelDataMap cdl = null;
        try {
            cdl = (ChannelDataMap) super.clone();
            cdl.chanDateMap = (HashMap) this.chanDateMap.clone();
            cdl.chanParmMap = (HashMap) this.chanParmMap.clone();
        }
        catch(CloneNotSupportedException ex) {
             ex.printStackTrace();
        }
        return cdl;
    }

    public int hashCode() {
        return (chanDateMap.hashCode()+chanParmMap.hashCode()) ;
    }

    public String toString() {
      return chanParmMap.toString();
    }

    public String toValuesString() {
        Collection vals = chanParmMap.values();
        Iterator iter = vals.iterator();
        StringBuffer sb = new StringBuffer(vals.size()*132);
        while (iter.hasNext()) {
            sb.append(iter.next().toString());
            if (iter.hasNext()) sb.append(", ");
        }
        return sb.toString();
    }


    protected Object makeDateMapHashKey(ChannelIdIF id) {
        return new DateKey(id);
    }
    protected Object makeDateMapHashKey(ChannelDataIF cd) {
        return new DateKey(cd.getChannelId());
    }
    protected Object makeParmMapHashKey(ChannelDataIF cd) {
        return new ParmKey(cd.getChannelId(), cd.getDateRange());
    }

    static private class DateKey implements Serializable {
      ChannelIdIF id;
      public DateKey(ChannelIdIF id) {
        this.id = id;
      }
      public boolean equals(Object obj) {
        if (obj == this) return true;
        if (! this.getClass().isInstance(obj)) return false;
        DateKey dk = (DateKey) obj;
        return id.equals(dk.id);
      }
      public int hashCode() {
        return id.hashCode();
      }
      public String toString() {
        return id.toDelimitedSeedNameString('.');
      }
    }
    static private class ParmKey implements Serializable {
      ChannelIdIF id;
      DateRange   dr;
      public ParmKey(ChannelIdIF id, DateRange dr) {
        this.id = id;
        this.dr = dr;
      }
      public boolean equals(Object obj) {
        if (obj == this) return true;
        if (! this.getClass().isInstance(obj)) return false;
        ParmKey pk = (ParmKey) obj;
        return ( id.equals(pk.id) && dr.equals(pk.dr) );
      }
      public int hashCode() {
        return ( id.hashCode()+dr.hashCode() );
      }
      public String toString() {
        return id.toDelimitedSeedNameString('.') +"|"+ dr.toDateString("yyyy/MM/dd:HH:mm:ss"); // no fractional secs
      }
    }
}   // end of ChannelDataMap class
