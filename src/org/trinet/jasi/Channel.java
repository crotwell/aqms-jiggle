// NOTE: aww removed ChannelReader/ChannelWriter class(s) for the binary/ascii i/o caching
//TODO: separate out ChannelFormatter class with static String methods
// could create LatLonZ subclass to include "edepth" attribute offset,
// could replace (dist,hdist,azimuth) with org.trinet.util.gazetteer.DistanceAzimuthElevation like object
// bundled with the latlonz
package org.trinet.jasi;

import java.sql.*;
import java.util.*;
import java.io.*;

import jregex.Pattern;

import org.trinet.jdbc.*;
import org.trinet.jdbc.table.*;
import org.trinet.jdbc.datatypes.*;

import org.trinet.util.*;
import org.trinet.util.gazetteer.*;

/**
 * Seed channel object. Contains a ChannelName member which includes
 * descriptive information for the channel like location
 * of the site (latlonz) and response info. <p>
 *
 * @See: org.trinet.jasi.ChannelName
 */

public abstract class Channel extends AbstractChannelData implements SeismicChannelIF {

    public static boolean debug = false;

    // Constants moved to gazetteer DistanceBearingIF
    //public static final double NULL_DIST    = 99999.;
    //public static final double NULL_AZIMUTH = Double.NaN;

    /** Channel description identifier */
    { channelId = new ChannelName(); }

    /** Location of the sensor. Note that 'z' is in km with down negative. */
    protected LatLonZ latlonz = new LatLonZ();

    /** Location emplacement depth in meters. 0. m unless this is a downhole sensor. */
    protected DataDouble snsDepth = new DataDouble(0.0);

    protected DataDouble snsAz  = new DataDouble(); // from Channel_data.azimuth
    protected DataDouble snsDip = new DataDouble(); // from Channel_data.dip

    /** Relative distance of this channel from the last point for which calcDistance was called.
    * Usually it the associated solution in km. Used for distance sorting, etc. Defaults to
    * NULL_DIST so channels with unknown distances show up at the END of a list. This is the true
    * distance taking into account depth and elevation. */
    private DataDouble  dist = new DataDouble(NULL_DIST); // see gazetteer DistanceBearingIF

    /** Horizontal distance of this channel.*/
    private DataDouble hdist = new DataDouble(NULL_DIST);

    /** Azimuth from the station/channel to the associated Solution. Degrees
        clockwise from north.*/
    private DataDouble azimuth = new DataDouble();

    /** Nominal sample rate (samples/second) expected for this channel.
     * The actual data rate of retreived time series may differ.*/
    public DataDouble sampleRate = new DataDouble();

    /**
     * The instrument gain.<br>
     * Units are counts/(cm/sec) for velocity or counts/(cm/sec2) for acceleration.
    */
    //public ChannelGain gain = new ChannelGain();

    //Problem many corrections, may be new types in future too.
    //Do we really want to read in "all" types for given channel at once?
    //Requires reading of entire stacorrection table, multiple self-joins
    //or multiple query scans for same channelList.
    //Rather than having the Channel load/create the mag corr/calibr data
    //each xxxMethod could create sta correction object, the xxxMethod may
    //have to query db for other additional info anyway, so the xxxMethod
    //could lookup sta by name and update channel found in input list with data?
    //
    //Either let the xxxMethod query db for channel mag corr/calibr data or
    //invoke method in Channel(xx) after object is created to query datasource
    //for missing mag corr/calibr data. For example, parse a "specific" correction
    //type query resultSet into the corrrection HashMap object
    //method gets specified mag corr type matching name and date, adds it to map
    //method gets all mag corr types matching name and date, adds them to map
    //
    //Generic map keyed by corrType to bundle corr types/ magCalibration objects
    //accessors:
    //generic getCorrection(key_type) setCorrection(key_type, value)
    //or specific getMagCalibration(key_type) setMagCalibration(key_type, value)

    // Waveform clipping info is currently loaded into the corrMap via a ChannelMap_xxxParms
    // table join with StaCorrections.
    // It could instead be derived from a join with ChannelResponse (gain) and stored in responseMap
    // or it could be recovered from ChannelMap_xxx with no join and stored separately in clipAmpMap - aww
    protected GlobalChannelCorrectionDataMap corrMap = new GlobalChannelCorrectionDataMap(11);
    protected ChannelDataMap responseMap = new ChannelDataMap(5, ChannelResponse.create());
    protected ChannelDataMap clipAmpMap  = new ChannelDataMap(5, ChannelClipAmpData.create());

    protected static boolean loadCorrections = true; // "clip" data may be found in corr calibrations
    protected static boolean loadClipping    = true; // set true if clip data not correction calibr or
    protected static boolean loadResponse    = true; // perhaps "clip" amps can be found with response
    protected static boolean dbLookup    = true; // ancillary data getter methods poll db if not in map

    public static int defaultType = JasiObject.DEFAULT;
    
    /** Constructor protected, use factory method to concrete instance.
     * @see #create()
     */
    protected Channel() { }

    /**
     * Factory Method: This is how you instantiate a jasi object. You do
     * NOT use a constructor. This is so that this "factory" method can create
     * the type of object that is appropriate for your site's database.
     * If no type is specifed it creates a Channel object of the DEFAULT type.<br>
     * @See: JasiObject
     */
    public static final Channel create() {
      return create(defaultType);
    }

    /**
     * Instantiate an object of this type. You do NOT use a constructor.
     * This "factory" method creates various concrete implementations.
     * The argument is an integer implementation type.<br>
     * @See: JasiObject
     */
    public static final Channel create(int schemaType) {
      return create(JasiFactory.suffix[schemaType]);
    }

    /**
     * Instantiate an object of this type. You do NOT use a constructor.
     * This "factory" method creates various concrete implementations.
     * The argument is as 2-char implementation suffix.<br>
     * @See: JasiObject
     */
    public static final Channel create(String suffix) {
      return (Channel) JasiObject.newInstance("org.trinet.jasi.Channel", suffix);
    }

    /** Implement Channelable interface. */
    // Can't call it getChannel() because that's used to return channel string
    public Channel getChannelObj() {
      return this;
    }
    /** Sets member attributes values to those of the input. */
    public void setChannelObj(Channel chan) {
      copy((Channelable) chan);
    }

    /** Return the ChannelName object of this Channel */
    public ChannelName getChannelName() {
        return (ChannelName) channelId;
    }
    /** Set this channel's name.
     * The channel name attributes are set those of the input ChannelName object.
    *  Returns this Channel so you can construct with call like:
    *  Channel chan = Channel.create().setChannelName("CI", "COK", "EHZ", "01");
    */
    public Channel setChannelName(ChannelName cname) {
      //channelId = (ChannelName) cname.clone(); // why create another object?
      ((ChannelName) channelId).copy(cname);

      return this;
    }
    /** Set this channel's name.
     * Sets attributes to the respective values of the input objects.
     */
    public Channel setChannelName(String net, String sta, String seedchan, String location,
                    String auth, String subsource, String comp, String channelsrc) {
      if (channelId == null) channelId =
                        new ChannelName(net, sta, seedchan, location, auth, subsource, comp, channelsrc);
      else ((ChannelName)channelId).set(net, sta, seedchan, location, auth, subsource, comp, channelsrc);
      return this;
    }
    /** Sets channel name attributes to the respective values of the input objects.
     * Also sets ChannelName.channel = ChannelName.seedchan,
     * and the remaining unspecified attributes to their defaults.
     */
    public Channel setChannelName(String net, String sta, String seedchan, String location) {
      return setChannelName(net, sta, seedchan, location, null, null, seedchan, null);
    }
    /** Sets channel name attributes to the respective values of the input objects.
     * Also sets ChannelName.channel = ChannelName.seedchan,
     * and the remaining unspecified attributes to their defaults.
     */
    public Channel setChannelName(String net, String sta, String seedchan, String location,
                    String auth, String subsource) {
      return setChannelName(net, sta, seedchan, location, auth, subsource, seedchan, null);
    }

     /**
      * Sets the ChannelName attributes to their defaults.
      */
    public Channel clearChannelName() {
      ((ChannelName) channelId).clear();
      return this;
    }

    public String getChannelNameAttribute(int attributeId) {
      switch (attributeId) {
        case NET :
          return getNet();
        case SEEDCHAN :
          return getSeedchan();
        case LOCATION :
          return getLocation();
        case STA :
          return getSta();
        case CHANNEL :
          return getChannel();
        case CHANNELSRC :
          return getChannelsrc();
        case AUTH :
          return getAuth();
        case SUBSOURCE :
          return getSubsource();
        default:
          System.err.println("Unknown ChannelName attributeId: " + attributeId);
          return ""; // throw new IllegalArgumentException();
      }
    }

///////////////////////////////////////////////////////////////////////////////////
    public String getSta() {
        return channelId.getSta();
    }
    public String getNet() {
        return channelId.getNet();
    }
    public String getSeedchan() {
        return channelId.getSeedchan();
    }
    public String getLocation() {
        return channelId.getLocation();
    }
    public String getChannel() {
        return channelId.getChannel();
    }
    public String getChannelsrc() {
        return channelId.getChannelsrc();
    }
    public String getAuth() {
        return ((AuthChannelIdIF) channelId).getAuth();
    }
    public String getSubsource() {
        return ((AuthChannelIdIF) channelId).getSubsource();
    }
    public void setSta(String sta) {
        channelId.setSta(sta);
    }
    public void setNet(String net) {
        channelId.setNet(net);
    }
    public void setLocation(String location) {
        channelId.setLocation(location);
    }
    public void setChannel(String channel) {
        channelId.setChannel(channel);
    }
    public void setSeedchan(String seedchan) {
        channelId.setSeedchan(seedchan);
    }
    public void setChannelsrc(String channelsrc) {
        channelId.setChannelsrc(channelsrc);
    }
    public void setAuth(String auth) {
        ((AuthChannelIdIF) channelId).setAuth(auth);
    }
    public void setSubsource(String subsource) {
        ((AuthChannelIdIF) channelId).setSubsource(subsource);
    }
///////////////////////////////////////////////////////////////////////////////////

    public double getSampleRate() {
      return sampleRate.doubleValue();
    }
    public void setSampleRate (double rate) {
      sampleRate.setValue(rate);
    }

    public double getSensorDepth() {
      return (snsDepth.isNull()) ? 0. : snsDepth.doubleValue();
    }
    public void setSensorDepth(double value) {
      if (Double.isNaN(value)) snsDepth.setNull(true);
      else snsDepth.setValue(value);
    }

    public boolean isBorehole() {
        return (snsDepth.isNull()) ? super.isBorehole() : (snsDepth.doubleValue() > 10.); // meters 
    }

    //
    public boolean isHorizontal() {
        if (snsAz.isNull() || snsDip.isNull()) return super.isHorizontal();
        return (Math.abs(snsDip.doubleValue()) < 45.) ? true : false;
    }

    public boolean isVertical() {
        if (snsDip.isNull() || snsAz.isNull()) return super.isVertical();
        return (Math.abs(snsDip.doubleValue()) > 45.) ? true : false;
    }
    //

    public double getSensorAzimuth() {
      return (snsAz.isNull()) ? Double.NaN : snsAz.doubleValue();
    }
    public void setSensorAzimuth(double value) {
      if (Double.isNaN(value)) snsAz.setNull(true);
      else snsAz.setValue(value);
    }

    public double getSensorDip() {
      return (snsDip.isNull()) ?  Double.NaN : snsDip.doubleValue();
    }
    public void setSensorDip(double value) {
      if (Double.isNaN(value)) snsDip.setNull(true);
      else snsDip.setValue(value);
    }

    public LatLonZ getLatLonZ() {
      return latlonz;
    }
    public void setLatLonZ(LatLonZ latLonZ) {
      this.latlonz = latLonZ;
    }

    /**
     * Calculates and sets both the horizontal distance (2-D or epicentral) and the
     * true distance (3-D or hypocentral) of this channel in <i>km</i> measured to
     * the input location. Returns the true distance.
     * Also sets the calculated azimuth from channel location to the input location.
     * Note: this is better than setDistance() or setHorizontalDistance() because
     * it does both and insures they are consistent. If the channel's LatLonZ or
     * the LatLonZ in the argument is null, both distances are set to NULL_DIST.
     * */
    public double calcDistance(GeoidalLatLonZ loc) {
        LatLonZ llz = (LatLonZ) loc.getLatLonZ().clone();
        if (llz == null || llz.isNull() || ! hasLatLonZ() ) { //lat/lon unknown
          setDistanceBearingNull();
        }
        else {
          // could do instanceof to discriminate types of input like Solution
          // For a Solution input do we include channel elev or only source
          // depth in "slant" distance ?
          // ? add GeoidalLatLonZ IF method to test for depth vs. elevation sign - aww ?
          setDistance(latlonz.distanceFrom(llz));
          setHorizontalDistance(latlonz.horizontalDistanceFrom(llz));
          //setAzimuth(latlonz.azimuthTo(llz)); // degrees from 0 North SEAZ, not ESAZ -aww
          //setAzimuth(latlonz.azimuthFrom(llz)); // degrees from 0 North ESAZ 06/11/2004 aww
          setAzimuth(llz.azimuthTo(latlonz)); // degrees from 0 North ESAZ 06/11/2004 aww
        }
        return getDistance();
    }

    public void setDistanceBearingNull() {
        setDistance(NULL_DIST);           // so they'll be at END of a sorted list
        setHorizontalDistance(NULL_DIST);
        setAzimuth(NULL_AZIMUTH);
    }

    public double getDistance() {
      return (dist.isNull()) ? NULL_DIST : dist.doubleValue();
    }
    public double getHorizontalDistance() {
      return (hdist.isNull()) ? NULL_DIST : hdist.doubleValue();
    }
    public double getVerticalDistance() {
      double distance = getDistance();
      double horizDistance = getHorizontalDistance();
      // Note value is positive square root
      return (distance == NULL_DIST || horizDistance == NULL_DIST) ?
         NULL_DIST : Math.sqrt((Math.pow(distance,2)-Math.pow(horizDistance,2)));
    }

    /** Set hypocentral distance ONLY. NOTE: calcDistance(LatLonZ) is prefered because
    * it sets both hypocentral and horizontal distance.
    * */
    public void setDistance(double distance) {
      if (Double.isNaN(distance)) dist.setValue(NULL_DIST);
      else dist.setValue(distance);
    }
    /** Set horizontal distance ONLY. NOTE: calcDistance(LatLonZ) is prefered because
    * it sets both hypocentral and horizontal distance.
    * */
    public void setHorizontalDistance(double distance) {
      if (Double.isNaN(distance)) hdist.setValue(NULL_DIST);
      else hdist.setValue(distance);
    }

    public double getAzimuth() {  // see DistanceBearingIF
      return (azimuth.isNull()) ?  NULL_AZIMUTH :  azimuth.doubleValue();
    }
    public void setAzimuth(double az) {
      if (Double.isNaN(az)) azimuth.setValue(NULL_AZIMUTH);
      else azimuth.setValue(az);
    }

     /** Return true if the channel has a valid LatLonZ, false if not. */
     public boolean hasLatLonZ() {
        return  ! (latlonz == null || latlonz.isNull()) ;
     }

    public boolean hasCorrection(String corrType) {
      return hasCorrection(new java.util.Date(), corrType);
    }
    public boolean hasCorrection(java.util.Date date, String corrType) {
      DataCorrectionIF dc = getCorrection(date, corrType);
      if (dc == null) return false;
      DataNumber value = dc.getCorr();
      if (debug) {
        if (value == null) System.out.println("DEBUG CHANNEL hasCorrection value is null.");
        else System.out.println("      CHANNEL hasCorrection value.isNull()? : " + (value.isNull()));
      }
      return (value != null && ! value.isNull());
    }
    public DataCorrectionIF getCorrection(String corrType) {
      return getCorrection(new java.util.Date(), corrType);
    }
    public DataCorrectionIF getCorrection(java.util.Date date, String corrType) {
        return getCorrection(date, corrType, dbLookup);
    }
    public DataCorrectionIF getCorrection(java.util.Date date, String corrType, boolean lookup) {
      // Returns null if none found in map that matches the inputs
      ChannelCorrectionDataIF cd = corrMap.get(channelId, date, corrType);
      if (cd == null && lookup) {
          // Scenario:
          // For multiple daterange clip,cutoff,corr values in timespan of Channel
          // we probe for the data by date and if found put it into the collection map 
          cd = getCorrectionFactoryForType(corrType);
          cd = (ChannelCorrectionDataIF) cd.getByChannelId(channelId, date);
          if (cd != null) corrMap.add(cd);
      }
      return cd;
    }

    public void setCorrection(DataCorrectionIF corr) {
      if (corr == null) {
        System.err.println("Warning! Channel.setCorrection(corr), input is null - no-op.");
        return;
      }
      corrMap.add((ChannelCorrectionDataIF)corr);
    }

    public void setAutoLoadingOfAssocDataOn() {
       loadResponse = true;
       loadCorrections = true;
       loadClipping = true;
    }
    public void setAutoLoadingOfAssocDataOff() {
       loadResponse = false;
       loadCorrections = false;
       loadClipping = false;
    }

    public static boolean getDbLookup() { return dbLookup; }
    public static void setDbLookup(boolean tf) { dbLookup = tf; }
    public static void setLoadResponse(boolean tf) { loadResponse = tf; }
    public static void setLoadKnownCorrections(boolean tf) { loadCorrections = tf; }
    public static void setLoadClippingAmps(boolean tf) { loadClipping = tf; }

    public static void setLoadOnlyLatLonZ() {
        loadResponse = false;
        loadCorrections = false;
        loadClipping = false;
        System.out.println(
                "Channel associated load settings: response="+loadResponse+
                ", corrections="+loadCorrections+", clipping="+loadClipping
        );
    }

    /** Loads into instance hashmaps the calibration, waveform clipping, and gain values
     * valid for the Channel on the specified input date.
     */
    public void loadAssocData(java.util.Date date) {
      if (loadCorrections) loadKnownCorrections(date);
      if (loadResponse) loadResponse(date);
      if (loadClipping) loadClippingAmps(date);
    }

    abstract public int loadKnownCorrections(java.util.Date date) ;
    abstract public ChannelCorrectionDataIF getCorrectionFactoryForType(String corrType);

    public int loadKnownCurrentCorrections() {
      int value = loadKnownCorrections(new java.util.Date()) ;
      return value;
    }

    public boolean loadCorrection(ChannelCorrectionDataIF corr, java.util.Date date) {
      //System.out.println("** Correction type : " + corr.toString() + " date: " + EpochTime.dateToString(date));
      //bm.reset();
      ChannelCorrectionDataIF data =
          (ChannelCorrectionDataIF) corr.getByChannelId(channelId, date);
      if (data == null) return false;
      setCorrection(data);
      //bm.print("Main: load correction time");
      return true;
    }

    public boolean loadCorrections(ChannelCorrectionDataIF corr, DateRange dr) {
      //System.out.println("** Correction type : " + corr.toString() + " dateRange: " + dr.toString());
      //bm.reset();
      Collection data  = corr.getByChannelId(channelId, dr);
      if (data == null) return false;
      corrMap.addAll(data);
      //bm.print("Main: load correction time");
      return true;
    }

    public boolean loadCurrentResponse() {
      return loadResponse(new java.util.Date() );
    }
    public boolean loadResponse(java.util.Date date) {
      return (responseMap.load(channelId, date) != null);
    }
    public boolean loadClippingAmps(java.util.Date date) {
      return (clipAmpMap.load(channelId, date) != null);
    }
    public boolean hasCurrentGain() {
      return hasGain(new java.util.Date());
    }
    public boolean hasGain(java.util.Date date) {
      ChannelResponse cr = getResponse(date);
      return (cr != null && ! Double.isNaN(cr.getGainValue()));
      //return gain;
    }
    public ChannelGain getCurrentGain() {
      return getGain(new java.util.Date());
    }
    public ChannelGain getGain(double trueSecs) { // for UTC true seconds input by default -aww 2008/02/11
      return getGain(LeapSeconds.trueToDate(trueSecs)); // for UTC true seconds input by default -aww 2008/02/11
    }
    public ChannelGain getGain(java.util.Date date) {
      return getGain(date, dbLookup);
    }
    public ChannelGain getGain(java.util.Date date, boolean lookup) {
      ChannelResponse cr = getResponse(date, lookup);
      return (cr == null) ?  new ChannelGain() :  cr.getGain();
    }
    public ChannelResponse getCurrentResponse() {
      return getResponse(new java.util.Date());
    }
    public ChannelResponse getResponse(java.util.Date date) {
      return getResponse(date, dbLookup);
    }
    public ChannelResponse getResponse(java.util.Date date, boolean lookup) {
      return (ChannelResponse) ( (lookup) ? responseMap.lookUp(channelId, date) : responseMap.get(channelId, date));
    }
    public void setResponse(ChannelResponse cr) {
      responseMap.add(cr);
    }

// Kludge for Doug's methods here: -aww 09/01/2004
    /** Return true if this channel is a velocity component.
     * use channel's current gain units (e.g. Units.CMS).
     * If gain units are undefined, uses 2nd character of SEED channel name
     * and returns <i>true</i> for all high-gain components.
     * @see #isHighGain()
     */
    public boolean isVelocity() {
        ChannelGain cg = getCurrentGain();
        return (cg.getUnits() != Units.UNKNOWN && cg.isValidNumber()) ? cg.isVelocity() : isHighGain();
    }
    public boolean isVelocity(java.util.Date date) {
        ChannelGain cg = getGain(date);
        return (cg.getUnits() != Units.UNKNOWN && cg.isValidNumber()) ? cg.isVelocity() : isHighGain();
    }
    /** Return true if this channel is an acceleration component.
     * use channel's current gain units (e.g. Units.CMSS).
     * If gain units are undefined, uses 2nd character of SEED channel name
     * and returns <i>true</i> for all low-gain components.
     * NOTE (DDG): this could be WRONG in some cases. E.g. "EL_" is lowgain velocity.
     * @see #isLowGain()
     */
    public boolean isAcceleration() {
        ChannelGain cg = getCurrentGain();
        return (cg.getUnits() != Units.UNKNOWN && cg.isValidNumber()) ? cg.isAcceleration() : isLowGain();
    }
    public boolean isAcceleration(java.util.Date date) {
        ChannelGain cg = getGain(date);
        return (cg.getUnits() != Units.UNKNOWN && cg.isValidNumber()) ? cg.isAcceleration() : isLowGain();
    }
// end of kludge

    //ChannelClipAmpData should map to a ChannelMap_xxxx table
    public double getMaxAmpValue(java.util.Date date) {
        return getMaxAmpValue(date, dbLookup);
    }
    public double getMaxAmpValue(java.util.Date date, boolean lookup) {
      ChannelClipAmpData cd = (ChannelClipAmpData)
          ( (lookup) ? clipAmpMap.lookUp(channelId, date) : clipAmpMap.get(channelId, date) );
      return (cd == null) ?  0. :  cd.getMaxAmpValue(); // was Double.NaN for null -aww 2009/03/10
    }
    public double getClipAmpValue(java.util.Date date) {
        return getClipAmpValue(date, dbLookup);
    }
    public double getClipAmpValue(java.util.Date date, boolean lookup) {
      ChannelClipAmpData cd = (ChannelClipAmpData)
          ( (lookup) ? clipAmpMap.lookUp(channelId, date) : clipAmpMap.get(channelId, date) );
      return (cd == null) ?  0. :  cd.getClipAmpValue(); // was Double.NaN for null -aww 2009/03/10
    }

    /**
     * Make a "deep" copy, except for corrections
     * which are aliased to the input's map contents.
     */
    public boolean copy(ChannelDataIF cd) {
      // no-op if input is not a Channelable object
      if (cd == null || ! (cd instanceof Channelable)) return false;
      return (copy((Channelable) cd) != null);
    }
    public Channel copy(Channelable ch) {
        Channel chan = ch.getChannelObj();
        if (this != chan) {
          ((ChannelName) channelId).copy(chan.getChannelName());
          setChannelObjData(chan);
        }
        return this;
    }

    public void setChannelObjData(Channelable ch) {
        setChannelObjData(ch.getChannelObj());
    }

    public void setChannelObjData(Channel chan) {

        if (chan == this) return;

        latlonz.copy(chan.latlonz);

        if (chan.snsDepth.isNull()) snsDepth.setNull(true);
        else snsDepth.setValue(chan.snsDepth.doubleValue());
        
        if (chan.snsAz.isNull()) snsAz.setNull(true);
        else snsAz.setValue(chan.snsAz);

        if (chan.snsDip.isNull()) snsDip.setNull(true);
        else snsDip.setValue(chan.snsDip);

        if (chan.sampleRate.isNull()) sampleRate.setNull(true);
        else sampleRate.setValue(chan.sampleRate.doubleValue());

        // aww reset relative location data, don't use that of the input ?
        setDistanceBearingNull();

        if (! chan.dist.isNull())
            dist.setValue(chan.dist.doubleValue());
        if (! chan.hdist.isNull())
            hdist.setValue(chan.hdist.doubleValue());
        if (! chan.azimuth.isNull())
            azimuth.setValue(chan.azimuth.doubleValue());

        // lookup data references (correction maps) should be SHARED for instances ?
        corrMap = (GlobalChannelCorrectionDataMap) chan.corrMap;
        responseMap = (ChannelDataMap) chan.responseMap;
        clipAmpMap  = (ChannelDataMap) chan.clipAmpMap;

        // aww also copy input's data validity DateRange:
        getDateRange().setLimits(chan.getDateRange());

    }

    public Object clone() {
        Channel chan = (Channel) super.clone();
        chan.channelId = (ChannelName) channelId.clone();
        chan.latlonz = (LatLonZ) latlonz.clone();
        chan.snsDepth   = (DataDouble) snsDepth.clone();
        chan.snsAz = (DataDouble) snsAz.clone();
        chan.snsDip = (DataDouble) snsDip.clone();
        chan.dist    = (DataDouble) dist.clone();
        chan.azimuth = (DataDouble) azimuth.clone();
        chan.sampleRate = (DataDouble) sampleRate.clone();
        //chan.gain    = (ChannelGain) gain.clone();
        chan.corrMap = (GlobalChannelCorrectionDataMap) corrMap.clone();
        chan.responseMap = (ChannelDataMap) responseMap.clone();
        chan.clipAmpMap  = (ChannelDataMap) clipAmpMap.clone();
        return chan;
    }

    /* REPLACED BY method equalsName(Ch)
     * Case sensitive compare. Returns true if sta, net and seedchannel are
     * equal. Does not check the other fields.  The "extra" fields like
     * 'channel' and 'location' are populated in some data sources and not
     * others.
    public boolean equals(Object x) {
        if (this == x) return true;
        else if ( x == null || ! (x instanceof Channel)) return false;
        Channel chan = (Channel) x;
        return(channelId.equals(chan.channelId));
    }
    */

    /**
     * Case insensitive compare. Returns true if sta, net, and
     * seedchan and location attributes are equal. <p>
     */
    public boolean equalsNameIgnoreCase(ChannelIdIF id) {
        if (id == null) return false;
        if (channelId == id) return true;
        return channelId.equalsNameIgnoreCase(id);
    }
    /**
     * Case sensitive compare. Returns true if sta, net, and
     * seedchan and location attributes are equal and match case. <p>
     */
    public boolean equalsName(ChannelIdIF id) {
        if (id == null) return false;
        if (channelId == id) return true;
        return channelId.equalsName(id); // equals -> equalsName bug fix 11-18-2005 -aww
    }
    public boolean sameNetworkAs(ChannelIdentifiable chan) {
       return (chan == this) ? true : super.sameNetworkAs((ChannelIdIF) chan.getChannelId());
    }
    public boolean sameNetworkAs(Channelable chan) {
       return (chan == this) ? true : super.sameNetworkAs((ChannelIdIF) chan.getChannelObj().getChannelId());
    }
    public boolean sameStationAs(ChannelIdentifiable chan) {
       return (chan == this) ? true : super.sameStationAs((ChannelIdIF) chan.getChannelId());
    }
    public boolean sameStationAs(Channelable chan) {
       return (chan == this) ? true : super.sameStationAs((ChannelIdIF) chan.getChannelObj().getChannelId());
    }
    public boolean sameSeedChanAs(ChannelIdentifiable chan) {
       return (chan == this) ? true : super.sameSeedChanAs((ChannelIdIF) chan.getChannelId());
    }
    public boolean sameSeedChanAs(Channelable chan) {
       return (chan == this) ? true : super.sameSeedChanAs((ChannelIdIF) chan.getChannelObj().getChannelId());
    }
    public boolean sameTriaxialAs(ChannelIdentifiable chan) {
       return (chan == this) ? true : super.sameTriaxialAs((ChannelIdIF) chan.getChannelId());
    }
    public boolean sameTriaxialAs(Channelable chan) {
       return (chan == this) ? true : super.sameTriaxialAs((ChannelIdIF) chan.getChannelObj().getChannelId());
    }

    /** Returns the result of comparing the strings generated by invoking toString()
    * for this instance and the input object.
    * Throws ClassCastException if input object is not an instanceof Channel.
    * A return of 0 == value equals, <0 == value less than, >0 == value greater than,
    *  the string value of the input argument.
    */
    public int compareTo(Object x) {
        if (x == null ||  x.getClass() != getClass() )
                 throw new ClassCastException("compareTo(object) argument must be a Channel class type: "
                                + x.getClass().getName());
        return channelId.toString().compareTo( ((Channel) x).channelId.toString());
    }

    /**
     * Look up the Channel described by the input in the DataSource and
     * return a fully populated Channel object.
     * This is how you look up LatLonZ and response info for a single channel.
     * Note this finds the LATEST entry if more then one entry exists
     * (returns record with largest 'ondate').
     * Does NOT require that the channel be "active".
     * If no entry is found, this method return the input reference.
     */
    public abstract Channel lookUp(Channelable chan) ; // SYS_EXTRACT_UTC(CURRENT_TIMESTAMP)
    public abstract Channel lookUp(Channelable chan, java.util.Date date) ;
    public abstract Channel lookUpLatest(Channelable chan) ; // NO DATE CHECK, LAST HISTORICALLY ACTIVE

    /**
     * Return a list of current channels that are defined by an arbitrary name.
     * The implementation details of how this is accomplished are left to the concrete instance.
     * For ChannelTN it is done by joining to the JASI_CHANNEL_VIEW & JASI_CONFIG_VIEW tables.
     */
    public abstract ChannelableList getNamedChannelList(String name);
    public abstract ChannelableList getNamedChannelList(String name, java.util.Date date);

    /**
     * Returns from the default DataSource a ChannelList
     * containing currently active Channels.
     */
    public ChannelList readList() { return readList((java.util.Date) null); }
    /**
     * Return from the default DataSource a ChannelList
     * containing Channels that were active on the input date.
     */
    public abstract ChannelList readList(java.util.Date date);
    public abstract ChannelList readListByName(String name, java.util.Date date);

    /** Return a list of channels that were active on the given date and
    *  that match the list of component types given in
    * the array of strings. The comp is compared to the SEEDCHAN field in the NCDC
    * schema.<p>
    *
    * SQL wildcards are allowed as follows:<br>
    * "%" match any number of characters<br>
    * "_" match one character.<p>
    *
    * For example "H%" would match HHZ, HHN, HHE, HLZ, HLN & HLE. "H_" wouldn't
    * match any of these but "H__" would match them all. "_L_" would match
    * all components with "L" in the middle of three charcters (low gains).
    * The ANSI SQL wildcards [] and ^ are not supported by Oracle.
    */

    public abstract ChannelList getByComponent(String[] compList, java.util.Date date) ;

    /** Return a list of currently acive channels that match the list of
     *  component types given in
    * the array of strings. The comp is compared to the SEEDCHAN field in the NCDC
    * schema.<p>
    *
    * SQL wildcards are allowed as follows:<br>
    * "%" match any number of characters<br>
    * "_" match one character.<p>
    *
    * For example "H%" would match HHZ, HHN, HHE, HLZ, HLN & HLE. "H_" wouldn't
    * match any of these but "H__" would match them all. "_L_" would match
    * all components with "L" in the middle of three charcters (low gains).
    * The ANSI SQL wildcards [] and ^ are not supported by Oracle.
    */

    public ChannelList getByComponent(String[] compList) {
        return getByComponent(compList, (java.util.Date) null ) ;
    }

    // BELOW ADDED after Doug's changes, rework to use ChannelDataList map?
    /** Match this reading's channel with one in this list. If a match is found
     * update this readings channel object with attribute values of the Channel
     * returned from reference list.
     * This is used to match objects to a more complete description of a channel then
     * they probably have otherwise. Gives access to lat/lon/z, response info, corrections,
     * distance from epicenter, etc. */
    public boolean setChannelObjFromList(ChannelList channelList, java.util.Date aDate) {
        if (channelList == null || channelList.isEmpty()) return false;
        Channel chanMatch = channelList.findSimilarInMap(getChannelObj(), aDate);
        if (chanMatch != null) {
          setChannelObjData(chanMatch);
            //System.out.println("setChannelObjFromList found channel: "+getChannel().toString());
          return true;
        } else {
            //System.out.println("setChannelObjFromList can't find channel: "+getChannel().toString());
          return false;
        }
    }
    /** Use object's ChannelList if it has been set, otherwise MasterChannelList if it has been set.
     * Returns true if found in list, else returns false list doesn't exist or Channel is not in list. */
    public boolean setChannelObjFromMasterList(java.util.Date aDate) {
        return setChannelObjFromList(MasterChannelList.get(), aDate); // aww ?
    }
    public boolean matchChannelWithDataSource(java.util.Date aDate) { // aww
      boolean status = setChannelObjFromMasterList(aDate); // do we want this behavior ?
      if (status) return true;
      Channel ch = getChannelObj();
      ch = ch.lookUp(ch, aDate);
      status = ( ch != getChannelObj() );
      setChannelObjData(ch);
      return status;
    }

    /**
     * Returns true if Channel of this reading has LatLonZ data set.
     * Otherwise lookups data for matching Channel in default ChannelList,
     * that was active at reading time, or if reading time is null,
     * the invocation time.
     * If no such data is found in ChannelList, tries to recover LatLonZ data
     * from the default DataSource.
     * Returns false if Channel location remains unknown.
     */
    public boolean insureLatLonZ(java.util.Date aDate) { // aww
      if (getChannelObj().hasLatLonZ()) return true;
      if (setChannelObjFromMasterList(aDate)) {
        if (getChannelObj().hasLatLonZ()) return true;
      }
      return matchChannelWithDataSource(aDate);
    }
//
//CODE Below could be moved up into the super class AbstractChannelData
//     then could add methods to interface declaration as well -aww
//
    // 7/18/2003 DDG added, removed by aww since ChannelData uses DateRange
    // Installation time of the channel.
    //public DataDouble timeOn  = new DataDouble(Double.MIN_VALUE) ;
    // Deinstallation time of the channel.
    //public DataDouble timeOff = new DataDouble(Double.MAX_VALUE);

    /** Return 'true' if channel was active at input true UTC seconds time.
     * @see AbstractChannelData.isValidFor(java.util.Date)
     * */
    public boolean wasActive(double trueSecs) { // for UTC true seconds input by default -aww 2008/02/11
      //return (trueSecs >= getTimeOn()) && (trueSecs <= getTimeOff());
      return dateRange.contains(trueSecs);
    }

    /** Set lower time bound in true UTC seconds for which this Channel's data are valid.
     * May not necessarily be the actual installation time.
     */
    public void setTimeOn(double trueSecs) { // for UTC true seconds input by default -aww 2008/02/11
      //timeOn.setValue(trueSecs);
      dateRange.setMinTrueSecs(trueSecs);
    }
    /** Get lower time bound in UTC seconds for which this Channel's data are valid
     * May not necessarily be the actual installation time.
     */
    public double getTimeOn() { // for UTC true seconds input by default -aww 2008/02/11
      //return timeOn.doubleValue();
      return dateRange.getMinTrueSecs();
    }
    /** Set the upper time bound in true UTC seconds for which thisChannel's data are valid
     * May not necessarily be the actual installation time.
     */
    public void setTimeOff(double trueSecs) { // for UTC true seconds input by default -aww 2008/02/11
      //timeOff.setValue(trueSecs);
      dateRange.setMaxTrueSecs(trueSecs);
    }
    /** Get the upper time bound in UTC seconds for which thisChannel's data are valid
     * May not necessarily be the actual installation time.
     */
    public double getTimeOff() { // for UTC true seconds input by default -aww 2008/02/11
      //return timeOff.doubleValue();
      return dateRange.getMaxTrueSecs();
    }
// End of added time methods

    public GlobalChannelCorrectionDataMap getCorrMap() {
        return corrMap;
    }

    public ChannelDataMap getClipMap() {
        return clipAmpMap;
    }

    public ChannelDataMap getResponseMap() {
        return responseMap;
    }

///////////////////////////////////////////////////////////////////////////////////
    /** Return ASCII representation of the channel name with the fields delimited
     *  by the given string. */
    public String toDelimitedNameString(String delimiter) {
        return channelId.toDelimitedNameString(delimiter);
    }
    /** Return ASCII representation of the channel name with the fields delimited
     *  by "." */
    public String toDelimitedNameString() {
        return ((ChannelName)channelId).toDelimitedNameString();
    }
    /** Return ASCII representation of the channel SEED name with the fields delimited
     *  by the given char. */
    public String toDelimitedSeedNameString(char delimiter) {
        return channelId.toDelimitedSeedNameString(delimiter);
    }
    /** Return ASCII representation of the channel SEED name with the fields delimited
     *  by the given string. */
    public String toDelimitedSeedNameString(String delimiter) {
        return channelId.toDelimitedSeedNameString(delimiter);
    }
    /** Return ASCII representation of the channel SEED name with the fields delimited
     *  by "." */
    public String toDelimitedSeedNameString() {
        return ((ChannelName)channelId).toDelimitedSeedNameString();
    }

    /**
     * Return brief channel name with LatLonZ.
     */
    public String toString(){
      StringBuffer sb = new StringBuffer(64);
      sb.append(channelId.toDelimitedNameString(' ')).append(" ").append(latlonz);
      return sb.toString();
    }
    public String toNeatString(){
        return toString();
    }
    public String getNeatHeader(){
        return "StaNetSeedchan  LatLonZ";
    }
    /**
     * Complete object dump. For debugging. Overides ChannelName.toDumpString()
     * to include LatLonZ.
     */
    public String toDumpString(){

        Format f31 = new Format("%3.1f");
        Format f41 = new Format("%4.1f");
        Format f51 = new Format("%5.1f");
        Format f52 = new Format("%5.2f");
        Format f61 = new Format("%6.1f");

        StringBuffer sb = new StringBuffer(1024);
            sb.append("ChannelId: ").append(((ChannelName) channelId).toDumpString()).append("\n");
            sb.append(" daterange = ").append(dateRange.toString()).append("\n");

            sb.append(" latLonZ = ").append(latlonz);
            sb.append(" snsDepth= ").append(snsDepth.isNull() ? "null" : f52.form(snsDepth.doubleValue()));
            sb.append(" snsDip= ").append(snsDip.isNull() ? "null" : f31.form(snsDip.doubleValue()));
            sb.append(" snsAz= ").append(snsAz.isNull() ? "null" : f41.form(snsAz.doubleValue()));
            sb.append("\n");

            sb.append(" dist= " ).append(dist.isNull() ? "null" : f61.form(dist.doubleValue()));
            sb.append(" hdist= " ).append(hdist.isNull() ? "null" : f51.form(hdist.doubleValue()));
            sb.append(" azim= " ).append(azimuth.isNull() ? "null" : f41.form(azimuth.doubleValue()));
            sb.append(" sampleRate= ").append(sampleRate.isNull() ? "null" : f41.form(sampleRate.doubleValue()));
            sb.append("\n");

            //1.4 sb.append(" clipMap = ").append(clipAmpMap.toString().replaceAll(", ","\n            ")).append("\n");
            //1.4 sb.append(" corrMap = ").append(corrMap.toString().replaceAll(", ","\n            ")).append("\n");
            //1.4 sb.append(" respMap = ").append(responseMap.toString().replaceAll(", ","\n            "));
            //
            String str = new Pattern(", ").replacer("\n           ").replace(clipAmpMap.toValuesString());
            sb.append(" clipMap = ").append(str).append("\n");
            str = new Pattern(", ").replacer("\n           ").replace(corrMap.toValuesString());
            sb.append(" corrMap = ").append(str).append("\n");
            str = new Pattern(", ").replacer("\n           ").replace(responseMap.toValuesString());
            sb.append(" respMap = ").append(str).append("\n");
        return sb.toString();
    }
    /*
    public static void main(String args[]) {

        double slat1 = 50.;
        double slon1 = 179.9;
        LatLonZ s1 = new LatLonZ(slat1,slon1,0.);

        double slat2 = 50.;
        double slon2 = -179.9;
        LatLonZ s2 = new LatLonZ(slat2,slon2,0.);

        double elat1 = 50.1;
        double elon1 = -179.95;
        LatLonZ e1 = new LatLonZ(elat1,elon1,0.);

        double elat2 = 50.1;
        double elon2 = +179.95;
        LatLonZ e2 = new LatLonZ(elat2,elon2,0.);

        System.out.println("Station 1-e1 dist: " + s1.distanceFrom(e1));
        System.out.println("Station 2-e1 dist: " + s2.distanceFrom(e1));
        System.out.println("Station 1-e2 dist: " + s1.distanceFrom(e2));
        System.out.println("Station 2-e2 dist: " + s2.distanceFrom(e2));
        
        slat1 = 50.;
        slon1 = -180.1;
        LatLonZ s1b = new LatLonZ(slat1,slon1,0.);
        slat2 = 50.;
        slon2 = 180.1;
        LatLonZ s2b = new LatLonZ(slat2,slon2,0.);

        System.out.println("Station 1b-e1 dist: " + s1b.distanceFrom(e1));
        System.out.println("Station 2b-e1 dist: " + s2b.distanceFrom(e1));
        System.out.println("Station 1b-e2 dist: " + s1b.distanceFrom(e2));
        System.out.println("Station 2b-e2 dist: " + s2b.distanceFrom(e2));
        
    }
    */
}   // end of Channel class
