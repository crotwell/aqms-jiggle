package org.trinet.jasi;

/**
 * Similar to a StringTokenizer for Channelable lists. Given a Channelable list,
 * it will return smaller lists that contain only Channelable objects for a single
 * Channel.<p>
 * This will sort the ChannelableList by NET.STA .
 * @see:
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: USGS</p>
 * @author Doug Given
 * @version 1.0e
 */

public class StationGrouper {

  ChannelableListIF list, sublist;
  Channelable ch1, chx;
  /** Remember type of Channelable list passed in so we can pass the same out. */
  Class listClass;

  // These pointers are "statefull" for a given instance of this class
  int groupcount = 0;
  int groupsReturned = 0;
  int arraypointer = 0;

  public static final int NAME_SORT = 0;
  public static final int DIST_SORT = 1;

  public StationGrouper() { }

  public StationGrouper(ChannelableListIF list) {
    setList(list, NAME_SORT);
  }

  public StationGrouper(ChannelableListIF list, int sortType) {
    setList(list, sortType);
  }

  /** Set the list to use. */
  public void setList(ChannelableListIF list) {
     setList(list, NAME_SORT);
  }

  public void setList(ChannelableListIF list, int sortType) {
    this.list = list;
    listClass = list.getClass();

    if (sortType == DIST_SORT) list.distanceSort();  // ActiveArrayList has special sort
    else list.sortByStation();  // sort by net/sta/chan

    setGroupCount();

    resetList();
  }

  /** Reset the list pointers to start scans from the start of the list. */
  public void resetList() {
	  groupsReturned = 0;
	  arraypointer = 0;	  
  }
  /** Set the total groups present .*/
  protected void setGroupCount() {
    groupcount = list.getStationCount();
  }
/** Return true if this enumerator has more groups to return. */
  public boolean hasMoreGroups() {
    return countRemainingGroups() > 0;
  }

/** Return a ChannelableList that contains the next group of Channelable
 * objects that are all from the same stations.  Returns null if list
 * was not initiated or no groups remain.
 * */
  public ChannelableListIF getNext()  {
    if (countRemainingGroups() <= 0 ||
        list == null ||
        list.isEmpty()) return null;

    // DDG was getting index out of bounds
    	if (arraypointer >= list.size()) {
    		return null;
    	}
    // get the next in line
    ch1 = (Channelable) list.get(arraypointer);
    arraypointer++;

    // create a sublist of the type of the original list
    try {
      sublist = (ChannelableListIF) Class.forName(listClass.getName()).newInstance();
    }
    catch (Exception ex) {
      System.err.println("StationGrouper - Bad class type: "+listClass.getName());
    }
//    catch (InstantiationException ex) {
//    }catch (IllegalAccessException ex) {
//    }catch (ClassNotFoundException ex) {
//    }
    //add the channel gotten above - will be used for comparision in the loop
    sublist.add(ch1);

    while (arraypointer < list.size()) {
      chx = (Channelable) list.get(arraypointer);
//      if (chan1.sameStationAs(chanx)) {
      if (inSameGroup(ch1, chx)) {
        sublist.add(chx);
        arraypointer++;
      } else {
        break;   // another channel, bail
      }
    } // end while

    groupsReturned++;
    return sublist;
  }
  /** Return true if the two channelabel object belong in the same group. */
  public boolean inSameGroup (Channelable ch1, Channelable ch2) {
    return ch1.getChannelObj().sameStationAs((ChannelIdIF)ch2.getChannelObj());
}
/** Return total number of Channelable object groups that
 * will be returned by repeated calls to getNext(). */
  public int countGroups () {
    return groupcount;
  }
  /** Return remaining number of Channelable objects groups
   * will be returned by repeated calls to getNext().  */
  public int countRemainingGroups () {
    //System.out.println("group count: " +groupcount+ " groupsReturned: " + groupsReturned);
    return countGroups() - groupsReturned;
  }
/**
 * Return a list of channelable objects that are in the same group as 
 * the one passed as 'ch'. Returns empty list if no match.
 * @param ch
 * @return ChannelableListIF
 */
  public ChannelableList getGroupContaining (Channelable ch) {
	  
	  ChannelableList grp;
	  
	  resetList();
	  
	    while (hasMoreGroups()) {

	        grp = (ChannelableList) getNext();
	        
	        if (grp.getIndexOf(ch) > -1 ) {  // is contained
	        	return grp;
	        }
	    }
	    
	    return new ChannelableList();  // nothing found, return empty list
  }

/*
  public static void main(String[] args) {

    System.out.println ("Making connection...");
    DataSource ds = TestDataSource.create("serverma");

    System.out.println (ds.toDumpString());

    AmpList sublist;

    // read in the current station list - use cached version if available
    long evid = 13950596;
    Amplitude amp = Amplitude.create();
    AmpList amplist = new AmpList(amp.getBySolution(evid));

    StationGrouper stationGrouper1 = new StationGrouper(amplist);

    System.out.println("amps = "+ amplist.size());
    System.out.println("grps = "+stationGrouper1.countGroups());

   // amplist.dump();

    int knt = 0;
    while (stationGrouper1.hasMoreGroups()) {
      sublist = (AmpList) stationGrouper1.getNext();
      System.out.println("grp #"+ knt++);
      System.out.println(sublist.toNeatString());
    }
    
  }
*/
}
