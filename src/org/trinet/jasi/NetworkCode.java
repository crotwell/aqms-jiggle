package org.trinet.jasi;

/**
 * Static class to hold value of the network code so that it can be set and
 * accessed globally. This is the two-character FDSN code identifying the
 * seismic network from whence data originates.<p>
 *
 * A current list of valid codes can be found at:
 *
 * http://www.iris.washington.edu/FDSN/networks.txt
 *
 * @author Doug Given
 * @version */

public class NetworkCode  {

    private String code = "  ";
    
    private NetworkCode() {
	
    }
    
    /** Set the code value. If the string is longer then two characters it will be 
     * truncated to two. */
    public void set(String str) {
	code = str.trim().substring(0, 1);
    }

    /** Return the 2-character network code. If set() has not been called, a
        blank string is returned. */
    public String get () {
	return code;
    }

} // Authority
