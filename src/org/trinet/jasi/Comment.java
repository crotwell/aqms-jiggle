package org.trinet.jasi;

import org.trinet.jdbc.datatypes.DataString;

/* !!!! NOT USED !!!!! */
public abstract class Comment extends JasiObject {

       /** The comment text */
       protected DataString text = new DataString();
// ////////////////////////////////////////////////////////////////////////////
    /**
     * Instantiate an object of this type. You do
     * NOT use a constructor. This "factory" method creates various
     * concrete implementations. Creates a Solution of the DEFAULT type.
     * @See: JasiObject
     */
     public static final Comment create() {
  return create(DEFAULT);
     }

    /**
     * Instantiate an object of this type. You do
     * NOT use a constructor. This "factory" method creates various
     * concrete implementations. The argument is an integer implementation type.
     * @See: JasiObject
     */
     public static final Comment create(int schemaType) {
        return create(JasiFactory.suffix[schemaType]);
     }

    /**
     * Instantiate an object of this type. You do
     * NOT use a constructor. This "factory" method creates various
     * concrete implementations. The argument is as 2-char implementation suffix.
     * @See: JasiObject
     */
     public static final Comment create(String suffix) {
  return (Comment) JasiObject.newInstance("org.trinet.jasi.Comment", suffix);
     }
// ////////////////////////////////////////////////////////////////////////////

    /** Commit any additions, changes or deletions to the data source. */
    abstract public boolean commit();

     /** Set the comment text. Leading and trailing blanks will be trimmed.
     * If the text string exceeds the value returned by
     * getMaxLength it will be truncated to that size. */
     public void setText(String comment) {

       String str = comment.trim();
       if (str.length() > getMaxLength()) str = str.substring( 0, getMaxLength() ) ;
       text.setValue(str);
     }
     /** Get the comment text. */
     public String getText() {
            return text.toString();
     }
     /** Delete the comment. */
     public void delete () {
       text = new DataString();
     }

     /** Return true if the comment is null. */
     public boolean isNull() {
       return text.isNull();
     }

     /** Return the maximum allowed comment length.
     * Implement in concrete class to return value for specific data source. */
     abstract public int getMaxLength() ;

}
