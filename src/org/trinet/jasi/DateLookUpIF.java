package org.trinet.jasi;

import java.util.*;

public interface DateLookUpIF {
    public java.util.Date getLookUpDate();
}
