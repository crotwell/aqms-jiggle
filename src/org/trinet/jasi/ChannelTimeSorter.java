package org.trinet.jasi;

import java.util.*;

/**
 * Provides comparator() method to sort a list of Channelable objects by time
 * then net, name, component. The distance from the sort point must already be set
 * for the compared objects with channelable.setDistance(double). <p>
 * @DEPRECATED
 */

public class ChannelTimeSorter implements Comparator {

    /** Comparator for component sort. */
    ChannelNameSorter componentSorter = new ChannelNameSorter();

    public int compare(Object o1, Object o2) {

      if ((o1 instanceof Channelable) && (o2 instanceof Channelable)) {

          Channel ch1 = ((Channelable) o1).getChannelObj();
          Channel ch2 = ((Channelable) o2).getChannelObj();

          double diff = ch1.getDistance() - ch2.getDistance(); // ok to use slant for sort

          if (Math.abs(diff) < .00001) diff = 0.; // added this test to handle weird roundoff? -aww 2010/10/20

          if (diff < 0.0) {
            return -1;
          } else if (diff > 0.0) {
            return  1;
          } else { // same dist sort by component type
            return componentSorter.compare(ch1.getChannelObj().getSeedchan(), ch2.getChannelObj().getSeedchan());
          }
      }
      return 0;  // wrong object types, void class cast exception
    }
}

