package org.trinet.jasi;
import org.trinet.jasi.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.jdbc.table.*;
import org.trinet.util.*;

/**
* Abstract type for a generic amplitude calibration for a station channel.
*/
public abstract class MlAmpMagCalibration extends AbstractAmpMagCalibration {

    protected MlAmpMagCalibration() {
        this(null, null);
    }

    protected MlAmpMagCalibration(ChannelIdIF id) {
        this(id, null);
    }
    protected MlAmpMagCalibration(ChannelIdIF id, DateRange dateRange) {
        super(id, dateRange);
        corrType.setValue(CorrTypeIdIF.ML);
    }
    protected MlAmpMagCalibration(ChannelIdIF id, DateRange dateRange, double value) {
        this(id, dateRange, value, null, null);
    }
    protected MlAmpMagCalibration(ChannelIdIF id, DateRange dateRange, double value,
                    String corrFlag) {
        this(id, dateRange, value, corrFlag, null);
    }
    protected MlAmpMagCalibration(ChannelIdIF id, DateRange dateRange, double value,
                    String corrFlag, String authority) {
        super(id, dateRange, value, CorrTypeIdIF.ML, corrFlag, authority);
    }

    //override super methods if necessary e.g.:
    //public double getDefaultClipAmp() ;

    public static MlAmpMagCalibration create() {
        return create(JasiObject.DEFAULT);
    }

    public static final MlAmpMagCalibration create(int schemaType) {
        return create(JasiFactory.suffix[schemaType]);
    }
    public static final MlAmpMagCalibration create(String suffix) {
        return (MlAmpMagCalibration) JasiObject.newInstance("org.trinet.jasi.MlStaCorrAmpMagCalibrView", suffix);
    }

    public AbstractChannelCalibration createDefaultCalibration(ChannelIdIF chan) {
        MlAmpMagCalibration calibr = MlAmpMagCalibration.create();
        calibr.channelId = (ChannelIdIF) chan.clone(); // try test for cloneable?
        calibr.corr.setValue(getDefaultCorrection()); // ? aww what should it be NaN or setNull
        calibr.clipAmp.setValue(getDefaultClipAmp());
        calibr.gainCorr.setValue(getDefaultGainCorr());
        return calibr;
    }
}
