// OVERLOADING VS. OVERIDDEN METHODS !
// CHECK EFFECT OF "IF"-TYPE BROADENING CASTS CASTS IN CLASS HIERARCHY,
// IS CORRECT OVERLOADED METHOD INVOKED IN SUBCLASSES?
// COMPILE TIME ARG-TYPE METHOD SIGNATURE USED, NOT RUNTIME ARG TYPE.
//
// TODO: implement copy/clear mag,sol data IF logic in super,sub assoc classes - aww
package org.trinet.jasi;
import java.util.*;
import org.trinet.jdbc.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.util.*;
import org.trinet.util.gazetteer.*;
/**
 * Abstract class that represents some sort of per channel reading like a phase
 * pick, amplitude or coda measurement. This class encapsulates the behaviors
 * that these things have in common.
 *
 * @See: Arrival
 * @See: Amplitude
 */

public abstract class JasiReading extends SolutionAssocJasiObject implements JasiReadingIF {

    protected boolean reject = false;

    /** Master list of Channel elements containing reference data retrieved from the data source.
     * Used to set data coordinates etc. of instance channel attribute.
     * */
    // do we want keep this list attribute or remove it? aww
    // we alos have chanList in JasiReadingList and MasterChannelList etc.
    protected ChannelList channelList; // = MasterChannelList.get();

    /** Channel information including name, location, reponse info and distance */
    protected Channel chan = Channel.create();

    /** Date and time of the reading. True epoch with leap seconds included. */
    public DataDouble  datetime = new DataDouble();

    //NOTE:
    // To have generic "quality" attribute declared at this abstract class level
    // requires either removing said attribute from the concrete subclasses
    // of Amplitude,Coda, and splitting it out of Phase's PhaseDescription,
    // or instead making a "PhaseDescription" the generic attribute
    // at this abstract level (a characteric of Amplitude,Coda, and Phase)
    // so we could then use the "quality" attribute of the description
    // class instance member to store the quality (weight?) of the reading. -aww


    /** Difference (error) between actual and expected reading value.
        Residual = observed - expected. If the significant value is a
        time, positive values are too late and negative values are too
        early.*/
    public DataDouble  residual = new DataDouble();

    /** Free format text comment. */
    public DataString comment = new DataString();

    //Distance of this reading from the associated solution in km. Used for distance sorting, etc.
    //public DataDouble distance = new DataDouble();
    //Azimuth from the reading to the associated Solution. Degrees clockwise from north.
    //public DataDouble azimuth = new DataDouble();

    public Object clone() {
        JasiReading jr = (JasiReading) super.clone();
        jr.channelList = this.channelList; // leave the same?
        jr.chan        = (Channel)    this.chan.clone();
        jr.datetime    = (DataDouble) this.datetime.clone();
        jr.residual    = (DataDouble) this.residual.clone();
        jr.comment     = (DataString) this.comment.clone();
        return jr;
    }

    // Default null argument constructor
    public JasiReading() {}

    public boolean isReject() {
        return reject;
    }
    public void setReject(boolean tf) {
        reject = tf;
        //if (reject) setInWgt(0.); // force toggle 11/16/2006 aww
        setInWgt((reject) ? 0. : 1.); // synch with boolean, give full weight or none -aww 2009/09/12 
    }

    /** Sets processing state flag 'F' */
    public void require() {
        setReject(false);
        setProcessingState(JasiProcessingConstants.STATE_FINAL_TAG);
    }

    /** Sets processing state flag 'H' */
    public void unrequire() {
        setProcessingState(JasiProcessingConstants.STATE_HUMAN_TAG);
    }

    abstract public double getInWgt();
    abstract public void setInWgt(double v);

    /**
     * Return true if the reading contributes to the summary result.
     * For example, A Phase to its associated Solution's location or
     * a Coda or Amplitude to its associated Magnitude's value.
     */
    public boolean contributes() { return getWeightUsed() > 0.; } // formerly wasUsed()  - aww

    abstract public boolean existsInDataSource();

    /** Writes data back to the data source. */
    abstract public boolean commit(); // note no exception thrown - aww

    /**
    * Extract all readings associated with Solution from the DataSource
    */
    abstract public Collection getBySolution(Solution sol); // assoc == true
    abstract public Collection getBySolution(Solution sol, String[] readingTypeList); // assoc == true
    abstract public Collection getBySolution(Solution sol, String[] readingTypeList, boolean assoc);

    /**
    * Extract all readings associated with this Solution ID from the
    * DataSource */
    // Note Magnitude is implemented with generic Object argument not the 'long id',
    // which way should it be Object or long ? The long id's from the db are
    // seem to be propagating upward into the generic abstractions of the heirarchy -aww
    abstract public Collection getBySolution(long id); // should this be in inteface?
    abstract public Collection getBySolution(long id, String[] readingTypeList); // should this be in inteface?

    /**
    * Returns array of readings within this time window. Times are seconds in
    * true epoch time (include leap seconds).
    * Returns null if no event or readings are found.  */
    abstract public Collection getByTime(double start, double stop);

    abstract public boolean copySolutionDependentData(JasiReadingIF jr);

    public void initSolDependentData() {
      getChannelObj().setDistanceBearingNull();
    }

    public abstract boolean hasChanged();

    // Next methods are convenience to avoid having to propagate into subtypes.
    // Perhaps better to define a new subtype of JasiReading = LongIdJasiReading
    // of subtype of specific subtypes like Phase - LongIdPhase
    // to implement these for these methods and have the other subtypes inherit
    // from the class which implements LongIdentifer interface:
    // package org.trinet.jasi;
    // public interface LongIdentifier {
    //   public JasiCommitableIF getById(long)
    // Returns true if reading identifier is equivalent to input value.
    //   public boolean idEquals(long)
    // }
    abstract public JasiCommitableIF getById(Object id);

    abstract public boolean isSameType(JasiReadingIF jr);

    // CONCRETE METHODS ///////////////////////////////////////////////////////


    public boolean isSameType(JasiCommitableIF jc) {
      return isSameType((JasiReadingIF) jc);
    }
    /** Returns true if the channel of this reading and
     *  that of the input are equivalent.
     * @see #isSameChanType(JasiReadingIF)
     * @see #isLikeChanType(JasiReadingIF)
     * @see #isSameChanTypeAndTime(JasiReadingIF)
     * @see #isLikeChanTypeAndTime(JasiReadingIF)
     * @see #equivalent(JasiReadingIF)
     * */
    public boolean hasSameChannel(JasiReadingIF jr) {
        return  (jr == this) ? true :
           ( getChannelObj().equalsNameIgnoreCase(jr.getChannelObj()) ); // only check for same name
    }
    /** Returns true only if channel and type (e.g. Amp "WAS", Phase "P", Coda "d") of
     * this reading and those of the input are equivalent.
     * No check for associated solution equivalence.
     * @see #isSameChanType(JasiReadingIF)
     * */
    public boolean isLikeChanType(JasiReadingIF jr) {
        //System.out.println("DEBUG: hasSameChannel:"+hasSameChannel(jr)+" isSameType:"+isSameType(jr));
        return  (jr == this) ? true :
          ( hasSameChannel(jr) && isSameType(jr));
    }
    /** Returns true only if Net, Station and type (e.g. Amp "WAS", Phase "P", Coda "d") of
     * this reading and those of the input are equivalent.
     * No check for associated solution equivalence.
     * @see #isSameChanType(JasiReadingIF)
     * */
    public boolean isLikeStaType(JasiReadingIF jr) {
        return  (jr == this) ? true :
          ( sameStationAs(jr) && isSameType(jr));
    }
    /** Returns true only if the time of this reading
     * and that of the input are exactly equivalent.
     * @see #isLikeTime(JasiReadingIF)
     * */
    public boolean isSameTime(JasiReadingIF jr) {
      return ( jr.getTime() == getTime() );
    }
    /** Returns true only if the time of this reading
     * and that of the input differ by less than
     * LIKE_TIME_RESOLUTION seconds.
     * @see #isSameTime(JasiReadingIF)
     * */
    public boolean isLikeTime(JasiReadingIF jr) {
      return ( Math.abs(jr.getTime() - getTime()) < LIKE_TIME_RESOLUTION );
    }
    /** Returns true only if the channel, type, and time
     * of this reading and those of the input are equivalent.
     * No check for associated solution equivalence.
     * @see #isSameChanTypeAndTime(JasiReadingIF)
     * */
    public boolean isLikeChanTypeAndTime(JasiReadingIF jr) {
        //System.out.println("DEBUG: JasiReading isLikeChanType:"+isLikeChanType(jr)+
        //" sameTime:" +(jr.getTime() == getTime())+ " times(my, its): "+getTime()+" "+jr.getTime());
        return  (jr == this) ? true :
          ( isLikeChanType(jr) && isLikeTime(jr) );
    }
    /** Returns true only if this reading and the input
     * are associated with equivalent Solutions and
     * their channel and type are equivalent.
     * @see #isLikeChanType(JasiReadingIF)
     * */
    public boolean isSameChanType(JasiReadingIF jr) {
      //Match solution (by identity) first.
      return  (jr == this) ? true :
        (assocEquals(jr)) ?  isLikeChanType(jr) : false;
    }
    /** Returns true only if this reading and the input
     * are associated with equivalent Solutions and
     * their Net, Station and type are equivalent.
     * @see #isLikeStaType(JasiReadingIF)
     * */
    public boolean isSameStaType(JasiReadingIF jr) {
      //Match solution (by identity) first.
      return  (jr == this) ? true :
        (assocEquals(jr)) ?  isLikeStaType(jr) : false;
    }
    /** Returns true only if this reading and the input
     * are associated with equivalent Solutions and
     * their channel, type, and times are equivalent.
     * @see #isLikeChanTypeAndTime(JasiReadingIF)
     * */
    public boolean isSameChanTypeAndTime(JasiReadingIF jr) {
      return  (jr == this) ? true :
        (isSameChanType(jr) && isSameTime(jr) );
    }

    public boolean equalsChannelId(Channelable chan) {
      return (chan == null) ? false :
        getChannelObj().equalsChannelId(chan.getChannelObj().getChannelId());
    }

    public boolean sameNetworkAs(Channelable chan) {
      return  (chan == this) ? true :
        getChannelObj().sameNetworkAs(chan);
    }

    public boolean sameStationAs(Channelable chan) {
      return  (chan == this) ? true :
        getChannelObj().sameStationAs(chan);
    }

    public boolean sameSeedChanAs(Channelable chan) {
      return  (chan == this) ? true :
        getChannelObj().sameSeedChanAs(chan);
    }

    public boolean sameTriaxialAs(Channelable chan) {
      return  (chan == this) ? true :
        getChannelObj().sameTriaxialAs(chan);
    }

    // see ChannelIdIF and ChannelIdentifiable interfaces
    public String toDelimitedSeedNameString(String delimiter) {
        return getChannelObj().toDelimitedSeedNameString(delimiter);
    }
    // see ChannelIdIF and ChannelIdentifiable interfaces
    public String toDelimitedNameString(String delimiter) {
        return getChannelObj().toDelimitedNameString(delimiter);
    }

    /** Return header string describing the format of toNeatString(). */
    static public String getNeatStringHeader() {return "";};

    /** A value between 0.0 and 1.0 used to indicate problem or bad readings in
    * a generic way. Gives a "grade" to a reading
    * that is used by components that want to highlight "bad" readings.
    * 0.0 is best, 1.0 is worst.
    */
    public double getWarningLevel() {
      //return Math.min(Math.abs(this.residual.doubleValue()), 1.0);
      return Math.abs(this.residual.doubleValue());
    }

    /** Set the reference ChannelList. Any call to setChannelObjFromList()
     * will examine this list for a matching channel.
     * If one is found, this reading's Channel object will be
    * replaced by a reference to the matching Channel object in the list.
    * If no match is found, the original Channel object is retained.
    * This is done so that the readings get the full channel description,
    * lat/lon/z, response info, corrections, distance from epicenter, etc.
    * @see #setChannelObjFromList()
    */
    public void setChannelList(ChannelList channelList) {
       this.channelList = channelList;
    }

    /**
     * Return the channel object associated with this reading.
     */
    public Channel getChannelObj() { // Channelable interface
        return chan;
    }
    public void setChannelObj(Channel chan) { // Channelable interface
        // throws null pointer exception if input is null
        if (this.chan == null) this.chan = (Channel) chan.clone();
        else this.chan.copy((Channelable)chan); // set instance values to input's
    }

    public void setChannelObjData(Channelable chan) { // Channelable interface ??
        // throws null pointer exception if input is null
        if (this.chan == null) this.chan = (Channel) chan.getChannelObj().clone();
        else this.chan.setChannelObjData(chan.getChannelObj()); // set instance data values to input's
    }


    /** Calculate and set both epicentral (horizontal) distance and true (hypocentral)
     * distance of this channel from the given location in km, the true distance calculation
     * includes the elevation difference between the reading's Channel and input location.
     * Note: this is better than setDistance() or setHorizontalDistance() because
     * it does both and insures they are consistent. */
    public double calcDistance(GeoidalLatLonZ loc) { // aww 06/11/2004 changed input arg type
      return getChannelObj().calcDistance(loc);
    }

    /* Calculates the distance in kilometers from this reading's Channel latitude and longitude
     * at a reference datum of 0 elevation to the hypocenter of the input Solution.
     * Returns NULL_DIST if the input or the coordinates of the Channel or Solution are
     * undefined or null.
     * Returns the calculation only, does not set any internal instance values.
    public double calcSlantDistance(Solution aSol) {
       if (aSol == null || ! aSol.hasLatLonZ() ) return Channel.NULL_DIST;
       LatLonZ llz = (LatLonZ) getChannelObj().getLatLonZ().clone();
       if (llz.isNull())  return Channel.NULL_DIST;
       llz.setZ(0.); // else remove elevation term
       return GeoidalConvert.distanceKmBetween(aSol.getLatLonZ(), llz); // use only solution depth
    }
    */

    /** Calculate and set both epicentral (horizontal) km distance and true (hypocentral)
     * distance of this channel from the location of the associated Solution.
     * Note: this is better than setDistance() or setHorizontalDistance() because
     * it does both and insures they are consistent. */
    public double calcDistance() {
      return (sol == null) ? Channel.NULL_DIST : calcDistance(sol); // aww 06/11/2004
    }

    /** Set hypocentral distance. */
    public void setDistance(double distance) {
      getChannelObj().setDistance(distance);
    }

    /** Set horizontal distance. */
    public void setHorizontalDistance(double distance) {
      getChannelObj().setHorizontalDistance(distance);
    }

    public double getDistance() {
      return getChannelObj().getDistance();
    }
    public double getHorizontalDistance() {
      return getChannelObj().getHorizontalDistance();
    }
    public double getVerticalDistance() {
      return getChannelObj().getVerticalDistance();
    }
    /** Returns the distance in kilometers from a location at this reading's
     * Channel latitude and longitude at a datum of 0 elevation to the location
     * of the hypocenter of the input Solution.  <p>
     * s = sqrt(epicentralDistance**2+solutionDepth**2) <p>
     * Returns NULL_DIST if the horizontal (epicentral) distance is undefined
     * or reading is not associated with a Solution having a valid LatLonZ.
     * @see #calcDistance()
     * @see #calcDistance(GeoidLatLonZ)
     * @see #setHorizontalDistance(double)
     */
    public double getSlantDistance() {

      double retVal = Channel.NULL_DIST;

      if ( sol == null || ! sol.hasLatLonZ() ) return retVal;

      double hDist = getHorizontalDistance();
      if ( hDist == Channel.NULL_DIST || Double.isNaN(hDist) ) return retVal;

      retVal = Math.sqrt(Math.pow(hDist,2)+Math.pow(sol.getLatLonZ().getZ(),2));

      return retVal;
    }

    public void setAzimuth(double az) {
        getChannelObj().setAzimuth(az);
    }
    public double getAzimuth() {
        return getChannelObj().getAzimuth();
    }

    /** Match this reading's channel with one in this list. If a match is found
     * update this readings channel object with attribute values of the Channel
     * returned from reference list.
     * This is used to match objects to a more complete description of a channel then
     * they probably have otherwise. Gives access to lat/lon/z, response info, corrections,
     * distance from epicenter, etc. */
    public boolean setChannelObjFromList(ChannelList channelList, java.util.Date aDate) {
        return getChannelObj().setChannelObjFromList(channelList, aDate);
    }
    public boolean setChannelObjFromMasterList(java.util.Date aDate) {
        return getChannelObj().setChannelObjFromMasterList(aDate);
    }
    public boolean matchChannelWithDataSource(java.util.Date aDate) { // aww
      boolean status = setChannelObjFromList(aDate); // do we want this behavior ?
      if (status) return true;
      return getChannelObj().matchChannelWithDataSource(aDate);
    }

    /** Use object's ChannelList if it has been set, otherwise MasterChannelList if it has been set.
     * Returns true if found in list, else returns false list doesn't exist or Channel is not in list. */
    public boolean setChannelObjFromList() {
        return setChannelObjFromList(getLookUpDate());
    }
    public boolean setChannelObjFromList(ChannelList chanList) {
        return setChannelObjFromList(chanList, getLookUpDate());
    }
    public boolean setChannelObjFromList(java.util.Date aDate) {
        if (channelList == null) setChannelList(MasterChannelList.get()); // aww , do we want to do this here?
        return setChannelObjFromList(channelList, aDate);
    }
    public boolean setChannelObjFromMasterList() {
        return setChannelObjFromMasterList(getLookUpDate());
    }
    public boolean matchChannelWithDataSource() { // aww
        return matchChannelWithDataSource(getLookUpDate());
    }


    public boolean insureLatLonZ(java.util.Date aDate) { // aww
      return getChannelObj().insureLatLonZ(aDate);
    }
    /**
     * Returns true if Channel of this reading has LatLonZ data set.
     * Otherwise lookups data for matching Channel in default ChannelList,
     * that was active at reading time, or if reading time is null,
     * the invocation time.
     * If no such data is found in ChannelList, tries to recover LatLonZ data
     * from the default DataSource.
     * Returns false if Channel location remains unknown.
     */
    public boolean insureLatLonZ() { // aww
      // use the observation time - aww
      return insureLatLonZ(getLookUpDate());
    }

    public java.util.Date getLookUpDate() {
        return getDateTime(); // changed -aww 2008/02/11 
    }

    /**
     * Set time of pick as a double true epoch seconds.
     */
    public void setTime(double dt) {
        datetime.setValue(dt);
    }

    /**
     * Set time of pick from Dateobject.
     */
    public void setTime(java.util.Date aDate) {
        if (aDate instanceof DateTime) {
            datetime.setValue(((DateTime)aDate).getTrueSeconds()); // for UTC - aww 2008/02/13
        }
        else {
            datetime.setValue(LeapSeconds.dateToTrue(aDate)); // for UTC - aww 2008/02/13
        }
    }

    /**
     * Return epoch time of pick as a double. Returns 0.0 if datetime is null.
     */
    public double getTime() {
        if (datetime.isNull()) return 0.0;
        return datetime.doubleValue();
    }

    public boolean hasValidTime() {
        return datetime.isValidNumber();
    }

    /**
     * Return true epoch time (UTC) as a DateTime object.
     * If no instance associated solution, return a epoch 0. nominal time;
     */
    public DateTime getDateTime() { // changed return type to DateTime subclass of java.util.Date
        // WARNING! data before Jan 1, 1970 has negative double secs
        // and this null return makes the data appear valid for Jan 1, 1970.
        double value = getTime();
        if (value != 0.0) {
            return new DateTime(value, true); // for UTC time -aww 2008/02/11
        }
     // If the reading's datetime is null, or 0 seconds, return instance associated solution's epoch (UTC) time.
        /*
        else if (sol != null && sol.hasValidDateTime()) {
            return sol.getDateTime(); // added this logic for case of NULL coda datatime -aww 2009/10/23
        }
        */
        else {
            return new DateTime(0., false);
        }
    }

    public boolean getNeedsCommit() {
        return (needsCommit || hasChanged());
    }
    /**
     * Return a summary string with the most important identifier info
     * for this reading instance.
    */
    public String toAssocIdString() {
        StringBuffer sb = new StringBuffer(128);
        sb.append("Deleted=").append(isDeleted()).append(" ");
        sb.append("Reject=").append(isReject()).append(" ");
        sb.append(getChannelObj().toDelimitedNameString(' ')).append(" ");
        sb.append(getTypeQualifier().toString());
        sb.append(" My id: ").append(StringSQL.valueOf(getIdentifier()));
        sb.append(" evid: ").append(((sol != null) ? sol.id.toStringSQL(): "NULL"));
        return sb.toString();
    }

    /** Override returns true if both identifiers are DataObjects and
     * have non-null equivalent DEFAULT (undefined) values.
     * Readings whose identifier values are UNDEFINED should have
     * their station channel descriptive data tested to establish
     * content equivalence.
     * */
    public boolean idEquals(Object identifier) {
      Object myId = getIdentifier();
      if (myId == null) return false;
      return (myId instanceof DataObject) ?
        ((DataObject) myId).equalsValue(identifier) : myId.equals(identifier);
    }
    /** Tests for data equivalence
     *  Returns true if identifier, channel name, reading type, and time agree.
     *  NOTE: No check for equivalence of associated solutions.
     *  @see #isLikeChanTypeAndTime(Object)
     *  @see #isSameChanTypeAndTime(Object)
     * */
    public boolean equivalent(Object obj) {
      if (! super.equivalent(obj)) return false;
      // DataObject id could be default undefined NULL_VALUE, check for identity with channel and time.
      JasiReadingIF jr = (JasiReadingIF) obj;
      // below code differs from isLikeChanTypeAndTime(jr) requiring an exact time match
      return ( hasSameChannel(jr) && isSameTime(jr) ); // require time exact matching by default or not?
    }

    // IF methods:
    public double getResidual() {
        //return (residual.isNull()) ? 0. : residual.doubleValue(); // used to be 0.
        return (residual.isNull()) ? Double.MAX_VALUE : residual.doubleValue(); // try aww 11/26/2002
    }


    /* Returns authority if set and valid. If not, it returns the first valid
     * value it finds in the following order:<p>
     * - the network code set in the EnvironmentInfo object.<p>
     * @return empty string, value is null or equals an empty or a blank string
     * @see: EnvironmentInfo()
     *
     */
    /*
    public String getClosestAuthorityString() {
        String str = authority.toString();
        if (! NullValueDb.isBlank(str)) return str;
        str = EnvironmentInfo.getNetworkCode();
        if (! NullValueDb.isBlank(str)) return str;
        Channel chan = getChannelObj();
        if (chan != null) {
          str = ((AuthChannelIdIF)chan.getChannelId()).getAuth();
        }
        return (NullValueDb.isBlank(str)) ? "" : str;
    }
    */

    // Defaults to current value if valid, else local network code
    public String getClosestAuthorityString() {
        /*
        // Implementation prefers the event auth -aww added 2011/08/17, removed 2011/09/21
        if (sol != null && !sol.isOriginGType(GTypeMap.TELESEISM)) {
            String eauth = sol.getAuthority(); // origin
            if (NullValueDb.isBlank(eauth)) {
                eauth = sol.getEventAuthority(); // event
                if (NullValueDb.isBlank(eauth)) eauth = EnvironmentInfo.getNetworkCode(); // local network code
            }
            if (!NullValueDb.isBlank(eauth) && !eauth.equals(EnvironmentInfo.DEFAULT_NETCODE)) return eauth;
        }
        */

        String str = authority.toString();
        if (! NullValueDb.isBlank(str) && !str.equals(EnvironmentInfo.DEFAULT_NETCODE)) return str;

        str = EnvironmentInfo.getNetworkCode();
        return (NullValueDb.isBlank(str)) ? EnvironmentInfo.DEFAULT_NETCODE : str;
    }
    //

    //Correction active at reading time (possibly not the current/active correction).
    public DataCorrectionIF getCorrectionAtReadingTime(String corrType) {
        return getCorrection(getLookUpDate(), corrType);
    }
    //Correction active on specified input date (possibly not the current/active correction).
    public DataCorrectionIF getCorrection(java.util.Date date, String corrType) {
        return getChannelObj().getCorrection(date, corrType);
    }

    // CorrectableIF
    /** Return the currently active correction. */
    public DataCorrectionIF getCorrection(String corrType) {
        return getChannelObj().getCorrection(corrType);
    }
    public void setCorrection(DataCorrectionIF dc) {
        getChannelObj().setCorrection(dc);
    }
    public boolean hasCorrection(String corrType) {
        return getChannelObj().hasCorrection(corrType);
    }

    public boolean hasCorrection(java.util.Date date, String corrType) {
        return getChannelObj().hasCorrection(date, corrType);
    }
    public boolean hasCorrectionAtReadingTime(String corrType) {
        return hasCorrection(getDateTime(), corrType);
    }

} // JasiReading
