package org.trinet.jasi;

/**
 * Return the commonly used quality values for an event.<p>

<tt>
     Horizontal<br>
        Error      Depth Error       Quality       Quality     Quality<br>
        (ERH)         (ERZ)          Grade      Description     Value<br>
        <1.0 km       <2.0 km           A         Excellent       1.00<br>
        <5.0 km       <2.5 km           B           Good          0.75<br>
        <5.0 km       >2.5 km           C           Fair          0.50<br>
        >5.0 km         any             D           Poor          0.25<br>

</tt>

ERH is an earthquake solution's calculated horizonal error. ERZ is the depth
error. Both are in kilometers. Thus, an ERH of 2.5 means that there is 95%
confidence that the earthquake's epicenter is within a circle of 2.5 km radius
around the reported epicenter. Likewise, ERZ is an estimate of the vertical or
depth error of the solution. Depth errors are almost always larger then
horizontal errors.
*
 * @author Doug Given
 * @version */

public class EventQuality  {
    
    private EventQuality() {
	
    }

    /** Returns a value between 0.0 and 1.0 scaled to quality of the solution.
    A value of 1.0 is the best and 0.0 the worst. */
    public static double getValue(Solution sol) {

	if (sol.errorHoriz.doubleValue() < 1.0 &&
	    sol.errorVert.doubleValue() < 2.0) return 1.0 ;

	if (sol.errorHoriz.doubleValue() < 5.0 &&
	    sol.errorVert.doubleValue() < 2.5) return 0.75 ;

	if (sol.errorHoriz.doubleValue() < 5.0 &&
	    sol.errorVert.doubleValue() >= 2.5) return 0.50 ;

	return 0.25 ;
    }

    /** Returns a string with a letter representing the quality of the solution.
    From "A" to "D". */
    public static String getLetter(Solution sol) {

	double qual = getValue(sol);
	if (qual == 1.00) return "A" ;
	if (qual == 0.75) return "B" ;
	if (qual == 0.50) return "C" ;
	return "D" ;
    }

    /** Returns a String containing a word letter representing the quality of
        the solution. Possible returns are: "Excellent", "Good", "Fair", and "Poor". */
    public static String getString(Solution sol) {

	double qual = getValue(sol);
	if (qual == 1.00) return "Excellent" ;
	if (qual == 0.75) return "Good" ;
	if (qual == 0.50) return "Fair" ;
	return "Poor" ;
    }

} // EventQuality
