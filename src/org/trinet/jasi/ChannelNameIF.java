package org.trinet.jasi;
public interface ChannelNameIF {
    public ChannelName getChannelName();
    public void setChannelName(ChannelName name);
    public void setChannelName(String net, String sta, String seedchan, String location);
    public void setChannelName(String net, String sta, String seedchan, String location,
                    String auth, String src, String chan, String chanSrc);
}
