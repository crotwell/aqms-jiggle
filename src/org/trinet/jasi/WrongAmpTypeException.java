package org.trinet.jasi;

/**
 * Exception class that gets thrown when a method is passed an inapproriate
 * Amplitude object for its processing. For example: 
 * passing an acceleration amp to an ML magnitude calculation class.
 *
 * Created: Tue Jun  6 11:07:09 2000
 *
 * @author Doug Given
 * @version */

public class WrongAmpTypeException extends WrongDataTypeException {
    public WrongAmpTypeException(String msg) {
	super(msg);
    }    
} // WrongAmpTypeException
