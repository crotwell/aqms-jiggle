package org.trinet.jasi;
import java.util.Date;
import org.trinet.jdbc.table.*;
import org.trinet.util.gazetteer.*;
/** Defines methods for classes that have a Channel. */
public interface Channelable extends HorizontalDistanceBearingIF {
// could make Channelable an extend ChannelIdentifiable interface ?
    public void setChannelObj(Channel chan);
    public Channel getChannelObj();
    public void setChannelObjData(Channelable chan);
    public double  calcDistance(GeoidalLatLonZ llz); // aww 06/11/2004
    public boolean equalsChannelId(Channelable chan);
    public boolean sameNetworkAs(Channelable chan);
    public boolean sameStationAs(Channelable chan);
    public boolean sameSeedChanAs(Channelable chan);
    public boolean sameTriaxialAs(Channelable chan);
    //Other Possible methods to implement below:
    //public boolean setChannelObjFromMasterList(java.util.Date aDate);
    public boolean setChannelObjFromList(ChannelList chanList, java.util.Date aDate);
//    public boolean setChannelObjFromList(ChannelableList chanList, java.util.Date aDate);
    public boolean matchChannelWithDataSource(java.util.Date aDate);
    //delegate to the LatLonZ attribute of Channel:
    //public void    setLatLonZ(LatLonZ latlonz);
    //public LatLonZ getLatLonZ();
    String toDelimitedNameString(String delimiter);
    String toDelimitedSeedNameString(String delimiter);
}
