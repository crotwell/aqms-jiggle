package org.trinet.jasi.magmethods;

import java.util.*;
import org.trinet.jasi.*;

public class MagMethodPropertyList extends SolutionWfEditorPropertyList {
/**
<pre>
--- AbstractMagnitudeMethod properties ---
"scanEnergyWindow"
"maxDistance"
"minDistance"
"maxChannels"
"trimResidual"
"requireCorrection"
"includedReadingTypes"
"minValidReadings"
"globalMagCorr"
"acceptChan"
"summaryChan"
"filterChan"
"filterCopiesWf"
"useAssignedWts"
"channelDbLookUp"
"sumMagStatTrim"
"chauvenetTrimValue"

--- AmpMagnitudeMethod ---
"minSNR"

--- CodaMagnitudeMethod ---
"codaStartOffsetSecs"
"codaPrePTimeBiasOffsetSecs"
"logCodaCalc"
"waveformLoadWaitMillis"
"maxLoadingWaits"
"waveformCacheSize"
"filter"
"resetOnClipping"
"minGoodWindowsToTerminateCoda"
"maxGoodWindowsToTerminateCoda"
"minSNRatioForCodaStart"
"minSNRatioCodaCutoff"
"passThruNSRatio"
"velocityModelClassName"
"scanAllWaveformsForMag"

--- HypoinvMdMagMethod ---
"fmagLogA0RelationType"
"fmagDurParms"
"fmagUseGainCorr"
"fmagDistZParms"
"fmagChannelDefaultCorr"
"fmagGainCorrChan"

--- MdMagMethod, McMagMethod ---
"disableMagnitudeDistanceCutoff"
"cutoffMagValues"
"cutoffMagDists"
"codaGeneratorParmsType"
"codaGeneratorType"

--- SoCalMlMagMethod ---
"disableMagnitudeDistanceCutoff"
</pre>
*/

    public MagMethodPropertyList() {}

    public MagMethodPropertyList(MagMethodPropertyList props) {
        super(props);
    }
    public MagMethodPropertyList(String propFileName) {
        super(propFileName);
    }

    private boolean privateSetup() { 
      boolean status = true;
      return status;
    }

    /** Return a String of space-delimited property names used
     * by application classes utilizing this class.
     * */
    public List getKnownPropertyNames() {
        List aList = super.getKnownPropertyNames();
        List myList = classDeclaredPropertyNames();
        if (aList instanceof ArrayList)
            ((ArrayList)aList).ensureCapacity(aList.size() + myList.size());
        aList.addAll(myList);
        Collections.sort(aList);
        return aList;
    }

    public static List classDeclaredPropertyNames() {

        ArrayList myList = new ArrayList(64);

        // AbstractMagnitudeMethod properties:
        myList.add("scanEnergyWindow");
        myList.add("maxDistance");
        myList.add("minDistance");
        myList.add("maxChannels");
        myList.add("trimResidual");
        myList.add("requireCorrection");
        myList.add("includedReadingTypes");
        myList.add("minValidReadings");
        myList.add("globalMagCorr");
        myList.add("acceptChan");
        myList.add("summaryChan");
        myList.add("filterChan");
        myList.add("filterCopiesWf");
        myList.add("useAssignedWts");
        myList.add("channelDbLookUp");
        myList.add("sumMagStatTrim");
        myList.add("chauvenetTrimValue");
        
        //AmpMagnitudeMethod properties:
        myList.add("minSNR");
        
        //MdMagMethod, McMagMethod SoCalMlMagMethod:
        myList.add("disableMagnitudeDistanceCutoff");

        //CodaMagnitudeMethod properties:
        myList.add("codaStartOffsetSecs");
        myList.add("codaPrePTimeBiasOffsetSecs");
        myList.add("logCodaCalc");
        myList.add("waveformLoadWaitMillis");
        myList.add("maxLoadingWaits");
        myList.add("waveformCacheSize");
        myList.add("filter");
        myList.add("resetOnClipping");
        myList.add("minGoodWindowsToTerminateCoda");
        myList.add("maxGoodWindowsToTerminateCoda");
        myList.add("minSNRatioForCodaStart");
        myList.add("minSNRatioCodaCutoff");
        myList.add("passThruNSRatio");
        myList.add("velocityModelClassName");
        myList.add("scanAllWaveformsForMag");
        
        //MdMagMethod, McMagMethodSoCalMlMagMethod
        myList.add("codaGeneratorParmsType");
        myList.add("codaGeneratorType");

        //HypoinvMdMagMethod properties:
        myList.add("fmagLogA0RelationType");
        myList.add("fmagDurParms");
        myList.add("fmagUseGainCorr");
        myList.add("fmagDistZParms");
        myList.add("fmagChannelDefaultCorr");
        myList.add("fmagGainCorrChan");

        Collections.sort(myList);

        return myList;

    }

}
