package org.trinet.jasi.magmethods;
import java.util.*;
//import org.trinet.filters.*;
import org.trinet.jasi.*;
import org.trinet.jasi.coda.*;
import org.trinet.jdbc.*;
import org.trinet.util.*;
import org.trinet.jasi.magmethods.TN.LeeMdMagMethod;

public abstract class MdMagMethod extends CodaMagnitudeMethod {

    /* coda sampling windows defined in the ew c-routines:
    protected static final int mdWindowCount[] = {
      1,   2,   3,   4,   5,   6,   7,   8,  10,  12,
     16,  20,  24,  28,  32,  36,  40,  44,  48,  56,
     64,  70,  73
    };
    protected static final int [] mdWindowCenterTimeSeconds[] = {
      1,   3,   5,   7,   9,  11,  13,  15 , 19,  23,
     31,  39,  47,  55,  63,  71,  79,  87,  95, 111,
    127, 139, 144
    };
    */

    protected double[] cutoffMagValues = new double [] { 1.4,  2.0,  2.5, 3.0};
    protected double[] cutoffMagDists = new double [] { 60., 120., 240., 320.};

    protected static String  defaultGainCorrChanPropString = "";

    public MdMagMethod() {
      super();
      magSubScript = MagTypeIdIF.MD;
      methodName = "Md";
      setMinDistance(20.0);
      setMaxDistance(600.0);
      defaultCalibrationFactory = MdCodaMagCalibration.create();
    }

    public static MagnitudeMethodIF create() {
        /*
        StringBuffer sb = new StringBuffer(132);
        String suffix = JasiObject.getDefaultSuffix();
        sb.append(AbstractMagnitudeMethod.DEFAULT_BASE_PACKAGE_NAME);
        sb.append(".").append(suffix).append("MdMagMethod").append(suffix);
        return AbstractMagnitudeMethod.create(sb.toString());
        */
        if (JasiObject.getDefault() == JasiFactory.TRINET)
           return new LeeMdMagMethod();
        else return null;
    }

    public double getDistanceCutoff(double magValue) {
      double value =  Math.max( getMinDistance(), getMaxDistance() );
      if ( ! props.getBoolean("disableMagnitudeDistanceCutoff") ) {
        for (int idx = 0; idx < cutoffMagValues.length; idx++) {
            if (magValue < cutoffMagValues[idx]) {
                value = cutoffMagDists[idx];
                break;
            }
        }
      }
      return value;
    }

    protected MagnitudeAssocJasiReadingIF createDefaultReadingType() {
        Coda coda = Coda.create();
        coda.setProcessingState(JasiProcessingConstants.STATE_AUTO_TAG); // algorithm calculated, so force AUTO 
        coda.setDurationType(CodaDurationType.D);
        return coda;
    }

    public boolean isIncludedMagType(Magnitude mag) {
      return mag.isCodaMag() && mag.getTypeQualifier().toString().equals(MagTypeIdIF.MD); // "Mc" vs. "Md" type
    }

    public boolean isIncludedReadingType(MagnitudeAssocJasiReadingIF jr) {
      if ( ! (jr instanceof Coda) ) return false;
      Coda coda = (Coda) jr;
      //System.out.println("DEBUG CodaMagnitudeMethod coda type qual: \""+coda.getTypeQualifier().toString()+"\"");
      // use the type list if it's defined else default to type Pd (codaType=P+durationType=d) tau duration
      return ( super.isIncludedReadingType(jr) || coda.getTypeQualifier().toString().equals("Pd"));
    }

    /* evaluate to decide whether or not to save station channel coda
    public boolean isValidCoda(org.trinet.jasi.Coda coda) {
      return super.isValidCoda(coda) ; // &&
    }
    */

    protected CodaGeneratorParms createCodaGeneratorParms(GenericPropertyList props) {

      String parmsType = props.getProperty("codaGeneratorParmsType");
      if (parmsType == null) parmsType = "NULL";

      if (parmsType.equalsIgnoreCase("NC")) {
        return CodaGeneratorParms.NC_MD;
      }
      else if (parmsType.equalsIgnoreCase("CI")) {
        return CodaGeneratorParms.CI_MD;
      }
      else {
        System.err.println(methodName +
          " Error null or unknown type, property codaGeneratorParmsType: " + parmsType
          + " in property filename: " + props.getFilename()
        );
        return null;
      }
    }

    protected CodaGeneratorIF createCodaGenerator(GenericPropertyList props) {
      String generatorType = props.getProperty("codaGeneratorType");
      if (generatorType == null) generatorType = "NULL";

      if (generatorType.equalsIgnoreCase("CI")) {
        return new SoCalMdCodaGenerator();
      }
      else if (generatorType.equalsIgnoreCase("NC")) {
        return new NoCalMdCodaGenerator();
      }
      else {
        System.err.println(methodName +
          " Error null or unknown type, property codaGeneratorType: " + generatorType);
        return null;
      }
    }

    public double calcChannelMagValue(MagnitudeAssocJasiReadingIF jr) throws WrongDataTypeException {
        if (! isIncludedReadingType(jr)) { // should we throw WrongDataTypeException?
          throw new WrongDataTypeException("input wrong subtype for MagnitudeMethod "+
                        methodName);
        }
        Coda coda = (Coda) jr;
        double tau = getCodaTau(coda);
        if ( tau <= 0. || Double.isNaN(tau) ) return -9.; // changed from -1 -aww

        Double magCorr = getMagCorr(jr);
        /* requireCorrection or NOT?
        if (magCorr == null) {
          if (requireCorrection) {
            //if (debug) reportMessage("input reading has no mag correction", jr);
            throw new WrongCodaTypeException(methodName+ " calcChannelMagValue input !hasMagCorr: " +
                            getStnChlNameString(jr));
          }
        }
        */
        //double zc = coda.getVerticalDistance(); // this value differs per channel elevation
        // Constants for coda algorithm use Solution depth, no elevation delta
        // value constant for all assoc coda 
        double z = coda.getAssociatedSolution().getModelDepth(); // switched from getDepth -aww 2015/10/09

        return calcChannelMagValue(
                        tau,
                        coda.getHorizontalDistance(),
                        z,
                        ((magCorr == null) ? 0. : magCorr.doubleValue())
               );
    }

    // Average taus at all channels at site, then derive single site magnitude.
    // ? Do you average the corrections ? Only works if all channels have same correction?
    public double calcStaMagFromReadings(List readings, String avgStaMagType, boolean resetChanMag2StaAvg, double[] wt) {
        return Double.NaN;
    }

    abstract protected double calcChannelMagValue(double tau, double distKm, double vertKm, double chlMagCorr);
    //mdMagValue = FMA +FMB*log10(tau) +FMF*tau +FMD*HorizDist +FMZ*depth +STACOR +FMGN*gainFact
    //where fma = ; fmb = ; fmf = ; fmd = ; fmz = ; fmg = ; fms = ; // method specific values

    public GenericPropertyList getProperties() {
      GenericPropertyList props = super.getProperties();
      props.setProperty("cutoffMagValues", cutoffMagValues);
      props.setProperty("cutoffMagDists", cutoffMagDists);
      return props;
    }

    public void initializeMethod() {
        super.initializeMethod();
        if (props == null) return; // may not have props set

        if (props.getProperty("cutoffMagValues") != null) {
          cutoffMagValues = props.getDoubleArray("cutoffMagValues");
        }

        if (props.getProperty("cutoffMagDists") != null) {
          cutoffMagDists = props.getDoubleArray("cutoffMagDists");
        }
        if (cutoffMagValues.length != cutoffMagDists.length) {
            throw new PropertyFormatException("cutoffMagDists length != cutoffMagValues length, check property declarations");
        }

        stateIsValid = true;
    }

    /**
     * Sets or reinitializes the default setting for this method.
     */
    public void setDefaultProperties() {
      defaultAcceptChanPropString = "EHZ HHZ";
      defaultFilterChanPropString = "HHZ";
      defaultSummaryChanPropString = "EHZ";
      cutoffMagValues = new double [] { 1.4,  2.0,  2.5, 3.0};
      cutoffMagDists = new double [] { 60., 120., 240., 320.};

      super.setDefaultProperties();
    }


}
