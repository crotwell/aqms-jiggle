package org.trinet.jasi.magmethods.TN;
import org.trinet.jasi.magmethods.*;

public class McMagMethodTN extends McMagMethod {
    public McMagMethodTN() {
        super();
    }
    // punt here for now
    public void configure(int src, String location, String section) {
        setProperties(location); // location like a filename url
    }

    public int getSummaryMagValueStatType() { return MEDIAN_MAG_VALUE; }

}
