package org.trinet.jasi.magmethods.TN;

import java.util.*;
import org.trinet.filters.*;
import org.trinet.jasi.*;
import org.trinet.jasi.magmethods.*;
import org.trinet.util.MathTN;
import org.trinet.util.GenericPropertyList;

/**
 * Static methods for ML (local magnitude) calculation.<p>
 *                        ML = log(A) + A0(r) + Cs<p>
 * Where:<br>
 *   A = peak half amp, in mm for an actual, or synthetic, Wood-Anderson (types WAS, WAC, and WAU)<br>
 *   A0(r) = distance correction<br>
 *   Cs = station correction<p>
 * A0(r) overrides the standard Richter ML distance correction. <br>
 * This A0 function, based on an emperical relationship derived by Hiroo Kanamori 
 * for southern California data, is approximated by the formula:
 * -log10(c*(r**-n)*exp(-kr)), where r, is hypocentral distance.
 * Epicentral distance is only used for the station distance cutoff range values. <p>
 *
 * @see AmpType()
 * @see ML()
 */
public class HirooMlMagMethod extends MlMagMethod {

    public static final double DEFAULT_K = -0.00505; // k
    public static final double DEFAULT_C =  0.3173;  // c, -log(c) is the distance independent term in log10 equation
    public static final double DEFAULT_N = -1.14;    // n

    public static final double DEFAULT_CUTOFF_SLOPE = 170.0; 
    public static final double DEFAULT_CUTOFF_INTERCEPT = -205.0; 

    protected static double kA0 = DEFAULT_K;
    protected static double cA0 = DEFAULT_C;
    protected static double nA0 = DEFAULT_N;

    protected static double cutoffDistSlope = DEFAULT_CUTOFF_SLOPE;
    protected static double cutoffDistIntercept = DEFAULT_CUTOFF_INTERCEPT; 

    public HirooMlMagMethod() {
        // Magnitude type subscript. For example: for "Ml" this would equal "l".
        magSubScript = MagTypeIdIF.ML;

        // String describing the magnitude method
        methodName = "HirooMl";

        // set the waveform filter to Synthetic Wood-Anderson
        setWaveformFilter( new WAFilter() );
    }

    // punt here for now
    public void configure(int src, String location, String section) {
        setProperties(location); // location like a filename url
    }

    /**
     * Override of method in default Richter ML method differs only in that it
     * retrieves the HYPOCENTRAL distance not epicentral distance 
     * from the input Amplitude.
     */ 
    public double calcChannelMagValue(Amplitude amp) throws WrongDataTypeException {
      if (! isIncludedReadingType(amp) ) {
          throw new WrongAmpTypeException(
            "Wrong Amp type for MagnitudeMethod " +methodName+
            " : " +amp.toNeatString() // +AmpType.getString(amp.type)
          );
      }
      if (amp.isClipped()) { // should we throw WrongDataTypeException?
          throw new WrongAmpTypeException(
            "Amp isClipped for MagnitudeMethod "+methodName+
            " : " +amp.toNeatString()
          );
      }
      double value = -9.; // changed from 0. to -9. to make it obvious to user -aww 08/12/2004
      try {
          Double corr = getMagCorr(amp);
          // use slant distance (dist to 0 stn elev)
          value = calcChannelMagValue(amp.getSlantDistance(),
                                      getAmpValue(amp),
                                      ((corr == null) ? 0. : corr.doubleValue())
                  );
      }
      catch (WrongDataTypeException ex) {
          ex.printStackTrace();
      }

      return value;
    }

    /** Override the distance correction of the Richter ML. <br>
     *  Based on an empirical relationship for southern California
     *  derived by Hiroo Kanamori.
     *  Input is the hypocenter "slant" distance km (not station epicentral distance). <p>
     *  Correction = -log10( 0.3173 * exp(-0.00505 * dist) * pow(dist, -1.14))
    */
    public double distanceCorrection(double distance) {
        // AWW input slant distance should be measured to a datum of zero solution depth 
        // that is, unlike 'true' distance station the station elevation is not included.
        // Channel Ml = log10(A/A0) + staCorr
        // where A0 is amp of the equivalent "zero" Ml magnitude and
        // log10(A0) =  a0 -n*log10(dist) -(k*log10(e)*dist) where (a0 is scalar = -log(c))
        // by Richter convention the value of log10(A0) should equal "-3" at 100 km distance
        // function -log10(c*(r**-n)*exp(-kr)), approximates A0 
        // from BSSA Vol.83,No.2,pp.330-346  c=0.4971, n=1.2178, k=0.00530
        // here in Jiggle Java code:         c=0.3173, n=1.1400, k=0.00505
        //double kA0 = -0.00505; // k
        //double cA0 =  0.3173;  // c
        //double nA0 = -1.14;    // n

        //n= 1.1476 k= 0.0056428 c= .34695 log(c) = -.45973
        // 2007 Hiroo type inversion of all HH and HL channels with PAS HHN fixed at 0.20, for selected events ML >= 3 from 2000-2006
        //double kA0 = -0.0056428; // k
        //double cA0 =  .34695;    // c
        //double nA0 = -1.1476;    // n

        // 2007 Hiroo type inversion of all HH and HL channels with PAS HHN fixed at 0.10, for selected events ML >= 3 from 2000-2006
        //double kA0 = -0.005655; // k
        //double cA0 =  .34514;    // c
        //double nA0 = -1.1462;    // n

        // 2007 Hiroo type inversion of all HH only with PAS HHN fixed at 0.10, selected for events ML >= 3 from 2000-2006
        //double kA0 = -.005847; // k
        //double cA0 =  .3899;   // c
        //double nA0 = -1.1685;  // n

        return - ( MathTN.log10(cA0*Math.pow(distance,nA0)*Math.exp(kA0*distance)) );

    }

    /**
     * Return the distance cutoff appropriate for the method. Amplitudes from
     * stations father then this will not be included in the summary magnitude.
     * Because it depends on the magnitude the cutoff can only be applied in a
     * second pass through the amplitude readings. In the
     * southern California magnitude scheme the distance cutoff is an empirical
     * relationship derived by Kanamori. Default is given by:<p>
     *<tt>
     * cutoff = 170.0 * magnitude - 205.0 (all distances are in km)
     *</tt>
     * The formula can yields 0.0 at a magnitude of 1.21, so a minimum cutoff is
     * enforced. This method will never yield a value less then 30.0km. <p>
     * The cutoff distance is compared with station's "epicentral" distance. 
     * The difference between epicentral and slant distances is typically &LT; 2 km.
     * For example, using epiDistance=minCutoff (30. km) and a solution 
     * depth of 10 km, the slantDistance is 31.6 km).
     */
    // mag  cut
    // 1.0  -35
    // 2.0  135
    // 3.0  305
    // 4.0  475
    public double getDistanceCutoff(double magnitude) {
        //property to disable and return the default MaxDistance
        if ( props.getBoolean("disableMagnitudeDistanceCutoff") ) 
           return super.getDistanceCutoff(magnitude);

        // Distance is epicentral, as compared with "slant"
        // cutoffKm = slope * ML + intercept
        double dist = Math.max(getMinDistance(),cutoffDistSlope*magnitude + cutoffDistIntercept);

        return Math.min(dist, getMaxDistance());
    }

    public void initializeMethod() {
       super.initializeMethod();

       if (props == null) return; // custom properties
       if (props.isSpecified("hiroo.A0.k")) kA0 = props.getDouble("hiroo.A0.k");
       if (props.isSpecified("hiroo.A0.c")) cA0 = props.getDouble("hiroo.A0.c");
       if (props.isSpecified("hiroo.A0.n")) nA0 = props.getDouble("hiroo.A0.n");

       if (props.isSpecified("cutoffDistSlope")) { // cutoffKm = slope * ML - intercept
           cutoffDistSlope = props.getDouble("cutoffDistSlope"); 
       }
       if (props.isSpecified("cutoffDistIntercept")) {
           cutoffDistIntercept = props.getDouble("cutoffDistIntercept"); 
       }
    }

    public GenericPropertyList getProperties() {
        GenericPropertyList props = super.getProperties();

        props.setProperty("hiroo.A0.k", kA0);
        props.setProperty("hiroo.A0.c", cA0);
        props.setProperty("hiroo.A0.n", nA0);

        props.setProperty("cutoffDistSlope", cutoffDistSlope);
        props.setProperty("cutoffDistIntercept", cutoffDistIntercept);

        return props;
    }

    public void setDefaultProperties() {
        setMinDistance(30.0);
        setMaxDistance(600.0);
        setMinSNR(8.0);
        setRequireCorrection(true);

        kA0 = DEFAULT_K;
        cA0 = DEFAULT_C;
        nA0 = DEFAULT_N;

        cutoffDistSlope = DEFAULT_CUTOFF_SLOPE;
        cutoffDistIntercept = DEFAULT_CUTOFF_INTERCEPT;
    }

} // HirooMlMagMethod

