package org.trinet.jasi.magmethods.TN;
import org.trinet.jasi.*;
/**
 * Overrides the distance correction of the SoCalMLMagMethod. <br>
 * This distance correction is based on an emperical relationship for southern
 * Uses epicentral distance for the station distance cutoff range values. <p>
 *
 * The method can use either corrected or uncorrected Wood-Anderson amplitudes
 * (types WAS, WAC, and WAU)
 * @see AmpType()
 */

public class MungSoCalMlMagMethod extends SoCalMlMagMethod {

    public MungSoCalMlMagMethod() {
        super();
        // String describing the magnitude method
        methodName = "MungSoCalMl";
    }

    // assume we don't need channel gains for the pre-digital era data 
    protected boolean hasRequiredChannelData(MagnitudeAssocJasiReadingIF jr) {
        Channel jrChan = jr.getChannelObj();
        java.util.Date date = jr.getDateTime(); // reading observation time
        if ( ! jrChan.hasLatLonZ() ) {
          // missing, if doLookUp reloads from source, but why repeated queries ad nauseum ?
          // also see implementation of JasiReading.setChannelObj(Channel) - aww
          if (doLookUp) jr.setChannelObjData(jrChan.lookUp(jrChan, date));
        }
        double dist = jrChan.getHorizontalDistance(); // slant ok to use for test -aww
        if ( (dist == Channel.NULL_DIST) || (dist == 0.0) || Double.isNaN(dist) ) {
          jrChan.calcDistance(jr.getAssociatedSolution()); // changed input arg type aww 06/11/2004
        }
        if (debug) {
          System.out.println(methodName+" DEBUG: hasRequiredChannelData() chan:"+jrChan+
          " for date:"+date+" latLonZ? "+jrChan.hasLatLonZ() + " dist: " + jrChan.getHorizontalDistance() + " az: " + jrChan.getAzimuth());
        }
        return (jrChan.hasLatLonZ()) ? true : false ; // isValid(jr);
    }

    /**
     * Return the distance cutoff appropriate for the method. Amplitudes from
     * stations father then this will not be included in the summary magnitude.
     * Because it depends on the magnitude the cutoff can only be applied in a
     * second pass through the amplitude readings.
     * This implementation has no Magnitude value dependence, it uses the
     * default Richter scheme, so the epicentral distance cutoff is 600 km unless
     * it is first set smaller by user.<p>
     * The cutoff distance is compared with station's "epicentral" distance. 
     * The difference between epicentral and slant distances is typically < 2 km,
     * for example, using epiDistance=minCutoff (30. km) and a solution 
     * depth of 10 km, the slantDistance is 31.6 km).
     * @see #setMaxDistance(double) 
     * @see #setMinDistance(double) 
     */
    public double getDistanceCutoff(double magnitude) {
        double dist = Math.max( getMinDistance(), 600.0 );
        return Math.min( dist, getMaxDistance() );
    }

} // MungSoCalMlMagMethod

