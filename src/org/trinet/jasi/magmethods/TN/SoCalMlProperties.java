package org.trinet.jasi.magmethods.TN;
import java.util.*;
import org.trinet.util.GenericPropertyList;
/**

ChauvenetThreshold = 0.5
MinCutoffDist
MaxCutoffDist
StepCutoffDist
DefaultCutoffDist
MinChannels
PreBuffer
MlClipValue

 * @see: PropertyList
 */

public class SoCalMlProperties extends GenericPropertyList {

    /** True to turn on debug messages. */
    static boolean debug = false;

    static final String DEFAULT_FILENAME = "SoCalMlProperties";

/**
 *  Construtor: reads default property file and System properties
 */

    public SoCalMlProperties() {
        this (DEFAULT_FILENAME);
    }

/**
 *  Construtor: Makes a COPY of a property list. Doesn't read the files.
 */
    public SoCalMlProperties(GenericPropertyList props) {
        super(props);
        setDefaultProperties();
    }

/**
 *  Read in a property file.
 */
    public SoCalMlProperties(String file) {
        super((Properties)null, file);
        setDefaultProperties();
    }

/**
 * Set values for certain essential properties so the program will work in the absence of
 * a default 'properties' file.
 */
    public void setDefaultProperties() {
       setProperty ("ChauvenetThreshold", 0.5);
       setProperty ("MinCutoffDist", 100.0);
       setProperty ("MaxCutoffDist", 800.0);
       setProperty ("StepCutoffDist", 100.0);
       setProperty ("DefaultCutoffDist", 300.0);
       setProperty ("MinChannels", 10);
       setProperty ("PreBuffer", 1.0);
       setProperty ("MlClipValue", 6.5);
    }

/**
* Save all the environment's properties.
*/
     public void save() {
          super.saveProperties ("--- SoCalMl Properties --- [" +
                          getUserPropertiesFileName()+"]");      // save user properties
    }

} // end of SoCalMlProperties class
