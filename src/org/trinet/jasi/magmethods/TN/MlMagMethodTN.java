package org.trinet.jasi.magmethods.TN;

//import org.trinet.jasi.*;
import org.trinet.jasi.magmethods.*;

public class MlMagMethodTN extends MlMagMethod {
    public MlMagMethodTN() {
        super();
    }
    // punt here for now
    public void configure(int src, String location, String section) {
        setProperties(location); // location like a filename url
    }

    public int getSummaryMagValueStatType() { return MEDIAN_MAG_VALUE; }

}
