package org.trinet.jasi.magmethods.TN;

import java.util.*;
import org.trinet.filters.*;
import org.trinet.jasi.*;
import org.trinet.jasi.magmethods.*;
import org.trinet.util.DateTime;
import org.trinet.util.Format;
import org.trinet.util.GenericPropertyList;
import org.trinet.util.MathTN;
import org.trinet.util.TimeSpan;
import org.trinet.util.gazetteer.GeoidalConvert;
import org.trinet.util.gazetteer.LatLonZ;

public class RichterMlMagMethod2 extends MlMagMethod {
    //
    private ButterworthFilterSMC bwf = null;
    private Format bwfFmt = new Format("%6.2f");
    private static final int DEFAULT_BWF_TYPE = 1; // 0 = HIGHPASS,  1 = BANDPASS 
    private int    bwfType = DEFAULT_BWF_TYPE;
    private boolean bwfScalePassBandByDistance = true;
    private double bwfLoFreq = 0.50;
    private double bwfHiFreq = 9.99; // Note for 20sps can't be 10 Hz
    private int    bwfOrder = 0; // by default don't do BWF unless order is > 0 by property setting

    private String bwfTypeStr = (bwfType == 0) ?
        "_HP_" + bwfFmt.form(bwfLoFreq).trim() + "_" + String.valueOf(bwfOrder): 
        "_BP_" + bwfFmt.form(bwfLoFreq).trim() + "_" + bwfFmt.form(bwfHiFreq).trim() + "_" + String.valueOf(bwfOrder); 

    private static double bwMicroseismMinAmp = .025; // when peak amp smaller, refilter waveform with butterworth
    private static double bwMicroseismMaxMag = 4.5; // except when channel magnitude larger than this


    private static double minPeriod = .045; // min amp period to accept, floor
    private static double maxPeriod = 5.0;  // max amp period to accept, ceiling

    // Exponential fit constants for min,max period based upon range where
    // minPeriodC1 * exp**(minPeriodC2*distance) <  period < maxPeriodC1 * exp**(maxPeriodC2*distance)
    private static double minPeriodC1 = .045;
    private static double minPeriodC2 = .005;
    private static double maxPeriodC1 = .85;
    private static double maxPeriodC2 = .003;

    private static double minVelAmp = .001;  // min amp allowed in cm for VEL sensor
    private static double maxVelAmp = 100.0; // max amp allowed for velocity sensor like HH
    private static double minAccAmp = .001;  // min amp allowed in cm for ACC sensor
    private static double maxAccAmp = 12000.; // max amp allowed for acceleration sensor like HL

    private double minSummarySNR = minSNR;

    private static double cutoffDistSlope     = 200.0; // will be reset from input cutoff parms props
    private static double cutoffDistIntercept = -270.0; // will be reset from input cutoff parms props

    private static double cutoffMag0Km     = -2.;
    private static double cutoffPivotMag   = 2.;
    private static double cutoffPivotMagKm = 130.;
    private static double cutoffMaxMag     = 4.35;
    private static double cutoffMaxMagKm   = 600.;

    private static double[] CORR_VERT = null;
    private static double[] CORR_DIST_VERT = null;

    private static boolean useSlant = false; // option to use slant for A0 array correction value -aww 2010/08/04

    private MlParms myparms = new MlParms();
    private HashMap parmsMap = new HashMap(3);

    private class MlParms {

        double [] corr = null;
        double [] corrDist = null;
        double [] corrVert = null;
        double [] corrVertDist = null;

        public void copy(MlParms mlp) {
            corr = mlp.corr;
            corrDist = mlp.corrDist;
            corrVert = mlp.corrVert;
            corrVertDist = mlp.corrVertDist;
        }

        public String toString() {
          StringBuffer sb = new StringBuffer(1024);
          sb.append(" Ml corrections dist,corr: ");
          if (corr.length == 0) return "";
          for (int idx=0; idx<corr.length; idx++) {
            sb.append(corrDist[idx]).append(",");
            sb.append(corr[idx]).append(" ");
          }
          sb.append("\n");
          if ( corrVert != null) {
            sb.append(" Ml corrections distV,corrV: ");
            for (int idx=0; idx<corrVert.length; idx++) {
              sb.append(corrVertDist[idx]).append(",");
              sb.append(corrVert[idx]).append(" ");
            }
            sb.append("\n");
          }
          return sb.toString();
        }
    }

    public RichterMlMagMethod2() {
        super();
        methodName = "RichterMl2";
    }

    // punt here for now - perhaps deprecate this method -aww
    public void configure(int src, String location, String section) {
        setProperties(location); // location like a filename url
    }

    public void initializeMethod() {
        super.initializeMethod();
        if (props == null) return; // custom properties

        if (props.getProperty("useSlant") != null) { // use amp slant distance in A0 function  -aww 2010/08/04
            useSlant = props.getBoolean("useSlant");
        }
        //
        if (props.getProperty("minPeriod") != null) { // smallest amp period allowed
            minPeriod = props.getDouble("minPeriod");
        }
        if (props.getProperty("minPeriodC1") != null) {
            minPeriodC1 = props.getDouble("minPeriodC1");
        }
        if (props.getProperty("minPeriodC2") != null) {
            minPeriodC2 = props.getDouble("minPeriodC2");
        }

        if (props.getProperty("maxPeriod") != null) { // longest amp period allowed
            maxPeriod = props.getDouble("maxPeriod");
        }
        if (props.getProperty("maxPeriodC1") != null) {
            maxPeriodC1 = props.getDouble("maxPeriodC1");
        }
        if (props.getProperty("maxPeriodC2") != null) {
            maxPeriodC2 = props.getDouble("maxPeriodC2");
        }

        if (props.getProperty("minVelAmp") != null) { // smallest amp value allowed for VEL
            minVelAmp = props.getDouble("minVelAmp");
        }
        if (props.getProperty("maxVelAmp") != null) { // largest amp value allowed for VEL
            maxVelAmp = props.getDouble("maxVelAmp");
        }
        if (props.getProperty("minAccAmp") != null) { // smallest amp value allowed for ACC
            minAccAmp = props.getDouble("minAccAmp");
        }
        if (props.getProperty("maxAccAmp") != null) { // largest amp value allowed for ACC
            maxAccAmp = props.getDouble("maxAccAmp");
        }

        minSummarySNR = minSNR; // smallest SNR for inclusion into summary mag stats
        if (props.getProperty("minSummarySNR") != null) {
            minSummarySNR = props.getDouble("minSummarySNR");
        }

        if (props.getProperty("corrDist") != null) {
            CORR_DIST = props.getDoubleArray("corrDist");
        }
        if (props.getProperty("corr") != null) {
            CORR = props.getDoubleArray("corr");
        }
        
        if (props.getProperty("corrDistVert") != null) {
            CORR_DIST_VERT = props.getDoubleArray("corrDist");
        }
        else CORR_DIST_VERT = CORR_DIST;

        if (props.getProperty("corrVert") != null) {
            CORR_VERT = props.getDoubleArray("corrVert");
        }
        else CORR_VERT = CORR;
        
        //
        if (props.getProperty("bwFilterType") != null) { 
          if( props.getProperty("bwFilterType", "BANDPASS").toUpperCase().startsWith("HIGH") ) bwfType = 0;
        }
        if (props.getProperty("bwFilterLoFreq") != null) { 
          bwfLoFreq = props.getDouble("bwFilterLoFreq");
        }

        if (props.getProperty("bwFilterHiFreq") != null) { 
          bwfHiFreq = props.getDouble("bwFilterHiFreq");
        }

        if (props.getProperty("bwFilterOrder") != null) { 
          bwfOrder = Math.min(props.getInt("bwFilterOrder"), 4);
          if (bwfOrder < 0) bwfOrder = 0;
        }
        if (props.getProperty("bwFilterScalePassBandByDistance") != null) { 
          bwfScalePassBandByDistance = props.getBoolean("bwFilterScalePassBandByDistance");
        }

        if (props.getProperty("bwMicroseismMinAmp") != null) { // when peak amp smaller, refilter waveform with butterworth HP
          bwMicroseismMinAmp = props.getDouble("bwMicroseismMinAmp"); 
        }
        if (props.getProperty("bwMicroseismMaxMag") != null) {// when channel mag larger do not refilter waveform with butterworth HP
          bwMicroseismMaxMag = props.getDouble("bwMicroseismMaxMag"); 
        }

        /*
        if (props.getProperty("cutoffDistSlope") != null) { // cutoffKm = slope * ML - intercept
          cutoffDistSlope = props.getDouble("cutoffDistSlope"); 
        }
        if (props.getProperty("cutoffDistIntercept") != null) {
         cutoffDistIntercept = props.getDouble("cutoffDistIntercept"); 
        }
        */

        if (props.getProperty("cutoffMag0Km") != null) {
        cutoffMag0Km = props.getDouble("cutoffMag0Km");
        }
        if (props.getProperty("cutoffPivotMag") != null) {
        cutoffPivotMag = props.getDouble("cutoffPivotMag");
        }
        if (props.getProperty("cutoffPivotMagKm") != null) {
        cutoffPivotMagKm = props.getDouble("cutoffPivotMagKm");
        }
        if (props.getProperty("cutoffMaxMag") != null) {
        cutoffMaxMag = props.getDouble("cutoffMaxMag");
        }
        if (props.getProperty("cutoffMaxMagKm") != null) {
        cutoffMaxMagKm = props.getDouble("cutoffMaxMagKm");
        }

        cutoffDistSlope = (cutoffMaxMagKm-cutoffPivotMagKm)/(cutoffMaxMag-cutoffPivotMag);
        cutoffDistIntercept = cutoffPivotMagKm - (cutoffDistSlope * cutoffPivotMag);

        //
    }

    protected void initRegionDependentProps() {


      MlParms mlp = (MlParms) parmsMap.get("DEFAULT");
      if ( mlp == null ) {
        //CORR = DEFAULT_CORR;
        //CORR_DIST = DEFAULT_CORR_DIST;
        mlp = new MlParms();
        mlp.corr = CORR;
        mlp.corrDist = CORR_DIST;
        mlp.corrVert = CORR_VERT;
        mlp.corrVertDist = CORR_DIST_VERT;
        parmsMap.put("DEFAULT", mlp);
      }

      myparms.copy(mlp);
      if (debug) System.out.println("DEBUG initRegionDependentProps default myparms: " + mlp.toString());

      if (props == null) return; // may not have had props set yet

      if (parmsRegionNames.length > 0) {
        String name = null;
        String propStr = null;
        for (int idx=0; idx<parmsRegionNames.length; idx++) {
            name = "ml."+parmsRegionNames[idx]+".corr";
            propStr = props.getProperty(name);
            if (propStr != null) {
                mlp = (MlParms) parmsMap.get(parmsRegionNames[idx]);
                if (mlp == null) {
                    mlp = new MlParms();
                    parmsMap.put(parmsRegionNames[idx], mlp);
                }
                mlp.corr = props.getDoubleArray(name);
                mlp.corrVert = mlp.corr;

            }
            name = "ml."+parmsRegionNames[idx]+".corrDist";
            propStr = props.getProperty(name);
            if (propStr != null) {
                mlp = (MlParms) parmsMap.get(parmsRegionNames[idx]);
                if (mlp == null) {
                    mlp = new MlParms();
                    parmsMap.put(parmsRegionNames[idx], mlp);
                }
                mlp.corrDist = props.getDoubleArray(name);
                mlp.corrVertDist = mlp.corrDist;
            }
            name = "ml."+parmsRegionNames[idx]+".corrVert";
            propStr = props.getProperty(name);
            if (propStr != null) {
                mlp = (MlParms) parmsMap.get(parmsRegionNames[idx]);
                if (mlp == null) {
                    mlp = new MlParms();
                    parmsMap.put(parmsRegionNames[idx], mlp);
                }
                mlp.corrVert = props.getDoubleArray(name);
            }
            name = "ml."+parmsRegionNames[idx]+".corrVertDist";
            propStr = props.getProperty(name);
            if (propStr != null) {
                mlp = (MlParms) parmsMap.get(parmsRegionNames[idx]);
                if (mlp == null) {
                    mlp = new MlParms();
                    parmsMap.put(parmsRegionNames[idx], mlp);
                }
                mlp.corrVertDist = props.getDoubleArray(name);
            }
        }
      }

      // Set current parms to default region settings, or else to the solution's region specific values
      if (eventRegionName != null) {
          if (debug) System.out.println("DEBUG recovering corr-dist parms from parmsMap for region: " + eventRegionName);
          mlp = (MlParms)parmsMap.get(eventRegionName);
          if (mlp != null) {
              myparms.copy(mlp);
              if (debug) System.out.println("DEBUG copied event region parms to myparms");
          }
          else if (debug) System.out.println("DEBUG NO parms found for region in map, copying DEFAULT parms to myparms");
      }
      else if (debug) System.out.println("DEBUG eventRegionName is NULL");

      if (debug) {
        System.out.println("DEBUG Current method myparms :");
        System.out.println(myparms.toString());

        System.out.println("DEBUG All known region parms :");
        System.out.println(parmsMap.toString());
      }
    }

    public GenericPropertyList getProperties() {
        
        GenericPropertyList props = super.getProperties();

        props.setProperty("useSlant", useSlant); // use amp slant distance in A0  -aww 2010/08/04

        props.setProperty("minPeriod", minPeriod); // smallest amp period allowed
        //
        props.setProperty("minPeriodC1", minPeriodC1);
        props.setProperty("minPeriodC2", minPeriodC2);
        //

        props.setProperty("maxPeriod", maxPeriod); // longest amp period allowed
        //
        props.setProperty("maxPeriodC1", maxPeriodC1);
        props.setProperty("maxPeriodC2", maxPeriodC2);
        //

        props.setProperty("minVelAmp", minVelAmp); // smallest amp value allowed for VEL
        props.setProperty("maxVelAmp", maxVelAmp); // largest amp value allowed for VEL
        props.setProperty("minAccAmp", minAccAmp); // smallest amp value allowed for ACC
        props.setProperty("maxAccAmp", maxAccAmp); // largest amp value allowed for ACC
        props.setProperty("minSummarySNR", minSummarySNR);

        props.setProperty("corrDist", CORR_DIST);
        props.setProperty("corr", CORR);
        props.setProperty("corrDistVert", CORR_DIST_VERT);
        props.setProperty("corrVert", CORR_VERT);

        //
        props.setProperty("bwFilterType", ((bwfType == 0) ? "HIGHPASS" : "BANDPASS")); // default is HP
        props.setProperty("bwFilterOrder", bwfOrder); // default is 0, no filtering
        props.setProperty("bwFilterLoFreq", bwfLoFreq); // default is 0.5
        props.setProperty("bwFilterHiFreq", bwfHiFreq); // default is 9.99 
        props.setProperty("bwMicroseismMinAmp", bwMicroseismMinAmp); // when peak amp smaller, refilter waveform with butterworth 
        props.setProperty("bwMicroseismMaxMag", bwMicroseismMaxMag);// when channel mag larger do not refilter waveform with butterworth 
        //

        //props.setProperty("cutoffDistSlope", cutoffDistSlope); // cutoffKm = slope * ML + intercept
        //props.setProperty("cutoffDistIntercept", cutoffDistIntercept);

        //
        props.setProperty("cutoffMag0Km", cutoffMag0Km);
        props.setProperty("cutoffPivotMag", cutoffPivotMag);
        props.setProperty("cutoffPivotMagKm", cutoffPivotMagKm);
        props.setProperty("cutoffMaxMag", cutoffMaxMag);
        props.setProperty("cutoffMaxMagKm", cutoffMaxMagKm);
        //

        ArrayList aList = new ArrayList(parmsMap.keySet());
        Collections.sort(aList);
        String keyname = null;
        MlParms mlp = null;
        for (int idx=0; idx<aList.size(); idx++) {
           keyname = (String) aList.get(idx);
           if (keyname.equals("DEFAULT")) continue;
           mlp = (MlParms) parmsMap.get(keyname);
           props.setProperty("ml."+keyname+".corr", mlp.corr);
           props.setProperty("ml."+keyname+".corrDist", mlp.corrDist);
           props.setProperty("ml."+keyname+".corrVert", mlp.corrVert);
           props.setProperty("ml."+keyname+".corrVertDist", mlp.corrVertDist);
        }

        return props;
    }

    public void setDefaultProperties() {
        super.setDefaultProperties();
        //
        useSlant = false;  // for A0(r) array correction distance -aww 2010/08/04
        //
        setMinDistance(20.0);  // floor for distance cutoff filter for valid amps 
        setMaxDistance(600.0); // NOTE Uhrhammer A0 is fit only to 500 km
        //
        bwfType = DEFAULT_BWF_TYPE; 
        bwfOrder = 0; // default, filter disabled
        bwfHiFreq = 9.99;
        bwfLoFreq = 0.50;
        bwMicroseismMinAmp = .025; // when peak amp smaller, refilter waveform with butterworth
        bwMicroseismMaxMag = 4.5; // except when channel magnitude larger than this
        //
        minPeriod = .045; // min amp period to accept, floor
        maxPeriod = 5.0; // max amp period to accept, ceiling
        minPeriodC1 = .045;
        minPeriodC2 = .005;
        maxPeriodC1 = .85;
        maxPeriodC2 = .003;
        minVelAmp = .001;  // min amp allowed in cm for VEL sensor
        maxVelAmp = 100.0; // max amp allowed for velocity sensor like HH
        minAccAmp = .001;  // min amp allowed in cm for ACC sensor
        maxAccAmp = 12000.; // max amp allowed for acceleration sensor like HL
        //
        minSummarySNR= minSNR;

        //cutoffDistSlope = 200.;
        //cutoffDistIntercept = -270.;

        cutoffMag0Km     = -2.;
        cutoffPivotMag   =  2.;
        cutoffPivotMagKm =  130.;
        cutoffMaxMag     =  4.35;
        cutoffMaxMagKm   =  600.;

        cutoffDistSlope = (cutoffMaxMagKm-cutoffPivotMagKm)/(cutoffMaxMag-cutoffPivotMag);
        cutoffDistIntercept = cutoffPivotMagKm - (cutoffDistSlope * cutoffPivotMag);

        scanPwave = false;
    }

    //public int getSummaryMagValueStatType() { return MEDIAN_MAG_VALUE; }
    // Override checks period range and/or peak amp min,max threshold?
    public boolean isValid(MagnitudeAssocJasiReadingIF jr) {

        Amplitude amp = (Amplitude) jr;
        String comment = (amp.comment.isNull()) ? "" : amp.comment.toString();

        if ( ! amp.isOnScale() ) {
            if (debug) {
                System.out.print("DEBUG RichterMl2 isValid input " +jr.getChannelObj().toDelimitedNameString());
                System.out.println(" !isValid, amp rejected, value not onScale: ");
            }
            if (comment.indexOf("!OS!") < 0) amp.comment.setValue(comment + " !OS!");
            return false;
        }

        if (amp.getType() == AmpType.WA) return true;  // accept older data without period or snr info

        //if (amp.isHuman()) return true; // accept all human "override" amps, won't work try new state of preferred "R"
        if (amp.isFinal()) return true; // accept preferred "override" required amps as trumps

        //if (! hasAcceptableSNR(amp)) return false;
        if (amp.snr.isValidNumber()) {
            double snr = amp.snr.doubleValue();
            if (snr > 0. && snr < getMinSNR()) {
              if (debug) {
                  System.out.print("DEBUG RichterMl2 isValid input " +jr.getChannelObj().toDelimitedNameString());
                  System.out.println(" !isValid, amp rejected, SNR: " + snr + " <  min ratio: " + getMinSNR());
              }
              if (comment.indexOf("!SNR!") < 0) amp.comment.setValue(comment + " !SNR!");
              return false;
            }
        }

        if ( amp.period.isValidNumber() ) { // screen by period -aww 03/13/2007 
            double d = amp.period.doubleValue();
            //default is 0.05 <= d <= 5.0,  was min 0.08, but some obs are smaller - aww 05/22/2007
            if (d < minPeriod || d > maxPeriod) {
                if (debug) {
                    System.out.print("DEBUG RichterMl2 isValid input " +jr.getChannelObj().toDelimitedNameString());
                    System.out.println(" !isValid, amp rejected, period: " + d + " not between " + minPeriod + " and " + maxPeriod);
                }
                if (comment.indexOf("!PER!") < 0) amp.comment.setValue(comment + " !PER!");
                return false;
            }
        }

        // Uhrhammer's screen of amp by min, max value -aww 05/01/2007
        // amplitude threshold for HH is 0.03 to 65 cm and for HL is .3 to 12000 cm
        if (isClipped(amp)) {
            if (debug)  {
                System.out.print("DEBUG RichterMl2 isValid input " +jr.getChannelObj().toDelimitedNameString());
                System.out.println(" !isValid, amp clipped");
            }
            if (comment.indexOf("!CLP!") < 0) amp.comment.setValue(comment + " !CLP!");
            return false;
        }

        return true;
    }

    public boolean isClipped(MagnitudeAssocJasiReadingIF jr) {
        Amplitude amp = (Amplitude) jr;
        boolean clipped = super.isClipped(jr);
        if (clipped) {
            if (debug) System.out.println("DEBUG RichterMl2 amp is onScale: " + amp.isOnScale());
            return true;
        }

        double d = Math.abs(amp.getValueAsCGS());
        if (Double.isNaN(d)) {
            if (debug) System.out.println("DEBUG RichterMl2 amp.getValueAsCGS is Double.NaN");
            clipped = true;
        }
        else {
          // Seems too strict - plenty of traces with signal energy at low amps, SNR matter more
          DateTime dt = jr.getDateTime();
          if (jr.getChannelObj().isVelocity(dt)) { // HH is 0.001 to 100 cm, seem ok at 100. -aww
            if ( d < minVelAmp || d > maxVelAmp) {
                if (debug) System.out.println("              !isValid bad VEL peak: " + d +" min,max:("+minVelAmp+","+maxVelAmp+")");
                clipped = true; 
            }
          }
          else if (jr.getChannelObj().isAcceleration(dt)) { // HL is .001 to 12000 cm -aww
            if ( d < minAccAmp || d > maxAccAmp) {
                if (debug) System.out.println("              !isValid bad ACC peak: " + d +" min,max:("+minAccAmp+","+maxAccAmp+")");
                clipped = true;
            }
          }
        }
        return clipped;
    }

    public boolean isValidForSummaryMag(MagnitudeAssocJasiReadingIF jr) {

        Amplitude amp = (Amplitude) jr;
        if (! isValid(amp)) {
            if (debug) {
                System.out.print("DEBUG RichterMl2 isValidForSummaryMag: " +jr.getChannelObj().toDelimitedNameString());
                System.out.println(" !isValidForSummaryMag amp !isValid");
            }
            return false;
        }


        if (amp.getType() == AmpType.WA) return true;  // accept older data without period or snr info

        //if (amp.isHuman()) return true; // accept all human "override" amps, won't work try new state of preferred "R"
        if (amp.isFinal()) return true; // accept preferred "override" required amps as trumps

        String comment = (amp.comment.isNull()) ? "" : amp.comment.toString();

        if (amp.snr.isValidNumber()) {
            double snr = amp.snr.doubleValue();
            if (snr > 0. && snr < minSummarySNR) {
              if (debug) {
                  System.out.print("DEBUG RichterMl2 isValidForSummaryMag: " +jr.getChannelObj().toDelimitedNameString());
                  System.out.println(" !isValidForSummaryMag amp rejected, SNR: " + snr + " < " + minSummarySNR);
              }
              if (comment.indexOf("!SSNR!") < 0) amp.comment.setValue(comment + " !SSNR!");
              return false;
            }
        }
        if ( amp.period.isValidNumber() ) { // screen by period -aww 03/13/2007 
            double per = amp.period.doubleValue();
            double dist = amp.getSlantDistance(); // use slant distance
            // reject shorter and longer periods
            if (per < minPeriodC1*Math.exp(minPeriodC2*dist) || per > maxPeriodC1*Math.exp(maxPeriodC2*dist) ) {
                if (debug) {
                    System.out.print("DEBUG RichterMl2 isValidForSummaryMag: " +jr.getChannelObj().toDelimitedNameString());
                    System.out.println(" !isValidForSummaryMag amp PER rejected: " + per +
                        " min,max per: " + minPeriodC1*Math.exp(minPeriodC2*dist)  + " , " + maxPeriodC1*Math.exp(maxPeriodC2*dist) );
                }
                if (comment.indexOf("!SPER!") < 0) amp.comment.setValue(comment + " !SPER!");
                return false;
            }
        }

        return true;
    }

    /**
     * Return the distance cutoff appropriate for the method. Amplitudes from
     * stations father then this will not be included in the summary magnitude.
     * Because it depends on the magnitude the cutoff can only be applied in a
     * second pass through the amplitude readings.
     */
    public double getAmpDistanceCutoff(double magValue) {

        if (disableMagnitudeDistanceCutoff) return super.getDistanceCutoff(magValue);

        // Return default ML max distance when filter is disabled by property setting
        // For relationship derived by Kanamori:  cutoffKm = 170.0 * magnitude - 205.0
        // we get for (ML, cutoffKM) :  (1, -35) (2, 135) (3, 305) (4, 475)

        // For cutoffMaxMagKm=600 and cutoffPivotMag=2, cutoffMaxMag = (600 - cutoffDistIntercept)/cutoffDistSlope
        // then cutoffMaxMag= (600+200)/200=4.0 (600+174/182)=4.25 (600+222/191)=4.3 (600+270)/200=4.35 (600+205)/170=4.74
        double cutoff = 0;
        if (magValue <= cutoffPivotMag) {
          cutoff = Math.max(getMinDistance(), ((cutoffPivotMagKm-cutoffMag0Km)*magValue/cutoffPivotMag + cutoffMag0Km));
        }
        else {
          cutoff = Math.max(getMinDistance(),cutoffDistSlope*magValue + cutoffDistIntercept);
        }
        //System.out.println(methodName + " ampDistanceCutoff : " + cutoff);
        return cutoff;
    }

    public double getDistanceCutoff(double magValue) {
        // Return default ML max distance when filter is disabled by property setting
        if (disableMagnitudeDistanceCutoff) return super.getDistanceCutoff(magValue);
        double dist = getAmpDistanceCutoff(magValue);
        return Math.ceil(Math.min(dist, Math.min(600., getMaxDistance())));
    }

    //
    // Override methods here to use a different A0(r) functions for Vertical vs. Horizontal components
    // would also need new properties like corrVert corrDistVert to  initialize arrays
    public double calcChannelMagValue(Amplitude amp) throws WrongDataTypeException {
      if (! isIncludedReadingType(amp) ) {
          throw new WrongAmpTypeException(
            "Wrong Amp type for MagnitudeMethod " +methodName+
            " : " +amp.toNeatString() // +AmpType.getString(amp.type)
          );
      }
      if (amp.isClipped()) { // should we throw WrongDataTypeException?
          throw new WrongAmpTypeException(
            "Amp isClipped for MagnitudeMethod "+methodName+
            " : " +amp.toNeatString()
          );
      }
      //to discriminate for small events with negative channel corrections changed to -9.
      double value = -9.; // changed 0. to -9 to make it obvious to user - aww 8/12/2004
      try {
        Double correction = getMagCorr(amp);
        if (debug) System.out.print(amp.getChannelObj().toDelimitedSeedNameString("."));
        value = calcChannelMagValue(
                        (useSlant) ? amp.getSlantDistance() : amp.getHorizontalDistance(), // option for slant, default=horizDist aww 08/04/2010
                        getAmpValue(amp),
                        (correction == null)? 0. : correction.doubleValue(),
                        amp.getChannelObj().isVertical()
                );
      }
      catch (WrongDataTypeException ex) {
        ex.printStackTrace();
      }

      if (value < -10.) value = -10.; 
      else if (value > 10.) value = 10.; 

      return value;
    }

    private final double calcChannelMagValue(double distance, double mmHalfAmp, double staticCorrection, boolean isVert) {

      mmHalfAmp = Math.abs(mmHalfAmp); // make sure its +positive

      if (debug) {
        System.out.println(
            " mmHalfAmp, log10(mmHalfAmp), distance, distanceCorrection(distance, isVert), staticCorrection:\n "+
            mmHalfAmp+ " "+ MathTN.log10(mmHalfAmp) +" " + distance +" " + distanceCorrection(distance, isVert) +" " + staticCorrection);
      }


      double value =  MathTN.log10(mmHalfAmp) +         // calculated mag
             distanceCorrection(distance, isVert) + // distance correction
             staticCorrection +             // channel correction
             getGlobalCorrection() ;        // global correction
      // use this with distanceCorrection method printf
      //System.out.printf(" =>chanMag: %4.2f\n", value);

      return value;
    }

    private final double distanceCorrection(double distance, boolean isVert) {

      if (distance < 0.) throw new IllegalArgumentException("input distance < 0");

      double value = (isVert) ? 
          distanceCorrection(distance, myparms.corrVert, myparms.corrVertDist) : distanceCorrection(distance, myparms.corr, myparms.corrDist); 

      if (debug) System.out.println("ML distance="+distance+" A0 corr= "+value + " isVert: " + isVert);

      return value; // out of allowed range

    }

    private final double distanceCorrection(double distance, double[] corr, double[] dist) {
        if (distance < dist[0]) {
          return corr[0]; // return minimum distance correction
        }
        double value = 0.;
        for (int i=1; i<dist.length; i++) {
            if (dist[i] > distance) {
                if (interpolate) {
                   value =  corr[i-1] + ((distance - dist[i-1])*(corr[i]-corr[i-1]))/(dist[i]-dist[i-1]);
                }
                else {
                   // modified 2015/05/14 -aww 
                   // If not interpolated, the corr index to return depends on the alignment of corr and distance array values.
                   // If corr arrays values are shifted right, e.g. the corr for 10 km is mapped to next distance element at 15 km etc.
                   // The method should return corr[i] but for when corr arrays values are not not shifted wrt distance array mapping,
                   // method should return corr[i-1] value.
                   // 
                   if ( ! useClosestDistCorr ) {
                     //value = (roundUp) ? CORR[i] : CORR[i-1];
                     // To preserve behavior of shifted Richter defaults would do this:
                     //value = corr[i]; // shifted map, method used to always return corr[i] when dist[i] > distance
                     value = corr[i-1]; // unshifted, one-to-one distance and corr array map
                   }
                   else {
                     //closest array element 1-to-1:
                     value = ((dist[i] - distance) < (distance - dist[i-1]) ) ? corr[i] : corr[i-1];
                   }
                }
                //System.out.printf(" chanKm:%6.2f array i: %02d corrDist,corr[i]: %6.2f,%4.2f [i-1]: %6.2f,%4.2f =>chanDistCorr: %4.2f",
                //           distance,i,dist[i],corr[i],dist[i-1],corr[i-1],value);

                return value; // interpolated or rounded value
            }
        }
        // return maximum distance correction, previously would return 0. -aww 2015/05/14
        return corr[dist.length-1];
    }
    //

    public MagnitudeAssocJasiReadingIF createAssocMagDataFromWaveform(Solution aSol, Waveform wf, TimeSpan scanSpan) {
        SolutionList sl = new SolutionList(2);
        sl.add(aSol, false);
        return createAssocMagDataFromWaveform(sl, wf, scanSpan);
    }
    //public MagnitudeAssocJasiReadingIF createAssocMagDataFromWaveform(Solution aSol, Waveform wf, TimeSpan scanSpan) {
    public MagnitudeAssocJasiReadingIF createAssocMagDataFromWaveform(SolutionList solList, Waveform wf, TimeSpan scanSpan) {

       if (wf == null) return null;

       // test SolutionList input code - aww 2014/08/15
       //if (solList == null || solList.size() < 1) {
       //  System.err.println("Error RichterMl2 createAssocMagDataFromWaveform input solution list null or empty");
       //}
       //solList.sortByTime(); // assume input list is already sorted ??
       Solution aSol = solList.getSolution(0);

       if ( ! scanAllWaveformsForMag ) { // only accept channels from stations with phase picks
          if ( aSol.getPhaseList().isEmpty() ) aSol.loadPhaseList();
          if ( ! aSol.getPhaseList().hasEntryForStation(wf.getChannelObj().getChannelName()) ) {
             if (debug) {
                System.out.println("DEBUG RichterMl2 createAssocMagDataFromWaveform scanAllWaveformsForMag=false and station has no picks, reject: " +
                        wf.getChannelObj().toDelimitedSeedNameString(" "));
             }
             return null;
          }
       }

       // if there is no time-series, we load it here and unload it when done.
       boolean localLoad = false;
       if (!wf.hasTimeSeries())  {
          if (wf.loadTimeSeries()) {     // try to load it
            //if (debug) System.out.println("DEBUG RichterMl2 createAssocMagDataFromWaveform LOCAL LOAD of timeseries for: "+
            //            wf.getChannelObj().toDelimitedSeedNameString(" "));
            localLoad = true;
          } else {
            if (debug) System.out.println("DEBUG RichterMl2 createAssocMagDataFromWaveform NO Scan NO timeseries: "+
                        wf.getChannelObj().toDelimitedSeedNameString(" "));
            return null ;                // nothing loaded
          }
       }

       // have it already
       //if (debug) System.out.println("DEBUG RichterMl2 createAssocMagDataFromWaveform timeseries ALREADY loaded for: "+
       //                 wf.getChannelObj().toDelimitedSeedNameString(" "));

       // Need way of passing the "clipping" level to the waveform instance
       // so that the filter knows whether to flag the resulting signal/amp
       // as a clipped timeseries like: wf.setClipCounts(value), however the wf
       // may be clipped outside of the timespan of interest for the seismic energy,
       // thus to be robust you need to do the wf.getPeakAmplitude(ts) before filtering,
       // adding much overhead, yuck!!! -aww
       if (wf.isClipped()) {
           if (debug)
              System.out.println ("DEBUG RichterMl2 createAssocMagDataFromWaveform b4 filter, input wf is clipped, reject: "+
                      wf.getChannelObj().toDelimitedSeedNameString(" "));
           if (localLoad) wf.unloadTimeSeries(); // aww added 2016/12/30
           return null; // filter failed
       }

       Waveform filteredWf = wf;
       String inputFilterName = wf.getFilterName();

       // filter if appropriate
       if (hasWaveformFilter()) {
          WAFilter.doLookUp = doLookUp; // test here to skip db query when property is set -aww 2010/08/19
          filteredWf = (Waveform) wfFilter.filter(wf);
          if (filteredWf == null) {
            if (debug)
              System.out.println ("DEBUG RichterMl2 createAssocMagDataFromWaveform b4 Filter failed: "+
                      wf.getChannelObj().toDelimitedSeedNameString(" "));
              if (localLoad) wf.unloadTimeSeries(); // aww added 2016/12/30
              return null; // filter failed
          }
       }

       Amplitude amp = null;

       currentSol = aSol; // for tracking purposes
       double originDateTime = aSol.getTime();
       double depth = aSol.getModelDepth(); // aww 2015/10/10
       if (Double.isNaN(depth)) depth = 0.; // moved here from down below in noise section -aww 06/04/2007

       double wfHorizDist = wf.getHorizontalDistance();
       double wfSlantDist = (wfHorizDist == Channel.NULL_DIST) ? 
           Channel.NULL_DIST : Math.sqrt(depth*depth + wfHorizDist*wfHorizDist);

       boolean outsideOfTimeSpan = false;

       TimeSpan ts = null;

       // get peak in energy window
       // Note if "clipping" counts level is known to waveform
       // then it could flag it as clipped after scanning it.
       // if not already done so by filteredWf method. -aww
       if (scanSpan != null) {
           outsideOfTimeSpan = ! filteredWf.getTimeSpan().contains(scanSpan);
           amp = filteredWf.getPeakAmplitude(scanSpan, peakType);
           if (debug) 
             System.out.println("DEBUG RichterMl2 createAssocMagDataFromWaveform Scan: "+
                             wf.getChannelObj().toDelimitedSeedNameString(" ")+
                             " windowsize1= "+scanSpan.getDuration() + " " + scanSpan.toString());
       }
       else if (scanWaveformTimeWindow) {
           ts = (scanPwave) ?
               wf.getEnergyTimeSpan(originDateTime, wfHorizDist, depth): wf.getSWaveEnergyTimeSpan(originDateTime, wfHorizDist, depth);
           if (scanSpanMaxWidth > 0. && ts.getDuration() > scanSpanMaxWidth) {
               ts.setEnd(ts.getStart()+scanSpanMaxWidth);
               if (debug) System.out.println("DEBUG RichterMl2     reset max scan span width to: " + scanSpanMaxWidth + " timespan: " + ts);
           }

           // xxx: added code to process any extra solutions found input SolutionList -aww 2014/08/20
           Solution sol2 = null;
           int solcnt = solList.size();
           if (solcnt > 1) { // get the next one (assumes temporal order, ie. later)
               sol2 = solList.getSolution(1);
           }
           else if (solcnt > 2 ) { // otherwise, get solution closest to 1st (assumes temporal sort with the 1st being the one whose amps are desired)
               double minDistKm = 999999.;
               double km = 0;
               Solution nextSol = null;
               LatLonZ llz = null;
               LatLonZ myllz = aSol.getLatLonZ();
               for ( int idx=1 ; idx < solcnt; idx++ ) {  
                    nextSol = solList.getSolution(idx);
                    llz = nextSol.getLatLonZ();
                    if (llz.isNull())  continue;
                    km = GeoidalConvert.horizontalDistanceKmBetween(myllz, llz);
                    if ( km < minDistKm ) {
                        minDistKm = km;
                        sol2 = nextSol;
                    }
               }
           }

           if ( sol2 != null ) { // we have another event after the first (assumes time sorted)
               // substract 2 seconds for cushion before predicted P, assuming second event's origin time is close.
               double ttp = sol2.getTime() - 2 + 
                   TravelTime.getInstance().getTTp(GeoidalConvert.horizontalDistanceKmBetween(sol2.getLatLonZ(),wf.getChannelObj().getLatLonZ()), depth);

               if (ttp <= ts.getStart() ) { // skip if start is after second event P
                   if (localLoad) wf.unloadTimeSeries(); // aww added 2016/12/30
                   return null;
               }

               if ( ttp < ts.getEnd() ) { // reset scan window end if second event predicted P is before window end
                   ts.setEnd(ttp);
                   if (debug)
                       System.out.println ("DEBUG RichterMl2 createAssocMagDataFromWaveform Scan: "+
                              wf.getChannelObj().toDelimitedSeedNameString(" ")+
                              " windowsize reset by sol2= "+ts.getDuration() + " " + ts.toString());
               }
               else if (debug) {
                       System.out.println ("DEBUG RichterMl2 createAssocMagDataFromWaveform Scan window ends for : "+
                              wf.getChannelObj().toDelimitedSeedNameString(" ")+ " before the predicted P for sol2");
               }
           }
           // xxx

           outsideOfTimeSpan = ! filteredWf.getTimeSpan().contains(ts);
           amp = filteredWf.getPeakAmplitude(ts, peakType);
           if (debug)
             System.out.println ("DEBUG RichterMl2 createAssocMagDataFromWaveform Scan: "+
                              wf.getChannelObj().toDelimitedSeedNameString(" ")+
                              " windowsize2= "+ts.getDuration() + " " + ts.toString());
       } else { // get peak in whole waveform
           amp = filteredWf.getPeakAmplitude(filteredWf.getTimeSpan(), peakType);
       }

       // amp can be 'null' if no time series
       if (amp == null) {
          if (debug) 
            System.out.println ("DEBUG RichterMl2 createAssocMagDataFromWaveform peak amp NULL: " +
                   wf.getChannelObj().toDelimitedSeedNameString() + " check that timeseries exists for time span!");
           if (localLoad) wf.unloadTimeSeries(); // aww added 2016/12/30
           return null;
       }

       // flag amp as questionable if scan span truncated by end of timeseries
       if (outsideOfTimeSpan) {
           if (! amp.isAuto()) amp.setQuality(Amplitude.COMPLETENESS_UNKNOWN); // assume human will review it, so let it pass
           if (debug)
             System.out.println ("DEBUG RichterMl2 createAssocMagDataFromWaveform timespan bounds beyond end of series " +
                     wf.getChannelObj().toDelimitedSeedNameString() );

       }

       // Check for clipping code added below -aww 2009/06/10
       double clip = 0.; // undefined clipping value is 0.
       // Either lookup clip amp from defined JASI_AMPPARMS_VIEW directly using waveform channel and amp date
       // clip = wf.getChannelObj().getClipAmpValue(amp.getDatetime());
       // or instead get clipping amp from correction map (which assumes correction lookup view joined to clip table)
       MagnitudeCalibrationIF calibrData = getCalibrationFor(amp);
       if (calibrData != null) {
         Double clipping = (Double) calibrData.getClipAmp(amp);
         if (clipping == null || clipping.isNaN()) clip = 0.;
         else {
             clip = Math.abs(clipping.doubleValue());
             if (debug) System.out.println(methodName + " DEBUG clipCheck= " + clipCheck + "  clip= " + clip + " clipFactorA= " + clipFactorA +
                 " clipFactorDA= " + clipFactorDA + " clipFactorDV=" + clipFactorDV);
             if  (clip <= 4096.) {
                 clip = clip*clipFactorA;
                 if (debug) System.out.println(methodName + " DEBUG clip < 4096, assuming analog now using clipFactorA= " + clipFactorA);
             }
             else clip = (wf.getChannelObj().isAcceleration()) ? clip*clipFactorDA : clip*clipFactorDV;
             if (debug) System.out.println(" re-scaled clip= " + clip);
         }
       }

       // use clipping to reject amp only when clip is a defined value > 0.
       if (clip <= 0.) {
           if (debug) 
               System.out.println(methodName + " DEBUG  createAssocMagDataFromWaveform CLIPPING AMP IS ZERO for: " + wf.getChannelObj().toDelimitedSeedNameString(" "));
       }
       else if (clipCheck) {
         //Get peak amp value
         double value = 0.;
         if (hasWaveformFilter()) { // like Wood-Anderson cm, so find raw wf peak in counts
           Sample smpl  // =  wf.closestSample(amp.getTime()); // filtered peak could be "time-shifted" from raw peak
                           = wf.scanForPeak(amp.getTime()-3, amp.getTime()+3); // window 3 secs
           value = (smpl == null || Double.isNaN(smpl.value)) ? 0. : Math.abs(smpl.value);
         }
         else { // no filter raw counts?
           value = amp.getValue().doubleValue();
           if (Double.isNaN(value)) {
             System.out.println(methodName + " DEBUG createAssocMagDataFromWaveform peak AMP IS NaN for: " + wf.getChannelObj().toDelimitedSeedNameString(" "));
             if (localLoad) wf.unloadTimeSeries(); // aww added 2016/12/30
             return null;
           }
           value = Math.abs(value);
           if (!amp.halfAmp) value /= 2.; // compare half
         }

         if (value >= clip) {
             if (debug)
               System.out.println(methodName + " DEBUG createAssocMagDataFromWaveform PEAK >= CLIP: (" +value+ "," +clip+ ") rejecting " +
                           wf.getChannelObj().toDelimitedSeedNameString(" "));
             if (localLoad) wf.unloadTimeSeries(); // aww added 2016/12/30
             return null;
         }
         else {
             if (debug)
                 System.out.println(methodName + " DEBUG createAssocMagDataFromWaveform PEAK < CLIP: (" +value+ "," +clip+ ") accepting " +
                           wf.getChannelObj().toDelimitedSeedNameString(" "));
         }
       }
       // end of clipping check code addition -aww

       // Associate solution with amp to allow slant distance calc using JasiReading method
       amp.assign(aSol); // added 04/25/2005 -aww
       if ( ! inputFilterName.trim().equals("") ) {
         if (debug) System.out.println("DEBUG RichterMl2 inputFilterName : " + inputFilterName);
         filteredWf.setFilterName("WA_" + inputFilterName);
         amp.setType(AmpType.WASF); // requires constraint change on AMP table in db - aww 11/11/2007
         amp.comment.setValue(filteredWf.getFilterName());
       }
       else amp.setType(AmpType.WAS);

       // calc SNR
       float noiseLevel = Float.NaN;
       int scanType = WFSegment.scanNoiseType;
       TimeSpan tsp = wf.getPreEnergyTimeSpan(originDateTime, wfHorizDist, depth); // aww 06/11/2004

       if (scanType != WFSegment.RMS) { // AVG peak
           noiseLevel = filteredWf.scanForNoiseLevel(tsp);
           if (! Float.isNaN(noiseLevel)) {
               double snr = Math.abs( amp.value.doubleValue() ) / noiseLevel;
               if (!amp.halfAmp) snr /= 2.; // compare half
               amp.snr.setValue(snr);
           }
           // UNCOMMENT BELOW TO OUTPUT unfiltered noise level per peak channel amp 
           // float unfilteredNoiseLevel = wf.scanForNoiseLevel(tsp);
           // System.out.println(wf.getChannelObj().toDelimitedSeedNameString()+" noiseLevel,gain,unfilteredNoiseLevel : "+
           // noiseLevel+" "+wf.getChannelObj().getGain(aSol.getDateTime())+" "+unfilteredNoiseLevel);
       }
       else { // note a RMS SNR value of ~2 is equivalent to ~5 the avg peak way -aww
           TimeSpan tsRMS = new TimeSpan(tsp);
           tsRMS.setEnd(tsRMS.getEnd()+3.); // adding 3 secs to get to within 1 sec p-arrival time
           tsRMS.setStart(Math.max(tsRMS.getStart(),tsRMS.getEnd()-6.)); // try a 6 sec noise window
           noiseLevel = filteredWf.scanForNoiseLevel(tsRMS);
           tsRMS = new TimeSpan(amp.datetime.doubleValue()-1.,amp.datetime.doubleValue()+1.); // try a 2 sec signal window
           float signalLevel = filteredWf.scanForNoiseLevel(tsRMS);
           if (! Float.isNaN(noiseLevel)) amp.snr.setValue( Math.abs(signalLevel)/noiseLevel );
           //String SNRstr = wf.getChannelObj().toDelimitedSeedNameString()+ " rmsSNR: "+ bwfFmt.form(signalLevel/noiseLevel);
       }

       // Noise worst in winter, as much as WAS amp ~ .03 cm, more typically < .01, but in summer usually < .005
       // amps for ML 2.5 at 500 km comparable to fair weather microseismic background noise
       // most microseisms < .005 cm, mag > 4.5 energy overlaps with microseism spectra at great distance -aww
       try {
         // System.out.println("DEBUG RichterMl2 amp channel: " + amp.getChannelObj().toDumpString() + " value: " + amp.value.doubleValue() +
         // " period: " + amp.period.doubleValue() + " maxPeriod: " +  maxPeriodC1*Math.exp(maxPeriodC2*wfSlantDist) ); 
         boolean doPerFilter = amp.period.isValidNumber();  
         boolean doBWFilter = false;
         // You can set the value of bwMicroseismMaxMag to very small value, say -1 to disable this second filtering
         if (doPerFilter) {
             // at 20 km range most have periods < .7 and at 500 km most have periods < 2. 
             // some waveforms have arriving phases combining to create a pseudo period > 2. but usually have higher amp signals
             doBWFilter = (amp.period.doubleValue() > maxPeriodC1*Math.exp(maxPeriodC2*wfSlantDist));
         }
         doBWFilter |= ((Math.abs(amp.value.doubleValue()) < bwMicroseismMinAmp ) && (calcChannelMagValue(amp) < bwMicroseismMaxMag));

         //if ( ! inputFilterName.trim().equals("") &&
         //NOTE that setting input property bwFilterOrder to value <=0 disables secondary filtering here
         if (doBWFilter && bwfOrder > 0) {
           //System.out.println("Doing BWF on channel : " + amp.getChannelObj().toDelimitedSeedNameString());

           if (bwf == null  || bwfScalePassBandByDistance) {
               int rate = (int) wf.getSampleRate(); 
               double bwfHiCut = bwfHiFreq;
               double bwfLoCut = bwfLoFreq;
               if (bwfScalePassBandByDistance) {
                   //bwfLoCut = 1./Math.min(maxPeriod, maxPeriodC1*Math.exp(maxPeriodC2*wfSlantDist));
                   //bwfHiCut = 1./Math.max(minPeriod, minPeriodC1*Math.exp(minPeriodC2*wfSlantDist));
                   // range scale, somewhat arbitrary bandpass between .5 and (20 Hz at 10 km and 8 Hz at 600 km), roughly
                   //bwfLoCut = 0.5; // remove 2012/02/21 -aww, default to user property value
                   bwfHiCut = 1./Math.max(minPeriod, .05*Math.exp(.0015*wfSlantDist));
               }
               bwfHiCut = Math.min(bwfHiCut, ((double)rate)/2. - 0.01); // Note subtract 0.01 from hi frequency in beyond Nyquist
               bwfLoCut = Math.min(bwfLoCut, bwfHiCut); // screen for low sample rate

               if (bwfType == 1) { // bandpass type
                 bwf = ButterworthFilterSMC.createBandpass(rate, bwfLoCut, bwfHiCut, bwfOrder, true);
                 bwfTypeStr = "_BP_" + bwfFmt.form(bwfLoCut).trim() + "_" + bwfFmt.form(bwfHiCut).trim() + "_" + String.valueOf(bwfOrder); 
               }
               else { // highpass type
                 bwf = ButterworthFilterSMC.createHighpass(rate, bwfLoCut, bwfOrder, true);
                 bwfTypeStr = "_HP_" + bwfFmt.form(bwfLoCut).trim() + "_" + String.valueOf(bwfOrder); 
               }
               if (bwf != null) bwf.copyInputWaveform(false);
               else System.err.println("Error RichterMl2 creating ButterworthFilter for low amp, skipping filter!");
           }

           if (bwf != null) { // else error?,  punt back to original amp -aww 2008/07/31
             filteredWf = (Waveform) bwf.filter(filteredWf);
             if ( ! inputFilterName.trim().equals("") )
                 filteredWf.setFilterName("WA" + bwfTypeStr +  "_+_" + inputFilterName);
             else
                 filteredWf.setFilterName("WA" + bwfTypeStr);
          
             if (scanSpan != null) {
               amp = filteredWf.getPeakAmplitude(scanSpan, peakType);
             }
             else if (scanWaveformTimeWindow) {
               //TimeSpan ts = wf.getEnergyTimeSpan(originDateTime, wfHorizDist, depth); // removed, does P energy, which is often noise -aww
               //TimeSpan ts = wf.getSWaveEnergyTimeSpan(originDateTime, wfHorizDist, depth);
               if (scanSpanMaxWidth > 0. && ts.getDuration() > scanSpanMaxWidth) {
                   ts.setEnd(ts.getStart()+scanSpanMaxWidth);
               }
               amp = filteredWf.getPeakAmplitude(ts, peakType);
             } else {
               amp = filteredWf.getPeakAmplitude(filteredWf.getTimeSpan(), peakType);
             }
  
             // amp can be 'null' 
             if (amp == null) {
                if (debug) 
                  System.out.println ("DEBUG RichterMl2 createAssocMagDataFromWaveform filtered peak amp NULL: " +
                         wf.getChannelObj().toDelimitedSeedNameString() + " check that timeseries exists for time span!");
                 if (localLoad) wf.unloadTimeSeries(); // aww added 2016/12/30
                 return null;
             }

             amp.assign(aSol);
             //amp.setType(AmpType.WAS); // need to enable below instead -aww
             amp.setType(AmpType.WASF); // requires constraint change on AMP table in db - aww 11/11/2007
             amp.comment.setValue(filteredWf.getFilterName());
             if (outsideOfTimeSpan && ! amp.isAuto()) amp.setQuality(Amplitude.COMPLETENESS_UNKNOWN); // assume human will review it, so let it pass
  
             noiseLevel = Float.NaN;
             scanType = WFSegment.scanNoiseType;
             tsp = wf.getPreEnergyTimeSpan(originDateTime, wfHorizDist, depth);
  
             if (scanType != WFSegment.RMS) { // AVG peak
                 noiseLevel = filteredWf.scanForNoiseLevel(tsp);
                 if (! Float.isNaN(noiseLevel)) {
                     double snr = Math.abs( amp.value.doubleValue() ) / noiseLevel;
                     if (!amp.halfAmp) snr /= 2.; // compare half
                     amp.snr.setValue(snr);
                 }
             }
             else {  // note a RMS SNR value of ~2 is equivalent to ~5 the avg peak way -aww
                 TimeSpan tsRMS = new TimeSpan(tsp);
                 tsRMS.setEnd(tsRMS.getEnd()+3.); // adding 3 secs to get to within 1 sec p-arrival time
                 tsRMS.setStart(Math.max(tsRMS.getStart(),tsRMS.getEnd()-6.)); // try a 6 sec noise window
                 noiseLevel = filteredWf.scanForNoiseLevel(tsRMS); // RMS
                 tsRMS =  new TimeSpan(amp.datetime.doubleValue()-1.,amp.datetime.doubleValue()+1.); // try a 2 sec signal window
                 float signalLevel = filteredWf.scanForNoiseLevel(tsRMS);
                 if (! Float.isNaN(noiseLevel)) amp.snr.setValue( Math.abs(signalLevel)/noiseLevel );
                 //String SNRstr = wf.getChannelObj().toDelimitedSeedNameString()+ " rmsSNR: "+ bwfFmt.form(signalLevel/noiseLevel) + " BW"; 
             }
           } // bwf filter is not null
         } // end of doBWFilter
       } catch (WrongDataTypeException ex) { }
       
       //System.out.println(SNRstr); // debug test -aww

       // unload time-series
       if (localLoad) wf.unloadTimeSeries();

       //if (amp.halfAmp) System.out.println("Amp is ZERO2PEAK value: " + amp.value.doubleValue() + " "+ wf.getChannelObj().toDelimitedSeedNameString(" "));
       //else System.out.println("Amp is PEAK2PEAK value: " + amp.value.doubleValue() + " "+ wf.getChannelObj().toDelimitedSeedNameString(" "));

       return amp;  // removed (hasAcceptableSNR(amp)) ? amp:null test, let isValid(amp) method decide -aww 11/02/2007
    }

    /*
    public void setCORR() {
        System.out.println("Setting defaults2....");
        //CORR=new double[]{
            //1.40,1.58,1.72,1.86,1.98,2.08,2.18,2.26,2.34,2.40,2.47,2.53,2.60,2.65,2.70,2.80,2.86,2.91,2.96,3.00,
            //3.10,3.20,3.30,3.40,3.50,3.60,3.65,3.70,3.80,3.90,4.00,4.10,4.20,4.30,4.40,4.50,4.60,4.70,4.80,4.90
        //};
        //CORR_DIST=new double[]{
        //    0.00,5.00,10.0,15.0,20.0,25.0,30.0,35.0,40.0,45.0,50.0,55.0,60.0,65.0,70.0,80.0,85.0,90.0,95.0,100.,
        //    110.,150.,170.,190.,210.,220.,230.,250.,270.,290.,310.,330.,350.,380.,400.,430.,470.,510.,560.,601.
        //};
        CORR_DIST = new double[]{
        0.00,5.00,10.0,15.0,20.0,25.0,30.0,35.0,40.0,45.0,50.0,55.0,60.0,65.0,70.0,80.0,85.0,90.0,95.0,100.,
        105.,110.,115.,120.,125.,130.,135.,140.,145.,150.,155.,160.,165.,170.,175.,180.,185.,190.,195.,200.,
        205.,210.,215.,220.,225.,230.,235.,240.,245.,250.,255.,260.,265.,270.,275.,280.,285.,290.,295.,300.,
        305.,310.,315.,320.,325.,330.,335.,340.,345.,350.,355.,360.,365.,370.,375.,380.,385.,390.,395.,400.,
        405.,410.,415.,420.,425.,430.,435.,440.,445.,450.,455.,460.,465.,470.,475.,480.,485.,490.,495.,500.,
        505.,510.,515.,520.,525.,530.,535.,540.,545.,550.,555.,560.,565.,570.,575.,580.,585.,590.,595.,601.
        };
        CORR = new double[]{
        1.40,1.58,1.72,1.86,1.98,2.08,2.18,2.26,2.34,2.40,2.47,2.53,2.60,2.65,2.70,2.80,2.86,2.91,2.96,3.00,
        3.03,3.08,3.10,3.12,3.15,3.19,3.21,3.23,3.28,3.29,3.30,3.32,3.35,3.38,3.40,3.43,3.45,3.47,3.50,3.53,
        3.56,3.59,3.62,3.65,3.68,3.70,3.72,3.74,3.77,3.79,3.81,3.83,3.85,3.88,3.92,3.94,3.97,3.98,4.00,4.02,
        4.05,4.08,4.10,4.12,4.15,4.17,4.20,4.22,4.24,4.26,4.28,4.30,4.32,4.34,4.36,4.38,4.40,4.42,4.44,4.46,
        4.48,4.50,4.51,4.52,4.54,4.56,4.57,4.59,4.61,4.62,4.63,4.64,4.66,4.68,4.69,4.70,4.71,4.72,4.73,4.74,
        4.75,4.76,4.77,4.78,4.79,4.80,4.81,4.82,4.83,4.84,4.85,4.86,4.87,4.88,4.89,4.90,4.91,4.92,4.93,4.94
        };
    
    }
  */

  /*
  public static final class Tester {

      //public static void main(String args[]) {
      //    doBaja();
      //}
      public static final void doBaja() {
        StringBuilder sb1 = new StringBuilder(1024);
        StringBuilder sb2 = new StringBuilder(1024);
        StringBuilder sb3 = new StringBuilder(1024);
        double r = 0.;
        double a,b;
        for (int ii = 0; ii < 120; ii++) {
          r += 1.0;
          a = 1.0134*Math.log10(r/100.)+ 0.0025*(r-100.) + 3.0;   // mex valley
          b = 1.1319*Math.log10(r/100.)+ 0.0017*(r-100.) + 3.0;  // mex sierra
          System.out.printf( "%4.0f %4.2f %4.2f\n", r, a, b);
          sb1.append(String.format("%4.0f",r)).append(" ");
          if ( ii > 0 && ii%20 == 19) sb1.append("\\\n"); 
          sb2.append(String.format("%3.2f", a)).append(" ");
          if ( ii > 0 && ii%20 == 19) sb2.append("\\\n"); 
          sb3.append(String.format("%3.2f",b)).append(" ");
          if ( ii > 0 && ii%20 == 19) sb3.append("\\\n"); 

        }
        System.out.println(sb1);
        System.out.println(sb2);
        System.out.println(sb3);
      }

      public static void main(String args[]) {
        RichterMlMagMethod2 ml = new RichterMlMagMethod2();
        //ml.setProperties("C:\\Users\\Allan Walter\\Jiggle\\scedc\\bcRichterMlMagMethod2.props");
        ml.initRegionDependentProps();
        ml.setInterpolate(false);
        ml.setUseClosestDistCorr(false);
        //ml.roundUp = true;
        //System.out.printf("-- ceiling to LARGER corr[i] (OLD BEHAVIOR) --%n");
        //System.out.printf("RichterML2  2.701 =%6.3f%n", ml.calcChannelMagValue(  5.0, 20.000, 0., false));
        //System.out.printf("RichterML2  0.797 =%6.3f%n", ml.calcChannelMagValue( 23.0,  0.125, 0., false));
        //System.out.printf("RichterML2  3.377 =%6.3f%n", ml.calcChannelMagValue( 32.0, 18.945, 0., false));
        //System.out.printf("RichterML2  3.676 =%6.3f%n", ml.calcChannelMagValue( 43.0, 15.000, 0., false));
        //System.out.printf("RichterML2  3.777 =%6.3f%n", ml.calcChannelMagValue( 77.0,  9.491, 0., false));
        //System.out.printf("RichterML2  3.000 =%6.3f%n", ml.calcChannelMagValue( 100.,  1., 0., false));
        //System.out.printf("RichterML2  4.157 =%6.3f%n", ml.calcChannelMagValue(308.0,  1.437, 0., false));
        //System.out.printf("RichterML2  4.070 =%6.3f%n", ml.calcChannelMagValue(600.0,  0.148, 0., false));
        //ml.roundUp = false;
        ml.setUseClosestDistCorr(false);
        System.out.printf("-- floor to SMALLER corr[i-1] (NEW BEHAVIOR) --%n");
        System.out.printf("RichterML2  2.701 =%6.3f%n", ml.calcChannelMagValue(  5.0, 20.000, 0., false));
        System.out.printf("RichterML2  0.797 =%6.3f%n", ml.calcChannelMagValue( 23.0,  0.125, 0., false));
        System.out.printf("RichterML2  3.377 =%6.3f%n", ml.calcChannelMagValue( 32.0, 18.945, 0., false));
        System.out.printf("RichterML2  3.576 =%6.3f%n", ml.calcChannelMagValue( 43.0, 15.000, 0., false));
        System.out.printf("RichterML2  3.777 =%6.3f%n", ml.calcChannelMagValue( 77.0,  9.491, 0., false));
        System.out.printf("RichterML2  3.000 =%6.3f%n", ml.calcChannelMagValue( 100.,  1., 0., false));
        System.out.printf("RichterML2  4.157 =%6.3f%n", ml.calcChannelMagValue(308.0,  1.437, 0., false));
        System.out.printf("RichterML2  4.070 =%6.3f%n", ml.calcChannelMagValue(600.0,  0.148, 0., false));
        System.out.printf("-- CLOSEST corr (LOCALMAG BEHAVIOR) --%n");
        ml.setUseClosestDistCorr(true);
        System.out.printf("RichterML2  2.701 =%6.3f%n", ml.calcChannelMagValue(  5.0, 20.000, 0., false));
        System.out.printf("RichterML2  0.997 =%6.3f%n", ml.calcChannelMagValue( 23.0,  0.125, 0., false));
        System.out.printf("RichterML2  3.377 =%6.3f%n", ml.calcChannelMagValue( 32.0, 18.945, 0., false));
        System.out.printf("RichterML2  3.676 =%6.3f%n", ml.calcChannelMagValue( 43.0, 15.000, 0., false));
        System.out.printf("RichterML2  3.877 =%6.3f%n", ml.calcChannelMagValue( 77.0,  9.491, 0., false));
        System.out.printf("RichterML2  3.000 =%6.3f%n", ml.calcChannelMagValue( 100.,  1., 0., false));
        System.out.printf("RichterML2  4.257 =%6.3f%n", ml.calcChannelMagValue(308.0,  1.437, 0., false));
        System.out.printf("RichterML2  4.070 =%6.3f%n", ml.calcChannelMagValue(600.0,  0.148, 0., false));
        ml.setInterpolate(true);
        // Need new interp values below since default array changed
        System.out.printf("%n-- INTERPOLATE=TRUE --%n");
        System.out.printf("RichterML2  2.701 =%6.3f%n", ml.calcChannelMagValue(  5.0, 20.000, 0., false));
        System.out.printf("RichterML2  0.917 =%6.3f%n", ml.calcChannelMagValue( 23.0,  0.125, 0., false));
        System.out.printf("RichterML2  3.457 =%6.3f%n", ml.calcChannelMagValue( 32.0, 18.945, 0., false));
        System.out.printf("RichterML2  3.636 =%6.3f%n", ml.calcChannelMagValue( 43.0, 15.000, 0., false));
        System.out.printf("RichterML2  3.847 =%6.3f%n", ml.calcChannelMagValue( 77.0,  9.491, 0., false));
        System.out.printf("RichterML2  3.000 =%6.3f%n", ml.calcChannelMagValue( 100.,  1., 0., false));
        System.out.printf("RichterML2  4.237 =%6.3f%n", ml.calcChannelMagValue(308.0,  1.437, 0., false));
        System.out.printf("RichterML2  4.068 =%6.3f%n", ml.calcChannelMagValue(600.0,  0.148, 0., false));
      }
  }
*/
}
