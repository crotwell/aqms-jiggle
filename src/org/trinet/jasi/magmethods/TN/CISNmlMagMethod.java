package org.trinet.jasi.magmethods.TN;

import java.util.*;
import org.trinet.filters.*;
import org.trinet.jasi.*;
import org.trinet.jasi.magmethods.*;
import org.trinet.util.MathTN;
import org.trinet.util.Format;

/**
 * Methods for ML (local magnitude) calculation.<p>
 *
 * Overrides the distance correction of the standard Richter ML. <br>
 * A0(r) Distance correction is based on an emperical relationship for 
 * California derived by Robert Uhrhammer UCB, where r is the
 * hypocentral "slant" distance. Channel epicentral map distance is used 
 * to determine a distance range based on channel magnitude for inclusion
 * in the summary magnitude calculation. <p>
 *
 * The method can use either corrected or uncorrected Wood-Anderson amplitudes
 * (types WAS, WAC, and WAU)
 * @see AmpType()
 */
public class CISNmlMagMethod extends MlMagMethod {

    // Uhrhammer's polynomial scalar's
    private static final double [] tp =
        new double [] {+0.056, -0.031, -0.053, -0.080, -0.028, +0.015};

    private static double a = 0.;
    private static double b = 0.;

    static {
        double l_r0 = MathTN.log10(8.0);
        double l_r1 = MathTN.log10(500.);
        b =  2.0 / (l_r1 - l_r0);
        a = -1.0 - b * l_r0;
    }

    public CISNmlMagMethod() {
        // Magnitude type subscript. For example: for "Ml" this would equal "l".
        magSubScript = MagTypeIdIF.ML;

        // String describing the magnitude method
        methodName = "CISNml";

        // set waveform filter to synthetic Wood-Anderson with a Highpass Butterworth prefilter
        setWaveformFilter( new WAHighPassFilter() );

        // cutoff dist
        setMinDistance(30.0);
        setMaxDistance(500.0); // NOTE Uhrhammer set at 500 km
        setMinSNR(8.0);        // Kate Hutton 4/2002

        setRequireCorrection(true);
    }

    // punt here for now - perhaps deprecate this method -aww
    public void configure(int src, String location, String section) {
        setProperties(location); // location like a filename url
    }

    public int getSummaryMagValueStatType() { return MEDIAN_MAG_VALUE; }

    // Override checks period range and/or peak amp min,max threshold?
    public boolean isValid(MagnitudeAssocJasiReadingIF jr) {
        if (! super.isValid(jr)) {
            //System.out.println("CISNml DEBUG not onScale: ");
            return false;
        }
        Amplitude amp = (Amplitude) jr;
        if ( amp.period.isValidNumber() ) { // screen by period -aww 03/13/2007 
            double d = amp.period.doubleValue();
            if (d < .05 || d > 5.0) {
                //System.out.println("CISNml DEBUG bad period: " + d);
                return false; // was min 0.08, but some obs are smaller - aww 05/22/2007
            }
        }
        if (! amp.value.isValidNumber() ) return false;

        // Uhrhammer's screen of amp by min, max value -aww 05/01/2007
        // amplitude threshold for HH is 0.03 to 65 cm and for HL is .3 to 12000 cm
        double d = Math.abs(amp.getValueAsCGS());
        if (! Double.isNaN(d)) {
        // Seems too strict - plenty of traces with signal energy at low amps, SNR matter more
          if (jr.getChannelObj().isVelocity()) { // HH is 0.001 to 100 cm, seem ok at 100. -aww
            if ( d < 0.001 || d > 100.0) {
                //System.out.println("CISNml DEBUG bad peak: " + d);
                return false; 
            }
          }
          else if (jr.getChannelObj().isAcceleration()) { // HL is .001 to 12000 cm -aww
            if ( d < 0.001 || d > 12000.0) {
                //System.out.println("CISNml DEBUG bad peak: " + d);
                return false;
            }
          }
        }

        return true;
    }

    public boolean isValidForSummaryMag(MagnitudeAssocJasiReadingIF jr) {
        // Do this constraint first for summary mag -aww
        // if(((Amplitude)jr).snr.doubleValue()<10.) System.out.println("CISNml DEBUG isValidForSummaryMag bad SNR");
        return (isValid(jr) && ((Amplitude)jr).snr.doubleValue() >= 10.);
    }

    /**
     * Override of method in default Richter ML method differs only in that it
     * retrieves the HYPOCENTRAL distance not epicentral distance 
     * from the input Amplitude and uses different A0 function.
     */ 
    public double calcChannelMagValue(Amplitude amp) throws WrongDataTypeException {
      if (! isIncludedReadingType(amp) ) {
          throw new WrongAmpTypeException(
            "Wrong Amp type for MagnitudeMethod " +methodName+
            " : " +amp.toNeatString() // +AmpType.getString(amp.type)
          );
      }
      if (amp.isClipped()) { // should we throw WrongDataTypeException?
          throw new WrongAmpTypeException(
            "Amp isClipped for MagnitudeMethod "+methodName+
            " : " +amp.toNeatString()
          );
      }
      double value = -9.; // changed from 0. to -9. to make it obvious to user -aww 08/12/2004
      try {
        Double corr = getMagCorr(amp);
        value = calcChannelMagValue(
                        amp.getSlantDistance(), // use slant distance (dist to 0 elev) - aww 06/15/2004
                        getAmpValue(amp),
                        (corr == null)? 0. : corr.doubleValue()
                );
      }
      catch (WrongDataTypeException ex) {
        ex.printStackTrace();
      }
      return value;
    }

    /**
     * Return the distance cutoff appropriate for the method. Amplitudes from
     * stations father then this will not be included in the summary magnitude.
     * Because it depends on the magnitude the cutoff can only be applied in a
     * second pass through the amplitude readings. In the
     * southern California magnitude scheme the distance cutoff is an empirical
     * relationship derived by Kanamori. It is given by:<p>
     *<tt>
     * cutoff = 170.0 * magnitude - 205.0 (all distances are in km)
     *</tt>
     * The formula can yields 0.0 at a magnitude of 1.21, so a minimum cutoff is
     * enforced. This method will never yield a value less then 30.0km. <p>
     * The cutoff distance is compared with station's "epicentral" distance. 
     * The difference between epicentral and slant distances is typically < 2 km,
     * for example, using epiDistance=minCutoff (30. km) and a solution 
     * depth of 10 km, the slantDistance is 31.6 km).
     */
    public double getDistanceCutoff(double magnitude) {
        //Implement property to disable mag filter and return default MaxDistance
        if ( props.getBoolean("disableMagnitudeDistanceCutoff") ) 
           return super.getDistanceCutoff(magnitude);

        // Note in TriMag dist km is compared with station's "slant" distance 
        // here we treating it as epicentral (difference typical < 2 km) -aww 
        // mag,cut (1,-35) (2,135) (3,305) (4,475)
        double dist = Math.max( getMinDistance(), (170.0 * magnitude) - 205.0 ) ;
        return Math.min(dist, getMaxDistance() );
    }


    /** Override the distance correction of the Richter ML. <br>
     *  Derived from the fit of channel dML's for events recorded by 
     *  both northern and southern California networks (Uhrhammer).
     *  Input is the hypocenter "slant" distance km (not epicentral distance).
    */
    public double distanceCorrection(double rdist) {

        double mlogA0 = -9.; // rdist < 1 km or > 500 km

        if( (rdist < 0.1) ) { 

          mlogA0 = -9.; // rdist < 1 km 

        }
        else if( (rdist <= 8. ) ) { // linear extrapolation

          // linear extrapolation of average slope between 8 km and 60 km
	  //slope = ( 2.6182-1.5429)/(log10(60.)-log10(8.)) = 1.22883;
	  mlogA0 = 1.5429 + 1.22883 * (MathTN.log10(rdist) - 0.90309);

        }
        else if ( rdist <= 500. ) { // Chebychev polynomial expansion

          mlogA0 = logA0(rdist) + 0.0054;  // added constant

          for (int j = 0; j < 6; j++) {
            mlogA0 += tp[j] * cheb(j, z(rdist));
            //System.out.println(rdist + " " + j + " " + tp[j] + " " + cheb(j, z(rdist)));
          }

        }

        return mlogA0;

    }

    private double logA0(double rdist) {
        return 1.11 * MathTN.log10( rdist ) + 0.00189 * rdist + 0.591;
    }
    
    // Chebyshev Polynomial
    private double cheb(int n, double x) {
        return Math.cos((double)(n+1) * Math.acos(x));
    }

    // translate scale from r to z
    private double z(double r) {
        return (a + (b * MathTN.log10(r)));
    }

    /* test code below
    public static final class Tester {
        public static void main(String args[]) {
            CISNmlMagMethod ml = new CISNmlMagMethod();
            Format f = new Format("%5.3f");
            if (args.length > 0) 
              System.out.println (args[0] + " km A0corr = " + ml.distanceCorrection(Double.parseDouble(args[0])));
            else {
              for (int i = 0; i < 502; i++) {
                System.out.println (i + " " + f.form(ml.distanceCorrection(i)));
              }
            }
        }
    }
    */

} // CISNmlMagMethod
