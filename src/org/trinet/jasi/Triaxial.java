package org.trinet.jasi;
import java.util.*;

import org.trinet.util.StringList;

/**
 * Defines legal SEED component codes for seismometers. There is limited support
 * for non-seismic instruments (e.g. barometers, thermometers, strain meters, etc.) 
 * Class can be used to check that a code is legal
 * or to build the set of triaxial codes if one is known.
 *
 * This method will work for ALL valid SEED channel types. It does not check that
 * the components actually exist. For example, if you pass it "BK.XYZ.SHZ" it will return
 * the set: "NN.XYZ.SHZ", "NN.XYZ.SHN", "NN.XYZ.SHE". But it is possible that the
 * horizontal components were not installed and don't exist.<P>
 * 
 * Also, non-standard orientation may be an issue. For example, a downhole instrument
 * may have components 'Z12' rather than 'ZNE' or '123'. 
 *
The characters in the string should be UPPERCASE, to match the convention, however
the comparison with the components is NOT case sensitive. If any part of the component code is not
described in the comparitor string array it will fall to the end of the sort
order but matching fields will still sort correctly.<p>

The default order is the order in which band, instrument and orientation codes
are described in the SEED Reference Manuel, v2.3, Appendix A, pg. 114.

@author: Doug Given

*/
// 4/12/07 - DDG - Added "N" as a valid inst type and groups "SD", "456", "789"
//
// NOTE: Could develop logic around ChannelLocation instances instead of complete Channel instances
//       since interested only in the name and location -aww
//
public class Triaxial {

    /** The default component groups based on Seed instrument code convention.  
     * Note that other grouping may be valid, e.g. "Z23".
     * */
    protected static String groups[] = {
      "ZNE12",
      "ABC",   //
      "123",   // non-traditional orientations
      "456",   //
      "789",    //
      "UVW",   // optional comps
      "TR",    // Transverse, Radial
      "SD",    // Speed, Direction (Wind) 
      "OID"    // Outside, Inside, Downhole (thermometer only?)
    };

    // list of legal types (Includes SEED v 2.4 revisions)
    protected static StringList bandList   = new StringList("E S H B M L V U R A O W X D", " ");
    protected static StringList instList   = new StringList("H L N G M P", " ");
    protected static StringList orientList = new StringList("Z N E A B C T R 1 2 3 4 5 6 U V W", " ");

//
    public Triaxial() {}

    /**
     * Return an array of 3-char component codes representing all orientations of
     * the component passed as 'comp'. Ignores case of 'comp' always returns upper case.
     * Returns null if string is not a legal component name. */
    public static String[] getComponentsAsArray(String comp) {
      StringList sl = getComponentsAsStringList(comp);
      if (sl == null) return null;
      return sl.toArray();
    }

    /* * Return true if both input Channels are not null and belong to same triaxial grouping, false otherwise.*/
    public static boolean sameTriaxialAs(Channel ch1, Channel ch2) {
        if (ch1 == null || ch2 == null || ! ch1.sameTriaxialAs((ChannelIdentifiable) ch2) ) return false;
        return Triaxial.sameGroup(ch1.getChannelObj().getSeedchan(), ch2.getChannelObj().getSeedchan());
    }

    /* * Return true if both seedchan are not null and belong to same triaxial grouping, false otherwise.*/
    public static boolean sameGroup(String seedchan1, String seedchan2) {
      String g1 =getGroup(seedchan1); 
      String g2 =getGroup(seedchan2); 
      return (g1 != null && g2 != null && g1 == g2);
    }

      /**
       * Return an array of 3-char component codes representing all orientations of
       * the component passed as 'comp'. Ignores case of 'comp' always returns upper case.
       * Returns null if string is not a legal component name.  */
    public static StringList getComponentsAsStringList(String comp) {

      // Test removed 12/8/05 - NC has illegal location codes
      //if (!isLegal(comp)) return null;   // a legal component type?
      
      String prefix = comp.substring(0,Math.min(comp.length(),2)).toUpperCase();  // 1st 2 chars

      StringList newcomps = new StringList();
      String grp = getGroup(comp);
      //if (grp == null) return null;   // nada
      if (grp == null) { // always return the input comp -aww 2013/02/20
          newcomps.add(comp);
          return newcomps;
      }

      for (int i = 0; i<grp.length(); i++) {
        newcomps.add(prefix+grp.substring(i,i+1));
      }
      return newcomps;
    }

    /** Return a StringList of all component suffixes for a component of this type.
     * If comp is longer than 1 character the LAST character is used.
     * For example if comp = "N" the StringList will contain "ZNE".
     * Returns null if there is no match.<p>
     */
    public static String getGroup(String comp) {
      if (comp == null || comp.length() < 1) return null;
      int l = comp.length();
      String type = comp.substring(l-1, l).toUpperCase();   // last char if longer than 1 char
      for (int i = 0; i< groups.length; i++ ) {
        if (groups[i].indexOf(type) > -1 ) {
            //System.out.println("group: " + groups[i]);
            return groups[i] ;
        }
      }
      return null;
    }
/** Return true if the 'comp' is a legal SEED component code. */
    public static boolean isLegal(String comp) {
      return (comp.length() >= 3 &&
              bandIsLegal(comp.substring(0,1)) &&
              instIsLegal(comp.substring(1,2)) &&
              orientIsLegal(comp.substring(2,3)) );
    }

/** Given a Channelable object return a ChannelableList that represents all components
 * (SEED channel names) in that set. For example: if chan's channel name is
 * "NP.ABC.BHE.01" this method will return Channelable objects with names
 *  "NP.ABC.BHZ.01", "NP.ABC.BHE.01", "NP.ABC.BHN.01". This method does not check
 * that the returned components actually exist.
 * Returns empty ChannelableList if 'chan' is not a valid Channelable.<p>
 *
 * The LatLonZ and distance values of the original Channelable are copied to the
 * resulting set. <p>
 *
 * Note that since the 'channel' part of a Channel name does NOT follow any standard
 * convention it is set identical to the SEED component name.
 *  */
    public static ChannelableList getSet(Channelable ch) {
      ChannelableList chList = new ChannelableList();
      Channel inChan = ch.getChannelObj();
      // get the list of components
      String comp[] = getComponentsAsArray(inChan.getSeedchan());
      if (comp == null || comp.length < 1) return chList; // empty array 

      // Build the channels, note difference below from the Channel.copy(ch) method
      // i.e. no copying of depth, dateRange, sampleRate or the maps of corrections/gains
      Channel newChan = null;
      for (int i = 0; i < comp.length; i++ ) {
        newChan = Channel.create();
        newChan.setChannelName(inChan.getChannelName()); // NOTE: copies inChan's location,channelsrc,auth,subsource
        newChan.setChannel(comp[i]);  // don't know what the local channel should be - set to SEED
        newChan.setSeedchan(comp[i]);

        newChan.setLatLonZ(inChan.getLatLonZ());
        newChan.setDistance(inChan.getDistance());
        newChan.setHorizontalDistance(inChan.getHorizontalDistance()); // added 09/03/2004 map distance should be used -aww

        chList.add(newChan);
      }
      return chList;
    }


    public static boolean bandIsLegal( String band ) {
      return bandList.contains(band.toUpperCase());
    }
    public static boolean instIsLegal( String inst ) {
      return instList.contains(inst.toUpperCase());
    }
    public static boolean orientIsLegal( String orient ) {
     return orientList.contains(orient.toUpperCase());
    }
    //  TEST
/*
    public static void main (String args[]) {
      ArrayList list = new ArrayList();
      list.add("BHZ");
      list.add("EHZ");
      list.add("HHE");
      list.add("HHZ");
      list.add("HHN");
      list.add("hle");
      list.add("HLN");
      list.add("UL3");
      list.add("ABN");
      list.add("UL2");
      list.add("ABZ");
      list.add("ABE");
      list.add("UL1");
      list.add("USFL1");
      list.add("EHG");
      list.add("U");

      for (int i=0; i<list.size();i++){
        System.out.println(list.get(i).toString() + ":" + Triaxial.getComponentsAsStringList((String)list.get(i)) );
      }

    }
*/

}

