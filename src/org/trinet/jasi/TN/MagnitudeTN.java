//TODO: MAGNITUDE getBy IMPLEMENTATION NEEDS EQUIVALENT TO AMP,CODA, TYPE FILTER .
//TODO: Migrate common "reading" method function to abstracted TN class
//
package org.trinet.jasi.TN;

import java.text.*;
import java.sql.*;
import java.util.*;

import org.trinet.jasi.*;
import org.trinet.jasi.coda.*;
import org.trinet.jdbc.*;
import org.trinet.jdbc.table.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.util.EpochTime;
import org.trinet.util.Utils;

/**
 * Component of the Java Seismic Abstraction Layer.
 * This implementation interfaces
 * to the Berkeley/Caltech/USGS schema in Oracle. <p>
 * All data members are DataObjects rather than primative types so they can
 * handle the concept of "nullness". The database can contain null values for
 * certain fields. Also applications may not want to write a "bogus" value like 0
 * into the database when when the real value is not known.
 */

/*
  In the NCDC schema you would NEVER delete or update an existing 'NetMag' row.
  You would only create a new one and set 'prefmag' in Origin and Event to the
  new row.
 */
public class MagnitudeTN extends Magnitude implements DbMagnitudeAssocIF, DbReadableJasiObjectIF {

    //
    //"Select "+NetMag.QUALIFIED_COLUMN_NAMES+
    //
    protected static final String SQL_PREFIX_BY_MAGID_STR =
         // netmag.nobs implemented -aww 2010/08/19 
         "SELECT n.magid,n.orid,n.magnitude,n.magtype,n.auth,n.subsource,n.magalgo,n.nsta,n.nobs,n.uncertainty,n.gap,n.distance,n.quality,n.rflag" +
         ",UTIL.getWhoNetMag(n.magid),n.lddate,0,0 FROM NETMAG n WHERE (magid=?)";

    protected static final String SQL_PREFIX_BY_EVID_ALL_STR =
         // netmag.nobs implemented
        "SELECT n.magid,n.orid,n.magnitude,n.magtype,n.auth,n.subsource,n.magalgo,n.nsta,n.nobs,n.uncertainty,n.gap,n.distance,n.quality,n.rflag,"+
        "UTIL.getWhoNetMag(n.magid) who,n.lddate,(select p.magid from eventprefmag p where p.evid=o.evid and p.magtype=n.magtype),o.prefmag " + 
        "FROM ORIGIN o, NETMAG n WHERE (n.orid=o.orid) AND (o.evid=?)";
    

    protected static final String SQL_PREFIX_BY_EVID_STR =
         // netmag.nobs implemented
         "SELECT n.magid,n.orid,n.magnitude,n.magtype,n.auth,n.subsource,n.magalgo,n.nsta,n.nobs,n.uncertainty,n.gap,n.distance,n.quality,n.rflag" +
        ",UTIL.getWhoNetMag(n.magid),n.lddate,0,0 FROM EVENT e, NETMAG n WHERE (n.orid=e.prefor) AND (e.evid=?)";

    protected static final String SQL_PREFIX_BY_PREFTYPE =
         "SELECT n.magid,n.orid,n.magnitude,n.magtype,n.auth,n.subsource,n.magalgo,n.nsta,n.nobs,n.uncertainty,n.gap,n.distance,n.quality,n.rflag" +
        ",UTIL.getWhoNetMag(n.magid),n.lddate,p.magid,o.prefmag FROM Origin o, NETMAG n, EVENTPREFMAG p WHERE " +
        " (o.orid=n.orid) and (n.magid=p.magid) AND (n.magtype=p.magtype) and (p.evid=?)";
    /*
      Here are protected data members used to support this particular
      implimentation of the abstract layer
     */

    // class mapping to the NCEDC schema NETMAG Table
    // NOTE: to avoid aliasing, you would need to "clone" some of the row's returned dataObjects 
    //protected NetMag magRow;

    protected DataLong commid = new DataLong();
    protected DataLong orid   = new DataLong();
    protected DataLong pmagid = new DataLong();
    protected DataLong omagid = new DataLong();

    /** True if this Solution was orginally read from the dbase. Used to know if
        a commit requires an 'insert' or an 'update' */
    boolean fromDbase = false;
    boolean commitMagChanged = false;

    protected static boolean debug = false;

    //private JasiDbReader jasiDataReader = new JasiDbReader(this); // if not static 
    //Can be static so long as parseResultSet() does not depend on state (like joinType in AmplitudeTN)
    //private static JasiDbReader jasiDataReader = new JasiDbReader();
    private static JasiDbReader jasiDataReader = new JasiDbReader("org.trinet.jasi.TN.MagnitudeTN");

    /*
    static {
      //jasiDataReader.setFactoryClassName(getClass().getName());
      setDebug(JasiDatabasePropertyList.debugTN);
      if (JasiDatabasePropertyList.debugSQL) jasiDataReader.setDebug(true);
    }
    */

    /**
     *  Create a Magnitude object.
     */
    public MagnitudeTN() {
        if (jasiDataReader != null) jasiDataReader.setDebug(JasiDatabasePropertyList.debugSQL);
        setDebug(JasiDatabasePropertyList.debugTN);
    }

    public static void setDebug(boolean tf) {
        debug = tf;
        if (debug && jasiDataReader != null) jasiDataReader.setDebug(true);
    }

    public Object clone() {
        MagnitudeTN magTN = (MagnitudeTN) super.clone();
        magTN.commid = (DataLong) this.commid.clone();
        magTN.orid   = (DataLong) this.orid.clone();
        //magTN.magRow = (Netmag) this.magRow
        // add the db reader if it becomes "instance" attribute
        return magTN;
    }

    /*
    private static void associateList(Solution sol,  List mags) {
        if (sol.getMagList().isEmpty()) {
            sol.fastAssociateAll(mags); // does not filter input elements
        }
        else {
            sol.associateAll(mags); // filters input for dupes or nulls
        }
    }
    */

    /**
     * Returns the Magnitude with this identifier from the DataSource.
     * Returns null if no Magnitude is found.<p>
     * Uses the default DataSource.
     */
    public JasiCommitableIF getById(Object magId) {
      if (magId instanceof Number)
          return getById(((Number)magId).longValue());
      else if (magId instanceof DataNumber)
          return getById(((DataNumber)magId).longValue());
      else
        return null;
    }

    public Magnitude getById(long magId) {
      return getByMagId(magId);
    }

    /**
     * Returns the Magnitude with this MagId number from the
     * data source. Returns null if no Magnitude is found.
     */
    protected Magnitude getByMagId(long magId) {
        // Must do join to find prefor and prefmag of the given evid
        //StringBuffer sb = new StringBuffer(512);
        //sb.append("Select netmag.* from netmag WHERE (netmag.magid=");
        //sb.append("Select ").append(NetMag.QUALIFIED_COLUMN_NAMES);
        //sb.append(",UTIL.getWhoNetMag(Netmag.magid) from Netmag WHERE (Netmag.magid=").append(magId).append(")");
        //ArrayList aList = (ArrayList) ((JasiDbReader) getDataReader()).getBySQL(sb.toString());
        ArrayList aList = null;
        PreparedStatement psByMagid = null;
        try {
            Connection conn = DataSource.getConnection();
            psByMagid = conn.prepareStatement(SQL_PREFIX_BY_MAGID_STR);
            psByMagid.setLong(1, magId);
            aList = (ArrayList) ((JasiDbReader) getDataReader()).getBySQL(conn, psByMagid);
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        }
        finally {
            JasiDatabasePropertyList.debugSQL(SQL_PREFIX_BY_MAGID_STR, psByMagid, debug);
            Utils.closeQuietly(psByMagid);
        }

        return (aList == null || aList.isEmpty()) ? null : (Magnitude) aList.get(0);
    }

    /**
     * Returns from the default DataSource all Magnitudes
     * associated with preferred origin of the Solution (event)
     * whose identifier (id) matches the input argument <i>evid</i>.
     * Returns null if no Magnitudes are found.
     */
    public Collection getBySolution(long evid) {
        Solution sol = Solution.create().getById(evid);
        // check for no such solution in DataSource
        return (sol == null) ? null : doGetBySolution(sol, null, true, SQL_PREFIX_BY_EVID_STR);
    }

    /**
     * Returns from the default DataSource all Magnitudes
     * associated with the preferred origin of the input Solution.
     * Returns null if no Magnitudes are found.
     */
    public Collection getBySolution(Solution sol) {
        return doGetBySolution(sol, null, true, SQL_PREFIX_BY_EVID_STR);
    }

    /**
     * Returns from the input Connection DataSource all Magnitudes
     * associated with the preferred origin of the input Solution.
     * Returns null if no Magnitudes are found.
     */
    public Collection getBySolution(Solution sol, String [] typeList) {
        return doGetBySolution(sol, typeList, true, SQL_PREFIX_BY_EVID_STR);
    }

    public Collection getBySolution(Solution sol, String [] typeList, boolean assoc ) {
        return doGetBySolution(sol, typeList, assoc, SQL_PREFIX_BY_EVID_STR);
    }

    public Collection getAllBySolution(Solution sol) {
        return getAllBySolution(sol, null);
    }

    // Used for History lookup (doesn't associate input sol) 
    public Collection getAllBySolution(Solution sol, String [] typeList) {
        // NOTE: Does not associate with input solution, need to preserve mixed origins from db in Magnitudes
        // and does not change contents/state of input sol, just assigns input without synching orid or lists.
        // returned list can then be used to compare db prefs and associations with those in input sol.
        ArrayList aList = (ArrayList) doGetBySolution(sol, typeList, false, SQL_PREFIX_BY_EVID_ALL_STR);
        // Below not using assign(sol) or Solution assignToUnassociated(List aList)
        // which assigns the sol to each mag's reading lists and also does prior association check 
        int count = aList.size();
        for (int idx =0; idx < count; idx++) {
            ((Magnitude)aList.get(idx)).sol = sol;
        }
        return aList;
    }
    // Used by Jiggle menubar dump menu for prefmag of type lookup (doesn't associate input sol) 
    public Collection getPrefMagOfTypeBySolution(Solution sol) {
        ArrayList aList = (ArrayList) doGetBySolution(sol, null, false, SQL_PREFIX_BY_PREFTYPE);
        int count = aList.size();
        for (int idx =0; idx < count; idx++) {
            ((Magnitude)aList.get(idx)).sol = sol;
        }
        return aList;
    }


    private Collection doGetBySolution(Solution sol, String [] typeList, boolean assoc, String sqlStr) {

        long evid = ((SolutionTN) sol).getEvidValue();
        // Must do join to find prefor and prefmag of the given evid
        if (evid <= 0l) return null; // not valid, don't query -aww

        StringBuffer sb = new StringBuffer(1024);

        sb.append(sqlStr);
        sb.append(makeTypeClause(typeList));
        sb.append(" ORDER BY n.magtype desc, n.lddate desc, n.magid desc");

        ArrayList aList = null;
        PreparedStatement psByEvid = null;
        final String sql = sb.toString();
        try {
            Connection conn = DataSource.getConnection();
            psByEvid = conn.prepareStatement(sql);
            psByEvid.setLong(1, evid);
            aList = (ArrayList) ((JasiDbReader) getDataReader()).getBySQL(conn, psByEvid);
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        }
        finally {
            JasiDatabasePropertyList.debugSQL(sql, psByEvid, debug);
            Utils.closeQuietly(psByEvid);
        }

        if (aList == null) return null;
        int dbCount  = aList.size();
        //System.out.println("getBySolution: mags found ="+dbCount);
        if (dbCount < 1) return aList; // no data

        // option to associate elements of list with the input argument
        // otherwise returned list elements are orphaned from context
        if (assoc) sol.associate(aList);
        /*
        // Could reset prefmag here, but do we use sol.magnitude or solTN prefmag ?
        // sol.magnitude might be more up to date since sol was last loaded from db.
        // long prefMagId = sol.getPreferredMagId(); // get current setting
        // if (prefMagId <= 0l) { // if none, set it from last db value ?
        //   prefMagId = ((SolutionTN)sol).getPrefmagValue();
        //   if (prefMagId > 0l) sol.setPreferredMagFromIdOnList(prefMagId); //set new instance reference
        // }
        */
        return aList;
    }

    // Must do 3-table join to find ALL magnitudes for ALL origins for a specified evid
    //   String sql = "Select "+NetMag.QUALIFIED_COLUMN_NAMES+
    //     " from event, origin, netmag " +
    //     " WHERE (origin.evid = event.evid) and " +
    //     " (netmag.orid = origin.orid) and "+
    //     " (event.evid = " + id + ")" ;

    protected String makeTypeClause(String[] typeList) {
        if (typeList == null || typeList.length == 0) return "";
        StringBuffer sb = new StringBuffer();
        sb.append(" and (");
        for (int i = 0; i < typeList.length; i++) {
            if (i > 0) sb.append(" or ");
            sb.append("(netmag.MAGTYPE like '").append(typeList[i]).append("')");
        }
        sb.append(")");
        return sb.toString();
    }

    /**
     * Sets the identifying DataSource id of this instance
     * to one assigned from a database sequence and returns it value.
     */
    public long setUniqueId() {
        //System.out.println("MagnitudeTN DEBUG setUniqueId >>>> id  in: " + StringSQL.valueOf(magid));
        long newId = getNextId();
        //magid.setMutable(true);
        magid.setValue(newId);
        //System.out.println("MagnitudeTN DEBUG setUniqueId >>>> id out: " + StringSQL.valueOf(magid));
        return newId;

    }
    /** Returns the next identifying magnitude sequence number from the datasource. */
    public long getNextId() {
        return SeqIds.getNextSeq(DataSource.getConnection(),
                   org.trinet.jdbc.table.NetMag.SEQUENCE_NAME);
    }
    /**
     * Return the magid DataObject identifier for this instance.
     */
    public DataLong getMagid() {
          return magid;
    }
    public long getMagidValue() {
       return (magid.isValidNumber()) ? magid.longValue() : 0l;
    }
    public DataLong getId() {
      return (DataLong) getIdentifier();
    }
    public boolean idEquals(long magid) {
      return idEquals(new Long(magid));
    }

    public void setMagidValue(long id) {
        //magid.setMutable(true);
        magid.setValue(id);
    }

    public boolean isEventPrefmag() {
       if (sol == null) return false;
       MagnitudeTN spmag = (MagnitudeTN) sol.magnitude;
       return (spmag != null && !spmag.hasChanged() && magid.longValue() == spmag.magid.longValue() );
    }
    public boolean isPrefmagOfType() {
       if (sol == null) return false;
       MagnitudeTN spmag = (MagnitudeTN) sol.getPrefMagOfType(this);
       return (spmag != null && !spmag.hasChanged() && magid.longValue() == spmag.magid.longValue());
    }
    public boolean isLastPrefmagId() {
       return ( magid.isValidNumber() &&
                (
                 pmagid.longValue() == magid.longValue() || ( isPrefmagOfType() && !hasChanged() || ((SolutionTN)sol).lastEvtPrefmag == magid.longValue())
                )
              );
    }
    public boolean isOriginPrefmagId() {
       return ( magid.isValidNumber() &&
                (
                 omagid.longValue() == magid.longValue() || ( sol != null && ((SolutionTN)sol).lastOrgPrefmag == magid.longValue() )
                )
              );
    }
    /**
     * Associate this Magnitude with input Solution.
     * Override of superclass sets the value of the origin
     * identifier (orid) for this instance to match that of
     * the input Solution.
     */
    public void associate(Solution sol) {
      super.associate(sol);
      synchWithAssocSolutionOrid();
    }
    /**
     * Assigns (affiliates) input Solution with this Magnitude.
     * Unlike associate, does not add this Magnitude instance
     * or its data to the internal Solution MagList collection.
     * Override of superclass sets the value of the origin
     * identifier (orid) for this instance to match that of
     * the input Solution.
     */
    public void assign(Solution sol) {
      super.assign(sol);
      synchWithAssocSolutionOrid();
    }
    private void synchWithAssocSolutionOrid() {
      // get orid, if null will assign one
      long id = ((SolutionTN)sol).getOridValue();
      long oldOrid = getOridValue();
      if (id != oldOrid) setOrid(id);
    }

    /** Reset null (unknown) data derived from location calculation. */
    public void initSolDependentData() {
      distance.setNull(true);
      gap.setNull(true);
    }

    /**
     * Returns true if all not-NULL fields are not-Null. Does not check other
     * constraints.  Does not check MAGID because that will be assigned from the
     * sequence if it is missing.  Does check nullness of: value, type,
     * authority and orid.  */
    protected boolean isSufficient() {
        return (
            value.isValid() &&
            subScript.isValid() &&
            authority.isValid() &&
            orid.isValid()
        ) ;
    }

    /*
      STATES:
      o  The class has a boolean deleteFlag.
      o  The fromDbase boolean tells us if we will 'insert' or 'update' on commit.
      o  If the individual DataObject.isUpdate() = true,  an update is necessary.

      Commit action matrix:

      fromDbase =          TRUE         FALSE
      deleteFlag=TRUE      dbaseDelete  noop
      isUpdate()=TRUE      update       insert
      isUpdate()=FALSE     noop         insert

     */

    //
    // amp,coda,phase commit() return true when isDeleted()
    // but mag returns false shouldn't all be false to be consistent? - aww
    //
    /* Write new NetMag row to the DataSource and write its amp or coda association rows.
       Note: DataSource.setWriteBackEnabled(true) must be set BEFORE data is read by query.
       If this instance is the preferred Magnitude, set it so in the database, i.e.
       set 'prefmag' in the Event table = magid.<p>
       Will not write a row and return 'true' if:<br>
        (isDeleted() || ! hasChanged() || ! getNeedsCommit())<br>
       @throws JasiCommitException if
       ((DataSource.isWriteBackEnabled() == false || !isSufficient()) or SQL error.
    */
    public boolean commit() throws JasiCommitException {

        boolean handEntered = algorithm.toString().toUpperCase().equals("HAND");

        if (!originDependent && !handEntered && !isDeleted()) { // added !isDeleted() condition to delete eventprefmag association below -aww 2009/05/13
            //if (debug)
              System.out.println("INFO: Magnitude (calc is not enabled for type, and it's not HAND entered or is deleted, commit is no-op for magid: "+
                      magid.longValue() + " : " + getTypeString());
            return true; // temporary kludge for BK Mw Mec problem -aww 11/14/2006
        }
        //The invoking SolutionTN updates the origin and event prefmag id after this method returns.
        if (!DataSource.isWriteBackEnabled())
          throw new JasiCommitException("Database is NOT write enabled.");

        if (debug) {
            System.out.println("DEBUG MagnitudeTN isDeleted:"+isDeleted()+" needsCommit:"+getNeedsCommit()+
                            " hasChanged:"+hasChanged()+" fromDbase:"+fromDbase + " isStale:"+ isStale); 
        }
        
        if (isDeleted()) {
            return doDeleteCommit();
        }

        if (handEntered) synchWithAssocSolutionOrid(); // force synch change -aww 2008/03/11

        if (! (getNeedsCommit() || hasChanged()) ) return true; // true/false logic fixed 01/13/2005

        // Handle case where new mag hand entered with no orid  -aww
        if (orid.isNull() && ((SolutionTN)sol).getOridValue() > 0l) {
          synchWithAssocSolutionOrid();
        }

        // Now test to see all not-null values assigned
        if (!isSufficient())
            throw new JasiCommitException("MALFORMED MAGNITUDE: subscript,value,auth, or orid is null.");

        // added below for debug, found eventprefmag with Jiggle subsource, 0 channels and 0 magnitude, find out why -aww 2010/06/17
        if (!algorithm.toString().toUpperCase().equals("HAND") && !hasNullType()) {
            if (value.doubleValue() == 0. && usedChnls.intValue() == 0) {
                System.out.println("ERROR: Magnitude is not HAND entered but its value and nobs are 0, commit FAILED for magid: "+
                      magid.longValue() + " : " + getTypeString());
                return false;
            }
        }

        // Stale commit dilemma:
        //   A stale mag committed has new magid and associated orid changed to the "new" orid,
        //   it no longer is tied to its derivation origin.
        if (isStale) {
          if ( ! staleCommitOk && ! handEntered) { // added "HAND" condition 8/20/2007 -aww
            System.out.println("ERROR: MagnitudeTN.commit(), STALE, RECALC NEEDED, COMMIT ABORTED, OLD MAGID: "+magid.longValue());
            //return false;  // may generate exception in Solution.commit() - removed, replaced with exception 02/17/2007 aww
            throw new JasiCommitException("ERROR: MagnitudeTN.commit(), STALE, RECALC NEEDED, COMMIT ABORTED, OLD MAGID: "+magid.longValue());
          }
          else {
            if (staleCommitNoop) {
              if (existsInDataSource()) {
                System.out.println("WARNING: MagnitudeTN.commit(), STALE, RECALC NEEDED, NO-OP, OLD MAGID: " + magid.longValue());
                return true; // do nothing, return success, preserve old prefmag mapping of Solution
              }
              // else force commit of stale data, it could be original hand edited data, not updatable by solve.
            }
          }
          System.out.println("WARNING: MagnitudeTN.commit(), STALE, RECALC NEEDED, NEW FROM OLD MAGID: " + magid.longValue());
          //BEWARE !!!
          //Stale commits may cause problems recovering valid readings and associations.
          //Commited stale magnitude and its readings are associated with a new orid, not their creation orid  
          synchWithAssocSolutionOrid(); // reset orid if it changes else mag keeps it old orid. - aww 06/21/2005
        }

        //If this magnitude is from database readings could exist in in database associated with the old magid,orid.
        //If this magnitude has no readings in its list, the new magid created will not be associated with readings 
        //Thus here force a lookup when missing supporting reading data.
        if (fromDbase && getReadingsCount() < 1) {
          System.out.println("WARNING: MagnitudeTN.commit(), NO READINGS, OLD MAGID: "+magid.longValue()
                  + " loading old magid associated readings from the database.");
          // force load to guarantee readings association with any new magid generated by insert below.
          // but should load action be a class property option? -aww 
          loadReadingList(false);
          ((JasiReadingList)getReadingList()).matchChannelsWithList(); // get lat,lon from MasterChannelList if any
          getReadingList().calcDistances(sol); // getAssociatedSolution() and calc distance and az
          //if (debug) System.out.println(">>> Magnitude readingList toNeatString()\n"+getReadingList().toNeatString(false));
        }

        //logic like in SolutionTN to decide UPDATE vs. INSERT row -aww
        commitMagChanged = hasChanged(); // save state before commit -aww 2008/02/29

        boolean status = false;
        if (hasChangedValue()) {
            status = dbaseInsert();
            if (debug) System.out.println("DEBUG MagnitudeTN dbaseInsert() returned status: " + status);
            if (status) status = commitReadingList(); // added here -aww 2010/06/30 only associate when newly inserted
        }
        else {
            status = dbaseUpdate();
            if (debug) System.out.println("DEBUG MagnitudeTN dbaseUpdate() returned status: " + status);
        }

        // Removed association here -aww 2010/06/30 instead only associate after dbaseInsert() 
        //if (status) status = commitReadingList(); // this may throw an SQLException

        if (!status)
          throw new JasiCommitException("Magnitude database insert or association failed for magid:"+magid.longValue());

        commitMagChanged = hasChanged(); // save state at end of commit -aww 2008/02/29

        setNeedsCommit(! status); //  -aww
        return status;
    }

    public void setStale(boolean tf) {
        super.setStale(tf);
    }

    /** Do everything necessary to delete event from dbase. */
    protected boolean doDeleteCommit() throws JasiCommitException {
        if (isPreferred()) return true; // can't delete it, extra insurance here
        if (! magid.isValidNumber()) return true;  // virgin not from db? -aww 2008/11/12 fix
        // Call stored procedure that knows rules for deleting an event, procedure does a database commit.
        //int retVal = doCallableSQL( "{ ?=call EPREF.delete_prefmag("+ ((SolutionTN) sol).getEvidValue() +","+ getMagid().longValue() +") }");

        final String sql = "{ ?=call EPREF.delete_prefmag(?,?) }";

        if (debug || JasiDatabasePropertyList.debugSQL)
            System.out.println("DEBUG MagnitudeTN doDeleteCommit SQL ?= call EPREF.delete_prefmag("+
                    ((SolutionTN) sol).getEvidValue()+","+getMagid().longValue() + ")");

        int retVal = -1;
        CallableStatement cs = null;
        Connection conn = DataSource.getConnection();
        if (conn == null) {
           throw new JasiCommitException("No connection to data source.");
        }
        try {
            cs = conn.prepareCall(sql);
            cs.registerOutParameter(1, java.sql.Types.INTEGER);
            cs.setLong(2,((SolutionTN) sol).getEvidValue());
            cs.setLong(3, getMagid().longValue());
            cs.executeUpdate();
            retVal = cs.getInt(1);
        } catch (SQLException ex) {
            retVal = -999;
            System.err.println("MagnitudeTN doDeleteCommit:\n"+sql);
            System.err.println(ex);
            ex.printStackTrace();
            // rollback on error
            try {
              conn.rollback();
            } catch (SQLException ex2)  { // rollback failed
              System.err.println("Rollback failed:\n"+ex2);
              ex2.printStackTrace();
            }
        }
        finally {
          JasiDatabasePropertyList.debugSQL(sql, cs, debug);
          Utils.closeQuietly(cs);
        }

        if (debug || JasiDatabasePropertyList.debugSQL) System.out.println("DEBUG MagnitudeTN doDeleteCommit retVal: " + retVal);
        boolean status = (retVal > -1); // -1, no event table row , 0 no associated eventprefmag table row -aww 2008/11/13
        setNeedsCommit(! status);

        return status;
    }

    //reworked logic to decide UPDATE vs. INSERT row -aww
    protected boolean dbaseUpdate() {

        if ( ! hasChanged() ) { // nothing to update, no-op
            return true;
        }
        else if (hasChangedValue()) { // revert to insert mode
            //return dbaseInsert(); changed to below -aww 2010/06/30 associate when newly inserted
            if (dbaseInsert()) return commitReadingList();
            return false;
        }

        // Only descriptive info should be updated 
        // (state or other descriptive data of Origin)
        if (debug) System.out.println("dbaseUpdate toNetMagRow()");
        NetMag magRow = toNetMagRow();
        magRow.setProcessing(DataTableRowStates.UPDATE);
        if (debug) System.out.println(magRow.toString());

        // Need setUpdate(true) to use key value in where condition
        magRow.getDataObject(NetMag.MAGID).setUpdate(true);
        boolean status = (magRow.updateRow(DataSource.getConnection()) > 0);
        magRow.getDataObject(NetMag.MAGID).setUpdate(false);
        if (debug) System.out.println("dbaseUpdate magRow.updateRow status: " + status);

        // don't continue if NetMag update failed 
        if (! status) return false;
        else {
             setUpdate(false);
             if (dbAttributionEnabled) {
                  // revised to return table value set -aww 2010/08/25
                  String attrib = ((JasiDbReader) getDataReader()).setAttribution(DataSource.getConnection(),
                                                             magid.longValue(),
                                                             "NETMAG",
                                                             EnvironmentInfo.getUsername()
                                                             ); 
                 //setWho(EnvironmentInfo.getUsername()); // no, its an alias here
                 //loadAttribution(); // read back the key mapping -aww removed 2010/08/25 
                 if (attrib != null) setWho(attrib); // new way -aww 2010/08/25
             }
        }

        /* add this block, from dbaseInsert if orid is ever changed by update -aww 2008/03/11
        if (this.isPreferred()) {
            ((SolutionTN)sol).dbaseSetPrefMag(this);
        }
        */

        return status;
    }

    protected boolean dbaseInsert() {

        boolean status = false; 
        //try { 
          //Allow change of key field forcing call to magseq by NetMag insertRow:
          //magid.setMutable(true).setNull(true); // not needed, see below
/*
          NetMag magRow = toNewNetMagRow(); // create the DataTableRow
          if (magRow.isNull()) throw new JasiCommitException("Malformed magnitude: NetMag row is null.");
          magRow.setProcessing(DataTableRowStates.INSERT);
          if (debug) System.out.println(magRow.toString());  // debug test

          status = (magRow.insertRow(DataSource.getConnection()) > 0);
*/
          // PreparedStatement update version with bind variables

          setUniqueId(); // when not calling toNewNetMagRow
          synchWithAssocSolutionOrid(); // when not calling toNewNetMagRow

          PreparedStatement ps = null;
          final String sql = 
              "INSERT INTO NETMAG (MAGID,ORID,MAGNITUDE,MAGTYPE,AUTH,SUBSOURCE,MAGALGO,NSTA,UNCERTAINTY,GAP,DISTANCE,QUALITY,RFLAG,NOBS)" +
              " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
          try {
              Connection conn = DataSource.getConnection();

              ps = conn.prepareStatement(sql);

              int icol = 1;

              //MAGID
              ps.setLong(icol++, magid.longValue());

              //ORID
              ps.setLong(icol++, orid.longValue());

              //MAGNITUDE
              if (value.isValid()) ps.setDouble(icol++, value.doubleValue());
              else ps.setDouble(icol++, -9.); // flag as too small

              //MAGTYPE
              ps.setString(icol++, subScript.toString());

              //AUTH
              ps.setString(icol++, authority.toString());

              //SUBSOURCE
              if (source.isValid()) ps.setString(icol++, source.toString());
              else ps.setString(icol++, null);

              //MAGALGO
              if (algorithm.isValid()) ps.setString(icol++, algorithm.toString());
              else ps.setString(icol++, null);

              //NSTA
              // assumes new table column NOBS added to Netmag, here lookup sta count for this value -aww 2010/08/19
              int used = (usedStations.isNull()) ? 0 : usedStations.intValue(); // channels read from db go into new field NOBS
              int listUsed = getStationsUsed(); // assoc channels contributing, zero if no assoc data
              used = (listUsed == 0) ? used : listUsed; // setting when assoc sta lookup count 0
              ps.setInt(icol++, used);

              //UNCERTAINTY
              if (error.isValid()) ps.setDouble(icol++, error.doubleValue()) ;
              else ps.setNull(icol++, Types.DOUBLE);

              //GAP
              if (gap.isValid()) ps.setDouble(icol++, gap.doubleValue());
              else ps.setNull(icol++, Types.DOUBLE);

              //DISTANCE
              if (distance.isValid()) ps.setDouble(icol++, distance.doubleValue());
              else ps.setNull(icol++, Types.DOUBLE);

              //QUALITY
              if (quality.isValid()) ps.setDouble(icol++, quality.doubleValue());
              else ps.setNull(icol++, Types.DOUBLE);

              //RFLAG
              if (processingState.isValid()) ps.setString(icol++, processingState.toString());
              else ps.setString(icol++, null);

              //NOBS
              // assumes new table column NOBS added to Netmag, here lookup sta count for this value -aww 2010/08/19
              used = (usedChnls.isNull()) ? 0 : usedChnls.intValue(); // channels read from db go into new field NOBS
              listUsed = getReadingsUsed(); // assoc sta contributing, zero if no assoc data
              used = (listUsed == 0) ? used : listUsed; // setting when assoc sta lookup count 0
              // after new table column NOBS added to Netmag, here lookup sta count for this value
              ps.setInt(icol++, used);

              status = (ps.executeUpdate() > 0);
          }
          catch (SQLException ex) {
              status = false;
              ex.printStackTrace();
          }
          finally {
              JasiDatabasePropertyList.debugSQL(sql, ps, debug);
              Utils.closeQuietly(ps);
          }

          if (status) {
              setUpdate(false);    // now there's a dbase row
              // set attribution here
             if (dbAttributionEnabled) {
                 // revised to return table value set -aww 2010/08/25
                 String attrib = ((JasiDbReader) getDataReader()).setAttribution(DataSource.getConnection(),
                                                             magid.longValue(),
                                                             "NETMAG",
                                                             EnvironmentInfo.getUsername()
                                                             ); 
                 //setWho(EnvironmentInfo.getUsername()); // no, its an alias here
                 //loadAttribution(); // read back the key mapping -aww removed 2010/08/25 
                 if (attrib != null) setWho(attrib); // new way -aww 2010/08/25
             }

             // Note call below may not be necessary here if call to EPREF.setprefmag_magType in SolutionTN commit does this update
             // TEST: removed update here -aww 2010/08/25
             /*
             if (this.isPreferred()) {
               ((SolutionTN)sol).dbaseSetPrefMag(this);
             }
             */
          }
        //}
        //catch (JasiCommitException ex) {
        //  ex.printStackTrace();
        //}
        return status;
    }

    private boolean commitReadingList() {
        return (isCodaMag()) ? commitCodaList() : commitAmpList();
    }


    private static final String psMagPriorityStr =  "Select MagPref.getMagPriority(?,?,?,?,?,?,?,?,?,?,?) from DUAL";
    private static final String psMagPriorityStrPG =  "Select MagPref.getMagPriority(?,?,?,?,?,?,?,?,?,?,?)";

    /**
     * Use the logic in the dbase to calculate the priority of this magnitude.
     * Returns 0 if this Magnitude is undefined.
    */
    public int getPriority() {

        Connection conn = DataSource.getConnection();

        if (conn == null) return 0;
        if (sol == null) {
          throw new NullPointerException("Magnitude:" + magid.longValue() + " has no associated Solution");
        }

        boolean useANSISQL = false;
        try {
            useANSISQL = (conn.getMetaData().getDatabaseProductName().equalsIgnoreCase("postgresql"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        PreparedStatement magPrioStatement = null;
        ResultSet rs = null;
        int priority = 0;

        final String sql;
        if (useANSISQL) {
          //Make dbase prepared statement to get Magnitude priority
          sql = psMagPriorityStrPG;
        } else {
          sql = psMagPriorityStr;
        }
        try {
            magPrioStatement = conn.prepareStatement(sql);
            // set the 9 args -aww 06/21/2006
            magPrioStatement.setDouble(1, sol.datetime.isValid() ? sol.getTime() : 0.);
            magPrioStatement.setDouble(2, sol.lat.isValid() ? sol.lat.doubleValue() : 0.);
            magPrioStatement.setDouble(3, sol.lon.isValid() ? sol.lon.doubleValue() : 0.);
            magPrioStatement.setString(4, subScript.toString());
            magPrioStatement.setString(5, authority.toString());
            magPrioStatement.setString(6, source.toString());
            magPrioStatement.setString(7, algorithm.toString());

            // Below logic changed after new Netmag table column NOBS enabled -aww 2010/08/19
            // what value maps to MAGPREFPRIORITY.minreadings column value?
            // NOTE: stations used was replaced by channels used -aww 2008/07/10
            // NOTE: channels used was replaced by stations used -aww 2010/08/19
            // Below assumes only station count value in mag priority table -aww 2010/08/19
            int used = getStationsUsed(); // first check for stations
            if (used == 0) used = (int) Math.floor((getReadingsUsed()+1)/2); // if no stations, check reading count
            magPrioStatement.setInt(8, used);

            magPrioStatement.setDouble(9, value.isValid() ? value.doubleValue() : 0.f);
            magPrioStatement.setDouble(10, error.isValid() ? error.doubleValue() : 0.f);
            //magPrioStatement.setDouble(11, quality.isValid() ? quality.doubleValue() : 1.f); // aww not defined yet
            magPrioStatement.setDouble(11, 1.0f); // alway assume PVR quality of 100 percent
            /*
            System.out.println("DEBUG Select magpref.getMagPriority(" +
            (sol.lat.isValid() ? sol.lat.doubleValue() : 0.)+","+
            (sol.lon.isValid() ? sol.lon.doubleValue() : 0.)+","+
            subScript.toString()+","+
            authority.toString()+","+
            source.toString()+","+
            algorithm.toString()+","+
            used+","+
            (value.isValid() ? value.doubleValue() : 0.f)+","+
            (error.isValid() ? error.doubleValue() : 0.f)+","+
            (quality.isValid() ? quality.doubleValue() : 1.f)+") from DUAL"
            );
            */

            rs = magPrioStatement.executeQuery();
            if ( rs.next() ) {
                priority = Math.abs(rs.getInt(1));
            }

        } catch(SQLException ex) {
            ex.printStackTrace();
        } catch(NullPointerException ex) {
            ex.printStackTrace();
        }
        finally {
          JasiDatabasePropertyList.debugSQL(sql, magPrioStatement, debug);
          Utils.closeQuietly(rs, magPrioStatement);
          magPrioStatement = null; // reset the statement to null 
        }

        return priority;
    }

    /**
    * Commits the amp list elements.
    * Only call this for once per magnitude, usually
    * right after the Magnitude is committed.
    * If Amp is new (not from dbase) an Amp row will be written.
    * An AssocAmM row will be written.
    * AssocAmO rows will NOT be written because we can't tell if they have been
    * previously written or not. That is the job of the Solution.commit() method.
    */
    protected boolean commitAmpList() {

        if (ampList == null) return true;
        return ampList.commit();

        /*
        boolean status = false;
        AmplitudeTN ampTN = null;
        int count = ampList.size();
        for (int i = 0; i < count; i++) {
            ampTN = (AmplitudeTN) ampList.get(i);  // cast to AmplitudeTN
            if (ampTN.isDeleted()) continue; // don't write deleted data association to db
            // Shouldn't we test and commit only those associated with this ? -aww
            if ( ! ampTN.isAssignedTo(this)) continue;

            // See if new row is needed
            if ( ampTN.isFromDataSource() && ! ampTN.getNeedsCommit() ) {
              // No implementation yet to discriminate "update" changes vs. "insert" changes to amp row
              status = (commitMagChanged) ? ampTN.dbaseAssociate(true) : true; // if ok associate both origin and netmag (update existing)
            } else { // needs new row
              status = ampTN.dbaseInsert();
              if (status) status = ampTN.dbaseAssociate(false); // if ok associate both origin and netmag 
              ampTN.setNeedsCommit(! status);
            }
            // Should assoc origin too, in case reading got "lost" or "deleted" from solList
            // by solution list purging by clear() for some Magnitude method recalculations
            // this should be done if sol.commitAlternateMagList is invoked -aww 06/29/2004
            // if ampTN.hasChanged() need also an insert logic here!!!!!!!!!!!
            // REALLY need an update not insert for non-solution dependent values 
            // WARNING: if magnitude and solution readings are not list aliased,
            // and both are need insert due to changes we get 2 new amps, instead of 1 
            //if (status) status = ampTN.dbaseAssociate(true); // if ok associate both origin and netmag (update existing)
        }
        */

    }

    /**
    *  Commits the coda list elements, creating database Coda and AssocCoO rows.
    */
    // Need analog of ampList implementation to do dataSource updates
    // of solAssoc versus magAssoc? - aww
    // Shouldn't we test and commit only those associatedWith(this)? -aww
    protected boolean commitCodaList() {
        if (codaList == null) return true;
        return codaList.commit();
        /*
        int count = codaList.size();
        boolean status = false;
        org.trinet.jasi.TN.CodaTN codaTN = null;
        for (int i = 0; i < count; i++) {
            codaTN = (org.trinet.jasi.TN.CodaTN) codaList.get(i);
            if (codaTN.isDeleted()) continue; // don't write deleted data association to db
            // Shouldn't we test and commit only those associated with this ? -aww
            if ( ! codaTN.isAssignedTo(this)) continue;

            // See if new row is needed
            if ( codaTN.isFromDataSource() && ! codaTN.getNeedsCommit() ) {
              // No implementation yet to discriminate "update" changes vs. "insert" changes to amp row
              status = (commitMagChanged) ? codaTN.dbaseAssociate(true) : true; // if ok associate both origin and netmag (update existing)
            } else { // needs new row
              status = codaTN.dbaseInsert();
              if (status) status = codaTN.dbaseAssociate(false); // if ok associate both origin and netmag 
              codaTN.setNeedsCommit(! status);
            }
            // Should do assoc origin too, in case reading got "lost" or "deleted" from solList
            // by solution list purging by clear() for some Magnitude method recalculations
            // this should be done if sol.commitAlternateMagList is invoked -aww 06/29/2004
            // if codaTN.hasChanged() need also an insert logic here!!!!!!!!!!!
            // REALLY need an update not insert for non-solution dependent values 
            // WARNING: if magnitude and solution readings are not list aliased,
            // and both are need insert due to changes we get 2 new amps, instead of 1 
        }
        */
    }

    public JasiObject parseData(Object rsdb) {
      return parseResultSetDb((ResultSetDb) rsdb);
    }
    public JasiObject parseResultSetDb(ResultSetDb rsdb) {
      return parseResultSet((ResultSetDb) rsdb);
    }
    /**Access to the JasiDbReader to allow generic SQL queries. */
    public JasiDataReaderIF getDataReader() {
        return jasiDataReader;
    }

    // A matching row key exists in datasource but its actually data may differ from instance
    public boolean existsInDataSource() {
        JasiDbReader jdr = (JasiDbReader) getDataReader();
        //int count = jdr.getCountBySQL(DataSource.getConnection(), "SELECT 1 FROM netmag WHERE magid = "+magid.longValue());
        int count = jdr.getCountBySQL(DataSource.getConnection(), "NETMAG", "MAGID", magid.toString());
        return (count > 0);
    }

    /**
     * Parse data out of a ResultSet NetMag row instance into a Magnitude object.
     */
    protected static Magnitude parseResultSet(ResultSetDb rsdb) {
     return parseResultSet(rsdb, 0);
    }
    /*
    private static Magnitude parseResultSet(ResultSetDb rsdb, int offset) {
        MagnitudeTN mag = new MagnitudeTN();
        mag.fromDbase = true;
        NetMag magRow = new NetMag();        // remember raw NetMag
        // parse result set into DataTableRow
        int myOffset = offset;
        magRow = (NetMag) magRow.parseOneRow(rsdb, myOffset);
        if (magRow == null) return null;
        // allow changes to this DataTableRow. Need to set this because we are
        // not using our own rather than org.trinet.jdbc parsing methods
        //if (DataSource.isWriteBackEnabled()) this.magRow.setMutable(true);
        if (DataSource.isWriteBackEnabled()) magRow.setMutable(true).setMutableAllValues(true); // 07/07/04 -aww

        // parse DataTableRow into this magnitude object
        mag.parseNetMagRow(magRow);

        myOffset = NetMag.MAX_FIELDS+1;
        try {
            String alias = rsdb.getResultSet().getString(myOffset++);
            if (alias != null) mag.getWho().setValue(alias); 
        } catch (SQLException ex) {
            System.err.println(ex);
            ex.printStackTrace();
        }

        // allow changes to this DataTableRow. Need to set this because we are
        // not using our own rather than org.trinet.jdbc parsing methods
        // Kludge to get user id if any (uses default connection)
        //DataDouble dd = rsdb.getDataDouble(myOffset++); // enable only if PVR queried above aww 06/28/2006
        //if (dd.isValidNumber()) mag.quality.setValue(dd.doubleValue()/100.); // map to quality ? -aww

        return (Magnitude) mag;
    }
    */

    private static Magnitude parseResultSet(ResultSetDb rsdb, int offset) {

        MagnitudeTN mag = new MagnitudeTN();
        mag.fromDbase = true;

        int myOffset = offset+1;

        ResultSet rs = rsdb.getResultSet();

        try {
            long ll = rs.getLong(myOffset++);

            if (rs.wasNull()) {
              mag.setUpdate(false);
              return mag;
            }

            mag.magid.setValue(ll);

            ll =  rs.getLong(myOffset++);
            if (!rs.wasNull()) mag.orid.setValue(ll);

            //n.commid
            //ll =  rs.getLong(myOffset++);
            //if (!rs.wasNull()) mag.commid.setValue(ll);

            // n.magnitude
            double dd = rs.getDouble(myOffset++);
            if (!rs.wasNull()) mag.value.setValue(dd);

            //n.magtype
            mag.subScript.setValue(rs.getString(myOffset++));

            //n.auth
            mag.authority.setValue(rs.getString(myOffset++));

            //n.subsource
            mag.source.setValue(rs.getString(myOffset++));

            //n.magalgo
            mag.algorithm.setValue(rs.getString(myOffset++));

            //n.nsta
            // NSTA based upon total number of assoc channel reading wts > 0. not usedStations -aww  02/02/2007 
            // usedChnls field replaced usedStations here -aww 2008/07/10
            //if (!rs.wasNull()) mag.usedChnls.setValue(ll); // assumes nobs column added to NetMag, nsta now not total readings -aww 2010/08/19
            ll = rs.getLong(myOffset++);
            if (!rs.wasNull()) mag.usedStations.setValue(ll);

            //n.nobs
            ll = rs.getLong(myOffset++);
            if (!rs.wasNull()) mag.usedChnls.setValue(ll); // assumes nobs column added to NetMag, set total readings here -aww 2010/08/19

            //Use of DataTableRow magRow SQL was replaced by prepared SQL statement removed -aww 2010
            //mag.usedStations = (DataLong) magRow.getDataObject(NetMag.NSTA); // assumes NOBS column added to Netmag
            //mag.usedChnls = (DataLong) magRow.getDataObject(NetMag.NOBS);    // assumes NOBS column added to Netmag

            //n.uncertainty
            dd = rs.getDouble(myOffset++);
            if (!rs.wasNull()) mag.error.setValue(dd);

            //n.gap
            dd = rs.getDouble(myOffset++);
            if (!rs.wasNull()) mag.gap.setValue(dd);

            //n.distance
            dd = rs.getDouble(myOffset++);
            if (!rs.wasNull()) mag.distance.setValue(dd);

            //n.quality
            dd = rs.getDouble(myOffset++);
            if (!rs.wasNull()) mag.quality.setValue(dd);

            //n.rflag
            mag.processingState.setValue(rs.getString(myOffset++));

            String alias = rs.getString(myOffset++);
            if (alias != null) mag.getWho().setValue(alias); 

            // save NetMag.LDDATE in timeStamp? -aww
            String dstr = rs.getString(myOffset++);
            if (dstr != null && dstr.indexOf('.',dstr.length()-4) < 0) dstr += ".0";
            mag.timeStamp.setValue( EpochTime.stringToDate(dstr) );
            //mag.timeStamp.setValue(EpochTime.stringToDate(rs.getString(myOffset++)));
            //mag.timeStamp.setValue(new DateTime(rs.getTimestamp(myOffset++)));

            mag.pmagid.setValue(rs.getLong(myOffset++));

            mag.omagid.setValue(rs.getLong(myOffset++));

        } catch (SQLException ex) {
            System.err.println(ex);
            ex.printStackTrace();
        }

        mag.setUpdate(false); // aww added if from dbase assume no changes
        return mag;
    }

    /**
     * Transfers NetMag DataTableRow DataObjects into a Magnitude object's
     * DataObjects. It is possible to have an SQL query (outer join) that
     * returns a NetMagRow populated by nulls. The only way to detect this
     * condition is to check if NetMag.MAGID == 9223372036854775807 or
     * Long.MAX_VALUE.
     * */
    protected Magnitude parseNetMagRow(NetMag magRow)
    {

        // remember DataTableRow for future writeback
        //this.magRow = magRow;

        // don't parse if its a 'null' row returned by an outer join
        // if you do, all the DataObjects are set isNull()=false but have
        // Long.MAX_VALUE or NaN in them
        if (magRow.getDataObject(NetMag.MAGID).isNull()) {
          setUpdate(false); // aww added if from dbase assume no changes
          return this;
        }

        fromDbase = true;

        this.magid     = (DataLong)   magRow.getDataObject(NetMag.MAGID);
        this.orid      = (DataLong)   magRow.getDataObject(NetMag.ORID);

        this.commid    = (DataLong)   magRow.getDataObject(NetMag.COMMID);

        this.value     = (DataDouble) magRow.getDataObject(NetMag.MAGNITUDE);
        this.subScript = (DataString) magRow.getDataObject(NetMag.MAGTYPE);

        this.authority = (DataString) magRow.getDataObject(NetMag.AUTH);
        this.source    = (DataString) magRow.getDataObject(NetMag.SUBSOURCE);

        this.algorithm    = (DataString) magRow.getDataObject(NetMag.MAGALGO);

        // NSTA based upon total number of assoc channel reading wts > 0. not usedStations -aww  02/02/2007 
        // usedChnls field replaced usedStations here -aww 2008/07/10
        //this.usedChnls = (DataLong) magRow.getDataObject(NetMag.NSTA); // replaced by below -aww 2010/08/19
        this.usedChnls = (DataLong) magRow.getDataObject(NetMag.NOBS); // assumes new table column nobs added to Netmag -aww 2010/08/19
        this.usedStations = (DataLong) magRow.getDataObject(NetMag.NSTA); // assumes new table column nobs added to Netmag -aww 2010/08/19

        this.error     = (DataDouble) magRow.getDataObject(NetMag.UNCERTAINTY) ;
        this.gap       = (DataDouble) magRow.getDataObject(NetMag.GAP);
        this.distance  = (DataDouble) magRow.getDataObject(NetMag.DISTANCE);

        this.quality   = (DataDouble) magRow.getDataObject(NetMag.QUALITY);

        this.processingState = (DataString) magRow.getDataObject(NetMag.RFLAG);
        //
        // save NetMag.LDDATE in timeStamp? -aww
        this.timeStamp = (DataDate) magRow.getDataObject(NetMag.LDDATE); // date of last revision?
        //

        //if (dbAttributionEnabled) this.loadAttribution();

        setUpdate(false); // aww added if from dbase assume no changes
        //Debug.println("  mag changed.. " + this.hasChanged());

        return this;
    }

    /**
     * Stuff contents of this Magnitude into a new NetMag (TableRow) object. Returns
     * an null NetMag row if there is insufficient data to make a valid NetMag row.
     */

    protected NetMag toNewNetMagRow() {
        // below forces a new magid to be set from the database sequence
        long newMagid = setUniqueId();
        if (debug) System.out.println("toNewNetMagRow new magid = "+newMagid);
        return toNetMagRow(newMagid);
    }
    protected NetMag toNetMagRow() {
        return toNetMagRow(magid.longValue());
    }

    private NetMag toNetMagRow(long magid) {
        NetMag magRow = new NetMag(magid); // if no change use pre-existing id -aww
        magRow.setUpdate(true);
        //
        synchWithAssocSolutionOrid(); // removed setOrid(newPrefor) in SolutionTN -aww 01/13/2005
        //
        // set not-NULL fields
        magRow.setValue(NetMag.ORID, orid);
        magRow.setValue(NetMag.MAGNITUDE, value);
        magRow.setValue(NetMag.MAGTYPE, subScript);
        magRow.setValue(NetMag.AUTH, authority);

        //channels used as of 01/17/2007, channels contributing as of 02/02/2007 -aww
        int used = (usedChnls.isNull()) ? 0 : usedChnls.intValue(); // channels read from db go into new field - aww 2008/07/10
        int listUsed = getReadingsUsed(); // assoc channels contributing, zero if no assoc data
        used = (listUsed == 0) ? used : listUsed; // setting when assoc channel lookup count 0
        //magRow.setValue(NetMag.NSTA, used); // assumes new table column NOBS added to Netmag -aww 2010/08/19
        magRow.setValue(NetMag.NOBS, used);  // assumes new table column NOBS added to Netmag -aww 2010/08/19

        // assumes new table column NOBS added to Netmag -aww 2010/08/19
        used = (usedStations.isNull()) ? 0 : usedStations.intValue();
        listUsed = getStationsUsed(); // assoc channels contributing, zero if no assoc data
        used = (listUsed == 0) ? used : listUsed; // setting when assoc sta lookup count 0
        magRow.setValue(NetMag.NSTA, used);

        // the rest can be null
        if (commid.isValid())       magRow.setValue(NetMag.COMMID, commid);
        if (source.isValid())       magRow.setValue(NetMag.SUBSOURCE, source);
        if (algorithm.isValid())    magRow.setValue(NetMag.MAGALGO, algorithm);
        if (error.isValid())        magRow.setValue(NetMag.UNCERTAINTY, error) ;
        if (gap.isValid())          magRow.setValue(NetMag.GAP, gap);
        if (distance.isValid())     magRow.setValue(NetMag.DISTANCE, distance);
        if (quality.isValid())      magRow.setValue(NetMag.QUALITY, quality);
        if (processingState.isValid()) magRow.setValue(NetMag.RFLAG, processingState);
        return magRow;
    }

    public boolean isFromDataSource() { return fromDbase;} // aww temp fix

    /**
     * Return true if any field has changed from what was read in from the
     * dbase. Returns true if it was not originally from the dbase.  */
    public boolean hasChanged() {
        if (hasChangedValue()) fromDbase = false;
        return (!fromDbase || hasChangedDescription());
    }

    public boolean hasChangedValue() {
        boolean status = false;
        if (!fromDbase )               status = true;
        // Insert needed for derived data:
        else if (orid.isUpdate())           status = true; // could moved to hasChangedDesc to just do pointer update, no new row -aww 2008/03/11
        else if (value.isUpdate())          status = true;
        else if (subScript.isUpdate())      status = true;
        else if (error.isUpdate())          status = true;
        else if (gap.isUpdate())            status = true;
        else if (distance.isUpdate())       status = true;
        else if (algorithm.isUpdate())      status = true; // could be new?
        //else if (usedChnls.isUpdate()) status = true;
        //System.out.println("MAG TN hasChanged fromDbase:" + fromDbase + " orid:" + (orid.isUpdate()) +
        //" val: " + (value.isUpdate())+ " subs: " + (subScript.isUpdate())+ " err: " + (error.isUpdate())+
        //" gap: " + (gap.isUpdate())+ " dist: " + (distance.isUpdate())+ " algo: " + (algorithm.isUpdate()));
        return status;
    }

    public boolean hasChangedDescription() {
        boolean status = false;
        if (!fromDbase )                     status = true;
        //else if (orid.isUpdate())            status = true; // could do this here -aww 2008/03/11
        else if (authority.isUpdate())       status = true;
        else if (source.isUpdate())          status = true;
        else if (commid.isUpdate())          status = true;
        else if (quality.isUpdate())         status = true;
        else if (processingState.isUpdate()) status = true;
        //System.out.println("MAG TN hasChangedDescription() fromDbase:" + fromDbase +
        //" auth: " + (authority.isUpdate())+ " src: " + (source.isUpdate())+
        //" commid: " + (commid.isUpdate())+ " quality: " + (quality.isUpdate())+
        //" processingState: " + (processingState.isUpdate()));
        return status;
    }

    /**
     * Set the isUpdate() flag for all data dbase members the given boolean value.
     * */
    protected void setUpdate(boolean tf) {
        fromDbase = !tf;
        orid.setUpdate(tf);
        authority.setUpdate(tf);
        source.setUpdate(tf) ;
        value.setUpdate(tf);
        subScript.setUpdate(tf) ;
        commid.setUpdate(tf) ;
        algorithm.setUpdate(tf);
 //     usedChnls.setUpdate(tf);
        error.setUpdate(tf);
        gap.setUpdate(tf);
        distance.setUpdate(tf) ;
        quality.setUpdate(tf) ;
        processingState.setUpdate(tf);
        timeStamp.setUpdate(tf);  // would need to update with SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) after commit() 
    }
    /**
     * Set the value of orid. This is the key field pointing to the Origin table.
     */
    public void setOrid(long newOrid) {
        //orid.setMutable(true);
        //if (debug) System.out.println("DEBUG MagnitudeTN setOrid to : " + newOrid);
        orid.setValue(newOrid);
    }
    /**
     * Return the value of orid. This is the key field pointing to the Origin table.
     */
    public Object getOriginIdentifier() {return orid;}
    public DataLong getOrid() {
          return orid;
    }
    public long getOridValue( ) {
      return (orid.isValidNumber()) ? orid.longValue() : 0l;
    }

    public int loadReadingList(boolean clearList) {
      return (isCodaMag()) ? loadCodaList(clearList) : loadAmpList(clearList);
    }

    protected int loadAmpList(boolean clearList) {
        if (! isAmpMag()) return 0; // is this a problem?
        // NOTE: With coupled list listeners clearing magnitude list here might also CLEAR solution list!
        if (clearList && ampList.size() > 0) ampList.clear();
        // getByXXX(,false) does NOT associate list elements.
        List aList = (List) Amplitude.create().getByMagnitude(this, null, false);
        if (aList == null) return ampList.size();
        //associate(aList); // 10/03 could try this instead aww
        // below equivalent to this.fastAssociateAll(aList)
        // doesn't check for duplicates (addOrReplace vs. add):
        ampList.assignListTo(aList, this);
        ampList.fastAddAll(aList); // no checks for nulls or dupes
        //ampList.addAll(aList); // slower, does checks
        // Really only should be stale if mag association table and input count mismatch
        //setStale(true); // not needed with list listeners aww
        // NOTE only preferred mag readings are automatically added by list
        // listeners to an associated solution's like list
        return ampList.size();
    }

    /**
     *  Clears CodaList and add any codas that are associated in the DataSource
     *  to this Magnitude's CodaList.  Note that references are used, the codas
     *  are not copied (cloned). Returns a count of the number that were added.
     *  Sets staleMagnitude 'true' if any are added */
    protected int loadCodaList(boolean clearList) {
        if (! isCodaMag()) return 0; // is this a problem?
        // NOTE: With coupled list listeners clearing magnitude list here might also CLEAR solution list!
        if (clearList && codaList.size() > 0) codaList.clear();
        // assumes getBySolution() does NOT associate list elements.
        List aList = (List) org.trinet.jasi.Coda.create().getByMagnitude(this, null, false);
        if (aList == null) return codaList.size();
        //associate(aList); // 10/03 could try this instead aww
        // below equivalent to this.fastAssociateAll(aList)
        // doesn't check for duplicates (addOrReplace vs. add):
        codaList.assignListTo(aList, this);
        codaList.fastAddAll(aList); // no checks for nulls or dupes
        //codaList.addAll(aList); // slower, does checks
        // Really only should be stale if mag association table and input count mismatch
        //setStale(true); // not needed with list listeners aww
        return codaList.size();
    }

    private void loadAttribution() {
        Connection conn = DataSource.getConnection();
        if (conn == null) return;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        final String sql = "SELECT refer FROM credit WHERE (id=?) AND tname='NETMAG'";
        try {
          stmt = conn.prepareStatement(sql);
          stmt.setLong(1, magid.longValue());
          stmt.executeQuery();

          rs = stmt.getResultSet();
          String attrib = null;
          if (rs.next()) attrib = rs.getString(1);
          if (attrib != null) setWho(attrib);
        }  
        catch (SQLException ex) {
          ex.printStackTrace();
        }
        finally {
          JasiDatabasePropertyList.debugSQL(sql, stmt, debug);
          Utils.closeQuietly(rs, stmt);
        }
    }
/*
  public static final class Tester {
    // Main for testing: % CatalogView [hours-back]  (default = 1)
    public static final void main(String args[]) { // For testing There's no DataSource.commit() done

        final String driver = org.trinet.jdbc.datasources.AbstractSQLDataSource.DEFAULT_DS_DRIVER;

        if (args.length < 1) {
            System.out.println("Syntax main args:  hostName");
            System.exit(0);
        }
        System.out.println("Creating new database connection for default account on:" + args[0]);

        DataSource init =  TestDataSource.create(args[0]); // DataSource (url, driver, user, passwd);

        //        int id = 9502492;
        //        int id = 9500013;
        long id = 9616101;

        if (true) { // test 0
          ArrayList magList = (ArrayList) Magnitude.create().getBySolution(id);
          Magnitude mags[] = (Magnitude[])magList.toArray(new Magnitude[magList.size()]);

          for (int i = 0; i<mags.length; i++ ) {
            System.out.println(mags[i].toDumpString());
          }

        } // end of test 0

        // RETREIVE
        if (false) { /// TEST 1
          System.out.println("*Retreiving mag for evid= "+id);
          // get the mag for this evic
          SolutionTN sol = (SolutionTN) Solution.create().getById(id);
          MagnitudeTN mag0 =(MagnitudeTN) sol.magnitude;
          if (mag0 == null) {
            System.out.println("No mag for id =" + id);
            System.exit(0);
          }

          System.out.println("EVID= "+id+ "  mag= "+mag0.toString() );
          System.out.println("priority = "+ mag0.getPriority());
          System.out.println("set priority = "+ sol.dbaseAutoSetPrefMag());
          System.out.println(mag0.toDumpString());
          System.exit(0); // BAIL OUT HERE
// Code below not executed?
          System.out.println(mag0.toDumpString());
          long orid = sol.getOridValue();
          // INSERT new mag
          MagnitudeTN mag = (MagnitudeTN) Magnitude.create();
          mag.setOrid(orid); // "borrow" orid so we can make valid mag
          mag.value.setValue(1.0);
          mag.subScript.setValue("x");
          mag.authority.setValue(EnvironmentInfo.getNetworkCode());
          mag.source.setValue("RT1");
          System.out.println("\n*Inserting record.");
          System.out.println(mag.toDumpString());
          boolean status = true;
          try {
             status = mag.commit();
          } catch (JasiCommitException ex) {}

          System.out.println("commit status = "+ status);
          // check - reread
          MagnitudeTN magx = null;
          if (status) {
            id = mag.magid.longValue();
            magx = (MagnitudeTN) new MagnitudeTN().getByMagId(id);
            System.out.println("\nCheck magid= "+id+":"+magx.toString());
            System.out.println(magx.toDumpString());
          }
          // UPDATE (change it)
          mag.value.setValue(2.2);
          mag.subScript.setValue("x");
          mag.authority.setValue(EnvironmentInfo.getNetworkCode());
          mag.source.setValue("RT1");
          //mag.usedChnls.setValue(99);
          System.out.println("\n*Updating record.");
          System.out.println(mag.toDumpString());
          try {
            status = mag.commit();
          } catch (JasiCommitException ex) {}
          System.out.println("commit status = "+ status);
          // check - reread
          if (status) {
            id = mag.magid.longValue();
            magx = (MagnitudeTN) new MagnitudeTN().getByMagId(id);
            System.out.println("\nCheck magid= "+id+":"+magx.toString());
            System.out.println(magx.toDumpString());
          }
      } //end of TEST 1
    } // end of main
  }
*/
} // end of class


