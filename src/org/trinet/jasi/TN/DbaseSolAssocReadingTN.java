// this is not yet used by any class 08/2004
package org.trinet.jasi.TN;
// import org.trinet.jasi.*;
public interface DbaseSolAssocReadingTN {
    public boolean isFromDataSource();
    public boolean dbaseDelete();
    public boolean dbaseInsert();
    public boolean dbaseAssociate(boolean updateExisting);
    public boolean dbaseAssociateWithOrigin(boolean updateExisting);
    public boolean hasChangedAttributes();
    // add others if needed to jasi package access
}
