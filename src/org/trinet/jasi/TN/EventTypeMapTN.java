package org.trinet.jasi.TN;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.StringTokenizer;

import org.trinet.jasi.*;

/** Concrete class that defines how event types are mapped in the TriNet schema. */
public class EventTypeMapTN extends EventTypeMap {

    /** Define TriNet schema event type strings. */
    static String thisType[] = {
        "le",
        "re",
        "ts",
        "qb",
        "ex",
        "sn",
        "lp",
        "vt",
        "st",
        "nt",
        "ot",
        "uk"};

    static String thisLocalDefault = "uk";

     public EventTypeMapTN() {
      localType = thisType;
      localDefault = thisLocalDefault;
     }

    public void loadEventTypes() {
        Connection conn = DataSource.getConnection();
        if (conn == null) return;

        Statement sm = null;
        ResultSet rs = null;
        String sql = "Select etype, name, description, category from EventType order by category, name";
        try {
            sm = conn.createStatement();
            if (JasiDatabasePropertyList.debugSQL) System.out.println ("EventTypeMapTN sql: "+sql);
            rs = sm.executeQuery(sql);
            idx = 0;
            ArrayList typeList = new ArrayList(32);
            ArrayList jasiList = new ArrayList(32);
            ArrayList etList = new ArrayList(32);
            String code = null;
            String name = null;
            String desc = null;
            String group = null;
            //double fix = 0.;
            boolean fix = false;
            EventType et = null;
            while (rs.next()) {
              code = rs.getString(1);
              typeList.add(code);

              name = rs.getString(2);
              jasiList.add(name);

              desc = rs.getString(3);
              group = rs.getString(4);

              //fix = rs.getDouble(5);
              //et = new org.trinet.jasi.EventType(code, name, desc, group, ((rs.wasNull()) ? EventType.DEFAULT_FIX_DEPTH : fix));

              fix = ( "explosion".equals(group) || "atmospheric".equals(group) || "earth movement".equals(group) || "impact".equals(group) );

              et = (fix) ? new org.trinet.jasi.EventType(code, name, desc, group, 0.) : new org.trinet.jasi.EventType(code, name, desc, group);

              etList.add(et);
            }
            localType = (String []) typeList.toArray(new String [typeList.size()]);
            jasiType = (String []) jasiList.toArray(new String [jasiList.size()]);
            eventTypes = (EventType []) etList.toArray(new org.trinet.jasi.EventType [etList.size()]);

            idx = localType.length-1;
            for (int ii = 0; ii <= idx; ii++) {
              LOCAL =        getIntOfLocalCode("le");
              REGIONAL =     getIntOfLocalCode("re");
              TELESEISM =    getIntOfLocalCode("ts");
              QUARRY =       getIntOfLocalCode("qb");
              EXPLOSION =    getIntOfLocalCode("ex");
              SONIC =        getIntOfLocalCode("sn");
              LONGPERIOD =   getIntOfLocalCode("lp");
              TREMOR =       getIntOfLocalCode("tr");
              TRIGGER =      getIntOfLocalCode("st");
              NUCLEAR =      getIntOfLocalCode("nt");
              OTHER =        getIntOfLocalCode("ot");
              UNKNOWN =      getIntOfLocalCode("uk");
            }
        }
        catch (SQLException ex) {
            System.err.println("EventType SQLException: " + ex.getMessage());
        }
        finally {
          try {
            if (rs != null) rs.close();
            if (sm != null) sm.close();
          }
          catch (SQLException ex) {}
        }
        System.out.println("Begin EventTypeMap Listing");
        System.out.println(this.toString());
        System.out.println("End of EventTypeMap Listing");
    }

}
