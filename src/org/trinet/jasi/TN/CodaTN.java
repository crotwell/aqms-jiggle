package org.trinet.jasi.TN;
import org.trinet.jasi.coda.TN.*;
import org.trinet.jasi.*;
import org.trinet.jasi.coda.*;
import org.trinet.jdbc.table.*;
public class CodaTN extends org.trinet.jasi.coda.TN.CodaTN {


    public CodaTN() {
    }

    // accessor methods are needed when subclasses belong to other packages -aww
   // since protected can access across package boundaries by Solution and Magnitude
    public boolean isFromDataSource() {
        return super.isFromDataSource();
    }
    protected boolean dbaseDelete() {
        return super.dbaseDelete();
    }
    protected boolean dbaseInsert() {
        return super.dbaseInsert();
    }
    protected boolean dbaseAssociate(boolean updateExisting) {
        return super.dbaseAssociate(updateExisting);
    }
    protected boolean dbaseAssociateWithMag(boolean updateExisting) {
        return super.dbaseAssociateWithMag(updateExisting);
    }
    protected boolean dbaseAssociateWithOrigin(boolean updateExisting) {
        return super.dbaseAssociateWithOrigin(updateExisting);
    }
    protected void setRowChannelId(StnChlTableRow aRow, Channel chan) {
        super.setRowChannelId(aRow, chan);
    }
    protected boolean hasChangedAttributes() {
        return super.hasChangedAttributes();
    }

    public void setUpdate(boolean tf) {
        super.setUpdate(tf);
    }

    // init of database reader after instantiation (note not done by jasi.coda.TN.CodaTN super class)
    // if static call getBy methods with this instance so resultset parsing has correct joinType state
    public JasiDataReaderIF getDataReader() {
        if (jasiDataReader == null) {
           //jasiDataReader = new JasiDbReader(this);
           jasiDataReader = new JasiDbReader();
           setDebug(JasiDatabasePropertyList.debugTN); 
        }
        return jasiDataReader;
    }

    // add other methods if needed to allow jasi package access
}
