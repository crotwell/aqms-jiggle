package org.trinet.jasi.TN;

import java.util.*;
import org.trinet.jasi.*;
import org.trinet.util.*;

/**
 * EventPriorityModelTN.java
 */
public class EventPriorityModelTN extends org.trinet.storedprocs.eventpriority.EventPriority {
    
    public EventPriorityModelTN() { }

    public static double getPriority (Solution sol) {
        return getPriority(sol.magnitude.value.doubleValue(), sol.datetime.doubleValue(), sol.quality.doubleValue() );
    }

/*
  static public final class Tester {
    public static void main (String args[]) {
        System.out.println ("Starting");

        System.out.println ("Making connection...");
        DataSource jc = TestDataSource.create();

        // get by time
        double hoursBack = 24;
        final int secondsPerHour = 60*60;
        Calendar cal = Calendar.getInstance();
        java.util.Date date = cal.getTime();    // current epoch millisec
        long now = date.getTime()/1000;   // current epoch sec (millisecs -> seconds)

        long then = now - (long) (secondsPerHour * hoursBack);    // convert to seconds

        double dstart = (double) then;
        double dend   = (double) now;
        TimeSpan ts = new TimeSpan( dstart, dend);

        System.out.println ("Get entries by timespan: \n"+ ts.toString());

        //  Solution sol[] = Solution.getByTime(dstart, dend);
        Solution sol[] = (Solution []) Solution.create().getValidByTime(dstart, dend).toArray(new Solution[0]);
        System.out.println ("Found "+sol.length+" events.");

        //  QualityControlModel qc = QualityControlModel.getInstance();
        QualityControlModel qc = QualityControlModel.create();

        for (int i = 0; i < sol.length; i++)
        {       
           System.out.println (sol[i].toSummaryString()+
              " Priority = "+ EventPriorityModelTN.getPriority(sol[i]));
        }
    } // end of main
  } // end of Tester
*/
} // EventPriorityModelTN
