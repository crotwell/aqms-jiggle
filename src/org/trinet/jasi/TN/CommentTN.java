package org.trinet.jasi.TN;

import org.trinet.jasi.*;

import org.trinet.jdbc.table.*;
import org.trinet.jdbc.table.SeqIds;
import org.trinet.jdbc.datatypes.*;
//import org.trinet.jdbc.table.Comment;

public class CommentTN extends Comment {
/* 
   NOT USED 11/2003
*/

   protected DataLong commid;
   
   public final static int TN_MAX_LENGTH = 80;

//  protected Comment commentRow;

   public CommentTN() { }


     /** Return the maximum allowed comment length.
     * Over ride in concrete class if another value is desired. */
     public int getMaxLength() {
       return TN_MAX_LENGTH;
     }


    /** Commit comment to dbase. Should be done BEFORE last update of Event table
    * keys so its key can be included. */
    public boolean commit() {

      final long rowNum = 1; // constant for now 

      if (!text.isUpdate() || text.isNull()) return false;

      // new comment ID #
      long newId = SeqIds.getNextSeq(DataSource.getConnection(), Remark.SEQUENCE_NAME);
//    String safeString = StringSQL.toSafeSqlString(comment.toString()); // handle blanks, null, ", ', etc.
      String safeString = text.toString();
      Remark remarkRow = new Remark(newId, rowNum, safeString);
      remarkRow.setProcessing(Remark.INSERT);

      if ( remarkRow.insertRow(DataSource.getConnection()) < 1) {
        return false;
      } else { // only set the commid value if commit is successful // aww
        commid.setValue(newId);
        return true;
      }
    }

    public DataLong getCommid() {
      return commid;
    }
    public long getCommidValue() {
      return (commid.isValidNumber()) ? commid.longValue() : 0l;
    }
}
