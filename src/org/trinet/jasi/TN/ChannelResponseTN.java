package org.trinet.jasi.TN;

import java.sql.*;
import java.util.*;

import org.trinet.jasi.*;
import org.trinet.jdbc.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.jdbc.table.*;
import org.trinet.util.*;

/**
* ChannelResponse originating from SimpleResponse database table.
*/
public class ChannelResponseTN extends ChannelResponse implements DbReadableJasiChannelObjectIF {

    protected boolean fromDbase = false;

    protected static String TN_TABLE_NAME = "JASI_RESPONSE_VIEW";
    //static {
        //TN_TABLE_NAME = (EnvironmentInfo.getNetworkCode().equals("CI")) ? "SIS_SIMPLE_RESPONSE": "SIMPLE_REPONSE";
    //}

    protected final static String SQL_SELECT_PREFIX =
      "SELECT NET,STA,SEEDCHAN,CHANNEL,CHANNELSRC,LOCATION," +
      "NATURAL_FREQUENCY,DAMPING_CONSTANT,GAIN,GAIN_UNITS, " +
      "LOW_FREQ_CORNER,HIGH_FREQ_CORNER,ONDATE,OFFDATE FROM " + TN_TABLE_NAME;

    static protected JasiChannelDbReader jasiDataReader =  new JasiChannelDbReader();
    // below used to be static block, made instance init -aww 2008/11/13
    // return to static block, -aww 2013/11/19
    static {
      jasiDataReader.setFactoryClassName("org.trinet.jasi.TN.ChannelResponseTN");
      jasiDataReader.setDebug(JasiDatabasePropertyList.debugSQL);
    }

    public ChannelResponseTN() {}

    public ChannelResponseTN(ChannelIdIF id) {
        super(id);
    }
    public ChannelResponseTN(ChannelIdIF id, DateRange dateRange) {
        super(id, dateRange);
    }
    public ChannelResponseTN(ChannelIdIF id, DateRange dateRange, ChannelGain gain) {
        super(id, dateRange, gain);
    }
    public ChannelResponseTN(ChannelIdIF id, DateRange dateRange, double gainValue, int gainUnits) {
        super(id, dateRange, gainValue, gainUnits);
    }

    public String toChannelSQLSelectPrefix() {
        return SQL_SELECT_PREFIX ;
    }
    public String toChannelSQLSelectPrefix(java.util.Date date) {
        return SQL_SELECT_PREFIX +
          " WHERE " + DataTableRowUtil.toDateConstraintSQLWhereClause(TN_TABLE_NAME, date) ;
    }

    /** Return count of all channels in data source at specified time. */
    public int getCount(java.util.Date date) {
      return jasiDataReader.getCountBySQL(DataSource.getConnection(), toChannelSQLSelectPrefix(date));
    }

    /**
     * Returns an instance valid for the specified input channel id and date.
     * If input date is null, returns most recently available data for the input channel identifier.
     * Returns null if no data are found satisfying input criteria.
    */
    public ChannelDataIF getByChannelId(ChannelIdIF id, java.util.Date date) {
        return (ChannelDataIF) jasiDataReader.getByChannelId(TN_TABLE_NAME, id, date);
    }
    public Collection getByChannelId(ChannelIdIF id, DateRange dr) {
        return jasiDataReader.getByChannelId(TN_TABLE_NAME, id, dr);
    }


    /**
    * Returns an instance whose data members values are parsed from the input ResultSetDb object.
    */
    private static ChannelResponseTN parseResultSet(ResultSetDb rsdb) {
        int offset = 0;
        ChannelResponseTN resp = new ChannelResponseTN();
        ResultSet rs = rsdb.getResultSet();
        try {
            if (resp.channelId == null) resp.channelId = new ChannelName();
            resp.channelId.setNet(rs.getString(++offset));
            resp.channelId.setSta(rs.getString(++offset));
            resp.channelId.setSeedchan(rs.getString(++offset));
            resp.channelId.setChannel(rs.getString(++offset));
            resp.channelId.setChannelsrc(rs.getString(++offset));
            resp.channelId.setLocation(rs.getString(++offset));

            resp.natFreq.setValue(rs.getDouble(++offset));
            resp.damping.setValue(rs.getDouble(++offset));

            resp.gain.setValue(rs.getDouble(++offset));
            resp.gain.setUnits(Units.getInt(rs.getString(++offset)));

            resp.loCutFreq.setValue(rs.getDouble(++offset));
            resp.hiCutFreq.setValue(rs.getDouble(++offset));

            if (resp.dateRange == null)  resp.dateRange = new DateRange();
            String dstr = rs.getString(++offset);
            if (dstr != null)
              if (dstr.indexOf('.',dstr.length()-4) < 0) dstr += ".0";
              resp.dateRange.setMin(EpochTime.stringToDate(dstr)); // GMT date ok - aww 2008/02/11
            dstr = rs.getString(++offset);
            if (dstr != null)
              if (dstr.indexOf('.',dstr.length()-4) < 0) dstr += ".0";
              resp.dateRange.setMax(EpochTime.stringToDate(dstr)); // GMT date ok - aww 2008/02/11

        }
        catch (SQLException ex) {
            System.err.println(ex);
            ex.printStackTrace();
        }

        resp.fromDbase = true; // flag as db acquired
        return resp;

    }

    /**
     * Returns a Collection of objects from the default DataSource regardless of date.
     * There may be multiple entries for each channel that represent changes through time.
     * Uses the default DataSource Connection.
    */
    public  Collection loadAll() {
        return loadAll((java.util.Date) null);
    }

    /*
     * Return Collection of objects that were valid on the input date.
     * Data is retrieved from the default DataSource Connection.
    */
    public Collection loadAll(java.util.Date date) {
        return loadAll(DataSource.getConnection(), date);
    }

    public Collection loadAll(java.util.Date date, String[] prefSeedchan) {
        return loadAll(date, null, prefSeedchan, null);
    }

    public Collection loadAll(java.util.Date date, String [] prefNet,
                    String [] prefSeedchan, String [] prefChannel) {
        return loadAll(date, prefNet, prefSeedchan, prefChannel, null);
    }
    public Collection loadAll(java.util.Date date, String [] prefNet,
                    String [] prefSeedchan, String [] prefChannel,
                    String [] prefLocations) {
        return jasiDataReader.loadAll(TN_TABLE_NAME, DataSource.getConnection(), date,
                        prefNet, prefSeedchan, prefChannel, prefLocations);
    }

    /**
    * Returns a Collection of objects created from data obtained from the specified connection
    * that are valid for the specified input date.
    * Note - individual lookups may be more time efficient, if one shot less then several hundred channels.
    */
    static public Collection loadAll(Connection conn, java.util.Date date) {
        return jasiDataReader.loadAll(conn, date);
    }

    /** Writes the data values of this instance to the default DataSource archive.*/
    public boolean commit() {
        return false;
    }

    public JasiObject parseData(Object rsdb) {
      return parseResultSetDb((ResultSetDb) rsdb);
    }
    public JasiObject parseResultSetDb(ResultSetDb rsdb) {
      return parseResultSet((ResultSetDb) rsdb);
    }
    /**Access to the JasiDbReader to allow generic SQL queries. */
    public JasiDataReaderIF getDataReader() {
        return jasiDataReader;
    }


    // below 1000mv vs. 800mv => 655L cnts see TIMIT$DEV$CHAR; 360L old cusp value from MCA.for
    public static final double MAX_AMP_E_DEFAULT  = 2048.; // VCO USGS
    public static final double MAX_AMP_H_DEFAULT  = 8388608.;
    public double getMaxAmpValue() {
        return 0.;
        // assume no default -aww 2009/03/10
        /*
        String type = channelId.getSeedchan().substring(0,1);
        if (type.equals("E")) return MAX_AMP_E_DEFAULT;
        else return MAX_AMP_H_DEFAULT;
        */
    }
    public static final double CLIP_AMP_E_DEFAULT = 1250.; // VCO USGS?
    public static final double CLIP_AMP_H_DEFAULT = 8388608.;
    public double getDefaultClipAmp() {
        String type = channelId.getSeedchan().substring(0,1);
        if (type.equals("E")) return CLIP_AMP_E_DEFAULT;
        else if (type.equals("H")) return CLIP_AMP_H_DEFAULT;
        else return MAX_AMP_H_DEFAULT;
    }

} // end of class ChannelResponseTN

