//Driving table is StaCorrections for channel key lookup (extra amp parms outer join)
//Uses all columns names declared in StaCorrections IF class
//Might be better to just explicitly state only those of interest for simplicity
package org.trinet.jasi.TN;

import java.sql.*;
import java.util.*;

import org.trinet.jasi.*;
import org.trinet.jdbc.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.jdbc.table.*;
import org.trinet.util.*;

/**
* Type for Ml amplitude calibrations origininating from TriNet using NCDC schema.
*/
public class MlAmpMagCalibrTN extends MlAmpMagCalibration implements DbReadableJasiChannelObjectIF {

    protected boolean fromDbase = false;

    protected final static String KEY_TABLE = StaCorrections.DB_TABLE_NAME;
    protected final static String OUTER_JOIN_TABLE = "ChannelMap_AmpParms" ; // restored 10/04 aww
    protected final static String OUTER_JOIN_COLUMNS = "x.clip";
    //protected final static String OUTER_JOIN_TABLE = "ChannelMap_MagParms" ; // replaced above 08/04 aww
    //protected final static String OUTER_JOIN_COLUMNS = "x.maxcnts,x.ampclip";

    // StaCorrections sc outer joined to Clip x (if clip table doesn't have data we still get stacorr) 
    // Note no alias for StaCorrections since qualified column names is used:
    protected final static String SQL_SELECT_PREFIX =
      "SELECT " +StaCorrections.QUALIFIED_COLUMN_NAMES+
         ","+OUTER_JOIN_COLUMNS+" FROM "+KEY_TABLE+" LEFT OUTER JOIN "+OUTER_JOIN_TABLE+
         " x ON "+KEY_TABLE+".net=x.net AND "+KEY_TABLE+".sta=x.sta AND "+
         KEY_TABLE+".seedchan=x.seedchan AND "+KEY_TABLE+".location=x.location"+
         " WHERE "+KEY_TABLE+ ".corr_type = 'ml' AND "+KEY_TABLE+".offdate > SYS_EXTRACT_UTC(CURRENT_TIMESTAMP)";

    static protected JasiChannelDbReader jasiDataReader =  new JasiChannelDbReader();
    { 
      jasiDataReader.setFactoryClassName(getClass().getName());
      jasiDataReader.setDebug(JasiDatabasePropertyList.debugSQL);
    }

    public MlAmpMagCalibrTN() {
        this(null, null);
    }

    public MlAmpMagCalibrTN(ChannelIdIF id) {
        this(id, null);
    }
    public MlAmpMagCalibrTN(ChannelIdIF id, DateRange dateRange) {
        super(id, dateRange);
    }
    public MlAmpMagCalibrTN(ChannelIdIF id, DateRange dateRange, double value) {
        this(id, dateRange, value, null, null);
    }
    public MlAmpMagCalibrTN(ChannelIdIF id, DateRange dateRange, double value, 
                    String corrFlag) {
        super(id, dateRange, value, corrFlag, null);
    }
    public MlAmpMagCalibrTN(ChannelIdIF id, DateRange dateRange, double value, 
                    String corrFlag, String authority) { 
        super(id, dateRange, value, corrFlag, authority);
    }

    public String toChannelSQLSelectPrefix() {
        return SQL_SELECT_PREFIX;
    }

    public String toChannelSQLSelectPrefix(java.util.Date date) {
    // Assume StaCorrections is not date dependent, just get most "recent" data (offdate > SYS_EXTRACT_UTC(CURRENT_TIMESTAMP))
    // however the AmpParms table data must satify input date constraint
      return "SELECT " +StaCorrections.QUALIFIED_COLUMN_NAMES+
         ","+OUTER_JOIN_COLUMNS+ " FROM "+KEY_TABLE+" LEFT OUTER JOIN "+OUTER_JOIN_TABLE+
         " x ON "+KEY_TABLE+".net=x.net AND "+KEY_TABLE+".sta=x.sta AND "+
         KEY_TABLE+".seedchan=x.seedchan AND "+KEY_TABLE+".location=x.location"+
         " AND " +DataTableRowUtil.toDateConstraintSQLWhereClause("x", date)+
         " WHERE "+KEY_TABLE+".corr_type = 'ml' AND "+KEY_TABLE+".offdate > SYS_EXTRACT_UTC(CURRENT_TIMESTAMP)";
    }

    /** Return count of all channels in data source at specified time. */
    public int getCount(java.util.Date date) {
      return jasiDataReader.getCountBySQL(DataSource.getConnection(), toChannelSQLSelectPrefix(date));
    }

    /**
     * Returns an instance valid for the specified input channel id and date.
     * If input date is null, returns most recently available data for the input channel identifier.
     * Returns null if no data are found satisfying input criteria.
    */
    public ChannelDataIF getByChannelId(ChannelIdIF id, java.util.Date date) {
      return (ChannelDataIF) jasiDataReader.getByChannelId(KEY_TABLE, id, date);
    }
    public Collection getByChannelId(ChannelIdIF id, DateRange dr) {
        return jasiDataReader.getByChannelId(KEY_TABLE, id, dr);
    }


    /**
    * Returns an instance whose data members values are parsed from the input ResultSetDb object.
    */
    private static MlAmpMagCalibrTN parseResultSet(ResultSetDb rsdb) {
        int offset = 0;
        StaCorrections newRow = (StaCorrections) new StaCorrections().parseOneRow(rsdb, offset); // from package org.trinet.jdbc.table
        if (newRow == null) {
           System.err.println("MlAmpCalibrTN parseResultSet cannot parse row from resultSet");
           return null;
        }

        if (DataSource.isWriteBackEnabled()) {
            newRow.setMutable(true); // allow changes with aliases to fields of this DataTableRow.
        }
        MlAmpMagCalibrTN calibr = new MlAmpMagCalibrTN();
        calibr.parseStaCorrectionsRow(newRow);

        offset += StaCorrections.MAX_FIELDS;
        try {
          //calibr.maxAmp  = rsdb.getDataDouble(++offset); // removed column from schema table -aww 10/27/2004
          calibr.clipAmp = rsdb.getDataDouble(++offset);
        }
        catch (SQLException ex) {
          System.out.println("ERROR parsing max/clip amps from database row.");
          ex.printStackTrace();
          return null;
        }
        calibr.fromDbase = true; // flag as db acquired
        return calibr;
    }

    public boolean isFromDataSource() { return fromDbase; }

    private void parseStaCorrectionsRow(StaCorrections row) {
        if (channelId == null) channelId = new ChannelName();
        channelId = DataTableRowUtil.parseChannelSrcFromRow(row, channelId);

        // DateDate must be GMT time from db (not local time)
        DataDate on = (DataDate) row.getDataObject(StaCorrections.ONDATE);
        java.util.Date onDate =  ( on.isNull()) ? null : on.dateValue();
        DataDate off = (DataDate) row.getDataObject(StaCorrections.OFFDATE);
        java.util.Date offDate = (off.isNull()) ? null : off.dateValue();
        if (dateRange == null) {
          dateRange = new DateRange(onDate, offDate);
        }
        else {
          dateRange.setMin(onDate);
          dateRange.setMax(offDate);
        }

        DataObject tmp = row.getDataObject(StaCorrections.CORR);
        if (! tmp.isNull()) corr.setValue(tmp);

        tmp  = row.getDataObject(StaCorrections.CORR_TYPE);
        if (! tmp.isNull()) corrType.setValue(tmp.toString());
        else corrType.setValue(CorrTypeIdIF.ML); // should not be missing data if stacorr is driving table

        tmp = row.getDataObject(StaCorrections.CORR_FLAG);
        if (! tmp.isNull()) corrFlag.setValue(tmp.toString());

        tmp = row.getDataObject(StaCorrections.AUTH);
        if (! tmp.isNull()) authority.setValue(tmp.toString());

    }

    private StaCorrections toStaCorrectionsRow() {
        StaCorrections newRow = new StaCorrections();
        newRow.setUpdate(true); // set flag to enable processing
        // not null, or error results on table insert
        DataTableRowUtil.setRowChannelId(newRow, channelId) ;
        if (! corr.isNull())
                newRow.setValue(StaCorrections.CORR, corr);

        if (! corrType.equalsValue(UNKNOWN_STRING_TYPE))
                newRow.setValue(StaCorrections.CORR_TYPE, corrType);

        java.util.Date minDate = dateRange.getMinDate();
        if (minDate != null) 
                newRow.setValue(StaCorrections.ONDATE, minDate);

        // nullable table columns fields
        if (! corrFlag.equalsValue(UNKNOWN_STRING_TYPE))
                newRow.setValue(StaCorrections.CORR_FLAG, corrFlag);

        if (! authority.equalsValue(UNKNOWN_STRING_TYPE))
                newRow.setValue(StaCorrections.CORR_FLAG, corrFlag);

        if (dateRange.hasMaxLimit())
                newRow.setValue(StaCorrections.OFFDATE, dateRange.getMaxDate());

        return newRow;
    }

    protected boolean dbaseInsert () {                      // aka JasiReading
        boolean status = true;
        StaCorrections row = toStaCorrectionsRow();
        //System.out.println("MlAmpCalibrTN dbaseInsert row.toString(): " + row.toString());
        if (fromDbase) {
            row.setProcessing(DataTableRowStates.UPDATE);
            status = (row.updateRow(DataSource.getConnection()) > 0);
        }
        else {
            row.setProcessing(DataTableRowStates.INSERT);
            status = (row.insertRow(DataSource.getConnection()) > 0);
        }
        if (status) {
            fromDbase = true; // now its "from" the dbase
        }
        return status;
    }

    /**
     * Returns a Collection of objects from the default DataSource regardless of date.
     * There may be multiple entries for each channel that represent changes through time.
     * Uses the default DataSource Connection.
    */
    public Collection loadAll() {
        return jasiDataReader.loadAll(DataSource.getConnection());
    }

    /**
     * Return Collection of objects that were valid on the input date.
     * Data is retrieved from the default DataSource Connection.
    */
    public Collection loadAll(java.util.Date date) {
        return loadAll(DataSource.getConnection(), date);
    }

    public Collection loadAll(java.util.Date date, String[] prefSeedchan) {
        return loadAll(date, null, prefSeedchan, null);
    }

    public Collection loadAll(java.util.Date date, String [] prefNet,
                    String [] prefSeedchan, String [] prefChannel) {
        return loadAll(date, prefNet, prefSeedchan, prefChannel, null);
    }

    public Collection loadAll(java.util.Date date, String [] prefNet,
                    String [] prefSeedchan, String [] prefChannel,
                    String [] prefLocations) {
        return jasiDataReader.loadAll(KEY_TABLE, DataSource.getConnection(), date,
                        prefNet, prefSeedchan, prefChannel, prefLocations);
    }

    /**
    * Returns a Collection of objects created from data obtained from the specified connection
    * that are valid for the specified input date.
    * Note - individual lookups may be more time efficient, if one shot less then several hundred channels.
    */
    static public Collection loadAll(Connection conn, java.util.Date date) {
        return jasiDataReader.loadAll(conn, date);
    }

    /** Writes the data values of this instance to the default DataSource archive.*/
    public boolean commit() {
        if (!DataSource.isWriteBackEnabled()) return false;
        return dbaseInsert();
    }

    public JasiObject parseData(Object rsdb) {
      return parseResultSetDb((ResultSetDb) rsdb);
    }
    public JasiObject parseResultSetDb(ResultSetDb rsdb) {
      return parseResultSet((ResultSetDb) rsdb);
    }
    /**Access to the JasiDbReader to allow generic SQL queries. */
    public JasiDataReaderIF getDataReader() {
        return jasiDataReader;
    }

    public boolean hasChanged() {
        return corr.isUpdate() || corrFlag.isUpdate() || corrType.isUpdate();
    }

} // end of class MlAmpMagCalibrTN
