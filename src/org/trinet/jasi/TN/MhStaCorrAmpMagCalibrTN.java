package org.trinet.jasi.TN;

import java.sql.*;
import java.util.*;
import org.trinet.jasi.*;
import org.trinet.jdbc.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.jdbc.table.*;
import org.trinet.util.*;

/**
* Type for Mh coda calibrations origininating from TriNet using NCDC schema.
*/
public class MhStaCorrAmpMagCalibrTN extends MhAmpMagCalibration implements DbReadableJasiChannelObjectIF {

    protected boolean fromDbase = false;

    protected final static String KEY_TABLE = "ChannelMap_AmpParms"; // used to be "ChannelMap_AmpClip x";
    //protected final static String KEY_TABLE = "ChannelMap_MagParms"; // used to be "ChannelMap_AmpParms x"; // aww 08/04
    protected final static String KEY_TABLE_ALIAS = "x";
    protected final static String OUTER_JOIN_TABLE = "StaCorrections";
    protected final static String SQL_SELECT_PREFIX =
    //"SELECT x.net,x.sta,x.seedchan,x.location,x.ondate,x.offdate,x.max,x.clip,x.gain_corr"+ // extra gain column
    "SELECT x.net,x.sta,x.seedchan,x.location,x.ondate,x.offdate,x.clip"+ // replaced by below 08/04 aww
    //"SELECT x.net,x.sta,x.seedchan,x.location,x.ondate,x.offdate,x.maxcnts,x.ampclip"+ // restored above 10/04 aww
      ",sc.corr,sc.corr_type,sc.corr_flag,sc.auth FROM "+ KEY_TABLE+" x LEFT OUTER JOIN "+OUTER_JOIN_TABLE+
      " sc ON sc.net=x.net AND sc.sta=x.sta AND sc.seedchan=x.seedchan AND sc.location=x.location AND "+
      //" sc.channel=x.channel AND" + // temporary tie-breaker until location codes fixed - aww 06/04
      "sc.corr_type = 'mh' AND sc.offdate > SYS_EXTRACT_UTC(CURRENT_TIMESTAMP)"; // assumes using current active "corr" 

    static protected JasiChannelDbReader jasiDataReader =  new JasiChannelDbReader();
    {
      jasiDataReader.setFactoryClassName(getClass().getName());
      jasiDataReader.setDebug(JasiDatabasePropertyList.debugSQL);
    }

    public MhStaCorrAmpMagCalibrTN() {
        this(null, null);
    }

    public MhStaCorrAmpMagCalibrTN(ChannelIdIF id) {
        this(id, null);
    }
    public MhStaCorrAmpMagCalibrTN(ChannelIdIF id, DateRange dateRange) {
        super(id, dateRange);
    }
    public MhStaCorrAmpMagCalibrTN(ChannelIdIF id, DateRange dateRange, double value) {
        this(id, dateRange, value, null, null);
    }
    public MhStaCorrAmpMagCalibrTN(ChannelIdIF id, DateRange dateRange, double value, 
                    String corrFlag) {
        super(id, dateRange, value, corrFlag, null);
    }
    public MhStaCorrAmpMagCalibrTN(ChannelIdIF id, DateRange dateRange, double value,
                    String corrFlag, String authority) {
        super(id, dateRange, value, corrFlag, authority);
    }

    public String toChannelSQLSelectPrefix() {
        return SQL_SELECT_PREFIX;
    }

    public String toChannelSQLSelectPrefix(java.util.Date date) {
      return 
      //"SELECT x.net,x.sta,x.seedchan,x.location,x.ondate,x.offdate,x.max,x.clip,x.gain_corr"+ // extra gain_corr column
      "SELECT x.net,x.sta,x.seedchan,x.location,x.ondate,x.offdate,x.clip"+ // replaced 08/04 by below
      //"SELECT x.net,x.sta,x.seedchan,x.location,x.ondate,x.offdate,x.maxcnts,x.ampclip"+ // restored above 10/26/04 aww
      ",sc.corr,sc.corr_type,sc.corr_flag,sc.auth FROM "+KEY_TABLE+" x LEFT OUTER JOIN "+OUTER_JOIN_TABLE+
      " sc ON sc.net=x.net AND sc.sta=x.sta AND sc.seedchan=x.seedchan AND sc.location=x.location AND"+
      //" sc.channel=x.channel AND" + // temporary tie-breaker until location codes fixed - aww 06/04
      " sc.corr_type = 'mh' AND" +
      " sc.offdate > SYS_EXTRACT_UTC(CURRENT_TIMESTAMP)" + 
      // instead of above SYDATE constraint use FULLDATE not just offdate :
      // DataTableRowUtil.toDateConstraintSQLWhereClause("sc", date) +
      " WHERE " + DataTableRowUtil.toDateConstraintSQLWhereClause(KEY_TABLE_ALIAS, date); // constrain driving table
    }

    /** Return count of all channels in data source at specified time. */
    public int getCount(java.util.Date date) {
      return jasiDataReader.getCountBySQL(DataSource.getConnection(), toChannelSQLSelectPrefix(date));
    }

    /**
     * Returns an instance valid for the specified input channel id and date.
     * If input date is null, returns most recently available data for the input channel identifier.
     * Returns null if no data are found satisfying input criteria.
    */
    public ChannelDataIF getByChannelId(ChannelIdIF id, java.util.Date date) {
        return (ChannelDataIF) jasiDataReader.getByChannelId(KEY_TABLE_ALIAS, id, date); // use alias for name
    }
    public Collection getByChannelId(ChannelIdIF id, DateRange dr) {
        return jasiDataReader.getByChannelId(KEY_TABLE_ALIAS, id, dr);
    }


    /**
    * Returns an instance whose data members values are parsed from the input ResultSetDb object.
    */
    protected static MhStaCorrAmpMagCalibrTN parseResultSet(ResultSetDb rsdb) {
        MhStaCorrAmpMagCalibrTN calibr = new MhStaCorrAmpMagCalibrTN();
        try {
            int offset = 0;
            ResultSet rs = rsdb.getResultSet();
            calibr.channelId =
                jasiDataReader.parseChannelIdKeyByOffset(calibr.channelId, offset, rs);
            offset = 4;

            if (calibr.dateRange == null) calibr.dateRange = new DateRange();
            // 2005/04/05 -removed aww
            //calibr.dateRange.setMin(rs.getTimestamp(++offset));
            //calibr.dateRange.setMax(rs.getTimestamp(++offset));
            // String to UTC Date because jdbc times are shifted to local tz millisecs (PST)
            String dstr = rs.getString(++offset);
            if (dstr != null)
              if (dstr.indexOf('.',dstr.length()-4) < 0) dstr += ".0";
              calibr.dateRange.setMin(EpochTime.stringToDate(dstr)); // GMT in db ok -aww 2008/02/11
            dstr = rs.getString(++offset);
            if (dstr != null)
              if (dstr.indexOf('.',dstr.length()-4) < 0) dstr += ".0";
              calibr.dateRange.setMax(EpochTime.stringToDate(dstr)); // GMT in db ok -aww 2008/02/11

            DataDouble dd = null;

            /* Removed attribute from schema table
            dd = rsdb.getDataDouble(++offset);
            if ( !dd.isNull()) {
              calibr.maxAmp.setValue(dd);
              // table does not currently store units
              // could use UnitsAmp or UnitsDataDouble type thus:
              // calibr.maxAmp.setUnits(CalibrUnits.COUNTS);
            }
            */

            dd = rsdb.getDataDouble(++offset);
            if ( !dd.isNull()) {
              calibr.clipAmp.setValue(dd);
              //calibr.clipAmp.setUnits(CalibrUnits.COUNTS);
            }
            /*
            dd = rsdb.getDataDouble(++offset);
            if ( !dd.isNull()) {
              calibr.gainCorr.setValue(dd);
              //calibr.setGainCorrUnits(CalibrUnits.MAGNITUDE);
            }
            */

            DataObject tmp = rsdb.getDataDouble(++offset);
            if (! tmp.isNull()) calibr.corr.setValue(tmp);

            tmp  = rsdb.getDataString(++offset);
            if (! tmp.isNull()) calibr.corrType.setValue(tmp.toString());
            else calibr.corrType.setValue(CorrTypeIdIF.MH); // missing, data so hard code override same as type in query

            tmp = rsdb.getDataString(++offset);
            if (! tmp.isNull()) calibr.corrFlag.setValue(tmp.toString());

            tmp = rsdb.getDataString(++offset);
            if (! tmp.isNull()) calibr.authority.setValue(tmp.toString());
        }
        catch (SQLException ex) {
          System.out.println("ERROR parsing Mh calibration data from result set.");
          ex.printStackTrace();
          return null;
        }
        calibr.fromDbase = true; // flag as db acquired
        return calibr;
    }


    public boolean isFromDataSource() { return fromDbase; }

    private StaCorrections toStaCorrectionsRow() {
        StaCorrections newRow = new StaCorrections();
        newRow.setUpdate(true); // set flag to enable processing
        // not null, or error results on table insert
        DataTableRowUtil.setRowChannelId(newRow, channelId) ;
        if (! corr.isNull())
                newRow.setValue(StaCorrections.CORR, corr);

        if (! corrType.equalsValue(UNKNOWN_STRING_TYPE))
                newRow.setValue(StaCorrections.CORR_TYPE, corrType);

        java.util.Date minDate = dateRange.getMinDate();
        if (minDate != null) 
                newRow.setValue(StaCorrections.ONDATE, minDate);

        // nullable table columns fields
        if (! corrFlag.equalsValue(UNKNOWN_STRING_TYPE))
                newRow.setValue(StaCorrections.CORR_FLAG, corrFlag);

        if (! authority.equalsValue(UNKNOWN_STRING_TYPE))
                newRow.setValue(StaCorrections.CORR_FLAG, corrFlag);

        if (dateRange.hasMaxLimit())
                newRow.setValue(StaCorrections.OFFDATE, dateRange.getMaxDate());

        return newRow;
    }

    protected boolean dbaseInsert () {                      // aka JasiReading
        boolean status = true;
        StaCorrections row = toStaCorrectionsRow();
        //System.out.println("MhAmpMagCalibrTN dbaseInsert row.toString(): " + row.toString());
        if (fromDbase) {
            row.setProcessing(DataTableRowStates.UPDATE);
            status = (row.updateRow(DataSource.getConnection()) > 0);
        }
        else {
            row.setProcessing(DataTableRowStates.INSERT);
            status = (row.insertRow(DataSource.getConnection()) > 0);
        }
        if (status) {
            fromDbase = true; // now its "from" the dbase
        }
        return status;
    }

    /**
     * Returns a Collection of objects from the default DataSource regardless of date.
     * There may be multiple entries for each channel that represent changes through time.
     * Uses the default DataSource Connection.
    */
    public Collection loadAll() {
        return jasiDataReader.loadAll(DataSource.getConnection());
    }

    /**
     * Return Collection of objects that were valid on the input date.
     * Data is retrieved from the default DataSource Connection.
    */
    public Collection loadAll(java.util.Date date) {
        return loadAll(DataSource.getConnection(), date);
    }

    public Collection loadAll(java.util.Date date, String[] prefSeedchan) {
        return loadAll(date, null, prefSeedchan, null);
    }

    public Collection loadAll(java.util.Date date, String [] prefNet,
                    String [] prefSeedchan, String [] prefChannel) {
        return loadAll(date, prefNet, prefSeedchan, prefChannel, null);
    }

    public Collection loadAll(java.util.Date date, String [] prefNet,
                    String [] prefSeedchan, String [] prefChannel,
                    String [] prefLocations) {
        return jasiDataReader.loadAll(KEY_TABLE_ALIAS, DataSource.getConnection(), date, // use alias for name
                        prefNet, prefSeedchan, prefChannel, prefLocations);
    }

    /**
    * Returns a Collection of objects created from data obtained from the specified connection
    * that are valid for the specified input date.
    * Note - individual lookups may be more time efficient, if one shot less then several hundred channels.
    */
    static public Collection loadAll(Connection conn, java.util.Date date) {
        return jasiDataReader.loadAll(conn, date);
    }

    /** Writes the data values of this instance to the default DataSource archive.*/
    public boolean commit() {
        if (!DataSource.isWriteBackEnabled()) return false;
        return dbaseInsert();
    }


    public JasiObject parseData(Object rsdb) {
      return parseResultSetDb((ResultSetDb) rsdb);
    }
    public JasiObject parseResultSetDb(ResultSetDb rsdb) {
      return parseResultSet((ResultSetDb) rsdb);
    }
    /**Access to the JasiDbReader to allow generic SQL queries. */
    public JasiDataReaderIF getDataReader() {
        return jasiDataReader;
    }

    public boolean hasChanged() {
        return corr.isUpdate() || corrFlag.isUpdate() || corrType.isUpdate();
    }

} // end of class MhStaCorrAmpMagCalibrTN
