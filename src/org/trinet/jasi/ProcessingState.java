package org.trinet.jasi;
import java.io.Serializable;

public class ProcessingState implements Cloneable, JasiProcessingConstants, java.io.Serializable {

    private static final String tag[] = { // "?", "A", "H", "I', "F", "C"
            JasiProcessingConstants.STATE_UNKNOWN_TAG,
            JasiProcessingConstants.STATE_AUTO_TAG,
            JasiProcessingConstants.STATE_HUMAN_TAG,
            JasiProcessingConstants.STATE_INTERMEDIATE_TAG,
            JasiProcessingConstants.STATE_FINAL_TAG,
            JasiProcessingConstants.STATE_CANCELLED_TAG
    };

    private static final String label[] = {
            JasiProcessingConstants.STATE_UNKNOWN_LABEL,
            JasiProcessingConstants.STATE_AUTO_LABEL,
            JasiProcessingConstants.STATE_HUMAN_LABEL,
            JasiProcessingConstants.STATE_INTERMEDIATE_LABEL,
            JasiProcessingConstants.STATE_FINAL_LABEL,
            JasiProcessingConstants.STATE_CANCELLED_LABEL
    };

    // Do this after above init, else NULL pointer reference in init of new ProcessingState(int) re isValidIndex ref to tag array
    public static final ProcessingState UNKNOWN = new ProcessingState(JasiProcessingConstants.STATE_UNKNOWN);
    public static final ProcessingState AUTO = new ProcessingState(JasiProcessingConstants.STATE_AUTO);
    public static final ProcessingState HUMAN = new ProcessingState(JasiProcessingConstants.STATE_HUMAN);
    public static final ProcessingState INTERMEDIATE = new ProcessingState(JasiProcessingConstants.STATE_INTERMEDIATE);
    public static final ProcessingState FINAL = new ProcessingState(JasiProcessingConstants.STATE_FINAL);
    public static final ProcessingState CANCELLED = new ProcessingState(JasiProcessingConstants.STATE_CANCELLED);

    private int indexId = 0; // instance id

    protected ProcessingState() { }

    protected ProcessingState(int idx) {
        if (! isValidIndex(idx))
            throw new IllegalArgumentException("Input state index" +idx+ " is invalid, value must be between 0 and "+(tag.length-1));
        indexId = idx;
    }

    // instance methods:
    public int getId() { return indexId; }
    public String getLabel() { return label[indexId]; }
    public String getTag() { return tag[indexId]; }
    public boolean isAuto() { return (indexId == JasiProcessingConstants.STATE_AUTO); }
    public boolean isCancelled() { return (indexId == JasiProcessingConstants.STATE_CANCELLED); }
    public boolean isFinal() { return (indexId == JasiProcessingConstants.STATE_FINAL); }
    public boolean isHuman() { return (indexId == JasiProcessingConstants.STATE_HUMAN); }
    public boolean isIntermediate() { return (indexId == JasiProcessingConstants.STATE_INTERMEDIATE); }
    public boolean isUnknown() { return (indexId == JasiProcessingConstants.STATE_UNKNOWN); }

    public String toString() {
        return getTag();
    }

    public boolean equals(Object obj) {
        if (obj == null || ! (obj instanceof ProcessingState) ) return false;
        ProcessingState ps = (ProcessingState) obj;
        return (indexId == ps.indexId);
    }
    public Object clone() {
        ProcessingState ps = null;
        try {
           ps = (ProcessingState) super.clone();
        }
        catch (CloneNotSupportedException ex) {
           ex.printStackTrace();
        }
        return ps;
    }

    // class methods:
    public static String getLabel(int state) {
        return label[state];
    }
    public static String getTag(int state) {
        return tag[state];
    }
    public static String[] getLabelArray() {
        return label;
    }
    public static String[] getTagArray() {
        return tag;
    }
    /** Returns index of tag String equal to input String
     * Returns STATE_UNKNOWN if no match.
     */
    public static int tagToId(String idTag) {
      for (int i=0; i < tag.length; i++) {
        if (tag[i].equals(idTag)) return i;
      }
      return JasiProcessingConstants.STATE_UNKNOWN;
    }

    /** Returns index of label String equal to input String
     * Returns STATE_UNKNOWN if no match.
     */
    public static int labelToId(String sLabel) { // renamed method - aww
      // DK NEW ADDED  061002
      for (int i=0; i < label.length; i++) {
        if(label[i].equals(sLabel)) return i;
      }
      return JasiProcessingConstants.STATE_UNKNOWN; // no match found
    }

    public static String labelToTag(String sLabel) {
      for (int i=0; i < label.length; i++) {
        if (label[i].equals(sLabel)) return tag[i];
      }
      return null;
    }

    public static String tagToLabel(String idTag) {
      for (int i=0; i < tag.length; i++) {
        if (tag[i].equals(idTag)) return label[i];
      }
      return null;
    }

    public static boolean isKnownTag(String idTag) {
        return (tagToLabel(idTag) != null);
    }

    public static boolean isKnownLabel(String sLabel) {
        return (labelToTag(sLabel) != null);
    }

    public static boolean isValidIndex(int idx) {
        return (idx >= 0 && idx < tag.length);
    }

}
