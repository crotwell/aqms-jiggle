// OVERLOADING VS. OVERIDDEN METHODS !
// CHECK EFFECT OF "IF"-TYPE BROADENING CASTS CASTS IN CLASS HIERARCHY,
// IS CORRECT OVERLOADED METHOD INVOKED IN SUBCLASSES?
// COMPILE TIME ARG-TYPE METHOD SIGNATURE USED, NOT RUNTIME ARG TYPE.
//
// TODO: Between the List types, Solution, Magnitude and their Reading types
// rework un/delete() such that the associated un/delete() delegates to the associator
// and the associators or the generic Lists methods use the setDeleteFlag flag
// rather than the un/delete() to toggle the flag, else feedback loop.
//
package org.trinet.jasi;
import org.trinet.jdbc.datatypes.DataObject;
import javax.swing.event.*;
//import org.trinet.util.StateChange;
//
// Changed hierarchy switched inheritance positions with JasiSourceAssocObject - aww 10/03
// since data is not necessarily commitable, deletable, but usually has a source/authority attribute
//public abstract class CommitableJasiObject extends JasiObject implements JasiCommitableIF {
public abstract class CommitableJasiObject extends SourceAssocJasiObject implements JasiCommitableIF {

  protected boolean needsCommit = false;
  protected boolean deleteFlag = false; 


  // "identifier" attribute is declared in subclasses, like DataLong for row sequence ids?
  // "type" attribute in declared in subclasses, DataString or String type?
  // Do we need a "commitDate" attribute here for use by subclasses, cf. "lddate"?

  /* Implement if objects attibutes are added to class.
  public Object clone() {
    return super.clone();
  }
  */

  abstract public JasiObject parseData(Object data);
  abstract public String toNeatString();
  abstract public String getNeatHeader();
  /**
  * Delete this reading. This is a "virtual delete". 
  * It is  not actually removed from memory or the source. 
  * Other classes must decide how they want to handle deleted readings when
  * commit() is called.
  * Override this method to add additional behavior.
  */
    public boolean delete() {
      setDeleteFlag(true);
      return true;
    }
    // aww until redesign to avoid unassociation list removal in subclass
    public boolean undelete() {
      setDeleteFlag(false);
      return true;
    }
    /** delete/undelete method implementation details overriden by dependent classes
     *  may need to override this method as well.
     */
    public boolean setDeleteFlag(boolean tf) {
      if (deleteFlag == tf) return false;
      deleteFlag = tf;
      //
      // For robust notification to work interested objects such as lists
      // would have to be added as a change listener for each element then
      // on change notification notify their respective list listeners.
      // Requires adding/removing the list as listener upon add/remove
      // of said element from the list.
      // Thus the below could be implemented to inform list:
      //fireStateChanged(new StateChange(this, "deleted", (isDeleted()) ? Boolean.TRUE:Boolean.FALSE));
      return true;
    }
    /** Returns true if this phase has been virtually deleted */
    public boolean isDeleted() {
      return deleteFlag;
    }


/** Commit any additions, changes or deletions to the data source. 
* The action taken depends on this instance creation state and data processing state.
* Member data may or may not have been initialized from the data source.
* As a result of commit, data be modified or unchanged in the DataSource.
*/
  abstract public boolean commit() throws JasiCommitException;
  abstract public boolean getNeedsCommit();
  abstract public boolean hasChanged();
  public void setNeedsCommit(boolean tf) { needsCommit = tf;}

  abstract public void setIdentifier(Object id);
  abstract public Object getIdentifier();
  /** Returns true if identifiers are not null, values are equivalent, 
   * If DataObjects are used as identifiers, returns false if the identifiers
   * are set to the DEFAULT undefined values.
   * This is prevent objects with such identifiers without other 
   * disciminating attributes from being matched. Override this method
   * in subclasses if matching null or undefined values should be equivalent.
   * */
  public boolean idEquals(Object identifier) {
    Object id = getIdentifier();
    if (id == null) return false;
    if (id instanceof DataObject) {
      DataObject myId = (DataObject) id;
      return (! myId.isNull() && myId.equalsValue(identifier)); 
    }
    return id.equals(identifier);
  }
  public boolean idEquals(JasiCommitableIF jc) {
    return (getClass().isInstance(jc)) ? idEquals(jc.getIdentifier()) : false;
  }
  abstract public JasiCommitableIF getById(Object id);
  abstract public Object getTypeQualifier();

  public boolean isSameType(Object type) { // subtype local, teleseism, ...
    return ( getTypeQualifier().equals(type) ) ? true : false;
  }
  public boolean isSameType(JasiCommitableIF jc) {
    return (getClass().isInstance(jc)) ? isSameType(jc.getTypeQualifier()) : false;
  }
  /**
  * Have to have guarantee unique identifiers for equivalent to work.
  * True => subclass equivalent, isSameType(), and idEquals().
  */
  public boolean equivalent(Object obj) {
    if (this == obj) return true;
    if (! (obj instanceof JasiCommitableIF)) return false;
    JasiCommitableIF jc = (JasiCommitableIF) obj;
    //System.err.println("Debug: equivalent jasiobject sameType: " + isSameType(jc));
    //System.err.println("Debug: equivalent jasiobject idEquals: " + idEquals(jc));
    //System.err.println("Debug: equivalent jasiobject isNull: " + (jc == null));
    return (jc != null && isSameType(jc) && idEquals(jc)); //  ? true : false;
  }
}
