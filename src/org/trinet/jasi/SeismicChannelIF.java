package org.trinet.jasi;
import java.util.*;
import org.trinet.util.*;
import org.trinet.util.gazetteer.*;
import org.trinet.jdbc.table.*;
public interface SeismicChannelIF extends Channelable, Correctable, AuthChannelIdIF {
    public ChannelName getChannelName() ;
    public Channel setChannelName(ChannelName cname) ;
    public Channel clearChannelName() ;

    public double getSensorDepth() ;
    public void setSensorDepth(double value) ;
    public double getSensorDip() ;
    public void setSensorDip(double value) ;
    public double getSensorAzimuth() ;
    public void setSensorAzimuth(double value) ;

    public LatLonZ getLatLonZ() ;
    public boolean hasLatLonZ() ;
    public void setLatLonZ(LatLonZ latLonZ) ;

    public double calcDistance(GeoidalLatLonZ loc) ; // changed input arg type aww 06/11/2004
    public void setDistanceBearingNull() ;
    public double getDistance() ;
    public double getHorizontalDistance() ;
    public double getVerticalDistance() ;
    public void setDistance(double distance) ;
    public void setHorizontalDistance(double distance) ;
    public double getAzimuth() ;
    public void setAzimuth(double az) ;

    public boolean hasCorrection(java.util.Date date, String corrType) ;
    public DataCorrectionIF getCorrection(java.util.Date date, String corrType) ;

    public boolean hasGain(java.util.Date date) ;
    public ChannelGain getGain(java.util.Date date) ;

    public ChannelResponse getResponse(java.util.Date date) ;
    public void setResponse(ChannelResponse cr) ;

    public double getMaxAmpValue(java.util.Date date) ;
    public double getClipAmpValue(java.util.Date date) ;

    public boolean equalsName(ChannelIdIF id) ; 
    public Channel copy(Channelable chan) ;
    public Channel lookUp(Channelable chan, java.util.Date date) ;
    //public Channel lookUpLatest(Channelable chan) ;
    public ChannelList readList(java.util.Date date);
    public ChannelList readListByName(String name, java.util.Date date);
    public ChannelList getByComponent(String[] compList, java.util.Date date) ;

    public void setAutoLoadingOfAssocDataOn();
    public void setAutoLoadingOfAssocDataOff();

    public int loadKnownCorrections(java.util.Date date) ;
    public boolean loadCorrection(ChannelCorrectionDataIF corr, java.util.Date date) ;
    public boolean loadCorrections(ChannelCorrectionDataIF corr, DateRange dr) ;
    public void loadAssocData(java.util.Date date) ;
    public boolean loadResponse(java.util.Date date) ;
    //public boolean loadClippingAmps(java.util.Date date) ;
    //
    public boolean isVelocity();
    public boolean isAcceleration();
}
