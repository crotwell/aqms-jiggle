package org.trinet.jasi;

/**
 * Exception class that gets thrown when a method is passed an inapproriate
 * Magnitude object for its processing.
 *
*/
public class WrongMagTypeException extends WrongDataTypeException {
    public WrongMagTypeException(String msg) {
	super(msg);
    }    
} // WrongMagTypeException
