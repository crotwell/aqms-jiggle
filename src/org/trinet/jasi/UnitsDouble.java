package org.trinet.jasi;
import org.trinet.jdbc.datatypes.*;

//TODO: extend generic DataNumber types like DataDouble e.g. UnitsDataDouble etc.?
public class UnitsDouble implements Cloneable, java.io.Serializable {

/** The actual value. */
     protected DataDouble value = new DataDouble();
     /** The units of the value. Default is Units.UNKNOWN.
     * @See: Units */
     protected int units = Units.UNKNOWN;

     public UnitsDouble() { }

     public UnitsDouble(UnitsDouble data) {
         this.value  = data.value;
         this.units = data.units;
     }

     public UnitsDouble(double value, int units) { 
         this.value.setValue(value);
         this.units = units;
     }

     /** Return the units string, e.g. "counts", "counts/(cm/sec)"
     * @See: Units */
     public String getUnitsString() {
       return Units.getString(units);
     }

     /** Return the value of the units type.
     * @See: Units
     */
     public int getUnits() {
       return units;
     }
     /** Set the units of the value. Returns false is the string is not a legal
     * units description.
     * @See: Units */
     public void setUnits(int iunits) {
       units = iunits;
     }
     /** Set the units of the value. Returns false is the string is not a legal
     * units description.
     * @See: Units */
     public boolean setUnits(String sunits) {
        if (Units.isLegal(sunits)) {
          units = Units.getInt(sunits);
          return true;
        }
        return false;
     }

     /** Return true if the value is null. */
     public boolean isNull () {
       return value.isNull();
     }
     /** Return the value. Returns NaN if the value is null.*/
     public double doubleValue() {
       return value.doubleValue();
     }

     /** Set the value. Units should be set also but will default to counts
     */
     public void setValue(double value) {
       this.value.setValue(value);
     }
     public void setValue(Number value) {
       this.value.setValue(value);
     }
     public void setValue(DataNumber value) {
       if (value.isNull()) this.value.setNull(true);
       this.value.setValue(value);
     }
     public String toString (){
       return value + " " +getUnitsString();
     }
     public Object clone() {
       UnitsDouble data = null;
       try {
         data = (UnitsDouble) super.clone();
       }
       catch (CloneNotSupportedException ex) {
         ex.printStackTrace();
       }
       data.value = (DataDouble) value.clone();
       return data;
     }
}
