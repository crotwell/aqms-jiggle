package org.trinet.jasi;
import java.io.*;
public interface EventParserIF extends SolutionListParserIF {
  public boolean isDataTypeDelimiter(String s);
  public BufferedReader getReader(String fileName);
  public String getFileName();
  //public void initParser();
  //public void setGraphicsOwner(java.awt.Component owner);
  //public void setFileName(String filePathName);
  //public GenericSolutionList parseSolutions();
  //public Solution parseSolutions();
  public GenericSolutionList parseSolutions(BufferedReader reader);
  public Solution     parseSolution(BufferedReader reader);
  public Solution     parseSolution(String text);
  public void         parseMagList(BufferedReader reader, Solution sol); 
  public void         parseAmpList(BufferedReader reader, Solution sol);
  public void         parseAmpList(BufferedReader reader, Magnitude mag);
  public void         parsePhaseList(BufferedReader reader, Solution sol); 
  public Phase        parsePhase(String text);
  public Amplitude    parseAmp(String text);
  //public Magnitude  parseMagnitude(String text);
  //public Coda       parseCoda(String text);
  //public void       parseCodaList(BufferedReader reader, Solution sol); 
  //public void       parseCodaList(BufferedReader reader, Magnitude mag); 
}
