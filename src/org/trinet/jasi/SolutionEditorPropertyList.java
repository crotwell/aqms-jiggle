package org.trinet.jasi;
import java.util.*;
import org.trinet.util.velocitymodel.UniformFlatLayerVelocityModel;

/**
 * Jasi solution editor properties.
 * @see: JasiDatabasePropertyList
*/

public class SolutionEditorPropertyList extends JasiDatabasePropertyList {

    private String defaultVelocityModelName = null;
    protected VelocityModelList velocityModelList = new VelocityModelList();
    boolean authRegionEnabled = false;
    boolean resetAllEtypeAuth = false;

/**
 *  Constructor: no-op.
 *  @see: setRequiredProperties()
 */
    public SolutionEditorPropertyList() {
      // force programmer to setup defaults -aww?
    }

/**
 *  Constructor: Makes a COPY of a property list. Doesn't read the files.
 */
    //public SolutionEditorPropertyList(GenericPropertyList props) {
    public SolutionEditorPropertyList(SolutionEditorPropertyList props) {
        super(props);
        privateSetup();
    }

/**
 *  Construtor: reads this property file and System properties
 */
    public SolutionEditorPropertyList(String fileName)
    {
        super(fileName);
        privateSetup();
    }
    
/**
*  Constructor: input filenames are not appended to constructed paths, but rather are
*  interpreted as specified (either relative to current working directory or a complete path). 
*/
    public SolutionEditorPropertyList(String userPropFileName, String defaultPropFileName)
    {
        super(userPropFileName, defaultPropFileName);
        privateSetup();
    }

    /**
     * Setup class static, instance, and/or environment variables from current properties.
     * This method should be called after the pertinent properties are initially read
     * or changed through updates.
     */
    public boolean setup() {
      boolean status = super.setup();
      return (privateSetup() && status);
    }

    private boolean privateSetup() {

      // NOTE: Applications using multiple properties files of this subtype should declare the
      // velocity model property only in one, or make sure they are consistent because its value
      // in last property file initialized will the value set in static TravelTime.
 
      boolean status = true;

      // Could have property declare a string list of classname models - aww 2008/03/24
      String vModelClassName = getProperty("velocityModelClassName");

      // >>>>>>   Begin velocity model init change -aww 2008/03/24
      if (vModelClassName != null) {
          velocityModelList.addOrReplaceByClassname(vModelClassName);
      }

      UniformFlatLayerVelocityModel model = null;
      if (isSpecified("velocityModelList")) { // -aww 2008/03/24
          // Let's assume we don't want to clear list, just update or add to it for now -aww
          String [] modelNameList = getStringList("velocityModelList").toArray();
          model = null;
          for (int idx = 0; idx < modelNameList.length; idx++) {
              model = UniformFlatLayerVelocityModel.getNamedModelFromProps(modelNameList[idx], this);
              if (model != null) velocityModelList.addOrReplace(model);
          }
      }

      // Get default velocity model -aww 2008/03/18
      defaultVelocityModelName = getProperty("velocityModel.DEFAULT.modelName");
      if (defaultVelocityModelName != null) {
          model = velocityModelList.getByName(defaultVelocityModelName);
          if (model == null) { // should be on list, so if not, add it
              model = UniformFlatLayerVelocityModel.getNamedModelFromProps(defaultVelocityModelName, this);
              if (model != null) {
                  velocityModelList.addOrReplace(model);
                  velocityModelList.setSelected(model);
              }
          }
          else velocityModelList.setSelected(model);
      }

      // If no model in the list is selected, set the list selection to the first model
      if (velocityModelList.getSelected() == null && velocityModelList.size() > 0) {
          velocityModelList.setSelected(velocityModelList.get(0));
      }

      //NOTE whenever the VelocityModelList selection changes we need to synch default TravelTime model
      //because it is used by AbstractWaveform, ChannelTimeWindowModel, xxMagMethod etc.
      model = (UniformFlatLayerVelocityModel) velocityModelList.getSelected();
      if (model != null) { // synch
          TravelTime.setDefaultModel(model);
      }
      // <<<<<<   End velocity model init change -aww 2008/03/24

      //disable loading of associated channel data like corrections if db tables are missing
      if ( isSpecified("channelListReadOnlyLatLonZ") && getBoolean("channelListReadOnlyLatLonZ") )
          ChannelList.readOnlyChannelLatLonZ();  // aww 02/23/2005

      if (isSpecified("channelListAddOnlyDbFound")) // aww 02/23/2005
          MasterChannelList.setAddChannelsOnlyFromDataSource(getBoolean("channelListAddOnlyDbFound"));

      // is there a channel cache file defined ?
      if (isSpecified("channelCacheFilename"))
          ChannelList.setCacheFilename(getUserFileNameFromProperty("channelCacheFilename"));

      // Below property is to toggle ChannelMap_AmpParms clipping amp load
      //if (isSpecified("channelLoadClippingAmp"))
      //   Channel.setLoadClippingAmps(getBoolean("channelLoadClippingAmp"));

      //if (isSpecified("channelDefaultLocationCode"))
         //EnvironmentInfo.setLocationCode(getProperty("channelDefaultLocationCode"));

      //see ChannelName.toDelimitedNameString(String) to allow for net,sta,seed,channel,src string
      if (isSpecified("channelDbLookUp")) 
          Channel.setDbLookup(getBoolean("channelDbLookUp")); // added 2010/11/12 -aww

      if (isSpecified("channelUseFullName"))
          ChannelName.useFullName = getBoolean("channelUseFullName");

      // Property "channelMatchLocation" is temporary until db location code is cleaned up.
      // boolean toggles ChannelName.location matching in ChannelName equals for a reading.
      if (isSpecified("channelMatchLocation"))
          ChannelName.useLoc = getBoolean("channelMatchLocation");  // aww 03/04

      // Property "channelCopyOnlySNCL" is temporary until db location code is cleaned up.
      // boolean toggles ChannelName.location matching in ChannelName equals for a reading.
      if (isSpecified("channelCopyOnlySNCL"))
          ChannelName.copyOnlySNCL = getBoolean("channelCopyOnlySNCL");  // aww 03/25/2005

      // Property "channelMatchMode" below is for the resolution of the db lookUp joins.
      // matchMode defines extent to which channel name attributes are equijoined - aww 6/2004
      // valid values of resolution are: SEEDCHAN, LOCATION, CHANNELSRC, and AUTHSUBSRC
      String matchMode = getProperty("channelMatchMode", "LOCATION");
      if ( matchMode.equalsIgnoreCase("SEEDCHAN") )
        JasiChannelDbReader.defaultMatchMode = JasiChannelDbReader.SEEDCHAN;
      else if ( matchMode.equalsIgnoreCase("CHANNELSRC") )
        JasiChannelDbReader.defaultMatchMode = JasiChannelDbReader.CHANNELSRC;
      else
        JasiChannelDbReader.defaultMatchMode = JasiChannelDbReader.LOCATION;


      //Another switch value would be needed for discriminating duplicate data values
      //Say same channel and amp value of interest, but with different rowids, then
      //we would need a new method in abstract class like "equivalentValues()".
      // EQUIVALENT => ids equal and their channel data are equivalent
      //         ID => ids equal, but their data values may differ
      //For JasiReadingList elements the default replacement mode is EQUIVALENT. 
      matchMode = getProperty("listMatchMode", "EQUIVALENT");
      if ( matchMode.equalsIgnoreCase("ID") )
        AbstractCommitableList.matchMode = AbstractCommitableList.MATCH_ID;
      else
        AbstractCommitableList.matchMode = AbstractCommitableList.MATCH_EQUIVALENT;
      //
      // more resolved static behavior -aww 01/05/2005
      //
      // class level "static" switch
      if ( isSpecified("authRegionEnabled") ) { 
        authRegionEnabled = getBoolean("authRegionEnabled");
        Solution.authRegionEnabled = authRegionEnabled;
      }

      if ( isSpecified("resetAllEtypeAuth") ) { 
        resetAllEtypeAuth = getBoolean("resetAllEtypeAuth");
        Solution.resetAllEtypeAuth = resetAllEtypeAuth;
      }

      if ( isSpecified("postInterimCommit") ) // could be used by any processor
        Solution.postInterimCommit = getBoolean("postInterimCommit");

      if ( isSpecified("solCommitAllMags") ) // could be used by any processor
        Solution.commitAlternateMagnitudes = getBoolean("solCommitAllMags");

      // class level "static" switch
      if ( isSpecified("solCommitResetsRflag") ) // could be used by any processor
        Solution.commitResetsRflag = getBoolean("solCommitResetsRflag");

      // class level "static" switch
      if ( isSpecified("solCommitResetsAttrib") ) // could be used by any processor
        Solution.commitResetsAttrib = getBoolean("solCommitResetsAttrib", true);

      // class level "static" switch
      if ( isSpecified("solStaleCommitOk") ) // could be used by any processor
        Solution.staleCommitOk = getBoolean("solStaleCommitOk");

      if (isSpecified("solCommitOriginError")) // ? aww 07/12/2006 is this needed or do we want to alway save ?
        Solution.commitOriginError = getBoolean("solCommitOriginError");

      // class level "static" switch
      if ( isSpecified("waveformCommitOk") ) // could be used by any processor
        Solution.waveformCommitOk = getBoolean("waveformCommitOk");

      // class level "static" switch
      if ( isSpecified("alarmDbLinks") )
        Solution.alarmDbLinks = getStringArray("alarmDbLinks");

      // temporary class level "static" switch
      if ( isSpecified("useEventPreforTable") )
        Solution.useEventPreforTable = getBoolean("useEventPreforTable");

      // class level "static" switch
      if ( isSpecified("autoSetPrefOr") ) // could be used by any processor
        Solution.autoSetPrefOr = getBoolean("autoSetPrefOr");

      // class level "static" switch
      if ( isSpecified("autoSetPrefMag") ) // could be used by any processor
        Solution.autoSetPrefMag = getBoolean("autoSetPrefMag");

      // class level "static" switch
      if ( isSpecified("magStaleCommitOk") ) // could be used by any processor
        Magnitude.staleCommitOk = getBoolean("magStaleCommitOk");

      // class level "static" switch
      if ( isSpecified("magStaleCommitNoop") ) // could be used by any processor
        Magnitude.staleCommitNoop = getBoolean("magStaleCommitNoop");

      if (isAttributionEnabled()) {
          Magnitude.dbAttributionEnabled = true;
          Solution.dbAttributionEnabled = true;
      }

      return status;
    }

    /** Return a List of property names used
     * by application classes utilizing this class.
     * */
    public List getKnownPropertyNames() {
        List aList = super.getKnownPropertyNames();
        List myList = classDeclaredPropertyNames();
        if (aList instanceof ArrayList)
            ((ArrayList)aList).ensureCapacity(aList.size() + myList.size());
        aList.addAll(myList);
        Collections.sort(aList);
        return aList;
    }

    public static List classDeclaredPropertyNames() {
        ArrayList myList = new ArrayList(24);

        myList.add("autoSetPrefMag");
        myList.add("autoSetPrefOr");
        myList.add("authRegionEnabled");
        myList.add("channelCacheFilename");
        myList.add("channelGroupName");
        myList.add("channelListCacheRead");
        myList.add("channelListCacheWrite");
        myList.add("channelListReadOnlyLatLonZ");
        myList.add("channelListAddOnlyDbFound");
        myList.add("channelMatchLocation");
        myList.add("channelMatchMode");
        myList.add("channelDbLookUp");
        myList.add("channelDebugDump");
        myList.add("channelUseFullName");
        myList.add("eventSelectionProps");
        myList.add("eventSelectionDefaultProps");
        myList.add("listMatchMode");
        myList.add("magStaleCommitOk");
        myList.add("magStaleCommitNoop");
        myList.add("resetAllEtypeAuth");
        myList.add("solCommitAllMags");
        myList.add("solCommitOriginError");
        myList.add("solStaleCommitOk");
        myList.add("velocityModelClassName");
        myList.add("velocityModel.DEFAULT.modelName");
        myList.add("velocityModelList");
        myList.add("waveformCommitOk");

        Collections.sort(myList);

        return myList;
    }

    public boolean getUseChannelLoc() {
        return getBoolean("channelMatchLocation");
    }
    public void setUseChannelLoc(boolean tf) {
        setProperty("channelMatchLocation", tf);
    }

    public boolean getChannelUseFullName() {
        return getBoolean("channelUseFullName");
    }
    public void setChannelUseFullName(boolean tf) {
        setProperty("channelUseFullName", tf);
    }

    // EQUIVALENT or ID
    public String getListMatchMode() {
        return getProperty("listMatchMode");
    }
    // EQUIVALENT or ID
    public void setListMatchMode(String mode) {
        setProperty("listMatchMode", mode);
    }

    // LOCATION or CHANNELSRC
    public String getChannelMatchMode() {
        return getProperty("channelMatchMode");
    }
    // LOCATION or CHANNELSRC
    public void setChannelMatchMode(String mode) {
        setProperty("channelMatchMode", mode);
    }

    public String getChannelCacheFilename() {
        return getProperty("channelCacheFilename");
    }
    public void setChannelCacheFilename(String filename) {
        setProperty("channelCacheFilename", filename);
    }

    public String getChannelGroupName() {
        return getProperty("channelGroupName");
    }
    public void setChannelGroupName(String name) {
        setProperty("channelGroupName", name);
    }

    /** If property is not present method returns true. */
    public boolean getChannelDbLookUp() { // force true to match magmethod magengine behavior -aww
        return isSpecified("channelDbLookUp") ? getBoolean("channelDbLookUp") : true;
    }
    public void setChannelDbLookUp(boolean tf) {
        setProperty("channelDbLookUp", tf);
    }
    public boolean getChannelDebugDump() {
        return getBoolean("channelDebugDump");
    }
    public void setChannelDebugDump(boolean tf) {
        setProperty("channelDebugDump", tf);
    }
    public boolean getChannelListCacheRead() {
        return getBoolean("channelListCacheRead");
    }
    public void setChannelListCacheRead(boolean tf) {
        setProperty("channelListCacheRead", tf);
    }
    public boolean getChannelListCacheWrite() {
        return getBoolean("channelListCacheWrite");
    }
    public void setChannelListCacheWrite(boolean tf) {
        setProperty("channelListCacheWrite", tf);
    }
    public boolean getChannelListReadOnlyLatLonZ() {
        return getBoolean("channelListReadOnlyLatLonZ");
    }
    public void setChannelListReadOnlyLatLonZ(boolean tf) {
        setProperty("channelListReadOnlyLatLonZ", tf);
    }
    public boolean getChannelListAddOnlyDbFound() {
        return getBoolean("channelListAddOnlyDbFound");
    }
    public void setChannelListAddOnlyDbFound(boolean tf) {
        setProperty("channelListAddOnlyDbFound", tf);
    }

    public String getEventSelectionProps() {
        return getProperty("eventSelectionProps");
    }
    public void setEventSelectionProps(String filename) {
        setProperty("eventSelectionProps", filename);
    }
    public String getEventSelectionDefaultProps() {
        return getProperty("eventSelectionDefaultProps");
    }
    public void setEventSelectionDefaultProps(String filename) {
        setProperty("eventSelectionDefaultProps", filename);
    }

    public boolean getCommitAllMags() {
        return getBoolean("solCommitAllMags");
    }
    public void setCommitAllMags(boolean tf) {
        setProperty("solCommitAllMags", tf);
    }

    public boolean getMagStaleCommitOk() {
        return getBoolean("magStaleCommitOk");
    }
    public void setMagStaleCommitOk(boolean tf) {
        setProperty("magStaleCommitOk", tf);
    }

    public boolean getMagStaleCommitNoop() {
        return getBoolean("magStaleCommitNoop");
    }
    public void setMagStaleCommitNoop(boolean tf) {
        setProperty("magStaleCommitNoop", tf);
    }

    public boolean getSolStaleCommitOk() {
        return getBoolean("solStaleCommitOk");
    }
    public void setSolStaleCommitOk(boolean tf) {
        setProperty("solStaleCommitOk", tf);
    }

    public boolean getWaveformCommitOk() {
        return getBoolean("waveformCommitOk");
    }
    public void setWaveformCommitOk(boolean tf) {
        setProperty("waveformCommitOk", tf);
    }

    public boolean getAutoSetPrefMag() {
        return getBoolean("autoSetPrefMag");
    }
    public void setAutoSetPrefMag(boolean tf) {
        setProperty("autoSetPrefMag", tf);
    }

    public boolean getAutoSetPrefOr() {
        return getBoolean("autoSetPrefOr");
    }
    public void setAutoSetPrefOr(boolean tf) {
        setProperty("autoSetPrefOr", tf);
    }

    //
    // Method implementations below added to manage velocity model properties -aww 2008/03/24
    //
    public void addVelocityModel(UniformFlatLayerVelocityModel model) {
        model.toNamedModelProps(this);
        velocityModelList.addOrReplace(model);
    }

    public void setVelocityModelList(VelocityModelList modelList) {
        if (modelList == null) return;
        velocityModelList = modelList;
        setVelocityModelProperties();
    }

    private void setVelocityModelProperties() {

        if (velocityModelList == null) return;

        setProperty("velocityModelList", velocityModelList.getModelNames());

        UniformFlatLayerVelocityModel uvm = (UniformFlatLayerVelocityModel) velocityModelList.getSelected();
        if (uvm != null) {
          setDefaultVelocityModelName(uvm.getName());
        }
        else { // just in case the default model is still in list but is not the selected 
          velocityModelList.setSelected(velocityModelList.getByName(getDefaultVelocityModelName()));
        }

        for (int idx = 0; idx < velocityModelList.size(); idx++) {
             uvm = (UniformFlatLayerVelocityModel) velocityModelList.get(idx); 
             uvm.toNamedModelProps(this);
        }
    }

    public VelocityModelList getVelocityModelList() {
        return velocityModelList;
    }

    public void setDefaultVelocityModelName(String name) {
        UniformFlatLayerVelocityModel model = velocityModelList.getByName(name);
        if (model != null) {
           setProperty("velocityModel.DEFAULT.modelName", name);
           defaultVelocityModelName = name;
           velocityModelList.setSelected(model);
           //TravelTime.setDefaultModel(model); // do we want to do this here ? -aww
        }
    }

    public String getDefaultVelocityModelName() {
        return getProperty("velocityModel.DEFAULT.modelName");
    }

    public UniformFlatLayerVelocityModel getSelectedVelocityModel() {
        return (UniformFlatLayerVelocityModel) velocityModelList.getSelected();
    }

    public Object clone() {
        SolutionEditorPropertyList sepList = (SolutionEditorPropertyList) super.clone();
        sepList.velocityModelList = (VelocityModelList) this.velocityModelList.clone();
        return sepList;
    }

} // end of class
