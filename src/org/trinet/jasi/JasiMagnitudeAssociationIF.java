package org.trinet.jasi;
import java.util.Collection;
public interface JasiMagnitudeAssociationIF extends JasiAssociationFitIF {
  //Need copy & init methods like JasiSolutionAssociationIF case:
  //how do we descriminate "channel" level vs. "summary level" actions
  public void initMagDependentData();
  public void copyMagDependentData(JasiMagnitudeAssociationIF jr);

  public void assign(Magnitude mag); // simple assignment affiliation with input Magnitude, programmers convenience.
  public boolean isAssignedToMag(); // check for a non-null Magnitude affiliation assignment, no list membership check
  public boolean isAssignedTo(Magnitude mag); // assignment reference matches input Magnitude, no list membership check
  public void associate(Magnitude mag); // Affiliate with input Magnitude and add reference element to its collection.
  public Magnitude getAssociatedMag(); // returns Magnitude reference assigned. (No collection membership check)
  public boolean isAssociatedWithMag(); //check for non-null affiliation and membership in its collection.
  public boolean isAssociatedWith(Magnitude mag);
  public void unassociateMag(); // Null Magnitude assignment and remove any reference from its collection.
  public Collection getByMagnitude(Magnitude mag);
  public boolean containedBy(Magnitude mag);
  public boolean addTo(Magnitude mag); 
  public boolean addOrReplaceIn(Magnitude mag);
  public boolean removeFrom(Magnitude mag); // unassociates mag
  public boolean deleteFrom(Magnitude mag);
  public boolean assocEquals(JasiMagnitudeAssociationIF obj);
  //public void removeFromAssocMagList(); //? remove from list mag not nulled
}

