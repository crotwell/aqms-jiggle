//TODO: Get rid of "importance" here and in TN readings, no one uses it

package org.trinet.jasi;

import org.trinet.jdbc.datatypes.*;
import org.trinet.util.Format;                // CoreJava printf-like Format class

/**
 * Attributes of a magnitude for a single channel.
 *
 *
 * Created: Fri May 26 13:49:14 2000
 *
 * @author Doug Given
 * @version
 */

public class ChannelMag extends JasiObject implements ChannelMagIF {

    /** Corrected one-station mag for this amplitude for its associated solution. The
    * correction has already been added to this value. */
    public DataDouble value = new  DataDouble();

    /** The residual for this magnitude. It is calculated as: ChannelMag - EventMag. */
    public DataDouble residual = new  DataDouble();

    /** The importance of this amp to the summary magnitude (0.0 - 1.0).
    * Default = 0.0 */
    public DataDouble importance = new  DataDouble(0.0);
    // *** This seems the same as weight. I'm not using it here.

    /** The correction for this magnitude. Added to calculated mag. Default = 0.0  */
    public DataDouble correction = new DataDouble(0.0);

    /** The input weight assigned to this channel magnitude for to be used by a summary magnitude calculation. 
    * Range is 0. to 1. Default = 0. (zero implies do not use for summary calculation).*/
    public DataDouble inWgt = new DataDouble(0.0);

    /** The output (used) weight assigned to this channel magnitude by a summary magnitude calculation.
    * Range is 0. to 1. Default = 0. (zero implies not used for summary calculation)
    */
    public DataDouble weight = new DataDouble(0.0);

    /** Magnitude type as the subscript to be appended to M. Like M<b>l<\b>.*/
    public DataString subScript = new DataString();

    /** Default constructor. */
    public ChannelMag() {
        setUpdate(false);
    }

    public Object clone() {
      ChannelMag cm = (ChannelMag) super.clone();
      cm.value      = (DataDouble) this.value.clone();
      cm.residual   = (DataDouble) this.residual.clone();
      cm.importance = (DataDouble) this.importance.clone();
      cm.correction = (DataDouble) this.correction.clone();
      cm.inWgt      = (DataDouble) this.inWgt.clone();
      cm.weight     = (DataDouble) this.weight.clone();
      cm.subScript  = (DataString) this.subScript.clone();
      return cm;
    }

    /** Set values of this instance to those of the input.
     * Preserves the null and update status of the input. */
    public void copy(ChannelMag cm) {
      this.value.setValue(cm.value);
      this.value.setUpdate(cm.value.isUpdate());

      this.residual.setValue(cm.residual);
      this.residual.setUpdate(cm.residual.isUpdate());

      this.importance.setValue(cm.importance);
      this.importance.setUpdate(cm.importance.isUpdate());

      this.correction.setValue(cm.correction);
      this.correction.setUpdate(cm.correction.isUpdate());

      this.inWgt.setValue(cm.inWgt);
      this.inWgt.setUpdate(cm.inWgt.isUpdate());

      this.weight.setValue(cm.weight);
      this.weight.setUpdate(cm.weight.isUpdate());

      this.subScript.setValue(cm.subScript);
      this.subScript.setUpdate(cm.subScript.isUpdate());
    }


    /** Resets the data members to the initial state. */
    public void initialize() {
      subScript.setNull(true);
      value.setNull(true);
      correction.setValue(0.);
      initAssocSummaryMagData();
    }

    public void initAssocSummaryMagData() {
      importance.setValue(0.);
      inWgt.setValue(0.);
      weight.setValue(0.);
      residual.setNull(true);
    }

    /** Returns 'true' if the instance magnitude value attribue is null. */
    public boolean isNull() {
      return value.isNull();
    }

    /**
     *  Set the value of the Channel Magnitude
     */
    public void setMagValue(double magVal ) {
        value.setValue(magVal);
    }
    /** Returns value if valid number, else returns -9.0 */
    public double getMagValue() {
        //changed from -1. to -9. to make it obvious to user -aww 8/12/2004
        return (value.isValidNumber()) ? value.doubleValue() : -9.0;
    }
    /**
     * Set the channel's residual relative to this magnitude. It is calculated as:
     * Observed - Calculated which is interpreted to mean ChannelMag - EventMag.
     * This is consistent with phase pick residuals where a negative value means the
     * observed pick is early relative to the model. A negative magnitude residual means
     * the observed mag for this channel is smaller then the event mag.
     * */
    public void setResidual( Magnitude mag ) {
        // There is a constraint error in the dbase that will not allow negative
        // residuals.  Last checked 1/3/2001 - still wrong
        // constraint dropped on 04/29/2005 -aww
        // residual.setValue(Math.abs(value.doubleValue() - mag.value.doubleValue()));
        residual.setValue(value.doubleValue() - mag.value.doubleValue());
    }
    public double getResidual() {
        return (residual.isValidNumber()) ? residual.doubleValue() : Double.MAX_VALUE;
    }

    public void setInWgt(double wt) {
        inWgt.setValue(wt);
    }
    public double getInWgt() {
        return (inWgt.isValidNumber()) ? inWgt.doubleValue() : 0.;
    }
    public void setWeight(double wt) {
        weight.setValue(wt);
    }
    public double getWeight() {
        return (weight.isValidNumber()) ? weight.doubleValue() : 0.;
    }
    public void setCorrection(double corr) {
        correction.setValue(corr);
    }
    public double getCorrection() {
        return (correction.isValidNumber()) ? correction.doubleValue() : 0.;
    }
    public void setType(String magType) {
        subScript.setValue(magType);
    }
    public String getType() {
        return (subScript.isValid() ? subScript.toString() : " ");
    }

    public void setUpdate(boolean tf) {
        value.setUpdate(tf);
        subScript.setUpdate(tf);
        inWgt.setUpdate(tf);
        weight.setUpdate(tf);
        residual.setUpdate(tf);
        importance.setUpdate(tf);
        correction.setUpdate(tf);
    }
    public boolean hasChanged() {
        return (
                value.isUpdate() ||
                subScript.isUpdate() ||
                weight.isUpdate() ||
                residual.isUpdate() ||
                inWgt.isUpdate() ||
                importance.isUpdate() ||
                correction.isUpdate() 
               );
    }

    public String toString() {
        Format ff = new Format("%6.3f");
        return ff.form(value.doubleValue()) +
          " res: "+ ff.form(residual.doubleValue()) +
//        " imp: "+ ff.form(importance.doubleValue()) +
          " corr: "+ ff.form(correction.doubleValue()) +
          " inWgt: "+ ff.form(inWgt.doubleValue())+
          " wt: "+ ff.form(weight.doubleValue())+
          " type: M"+subScript;
    }

    /** Returns a fixed field width formated String describing the data attributes of this instance. */
    public String toNeatString() {
//        Format df = new Format("%9.4f");            // CORE Java Format class
        Format df1 = new Format("%5.2f");            // CORE Java Format class
        StringBuffer sb = new StringBuffer(64);
        sb.append(df1.form(value.doubleValue())).append(" ");
        sb.append(df1.form(residual.doubleValue())).append(" ");
//      sb.append(df.form(importance.doubleValue())).append(" ");
        sb.append(df1.form(correction.doubleValue())).append(" ");
        sb.append(df1.form(inWgt.doubleValue())).append(" ");
        sb.append(df1.form(weight.doubleValue()));
        return sb.toString();
    }

    public static String getNeatStringHeader() {
     // return "   ChnlMag  Residual   Quality      Corr    Weight";
     //      xxxxxxxxx ......... xxxxxxxxx ......... xxxxxxxxx
        return " ChMag Resid  Corr InWgt    Wt";
    }

} // ChannelMag
