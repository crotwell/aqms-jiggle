package org.trinet.jasi;
public interface JasiAssocDataWriterIF {
    public boolean commit();
    public boolean dbaseInsert();
    public boolean dbaseAssoc();
}

