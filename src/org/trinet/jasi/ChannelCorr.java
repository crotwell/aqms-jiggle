package org.trinet.jasi;
import java.util.*;
import org.trinet.util.*;
import org.trinet.jdbc.table.*;
public abstract class ChannelCorr extends AbstractChannelCorrection {

    protected ChannelCorr() {}

    protected ChannelCorr(ChannelIdIF id) {
        super(id);
    }
    protected ChannelCorr(ChannelIdIF id, DateRange dateRange) {
        super(id, dateRange);
    }
    protected ChannelCorr(ChannelIdIF id, DateRange dateRange, double value, String corrType) {
        this(id, dateRange, value, corrType, null, null);
    }
    protected ChannelCorr(ChannelIdIF id, DateRange dateRange, double value, String corrType, String corrFlag) {
        this(id, dateRange, value, corrType, corrFlag, null);
    }
    protected ChannelCorr(ChannelIdIF id, DateRange dateRange, double value, String corrType,
                    String corrFlag, String authority) {
        super(id, dateRange, value, corrType, corrFlag, authority);
    }

    public static final ChannelCorr create() {
        return create(JasiObject.DEFAULT);
    }
    public static final ChannelCorr create(int schemaType) {
        return create(JasiFactory.suffix[schemaType]);
    }
    public static final ChannelCorr create(String suffix) {
        return create(suffix, null);
    }
    public static final ChannelCorr create(String suffix, String type) {
        ChannelCorr corr = (ChannelCorr)
            JasiObject.newInstance("org.trinet.jasi.ChannelCorr", suffix);
        if (type != null) corr.corrType.setValue(type);
        return corr;
    }

    public void setCorrType(String type) { corrType.setValue(type); }

    public static Collection getByCorrType(java.util.Date date, String corrType) {
        ChannelCorr cc = ChannelCorr.create();
        cc.setCorrType(corrType);
        return cc.loadAll(date);
    }
    public static ChannelCorr getByChanIdCorrType(Channel chan, java.util.Date date, String corrType) {
        ChannelCorr cc = ChannelCorr.create();
        cc.setCorrType(corrType);
        return (ChannelCorr) cc.getByChannelId(chan.getChannelObj().getChannelId(), date);
    }
    public static Collection getAllTypesByChanId(ChannelIdIF id, java.util.Date date) {
        ChannelCorr cc = ChannelCorr.create();
        return cc.getAllTypesByChanId(id, date);
    }
    public static Collection getLikeTypesByChanId(ChannelIdIF id, java.util.Date date, String typeExpr) {
        ChannelCorr cc = ChannelCorr.create();
        return cc.getLikeTypesByChannelId(id, date, typeExpr);
    }
    abstract public Collection getAllTypesByChannelId(ChannelIdIF id, java.util.Date date) ;
    abstract public Collection getLikeTypesByChannelId(ChannelIdIF id, java.util.Date date, String typeExpr);
}
