package org.trinet.jasi;

public interface ChannelMagIF  {
    public void initialize();
    public void initAssocSummaryMagData() ;
    public boolean isNull() ; // value not defined
    public void setMagValue(double magValue) ;
    public double getMagValue() ; // return -1 if not defined
    public void setResidual( Magnitude mag ) ;
    public double getResidual() ; // return Double.MAX_VALUE if not defined
    public void setInWgt(double wt) ;
    public double getInWgt() ;  // return 0 if not defined
    public void setWeight(double wt) ;
    public double getWeight() ;  // return 0 if not defined
    public void setCorrection(double corr) ;
    public double getCorrection() ;  // return 0 if not defined
    public void setType(String magType) ;
    public String getType() ;  // return " " if not defined
    public String toNeatString() ;
}
