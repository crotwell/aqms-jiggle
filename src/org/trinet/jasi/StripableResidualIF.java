//abstract methods belong to StripableDistanceResidualIF implement by JasiReading, MagList, SolutionList?
package org.trinet.jasi;
public interface StripableResidualIF { 
  public int stripByResidual(double val);
}
