package org.trinet.jasi;
/** Methods to retrieve NCDC database schema station channel description elements. */
public interface ChannelIdIF {
    public static final int NET         = 0;
    public static final int STA         = 1;
    public static final int SEEDCHAN    = 2;
    public static final int LOCATION    = 3;
    public static final int CHANNEL     = 4;
    public static final int CHANNELSRC  = 5;
    public static final String UNKNOWN_LOCATION = "";
    String getSta();
    String getNet();
    String getSeedchan();
    String getLocation();
    String getChannel();
    String getChannelsrc();

    void setSta(String sta);
    void setNet(String net);
    void setSeedchan(String seedchan);
    void setLocation(String location);
    void setChannel(String channel);
    void setChannelsrc(String channelsrc);

    boolean equalsName(ChannelIdIF id);
    boolean equalsNameIgnoreCase(ChannelIdIF id);

    String toDelimitedNameString(char delimiter);
    String toDelimitedSeedNameString(char delimiter);
    String toDelimitedNameString(String delimiter);
    String toDelimitedSeedNameString(String delimiter);

    public Object clone();
}

