package org.trinet.jasi.engines;
import java.awt.*;
import java.beans.*;
import java.util.*;
import org.trinet.jasi.*;
import org.trinet.jasi.engines.*;
import org.trinet.jasi.magmethods.*;
import org.trinet.jiggle.common.AlertUtil;
import org.trinet.jiggle.common.JiggleConstant;
import org.trinet.util.*;
import org.trinet.util.locationengines.*;
import org.trinet.util.gazetteer.*;

import javax.swing.*;

public abstract class AbstractHypoMagEngineDelegate extends AbstractChannelDataEngineDelegate
    implements HypoMagEngineDelegateIF {

    protected ChannelTimeWindowModel ctwModel = null;  // added 09/08/2006 -aww

    protected boolean haveLocService = false;
    protected boolean haveMagService = false;

    // Reading list data of Solution and Magnitude are used for calcs
    // if loadXXX=false invoking application to must 1st load the list.
    // if loadXXX=true delegate will load data from db if list is empty.
    protected boolean loadArrivalsFromDb = false;
    protected boolean loadMagReadingsFromDb = true;

    //No implementation yet like AbstractMagnitudeEngine for creating
    //a network default engine type (TN,EW) -aww
    private static String defaultLocEngClassName =
        "org.trinet.util.locationengines.LocationEngineHypoInverse";

    //if engine property not defined, use default engine type (TN,EW): 
    private static String defaultMagEngClassName = 
         "org.trinet.jasi.engines."+JasiObject.getDefaultSuffix()+ //default package name
         ".MagnitudeEngine"+JasiObject.getDefaultSuffix(); //default class name

    protected HashMap magMethodMap = new HashMap(11);
    protected HashMap magTypeEngPropMap = new HashMap(11); // added -aww 2008/11/07 

    protected AbstractHypoMagEngineDelegate() { 
        super();
    }
    protected AbstractHypoMagEngineDelegate(JasiPropertyList props) { 
        super(props);
    }

    protected boolean initialize() {
        return initialize(true);
    }

    protected boolean initialize(boolean useExisting) {

        initIO();          // do first to enable flags
        initChannelList(); // must do before engines
        initEngines(useExisting);

        if ( myProps.isSpecified("delegateLoadArrivalsFromDb") )
            loadArrivalsFromDb = myProps.getBoolean("delegateLoadArrivalsFromDb");

        if ( myProps.isSpecified("delegateLoadMagReadingsFromDb") )
            loadMagReadingsFromDb = myProps.getBoolean("delegateLoadMagReadingsFromDb");

        // Added below allows use of ChannelTimeWidowModel to define waveform list - 09/08/2006 aww
        if (myProps instanceof SolutionWfEditorPropertyList)
            ctwModel = ((SolutionWfEditorPropertyList) myProps).getCurrentChannelTimeWindowModel();

        if (ctwModel == null &&  myProps.isSpecified("currentChannelTimeWindowModel") ) {
            ctwModel = ChannelTimeWindowModel.getInstance(myProps.getProperty("currentChannelTimeWindowModel"));
            ctwModel.setProperties(myProps);
        }
        if (debug) 
          System.out.println("DEBUG " +
                 getClass().getName() + " currentChannelTimeWindowModel = " + 
                ((ctwModel == null) ? "null" : ctwModel.getClass().getName()));

        return true; 
    }

    public void setDebug(boolean tf) {
        super.setDebug(tf);
    }

    protected void setDebugOfMagnitudeEngine(boolean tf) {
        MagnitudeEngineIF magEng = getMagnitudeEngine();
        if (magEng != null) magEng.setDebug(tf);
    }
    protected void setVerboseOfMagnitudeEngine(boolean tf) {
        MagnitudeEngineIF magEng = getMagnitudeEngine();
        if (magEng != null) magEng.setVerbose(tf);
    }
    protected void setDebugOfLocationEngine(boolean tf) {
        LocationEngineIF locEng = getLocationEngine();
        if (locEng != null) locEng.setDebug(tf);
    }
    protected void setVerboseOfLocationEngine(boolean tf) {
        LocationEngineIF locEng = getLocationEngine();
        if (locEng != null) locEng.setVerbose(tf);
    }

    public boolean setDelegateProperties(String propFileName) {
      myProps = new SolutionWfEditorPropertyList();
      myProps.setFiletype(EnvironmentInfo.getApplicationName().toLowerCase());
      myProps.setFilename(propFileName);
      myProps.reset(); // does the load
      if (myProps instanceof JasiPropertyList) ((JasiPropertyList)myProps).setup();
      return (myProps.isEmpty()) ? false : initializeEngineDelegate();
    }

    // To make initEngines() comprehensive (global generic)
    // it could be made to iterate over its property list looking for 
    // all property names of the form "xxxEngine" then try to create instances
    // of the declared classes to add to the map, engines could auto-initialize
    // only if their property filenames followed a naming template convention.
    // like:
    //   String propFileName = myProps.getUserFileNameFromProperty("xxxEngineProps");
    //   if (propFileName != null) xxxEng.setProperties(propFileName);
    //   else xxxEng.setProperties(myProps);
    protected void initEngines() {
        initEngines(true);
    }

    protected void initEngines(boolean useExisting) {
        initLocEngine(useExisting);
        initMagEngine(useExisting);
    }

    protected void initLocEngine() {
        initLocEngine(true);
    }

    protected void initLocEngine(boolean useExisting) {

        LocationEngineIF locEng = null;
        boolean newService = false;
        if (! haveLocService || ! useExisting) {
          if (haveLocService)  {
            locEng = getLocationEngine();
            if (locEng instanceof LocationEngineServiceIF) { // added for insurance to disconnect pre-existing before adding new -aww 2010/05/24
              LocationEngineServiceIF service = (LocationEngineServiceIF) locEng;
              if (verbose) System.out.println("INFO: ...HypoMagEngineDelegate.initLocEngine disconnecting prior location engine service");
              service.disconnect();
            }
          }
          // Creating NEW engine here!
          // Could change code to get the locationEngine name from locEngineProps file (see below)
          // but 1st would need to update Jiggle code properties lookup to enable this change -aww 
          String engineClassName = myProps.getProperty("locationEngine");
          //Jiggle uses upper case "L"? why not make it consistent with other properties -aww
          if (engineClassName == null)
                engineClassName = myProps.getProperty("LocationEngine", defaultLocEngClassName);

          locEng = AbstractLocationEngine.create(engineClassName);
          if (locEng == null)  {
            System.err.println("WARNING! ...HypoMagEngineDelegate.initLocEngine can't create Location engine: " + engineClassName);
            haveLocService = false;
            return;
          }
          addEngine(LOCATION, locEng);
          newService = true;
        }

        locEng = getLocationEngine();  // may be prexisting, if above block skipped

        if (locEng == null)  {
            System.err.println("WARNING! ...HypoMagEngineDelegate.initLocEngine location engine is null");
            haveLocService = false;
            return;
        }
        if (verbose) System.out.println("INFO: ...HypoMagEngineDelegate has Location engine: " + locEng.getClass().getName());

        String propFileName = myProps.getUserFileNameFromProperty("locEngineProps");
        if (propFileName != null) {
            if (verbose)
                System.out.println("INFO: ...HypoMagEngineDelegate.initLocEngine with 'locEngineProps' = " + propFileName); 
            locEng.setProperties(propFileName);
        }
        else {
            if (verbose)
                System.out.println("INFO: ...HypoMagEngineDelegate.initLocEngine, 'locEngineProps' property not defined, using delegate properties.");
            locEng.setProperties(myProps);
        }

        // Synch with delegate
        locEng.setVerbose(verbose);
        locEng.setDebug(debug);
        if (debug && propFileName != null) { // don't dump the delegate properties (e.g. jiggle.props)
            System.out.println("DEBUG " + getClass().getName() + " location engine properties:");
            locEng.getProperties().dumpProperties();
        }


        // could setChannelList via engine init config properties too:
        setEngineChannelList(locEng);  // aww added 1/04 because of revised delegate implementation

        haveLocService = true;

        if (locEng instanceof LocationEngineServiceIF) {
          LocationEngineServiceIF service = (LocationEngineServiceIF) locEng;
          if (!newService) {
              if (verbose) System.out.println("INFO: ...HypoMagEngineDelegate.initLocEngine disconnecting prior location engine service");
              service.disconnect(); // insurance to force disconnection in case of a preexisting service -aww 2010/05/24
          }

          int millis = service.getServerTimeoutMillis(); // save current timeout
          // below use small timeout value to reduce "hang time" -aww 2009/04/16, set min 4000 on 2010/04/20
          service.setServerTimeoutMillis(Math.min(4000, millis)); 
          haveLocService = service.connect(); // test new service connection
          service.setServerTimeoutMillis(millis); // reset timeout back to original value
          if (haveLocService) {
            service.disconnect(); // now disconnect
            if (verbose) System.out.println("INFO: ...HypoMagEngineDelegate has Location engine service: " +
                             service.describeConnection());
          } else {
              System.err.println("WARNING! ...HypoMagEngineDelegate Location engine can't connect to service: " +
                    service.describeConnection());
              AlertUtil.displayError("Failed Connection",
                    "Cannot connect to the Location Server." + JiggleConstant.NEWLINE +
                    "Server: " + service.getIPAddress() + JiggleConstant.NEWLINE +
                    "Port: " + service.getPort(),
                    "Please check the location server connection setting!", JOptionPane.WARNING_MESSAGE);
          }
        }
    }

    protected void initMagEngine() {
        initMagEngine(true);
    }

    protected void initMagEngine(boolean useExisting) {

        if (haveMagService && useExisting) return; // test to avoid resetting properties from those set in GUI 2011/04/19 -aww

        MagnitudeEngineIF magEng = null;
        if (! haveMagService || ! useExisting) {
           // use predefined default name - aww
           String engineClassName = myProps.getProperty("magEngine", defaultMagEngClassName);
           magEng = AbstractMagnitudeEngine.create(engineClassName);
           if (magEng == null) {
             System.err.println("WARNING! ...HypoMagEngineDelegate.initMagEngine can't create Magnitude engine: " + engineClassName);
             haveMagService = false;
             return;
           }
           addEngine(MAGNITUDE, magEng);
        }

        magEng = getMagnitudeEngine();
        if (magEng == null) {
             System.err.println("WARNING! ...HypoMagEngineDelegate.initMagEngine magnitude engine is null");
             haveMagService = false;
             return;
        }
        if (verbose) System.out.println("INFO: ...HypoMagEngineDelegate has Magnitude engine: " + magEng.getClass().getName());

        String propFileName = myProps.getUserFileNameFromProperty("magEngineProps");
        if (propFileName != null) {
            if (verbose)
                System.out.println("INFO: ...HypoMagEngineDelegate.initMagEngine with 'magEngineProps' = " + propFileName); 
            magEng.setProperties(propFileName);
        }
        else {
            if (verbose)
                System.out.println("INFO: ...HypoMagEngineDelegate.initMagEngine, 'magEngineProps' property not defined, using DEFAULT properties.");
                //System.out.println("INFO: ...HypoMagEngineDelegate.initMagEngine, 'magEngineProps' property not defined, using delegate properties.");
            // Doing below results in the default engine property setttings being added to input delegate props (e.g. jiggle.props)
            //magEng.setProperties(myProps); // REMOVED 2011/05/24 -aww
        }

        // could setChannelList via engine init config properties too:
        setEngineChannelList(magEng);  // aww added 1/04 because of revised delegate implementation

        haveMagService = true;
    }

    public GenericPropertyList getMagEngMapPropertiesForType(String magType) {
        return (GenericPropertyList) magTypeEngPropMap.get(magType.toLowerCase());
          
    }

    public void mapMagEnginePropertiesForType(String magType, GenericPropertyList gpl) { // added -aww 2008/11/07 
          magTypeEngPropMap.put(magType.toLowerCase(), gpl);
    }

    public MagnitudeEngineIF initMagEngineForType(String magType) {
        return initMagEngineForType(null, magType, true);
    }

    public MagnitudeEngineIF initMagEngineForType(Solution sol, String magType) {
        return initMagEngineForType(sol, magType, true);
    }

    public MagnitudeEngineIF initMagEngineForType(Solution sol, String magType, boolean useExisting) { // added -aww 2008/11/07 
        // first see if input type is enabled 
        String type = magType.toLowerCase();
        if ( myProps.getBoolean(type+"Disable") ) {
            statusString = type + " engine is disabled by user input property "+ type + "Disable=true";
            System.err.println("INFO: " + statusString);
            return null;
        }

        MagnitudeEngineIF magEng = getMagnitudeEngine();
        if (magEng == null) {
            statusString = "EngineDelegate null magnitude engine";
            System.err.println(statusString);
            return null;
        }

        // magType is mapped to a Properties object by magTypeEngPropMap.put(type, magTypeProperties)
        // when  (useExisting == true) the properties set are those from the map, NOT those read from disk file
        GenericPropertyList gpl = (GenericPropertyList) magTypeEngPropMap.get(type);
        if (useExisting && gpl != null ) { // if/else added -aww 2008/11/07 
            magEng.setProperties(gpl);
        }
        else {
          String propFileName = myProps.getUserFileNameFromProperty(type+"MagEngineProps");
          if (propFileName != null) {
              magEng.setProperties(propFileName);
          }
          else {
            System.out.println("Note: No magEngine property file declared (property: "+type+
                             "MagEngineProps) using HypoMagEngineDelegate properties for engine.");
            magEng.setProperties(myProps);
          }
          mapMagEnginePropertiesForType(type, magEng.getProperties());
        }

        // engine configuration property overrides go below here
        // below forces delegate settings to override of engine property settings ?
        //System.out.println("DEBUG >>> HypoMagEngineDelegate settings debug: " + debug +" ; verbose: " + verbose); 
        magEng.setVerbose(verbose);
        magEng.setDebug(debug);
        if (debug) {
            System.out.println("DEBUG " + getClass().getName() + " magnitude engine properties:");
            magEng.getProperties().dumpProperties();
        }

        // magEng.setPreferredMag(false); // default is true, user input properties can set it too.
        // could setChannelList via engine init config properties too:
        setEngineChannelList(magEng); 

        MagnitudeMethodIF magMethod = initMagMethod(sol, type);

        // method configuration property overrides go below here
        // below forces delegate settings to override of method property settings ?
        if (magMethod != null) {
            magMethod.setVerbose(verbose);
            magMethod.setDebug(debug);
            if (debug) {
                System.out.println("DEBUG " + getClass().getName() + " properties for magnitude method type: " + type);
                magMethod.getProperties().dumpProperties();
            }
        }
        //
        magEng.setMagMethod(magMethod);

        if (! magEng.isEngineValid()) {
          statusString = "EngineDelegate invalid magnitude engine";
          if (magMethod == null) {
             statusString += " magnitude method is null.";
          }
          System.err.println(statusString);
          return null;
        }

        return magEng;
    }

    public MagnitudeMethodIF initMagMethod(String magType) {
        return initMagMethod(null, magType, true);
    }

    public MagnitudeMethodIF initMagMethod(Solution sol, String magType) {
        return initMagMethod(sol, magType, true);
    }

    public MagnitudeMethodIF initMagMethod(Solution sol, String magType, boolean useExisting) {
        String type = magType.toLowerCase();
        MagnitudeMethodIF magMethod = (MagnitudeMethodIF) magMethodMap.get(type);
        if (magMethod != null && useExisting) {
            magMethod.setCurrentSolution(sol);
            if (debug) System.out.println("DEBUG: " +getClass().getName()+ " initMagMethod " +type+" magMethodClassName =" + magMethod.getClass().getName());
            return magMethod;
        }

        // create instance and store in map
        String magMethodClassName = myProps.getProperty(type+"MagMethod", null);
        if (debug)
            System.out.println("DEBUG: " +getClass().getName()+ " initMagMethod " +type+" magMethodClassName =" + magMethodClassName);

        if (magMethodClassName == null) {
          System.err.println(getClass().getName() +
            " Error initMagMethod(type) method classname property undefined for type: " + type);
          return null;
        }

        magMethod = AbstractMagnitudeMethod.create(magMethodClassName);
        if (magMethod == null) {
          System.err.println(getClass().getName() + 
            " Error initMagMethod(type) can't create magMethod class : " + magMethodClassName);
          return null;
        }

        // implementation could be changed like engine to use method like
        // setProperties(...) configure(...) - aww
        String propFileName = myProps.getUserFileNameFromProperty(type+"MagMethodProps");
        if (debug) System.out.println("DEBUG: " +getClass().getName()+ " initMagMethod setProperties " + type+"MagMethodProps=" + propFileName);
        if (propFileName != null) magMethod.setProperties(propFileName);
        else {
          System.out.println("Note: No magMethod property file declared (property: "+type+
                             "MagMethodProps) using DEFAULT properties for method.");
                             //"MagMethodProps) using HypoMagEngineDelegate properties for method.");
          // Doing below could result in the default method property setttings being added to input delegate props (e.g. jiggle.props)
          //magMethod.setProperties(myProps); // REMOVED 2011/05/24 -aww
        }
        //
        // save configured magMethod in lookup map 
        magMethod.setCurrentSolution(sol);
        magMethodMap.put(type, magMethod);

        return magMethod;
    }

    public String magMethodPropertiesString() {
        Collection coll = magMethodMap.values();
        if (coll == null || coll.size() == 0) return "";
        Iterator iter = coll.iterator();
        StringBuffer sb = new StringBuffer(4096);
        while (iter.hasNext()) {
            sb.append(((MagnitudeMethodIF) iter.next()).getProperties().toDumpString());
        }
        return sb.toString();

    }
    public String magMethodPropertiesString(String magType) {
        StringBuffer sb = new StringBuffer(4096);
        MagnitudeMethodIF magMethod = (MagnitudeMethodIF) magMethodMap.get(magType.toLowerCase());
        if (magMethod != null) {
            sb.append(magMethod.getProperties().toDumpString());
        }
        return sb.toString();
    }

    public String magEnginePropertiesString() {
        Collection coll = magTypeEngPropMap.values();
        if (coll == null || coll.size() == 0) return "";
        Iterator iter = coll.iterator();
        StringBuffer sb = new StringBuffer(4096);
        while (iter.hasNext()) {
            sb.append(((GenericPropertyList) iter.next()).toDumpString());
        }
        return sb.toString();

    }
    public String magEnginePropertiesString(String magType) {
        StringBuffer sb = new StringBuffer(4096);
        GenericPropertyList gpl = (GenericPropertyList) magTypeEngPropMap.get(magType.toLowerCase());
        if (gpl != null) {
            sb.append(gpl.toDumpString());
        }
        return sb.toString();
    }

//-------------------------------------------------------------------

    public LocationEngineIF getLocationEngine() {
        return (LocationEngineIF) getEngine(LOCATION);
    }
    public MagnitudeEngineIF getMagnitudeEngine() {
        return (MagnitudeEngineIF) getEngine(MAGNITUDE);
    }
    public boolean hasLocationEngine() {
        return hasEngine(LOCATION);
    }
    public boolean hasMagnitudeEngine() {
        return hasEngine(MAGNITUDE);
    }

    public void reset() {
        resultsString = "";
        statusString = "";
        solveSuccess = false;
        Collection values = engMap.values();
        Iterator iter = values.iterator();
        EngineIF engine = null;
        GenericServiceIF service = null;
        while (iter.hasNext()) {
          engine = (EngineIF) iter.next();
          engine.reset();
          if (engine instanceof GenericServiceIF) {
              service = (GenericServiceIF) engine;
              service.disconnect();
              service.connect();
          }
        }
    }

    public void disconnect() {
        Collection values = engMap.values();
        Iterator iter = values.iterator();
        Object engine = null;
        GenericServiceIF service = null;
        while (iter.hasNext()) {
          engine = iter.next();
          if (engine instanceof GenericServiceIF) {
              service = (GenericServiceIF) engine;
              service.disconnect();
          }
        }
    }

    protected void finalize() throws Throwable {
      disconnect();
      super.finalize();
    }

    public boolean solve(Object data) {
      if (data instanceof Solution) return solve((Solution) data);
      else if (data instanceof Magnitude) return solve((Magnitude) data);
      return false;
    }
    public boolean solve(Solution sol) {
      return (sol != null) ? locate(sol) : false;
    }
    public boolean solve(Magnitude mag) {
      return (mag != null) ? calcMag(mag, mag.getAssociatedSolution()) : false; // changed "null" solution arg -aww
    }
    public boolean solve(Solution sol, Magnitude mag) {
      return (mag != null) ? calcMag(mag, sol) : locate(sol);
    }

    abstract public boolean locate(Solution sol);
    abstract public boolean calcMag(Magnitude mag, Solution sol);

    /** Calculate the preferred magnitude of input solution.
    Does NOT re-examine the waveforms, uses existing readings. */
    public boolean calcMag(Solution sol) {
        solveSuccess = false;
        Magnitude mag = sol.getPreferredMagnitude();
        if (mag == null) return false;  // no mag
        return calcMag(sol, mag.getTypeString());
    }

    abstract public boolean calcMag(Solution sol, String magType) ;

    /** Calculate the preferred magnitude of input solution by doing
    * scans of its Waveforms to calculate new Channel magnitude data.
    * */
    public boolean calcMagFromWaveforms(Solution sol) {
        return calcMagFromWaveforms(sol, sol.getPreferredMagnitude().getTypeString());
    }

    abstract public boolean calcMagFromWaveforms(Solution sol, String magType) ;

    abstract protected void reportMessage(String titleType, String message);

    protected void reportStatusMessage(String titleType) {
        reportMessage(titleType, statusString);
    }

    protected void reportEngineCalculationStatus(EngineIF engine) {
        if (! engine.success()) {
          StringBuffer sb = new StringBuffer(512);
          sb.append(engine.getEngineName()).append(" calculation failure! ");
          sb.append(engine.getStatusString()); // debug
          statusString = sb.toString();
          reportStatusMessage("ERROR:");
        }
        else {
          statusString = engine.getEngineName() + " calculation success"; // test
          if (verbose) reportStatusMessage("INFO:");  // default, don't report success
        }
    }

    public boolean resolveLocation(Solution aSol) {
        return (
             //aSol.validFlag.longValue()==0l || // removed this test - not appropiate -aww 11/22/2006
             aSol.isStale() ||
             aSol.dummyFlag.longValue()==1l
           ) ? locate(aSol) : true;
    }

    protected boolean resolveSolPhaseList(Solution aSol) {
        PhaseList phList = aSol.getPhaseList();

        if (aSol == null || phList == null) return false;

        if (phList.size() == 0 && loadArrivalsFromDb ) {
          System.out.println("INFO: HypoMagEngineDelegate Loading Solution phase list from db.");
          aSol.loadPhaseList(false);
          phList = aSol.getPhaseList();
        }
        return (phList.size() >= getLocationEngine().getMinPhaseCount()) ? true : false;
    }

    /** Use existing readings of the input Magnitude's reading list, if that list is not empty.
     * Otherwise, load all readings from the data source and associated them with the input Magnitude
     * as defined by the load options specified by the input property <i>magDataLoadOption</i>.
     * Load options are specified by a String value that can be any combination of the letters:
     * "m","p", and "s". These letter codes are flags for loading and associating the Magnitude
     * dependent readings found in the data source (database) as follows:<p>
     * <i>m</i> those of the input Magnitude.<br>
     * <i>p</i> those of the preferred Magnitude of the same type as the input Magnitude.<br>
     * <i>s</i> those of the input Solution associated with the input Magnitude.<br>
     * So a String value of <i>"mps"</i> means to first try to load the readings of the input
     * magnitude, if none, then those of the prefmag of same type, if none, then those of
     * the input Solution. A String value of <i>"ps"</i> means load readings of the prefmag,
     * then, if none those associated with the Solution id. Similarly, a single character means
     * only load the readings from that data source type. 
     * If the input property <i>magDataLoadClonesReadings=true</i> then all readings loaded
     * loaded from either a solution ("s") or prefmag ("p") source are cloned before association
     * with the input Magnitude to allow a Magnitude's list to be independently edited or deleted
     * without effecting another Magnitude's results (such as the verticals components being
     * used by an Mh magnitude but being deleted by a subsequent Ml calculation).<p>
     * If the property <i>magDataLoadClearsSolution=true</i>, the Solution's associated list
     * is first cleared and only new loaded readings are associated with the input Solution.
     * If set <i>false</i> association will occur only if the input Magnitude is already the
     * preferred magnitude for the Solution, because list listeners automatically associate
     * readings of the preferred Magnitude, for the alternative Magnitudes, with this property
     * flag set to <i>false</i>, association of the readings with the Solution's list will
     * occur when that Magnitude is added the Solution's MagList by subsequent processing
     * methods.
     * @throws NullPointerException when input Magnitude or Solution argument is null.
     * @return true if magnitude reading data was successfully loaded, false otherwise.
     */
    protected boolean resolveMagReadingList(Magnitude aMag, Solution aSol) {
      
      if (! aMag.getReadingList().isEmpty()) {
          return true; // we already have data use it
      }

      // Get property setting to "clear" Solution list, if property is absent,
      // default is "true" to avoid ambiguity of dupe readings from a prior mag solve.
      boolean clearSolution = (myProps.isSpecified("magDataLoadClearsSolution")) ? 
          myProps.getBoolean("magDataLoadClearsSolution") : true ;

      String answer = myProps.getProperty("magDataLoadOption");
      //if (answer.equalsIgnoreCase("magnitude"))  // remove 01/09/2005 - aww
      //new property value logic below 01/09/2005 -aww
      answer = (answer == null) ? "m" : answer.toLowerCase();

      boolean status = false;
      //a magnitude stale state may result upon db data readings load
      if (answer.indexOf("m") > -1) {
        status = loadMagReadingsByMagnitude(aMag, aSol, clearSolution);
      }

      if ( ! status && answer.indexOf("p") > -1)  { // get all readings assoc with like prefmag 
        status = loadMagReadingsByPrefMag(aMag, aSol, clearSolution);
      } 

      if ( ! status && answer.indexOf("s") > -1)  { // get all readings assoc with input aSol
        status = loadMagReadingsBySolution(aMag, aSol, clearSolution);
      } 

      if (! status) {
        statusString = "EngineDelegate cannot calculate magnitude; no readings for magtype: "+aMag.getTypeString();
        reportStatusMessage("ERROR");
      }

      return status;
    }

    /**
     * Load and associate the readings input Magnitude id from the db
     * if property <i>loadMagReadingsFromDb</i>=<i>true</i>.
     * Associated readings are clones if property <i>magDataLoadClonesReadings</i>=<i>true</i>.
     * */
    protected boolean loadMagReadingsByMagnitude(Magnitude aMag, Solution aSol, boolean clearSolution) {

        if (! loadMagReadingsFromDb ) return false;

        MagnitudeAssocJasiReading jr = prepareForLoad(aMag, aSol, clearSolution);
        if (jr == null) return false;

        //Note aMag.getBy(...,true) associates readings with aMag, and also aSol, if aMag is assigned to aSol,
        //a side-effect not wanted for UNCLEARED list, so instead:
        java.util.List aList = (java.util.List) jr.getByMagnitude(aMag, null, false);
        if ( aList == null || aList.isEmpty() ) return false;

        associateReadings(aList, aMag, aSol); // only adds to "cleared" sol list
        return true;
    }

    /**
     * If prefmag of type for input Solution already has its readings loaded
     * associate them also with the,
     * else load the readings associated with the prefmag of type id from the db
     * if property <i>loadMagReadingsFromDb</i>=<i>true</i>.
     * Associated readings are clones if property <i>magDataLoadClonesReadings</i>=<i>true</i>.
     * */
    protected boolean loadMagReadingsByPrefMag(Magnitude aMag, Solution aSol, boolean clearSolution) {
        Magnitude prefMag = aSol.getPrefMagOfType(aMag.getTypeString().toLowerCase());
        if (prefMag == null) {
           prefMag = aSol.getPreferredMagnitude();
           if ( prefMag == null || ! prefMag.isSameType(aMag) ) prefMag = null;
        }
        if (prefMag == null) return false;

        java.util.List aList = prefMag.getReadingList();
        if ( aList == null || aList.isEmpty() ) {

          if (! loadMagReadingsFromDb ) return false;

          MagnitudeAssocJasiReading jr = prepareForLoad(aMag, aSol, clearSolution);
          if (jr == null) return false;
          aList = (java.util.List) jr.getByMagnitude(prefMag, null, false);
        }
        if ( aList == null || aList.isEmpty() ) return false;

        associateReadings(aList, aMag, aSol); // only adds to "cleared" sol list
        return true;
    }

    /**
     * If input Solution already has readings loaded associate them with input aMag
     * if input <i>clearSolution</i> is set <i>false</i>,
     * else load the readings associated with the input Solution id from the db
     * if property <i>loadMagReadingsFromDb</i>=<i>true</i>.
     * Associated readings are clones if property <i>magDataLoadClonesReadings</i>=<i>true</i>.
     * */
    protected boolean loadMagReadingsBySolution(Magnitude aMag, Solution aSol, boolean clearSolution) {

        // Do we want to associate pre-existing (edited) input aSol list readings with an input aMag?
        // A prior magnitude calc could delete/purge of the aSol readings to be associated with aMag.
        // however, the edited readings may supercede "stale" data found in data source store, a dilemma.
        //
        // When input "clearSolution" is "true" aMag readings will be loaded from db only if the property
        // "loadMagReadingsFromDb" is set to "true", if set "false" the aSol list will not be cleared,
        // in which case if the list is not empty its readings will be associated with input aMag. -aww
        MagnitudeAssocJasiReading jr = prepareForLoad(aMag, aSol, (loadMagReadingsFromDb && clearSolution));
        if (jr == null) return false;

        java.util.List aList = aSol.getListFor(jr);
        //Note getBySolution(...) should order channels by lddate,id ascending, using SQL 
        //analytic function dense_rank() for SQL filter join in TN subclass -aww 01/07/2005
        if ( aList == null || aList.isEmpty() ) {
          if (! loadMagReadingsFromDb ) return false;
          // load Solution's data from the database to be associated with input aMag.
          // Note aSol.getBy(...,true) associates readings with aSol, we don't do this here if 
          // in case we want to "preserve" uncleared list state.
          aList = (java.util.List) jr.getBySolution(aSol, null, false);
          if ( aList == null || aList.isEmpty() ) return false;

          // Added logic to remove duplicate readings loaded for same channel, type, and origin.
          // selects from any duplicate matches, the row with the greatest LDDATE and identifier id.
          // if SQL in getBy(...) returns a distinct channel set for an origin, below is not needed.
          // for example if SQL of TN class, uses analytic function (dense_rank) to return a distinct set
          // then kludge below is not needed here. -aww 01/07/2005
          JasiReadingList jrList =
            (JasiReadingList) aSol.getListFor(jr).newInstance(); // e.g. CodaList or AmpList
          jrList.addAll(aList, false);  // stuff list with db data, no notify
          jrList.addOrReplaceAllOfSameChanType(aList); // get rid of dupes for same channel type
          aList = jrList;
        }
        if ( aList == null || aList.isEmpty() ) return false;

        //aMag.associateAll(aList); // side-effect associates readings with sol list
        //this side-effect is not wanted for an UNCLEARED list, so:
        associateReadings(aList, aMag, aSol);
        return true;
    }

    protected MagnitudeAssocJasiReading prepareForLoad(Magnitude aMag, Solution aSol, boolean clearSolution) {
        MagnitudeAssociatedListIF jrl = aMag.getReadingList();
        jrl.clear(false); // clear input magnitude's readings, but don't notify list listeners
        //
        // IF SOLUTION LIST IS NOT "CLEARED" ONLY THOSE READINGS ASSOCIATED
        // WITH AN EXISTING PREFERRED MAG LISTENER ARE REPLACED IN THE SOLUTION LIST.
        // FOR ALTERNATE MAGS THE READING OBJECTS LOADED FROM DB ARE NOT ADDED TO
        // AN "UNCLEARED" SOLUTION LIST.
        //
        if (clearSolution) {
          //Note that "clear()" causes the prefmag list listeners remove like readings from its reading list!
          //which invalidates the prior prefmag if the input mag is not this preferred magnitude.
          //clear(false) disables notify, which should allow prior prefmag to keep readings - 08/04/2004 -aww
          aSol.getListFor(jrl).clear(false); // doesn't notify prefmag listeners
        }

        MagnitudeAssocJasiReading jr  = aMag.getReadingTypeInstance();
        if (jr == null) {
          statusString = "EngineDelegate unable to create reading instance for mag type : " + aMag.getTypeString();
          reportStatusMessage("ERROR");
          return null;
        }
        return jr;
    }

    private void associateReadings(java.util.List aList, Magnitude aMag, Solution aSol) {
        //PROBLEM 1:
        // If property value "magDataLoadCloneReadings" is false then
        // the solution and magnitude list reading instances are aliased.
        // Thus if a mag method "virtually" deletes a reading type it's deleted in
        // all magnitudes associated with that instance as well as in solution's list.
        // so a commit would skip it in cases where it should be create a new "id" row. 
        // Changing magMethod logic to remove reading from specific magList rather
        // than "delete" creates a problem with erroneous data edits or newly 
        // created readings being lost to the Human editor, thus data recovery
        // from an error would only be possible if the removed data were 
        // preserved in and recovered from the Solution's equivalent list. 
        //
        //PROBLEM 2:
        // However if when we clone solution's reading list, the readings in the lists
        // are now decoupled so readings can be deleted or purged, but now for
        // every "original" solution's list reading "changed" we get a "new" reading
        // row in the db table upon commit, "different" row ids for the same data.
        // For example, you get multiples in the AssocAmO, AssocAmM for a changed amps
        // when solving for both an Mh and Ml.
        // In addition, if reading lists are deep cloned, then an edit of a reading in 
        // a container's list would not be reflected in the equivalent reading of the
        // others containing lists (i.e. Solution and related Magnitudes). 
        //
        java.util.List listOfReadings = aList; // matches input
        MagnitudeAssociatedListIF magList =  null;
        if (myProps.getBoolean("magDataLoadClonesReadings")) {
          magList = (MagnitudeAssociatedListIF) aMag.getReadingList().newInstance();
          magList.addAll(aList);
          /* If mag is committed after the solution below calcDist should not be needed.
          if (chanList != null) {
              // here match channel lat,lon,z and do distances since cloning reading
              magList.matchChannelsWithList(chanList);
              magList.calcDistances(aSol);
          }
          */
          listOfReadings = (java.util.List) magList.clone(); // clone of input list
        }
        // Magnitude associateAll implementation forces solution association by default.
        // aMag.associateAll(listOfReadings); // does solution association too.
        // If aMag is preferred, list listeners associate with aSol list as well provided 
        // that aMag.sol.synchWithMagLists == true,
        aMag.fastAssociateAll(listOfReadings); // list listener is ONLY on the preferred magnitude.
        // Thus if aMag is not in aSol MagList, or is not preferred,
        // NO list listeners are implemented to update aSol list 
        // so below logic is needed to add data only to an empty, "clear(true)", aSol list:
        magList = aMag.getReadingList();
        //System.out.println(">>> DEBUG AHMED associateReadings(aList, aMag, aSol) readings: \n"+magList.toString());
        if (magList.size() > 0) {
          JasiReadingList solList = (JasiReadingList) aSol.getListFor(magList);
          if (solList != aList) {
            if (solList.isEmpty()) {
              // then either there was no association listener with above mag list add
              // or list was "cleared" for in input aSol
              aSol.fastAssociateAll(aList); // adds the input list data to input solution's list
            }
          }
        }
    }

    protected boolean checkInputAssocAreValid(Magnitude aMag, Solution aSol) {
        // Check for valid inputs
        boolean status = true;
        statusString = "calcMagAssocInputValid";
        if (aSol == null) {
          statusString = "EngineDelegate unable to calculate result, associated input solution null.";
          status = false ;
        }
        if (aMag == null) {
          statusString = "EngineDelegate unable to calculate mag associated input magnitude null.";
          status =  false;
        }
        else if (! aMag.isAssignedTo(aSol)) {
          System.out.println("WARNING! EngineDelegate calcMag inputMag is inputSol preferred? " +
                          (aSol.magnitude == aMag) + "\n" + aMag.toNeatString());
          statusString = "EngineDelegate unable to calculate input magnitude not associated with input solution.";
          status =  false;
        }
        if (! status) {
          reportStatusMessage("ERROR");
          return false;
        }
        return status;
    }

    protected boolean okToLocate(Solution aSol) {
      statusString = "okToLocate";
      boolean status = true;
      LocationEngineIF locEng = getLocationEngine();
      if (locEng == null) {
        statusString = "EngineDelegate null location engine; No solution possible.";
        status = false;
      }
      else if (! haveLocService) {
        statusString = "EngineDelegate has no location engine service; No solution possible.";
        status = false;
      }
      else if (aSol == null) {
        statusString = "EngineDelegate null solution; No associated solution set to locate.";
        status = false;
      }
      else if (chanList == null || chanList.isEmpty()) { // channel latLonZ info matched to phases
        //loadChannelList(); ?
        statusString = "EngineDelegate has null or empty ChannelList; No solution possible.";
        status = false;
      }
      else if ( ! resolveSolPhaseList(aSol) ) {
          statusString = "EngineDelegate too few phases; solution location not possible.";
          status = false;
      }
      if (! status) reportStatusMessage("ERROR");
      return status;
    }

    public boolean hasMagnitudeService() { return haveMagService; }
    public boolean hasLocationService() { return haveLocService; }

    public String [] getMagMethodTypes() {
        return (String []) magMethodMap.keySet().toArray(new String [magMethodMap.size()]);
    }

    public String toString() {
        StringBuffer sb = new StringBuffer(256);
        LocationEngineIF locEng = getLocationEngine();
        if (locEng != null) sb.append(locEng.toString());
        else sb.append("No Location Engine ");
        sb.append("\n MagTypes: ");
        String [] types = getMagMethodTypes();
        if (types.length > 0) {
            for (int ii = 0; ii < types.length; ii++) {
                sb.append(types[ii]).append(" ");
            }
        }
        else sb.append("None");
        sb.append("\n loadArrivalsFromDb: " + loadArrivalsFromDb);
        sb.append("\n loadMagReadingsFromDb: " + loadMagReadingsFromDb);
        return sb.toString();
    }
}
