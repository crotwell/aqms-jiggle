package org.trinet.jasi.engines;
import java.beans.*;
import java.io.*;
import java.util.*;
import org.trinet.jasi.*;
import org.trinet.jasi.magmethods.*;
import org.trinet.jasi.magmethods.TN.*;
import org.trinet.jasi.coda.*;
import org.trinet.jdbc.*;
import org.trinet.util.*;

/**
 * Abstract summary magnitude calculation engine.
 * Subclass for network specific implementation variation of behavior.
 * Methods are implemented to allow the calculation of both the Channel
 * and/or a Solution summary Magnitude from seismic event data by
 * using a MagnitudeMethodIF interface. A given MagnitudeMethodIF
 * implementation creates a Magnitude subtype of interest from
 * the appropriate input data of a Solution or Magnitude instance.
 *
 * @see MagnitudeMethodIF
 * @see AbstractMagnitudeMethod
 * @see ChannelMag
 * @see Solution
 * @see Magnitude
 * @see ChannelMag
 */

public abstract class AbstractMagnitudeEngine extends AbstractSolutionAssocDataEngine implements MagnitudeEngineIF {

    /** The magnitude calculation method. */
    protected MagnitudeMethodIF magMethod = null;

    /** Name of a MagnitudeMethodIF class instantiate initialized from a configuration particular to the subtype. */
    protected String magMethodClassName = null;

    //protected String [] defaultComponentList = {"HH_", "E%", "HL_"};

    protected Magnitude summaryMag = null;  // last calculated success, not null
    protected Magnitude lastNewMag = null;  // last calculated failure, could be input
    protected MagDataStatistics summaryMagStats = null;

    protected boolean forceReadingListClear = true;

    // engine properties:
    protected boolean recalcChanMagsForSumMag = true;
    protected boolean makePreferredMag = true;
    protected boolean addMagToMagList = false;
    protected boolean logMagResiduals = false;
    protected boolean avgStaChannelMags = false;
    protected boolean resetChanMag2StaAvg = true;
    protected String avgStaMagType = "mags"; 
    protected double nextEventWindowSecs = 0.;
    protected double nextEventMaxDist = 50.;

    protected Format ff = new Format("%5.2f");

    /** Default no-op constructor, subtypes can implement a default initialization
     * such a setting a default MagnitudeMethodIF subtype.
     * */
    protected AbstractMagnitudeEngine() {
        this(null);
    }

    /** Constructor sets the active MagnitudeMethodIF for the engine.*/
    protected AbstractMagnitudeEngine(MagnitudeMethodIF magMethod) {
      setMagMethod(magMethod);
    }

// Not sure about network subtypes for engine the initial configuration so
// added logic like JasiObject here:
    /** Factory methods to create an subclass instance of this abstract type.
    * Each subtype has is configured for the invoking network's default
    * implementation as flagged by the value of JasiObject.DEFAULT.
    * @see MagnitudeEngineIF
    * @see JasiObject
    * */
    public static MagnitudeEngineIF create() {
      MagnitudeEngineIF magEng = null;
      try {
        magEng = create(JasiObject.DEFAULT);
      }
      catch (IllegalArgumentException ex) {
        System.err.println("Not a known subtype value: " + JasiObject.DEFAULT);
      }
      return magEng;
    }
    public static MagnitudeEngineIF create(int subtype) throws IllegalArgumentException {
      MagnitudeEngineIF magEng = null;
      switch (subtype) {
        case TRINET:
          magEng = create("org.trinet.jasi.engines.TN.MagnitudeEngineTN");
          break;
        case EARTHWORM:
          magEng = create("org.trinet.jasi.engines.EW.MagnitudeEngineEW");
          break;
        default:
          //System.err.println("Not a known subtype value: " + subtype);
          throw new IllegalArgumentException("Not a known subtype value: " + subtype);
      }
      return magEng;
    }

     // found in DK/DG revision  - aww
    /** Factory methods to create an subclass instance of this abstract type.
    * requires class name by input String to have a no arg constructor, with
    * public access if it is to be created by a class outside its package.
    * Returns null if input is null.
    * */
    public static MagnitudeEngineIF create(String className) {

        // Do what here? force exception, return null or return default create() type
        // for now try null option: -aww
        if (className == null) return null; // ? return create();

        MagnitudeEngineIF magEng = null;
        try {
          magEng = (MagnitudeEngineIF)Class.forName(className).newInstance();
        }
        catch (ClassNotFoundException ex) {
          ex.printStackTrace();
        }
        catch (InstantiationException ex) {
          ex.printStackTrace();
        }
        catch (IllegalAccessException ex) {
          ex.printStackTrace();
        }
        return magEng;
    }

    public void setDefaultProperties() {
        super.setDefaultProperties();
        recalcChanMagsForSumMag = true;
        makePreferredMag = true;
        addMagToMagList = false;
        logMagResiduals = false;
        avgStaChannelMags = false;
        resetChanMag2StaAvg = true;
        avgStaMagType = "mags"; 
    }

    public void initializeEngine() {
        engineIsValid = false;
        try {
          super.initializeEngine();
          if (props == null) props = new SolutionWfEditorPropertyList(); 
          initIOState();
          initMagProcessing();
          initEnvironmentInfo();
          reset();
          engineIsValid = true;
        }
        catch (IllegalStateException ex) {
          System.err.println(ex.getMessage());
          System.err.println(getClass().getName() + " initializeEngine failed!");
        }
    }


    protected void initIOState() {

        if (props.getProperty("logMagResiduals") != null) {
           logMagResiduals = props.getBoolean("logMagResiduals");
        }
        else props.setProperty("logMagResiduals", logMagResiduals);

        /*
        if (props.getProperty("verbose") != null) {
           verbose = props.getBoolean("verbose");
        }
        else props.setProperty("verbose", verbose);

        String propStr = props.getProperty("debug");
        if (propStr != null) debug = propStr.equalsIgnoreCase("true");
        else props.setProperty("debug", debug);
        */

        if (debug) {
           logMagResiduals = true;
           verbose = true;
        }
    }

    protected void initMagProcessing() throws IllegalStateException {

        String propStr = props.getProperty("makePreferredMag");
        if (propStr != null) makePreferredMag = propStr.equalsIgnoreCase("true");
        else props.setProperty("makePreferredMag", makePreferredMag);

        propStr = props.getProperty("addMagToMagList");
        if (propStr != null) addMagToMagList = propStr.equalsIgnoreCase("true");
        else props.setProperty("addMagToMagList", addMagToMagList);

        propStr = props.getProperty("recalcChanMagsForSumMag");
        if (propStr != null) recalcChanMagsForSumMag = propStr.equalsIgnoreCase("true");
        else props.setProperty("recalcChanMagsForSumMag", recalcChanMagsForSumMag );
        
        // true => don't re-scan waveforms to create new readings
        //propStr = props.getProperty("useExistingData");
        //if (propStr != null) useExistingData = propStr.equalsIgnoreCase("true");
        //else props.setProperty("useExistingData", useExistingData); 

        //propStr = props.getProperty("defaultChannelListComponents");
        //if (propStr != null) defaultComponentList = props.getStringArray("defaultChannelListComponents");
        //else props.setProperty("defaultChannelListComponents", defaultComponentList);

        propStr = props.getProperty("avgStaChannelMags");
        if (propStr != null) avgStaChannelMags = propStr.equalsIgnoreCase("true");
        else props.setProperty("avgStaChannelMags", avgStaChannelMags);

        propStr = props.getProperty("avgStaChannelMags.type");
        if (propStr != null) avgStaMagType = propStr;
        else props.setProperty("avgStaChannelMags.type", avgStaMagType); 

        propStr = props.getProperty("avgStaChannelMags.reset");
        if (propStr != null) resetChanMag2StaAvg = ! propStr.equalsIgnoreCase("false");
        else props.setProperty("avgStaChannelMags.reset", resetChanMag2StaAvg);

        if ( props.isSpecified("nextEventWindowSecs") ) {
           nextEventWindowSecs = props.getDouble("nextEventWindowSecs");
        }
        else props.setProperty("nextEventWindowSecs", 0.);

        initMagMethod();
        if ( props.isSpecified("nextEventMaxDist") ) {
           nextEventMaxDist = props.getDouble("nextEventMaxDist");
        }
        else props.setProperty("nextEventMaxDist", 50.);

        initMagMethod();
    }

    protected void initMagMethod() throws IllegalStateException {
        magMethodClassName = props.getProperty("magMethod");
        if (magMethodClassName == null) return;
        if (verbose)
            System.out.println("Magnitude engine create new method instance; getProperty magMethod= " + magMethodClassName);

        try {
            magMethod = (MagnitudeMethodIF) Class.forName(magMethodClassName).newInstance();
        }
        catch (Exception ex) {
            engineIsValid = false;
            System.err.println(ex.getMessage());
            throw new IllegalStateException("MagEng unable to create instance of class: " + magMethodClassName);
        }
        if (magMethod != null) {
          String propString = props.getUserFileNameFromProperty("magMethodProps");
          if (propString != null) magMethod.setProperties(propString); // configure by other file
          else magMethod.setProperties(props); // configure method by engine properties
          // "channelDbLookUp" prop of method is independent of engine, default "true" 
        }
    }

    /** No-op default */
    protected void initEnvironmentInfo() {};

    public void setRecalculationOfChanMagsForSummary(boolean tf) {
        recalcChanMagsForSumMag = tf;
    }
    public boolean recalculateChanMagsForSummary() {
        return recalcChanMagsForSumMag;
    }
    public boolean useExistingChanMagsForSummary() {
        return ! recalcChanMagsForSumMag;
    }

    public Object getResult() { // EngineIF method
        return getSummaryMagnitude();
    }

    public void reset() {
        super.reset();           // resets result and status
        summaryMag = null;
        lastNewMag = null;
        solveSuccess = false;
        if (summaryMagStats == null) summaryMagStats = new MagDataStatistics();
        else summaryMagStats.init();
        if (magMethod != null) magMethod.reset();
    }


    /** Return a current processing status message for this engine instance.*/
    public String getStatusString() {
      StringBuffer sb = new StringBuffer(256);
      sb.append(statusString);
      String tmpStr = getMethodResultsString();
      if (tmpStr != null && tmpStr.length() > 0) {
        sb.append("\n magMethod message: ").append(tmpStr);
      }
      return sb.toString();
    }

    /** Return a current processing status message for this engine instance.*/
    public String getResultsString() {
        return getStatusString();
    }

    public String getMethodResultsString() {
      return magMethod.getResultsString();
    }

    /**
    * Set the magnitude calculation method.
    * @see MagnitudeMethodIF
    * @see AbstractMagnitudeMethod
    */
    public void setMagMethod(MagnitudeMethodIF magMethod) {
      this.magMethod = magMethod;
      engineIsValid = (magMethod != null);
    }
    /**
    * Return the magnitude calculation method.
    * @see MagnitudeMethodIF
    * @see AbstractMagnitudeMethod
    */
    public MagnitudeMethodIF getMagMethod() {
      return magMethod;
    }

    public final double getSummaryMagnitudeValue() {
        // Changed -1. to -9. to make it more obvious to user -aww 08/12/2004
        return (summaryMag == null) ? -9. : summaryMag.value.doubleValue();
    }

    public final Magnitude getSummaryMagnitude() {
        return summaryMag;
    }
    public final boolean isMakePreferredMagEnabled() {
        return makePreferredMag;
    }
    public final void setMakePreferredMag(boolean tf) {
        makePreferredMag = tf;
    }
    public final boolean isAddMagToMagListEnabled() {
        return addMagToMagList;
    }
    public final void setAddMagToMagList(boolean tf) {
        addMagToMagList = tf;
    }

// MagEng Level
    /** Trim outliers and recalculate the magnitude. Readings with residuals greater
    * than that value returned by the method getTrimResidual() have their weights
    * set to 0.0.
    * Returns 'true' if the input Magnitude data list is changed by the scan. */
    public boolean trimByResidual(Magnitude mag) {
      return trimByResidual(mag.getReadingList());
    }

    protected boolean trimByResidual(List aList) {
      double resid = magMethod.getTrimResidual();
      if (resid == Double.MAX_VALUE) return false;  // not set

      boolean changed = false;
      int count = aList.size();
      for (int i=0; i<count; i++) {
        if (magMethod.trimByResidual((MagnitudeAssocJasiReadingIF) aList.get(i), resid) ) changed = true;
      }
      return changed;
    }

    public boolean trimByDistance(Solution aSol) {
      return trimByDistance(aSol.getListFor(magMethod.getReadingClass()), magMethod.getMaxDistance());
    }
    public boolean trimByDistance(Magnitude mag) {
      double cutoff = (mag.hasNullValue()) ?  magMethod.getMaxDistance() : magMethod.getDistanceCutoff(mag);
      return trimByDistance(mag.getReadingList(), cutoff);
    }

    protected boolean trimByDistance(List aList, double cutoff) {
      if (cutoff == Double.MAX_VALUE) return false;
      int count = aList.size();
      boolean changed = false;
      for (int idx=0; idx<count; idx++) {
        if (magMethod.trimByDistance((MagnitudeAssocJasiReadingIF) aList.get(idx), cutoff)) changed = true;
      }
      return changed;
    }
    protected void logMagResiduals(List jrList) {
        int count = jrList.size();
        if (count == 0) return;
        MagnitudeAssocJasiReadingIF jr = null;
        magMethod.logHeader();
        // sort list by distance time
        Comparator cmp = AbstractReversibleSorter.createCompositeComparator(
                             AbstractReversibleSorter.createDistanceSorter(true),
                             //AbstractReversibleSorter.createSiteTimeSorter(true) // 2008/02/07 -aww
                             AbstractReversibleSorter.createSeedChannelSorter(true) // 2008/02/07 -aww
                         );
        if (jrList instanceof ActiveArrayList) {
           // Do not use Collections.sort on ActiveArrayLists!
           ((ActiveArrayList) jrList).sort(cmp);
        }
        else Collections.sort(jrList, cmp);

        for (int idx = 0; idx < count; idx++ ) {
          jr = (MagnitudeAssocJasiReadingIF) jrList.get(idx);
          //if (debug) System.out.println("DEBUG MagEng logMagResidual input jr ["+idx+"]: "+jr.toString());
          magMethod.logChannelResidual(jr);
        }
        System.out.println();
    }

    /**  Sets the values of the channel magnitude residuals for input's readings
     * using the value of the input magnitude.
     * Includes the undeleted, zero-weighted readings, but skips the virtually deleted readings.
     * */
    public void setResiduals(Magnitude mag) {
      setResiduals(mag.getReadingList().getUndeleted(), mag); // undeleted, any wt value, even 0. 
    }
    /**  Sets the values of the channel magnitude residuals for only those readings
     * contributing to the summary value of the input Magnitude.
     * Skips virtually deleted or zero-weighted readings. 
     * */
    public void setContributingResiduals(Magnitude mag) {
      setResiduals(magMethod.getDataUsedBySummaryMagCalc(mag), mag); // reading weightUsed > 0.
    }
    /**  Sets the values of the channel magnitude residuals for every reading 
     * in the input Magnitude's reading list, including deleted or zero-weighted readings. 
     * */
    public void setAllResiduals(Magnitude mag) {
      setResiduals(mag.getReadingList(), mag);
    }

    protected void setResiduals(List aList, Magnitude mag) {
      int count = aList.size();
      for (int i=0; i<count; i++) {
        ((MagnitudeAssocJasiReadingIF)aList.get(i)).setMagResidual(mag);
      }
    }

    /*
     * If 'true' only that part of a waveform that is likely to contain seismic
     * energy will be scanned. If 'false' the whole available waveform is scanned.<p>
     * If 'true' window scanned begins 1 sec before expected P-wave onset and
     * ends at P-wave onset plus 2x the S-P time.
    protected boolean scanWaveformTimeWindow = true;

     * If 'true' the waveform window scanned begins 1 sec before expected
     * P-wave onset and ends at P-wave onset plus 2x the S-P time.
     * If 'false' the whole available waveform is scanned.
    public void setScanWaveformTimeWindow(boolean tf) {
      scanWaveformTimeWindow = tf;
    }
    */

    public final Magnitude getLastNewMag() {
        return lastNewMag;
    }

    /** Return a new magnitude with basic attributes set mag type. */
    public Magnitude getNewMag(Solution aSol, List magDataList ) {
      Magnitude newMag = getNewMag(aSol);
      if (debug) System.out.println("DEBUG MagEng getNewMag newMag.associateAll(magDataList)");
      newMag.associateAll(magDataList);
      return newMag;
    }

    /** Return a new magnitude with basic attributes set mag type. */
    public Magnitude getNewMag(Solution aSol) {
      Magnitude newMag = Magnitude.create();
      // NOTE: regional eventAuthority or solution's origin authority could be different
      //newMag.authority.setValue(aSol.getEventAuthority()); // added -aww 2011/08/17 - removed 2011/09/21 
      newMag.authority.setValue(EnvironmentInfo.getNetworkCode()); // reverted -aww 2011/09/21
      newMag.source.setValue(EnvironmentInfo.getApplicationName());
      newMag.algorithm.setValue(getMagMethod().getMethodName());
      // should this be already set by passed subtype:
      newMag.subScript.setValue(magMethod.getMagnitudeTypeString());
      //02/03 do not add this data to sol MagList: newMag.associate(aSol); // this also sets mag's orid to sol's
      newMag.assign(aSol); // alternative here also sets mag's orid to sol's
      lastNewMag = newMag;
      return newMag;
    }

    /*
    public Magnitude updateWithSummaryData(Magnitude mag) {
      mag.value        = summaryMag.value;
      mag.source       = summaryMag.source;
      mag.algorithm    = summaryMag.algorithm;
      mag.usedStations = summaryMag.usedStations;
      mag.usedChnls    = summaryMag.usedChnls;
      mag.error        = summaryMag.error;
      mag.gap          = summaryMag.gap;
      mag.distance     = summaryMag.distance;
      mag.quality      = summaryMag.quality;
      return mag;
    }
    */

    public boolean solve(Object data) {
      solveSuccess = false;
      if (data instanceof Solution) solveSuccess = solve((Solution)data);
      else if (data instanceof Magnitude) solveSuccess = solve((Magnitude)data);
      return solveSuccess;
    }

/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //
    //public Object solve(Object data); // could change IF hierarchy to return Object - aww
    //
    // Need to check into next few methods implementation below!
    // This solve method hierarchy below differs from my implementation. -aww
    public Magnitude solve(Solution sol) {
      return solve(sol, sol.waveformList);
    }
    public Magnitude solve(Solution sol, Collection waveforms) {
       return solve(null, sol, waveforms);
    }
    public abstract Magnitude solve(Magnitude mag, Solution sol, Collection waveforms);
*/

    /** Calculates a summary Magnitude using Solution data appropiate for the
     * the currently set MagnitudeMethodIF.
     * Uses existing reading data if engine property <i>useExistingData'true</i>
     * and calculates new ChannelMag values unless engine property
     * <i>recalcChanMagsForSumMag=false</i> (default = <i>true</i>).
     * Otherwise scans the appropriate Solution associated Waveforms 
     * calculating new channel and summary magnitude data from the timseries.
     * Sets this summary value as the preferred if 'makePreferredMag' is 'true',
     * otherwise, it is just added to the Solution's alternate magnitudes list.
     * Follows all the rules of currently set MagnitudeMethodIF
     * and checks channel rejection criteria like 'maxDistance'.
     * Does NOT assume data is in any particular order, so if 'maxChannels'
     * is set, only the first 'maxChannels" elements in the list will
     */
    public boolean solve(Solution aSol) {
      return solve(aSol, this.useExistingData); // settable property, false => scan waveforms
    }
    /** Calculates a summary Magnitude using the appropriate Solution data 
     * Uses existing reading data if the input argument <i>useExistingData=true</i>
     * and recalculates new ChannelMag values unless engine property
     * <i>recalcChanMagsForSumMag=false</i> (default = <i>true</i>).
     * Otherwise scans the appropriate Solution associated Waveforms 
     * calculating new channel and summary magnitude data from the timseries.
     * Sets the new summary Magnitude as the preferred if 'makePreferredMag' is 'true',
     * otherwise, it is just added to the Solution's alternate magnitudes list.
     * Follows all the rules of currently set MagnitudeMethodIF
     * and checks channel rejection criteria like 'maxDistance'.
     * Does NOT assume data is in any particular order, so if 'maxChannels'
     * is set, only the first 'maxChannels" elements in the list will
     * contribute to the Magnitude.
     *
     */
    public boolean solve(Solution aSol, boolean useExistingData) {
      if (useExistingData)
         return solve(aSol, (MagnitudeAssociatedListIF)aSol.getListFor(magMethod.getReadingClass()));
      else
         return solveFromWaveforms(aSol, getSolutionWaveformList(aSol));
    }

    /** Calculate a new summary Magnitude from the ChannelMag data
     * of the input list. Recalculates new ChannelMag values first,
     * unless engine property <i>recalcChanMagsForSumMag=false</i>.
     *
     * Sets this summary value as the preferred if 'makePreferredMag' is 'true',
     * otherwise, it is just added to the Solution's alternate magnitudes list.
     * Follows all the rules of currently set MagnitudeMethodIF
     * and checks channel rejection criteria like 'maxDistance'.
     * Does NOT assume data is in any particular order, so if 'maxChannels'
     * is set, only the first 'maxChannels" elements in the list will
     * contribute to the Magnitude.
     *
     */
    public boolean solve(Solution aSol, MagnitudeAssociatedListIF inputList) {
      return solve(getNewMag(aSol, inputList));
    }

    /** Calculate the Magnitude using this Solution and input waveforms.
     * Always creates a new set of readings and ChannelMag values.
     * Sets this summary value as the preferred if 'makePreferredMag' is 'true',
     * otherwise, it is just added to the Solution's alternate magnitudes list.
     * Follows all the rules of currently set MagnitudeMethodIF
     * and checks channel rejection criteria like 'maxDistance'.
     * Does NOT assume data is in any particular order, so if 'maxChannels'
     * is set, only the first 'maxChannels" elements in the list will
     * contribute to the Magnitude.
     *
     */
    public boolean solveFromWaveforms(Solution aSol, List wfList) {
      setSolution(aSol); // resets state, but DOES NOT RE-INIT MagMethod maps !!! Encapsulating Delegate must do that
      Magnitude newMag = getNewMag(aSol); // make a fresh, blank magnitude object
      if (debug) System.out.println("DEBUG MagEng solveFromWaveforms input wflist.size() = "+ wfList.size());
      List aList = createChannelMagDataFromWaveforms(aSol, (List) wfList, forceReadingListClear);
      // no contributing observation data
      if (aList.size() < 1) {
        statusString = "MagEng solveFromWaveforms - failed, 0 new readings created from input waveform count of: " + wfList.size();
        if (verbose) System.out.println(statusString);
        newMag.setStale(false);
        solveSuccess = false; // ?
        return solveSuccess;
      }
      else { // got data now do a summary mag
        try {
          if (debug) {
              System.out.println("DEBUG MagEng solveFromWaveforms newMag.associateAll(aList) of size:" + aList.size());
              System.out.println(aList);
          }
          newMag.associateAll(aList); // forces a like Solution list association
          // used to be: solveSuccess = solve(newMag);
          // false => don't recalc channel magnitudes, already done above 
          calcSummaryMag(newMag, false); // method call sets solveSuccess
        }
        catch (WrongMagTypeException ex) {
          System.err.println("MagEng ERROR: solve(magnitude), wrong type for method: "+ newMag.toNeatString());
          solveSuccess = false;
        }
      }
      return solveSuccess; // assuming newMag data maps to instance summaryMag 
    }

    /** Calculate the Magnitude using the data in its associated list.
     * First calculates new ChannelMag values unless the user set engine property
     * <i>recalcChanMagsForSumMag=false</i>.
     * Sets the new summary Magnitude as the prefered one and the old preferred
     * is retained in the Solution's list of alternate mags.
     * Follows all the rules of the MagnitudeMethodIF and
     * checks the channel rejection criteria like maxDistance.  Does NOT assume
     * the data in list is in any particular order, so if maxChannels is set
     * only the first 'maxChannels" elements in the list will contribute to the
     * Magnitude. <p> It is up to the caller to set the resulting Magnitude as
     * the Solution's magnitude if it wants to. <p> The values of 'authority'
     * and 'source' are set to the Solution's event authority.
     * @see EnvironmentInfo
     * */
    public boolean solve(Magnitude mag) {     // throws WrongMagTypeException {
      setSolution(mag.getAssociatedSolution()); // resets state, but DOES NOT RE-INIT MagMethod maps !!! Encapsulating Delegate must do that
      // are there readings?
      MagnitudeAssociatedListIF magReadingList =
          (MagnitudeAssociatedListIF) mag.getListFor(magMethod.getReadingClass());
      if (magReadingList == null || magReadingList.isEmpty()) {
        if (verbose) System.out.println("MagEng solve(mag) FAILED, input mag is null or its reading list is empty!");
        return false;
      }
      // !!! have distances?
      channelLookup(magReadingList, mag.getAssociatedSolution().getDateTime());
      calcDistances(magReadingList, mag.getAssociatedSolution()); // aww 06/11/2004

      // do the summary mag, data elements are added to the mag
      Magnitude sumMag = null;
      try {
        sumMag = calcSummaryMag(mag); // sets solveSuccess state
      }
      catch (WrongMagTypeException ex) {
        System.err.println("MagEng ERROR: solve(magnitude), wrong type for method: "+ mag.toNeatString());
        return false;
      }
      return solveSuccess; // assuming local method sumMag equals instance summaryMag 
    }

    private void zeroMag(Magnitude mag) {
        mag.value.setValue( 0.0 );      // should be null?
        mag.error.setValue( 0.0 );
        mag.distance.setValue( 0.0 );
        mag.quality.setValue(0.0);
        mag.gap.setValue(360.0); // added to flag bogus -aww 12/23/2005
        solveSuccess = false;
    }

    /** Given a Magnitude with an collection of ChannelMag data,
     * calculate the median magnitude using only non-zero-weight readings.
     * Does NOT apply weights otherwise, just finds median of non-zero weight
     * data (all contributing full weight).
     * Returns the input Magnitude object with proper summary values set.
     * Updates the ChannelMag residuals except those of virtually
     * deleted readings.
     * @see #calcSummaryMagStats(Magnitude)
     * */
    public Magnitude calcMedianMag(Magnitude mag) {
      List aList = magMethod.getDataForSummaryMagCalc(mag); // list without the rejects 
      int count = aList.size();
      double goodList[] = new double[count];
      int nused = 0;
      double closest = Double.MAX_VALUE; // for closest station search

      // No statistical weighting, so median output weights are either 1 = used, 0 = not used
      MagnitudeAssocJasiReadingIF jr = null;
      for (int i=0; i<count; i++) { // Accumulate usable data
        jr = (MagnitudeAssocJasiReadingIF) aList.get(i);
        if ( jr.getInWgt() > 0.0) { //  -aww 2009/06/12 replace wgt with "quality", in_wgt=0 when rejected quality != 0
          jr.setWeightUsed(1.); // full output assoc weight assigned to channelMag
          goodList[nused++] = jr.getChannelMagValue();
          //closest = Math.min( closest, jr.getDistance() ); // no don't archive slant aww 06/11/2004
          closest = Math.min( closest, jr.getHorizontalDistance() ); // horizontal to archive aww 06/11/2004
        }
        else {
          // zero output assoc weight assigned to channelMag, not used in summary
          jr.setWeightUsed(0.);
        }
      }

      // bail if none used
      if (nused > 0)  {
        // calculate revised summary mag
        double medianMag = Math.round(Stats.median(goodList, nused)*100.)*.01; // aww 2008/11/21 added rounding
        mag.value.setValue(medianMag);
        //mag.error.setValue( Stats.standardDeviation(goodList, nused) ); // removed 2007/01/11 -aww
        mag.error.setValue( Stats.medianDeviation(goodList, medianMag, nused) ); // added 2007/01/11 -aww
        //NEED a magnitude quality algorithm implementation here or in Magnitude class!
        mag.quality.setValue(1.0);      // real meaning of quality is undefined
        mag.distance.setValue(closest); // awwDist epicentral?
        // calc residuals, needed for 2nd etc. passes of trimming in createSummaryMag loop
        //setContributingResiduals(mag); // only those undeleted with non-zero wts
        //setAllResiduals(mag); // update all readings
        setResiduals(mag); // only undeleted, the commitable data - aww
        solveSuccess = true;
        mag.setSolveDate(); // set time of success
      } else {
        zeroMag(mag); // sets solveSuccess false;
      }
      mag.setStale(false); // ok, now
      return mag;
    }

    /** Given a Magnitude with an collection of ChannelMag data,
     * calculates statistics using only the non-zero weighted readings
     * of the input Magnitude. The final summary Magnitude is either the
     * median value or the weighted median depending on subclass implementation.
     * Returns the input Magnitude object with proper summary values set and
     * updates the ChannelMag residuals except those of virtually
     * deleted readings.
     * @see #WT_MEDIAN_MAG_VALUE
     * @see #MEDIAN_MAG_VALUE
     * @see #getSummaryMagValueStatType()
     * */
    protected boolean calcSummaryMagStats(Magnitude mag) {
        List aList = magMethod.getDataForSummaryMagCalc(mag); // list without the rejects 
        int count = aList.size();
        if (count < magMethod.getMinValidReadings()) {
            statusString = "MagEng calcSummaryMagStats INFO: input Magnitude data list count < minValidReadings: " +
                count + " < " + magMethod.getMinValidReadings();
            zeroMag(mag); // sets solveSuccess false;
            return false;
        }

        double [] magValues = new double[count];
        double [] range     = new double[count];
        double [] azimuth   = new double[count];
        double [] weights   = new double[count];

        int nused = 0;
        if (avgStaChannelMags) { // need to modify code logic if more than 2-components per sta
            nused = getSummaryAvgStaMagData(magValues, range, azimuth, weights, aList);
        } else {
            nused = getSummaryChannelMagData(magValues, range, azimuth, weights, aList);
        }


        if (nused < magMethod.getMinValidReadings())  { // bail if using less than minValidReadings
            statusString = "MagEng calcSummaryMagStats INFO: Magnitude data list count used < minValidReadings: " +
                               nused + " < " + magMethod.getMinValidReadings();
            zeroMag(mag); // sets solveSuccess false;
            return false;
        }

        // calculate revised summary mag based upon readings stats
        summaryMagStats.calcStatistics(magValues, weights, nused);
        if (verbose) {
            System.out.println( "MagEng magnitude statistics for event id: " + currentSol.getId().longValue() + summaryMagStats.toString());
        }

        // update summary mag data
        Arrays.sort(range,   0, nused); // ascending, first element is closest
        Arrays.sort(azimuth, 0, nused); // must first sort to find max gap
        updateSummaryMag(mag, summaryMagStats.count, summaryMagStats.value, summaryMagStats.getError(),
          range[0], getMaxGap(azimuth, nused));

        // reset reading residuals, needed for 2nd pass etc. of trimming in createSummaryMag loop
        //setContributingResiduals(mag); // only those undeleted with non-zero wts
        setResiduals(mag); // undeleted, the commitable data - aww
        mag.setStale(false); // ok, now
        summaryMag = mag;    // save last results
        statusString = "MagEng SUCCESS - created summary magnitude";
        if (logMagResiduals) logMagResiduals(aList);
        solveSuccess = true; // implies a valid summary mag
        summaryMag.setSolveDate(); // set time of success
        return solveSuccess;
    }

    private int getSummaryAvgStaMagData(double[] magValues, double[] range, double[] azimuth, double[] weights, List aList) {

        //TriaxialGrouper grouper = new TriaxialGrouper(new ChannelableList(aList));  // break input into sublists by station and HH or HN
        StationGrouper grouper = new StationGrouper(new ChannelableList(aList));  // break input into sublists by station can mix HN and HH

        ChannelableListIF stationList = null;
        Channel chan = null;

        int nused = 0;
        double mag = Double.NaN;
        double[] wt = new double[1];

        while (grouper.hasMoreGroups()) { // go through subgroups in input list

            stationList = grouper.getNext(); // output is same class type as original list

            // For mag derived like done in ew localmag must set avgStaMagType= "mags", "amps", or "taus" ?
            // resetChanMag2StaAvg=true resets readings mag,corr,wt to sta avg values
            // avg sta wt is returned in the input array wt, default is 1.
            wt[0] = 1.;
            mag = magMethod.calcStaMagFromReadings(stationList, avgStaMagType, resetChanMag2StaAvg, wt);
            if (Double.isNaN(mag)) continue;  // skip it, bad station data?

            magValues[nused] = mag; // sta avg mag  
            weights[nused]   = wt[0]; // sta avg wt

            chan = ((Channelable)stationList.get(0)).getChannelObj();
            range[nused]     = chan.getHorizontalDistance();
            azimuth[nused]   = chan.getAzimuth();

            nused++; // bump, sta included in summary stat arrays

            if (verbose) {
                System.out.println("AbstractMagnitudeEngine getSummaryAvgStaMagData by " + avgStaMagType + " nsta: " + nused +
                                   " mag: " + ff.form(mag) + " wgt: " + ff.form(wt[0]) +
                                   " sta: " + chan.getNet() + "." + chan.getSta() 
                                  );
            }
        }

        return nused;
    }

    private int getSummaryChannelMagData(double[] magValues, double[] range, double[] azimuth, double[] weights, List aList) {
        MagnitudeAssocJasiReadingIF jr = null;
        Channel chan = null;
        int nused = 0;
        int count = aList.size();
        for (int i=0; i<count; i++) { // Accumulate usable data, wgt > 0
          jr = (MagnitudeAssocJasiReadingIF) aList.get(i);
          
          // inWgt have been derived via the current magnitude method's
          // pre- post - filtering, trim, or assignWt implementations 
          double wgt = jr.getInWgt();  //  -aww 2009/06/12 replace wgt with "quality", in_wgt=0 when rejected quality != 0
          if (! jr.isReject()) {
              wgt = jr.getQuality(); // not rejected so set used to the quality -aww 2009/08/28
              // Below added to utilized property in magnitude methods when set - aww 2008/11/16
              if (! magMethod.useAssignedWts()) wgt = 1.; // force all channelmags not "rejected" to contribute equally 
          }

          jr.setWeightUsed(wgt);  // sets output assoc weight assigned to channelMag

          if (wgt > 0.0) {
            //assume already have channel mag calculated with its channel correction applied
            magValues[nused] = jr.getChannelMagValue();
            chan   = jr.getChannelObj();
            range[nused]     = chan.getHorizontalDistance();
            azimuth[nused]   = chan.getAzimuth();
            weights[nused]   = wgt;
            if (debug) {
                System.out.println("DEBUG AbstractMagnitudeEngine getSummaryChannelMagData nused: " + (nused+1) +
                            " sta: " + chan.getSta() + " mag: " + magValues[nused] + " wgt: " + wgt);
            }
            nused++;
          }

        }

        return nused;
    }

    /** Calculate summary magnitude from the list data of the input Magnitude.
     * First calculates new ChannelMag values unless the user set engine property
     * <i>recalcChanMagsForSumMag=false</i>.
     * Doesn't use zero-weighted readings in the summary.
     * Finds median on non-zero weight data and returns the Magnitude object.
     * Sets residuals for ALL data regardless of weight.
     * */
    public Magnitude calcSummaryMag(Magnitude mag) throws WrongMagTypeException {
        return calcSummaryMag(mag, this.recalcChanMagsForSumMag); // settable property, false => no new chanMag
    }
    /** Calculate summary magnitude from the list data of the input Magnitude.
     * First calculates new Channel Magnitudes for the readings if input
     * <i>calcChannelMags=true</i>, otherwise uses those of the readings
     * associated the input Magnitude.
     * Doesn't use zero-weighted readings in the summary.
     * Finds median on non-zero weight data and returns the Magnitude object.
     * Sets residuals for ALL data regardless of weight.
     * */
    public Magnitude calcSummaryMag(Magnitude mag, boolean calcChannelMags) throws WrongMagTypeException {
      // calculate the associated readings channel magnitudes
      // pre-filters the list, throws WrongMagTypeException:
      if (calcChannelMags) calcChannelMag(mag);
      Magnitude myMag = createSummaryMag(mag);
      if (updateSolutionAssociation(myMag))
        firePropertyChange(EngineIF.SOLVED, (Magnitude) null, myMag);
      if (debug) System.out.println("DEBUG MagEng new mag readingList size(): "+myMag.getReadingList().size());
      if (success()) statusString = "MagEng DONE: created summary Netmag";
      return myMag;
    }

    //process all observation in input's list without summary calc:
    public int calcChannelMag(Magnitude mag) throws WrongMagTypeException {
      if (!  magMethod.isIncludedMagType(mag) ) {
        throw new WrongMagTypeException("Input magnitude is wrong type for method, type = " + mag.getTypeString());
      }
      List aList = null;
      try {
        magMethod.filterDataBeforeChannelMagCalc(mag);
        aList = magMethod.getValidChannelMagData(mag);
        if (aList.size() < 1 && verbose)
          System.out.println("MagEng filter found NO acceptable input data for channel magnitude calculations !");
      }
      catch (WrongDataTypeException ex) {
        ex.printStackTrace();
      }
      return calcChannelMag(aList);
    }

    protected int calcChannelMag(List aList) {
      int count = aList.size();
      MagnitudeAssocJasiReadingIF jr = null;
      if (debug) System.out.println("DEBUG calcChannelMag input list size: " + count);
      for (int idx=0; idx<count; idx++) {
        try {
          jr = (MagnitudeAssocJasiReadingIF) aList.get(idx);
          magMethod.calcChannelMag(jr);
        }
        catch (WrongDataTypeException ex) {
          //magMethod.deleteReading(jr); // now uses remove 07/15/2004 -aww
          // MagMethod does remove to preserve state for aliased elements of other lists 07/15/2004 -aww
          magMethod.removeReading(jr); // now uses remove 07/15/2004 -aww
          System.err.println(ex.getMessage());
        }
      }
      return count;
    }


    // virgin channel mags from raw observations, no pre-filtering, no summary
    /** Calculates ChannelMagnitude's for the Solution data appropiate for the
     * currently set MagnitudeMethodIF.
     * Uses existing datasource data if engine property <i>useExistingData=true</i>,
     * otherwise calculates new magnitude data from scans of the Solution waveforms.
     */
    public int calcChannelMag(Solution aSol) {
      return calcChannelMag(aSol, this.useExistingData); // settable property, false => scan waveforms
    }

    /** Calculates ChannelMagnitude's for the Solution data appropiate for the
     * currently set MagnitudeMethodIF.
     * Uses existing data if input argument 'useExistingData' is true,
     * otherwise calculates new magnitude data from scans of the Solution waveforms.
     */
    public int calcChannelMag(Solution aSol, boolean useExistingData) {
      setSolution(aSol); // resets for this instance, but DOES NOT RE-INIT MagMethod maps !!! Encapsulating Delegate must do that
      if (aSol == null) {
        if (verbose) System.out.println("MagEng calcChannelMag null solution input");
        return 0;
      }
      JasiCommitableListIF jrl = aSol.getListFor(magMethod.getReadingClass());
      if (jrl == null) {
        System.err.println("MagEng ERROR: list data not found for associated magnitude method: " +
                           magMethod.getMethodName() +
                           " data reading data class: " +
                           magMethod.getReadingClass().getName()
                          );
        return 0;
      }

      int count = 0; // number of processed channel magnitudes 
      List aList = null;
      if (useExistingData) {
        //trimByDistance(jrl, magMethod.getMaxDistance()); // removed 03/23/2006 aww instead done by below filter
        magMethod.filterDataBeforeChannelMagCalc(jrl); // deletes unacceptable by type,component, or distance 
        aList = magMethod.getValidChannelMagData(jrl); // rejects deleted readings from list 
        count = calcChannelMag(aList); // only acceptable readings used
      }
      else {
        aList = createChannelMagDataFromWaveforms(aSol, getSolutionWaveformList(aSol), forceReadingListClear);
        // below could use addOrReplace for different types to avoid pitfalls of forceReadingListClear
        // depends on whether associate(jr) does "add" or "addOrReplace"
        jrl.addAll(aList);   // add new data to solution's list, no summary mag yet
        count = aList.size();
      }
      return count;
    }

    public List createChannelMagDataFromWaveforms(Solution aSol, List wfList, boolean clearReadingList) {
      if (wfList == null  || wfList.isEmpty()) {
        if (verbose) {
          System.out.println("MagEng createChannelMagDataFromWaveforms: null/empty waveform list for "
                         + aSol.getId());
          return new ArrayList(0);
        }
      }
      else if (debug) {
        System.out.println("DEBUG MagEng createChannelMagDataFromWaveforms wflist.size()= "+ wfList.size());
      }

      JasiCommitableListIF jrl = aSol.getListFor(magMethod.getReadingClass());
      if (jrl == null) {
        System.err.println("MagEng ERROR: list data not found for associated magnitude method: " +
                           magMethod.getMethodName() +
                           " data reading data class: " +
                           magMethod.getReadingClass().getName()
                          );
        return new ArrayList(0);
      }

      if (clearReadingList) {
        //jrl.clear();  // clear away any pre-existing readings - removed 8/04/2004
        // test clear of readings WITHOUT notify to prefmag, should allow prior prefmag to keep data in its list - aww
        jrl.clear(false);
        if (debug) System.out.println("DEBUG MagEng createChannelMagDataFromWaveforms jrl.clear()");
      }

      channelLookup(wfList, aSol.getDateTime());

      if (! hasChannelList()) { // channelLookup should have created some ChannelList data
        System.err.println("MagEng ERROR: createChannelMagDataFromWaveforms ChannelList data retrieval failed.");
        return new ArrayList(0);
      }

      // Waveform from database in a list returned by getBySolution may not have its distance set, so:
      calcDistances(wfList, aSol); // aww 06/11/2004

      // get mag data trimmed by distance list, list is distance sorted:
      //List aList = magMethod.createAssocMagDataFromWaveformList(aSol, wfList);
      SolutionList solList = new SolutionList(2);
      if ( nextEventWindowSecs > 0. ) {
        solList.fetchValidByTime(aSol.getTime(), aSol.getTime()+nextEventWindowSecs);
      }
      solList.addOrReplaceById(aSol);
      solList.setSelected(aSol);
      solList.sortByTime();

      // Is there more than 1 event within this window?
      if ( solList.size() > 1) {
        Solution [] sols = solList.getArray();
        // Cull any that are farther than the nextEventMaxDist
        for ( int idx=0; idx < sols.length; idx++) {
          if ( aSol == sols[idx] ) continue;
          if (aSol.calcHorizDistance(sols[idx].getLatLonZ()) > nextEventMaxDist ) {
              if (verbose) {
                System.out.println("    MagEng createChannelMagDataFromWaveforms found " + solList.size() +
                        " event(s) within next event window, removing event located > " + nextEventMaxDist +
                        " km away:\n" + sols[idx].toNeatString());
              }
              solList.remove(sols[idx]);
          }
        }
      }
      List aList = magMethod.createAssocMagDataFromWaveformList(solList, wfList);

      calcChannelMag(aList);

      if (verbose) {
        System.out.println("    MagEng createChannelMagDataFromWaveforms readings count: " + aList.size() +
                        " created from waveforms count:" + wfList.size());
      }
      return aList;
    }


    /** Convenience wrapper.
     *  Returns 'true' if the channel can contribute a summary magnitude derivation by the current MagMethod.
     *  Returns false if MagMethod is undefined (null).
     *  @see #setMagMethod(MagnitudeMethodIF)
     * */
    public boolean isSummaryMagType(Channel chan) {
      return (magMethod == null) ? false : magMethod.summaryChannelType(chan);
    }
    /** Convenience wrapper.
     *  Returns 'true' if the channel can contribute to a channel magnitude derivation by the current MagMethod.
     *  Returns false if MagMethod is undefined (null).
     *  @see #setMagMethod(MagnitudeMethodIF)
     * */
    public boolean isChannelMagType(Channel chan) {
      return (magMethod == null) ? false : magMethod.isIncludedComponent(chan);
    }
    /*
    public boolean methodAccepts(Channel chan) {
      return (magMethod == null) ? false : magMethod.acceptChannelType(chan);
    }
    */

    protected Magnitude updateSummaryMag(Magnitude mag, int count, double value, double err,
                    double minRange, double maxGap) {
        //if (! NullValueDb.isBlank(currentSol.source.toString()))
        //    mag.source.setValue(currentSol.source); // not always true, set below -aww
        mag.source.setValue(EnvironmentInfo.getApplicationName()); // aww 01/11/2005
        value = Math.round(value*100.) * .01; // aww 2008/11/21 added rounding
        mag.value.setValue(value);
        mag.subScript.setValue(magMethod.getMagnitudeTypeString());
        //mag.authority.setValue(mag.sol.getEventAuthority()); // added -aww 2011/08/17, removed 2011/09/21
        mag.authority.setValue(EnvironmentInfo.getNetworkCode()); // reverted -aww 2011/09/21
        mag.algorithm.setValue(magMethod.getMethodName());
        mag.setProcessingState(EnvironmentInfo.getAutoString());  // added 2007/11/02 -aww (case of recalc of old 'A' mag by 'H'?)
        mag.error.setValue(err);
        mag.gap.setValue(maxGap);
        //mag.gap.setValue(mag.getReadingList().getMaximumAzimuthalGap());
        //mag.usedStations.setValue(mag.getReadingList().getStationUsedCount());  // number of sites, not channels
        //mag.usedStations.setValue(count); // count is non zero wt channels, now used in new field below - aww 2008/07/10
        mag.usedStations.setNull(true); // reset - aww 2008/07/10
        //mag.usedChnls.setValue(count); // non zero wt channels - aww 2008/07/10
        mag.usedChnls.setNull(true); // reset - aww 2011/04/22
        mag.distance.setValue(minRange);
        // Arbitrary value set to 1. need algorithm to weight "quality" based upon statistics
        mag.quality.setValue(1.0);      // real meaning of quality is undefined for now
        return mag;
    }

// Begin calc summaryMag methods
    /**
     * Calculates a summary magnitude value.
     * If engine property <i>useExistingData=true</i>, uses the existing database data
     * associated with the input Solution, otherwise rescans the Solution's associated
     * waveforms creating new reading observations and ChannelMagnitudes.
     * If engine property <i>recalcChanMagsForSumMag=false</i>, ChannelMags are
     * not recalculated for readings, those already associated with the data are used.
     * Unless set otherwise, the default is to calculate new channel magnitudes for
     * existing data.
     */
    public Magnitude calcSummaryMag(Solution aSol) {
      return calcSummaryMag(aSol, this.useExistingData); // settable property, false => scan waveforms
    }
    /**
     * Calculates a summary magnitude value.
     * If input argument <i>useExistingData=true</i>, uses the existing datasource data
     * associated with the input Solution, otherwise rescans the Solution's associated
     * waveforms creating new reading observations with ChannelMagnitudes.
     * If engine property <i>recalcChanMagsForSumMag=false</i>, ChannelMags are
     * not recalculated for readings, those already associated with the data are used.
     * Unless set otherwise, the default is to calculate new channel magnitudes for
     * existing data.
     */
    public Magnitude calcSummaryMag(Solution aSol, boolean useExistingData) {
        setSolution(aSol); // resets for this instance, but DOES NOT RE-INIT MagMethod maps !!! Encapsulating Delegate must do that
        Class aClass = magMethod.getReadingClass();
        MagnitudeAssociatedListIF aList = (MagnitudeAssociatedListIF) aSol.getListFor(aClass);

        if (useExistingData) { // get from db, don't scan waveforms
            // assumes load method populates aList alias above
            if (aList.isEmpty()) aSol.loadListFor(aClass);
            // check settable property, false => no new ChannelMags, use those loaded from db 
            if (this.recalcChanMagsForSumMag) calcChannelMag(aSol, useExistingData);
        }
        else { // scan waveform timseries to create new data
          // assumes calcChannelMag method populates aList alias above with new data
          calcChannelMag(aSol, false); // false => scan waveforms
        }
        // aList = (MagnitudeAssociatedListIF) aSol.getListFor(aClass); // just use alias above
        return calcSummaryMag(aList);
    }

    /** Calculate a summary Magnitude from the data of the input list.
     * Does not calculate any Channel magnitudes, those of the input list
     * are used to determine the statistical summary value.
     * */
    protected Magnitude calcSummaryMag(List dataList) {
        summaryMag = null; // reset summary result
        if (currentSol == null)
          throw new NullPointerException("MagEng ERROR: solution is null, see setSolution(Solution)");

        if (dataList == null)  {
            statusString = " MagEng ERROR: List input parameter null";
            System.err.println(statusString + " for solution id: " + currentSol.getId().longValue());
            return null;
        }

        if (dataList.size() < magMethod.getMinValidReadings()) {
            statusString = "MagEng INFO: input data list size < minValidReadings: " +
             dataList.size() + " < " + magMethod.getMinValidReadings();
            if (verbose) System.out.println(statusString + " for solution id: " + currentSol.getId().longValue());
            return null;
        }

        // filter readings to acceptable corrected types and create a new summary magnitude object:
        summaryMag = createSummaryMag(dataList);
        if (summaryMag == null) return null;
        if (updateSolutionAssociation(summaryMag))
           firePropertyChange(EngineIF.SOLVED, (Solution) null, currentSol);
        if (success()) statusString = "MagEng DONE: created summary Netmag";
        return summaryMag;
    }

    protected boolean updateSolutionAssociation(Magnitude mag) {
        mag.assign(currentSol); // first just affiliate
        if (debug)
          System.out.println("DEBUG updateSolutionAssociation(mag) solveSuccess: "+solveSuccess+" makePref: "+makePreferredMag);
        if (solveSuccess) { // set only good results - aww 08/22/2003
          if (makePreferredMag) {
            currentSol.setPreferredMagnitude(mag);
            if (debug) {
              System.out.println("DEBUG MagEng after setPref sol codalist size:"+currentSol.getCodaList().size()+
               " ampList size:"+currentSol.getAmpList().size()+" newMag readingList size :"+mag.getReadingList().size());
            }
          }
          else if (addMagToMagList) {
            //currentSol.add(mag); // may add to sol maglist 2/03 aww
            currentSol.addOrReplaceMagnitudeByType(mag); // replaces above code line 07/15/2004 -aww
            // above replaces a prexisting Magnitude of a type with input magnitude object
          }
        }
        // Do we want to return false if not valid summary mag ?
        // need to check if event firing is always needed for any modified
        // "deleted" data in reading list.
        return true;
    }

    protected Magnitude createSummaryMag(Magnitude mag) throws WrongMagTypeException {
      try {
        magMethod.filterDataBeforeSummaryMagCalc(mag); // summary pre-filter rejects data
      }
      catch (WrongDataTypeException ex) {
        ex.printStackTrace();
        statusString = "MagEng ERROR: createSummaryMag input Magnitude wrong type: "+ mag.getTypeString();
        return mag;
      }
        //
        // magMethod.getData... below could invoke magMethod's summary pre-filter as above
        // The pre-filter rejects any readings that do not have a correction,
        // if requireCorrection=true (looks for non-null correction in channel object
        // collection based on the specific magnitude subtype). Filter also
        // rejects readings whose component types are not acceptable for summary mag 
        //

      //removed test below, redundant with calcSummaryMagStats method - aww 2008/01/21 
      //List summaryDataList = magMethod.getDataForSummaryMagCalc(mag); // list without rejects 
      //if (summaryDataList.size() < magMethod.getMinValidReadings()) {
        //statusString = "MagEng INFO: createSummaryMag summary data list size < minValidReadings: " +
        //     summaryDataList.size() + " < " + magMethod.getMinValidReadings();
        //return mag;
      //}

      // iterate, keep calulating the median magnitude and trimming readings until it's stable
      int passCount = 0;
      boolean filtered = false;
      boolean status = true;
      try {
        do  { // is it statistically valid to keep trimming? -aww 2007/12/13 

          passCount++;
          status = calcSummaryMagStats(mag);  // note each call invokes getDataForSummaryMagCalc which re-culls the list (e.g. low-gains) 
          if (!status) {
            if (verbose) System.out.println(statusString);
            break; // too few readings
          }
          // filter channel data by distance,residual and maxchannels, input mag becomes stale if contributing data are deleted
          filtered = magMethod.filterDataAfterSummaryMagCalc(mag);
          if (debug) {
            System.out.println("DEBUG MagEng createSummaryMag pass: "+passCount+
                               " filtered:"+filtered+" isStale: "+mag.isStale() +
                               "\n mag: "+mag.toNeatString()
                              );
          }
        } while (filtered);  // are higher iterations statistically valid? - aww


        if (status) { // Do Chauvenet once, at end of other trimming
          filtered = magMethod.trimBySummaryStatistics(mag); // rejects have wt = 0
          if (filtered) calcSummaryMagStats(mag);
          if (debug) System.out.println("\nDEBUG MagEng createSummaryMag trimBySummaryStatistics trimmed:"+ filtered);

          // force mag residual update of all readings with current magnitude value
          // (includes the deleted and zero-wt readings) - aww 03/04
          if (passCount > 0) setAllResiduals(mag); // aww 03/04
        }
      }
      catch (WrongDataTypeException ex) {
        ex.printStackTrace();
      }
      //mag.setStale(false); // reset after abnormal exit of loop ?
      return mag;
    }

    /** Doesn't create new ChannelMags, uses those in input data list for determine summary. */
    protected Magnitude createSummaryMag(List dataList) {
        Magnitude mag = null;
        try {
          mag = createSummaryMag(getNewMag(currentSol, dataList)); // sets status
        }
        catch (WrongDataTypeException ex) {
          ex.printStackTrace();
          statusString = "MagEng ERROR: createSummaryMag(List) wrong input type.";
        }
        return mag;
    }

// MagEng Level Bulk processor
// begin extension of bulk calcChannelMag methods int return is number of solutions with success
    public int calcChannelMagForEvid(long evid) {
        return calcChannelMag(Solution.create().getById(evid)); // create a Solution object using prefor of evid
    }
    /** Input times true epoch with leap seconds. */
    public int calcChannelMagByTime(double start, double end) {
        return calcChannelMag((Solution []) Solution.create().getByTime(start, end).toArray(new Solution[0]));
    }
    public int calcChannelMag(GenericSolutionList solList) {
        return calcChannelMag(solList.getArray());
    }
    public int calcChannelMag(Solution [] solutions) {
        int size = (solutions == null) ?  0 : solutions.length;
        if (size < 1) return 0;
        int successCount = 0;
        for (int index = 0; index < size; index++) {
            if (calcChannelMag(solutions[index]) > 0) successCount++;
        }
        return successCount;
    }
// end extension of bulk calcChannelMag convenience methods
// begin extension of bulk calcSummaryMag methods int return is number of solutions with success
    /** Creates a new Solution loading from the datasource any data associated with the
     * the input id. Calculates a summary magnitude value.
     * @see #calcSummaryMag(Solution)
     */
    public Magnitude calcSummaryMagForEvid(long id) {
      return calcSummaryMagForEvid(id, this.useExistingData); // settable property, false => scan waveforms
    }
    /** Calculates a summary magnitude value.
     * Creates a Solution, if input argument <i>useExistingData=true</i>,
     * uses the existing datasource data associated with the input Solution,
     * otherwise rescans the Solution's associated waveforms creating new
     * reading observations with ChannelMagnitudes.
     * the input id if input argument useExistingData=true, otherwise scan the 
     * wave
     * @see #calcSummaryMag(Solution, boolean)
     */
    public Magnitude calcSummaryMagForEvid(long id, boolean useExistingData) {
        Solution aSol = Solution.create().getById(id);
        if (aSol == null) {
            statusString = "MagEng ERROR: calcSummaryMagForEvid - no solution for input id:" + id;
            if (verbose) System.out.println(statusString);
            return null;
        }
        return calcSummaryMag(aSol, useExistingData);
    }

    /** Calculate new summary mag value for every Solution whose preferred origin is within
     * the specified epoch start and end time span.
     * @see #calcSummarymag(Solution)
     * */
    public int calcSummaryMagByTime(double start, double end) {
        return calcSummaryMag((Solution []) Solution.create().getByTime(start, end).toArray(new Solution[0]));
    }
    /** Calculate new summary mag value for every Solution in the input list
     * @see #calcSummarymag(Solution)
     * */
    public int calcSummaryMag(GenericSolutionList solList) {
        return calcSummaryMag((Solution []) solList.toArray(new Solution[solList.size()]));
    }
    /** Calculate new summary mag value for every Solution in the input array
     * @see #calcSummarymag(Solution)
     * */
    public int calcSummaryMag(Solution [] solutions) {
        int size = (solutions == null) ? 0 : solutions.length;
        if (size < 1) return 0;
        int successCount = 0;
        for (int index = 0; index < size; index++) {
            if (calcSummaryMag(solutions[index]) != null) successCount++;
        }
        return successCount;
    }
// end of extension for bulk convenience methods

    //Engine inner class for channel mag stats use for summary
    public class MagDataStatistics {
        public int    count;
        public double value;
        public double mean;
        public double median;
        public double weightedMedian;
        public double medDev;
        public double stdDev;
        //public double quality;

        public void init() {
            count           = 0;
            value           = 0.;
            mean            = 0.;
            median          = 0.;
            weightedMedian  = 0.;
            medDev          = 0.;
            stdDev          = 0.;
        }

        // Need more robust statistics to allow for different channel data counts, small data sets
        public void calcStatistics(double [] magValues, double [] weights, int dataCount) {
            count           = dataCount;
            mean            = Stats.mean(magValues, count);
            median          = Stats.median(magValues, count);
            //weightedMedian  = WeightedMedian.calcMedian(count, magValues, weights);
            weightedMedian  = HypoinvWeightedMedian.calcMedian(count, magValues, weights);
            stdDev          = Stats.standardDeviation(magValues, count);

            int statType = magMethod.getSummaryMagValueStatType();
            if (statType == MagnitudeMethodIF.WT_MEDIAN_MAG_VALUE) {
                value = weightedMedian;
            }
            else if (statType == MagnitudeMethodIF.MEDIAN_MAG_VALUE) {
                value = median;
            }
            else if (statType == MagnitudeMethodIF.AVG_MAG_VALUE) {
                value = mean;
            }

            medDev          = Stats.medianDeviation(magValues, value, count); // replaced stddev above 2007/01/11 -aww
        }
        public double getError() {
            return medDev; // 01/12/2007 changed from stdDev -aww
        }
        public String toString() {
            StringBuffer sb = new StringBuffer(132);
            sb.append(" wgtMedian:");
            //Concatenate.format(sb, weightedMedian, 3, 2);
            sb.append(ff.form(weightedMedian));
            sb.append(" median:");
            //Concatenate.format(sb, median, 3, 2);
            sb.append(ff.form(median));
            sb.append(" mean:");
            //Concatenate.format(sb,mean, 3, 2);
            sb.append(ff.form(mean));
            sb.append(" stdDev:");
            //Concatenate.format(sb, stdDev, 3, 2);
            sb.append(ff.form(stdDev));
            sb.append(" medDev:");
            //Concatenate.format(sb, medDev, 3, 2);
            sb.append(ff.form(medDev));
            sb.append(" count:");
            Concatenate.format(sb, count, 4);
            return sb.toString();
        }
    } // end of MagDataStatistics inner class
/*
 public static final class Tester {
   public static final void main(String args[]) {
      if (args.length < 5) {
        System.out.println ("arg syntax: <dbhost> <user> <passwd> <evid> <magType (ml,md ...)> [wave]");
        System.exit(0);
      }
      String host = args[0];
      String user   = args[1];
      String passwd = args[2];
      long evid = Long.parseLong(args[3]);
      String magType = args[4];
      String methodType = (args.length < 6) ? "parms" : args[5];

      System.out.println ("Main: Making DataSource connection to "+host);
      String url= "jdbc:" + AbstractSQLDataSource.DEFAULT_DS_SUBPROTOCOL +":@"+ host +":"+ AbstractSQLDataSource.DEFAULT_DS_PORT +":"+ dbname;
      System.out.println("Main db url: " + url);
      boolean writeBack = false;
      DataSource dbs = new DataSource(url, org.trinet.jdbc.datasources.AbstractSQLDataSource.DEFAULT_DS_DRIVER, user, passwd,writeBack);
      if (dbs == null) System.exit(0);
      System.out.println ("Main: Getting evid = "+evid);
      Solution aSol = Solution.create().getById(evid);
      if (aSol == null) {
        System.err.println("Main: Null solution, check id");
        dbs.close();
        System.exit(0);
      }

      EnvironmentInfo.setNetworkCode("CI");
      EnvironmentInfo.setApplicationName("MagEng");
      EnvironmentInfo.setAutomatic(false);

      List aList = null;
      MagnitudeEngineIF magEng = AbstractMagnitudeEngine.create();
      if (magType.equals(CorrTypeIdIF.ML)) {
        magEng.setMagMethod((MagnitudeMethodIF) new CuspSoCalMlMagMethod());
        aList = (List) Amplitude.create().getBySolution(aSol, new String [] {"WA", "WAS", "WAU"}, false);
        magEng.setProperties("C:\\Program Files\\Mung\\mlMagEng.props");
        magEng.getMagMethod().setProperties("C:\\Program Files\\Mung\\mlMagMeth.props");
        magEng.getMagMethod().setLookUp(false); // don't probe database for missing data
      }
      else if (magType.equals(CorrTypeIdIF.MH)) {
        magEng.setMagMethod((MagnitudeMethodIF) new CuspSoCalMhMagMethod());
        aList = (List) Amplitude.create().getBySolution(aSol, new String [] {"WA","HEL","WAS", "WAU"}, false);
        magEng.setProperties("C:\\Program Files\\Mung\\mhMagEng.props");
        magEng.getMagMethod().setProperties("C:\\Program Files\\Mung\\mhMagMeth.props");
        magEng.getMagMethod().setLookUp(false); // don't probe database for missing data
      }
      else if (magType.equals(CorrTypeIdIF.MD)) {
        magEng.setMagMethod((MagnitudeMethodIF) new LeeMdMagMethod());
        aList = (List) Coda.create().getBySolution(aSol, new String [] {CodaDurationType.D.getName()}, false);
        magEng.setProperties("C:\\Program Files\\Mung\\mdMagEng.props");
        magEng.getMagMethod().setProperties("C:\\Program Files\\Mung\\mdMagMeth.props");
        magEng.getMagMethod().setLookUp(false); // don't probe database for missing data
      }
      else if (magType.equals(CorrTypeIdIF.MC)) {
        magEng.setMagMethod((MagnitudeMethodIF) McMagMethod.create());
        aList = (List) Coda.create().getBySolution(aSol, new String [] {CodaDurationType.A.getName()}, false);
        magEng.setProperties("C:\\Program Files\\Mung\\mcMagEng.props");
        magEng.getMagMethod().setProperties("C:\\Program Files\\Mung\\mcMagMeth.props");
        //magEng.getMagMethod().loadCalibrations(aSol.getDateTime());
        magEng.getMagMethod().setLookUp(false); // don't probe database for missing data
      }
      else {
        System.err.println("Main: Unknown magMethod type, check input = mc,md,ml,or mh: " + magType);
        dbs.close();
        System.exit(0);
      }
      if (methodType.equals("wave")) {
        System.out.println("Main: now creating new mag data list from Solution waveforms ...");
        aList = magEng.createChannelMagDataFromWaveforms(aSol, magEng.getSolutionWaveformList(aSol), true);
      }
      else {
        System.out.println("Main: Processing mag with existing readings, no waveform processing");
      }
      Magnitude aNewMag = magEng.getNewMag(aSol, aList);
      System.out.println ("Main: MagEng new mag for sol: "+aSol.toString());
      System.out.println ("Main: MagEng new mag dump BEFORE calc:\n" + aNewMag.toDumpString()) ;
      System.out.println ("") ;
      JasiReadingListIF jrl = aNewMag.getReadingList();
      jrl.distanceSort(true, JasiReadingListIF.SITE_TIME_SORT);
      System.out.println (jrl.toNeatString(true));
      System.out.println ("") ;
      System.out.println ("Main: MagEng calculating a new summary mag by method: "+
                      magEng.getMagMethod().getMethodName()+" for input id: "+evid);
      magEng.solve(aNewMag); // returns boolean
      System.out.println("Main: MagEng solve(aNewMag) successful? " + magEng.success());
      Magnitude prefMag =  aSol.getPreferredMagnitude();
      System.out.println("Main: MagEng prefmag = aNewMag ? "+(prefMag == aNewMag));
      System.out.println ("Main: MagEng prefmag dump AFTER calc:\n" + prefMag.toDumpString()) ;
      if (prefMag !=  aNewMag) {
        System.out.println ("Main: MagEng aNewMag dump AFTER calc:\n" + aNewMag.toDumpString()) ;
      }
      jrl = prefMag.getReadingList();
      jrl.distanceSort(true, JasiReadingListIF.SITE_TIME_SORT);
      System.out.println (jrl.toNeatString(true));
      System.out.println ("") ;
      System.out.println("Main: MagEng chanList size: "+
                      ( (magEng.getChannelList() == null) ? 0 : magEng.getChannelList().size()));
      //
      //if (magEng.debug == true && magEng.chanList != null) {
      //  System.out.println("Main: magEng.chanList dump:\n" + magEng.chanList.toDumpString());
      //}
      System.out.println("Main: FINISHED TEST RUN");
      dbs.close();
   }
  } // end of static class Tester
*/
} // AbstractMagnitudeEngine
