package org.trinet.jasi.engines;
import java.util.*;
import org.trinet.jasi.*;
import org.trinet.jasi.magmethods.*;
import org.trinet.util.*;

public interface MagnitudeEngineIF extends SolutionDataEngineIF {

    /** Sets the data processing module for channel magnitude calculations. Some
     * methods calculate the channel magnitudes from input data observation
     * types on which they depend.<br>
     * Other methods use the channel magnitude to calculate the statistical summary
     * magnitude.
     */
    public void setMagMethod(MagnitudeMethodIF magMethod);
    public MagnitudeMethodIF getMagMethod();
    public String getMethodResultsString();

    /* Set true means do not scan the entire waveform to calculate the observations
     * (Amplitude, Coda etc) needed to determine the channel magnitude,
     * instead the scan is started at a time determined relative to the event origin
     * time and distance to the site.<br>
     * Default is true.
    public void setScanWaveformTimeWindow(boolean tf); // in MagnitudeMethodIF?
    */

    // below methods were protected in original Abstract class implementation
    public List createChannelMagDataFromWaveforms(Solution aSol, List wfList, boolean clearReadingList);
    public Magnitude getLastNewMag();
    public Magnitude getNewMag(Solution aSol);
    public Magnitude getNewMag(Solution aSol, List magDataList );
    // 

    public boolean solve(Magnitude mag);
    public boolean solve(Solution sol);
    public boolean solve(Solution sol, boolean useExistingData) ;
    public boolean solve(Solution sol, MagnitudeAssociatedListIF aList);
    //public boolean solveFromWaveforms(Solution sol, List wfList);

    /** Calculate the channel Magnitude data from the observations 
     * associated with the input Solution. If <i>useExistingData'</i> is set
     * <i>false </i> the associated Waveform timeseries are scanned to 
     * derive the observations appropriate for the set Magnitude method.<br>
     * Does not calculate a summary Magnitude.
     */
    public int calcChannelMag(Solution sol, boolean useExistingData);

    /** Calculate the statistical summary Magnitude from the observations 
     * associated with the input Solution. If <i>useExistingData'</i> is set
     * <i>false </i> the associated Waveform timeseries are scanned to 
     * derive the observations appropriate for the set Magnitude method.
     * and channel magnitudes are calculated from these observations to
     * determine a summary value. If set to <i>true</i> any existing data
     * with Channel magnitudes are used instead.
     */
    public Magnitude calcSummaryMag(Solution sol, boolean useExistingData) ;

    /** Equivalent to invoking <i>calcChannelMag(sol,false)</i>.
     */
    public int calcChannelMag(Solution sol);

    /** Calculate the channel magnitudes for the list of observations associated 
     * with the input Magnitude.<br>
     * Does not calculate a new value for input Magnitude, only updates its
     * associated observations.
     * @see #calcSummaryMag(Magnitude)
     * @throws WrongMagTypeException input is wrong type for set method.
     */
    public int calcChannelMag(Magnitude mag) throws WrongMagTypeException;

    /** Calculates the summary Magnitude from the list of observations
     * associated with the input Magnitude.
     * @throws WrongMagTypeException input not correct for MagnitudeMethodIF
     */
    public Magnitude calcSummaryMag(Magnitude mag) throws WrongMagTypeException;

    /** Sets channel magnitude residuals for ALL channels, including those
     * not contributing to the input Magnitude's summary value calculation.
     */
    public void setAllResiduals(Magnitude mag); // global

    /** Sets channel magnitude residuals for ONLY those channels 
     * that contributed to the input Magnitude's summary value calculation.
     */
    public void setResiduals(Magnitude mag); // global

    /** Delete or apply zero weight to channel observations from sites located
     * at distances greater than the distance cutoff value for input Magnitude.
     * The cutoff value is set by the current MagnitudeMethodIF instance.
     */
    public boolean trimByDistance(Magnitude mag);
    public boolean trimByDistance(Solution sol);

    /** Delete or apply zero weight to channel observations from sites 
     * whose channel magnitude residuals are greater than the maximum value 
     * set by the current MagnitudeMethodIF instance.
     */
    public boolean trimByResidual(Magnitude mag);

    /** Inquire the currently set MagnitudeMethodIF instance,
     * if input component can be used for a channel magnitude calculation.
     * Specific to method subtype.
     * @see MagnitudeMethodIF.acceptChannelType(Channel)
     */
    public boolean isChannelMagType(Channel chan);

    //public boolean isAcceptable(Channel chan);

    /** Inquire currently set MagnitudeMethodIF instance,
     * if input component can be used in a summary magnitude calculation.
     * Specific to method subtype.
     * @see MagnitudeMethodIF.summaryChannelType(Channel)
     */
    public boolean isSummaryMagType(Channel chan);
   

    public double getSummaryMagnitudeValue() ;
    public Magnitude getSummaryMagnitude() ;

    public void setMakePreferredMag(boolean tf) ;
    public boolean isMakePreferredMagEnabled() ;
    public void setAddMagToMagList(boolean tf) ;
    public boolean isAddMagToMagListEnabled() ;

    //public int calcChannelMagForEvid(long evid) ;
    //public int calcChannelMagByTime(double start, double end) ;
    //public int calcChannelMag(GenericSolutionList solList) ;
    //public int calcChannelMag(Solution [] solutions) ;
    //public Magnitude calcSummaryMagForEvid(long id) ;
    //public Magnitude calcSummaryMagForEvid(long id, boolean useExistingData) ;
    //public int calcSummaryMagByTime(double start, double end) ;
    //public int calcSummaryMag(GenericSolutionList solList) ;
    //public int calcSummaryMag(Solution [] solutions) ;
}
