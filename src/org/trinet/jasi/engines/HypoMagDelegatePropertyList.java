package org.trinet.jasi.engines;
import java.util.*;
import org.trinet.jasi.*;

import org.trinet.util.locationengines.LocationServerGroup;

public class HypoMagDelegatePropertyList extends SolutionWfEditorPropertyList {

    LocationServerGroup lsg = null;
    public static boolean unfixEqDepth = false;

    public HypoMagDelegatePropertyList() {}

    public HypoMagDelegatePropertyList(HypoMagDelegatePropertyList props) {
        super(props);
        privateSetup();
    }

    public HypoMagDelegatePropertyList(String propFileName) {
        super(propFileName);
        privateSetup();
    }

    public HypoMagDelegatePropertyList(String userPropFileName, String defaultPropFileName) {
        super(userPropFileName, defaultPropFileName);
        privateSetup();
    }

    private boolean privateSetup() { 
      boolean status = true;
      lsg = new LocationServerGroup(this);
      unfixEqDepth = getBoolean("unfixEqDepth", false);
      if (getBoolean("useCalcOriginDatum")) {
          Solution.calcOriginDatum = true;
      }
      return status;
    }

    public LocationServerGroup getLocationServerGroup() {
        return lsg;
    }

    public Object clone() {
        HypoMagDelegatePropertyList hmd = (HypoMagDelegatePropertyList) super.clone();
        if (this.lsg != null) hmd.lsg = (LocationServerGroup) this.lsg.clone();
        return hmd;
    }

    /**
     * Setup class static, instance, and/or environment variables from current properties.
     * This method should be called after the pertinent properties are initially read
     * or changed through updates.
     */
    public boolean setup() {
      boolean status = super.setup();
      return (privateSetup() && status);
    }

    /** Return a String of space-delimited property names used
     * by application classes utilizing this class.
     * */
    public List getKnownPropertyNames() {
        List aList = super.getKnownPropertyNames();
        List myList = classDeclaredPropertyNames();
        if (aList instanceof ArrayList)
            ((ArrayList)aList).ensureCapacity(aList.size() + myList.size());
        aList.addAll(myList);
        Collections.sort(aList);
        return aList;
    }

    public static List classDeclaredPropertyNames() {

        ArrayList myList = new ArrayList(64);

        myList.add("channelMap");
        myList.add("networkMap");
        myList.add("seedchanMap");
        myList.add("delegateLoadArrivalsFromDb");
        myList.add("delegateLoadMagReadingsFromDb");

        myList.add("LocationEngine"); // start with uppercase letter?
        myList.add("locationEngine"); // like magEngine?
        myList.add("locEngineProps");
        myList.add("locationEngineAddress");
        myList.add("locationEnginePort");
        myList.add("locationEngineType");
        myList.add("locationEngineTimeoutMillis");
        myList.add("locationServerGroupList");
        myList.add("locationServerSelected");

        myList.add("magDataLoadClearsSolution");
        myList.add("magDataLoadOption");
        myList.add("magDataLoadClonesReadings");
        myList.add("magEngine");
        myList.add("magEngineProps");
        // ADD NEW MAGTYPES HERE:
        // lc magType prefix properties:
        myList.add("mdDisable");
        myList.add("mhDisable");
        myList.add("mlDisable");
        myList.add("mdMagEngineProps");
        myList.add("mhMagEngineProps");
        myList.add("mlMagEngineProps");
        myList.add("mdMagMethod");
        myList.add("mhMagMethod");
        myList.add("mlMagMethod");
        myList.add("mdMagMethodProps");
        myList.add("mhMagMethodProps");
        myList.add("mlMagMethodProps");

        // props of DefaultHypoMagEngineDelegate for output control not used by Jiggle delegate
        myList.add("printLocEngPhaseList");
        myList.add("printMagEngReadingList");
        myList.add("printSolutionPhaseList");
        myList.add("printSolutions");
        myList.add("printMagnitudes");
        myList.add("printHypoMagHeaders");

        myList.add("unfixEqDepth");
        myList.add("useCalcOriginDatum");
        myList.add("useTrialLocation");
        myList.add("useTrialOriginTime");

        // props only in JiggleSolutionSolverDelegate declared in JiggleProperties
        //myList.add("autoRecalcM."); // upper case magType suffix like "ML"
        //myList.add("defaultTriggerEventType");

        Collections.sort(myList);

        return myList;
    }

    /** Return array of magnitude method types declared by property name
     * of the form "<magtype>MagMethod" that will be available to GUI. */
    public String [] getMagMethodTypes() {
        Enumeration e = propertyNames();
        int idx = -1;
        String prop = null;
        ArrayList aList = new ArrayList();
        while (e.hasMoreElements()) {
            prop = (String) e.nextElement();
            idx = prop.indexOf("MagMethod");
            if (idx > 0 && prop.length() == (idx+9)) {
              aList.add(prop.substring(0,idx));
            }
        }
        return (String []) aList.toArray(new String[aList.size()]);
    }

    /** Return true if the relevant LocationEngine properties: classname, type, address, and port are equivalent. */
    public boolean locationEngineEquals(HypoMagDelegatePropertyList otherProps) {
        return (
          ( getProperty("LocationEngine","").equals(otherProps.getProperty("LocationEngine","")) ||
            getProperty("locationEngine","").equals(otherProps.getProperty("locationEngine",""))
          ) &&
          getProperty("locationEngineType","").equals(otherProps.getProperty("locationEngineType","")) &&
          getProperty("locationEngineAddress","").equals(otherProps.getProperty("locationEngineAddress","")) &&
          getProperty("locationEnginePort","").equals(otherProps.getProperty("locationEnginePort","")) &&
          getProperty("unfixEqDepth","").equals(otherProps.getProperty("unfixEqDepth","")) &&
          getProperty("useCalcOriginDatum","").equals(otherProps.getProperty("useCalcOriginDatum",""))
        );
    }

    public void synchProperties() {
        super.synchProperties();
        if (lsg != null) lsg.updateProperties(this);
    }
}
