package org.trinet.jasi.engines;

import java.util.*;
import org.trinet.jasi.*;
//import org.trinet.util.gazetteer.*;

public abstract class AbstractSolutionAssocDataEngine extends AbstractChannelDataEngine implements SolutionDataEngineIF {

    /** The most currently processed solution */
    protected Solution currentSol;   // last processed

    /** Set true to use existing data rather then rescanning Solution associated waveform's time-series. */
    protected boolean useExistingData = false;

    protected AbstractSolutionAssocDataEngine() {
      super();
    }

    /** Set true reuses the existing data set in the associated elements of the lists
     * rather then rescaning the time-series. */
    public void setUseExistingData(boolean tf) {
      useExistingData = tf;
    }
    public boolean useExistingData() {
      return useExistingData;
    }

    /*
    public void calcDistances(List list, GeoidalLatLonZ latLonZ) {
      int count = list.size();
      if (count == 0) return;
      Channelable jr = null;
      for (int i = 0; i<count; i++) {
        jr = ((Channelable)list.get(i));
        /* Do we want to force a look up latlonz if there is none ?
        Channel chan = jr.getChannelObj();
        if ( ! chan.hasLatLonZ() ) {
          if (debug) System.out.println("DEBUG Engine calcDistances needs LatLonZ so channelLookup(chan,date)");
          chan = channelLookup(chan, getSolution().getDateTime());
          jr.setChannelObjData(chan); // copies channel data to reading
        }
        jr.calcDistance(latLonZ);
      }
    }
    */

    public void setDefaultProperties() {
        super.setDefaultProperties();
        useExistingData = false;
    }

    public void initializeEngine() {
        super.initializeEngine();
        if (props == null) props = new SolutionEditorPropertyList(); // empty list
        // true => don't re-scan waveforms to create new readings
        String propStr = props.getProperty("useExistingData");
        if (propStr != null) useExistingData = propStr.equalsIgnoreCase("true");
        else props.setProperty("useExistingData", useExistingData); 
    }


    public List getSolutionWaveformList(Solution aSol) {
        if (aSol == null) throw new NullPointerException("Current solution is null");
        List wfList = aSol.getWaveformList();
        if (wfList.size() < 1) {
            //wfList = (List) AbstractWaveform.createDefaultWaveform().getBySolution(aSol);
            // associate all waveforms with chosen origin.
            //aSol.addWaveforms(wfList);
            aSol.loadWaveformList(); // aww 6/04
        }
        return wfList;
    }

    public final Solution getSolution() {
        return currentSol;
    }
    public void setSolution(Solution aSol) {
        currentSol = aSol;
        // do reset after assigning currentSol
        // service check currentSol value to determine if connection needs resetting
        reset();

        statusString = "INFO: Engine reset() for solution : " + ((aSol == null) ? "NULL" : String.valueOf(aSol.getId().longValue()));
    }

    abstract public boolean solveFromWaveforms(Solution aSol, List wfList);
}
