package org.trinet.jasi.engines.TN;
import org.trinet.jasi.*;
import org.trinet.jasi.engines.*;
import org.trinet.jasi.magmethods.*;
import org.trinet.jasi.magmethods.TN.*;
import org.trinet.util.*;

public class MagnitudeEngineTN extends AbstractMagnitudeEngine {

    public static final String DEFAULT_MAG_ENG_PROP_FILE_NAME = "defaultMagEngineTN.props";

    /** Default no-op constructor.*/
    public MagnitudeEngineTN() {
      this(null);
    }

    /** Constructor sets the active MagnitudeMethod.*/
    public MagnitudeEngineTN(MagnitudeMethodIF magMethod) {
      super(magMethod);
      setEngineName("MagnitudeEngineTN");
    }

// For now, just implement interface methods of ConfigurationSourceIF
// as wrappers, we could instead implement the interface only in the
// EW subclass rather than super, thus we wouldn't need it here.
// Do we need the generic SourceConfigurationIF in TN code or not ? -aww  
    public void configure() {
       if (props == null) {
         props = new SolutionWfEditorPropertyList();
         props.setFiletype("jiggle"); // or "jasi" ?
         props.setFilename(DEFAULT_MAG_ENG_PROP_FILE_NAME);
         props.reset();
       }
       initializeEngine();
    }
    public void configure(int source, String location, String section) {
       if (source == ConfigurationSourceIF.CONFIG_SRC_FILE) setProperties(location);
    }
    public void configure(int source, String location, String section, ChannelList channelList) {
      configure(source,location,section);
      setChannelList(channelList);
    }
// end of ConfigurationSourceIF methods

} // end of MagnitudeEngineTN
