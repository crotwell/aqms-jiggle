package org.trinet.jasi.engines;

import java.util.*;
import org.trinet.jasi.*;

public class MagEnginePropertyList extends SolutionWfEditorPropertyList {

    public MagEnginePropertyList() {}

    public MagEnginePropertyList(MagEnginePropertyList props) {
        super(props);
        privateSetup(); 
    }
    public MagEnginePropertyList(String propFileName) {
        super(propFileName);
        privateSetup(); 
    }

    private boolean privateSetup() { 
      boolean status = true;
      return status;
    }

    /** Return a String of space-delimited property names used
     * by application classes utilizing this class.
     * */
    public List getKnownPropertyNames() {
        List aList = super.getKnownPropertyNames();
        List myList = classDeclaredPropertyNames();
        if (aList instanceof ArrayList)
            ((ArrayList)aList).ensureCapacity(aList.size() + myList.size());
        aList.addAll(myList);
        Collections.sort(aList);
        return aList;
    }

    public static List classDeclaredPropertyNames() {

        ArrayList myList = new ArrayList(12);

        myList.add("avgStaChannelMags");
        myList.add("addMagToMagList");
        myList.add("channelDbLookUp");
        myList.add("defaultChannelListComponents");
        myList.add("logMagResiduals");
        myList.add("magMethod");
        myList.add("magMethodProps");
        myList.add("makePreferredMag");
        myList.add("recalcChanMagsForSumMag");
        // Below is switch on how to handle data needed for generic solve(...) method
        myList.add("useExistingData");

        Collections.sort(myList);

        return myList;
    }

}
