package org.trinet.jasi.engines;
import java.util.*;
import org.trinet.jasi.*;
import org.trinet.util.*;
import org.trinet.pcs.PcsChannelListEngineDelegateIF;

public abstract class AbstractChannelDataEngineDelegate extends AbstractEngineDelegate
        implements ChannelDataEngineDelegateIF, PcsChannelListEngineDelegateIF {

    protected String channelCacheFileName;

    protected ChannelList chanList;

    protected String [] seedChanCodes;
    protected String [] chanCodes;
    protected String [] netCodes;

    protected AbstractChannelDataEngineDelegate() { 
        this(null);
    }

    protected AbstractChannelDataEngineDelegate(GenericPropertyList props) { 
        super(props);
    }

    public boolean setDelegateProperties(String propFileName) {
      myProps = new SolutionEditorPropertyList();
      myProps.setFiletype(EnvironmentInfo.getApplicationName().toLowerCase());
      myProps.setFilename(propFileName);
      myProps.reset(); // does the load
      if (myProps instanceof JasiPropertyList) ((JasiPropertyList)myProps).setup();
      return (myProps.isEmpty()) ? false : initializeEngineDelegate();
    }

    protected boolean loadProperties(String propFileName) {
        myProps =  new SolutionEditorPropertyList(); // empty list
        boolean status = myProps.readPropertiesFile(propFileName);
        if (status) status = ((SolutionEditorPropertyList) myProps).setup();
        myProps.setUserPropertiesFileName(propFileName);
        return status;
    }

    protected boolean initialize() {
        initIO();          // do first to enable flags
        initChannelList(); // must do before engines
        initEngines();
        return true;
    }

    abstract protected void initEngines();

    protected void setChannelFilterCodes(String [] netCodes, String [] seedChanCodes, String [] chanCodes) {
        this.netCodes = netCodes;
        this.seedChanCodes = seedChanCodes;
        this.chanCodes = chanCodes;
    }

    /** Initialized by array of String values for properties named <i>networkMap channelMap and seedchanMap</i>.*/
    protected void initChannelFilters() {
      netCodes = myProps.getStringArray("networkMap");
      seedChanCodes = myProps.getStringArray("seedchanMap");
      chanCodes = myProps.getStringArray("channelMap");
    }

    protected void initChannelList() {
        initChannelFilters(); // channel to screen out? 
        channelCacheFileName =
            myProps.getUserFileNameFromProperty("channelCacheFilename"); // added 10/16/03 aww
        if (verbose) System.out.println("ChannelDataEngineDelegate INFO: property channelCacheFileName: "+channelCacheFileName);
        if (channelCacheFileName != null) { // reset the static ChannelList value
            ChannelList.setCacheFilename(channelCacheFileName); // 10/16/03 aww
        }
        chanList = MasterChannelList.get(); // use same universal list to start with
    }

    public void loadChannelList(java.util.Date date) {
        initChannelList();
        ChannelList cl = null;
        //
        // To load only those Channels filtered by types from data source use:
        //  chanList = new ChannelList(Channel.create().loadAll(date,netCodes,seedChanCodes,chanCodes));
        //
        if (channelCacheFileName == null) { // get all active on date from DataSource
              if (verbose) System.out.println("INFO: ...EngineDelegate loading MasterChannelList from data source");
              cl = ChannelList.readList(date);
        }
        else { // do cache load (ignores date)
              if (verbose) System.out.println("INFO: ...EngineDelegate loading MasterChannelList from cache file");
              cl = ChannelList.readFromCache(channelCacheFileName);
        }
        setChannelList(cl);
        MasterChannelList.set(cl); // now the master list
    }

    public final void setChannelList(ChannelList chanList) {
        this.chanList = chanList;
        setEnginesChannelList(); // for those engines that have that attribute
    }

    public boolean hasChannelList() {
        return (this.chanList != null);
    }

    protected void setEngineChannelList(ChannelDataEngineIF engine) {
        engine.setChannelList(chanList);
    }

    private void setEnginesChannelList() {
        Collection values = engMap.values();
        Iterator iter = values.iterator();
        EngineIF engine = null;
        while (iter.hasNext()) {
          engine = (EngineIF) iter.next();
          if (engine instanceof ChannelDataEngineIF) {
             setEngineChannelList((ChannelDataEngineIF) engine);
          } 
        }
    }
}
