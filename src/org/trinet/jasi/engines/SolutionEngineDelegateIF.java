package org.trinet.jasi.engines;
import org.trinet.jasi.*;
import org.trinet.util.*;
public interface SolutionEngineDelegateIF extends EngineDelegateIF {
  public boolean solve(Solution sol);
}
