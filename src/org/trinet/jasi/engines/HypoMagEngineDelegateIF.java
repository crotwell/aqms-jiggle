package org.trinet.jasi.engines;
import org.trinet.jasi.Solution;
import org.trinet.jasi.Magnitude;
import org.trinet.jasi.magmethods.MagnitudeMethodIF ;
import org.trinet.util.locationengines.LocationEngineDelegateIF;

public interface HypoMagEngineDelegateIF extends
    ChannelDataEngineDelegateIF, LocationEngineDelegateIF, MagnitudeEngineDelegateIF {
  public boolean solve(Solution sol, Magnitude mag); // does both locateSol & calcMag
  public boolean calcMag(Magnitude mag, Solution sol);
  public MagnitudeEngineIF initMagEngineForType(String magType);
  public MagnitudeEngineIF initMagEngineForType(Solution sol, String magType);
  public MagnitudeMethodIF initMagMethod(String magType);
  public MagnitudeMethodIF initMagMethod(Solution sol, String magType);
}
