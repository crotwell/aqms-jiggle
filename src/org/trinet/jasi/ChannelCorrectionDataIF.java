package org.trinet.jasi;
import java.util.Collection;
import org.trinet.jdbc.table.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.util.DateRange;
public interface ChannelCorrectionDataIF extends ChannelDataIF, JasiCommitableIF, ChannelDataCorrectionIF {
    public Collection getByChannelId(ChannelIdIF id, DateRange dr);
}
