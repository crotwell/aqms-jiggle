package org.trinet.jasi;
import org.trinet.jdbc.datatypes.*; 
import org.trinet.jdbc.table.*; 
import org.trinet.util.*; 
//public abstract class CommitableChannelData extends AbstractChannelData implements Commitable {
// implemented methods of JasiCommitableIF here and replaced simple Commitable with JasiCommitableIF
public abstract class CommitableChannelData extends AbstractChannelData implements JasiCommitableIF {
  protected boolean needsCommit = false;
  protected boolean deleteFlag = false; 

  protected CommitableChannelData() {
    this(null, null);
  }

  protected CommitableChannelData(ChannelIdIF id) {
    this(id, null);
  }
  protected CommitableChannelData(ChannelIdIF id, DateRange dateRange) {
    super(id, dateRange);
  }
  /* Implement if objects attibutes are added to class.
  public Object clone() {
    return super.clone();
  }
  */

  abstract public JasiObject parseData(Object data);
  // could move Neat methods to super
  abstract public String toNeatString();
  abstract public String getNeatHeader();
  /**
  * Delete this reading. This is a "virtual delete". 
  * It is  not actually removed from memory or the source. 
  * Other classes must decide how they want to handle deleted readings when
  * commit() is called.
  * Override this method to add additional behavior.
  */
    public boolean delete() {
      setDeleteFlag(true);
      return true;
    }
    // aww until redesign to avoid unassociation list removal in subclass
    public boolean undelete() {
      setDeleteFlag(false);
      return true;
    }
    /** delete/undelete method implementation details overriden by dependent classes
     *  may need to override this method as well.
     */
    public boolean setDeleteFlag(boolean tf) {
      if (deleteFlag == tf) return false;
      deleteFlag = tf;
      //
      // For robust notification to work interested objects such as lists
      // would have to be added as a change listener for each element then
      // on change notification notify their respective list listeners.
      // Requires adding/removing the list as listener upon add/remove
      // of said element from the list.
      // Thus the below could be implemented to inform list:
      //fireStateChanged(new StateChange(this, "deleted", (isDeleted()) ? Boolean.TRUE:Boolean.FALSE));
      return true;
    }
    /** Returns true if this phase has been virtually deleted */
    public boolean isDeleted() {
      return deleteFlag;
    }


/** Commit any additions, changes or deletions to the data source. 
* The action taken depends on this instance creation state and data processing state.
* Member data may or may not have been initialized from the data source.
* As a result of commit, data be modified or unchanged in the DataSource.
*/
  abstract public boolean commit() throws JasiCommitException;

  //abstract public boolean hasChanged();
  public boolean getNeedsCommit() {
    return needsCommit;
  }

  public void setNeedsCommit(boolean tf) { needsCommit = tf;}

  public void setIdentifier(Object id) {
    copy((ChannelDataIF) id);
  }
  public Object getIdentifier() {
    return this;
  }
  /** Returns true if identifiers are not null, values are equivalent, 
   * If DataObjects are used as identifiers, returns false if the identifiers
   * are set to the DEFAULT undefined values.
   * This is to prevent objects with such identifiers without other 
   * disciminating attributes from being matched. Override this method
   * in subclasses if matching null or undefined values should be equivalent.
   * */
  public boolean idEquals(Object identifier) {
    Object id = getIdentifier();
    if (id == null) return false;
    if (id instanceof DataObject) {
      DataObject myId = (DataObject) id;
      return (! myId.isNull() && myId.equalsValue(identifier)); 
    }
    return id.equals(identifier);
  }
  public boolean idEquals(JasiCommitableIF jc) {
    return (getClass().isInstance(jc)) ? idEquals(jc.getIdentifier()) : false;
  }
  public JasiCommitableIF getById(Object id) {
    return (JasiCommitableIF) load(this);
  }
  abstract public Object getTypeQualifier();

  public boolean isSameType(Object type) {
    return ( getTypeQualifier().equals(type) ) ? true : false;
  }
  public boolean isSameType(JasiCommitableIF jc) {
    return (getClass().isInstance(jc)) ? isSameType(jc.getTypeQualifier()) : false;
  }
  /**
  * Have to have guarantee unique identifiers for equivalent to work.
  * True => subclass equivalent, isSameType(), and idEquals().
  */
  public boolean equivalent(Object obj) {
    if (this == obj) return true;
    if (! (obj instanceof JasiCommitableIF)) return false;
    JasiCommitableIF jc = (JasiCommitableIF) obj;
    //System.err.println("Debug: equivalent jasiobject sameType: " + isSameType(jc));
    //System.err.println("Debug: equivalent jasiobject idEquals: " + idEquals(jc));
    //System.err.println("Debug: equivalent jasiobject isNull: " + (jc == null));
    return (jc != null && isSameType(jc) && idEquals(jc)); //  ? true : false;
  }
}
