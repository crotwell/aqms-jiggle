package org.trinet.jasi;

import java.io.*;
import java.sql.*;
import java.util.*;
import org.trinet.jasi.seed.*;
import org.trinet.jdbc.*;
import org.trinet.jdbc.datasources.SQLDataSourceIF;
import org.trinet.jdbc.datatypes.*;
import org.trinet.util.*;

/**
 * Represents the waveform time series for one channel for a time window (TimeSpan).
 * Because a time window may contain gaps or no data, the TimeSeries may contain
 * from zero to 'n' WFSegments. Each WFSegment is a CONTIGUOUS time series.<p>
 *
 */
public class WaveletGroup extends AbstractWaveform {
    // maximum number of time gaps allowed before truncating timeseries
    public static int MAXCNT = 200;

    private ChannelableList waveletList = new ChannelableList();

    private Comparator sortComparator = new Comparator() {
        public int compare(Object o1, Object o2) {
            if (o1 == o2) return 0;
            double t1 = ((Wavelet) o1).getEpochStart();
            double t2 = ((Wavelet) o2).getEpochStart();
            if (t1 == t2) return 0;
            return ( t1 < t2) ? -1 : 1;
        }

        public boolean equals(Object o1) {
            return (o1 == this);
        }
    };

    private Wavelet defaultWv = null; 

    protected WaveletGroup() { }

// ////////////////////////////////////////////////////////////////////////////
    /**
     * Instantiate an object of this type. You do
     * NOT use a constructor. This "factory" method creates various
     * concrete implementations. Creates a Solution of the DEFAULT type.
     * @See: JasiObject
     */
    public static final WaveletGroup create() {
        return new WaveletGroup();
    }

    public Waveform createWaveform() {
        return WaveletGroup.create();
    }

    /** Do "deep" copy of the header, not the data, portion of the Waveform. */
    public void copyHeader(Waveform wf) {
        super.copyHeader(wf);
        if (wf instanceof WaveletGroup) {
          WaveletGroup wv = (WaveletGroup) wf;
          waveletList = new ChannelableList(wv.waveletList);
          sortWaveletSegments();
        }
    }

    private void sortWaveletSegments() {
        if (waveletList != null) waveletList.sort(sortComparator);
    }

    private void setTimeSpanFromWavelets() {
        if (waveletList != null && ! waveletList.isEmpty()) {
            waveletList.sort(sortComparator);
            // set values presumed not varying between segements except for times
            Wavelet w = (Wavelet) waveletList.get(0);
            copyHeader(w);
            // set ending time to that of last wavelet
            w = (Wavelet) waveletList.get( waveletList.size() - 1 );
            setEnd(w.getEnd());
        }
    }

/**
 * Get an Waveform from the DataSource associated with input Solution and instance Channel object.
 * Returns a Collection with a single WaveletGroup element.
 */
    public Collection getByChannel(Channelable ch, long evid) {
        Collection wfList = Wavelet.create().getByChannel(ch, evid);
        if (wfList == null) return null;
        WaveletGroup wg = WaveletGroup.create();
        Iterator it = wfList.iterator();
        while (it.hasNext()) {
            wg.waveletList.add( (Wavelet) it.next() );
        }
        wg.setTimeSpanFromWavelets();
        ArrayList aList = new ArrayList();
        aList.add(wg);
        return aList;
    }

    //public boolean makeRequest(long evid, String auth, String staAuth, String archiveSrc, double start, double end) {
    //    if (defaultWv == null) defaultWv = Wavelet.create();
    //    return defaultWv.makeRequest(evid, auth, staAuth, archiveSrc, start, end);
    //}

/**
 * Get a Collection of Waveforms from the DataSource associated with input Solution id.
 */
    public Collection getBySolution(long evid) {
        Collection wvList = Wavelet.create().getBySolution(evid);
        if (wvList == null) return null; // no data

        Iterator iter = wvList.iterator();
        Wavelet wv = null;
        Channel oldChan = null;
        Channel newChan = null;
        WaveletGroup wg = null;
        ArrayList wgList = new ArrayList();
        //System.out.println("DEBUG WaveletGroup getBy Solution wavelets total: "+ wvList.size());
        while (iter.hasNext()) {
            wv = (Wavelet) iter.next();
            newChan = wv.getChannelObj();
            if ( ! newChan.equalsName(oldChan) ) { 
              if (wg != null) wg.setTimeSpanFromWavelets(); // finalize header for prior channel
              wg = WaveletGroup.create();
              wgList.add(wg);
              oldChan = newChan;
            }
            wg.waveletList.add(wv);
        }
        // last wavelet loop exit so now : 
        if (wg != null) wg.setTimeSpanFromWavelets(); // finalize header for last channel processed in loop
        //System.out.println("DEBUG WaveletGroup getBy Solution wgList total: "+ wgList.size());
        return wgList;
    }

/**
 * Get an count of Channels with Waveforms associated with input event id in the DataSource 
 */
    public int getCountBySolution(long evid) {
        if (defaultWv == null) defaultWv = Wavelet.create();
        return defaultWv.getCountBySolution(evid);
    }

    /**
     * Return the waveform file root.
     * Access to this path is tested to determine if files can be read locally.
     */
    public String getFileRoot() {
        Wavelet wv = null;
        Iterator it = waveletList.iterator();
        if (it.hasNext()) wv = (Wavelet) it.next();
        return (wv == null) ? null : wv.getFileRoot();

        /*
        if (defaultWv == null) defaultWv = Wavelet.create();
        return defaultWv.getFileRoot();
        */
    }

    /** Waveroots table wcopy number for 1st loaded wavelet (assumes all wavelets match). */
    public int getWcopy() {
        Wavelet wv = null;
        Iterator it = waveletList.iterator();
        if (it.hasNext()) wv = (Wavelet) it.next();
        return (wv == null) ? 0 : wv.getWcopy();
    }

    /**
     * Return the waveform file root for a particular event id.
     * Access to this path is tested
     * to determine if files can be read locally.
     */
    public String getFileRoot(long eventId) {
        if (defaultWv == null) defaultWv = Wavelet.create();
        return defaultWv.getFileRoot(eventId);
    }
     

    /**
     * Override AbstractWaveform.filesAreLocal() to always return false so dbase access of waveforms
     * will be used.
     */
    public boolean filesAreLocal() {
        return false;
    }

/**
 * Load the time series data for this Waveform. This is done as a separate step
 * because it is often more efficient to only load selected waveforms after
 * you have examined or sorted the Waveform objects by their header info.<p>
 *
 * The waveform is collapsed into the minimum number of WFSegments possible. More
 * then one WFSegment would only be necessary if there are time tears. The start-stop
 * times of the original data packets is preserved in a packet TimeSpan List.<p>
 *
 * Fires ChangeEvents when the time-series is loaded so that time-series can be
 * loaded asynchronously (in background thread) then notify a graphics object when
 * loading is complete. <p>
 *
 * This method must synchronize on the waveform object for thread safty. Simply
 * synchronizing the method is not enough.
 */

    public boolean loadTimeSeries(double startTime, double endTime) {
     // Try to prevent two threads trying to load from the server at the same time - aww
      synchronized (this) {  // 2005/08/12 -aww
        if (hasTimeSeries() && (getStart().doubleValue() == startTime) &&
           (getEnd().doubleValue() == endTime) ) return true;

        if (format.intValue() != SEED_FORMAT) return false;

        if (waveletList.size() <= 0) // From a local disk file or WaveServer source
            SeedReader.getData(this, startTime, endTime);
        else // From Database source
            loadTimeSeriesByWaveletList(startTime, endTime); 

        if (! hasTimeSeries()) return false;
        //System.out.println("WAVELET GROUP loadTimeSeries segList.size: " + segList.size());

        // set max, min, bias values
        scanForAmps();
        scanForBias();
        // Added logic below to finesse database Waveform row having samplerate undefined -aww 12/14/2006
        double rate = getSampleRate();
        if (rate <= 0. && segList.size() > 0) {
            rate = 1./((WFSegment)segList.get(0)).getSampleInterval();
        }
        setSampleRate(rate);

        // Notify any listeners that Waveform state has changed
        // (changeListener logic is in jasiObject())
        fireStateChanged();
        return true;
      }
    }

    private void loadTimeSeriesByWaveletList(double startTime, double endTime) {
        int cnt = waveletList.size();
        if (cnt == 0) return; // don't have any to load
        if (cnt > 1) sortWaveletSegments(); // force sort by ondate, should already be sorted 
        // clear out existing list data
        segList.clear();
        packetSpans.clear();

        Wavelet wv = null;
        // Loop over all wavelets adding time segments in each to group list
        boolean truncated = false;
        if (cnt > MAXCNT) {
            cnt = MAXCNT;
            System.err.println("Error: WaveletGroup.loadTimeSeriesByWaveletList, wavelet count exceeds "+MAXCNT);
            truncated = true;
        }

        for (int ii = 0; ii < cnt; ii++) {
          wv = (Wavelet) waveletList.get(ii);
          wv.loadTimeSeries(startTime, endTime);
          // add to WaveletGroup lists
          segList.addAll(wv.getSegmentList());
          packetSpans.addAll(wv.getPacketSpanList());

          wv.unloadTimeSeries(); // purge duplicate time series (garbage collect memory) 
        }
        if (truncated) System.err.println(".... truncating timeseries at: " + wv.toString());
        //System.out.println("WAVELET GROUP loadTimeSeriesByWaveletList segList.size: " + segList.size());

        // WaveletTN.loadTimeSeries invokes SeedReader.getData(...) which invokes collapse()
        // Assume data already collapsed for single wavelet case as invoked by SeedReader
        if ( cnt > 1 && ! segList.isEmpty() ) {
          // Segments are presumed to be in chronological ondate order
          // collapse multiple segments into minimum possible but don't alter packetSpans
          // from original wavelets
          setSegmentList(WFSegment.collapse(segList));
        }

        //System.out.println("WAVELET GROUP loadTimeSeriesByWaveletList ENDING segList.size: " + segList.size());
        //if (segList.size() > 0) System.out.println("WAVELET GROUP seg(0) ts.length:"  + ((WFSegment) segList.get(0)).size());
    }

    /**
     * Delete the time series for this waveform and allow garbage collection to reclaim
     * the memory space. This is used to manage memory and prevent overflows.  */
    public boolean unloadTimeSeries() {
        //System.out.println("WAVELET GROUP unloadTimeSeries: " + segList.size() + " " + getChannelObj().toDelimitedSeedNameString());
        boolean status = super.unloadTimeSeries();
        synchronized (this) {
            unloadWaveletTimeSeries(); // they should be empty, but just in case 
        }
        return status;
    }

    private void unloadWaveletTimeSeries() {
        if (waveletList == null || waveletList.isEmpty()) return;
        Iterator it = waveletList.iterator();
        while (it.hasNext()) {
            ((Wavelet)it.next()).unloadTimeSeries();
        }
    }

    public String toWaveletString() {
        if (waveletList == null || waveletList.isEmpty()) return "No wavelets in list";
        StringBuffer sb =  new StringBuffer(1024*waveletList.size());
        Iterator it = waveletList.iterator();
        while (it.hasNext()) {
            sb.append(((Wavelet)it.next()).toString()).append("\n");
        }
        return sb.toString();
    }

    public String toWaveletBlockString() {
        if (waveletList == null || waveletList.isEmpty()) return "No wavelets in list";
        StringBuffer sb =  new StringBuffer(1024*waveletList.size());
        Iterator it = waveletList.iterator();
        while (it.hasNext()) {
            sb.append(((Wavelet)it.next()).toBlockString()).append("\n");
        }
        return sb.toString();
    }

} // end of class
