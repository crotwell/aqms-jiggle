package org.trinet.jasi;

/**
 * Map TriNet schema event types to jasi event type labels and visa versa.<p>
 * You must extend this class with a concrete instance to define local eventtype
 * descriptive strings.
 */

public abstract class EventTypeMap extends JasiObject {

    protected static int idx = 0;
    public static int LOCAL =        idx++;
    public static int REGIONAL =     idx++;
    public static int TELESEISM =    idx++;
    public static int QUARRY =       idx++;
    public static int EXPLOSION =    idx++; // added 11/30/2007
    public static int SONIC =        idx++;
    public static int LONGPERIOD =   idx++; // added 11/30/2007
    public static int TREMOR =       idx++; // added  2008/09/10
    public static int TRIGGER =      idx++; // added 9/25/2000
    public static int NUCLEAR =      idx++;
    public static int OTHER =        idx++; // added 2008/09/10
    public static int UNKNOWN =      idx++;

    /** Define jasi event types. These are generic longer names for
        event types. */
    protected static String jasiType[] = {
        "local",
        "regional",
        "teleseism",
        "quarry",
        "explosion", 
        "sonic",
        "longperiod",
        "tremor",
        "trigger",
        "nuclear",
        "other",
        "unknown"
    };

    protected static EventType [] eventTypes = null;

    protected static String jasiDefault = "unknown";

    /** Define Local schema event type strings. */
    protected static String localType[];

    /** Define the Local default even type (usually "unknown")*/
    protected static String localDefault;

    public static final EventTypeMap MAP = EventTypeMap.create();

    protected EventTypeMap() { }

// -- Concrete FACTORY METHODS ---
    /**
     * Factory Method: This is how you instantiate an object. You do
     * NOT use a constructor. This is so that this "factory" method can create
     * the type of object that is appropriate for your site's database.    */

// ////////////////////////////////////////////////////////////////////////////
    /**
     * Instantiate an object of this type. You do
     * NOT use a constructor. This "factory" method creates various
     * concrete implementations. Creates a Solution of the DEFAULT type.
     * @See: JasiObject
     */
    public static final EventTypeMap create() {
        return create(DEFAULT);
    }

    /**
     * Instantiate an object of this type. You do
     * NOT use a constructor. This "factory" method creates various
     * concrete implementations. The argument is an integer implementation type.
     * @See: JasiObject
     */
    public static final EventTypeMap create(int schemaType) {
        return create(JasiFactory.suffix[schemaType]);
    }

    /**
     * Instantiate an object of this type. You do
     * NOT use a constructor. This "factory" method creates various
     * concrete implementations. The argument is as 2-char implementation suffix.
     * @See: JasiObject
     */
    public static final EventTypeMap create(String suffix) {
        return (EventTypeMap) JasiObject.newInstance("org.trinet.jasi.EventTypeMap", suffix);
    }
// ////////////////////////////////////////////////////////////////////////////
    /** Return the default event type. */
    public static String getDefaultEventType() {
        return jasiDefault;
    }

    /** Return the string of this event type. If not a valid value, returns 'unknown'*/
    public static String get(int val) {
        if (isValid(val)) return jasiType[val];
        return jasiType[UNKNOWN];
    }

    /** Returns true if the string is a valid event type. This is case insensitive.
    */
    public static boolean isValid(String str) {
        for (int i = 0; i < jasiType.length; i++) {
            if (str.equalsIgnoreCase(jasiType[i])) return true;
        }
        return false;
    }

    /** Returns int code of a valid site-specific event type. Returns -1 if not valid.
    * Local = 0 (LOCAL) , quarry = 1 (QUARRY) , etc.
    */
    public static int getIntOfLocalCode(String str) {
        int count = (localType == null) ? 0 : localType.length;
        for (int i = 0; i < count; i++) {
            if (str.equalsIgnoreCase(localType[i])) return i;
        }
        return -1;
    }
    /** Returns int code of a valid Jasi event type. Returns -1 if not valid.
    * Local = 0 (LOCAL) , quarry = 1 (QUARRY) , etc.
    */
    public static int getIntOfJasiCode(String str) {
        for (int i = 0; i < jasiType.length; i++) {
            if (str.equalsIgnoreCase(jasiType[i])) return i;
        }
        return -1;
    }

    /** Returns true if the int is a valid event type. This is case insensitive.
    */
    public static boolean isValid(int val) {
        if (val > -1 && val < jasiType.length) return true;
        return false;
    }

    /** Given a jasi event type return the corresponding TriNet schema type. */
    public static String toLocalCode(String str) {
        int count = (localType == null) ? 0 : jasiType.length;
        for (int i = 0; i < count; i++) {
            if (str.equalsIgnoreCase(jasiType[i])) return localType[i];
        }
        return localDefault;
    }

    /** Given a TriNet schema type return the corresponding jasi event type. */
    public static String fromLocalCode(String str) {
        int count = (localType == null) ? 0 : localType.length;
        for (int i = 0; i < count; i++) {
            if (str.equalsIgnoreCase(localType[i])) return jasiType[i];
        }
        return jasiDefault;
    }

    /** Given a TriNet schema type return the corrisponding jasi event type. */
    public static String toJasiCode(String str) {
        return fromLocalCode(str);
    }

    /** Return array containing the list of jasi event types */
    public static String[] getJasiTypeArray() {
        return jasiType;
    }
    /** Return array containing the list of local event types */
    public static String[] getEventLocalTypeArray() {
        return localType;
    }

    public static EventType getEventTypeByCode(String code) {
        for (int ii = 0; ii < eventTypes.length; ii++) {
            if (eventTypes[ii].code.equals(code)) return eventTypes[ii];
        }
        return null;
    }

    public static EventType getEventTypeByName(String name) {
        for (int ii = 0; ii < eventTypes.length; ii++) {
            if (eventTypes[ii].name.equals(name)) return eventTypes[ii];
        }
        return null;
    }

    public static EventType [] getEventTypes() {
        return eventTypes;
    }

    public abstract void loadEventTypes();

    public String toString() {
        StringBuffer sb = new StringBuffer(1024);
        //for (int idx = 0; idx < localType.length; idx++) {
        for (int idx = 0; idx < eventTypes.length; idx++) {
            if (eventTypes != null) sb.append(eventTypes[idx].toString()+"\n");
            else sb.append(localType[idx]).append(" ").append(jasiType[idx]).append("\n");
        }
        return sb.toString();
    }

} // EventTypeMap
