package org.trinet.jasi;

import java.io.*;
import java.sql.*;
import java.util.*;
import org.trinet.jdbc.*;
import org.trinet.jdbc.datasources.SQLDataSourceIF;
import org.trinet.jdbc.datatypes.*;
import org.trinet.util.*;

/**
 * Represents the waveform time series for one Waveform table row entry in the database.
 */

public abstract class Wavelet extends AbstractWaveform  // implements JasiCommitableIF
{


    public static int wcopy = 1;

    protected int dbWcopy = Wavelet.wcopy;   // value of Wavelet.wcopy when query invoked

    /** Optional unique ID number of a waveform. May not exist in all systems.
     * For NCDN schema data this would be the 'wfid'. */
    protected DataLong wfid = new DataLong();

    /** Optional unique ID external number of a waveform. May not exist in all
        systems. */
    protected DataString externalId = new DataString();

    protected Wavelet() {}

// ////////////////////////////////////////////////////////////////////////////
    /**
     * Instantiate an object of this type. You do
     * NOT use a constructor. This "factory" method creates various
     * concrete implementations. Creates a Solution of the DEFAULT type.
     * @See: JasiObject
     */
    public static final Wavelet create() {
        return create(DEFAULT);
    }

    /**
     * Instantiate an object of this type. You do
     * NOT use a constructor. This "factory" method creates various
     * concrete implementations. The argument is an integer implementation type.
     * @See: JasiObject
     */
    public static final Wavelet create(int schemaType) {
        return create(JasiFactory.suffix[schemaType]);
    }

    /**
     * Instantiate an object of this type. You do
     * NOT use a constructor. This "factory" method creates various
     * concrete implementations. The argument is as 2-char implementation suffix.
     * @See: JasiObject
     */
    public static final Wavelet create(String suffix) {
        return (Wavelet) JasiObject.newInstance("org.trinet.jasi.Wavelet", suffix);
    }
// ////////////////////////////////////////////////////////////////////////////

    public Waveform createWaveform() {
        return Wavelet.create();
    }

    //abstract public boolean makeRequest(long evid, String archiveSrc, double start, double end);

    /** Do "deep" copy of the header, not the data, portion of the Waveform. */
    public void copyHeader(Waveform wf) {

        super.copyHeader(wf);

        if (wf instanceof Wavelet) {
          Wavelet wv = (Wavelet) wf; 
          synchronized (wf) { // make sure another thread doesn't change it under us
            wfid = new DataLong(wv.wfid);
            externalId = new DataString(wv.externalId);
            dbWcopy = wv.dbWcopy;  // value of Wavelet.wcopy when SQL query last invoked
          } // end of synch
        }

    } 

    /**
     * Get one waveform from the DataSource by Waveform record ID #
     */
    public abstract Wavelet getByWaveformId(long wfid);

    public long getId() {
        return (wfid.isNull()) ? 0l : wfid.longValue();
    }

    public int getWcopy() {
        return dbWcopy;
    }

    /**
     * Return string describing the Waveform object, primarily for debugging.<p>
     */
    public String toString() {
      StringBuffer sb = new StringBuffer(1024);
      sb.append(super.toString());
      sb.append(", wfid = ").append(wfid);
      sb.append(", wcopy = ").append(dbWcopy);
      return sb.toString();
    }

    /**
     * Return string describing the Waveform object, primarily for debugging.<p>
     */
    public String toBlockString() {
        StringBuffer sb = new StringBuffer(1024);
        sb.append(super.toBlockString());
        sb.append("\nwfid = ").append((wfid.isNull()) ? "null" : wfid.toString());
        sb.append("\nwcopy = ").append(dbWcopy);
        return sb.toString();
    }

} // end of Wavelet class
