package org.trinet.jasi;
import java.util.ArrayList;

import org.trinet.util.*;

/**
 * @Deprecated TimeWindowModel based on an explicit ChannelList a start time and a window length. </p>
 * Deprecated since it's now the same model as NamedChannelTimeWindowModel class.
 * @see NamedChannelTimeWindowModel
 */
public class ListBasedChannelTimeWindowModel extends ChannelTimeWindowModel {

    /** Model name string. */
    public static final String defModelName = "List";
    /** A string with an brief explanation of the model.*/
    public static final String defExplanation = "channels associated with a named list (deprecated)";

    protected static final double defMaxWindowSize = 120.;

    {
      setModelName(defModelName);
      setExplanation(defExplanation);
    }
  
    public ListBasedChannelTimeWindowModel() {
        setMyDefaultProperties();
    }

    public ListBasedChannelTimeWindowModel(GenericPropertyList gpl, Solution sol, ChannelableList candidateList) {
        super(gpl, sol, candidateList);
        if (sol != null) setStartTime(sol.getTime()-getPreEventSize());
    }

    public ListBasedChannelTimeWindowModel(Solution sol, ChannelableList candidateList) {
        this(null, sol, candidateList);
    }

    public ListBasedChannelTimeWindowModel(ChannelableList candidateList) {
        this(null, null, candidateList);
    }

    /** Set the Solution to use in the model. May do special setup specific to
     * time or location of event here. */
    public void setSolution(Solution sol) {
        super.setSolution(sol);
        if (sol != null) setStartTime(sol.getTime()-getPreEventSize());
    }

    public TimeSpan getTimeWindow(double dist) {
        return new TimeSpan(starttime, starttime+getMaxWindowSize());
    }

    public void setDefaultProperties() {
      super.setDefaultProperties();
      setMyDefaultProperties();
    }

    private void setMyDefaultProperties() {
      maxWindow = defMaxWindowSize;
      starttime = defStartTime;
    }
}
