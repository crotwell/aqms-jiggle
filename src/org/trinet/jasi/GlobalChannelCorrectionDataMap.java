package org.trinet.jasi;
import java.io.*;
import java.util.*;
import org.trinet.util.*;
import org.trinet.jdbc.table.*;

public class GlobalChannelCorrectionDataMap extends Object implements Cloneable, Serializable {
/**
* Store in a hashmap keyed by the ChannelIdIF a set of valid dates ranges for the ChannelIdIF
* If an input Date lies within a DateRange (ondate offdate) of the set, cached data object is returned.
* Otherwise, object if a new ChannelData object can be created from the database it is added to the cache
* and returned. Returns null if the data is not recoverable.
* The cached ChannelData object's DateRange and ChannelId are used to compose the key that is mapped to the
* ChannelData object in the hashmap.
*/
    protected static final int DEFAULT_CAPACITY = 1009;

    protected HashMap chanParmMap;
    protected HashMap chanDateMap;

    public GlobalChannelCorrectionDataMap() { this(DEFAULT_CAPACITY); }

    public GlobalChannelCorrectionDataMap(int capacity) {
      chanParmMap = new HashMap(capacity);
      chanDateMap = new HashMap(capacity);
    }
    public GlobalChannelCorrectionDataMap(Collection data) {
      int count = data.size();
      chanParmMap = new HashMap(count);
      chanDateMap = new HashMap(count);
      ChannelCorrectionDataIF [] input = (ChannelCorrectionDataIF []) data.toArray(new ChannelCorrectionDataIF [count]);
      addAll(input);
    }
    public GlobalChannelCorrectionDataMap(ChannelDataCorrectionMap data) {
      putAll(data);
    }

/**
* Writes the list data to the default data archive.
*/
    public boolean commit() {
      if (chanParmMap == null) return false;
      Iterator iter = chanParmMap.values().iterator();
      boolean status = true;
      try {
        while (iter.hasNext()) {
            status = ((ChannelCorrectionDataIF) iter.next()).commit();
            if (! status) break;
        }
      }
      catch (JasiCommitException ex) {
        ex.printStackTrace();
        status = false;
      }
      return status;
    }

/**
*  Initializes the list with new elements of default type using data from data source.  
*  valid for current date.
*/
    public boolean load(ChannelCorrectionDataIF data) {
        return load(data, (java.util.Date) null);
    }

/**
*  Initializes the list with new elements of default type using data from data source
*  for specified date.
*/
    public boolean load(ChannelCorrectionDataIF data, java.util.Date date) {
        Collection newCollection = data.loadAll(date);  // retrieve all data from data source
        if (newCollection == null) return false;
        int count = newCollection.size();
        if (count < 1) return false;
        chanParmMap = new HashMap(count);
        chanDateMap = new HashMap(count);
        addAll(newCollection);
        return true;
    }

    public void printValues() {
        System.out.println(getValuesString());
    }

    public String getValuesString() {
        if (chanParmMap == null || chanParmMap.isEmpty()) {
            return "null or empty";
        }
        Collection list = chanParmMap.values();
        Iterator iter = list.iterator();
        StringBuffer sb = new StringBuffer(list.size() * 132);
        while (iter.hasNext()) {
            sb.append(iter.next().toString());
            sb.append("\n");
        }
        return sb.toString();
    }

    public boolean isEmpty() {
        if (chanParmMap.isEmpty()) {
           clear();
           return true;
        }
        else return false;
    }

    public int size() {
      return chanParmMap.size();
    }
    public Collection values() {
      return chanParmMap.values();
    }
    public void clear() {
      chanParmMap.clear();
      chanDateMap.clear();
    }

    public boolean contains(Channelable chan, java.util.Date date, String type) {
      return (chan == null) ? false : contains(chan.getChannelObj().getChannelId(), date, type);
    }
    public boolean contains(ChannelIdIF id, java.util.Date date, String type) {
        if (id == null || date == null) return false;
        Set aDateRangeSet = (Set) chanDateMap.get( makeDateMapHashKey(id, type) );
        if (aDateRangeSet == null) return false;
        Iterator iter = aDateRangeSet.iterator();
        boolean retVal = false;
        while (iter.hasNext()) {
          DateRange dr = ((DateRange) iter.next());
          if (dr.contains(date))  {
            retVal = chanParmMap.containsKey( makeParmMapHashKey(id, dr, type) );
            break;
          }
        }
        return retVal;
    }

    public boolean contains(ChannelCorrectionDataIF cd) {
      return (cd == null) ? false : chanParmMap.containsKey( makeParmMapHashKey(cd) ); 
    }

    public Object remove(ChannelCorrectionDataIF cd) {
      if (cd == null) return null;
      DateRange dateRange = cd.getDateRange();
      Set aDateRangeSet = (Set) chanDateMap.get( makeDateMapHashKey(cd) );
      if (aDateRangeSet != null) {
        Iterator iter = aDateRangeSet.iterator();
        while (iter.hasNext()) {
          if ( ((DateRange) iter.next()).equals(dateRange) ) {
            iter.remove();
            break;
          }
        }
      }
      return chanParmMap.remove( makeParmMapHashKey(cd) );
    }

    public boolean addAll(Collection data) {
      return (data == null) ? false : addAll((ChannelCorrectionDataIF [])data.toArray(new ChannelCorrectionDataIF[data.size()]));
    }

    public boolean addAll(ChannelCorrectionDataIF [] data) {
        if (data == null) return false;
        ChannelCorrectionDataIF newValue = null;
        boolean retVal = false;
        for (int idx = 0; idx < data.length; idx++) {
          newValue = data[idx];
          if (add(newValue) != newValue) retVal = true;
        }
        return retVal;
    }

    public Object add(ChannelCorrectionDataIF cd) {
      if (cd == null) return null;
      DateRange dr = cd.getDateRange();
      Object dateKey = makeDateMapHashKey(cd);
      Set aDateRangeSet = (Set) chanDateMap.get(dateKey);
      if (aDateRangeSet != null) {
        aDateRangeSet.add(dr);
      }
      else {
        aDateRangeSet = new HashSet(7);
        aDateRangeSet.add(dr);
        chanDateMap.put(dateKey, aDateRangeSet);
      }
      return chanParmMap.put( makeParmMapHashKey(cd), cd );
    }

    public void putAll(ChannelDataMap cdl) {
       if (cdl == null) return;
       if (chanDateMap == null) chanDateMap = (HashMap) cdl.chanDateMap.clone();
       else chanParmMap.putAll(cdl.chanDateMap);
       if (chanParmMap == null) chanParmMap = (HashMap) cdl.chanParmMap.clone();
       else chanParmMap.putAll(cdl.chanParmMap);
    }

    public ChannelCorrectionDataIF lookUp(ChannelCorrectionDataIF cd, java.util.Date date) {
        if (cd == null) return null;
        ChannelCorrectionDataIF data = get(cd, date);
        return (data != null) ?  data : (ChannelCorrectionDataIF) cd.load(cd, date) ;
    }

    public ChannelCorrectionDataIF get(Channelable chan, java.util.Date date, String type) {
        return (chan == null) ? null : get(chan.getChannelObj().getChannelId(), date, type);
    }
    public ChannelCorrectionDataIF get(ChannelDataIF cd, java.util.Date date, String type) {
        // Note input may be a different ChannelData "type" than stored in map 
        // but have like ChannelId
        return (cd == null) ? null : get(cd.getChannelId(), date, type);
    }
    public ChannelCorrectionDataIF get(ChannelCorrectionDataIF cd, java.util.Date date) {
        // Note input may be a different ChannelData "type" than stored in map 
        // but have like ChannelId
        return (cd == null) ? null : get(cd.getChannelId(), date, cd.getCorrType());
    }
    public ChannelCorrectionDataIF get(ChannelIdIF id, java.util.Date date, String type) {
        if (id == null) return null;
        Set aDateRangeSet = (Set) chanDateMap.get( makeDateMapHashKey(id, type) );
        if(aDateRangeSet == null) return null;
        Object retVal = null;
        Iterator iter = aDateRangeSet.iterator();
        while (iter.hasNext()) {
          DateRange dateRange = ((DateRange) iter.next());
          if (dateRange.contains(date)) {
            // Update dateRange to go with input channelId for key object
            // use defaultFactory type for lookup:
            retVal =
              chanParmMap.get( makeParmMapHashKey(id, dateRange, type) );
            break;
          }
        }
        return (ChannelCorrectionDataIF) retVal; // or return input cd if null //??
    }

    public boolean equals(Object object) {
        if (object == this) return true;
        if (object == null || object.getClass() != this.getClass()) return false;
        GlobalChannelCorrectionDataMap list = (GlobalChannelCorrectionDataMap) object;
        return ( chanDateMap.equals(list.chanDateMap) && chanParmMap.equals(list.chanParmMap) );
    }

    public Object clone() {
        GlobalChannelCorrectionDataMap cdl = null;
        try {
            cdl = (GlobalChannelCorrectionDataMap) super.clone();
            cdl.chanDateMap = (HashMap) this.chanDateMap.clone();
            cdl.chanParmMap = (HashMap) this.chanParmMap.clone();
        }
        catch(CloneNotSupportedException ex) {
             ex.printStackTrace();
        }
        return cdl;
    }

    public int hashCode() {
        return (chanDateMap.hashCode()+chanParmMap.hashCode()) ;
    }

    public String toString() {
      return chanParmMap.toString();
    }
    public String toValuesString() {
        Collection vals = chanParmMap.values();
        Iterator iter = vals.iterator();
        StringBuffer sb = new StringBuffer(vals.size()*132);
        while (iter.hasNext()) {
            sb.append(iter.next().toString());
            if (iter.hasNext()) sb.append(", ");
        }
        return sb.toString();
    }

    protected Object makeDateMapHashKey(ChannelIdIF id, String type) {
      return new DateKey(id, type);
    }
    protected Object makeDateMapHashKey(ChannelCorrectionDataIF cd) {
      return new DateKey(cd.getChannelId(), cd.getCorrType());
    }
    protected Object makeParmMapHashKey(ChannelIdIF id, DateRange dr, String type) {
      return new ParmKey(id, dr, type);
    }
    protected Object makeParmMapHashKey(ChannelCorrectionDataIF cd) {
      return new ParmKey(cd.getChannelId(), cd.getDateRange(), cd.getCorrType());
    }

    static private class DateKey implements Serializable {
      ChannelIdIF id;
      String type;
      public DateKey(ChannelIdIF id, String type) {
        this.id = id;
        this.type = type;
      }
      public boolean equals(Object obj) {
        if (obj == this) return true;
        if (! this.getClass().isInstance(obj)) return false;
        DateKey dk = (DateKey) obj;
        return type.equals(dk.type);
      }
      public int hashCode() {
        return (id.hashCode() + type.hashCode());
      }
      public String toString() {
        return id.toDelimitedSeedNameString('.') +"|"+ type;
      }
    }
    static private class ParmKey implements Serializable {
      ChannelIdIF id;
      DateRange   dr;
      String type;

      public ParmKey(ChannelIdIF id, DateRange dr, String type) {
        this.id = id;
        this.dr = dr;
        this.type = type;
      }
      public boolean equals(Object obj) {
        if (obj == this) return true;
        if (! this.getClass().isInstance(obj)) return false;
        ParmKey pk = (ParmKey) obj;
        return ( id.equals(pk.id) && dr.equals(pk.dr) && type.equals(pk.type) );
      }
      public int hashCode() {
        return ( id.hashCode()+dr.hashCode()+type.hashCode() );
      }
      public String toString() {
        // no fractional secs
        return id.toDelimitedSeedNameString('.') +"|"+ dr.toDateString("yyyy/MM/dd:HH:mm:ss") +"|"+ type;
      }
    }
}   // end of GlobalChannelCorrectionDataMap class
