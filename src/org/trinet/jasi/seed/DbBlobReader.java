package org.trinet.jasi.seed;

import java.io.*;
import java.sql.*;
import org.trinet.jasi.*;
import org.trinet.util.*;
import oracle.sql.BLOB;
import java.sql.Blob;

/** Gets waveform timeseries from the data source by using a stored procedure
 *  that returns the data as a blob (Binary Large OBject). The blob contains
 *  miniSeed packets. This allows us to use the dbase server as the waveserver. <p>
 *  The WAVE.GET_WAVEFORM_BLOB(?,?,?,?,?) stored procedure could be implemented
 *  on any database for any waveform data set. */

// AWW 12/2002
// AWW 2008/04/03 - made methods final and synchronized (trying to avoid thread interference in SeedReader?)

public class DbBlobReader {

  private static final int MAX_READ_ATTEMPTS = 2;
  private static final String sql = "{ ? = call WAVE.GET_WAVEFORM_BLOB(?,?,?,?,?) }";
  private Connection conn = null;
  private CallableStatement csStmt = null;
  private boolean isPostgreSQL = false;

  public DbBlobReader() {}

  public DbBlobReader(Connection conn) {
      this.conn = conn;
  }

  public final synchronized void closeConnection() { // -aww 2007/01/10
      try {
        try {
          if (csStmt != null) csStmt.close();
        }
        catch (SQLException ex2) {
          System.err.println("DbBlobReader closeConnection closed statement:" + ex2.getMessage());
        }

        // assume connection unique to this instance, not shared -aww 2010/08/11
        if (conn != null) {
          conn.close();
        }

      }
      catch (SQLException ex) {
        System.err.println("DbBlobReader closeConnection " + ex.getMessage());
        //ex.printStackTrace();
      }
      finally {
        // set null here no matter what
        csStmt = null;
        conn = null;
      }
  }

  public final void reconnect() { // -aww 2007/01/10
     closeConnection();
     connect();
  }

  public final synchronized void connect() { // -aww 2007/01/10
    try {

      boolean isClosed = true; // assume so

      if (conn != null) { // find out if it is not closed here
         try {
             isClosed = conn.isClosed(); //  isClosed test 12/06/2006 -aww
         }
         catch (SQLException ex2) {
             System.err.println("DbBlobReader connect(): " + ex2.getMessage());
         }
      }

      if (isClosed) { // need to create a new connection
        try {
          if (csStmt != null) csStmt.close(); // clear any preexisting statement resources
        }
        catch (SQLException ex3) {
          System.err.println("DbBlobReader connect() closed statement:" + ex3.getMessage());
        }
        finally {
          csStmt = null;
        }

        // If wf.getWaveDataSource() is null, use default DataSource connection
        // and set the static source attribute of the input wf object?
        // else if wf.getWaveDataSource() is not null we need to throw/handle an
        // exception if wf wave data source is instead assigned to a rt.Waveserver.
        // Connection conn = ((SQLDataSourceIF) wf.getWaveDataSource()).getConnection();
        //Connection conn = DataSource.getConnection(); // does it conflict with parametric SQL??
        conn = DataSource.getNewConnect(); // experiment with isolation to new connection - aww 09/01/2005
        if (conn == null) throw new SQLException("DbBlobReader DataSource connection is null!");
      }
      isPostgreSQL = conn.getMetaData().getDatabaseProductName().toString().equalsIgnoreCase("postgresql");

      // Done once after a new connection creation, until it's closed
      if (csStmt == null) {
        csStmt = conn.prepareCall(sql); // time range extraction
        if (isPostgreSQL) {
          csStmt.registerOutParameter(1, java.sql.Types.BINARY);
        } else {
          csStmt.registerOutParameter(1, java.sql.Types.BLOB);
        }
      }

    }
    catch (SQLException ex) {
      System.err.println("DbBlobReader Connection Error");
      ex.printStackTrace();
    }
  }

  /** Read the timeseries for this waveform, decode the miniSeed and put the
   *  resulting timeseries in the Waveform. Returns the size of the resulting
   *  Waveform segment list.*/
  public final int getDataFromDataSource(Waveform waveform) {
    return this.getDataFromDataSource(waveform,waveform.getStart().doubleValue(),waveform.getEnd().doubleValue());
  }

  /** Read the timeseries for this waveform, decode the miniSeed and put the
   *  resulting timeseries in the Waveform. Returns the size of the resulting
   *  Waveform segment list.
   *  Input times are true epoch with leap seconds included.
   *  */
  public final synchronized int getDataFromDataSource(Waveform waveform, double startTime, double endTime) {
    Waveform wf = waveform;
    double wfStartTime = wf.getStart().doubleValue();
    double wfEndTime   = wf.getEnd().doubleValue();
    if(startTime == 0.) startTime = wfStartTime;
    if(endTime   == 0.) endTime = wfEndTime;
    if (wfStartTime > endTime || wfEndTime < startTime) return 0;
    String filename  = wf.getPathFilename();
    if (filename == null)  return 0; // could try to get db file info here -aww ??
    int traceoff = wf.getFileOffset();
    int nbytes   = wf.getFileByteCount();
    if (traceoff < 0 || nbytes <= 0) return 0;

    int status = 0;
    int readAttempt = 1;
    wf.setAmpUnits(Units.COUNTS);
    while (readAttempt <= MAX_READ_ATTEMPTS) { // retry until cause of error resolved ? -aww 12/04/2007
      try {
        //
        // removed and replace code by method call below - aww 2007/01/10
        //
        connect();
        // bind args
        csStmt.setString(2,filename);
        csStmt.setInt(3,traceoff);
        csStmt.setInt(4,nbytes);
        //
        // NCEDC tables data epoch times include leap seconds; 
        // internally, the jasi java code uses nominal epoch seconds.
        // Leap seconds should be substracted/added when data is 
        // read/written from/to datasource by the concrete subtypes,
        // Use "nominal" time in the data request to the db for
        // timeseries, the nominal times are used to filter the
        // miniseed packet header times.
        // DataSource:
        // isdbTimeBaseLeap(), isdbTimeBaseNominal() 
        //
        csStmt.setDouble(5,startTime); // nominal
        csStmt.setDouble(6,endTime); // nominal
        csStmt.execute();
        byte[] wfBytes;
        if (isPostgreSQL) {
          wfBytes = csStmt.getBytes(1);
        } else {
          java.sql.Blob blob = (java.sql.Blob) csStmt.getBlob(1);
          //System.out.println("DbBlobReader blob class: " + blob.getClass().getName());
          if (blob == null) return 0;
          wfBytes = blob.getBytes(1,(int) blob.length());

          if (blob instanceof java.sql.Blob) {
            //oracle.sql.BLOB BLOB = (oracle.sql.BLOB) blob;
            java.sql.Blob BLOB = (java.sql.Blob) blob;
            //System.out.println("BLOB open? " +BLOB.isOpen()+ " temp ? "+BLOB.isTemporary());
            if (nbytes > 0 && (wfBytes == null || wfBytes.length == 0)) {
              System.out.println("DbBlobReader " + wf.getChannelObj().toDelimitedSeedNameString() + " blob bytes read: " +
                      ((wfBytes == null) ? "-1" : String.valueOf(wfBytes.length)) + " IF WCOPY,PATH are correct, then are DISKS mounted on your DB host?");
            }
            try {
              if (BLOB instanceof oracle.sql.BLOB) {
                ((oracle.sql.BLOB) BLOB).freeTemporary();
              }
              // Comment out until all db Oracle versions >= 12c , needs Java1.6 to call this method
              //else {
              //    BLOB.free();
              //}
            } catch (SQLException e) {
              System.err.println(e.getMessage());
              //System.err.println("Warning: DbBlobReader " +wf.getChannelObj().toString()+ " BLOB.freeTemporary() ? at " + new DateTime().toString());
              System.err.println("Warning: DbBlobReader " + wf.getChannelObj().toString() + " BLOB.free() ? at " + new DateTime().toString());
            }
          }
          blob = null;
        }

        readAttempt = MAX_READ_ATTEMPTS+1;
    
        if (wfBytes == null || wfBytes.length == 0) return 0;
        // decode the mSEED packets that were returned in the BLOB
        status = getDataPacketBytes(wf, wfBytes);
        wfBytes = null;
      }
      catch (SQLException ex) {
        if (ex.getMessage().indexOf("22275") > -1) { // ORA-22275: invalid LOB locator specified
          readAttempt++;
          if (readAttempt <= MAX_READ_ATTEMPTS) {
              try {
                Thread.currentThread().sleep(35L); // arbitrary 35L ms sleep temporary here, change if too big of delay (but 13L too small) -aww
              } catch (InterruptedException ex2) {
                readAttempt = MAX_READ_ATTEMPTS+1;
              }
          }
          else { // don't try again, report and bail
            System.err.println("ERROR DbBlobReader.getDataFromDataSource " + new DateTime().toString() + " " +
                    ex.getMessage() + " attempt #" + readAttempt + " for: ");
            System.err.println(wf.toString());
          }
        }
        else { // other exception, report and bail
           System.err.println("ERROR DbBlobReader.getDataFromDataSource " + new DateTime().toString() + " " +
                   ex.getMessage() + " for: ");
           System.err.println(wf.toString());
           ex.printStackTrace();
           readAttempt = MAX_READ_ATTEMPTS+1;
        }
      } finally {
        JasiDatabasePropertyList.debugSQL(sql, csStmt);
      }
    } // while invalid LOB locator, retry getBlob until MAX_READ_ATTEMPTS
  
    // collapse the Waveform into the minimum number of WFSegments and
    // populate the packetSpans array
    wf.collapse();
    return status;
  }

  /** Decode the miniSeed packets in the byte array using SeedReader and put the
   *  resulting timeseries in the Waveform's ArrayList of WFSegments. The list is
   *  not collapsed.
   *  Returns the size of the resulting Waveform segment list.*/
  public static final int getDataPacketBytes(Waveform wf, byte [] bytes) {

    byte [] aBuffer = new byte [SeedReader.DEFAULTSEEDBLOCKSIZE];

    try {

      ByteArrayInputStream inBufferStream = new ByteArrayInputStream (bytes);

      // loop through the SEED packets
      SeedHeader hdr = null;
      WFSegment wfseg = null;
      while (inBufferStream.available() > 0) {

        // read header only
        inBufferStream.read(aBuffer, 0, SeedReader.FrameSize);
        hdr = SeedHeader.parseSeedHeader(aBuffer);
        if (SeedReader.debug == true) System.out.println(hdr.toString());
        wfseg = hdr.createWFsegment();

        // you only know the SEED block size and therefore data size
        // AFTER reading the header.
        // NOTE: can't assume byte.length will tell you because the array may have
        //       been dimensioned arbitrarily large.

        // calc. size for data part of packet
        int dataSize = hdr.seedBlockSize-SeedReader.FrameSize;
        // put data into aBuffer
        inBufferStream.read(aBuffer, SeedReader.FrameSize, dataSize);

        // decompress data and put in WaveformSegment
        if ( hdr.isData() ) {
          wfseg.setTimeSeries( SeedReader.decompress (hdr, aBuffer) );
          wf.getSegmentList().add(wfseg);
        }
      } // end of while loop

      inBufferStream.close();
    }
    catch (IOException ex) {
      System.err.println ("IO error: " + ex.toString());
    }
    catch (Exception ex) {
      System.err.println ("General exception: " + ex.toString());
    }
    return wf.getSegmentList().size();
  }

  public final void finalize() {
    try {
      try {
        if (csStmt != null) {
          csStmt.close();
        }
      }
      catch(SQLException ex) { }

      if (conn != null) {
          conn.close(); // assume connection unique to this instance, not shared -aww 2010/08/11
      }

    }
    catch(SQLException ex) { }
    finally {
        conn = null;
        csStmt = null;
    }
  }

}
