package org.trinet.jasi;

import org.trinet.util.*;
import org.trinet.util.gazetteer.*;
import org.trinet.util.velocitymodel.*;
/**
 * Friendly interface to org.trinet.util.velocitymodel
 */
public class TravelTime implements java.io.Serializable {

  protected static boolean debug = false;

  protected static TravelTime defaultTT = new TravelTime(null);

  protected PrimarySecondaryWaveTravelTimeGeneratorIF ttGenerator = null;

  /** Returns default model. */
  protected TravelTime() {
    this(null, null);
  }
  protected TravelTime(UniformFlatLayerVelocityModelIF model) {
    this(model, null);
  }
  protected TravelTime(UniformFlatLayerVelocityModelIF model, Geoidal source) {
    initGenerator(model, source);
  }

  public static void setDebug(boolean tf) {
      debug = tf;
  }

  protected void initGenerator(UniformFlatLayerVelocityModelIF model, Geoidal source) {
    UniformFlatLayerVelocityModelIF myModel = model; 
    if (myModel == null) { // Kludge here for default
      myModel = createDefaultModel();
    }
    if (ttGenerator == null) ttGenerator = new TTLayer(myModel);
    else ttGenerator.setModel(myModel);

    Geoidal mySource = source;
    if (mySource == null) mySource = new LatLonZ(0.,0., 6.);  // 6km depth source
    setSource(mySource);
  }

  /** Return new instance using specified input velocity model */
  public static TravelTime getInstance(UniformFlatLayerVelocityModelIF model) {
    // create the single instance
    //if (defaultTT == null) defaultTT = new TravelTime(model);
    //return defaultTT;
    return new TravelTime(model);
  }

  /** Return default class Singleton.*/
  public static TravelTime getInstance() {
    return defaultTT; // class default 
  }

  public static UniformFlatLayerVelocityModelIF createDefaultModel() {
      UniformFlatLayerVelocityModelIF flatLayerModel = null;
      UniformFlatLayerVelocityModel.createDefaultModels();
      if (EnvironmentInfo.getNetworkCode().equals("CI")) // SCEDC
          flatLayerModel = UniformFlatLayerVelocityModel.SOCAL_DEFAULT;
      else if (EnvironmentInfo.getNetworkCode().equals("NC")) // NCEDC
          flatLayerModel = UniformFlatLayerVelocityModel.NOCAL_DEFAULT;
      else // UNKNOWN do we return null or?
          flatLayerModel = UniformFlatLayerVelocityModel.TWO_LAYER_DEFAULT;
      return flatLayerModel; 
  }

  /** Set the velocity model for the class default (static) instance.
   * @see #getInstance()
   * */
  public static void setDefaultModel(UniformFlatLayerVelocityModelIF model) {
      defaultTT.setModel(model);
  }
  public static void setDefaultModel(String velModelClassName) {
      defaultTT.setModel(velModelClassName);
  }

  public UniformFlatLayerVelocityModelIF getModel() {
      if (ttGenerator == null) return null;
      return (UniformFlatLayerVelocityModelIF) ttGenerator.getModel();
  }

  /** Set the velocity model for this instance.*/
  public void setModel(UniformFlatLayerVelocityModelIF model) {
      if (debug) System.out.println("DEBUG TravelTime.setModel : " + model);
      ttGenerator.setModel(model);
  }
  public void setModel(String velModelClassName) {
      UniformFlatLayerVelocityModelIF vm = null; 
      try { // model with a zero-arg default constructor
          vm = (UniformFlatLayerVelocityModelIF) Class.forName(velModelClassName).newInstance();
      }
      catch (ClassNotFoundException ex) {
          ex.printStackTrace();
      }
      catch (InstantiationException ex) {
          ex.printStackTrace();
      }
      catch (IllegalAccessException ex) {
          ex.printStackTrace();
      }
      setModel(vm);
  }

  public void setSource(Geoidal source) {
      ttGenerator.setSource(source);
  }

  public void setSource(Solution sol) {
      //ttGenerator.setSource(new LatLon); // replaced by below to use mdepth -aww 2015/10/10
      ttGenerator.setSource(new LatLonZ(sol.lat.doubleValue(),sol.lon.doubleValue(),sol.mdepth.doubleValue())); // use mdepth -aww 2015/10/10
  }

  public void setSourceZ(double depthKm) {
    setSource(new LatLonZ( 0.0, 0.0, depthKm));
  }

  /** Assumes both source and receiver at zero depth. */
  public double getTTp(double dist) {
    return ttGenerator.pTravelTime(dist);
  }
  /** Assumes source at 'depth' and receiver at zero depth. */
  public double getTTp(double dist, double depth) {
    setSourceZ(depth);
    return ttGenerator.pTravelTime(dist);
  }
  /** Assumes source at 'depth' and receiver at zero depth. */
  public double getTTp(LatLonZ source, LatLonZ receiver) {
    double dist = source.horizontalDistanceFrom(receiver);
    setSource(source);
    return getTTp(dist);
  }
//
  public double getTTs(double dist) {
    return getTTp(dist) * getPSRatio();
  }
  public double getTTs(double dist, double depth) {
    return getTTp(dist, depth) * getPSRatio();
  }
  public double getTTs(LatLonZ source, LatLonZ receiver) {
    return getTTp(source, receiver) * getPSRatio();
  }

  /** More efficient to get them together. */
  public DoubleRange getTTps(double dist) {
    double p = getTTp(dist);
    return new DoubleRange(p, p*getPSRatio());
  }
  public DoubleRange getTTps(double dist, double depth) {
    double p = getTTp(dist, depth);
    return new DoubleRange(p, p*getPSRatio());
  }
  public DoubleRange getTTps(LatLonZ source, LatLonZ receiver) {
    double p = getTTp(source, receiver);
    return new DoubleRange(p, p*getPSRatio());
  }

  public double getPSRatio() {
    return ((UniformFlatLayerVelocityModelIF) ttGenerator.getModel()).getPSRatio();
  }

  public String toString() {
    return ttGenerator.toString();
  }

  /*
  public final static class Tester {
    public static void main (String args[]) {
      TravelTime tt = TravelTime.getInstance();
      for (int i =0; i<=100; i += 5) {
        System.out.println (i + "   "+  tt.getTTp(i) + "  "+tt.getTTs(i));
      }
    }
  }
  */
}
