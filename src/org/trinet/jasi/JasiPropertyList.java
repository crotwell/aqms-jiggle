package org.trinet.jasi;

import java.text.*;
import java.util.*;
import java.io.*;
import org.trinet.util.*;

/**
 * Jasi API application properties list.
 * Default properties are in the file "$JASI_HOME/<fileName>" (UNIX).
 * thus the system environmental variable "JASI_USER_HOMEDIR" should be
 * defined when the JVM is started using the "-D" switch.
 * User specific properties should in "$JASI_USER_HOMEDIR/.jasi/<fileName>" (UNIX).
 * Use 'getProperty()' to retrieve properties from this list.
 */
public class JasiPropertyList extends GenericPropertyList {

   // Path & file names constructed thus:
   // "JASI_HOME/<filename>"
   // "JASI_USER_HOMEDIR/.jasi/<filename>"

/**
 *  Constructor: makes empty property list.
 *  Use setFilename() and reset() to load it.
 */
    public JasiPropertyList() { }

/**
 *  Constructor: Makes a COPY of a property list.
 *  Doesn't read the files.
 */
    public JasiPropertyList(JasiPropertyList props) {
        super(props);
        // Don't call setup() it will use any overridden
        // implementation of this method in subclasses
        // which may cause initialization problems!
        privateSetup(); // added 11/05/2004 -aww
    }

/**
 *  Constructor: Makes a COPY of a property list, then read the files.
 */
    public JasiPropertyList(Properties props, String fn) {
        super(props,fn);
        // Don't call setup() it will use any overridden
        // implementation of this method in subclasses
        // which may cause initialization problems!
        privateSetup(); // added 11/05/2004 -aww
    }
/**
 *  Constructor: reads properties from file matching input filename String.
 */
    public JasiPropertyList(String fn) {
        super(fn);
        // Don't call setup() it will use any overridden
        // implementation of this method in subclasses
        // which may cause initialization problems!
        privateSetup(); // added 11/05/2004 -aww
    }

    /**
     * This form of Constructor uses explicite path/file as passed and
     * does not try to build a path using <userhome>.
     * @param userPropFileName
     * @param defaultPropFileName
     */
    public JasiPropertyList(String userPropertiesFileName, String defaultPropertiesFileName) {
        super(userPropertiesFileName, defaultPropertiesFileName);
        // Don't call setup() it will use any overridden
        // implementation of this method in subclasses
        // which may cause initialization problems!
        privateSetup(); // added 11/05/2004 -aww
    }

    /** Set the subdir location and name of the property file relative to user default path,
     * load properties from file, set default required values, and configure environment. */
    public boolean initialize(String filetype, String userFileName, String defaultFileName) {
        boolean status = super.initialize(filetype, userFileName, defaultFileName);
        return (setup() && status); // configure local environment first
    }

    /** Overrides GenericProperyList.getFiletype() to default to "jasi". */
    public String getFiletype() {
        if (filetype == "") { // not yet defined
            // declare userhome subdir for property file
            filetype = (EnvironmentInfo.hasApplicationName()) ?
                EnvironmentInfo.getApplicationName().toLowerCase() : "jasi";
        }
        return filetype;
    }


    /**
     * Setup class static, instance, and/or environment variables from current properties.
     * This method should be called after the pertinent properties are initially read
     * or changed through updates. (Don't invoke in constructors, subclasses override).
     */
    public boolean setup() {
        return privateSetup();
    }

    //Kludge to get around re-read of aux properties after they have been changed via GUI a dialog in Jiggle preferences. -aww 2008/05/14
    boolean readAuxFile = true;
    public boolean setup(boolean readAuxFile) {
        this.readAuxFile = readAuxFile;
        boolean status = setup();
        this.readAuxFile = true;;
        return status;
    }

    /** Use in constructors */
    private boolean privateSetup() {

        boolean status = true;

        if (readAuxFile) {
          // Auxillary file can override properties already set in parent, since last read property trumps previously set property.
          String file = getUserFileNameFromProperty("auxPropFile");
          if (file != null) {
            System.out.println("INFO: Loading properties from auxPropFile: " + file); 
            if (! readPropertiesFile(file) ) {
                System.err.println("    ERROR: unable to load properties from auxPropFile: " + file); 
                return false;
            }
          }
          // Below ok for input, but what about saving of GUI changed properties, they would all go into one file.
          String [] propfileTags = getStringArray("auxPropFileTags");
          if (propfileTags != null && propfileTags.length > 0) {
              for (int idx=0; idx < propfileTags.length; idx++) {
                file = getUserFileNameFromProperty("auxPropFile."+propfileTags[idx]);
                if (!file.matches(".*\\.prop.*$")) file += ".props";
                if (file != null) {
                  System.out.println("INFO: Loading properties from auxPropFile." + propfileTags[idx] + ": " + file); 
                  if (! readPropertiesFile(file) ) {
                      System.err.println("    ERROR: unable to load properties from auxPropFile." + propfileTags[idx] + ": " + file); 
                      return false;
                  }
                }
              }
          }
          //
        }

        //if (System.getProperty("JASI_USER_NAME") == null && isSpecified("jasiUserName")) { // instead, let property spec override -aww 2008/11/07 
        if (isSpecified("jasiUserName")) {
           String user = getProperty("jasiUserName", "");
           if (! user.equals("")) System.setProperty("JASI_USER_NAME", user);
        }

        if (isSpecified("authNetCodes")) {
           EnvironmentInfo.setAuthNetCodes(getStringArray("authNetCodes"));
        }
        else if (EnvironmentInfo.hasAuthNetCodes()) {
            setProperty("authNetCodes", EnvironmentInfo.getAuthNetCodes());
        }

        if (isSpecified("localNetCode")) {
           EnvironmentInfo.setNetworkCode(getProperty("localNetCode"));
        }
        else if (EnvironmentInfo.hasNetworkCode()) {
            setProperty("localNetCode", EnvironmentInfo.getNetworkCode());
        }
        else System.err.println("WARNING: check properties, localNetCode not defined!");

        // Use method lookup to define processing default flag as "A" or "H"
        if (isSpecified("autoProcessing") )
          EnvironmentInfo.setAutomatic(getBoolean("autoProcessing"));

        // Set the default jasi object type to be used
        if ( isSpecified("jasiObjectType") )
          status = JasiObject.setDefault(getProperty("jasiObjectType"));

        return status;
    }

    /** Return <i>true</i> if input property name is referenced
     * by the methods in this class or its parents, a name that can
     * be utilized by application classes.
     * */
    public boolean isKnownPropertyName(String propName) {
        return getKnownPropertyNames().contains(propName);
    }

    // Need method below implemented in each subclass so that
    // the compilation of the subclass references its "static"
    // method of same name:
    /** Return a List of property names used
     * by application classes utilizing this class.
     * */
    public List getKnownPropertyNames() {
        List aList = super.getKnownPropertyNames();
        List myList = classDeclaredPropertyNames();
        if (aList instanceof ArrayList)
            ((ArrayList)aList).ensureCapacity(aList.size() + myList.size());
        aList.addAll(myList);
        Collections.sort(aList);
        return aList;
    }

    public static List classDeclaredPropertyNames() {
        List myList = new ArrayList(10);

        myList.add("debug");
        myList.add("autoProcessing");
        myList.add("auxPropFile");
        myList.add("jasiObjectType");
        myList.add("jasiUserName");
        myList.add("localNetCode");
        myList.add("authCodes");
        myList.add("verbose");

        Collections.sort(myList);

        return myList;
    }

    public List getUnknownPropertyNames() {
        Enumeration e = propertyNames();
        List known = getKnownPropertyNames();
        List unknown = new ArrayList(known.size());
        String name = null;
        while ( e.hasMoreElements() ) {
          name = (String) e.nextElement();
          if ( ! known.contains(name) ) unknown.add(name);
        }
        return unknown;
    }

    public List getUndefinedPropertyNames() {
        Enumeration e = propertyNames();
        List undefined = getKnownPropertyNames();
        while ( e.hasMoreElements() ) undefined.remove(e.nextElement());
        return undefined;
    }

    public String getLocalNetCode() {
        return getProperty("localNetCode");
    }
    public void setLocalNetCode(String net) {
        setProperty("localNetCode", net);
    }

    public String [] getAuthNetCodes() {
        return getStringArray("authNetCodes");
    }
    public void setAuthNetCodes(String [] codes) {
        setProperty("authNetCodes", codes);
    }

    /** Defaults <i>true </i> if property is not specified to be consistent
     * with EnvironmentInfo default automatic state.
     */
    public boolean getAutoProcessing() {
        return ( isSpecified("autoProcessing") ) ?
            getBoolean("autoProcessing") : true;
    }
    public void setAutoProcessing(boolean tf) {
        setProperty("autoProcessing", tf);
    }
    public String getJasiObjectType() {
        return getProperty("jasiObjectType");
    }
    public void setJasiObjectType(String type) {
        setProperty("jasiObjectType",type);
    }

    /** Save the user's current properties configuration to file with a default header. */
    public boolean saveProperties() {
        StringBuffer sb = new StringBuffer(256);
        sb.append("--- JasiPropertyList --- [").append(getUserPropertiesFileName()).append("]");
        remove("auxPropFile"); // since all properties input are now written to parent
        return saveProperties(sb.toString());
    }

} // end of class

