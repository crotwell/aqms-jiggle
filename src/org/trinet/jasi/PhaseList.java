package org.trinet.jasi;
import java.util.*;
import org.trinet.util.*;
/**
 * PhaseList.java
/*

ArrayList
    ActiveArrayList
      JasiReadingList
        PhaseList
*/
public class PhaseList extends JasiReadingList {

    // Class which list elements must be instanceof
    //protected static final Class elementClass = Phase.class;
 
    /** No-op default constructor, empty list. */
    public PhaseList() {}

    /** Create empty list with specified input capacity.*/
    public PhaseList(int capacity) { super(capacity); }

    /** Create list and add input collection elements to list.*/
    public PhaseList(Collection col) {
      super(col);
    }

    /** This "factory" method creates concrete subclass with database commit methods specific to implementation. */
    public static final PhaseList create() {
        return create(JasiObject.DEFAULT);
    }

    /** This "factory" method creates concrete class with database commit methods specific to implementation. 
     * The argument is an integer implementation type.
     * @See: JasiObject
     */
    private static final PhaseList create(int schemaType) {
        return create(JasiFactory.suffix[schemaType]);
    }

    /** This "factory" method creates concrete class with database commit methods specific to implementation. 
     * The argument is as 2-char implementation suffix.
     * @See: JasiObject
     */
    private static final PhaseList create(String suffix) {
        return (PhaseList) JasiObject.newInstance("org.trinet.jasi.PhaseList", suffix);
    }

    public void initSolDependentData() {
        for (int ii = 0; ii < size(); ii++) {
          getPhase(ii).initSolDependentData();
        }
    }

    /** Returns header string describing data returned by toNeatString().*/
    public String getNeatHeader() {
      return Phase.getNeatStringHeader();
    }

    /**
     * Convenience method that casts the object array returned by
     * Collection.toArray() as an array of type Phase.
     * */
    public Phase[] getArray() {
        return (Phase[]) toArray(new Phase[this.size()]);
    }

    /**
     * Convenience wrapper for ArrayList.get() that casts the returned object 
     * as a Phase.
     * */
    public Phase getPhase(int idx) {
        return (Phase) get(idx);
    }

  /**
   * Returns as String the entire list of undeleted Phases in Hypoinverse archive format. 
   * Need a static method of a formating filter class to do this instead of here ! - aww
   */
    public String dumpToArcString() {
      int count = size();
      if (count == 0) return "- Empty PhaseList -";
      StringBuffer sb = new StringBuffer(count * 148);
      Phase ph = null;
      for (int i = 0; i < count; i++) {
        ph = (Phase) get(i);
        if (!ph.isDeleted())
          sb.append(HypoFormat.toArchiveString(ph)).append("\n");
      }
      return sb.toString();
    }

    // NOTE: Null input values for channel, sol, type, skips condition test for matching that attribute 
    public Phase getNearestToTime(double time, Channel chan, Solution sol, String type) {
        if (isEmpty()) return null;
        Phase nearest = null;
        double nearestDiff = Double.MAX_VALUE;
        double diff;
        int mySize = size();
        for (int i = 0; i<mySize; i++) {
          JasiReadingIF jr = (JasiReadingIF) get(i);
          // which line order below is optimal for rejection test?
          if ((chan != null) && ! chan.equalsNameIgnoreCase(jr.getChannelObj()) ) continue;
          if ((sol != null)  && ! jr.isAssignedTo(sol) ) continue;
          if ((type != null)  && ! type.equals(jr.getTypeQualifier())) continue;
          diff = Math.abs(jr.getTime() - time);
          if (diff < nearestDiff) {
            nearestDiff = diff;
            nearest = (Phase) jr;
          }
        }
        return nearest;
    }

} // PhaseList
