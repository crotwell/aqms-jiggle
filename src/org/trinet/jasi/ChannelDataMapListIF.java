package org.trinet.jasi;
import java.util.*;
import org.trinet.jdbc.table.*;

public interface ChannelDataMapListIF {
    public void setDefaultDataFactory(ChannelDataIF defaultDataFactory) ;
    public boolean isEmpty() ;
    public int size() ;
    public void clear() ;
    public Object remove(ChannelDataIF cd) ;
    public Object add(ChannelDataIF cd) ;
    public boolean addAll(Collection cdList) ;
    public boolean addAll(ChannelDataIF [] data) ;
    public void putAll(ChannelDataMap cdl) ;
    public Collection values() ;
    public boolean contains(Channelable chan, java.util.Date date) ;
    public boolean contains(ChannelIdIF id, java.util.Date date) ;
    public boolean contains(ChannelDataIF cd) ;
    public ChannelDataIF lookUp(Channelable chan, Date date) ;
    public ChannelDataIF lookUp(ChannelIdIF id, Date date) ;
    public ChannelDataIF lookUp(ChannelDataIF cd, Date date) ;
    public ChannelDataIF get(Channelable chan, Date date) ;
    public ChannelDataIF get(ChannelIdIF id, Date date) ;
    public ChannelDataIF get(ChannelDataIF cd, Date date) ;
    public boolean load() ;
    public boolean load(java.util.Date date) ;
    public ChannelDataIF load(ChannelIdIF chan, java.util.Date date) ;
}
