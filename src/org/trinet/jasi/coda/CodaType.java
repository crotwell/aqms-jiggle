package org.trinet.jasi.coda;
import java.io.*;
/** Type of coda measured from P-time, S-time, origin-time, etc.*/
public class CodaType extends Object implements Cloneable, Serializable {
    public final static int UNK_ID = 0;
    public final static int P_ID = 1;
    public final static int S_ID = 2;
    public final static int L_ID = 3;
    public final static CodaType UNKNOWN = new CodaType(UNK_ID, "?");
    public final static CodaType P = new CodaType(P_ID, "P");
    public final static CodaType S = new CodaType(S_ID, "S");
    public final static CodaType L = new CodaType(L_ID, "L"); // origin elapsed time

    private int id = UNK_ID;
    private String type = "?";

    private CodaType(int id, String type) {
        this.id = id;
        this.type = type;
    }

    protected int getId() { return id;}

    protected String getName() { return type;}

    public static CodaType getCodaType(int id) {
        if (id == P_ID) return P;
        if (id == S_ID) return S;
        if (id == L_ID) return L;
        return UNKNOWN;
    }
    public static CodaType getCodaType(String typeName) {
        if (typeName.equalsIgnoreCase("S")) return S;
        if (typeName.equalsIgnoreCase("P")) return P;
        if (typeName.equalsIgnoreCase("L")) return L;
        return UNKNOWN;
    }

    public String toString() {
        return type;
    }

    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (getClass().isInstance(obj)) return false;
        CodaType cd = (CodaType) obj;
        return (id == cd.getId() && type.equals(cd.getName()));
    }

    public Object clone() {
      CodaType ct = null;
      try {
        ct = (CodaType) super.clone();
      }
      catch (CloneNotSupportedException ex) {
        System.err.println(ex.toString());
      }
      return ct;
    }
}

