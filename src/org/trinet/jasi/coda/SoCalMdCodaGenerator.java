package org.trinet.jasi.coda;

/** To calculate time series amplitude coda decay data and get results in a Coda object. */
public class SoCalMdCodaGenerator extends DefaultMdCodaGenerator {
    /*
    protected static final int soCalMdWindowCount[] = {
       1,  2,  3,  4,  5,  6,  7,  8, 10, 12,
      16, 20, 24, 28, 32, 36, 40, 44, 48, 56,
      64, 72
    }
    */
    /*
    // First 21 windows are like NoCal version, but then it skips
    // the NoCal window at 138 seconds and instead shifts the next
    // window start to 142 secs to maintain 16 secs offset window spacing
    // The last window ends at 144 secs, with its center at 143 secs.
    private static final int soCalMdWindowCountStartingSecs[] = {
       0,  2,  4,  6,  8, 10, 12, 14, 18, 22,
      30, 38, 46, 54, 62, 70, 78, 86, 94, 110,
     126, 142
    };
    */

    public SoCalMdCodaGenerator() {
        super();
        this.algorithm  = "SoCalMd";
    }

    /*
    protected double [] getWindowTimesToSave() {
        return soCalMdWindowCountStartingSecs;
    }
    */

    protected double getWindowStartingSampleSecsOffset(int windowCount) {
        if (overlapStartingWindows) {
            if (windowCount <= overlapWindowMax) { // 1-sec center offsets, overlap windows
                return windowCount;
            }
            else { // for greater window counts use 2-sec offset
                return super.getWindowStartingSampleSecsOffset(windowCount) - overlapWindowMax;
            }
        }
        return super.getWindowStartingSampleSecsOffset(windowCount); // standard 2-sec offsets, end to end

        //if (windowCount < 22) {
        //  return (double) soCalMdWindowCountStartingSecs[windowCount];
        //}
        //else {
        //  return 32.0 * (windowCount - 17); // at 22 = 160 secs, then 32 sec spacing after
        //}
    }

}
