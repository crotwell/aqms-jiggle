package org.trinet.jasi.coda;
import java.util.*;
import org.trinet.jasi.*;
public class CodaList extends AbstractMagnitudeAssocList {

    // Class which list elements must be instanceof
    //protected static final Class elementClass = Coda.class;

    /** No-op default constructor, empty list. */
     public CodaList() { }
    /** Create empty list with specified input capacity.*/
     public CodaList(int capacity) { super(capacity); }
    /** Create list and add input collection elements to list.*/
     public CodaList(Collection col) { super(col); }

    /** Returns header string describing data returned by toNeatString().*/
    public String getNeatHeader() {
      return Coda.getNeatStringHeader();
    }
    /**
     * Convenience method that casts the object array returned by
     * Collection.toArray() as an array of type Coda.
     * */
    public Coda[] getArray() {
        return (Coda[]) toArray(new Coda[this.size()]);
    }
    /**
     * Convenience wrapper for ArrayList.get() that casts the returned object 
     * as an Coda.
     * */
    public Coda getCoda(int idx) {
        return (Coda) get(idx);
    }

    public static final CodaList create() {
        return create(JasiObject.DEFAULT);
    }

    private static final CodaList create(int schemaType) {
        return create(JasiFactory.suffix[schemaType]);
    }

    private static final CodaList create(String suffix) {
        return (CodaList) JasiObject.newInstance("org.trinet.jasi.CodaList", suffix);
    }

    /*
    * Returns list containing the elements of this list whose channel
    * data match that of the input Channel.
    * If Channel is null, an empty list is returned. 
    public CodaList getAssociated(Channel chan) {
        return (CodaList)getAssociatedByChannel(chan);
    }
    * Returns list containing the elements of this list associated
    * with input solution. If Solution is null, unassociated elements 
    * are returned.
    public CodaList getAssociated(Solution sol) {
        return (CodaList) getAssociatedWith(sol, true);
    }
    */
/*
    //Return the element nearest to input time. Returns null if there are none.  
    public Coda getNearestCoda(double time) {
      return (Coda) getNearestToTime(time);
    }
    // Return element nearest to this time whose channel data match input Channel. Returns null if none.
    public Coda getNearestCoda(double time, Channel chan) {
      return (Coda) getNearestToTime(time, chan);
    }
    //Returns list element nearest to input time associated with input Solution.
    //Returns null if there is no match.
    public Coda getNearestCoda(double time, Solution sol) {
     return (Coda) getNearestToTime(time, sol);
    }
    // Return true if the list has codas.
    public boolean hasCoda() { return hasElements(true); }
*/
}
