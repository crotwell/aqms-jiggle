package org.trinet.jasi.coda;
import java.util.*;
import org.trinet.jasi.*;

/** To calculate time series amplitude coda decay data and get results in a Coda object. */
public class DefaultMdCodaGenerator extends AbstractCodaGenerator {


    public static final int     DEFAULT_MIN_GOOD_WINDOWS       = 2;
    public static final int     DEFAULT_MAX_GOOD_WINDOWS       = 20;

    public static final double  DEFAULT_QFIX                   = 1.8;

    public static final double  DEFAULT_BIAS_LTA_SECS          = 10.;
    public static final double  DEFAULT_CODA_START_SNR         = 3.0;
    public static final double  DEFAULT_CODA_CUTOFF_SNR        = 1.5;
    public static final double  DEFAULT_PASS_THRU_NSR          = 1.8; 

    public static final boolean DEFAULT_RESET_ON_CLIP          = false;

    /*
    // EW (RTP) window offsets; "saved" to to output, "last" 6 values:
    // Method could implement storage of time-amp data at these
    // selected intervals in reversed time order
    // (cf. Mc method only saves 1st and last good windows 2-pairs).

    protected static final int mdWindowCount[] = {
       1,  2,  3,  4,  5,  6,  7,  8, 10, 12,
      16, 20, 24, 28, 32, 36, 40, 44, 48, 56,
      64, 70, 73
    };
    protected static final int [] mdWindowCenterTimeSeconds[] = {
      1,   3,   5,  7,  9, 11, 13, 15, 19,  23,
      31,  39,  47, 55, 63, 71, 79, 87, 95, 111,
      127, 139, 144
    };
    */

    public DefaultMdCodaGenerator() {
        initBuffers(DEFAULT_MAX_GOOD_WINDOWS);
        this.algorithm                         = "Md";
        this.biasLTASecs                       = DEFAULT_BIAS_LTA_SECS;       // optionally set by method input
        this.minGoodWindowsToEndCalcOnClipping = DEFAULT_MIN_GOOD_WINDOWS;    // optionally set by method input
        this.codaStartSignalToNoiseRatio       = DEFAULT_CODA_START_SNR;      // optionally set by method input
        this.codaCutoffSignalToNoiseRatio      = DEFAULT_CODA_CUTOFF_SNR;     // optionally set by method input
        this.passThruNoiseDecayAmpRatio        = DEFAULT_PASS_THRU_NSR;       // optionally set by method input
        this.qFix                              = DEFAULT_QFIX;                // optionally set by method input
        this.resetOnClipping                   = DEFAULT_RESET_ON_CLIP;       // optionally set by method input
    }

    /*
    protected Coda timeAmpsToResults(Coda resultsCoda) {
        return super.timeAmpsToResults(resultsCoda, 1); // save values from end
    }
    */

    /*  Weight algorithm from Mc algorithm, see Carl Johnson PhD thesis. -aww
    protected double getWindowWeight(double tauCurrentWindow, double averagedAbsAmp) {
	double weight = 1.0;
	if (averagedAbsAmp < minCodaStartingAmp) {
             weight = (averagedAbsAmp - maxCodaNoiseCutoffAmp)/(minCodaStartingAmp - maxCodaNoiseCutoffAmp);
        }
        return (100.*weight)/(tauCurrentWindow*tauCurrentWindow);
    }
    */
}
