package org.trinet.jasi.coda;
import org.trinet.jdbc.datatypes.*;
import java.io.Serializable;

public class TimeAmp extends Object implements Cloneable, java.io.Serializable {
    protected DataDouble time;
    protected DataDouble amp;

    public TimeAmp(DataNumber time, DataNumber amp) {
      this.time = (time.isNull()) ? new DataDouble() : new DataDouble(time.doubleValue()); 
      this.amp  = (amp.isNull())  ? new DataDouble() : new DataDouble(amp.doubleValue()); 
    }

    public TimeAmp(double time, double amp) {
      this.time = new DataDouble(time);
      this.amp = new DataDouble(amp);
    }

    public String toString() {
       StringBuffer sb = new StringBuffer(48);
       sb.append(time.toStringSQL()).append(" ").append(amp.toStringSQL());
       return sb.toString();
    }

    public String toString(char delimiter) {
       StringBuffer sb = new StringBuffer(48);
       sb.append(time.toStringSQL()).append(delimiter).append(amp.toStringSQL());
       return sb.toString();
    }

    public boolean hasNullValue() {
      return time.isNull() || amp.isNull();
    }

    public boolean isValid() {
      return !hasNullValue() && !(amp.doubleValue() == 0. && time.doubleValue() == 0.); 
    }


    public DataDouble getTime() { return time; }
    public DataDouble getAmp() { return amp; }
    public double getTimeValue() { return time.doubleValue();}
    public double getAmpValue() { return amp.doubleValue();}
    public void setAmpValue(double value) { amp.setValue(value);}
    public void setTimeValue(double value) { time.setValue(value);}

    public boolean equals(Object obj) {
      if ( ! (obj instanceof TimeAmp) ) return false;
      TimeAmp ta = (TimeAmp) obj;
      return time.equalsValue(ta.time) && amp.equalsValue(ta.amp);
    }

    public Object clone() {
      TimeAmp ta = null;
      try {
        ta = (TimeAmp) super.clone();
        ta.time = (DataDouble) this.time.clone();
        ta.amp  = (DataDouble) this.amp.clone();
      }
      catch (CloneNotSupportedException ex) {
        ex.printStackTrace();
      }
      return ta;
    }
}

