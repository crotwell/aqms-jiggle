package org.trinet.jasi.coda;
import java.io.*;
/** Type of coda duration ('a' = amplitude, 'd' = tau duration, 'h' = human input)
 * */
public class CodaDurationType extends Object implements Cloneable, Serializable {

    /** Default unknown data type id. */
    public final static int UNK_ID = 0;

    /** Mc acceptable type id. */
    public final static int A_ID = 1;

    /** Md acceptable type id. */
    public final static int D_ID = 2;

    /** Human type id, could be acceptable for any algorithm. */
    public final static int H_ID = 3;

    /** Default unknown coda duration type */
    public final static CodaDurationType UNKNOWN = new CodaDurationType(UNK_ID, "?");

    /** Data derived from a Mc algorithm scan of waveform timeseries. */
    public final static CodaDurationType A = new CodaDurationType(A_ID, "a");

    /** Data derived from a Md algorithm scan of waveform timeseries. */
    public final static CodaDurationType D = new CodaDurationType(D_ID, "d");

    /** Data measured/entered by human. */
    public final static CodaDurationType H = new CodaDurationType(H_ID, "h");

    private int id;
    private String type;

    private CodaDurationType(int id, String type) {
        this.id = id;
        this.type = type;
    }

    protected int getId() { return id;}

    /** String identifying the derivation origin of the coda duration data. */
    public String getName() { return type;}

    public static CodaDurationType getDurationType(int id) {
        if (id == A_ID) return A;
        if (id == D_ID) return D;
        return UNKNOWN;
    }
    public static CodaDurationType getDurationType(String typeName) {
        if (typeName.equalsIgnoreCase("a")) return A;
        if (typeName.equalsIgnoreCase("d")) return D;
        if (typeName.equalsIgnoreCase("h")) return H;
        return UNKNOWN;
    }

    public String toString() {
        return type;
    }

    public Object clone() {
      CodaDurationType dt = null;
      try {
        dt = (CodaDurationType) super.clone();
      }
      catch (CloneNotSupportedException ex) {
        System.err.println(ex.toString());
      }
      return dt;
    }

    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != getClass()) return false;
        CodaDurationType cdt = (CodaDurationType) obj;
        return ( (id == cdt.id) && (type.equals(cdt.type)) ); 
    }
}

