package org.trinet.jasi.coda;
import java.io.*;
import org.trinet.jasi.*;
public abstract class CodaPhaseDescriptor implements Cloneable, Serializable {
/** Reference phase type of coda measurement ('P', 'S', etc.) */
    private CodaType codaType = CodaType.UNKNOWN;  // unknown
/** Calculated coda parameters fit type ('a', 'd', etc.) */
    private CodaDurationType durationType = CodaDurationType.UNKNOWN; // unknown
/** Description of of observed phase's attributes (e.g. noise, truncation) */
    private String description = "";

    private boolean changed = false;

    protected CodaPhaseDescriptor() {}
    protected CodaPhaseDescriptor(CodaType codaType, CodaDurationType durationType, String description) {
        this.codaType = codaType;
        this.durationType = durationType;
        if (description != null) this.description = description;
    }
    public void copy(CodaPhaseDescriptor cd) {
        setCodaType(cd.codaType);
        setDurationType(cd.durationType);
        setDescription(cd.description);
    }
    public boolean hasChanged() { return changed;}
    public void setUpdate(boolean tf) { changed = tf;};
    public CodaType getCodaType() { return codaType; }
    public CodaDurationType getDurationType() { return durationType; }
    public String getCodaTypeName() { return (codaType == null) ? "?" : codaType.getName();}
    public String getDurationTypeName() { return (durationType == null) ? "?" : durationType.getName();}
    public String getDescription() { return description;}
    public String getTypeString() { return getCodaTypeName()+getDurationTypeName(); }
    public void setCodaType(CodaType type) {
        if (codaType.equals(type)) return;
        changed = true;
        this.codaType = type;
    }
    public void setDurationType(CodaDurationType type) {
        if (durationType.equals(type)) return;
        changed = true;
        this.durationType = type;
    }
    public void setDescription(String descript) {
        if (descript == null) descript = "";
        if (description.equals(descript)) return;
        changed = true;
        this.description = descript;
    }

    public void setTauType(char type) {
        if (description == null) description = "PS"+ type;
        else if (description.length() >= 3) description = description.substring(0,2) + type; 
        else if (description.length() == 2) description = description + type; 
    }

    public boolean isSameType(CodaPhaseDescriptor cd) {
        return (this.codaType == cd.codaType && this.durationType == cd.durationType);
    }
    public String toNeatString() {
        StringBuffer sb = new StringBuffer(16);
        sb.append(getTypeString()).append(" ").append(description);
        return sb.toString();
    }
    public String toString() {
        return "CodaType:\""+getCodaTypeName()+"\" DurType:\""+getDurationTypeName()+"\" Desc:\""+description+"\"";
    }
    public boolean equals(Object codaDesc) {
        if (codaDesc == this) return true;
        if (codaDesc == null || ! (codaDesc instanceof CodaPhaseDescriptor)) return false;
        CodaPhaseDescriptor cd = (CodaPhaseDescriptor) codaDesc;
        return (isSameType(cd) && this.description.equals(cd.description) ) ? true : false;
    }

    public Object clone() {
        CodaPhaseDescriptor cd = null;
        try {
            cd = (CodaPhaseDescriptor) super.clone();
        }
        catch(CloneNotSupportedException ex) {
            ex.printStackTrace();
        }
        return cd;
    }

     public static final CodaPhaseDescriptor create() {
        return create(JasiObject.DEFAULT);
     }

    /**
     * Instantiate an object of this type. You do
     * NOT use a constructor. This "factory" method creates various
     * concrete implementations. The argument is an integer implementation type.
     * @See: JasiObject
     */
     public static final CodaPhaseDescriptor create(int schemaType) {
        return create(JasiFactory.suffix[schemaType]);
     }

    /**
     * Instantiate an object of this type. You do
     * NOT use a constructor. This "factory" method creates various
     * concrete implementations. The argument is as 2-char implementation suffix.
     * @See: JasiObject
     */
     public static final CodaPhaseDescriptor create(String suffix) {
        return (CodaPhaseDescriptor) JasiObject.newInstance("org.trinet.jasi.coda.CodaPhaseDescriptor", suffix);
     }
/*
  static public final class Tester {
     public static final void main(String [] args) {
             CodaPhaseDescriptor cpd = CodaPhaseDescriptor.create();
             String tmp = cpd.toNeatString();
             System.out.println(tmp  + " " +(tmp== null));
             tmp = cpd.toString();
             System.out.println(tmp + " " + (tmp==null));
             cpd.codaType = CodaType.UNKNOWN;
             cpd.durationType = CodaDurationType.UNKNOWN;
             cpd.description = null;
             tmp = cpd.toNeatString();
             System.out.println(tmp + " " + (tmp==null));
             tmp = cpd.toString();
             System.out.println(tmp + " " + (tmp==null));
     }
  }
*/
}

