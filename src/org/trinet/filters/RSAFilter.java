package org.trinet.filters;

import org.trinet.jasi.*;
import org.trinet.util.*;

/**
 * <p>Compute recusive Response Spectral Amplitudes for periods of 0.3, 1.0 & 3.0 seconds.
 * The filter is the same as for synthetic Wood-Andersons but the constants are different.
 * Implimentation of the technique developed by Hiroo Kanamori described in:<p>
 * "Continuous Monitoring of Ground-Motion Parameters", BSSA, V89, pp. 311-316, Feb. 1999.</p>
 *
 * This is only valid for 20, 40, 50, 80, 100, 200, 250, or 500 sample/sec data.<p>
 *
 * !!!!!!!!!!!!! NOTE !!!!!!!!!!!!!!!!<p>
 *
 * If input is VELOCITY result is multipied by omega to returns spectral ACCELERATION.<br>
 * If input is ACCELERATION this returns spectral ACCELERATION<br>
 *
 * On June 15, 2004 all Sectral Amps were standardize to be
 * Spectral ACCELERATIONS because thats what ShakeMap wants.
*/

public class RSAFilter extends WAFilter {

  public static final String FILTER_NAME = "RSA";
  protected static boolean DEFAULT_APPLY_TAPER = true; // used to be false - aww 12/04/2006 - need to check with Kanamori

  protected int outputAmpType = AmpType.UNKNOWN;   // default

  public RSAFilter(int type) {
      this(type, 0);
  }

  public RSAFilter(int type, int sampleRate) {
    // Sets filter description string, etc.
    setDescription(RSAFilter.FILTER_NAME);
    setApplyTaper(RSAFilter.DEFAULT_APPLY_TAPER);
    // this prevents "correcting" for 2080 WA gain factor
    // which shouldn't be done for Spectral Amps
    setWAmagnification (WA_MAGNIFICATION_2800);  // this prevents "correcting" for 2080 WA gain factor
    setType(type);
    setSampleRate(sampleRate);
  }

  public FilterIF getFilter(String type) {
    return new RSAFilter(FilterTypes.getType(type));
  }
  public FilterIF getFilter(int sampleRate) {
    return new RSAFilter(this.type, sampleRate);
  }
  public FilterIF getFilter(int type, int sampleRate) {
    return new RSAFilter(type, sampleRate);
  }

  public void setType(int type) {
    super.setType(type);
    if (type == FilterTypes.SP03) setOutputAmpType(AmpType.SP03);
    else if (type == FilterTypes.SP10) setOutputAmpType(AmpType.SP10);
    else if (type == FilterTypes.SP30) setOutputAmpType(AmpType.SP30);
    else System.err.println("RSAFilter: unknown filter type: " + type);
  }

  public void setUnits(Waveform wf) {  // added 12/20/2006 -aww
      //wf.setAmpUnits(outputAmpType); // wrong type index should be indexed by Units not AmpType
      wf.setAmpUnits(Units.CMSS);
  }

  /** Set output amp type to produce. Returns true if amp type is a legal one, else false. */
  public boolean setOutputAmpType(int ampType) {
    if (outputAmpTypeIsLegal(ampType)) {
      setDescription(AmpType.getString(ampType));
      outputAmpType = ampType;
      return true;
    } else {
      System.err.println("RSAFilter: illegal amp type = "+ ampType);
      return false;
    }
  }

  /** Set adjusted values of  h, f, g which depend on the sample rate
   * and the gain (high or low). Sample interval is the sampling interval in
   * seconds per sample, e.g. 0.01 for 100 sample/sec data. The boolean argument is 'true'
   * for high-gains and false for low-gains. This is only valid for 20, 40, 50, 80, or
   * 100, 200, 250, or 500 sample/sec data. Returns false if sample rate is invalid.
   * For example values, see Table 1 in Kanamori, etal., 1999 where: <p>
   *
   * h   = the damping constant (Uncorrected = 0.8) <br>
   * w0  = natural angular frequency (Uncorrected = 2(pi)/period or 2(pi) * freq. <br>
   * gwa = gain factor of a Wood-Anderson instrument (Uncorrected = 2800)<br><p>
   *
   * The paper says the constants for 80 and 100 sps are the same. Hiroo's code (Subroutine c1c2s()),
   * however, has separate values for 80sps that don't appear in the paper. Those are used here for
   * the 80 sps channels.
   */
/*
// Filter coefficients below h0 = damping, f0 natural frequency, g0 gain are
// outputs from one pass through Hiroo's difcdet least squares program with
// inputs of damping= .05 for SP, and .8 for WA and 400 Freq steps for
// input fmax = 10 Hz and fmaxOut = 10 Hz.  on 09/14/2007 - aww
Example input file i_difcdet:

itype label
h0    f0      per    n  fmax  g   fmax0
Href  Fref Gref

1  'VEL 200sps SP0.3'
0.05  3.3333  0.005  400  10  1.  10
0.05  3.3333  1.

1  'VEL 200sps SP1.0'
0.05  1.0  0.005  400  10  1.  10
0.05  1.0  1.

1  'VEL 200sps SP3.0'
0.05  0.3333  0.005  400  10  1.  10
0.05  0.3333  1.

1  'VEL 200sps WA2800'
0.8  1.25  0.005  400  10  2800  10
0.8  1.25  2800

1  'VEL 200sps WA2080'
0.8  1.25  0.005  400  10  2080  10
0.8  1.25  2080


VEL 20sps SP0.3          h0 = -0.46096E+00; f0 =  0.32615E+01; g0 =  0.87998E+00; // 10 Hz  0.05 or -.05 seed

VEL 20sps SP1.0          h0 = -0.20437E+00; f0 =  0.98814E+00; g0 =  0.96269E+00; // 10 Hz
VEL 20sps SP3.0          h0 = -0.26340E-02; f0 =  0.33404E+00; g0 =  0.99978E+00; // 10 Hz
VEL 40sps SP0.3          h0 = -0.21132E+00; f0 =  0.33379E+01; g0 =  0.98537E+00; // 10 Hz
VEL 40sps SP1.0          h0 = -0.12787E+00; f0 =  0.99505E+00; g0 =  0.98712E+00; // 10 Hz
VEL 40sps SP3.0          h0 =  0.23796E-01; f0 =  0.33370E+00; g0 =  0.10015E+01; // 10 Hz
VEL 50sps SP0.3          h0 = -0.15941E+00; f0 =  0.33433E+01; g0 =  0.99502E+00; // 10 Hz
VEL 50sps SP1.0          h0 = -0.12881E-01; f0 =  0.10025E+01; g0 =  0.10030E+01; // 10 Hz
VEL 50sps SP3.0          h0 =  0.29054E-01; f0 =  0.33362E+00; g0 =  0.10014E+01; // 10 Hz
VEL 80sps SP0.3          h0 = -0.17940E+00; f0 =  0.33020E+01; g0 =  0.97716E+00; // 10 Hz
VEL 80sps SP1.0          h0 =  0.10741E-01; f0 =  0.10017E+01; g0 =  0.10027E+01; // 10 Hz
VEL 80sps SP3.0          h0 =  0.36927E-01; f0 =  0.33352E+00; g0 =  0.10011E+01; // 10 Hz
VEL 100sps SP0.3         h0 = -0.15375E+00; f0 =  0.33098E+01; g0 =  0.98330E+00; // 10 Hz
VEL 100sps SP1.0         h0 =  0.18606E-01; f0 =  0.10014E+01; g0 =  0.10023E+01; // 10 Hz
VEL 100sps SP3.0         h0 =  0.39526E-01; f0 =  0.33347E+00; g0 =  0.10006E+01; // 10 Hz
VEL 200sps SP0.3         h0 = -0.23338E-02; f0 =  0.33405E+01; g0 =  0.10036E+01; // 10 Hz
VEL 200sps SP1.0         h0 =  0.34316E-01; f0 =  0.10007E+01; g0 =  0.10013E+01; // 10 Hz
VEL 200sps SP3.0         h0 =  0.44844E-01; f0 =  0.33345E+00; g0 =  0.10010E+01; // 10 Hz

ACC 20sps SP0.3          h0 = -0.45895E+00; f0 =  0.32616E+01; g0 =  0.86527E+00; // 10 Hz
ACC 20sps SP1.0          h0 = -0.20499E+00; f0 =  0.98796E+00; g0 =  0.96789E+00; // 10 Hz
ACC 20sps SP3.0          h0 = -0.23077E-02; f0 =  0.33402E+00; g0 =  0.10034E+01; // 10 Hz
ACC 40sps SP0.3          h0 = -0.21083E+00; f0 =  0.33375E+01; g0 =  0.98061E+00; // 10 Hz
ACC 40sps SP1.0          h0 = -0.12800E+00; f0 =  0.99502E+00; g0 =  0.98805E+00; // 10 Hz
ACC 40sps SP3.0          h0 =  0.23859E-01; f0 =  0.33369E+00; g0 =  0.10021E+01; // 10 Hz
ACC 50sps SP0.3          h0 = -0.15911E+00; f0 =  0.33430E+01; g0 =  0.99189E+00; // 10 Hz
ACC 50sps SP1.0          h0 = -0.12797E-01; f0 =  0.10025E+01; g0 =  0.10036E+01; // 10 Hz
ACC 50sps SP3.0          h0 =  0.29094E-01; f0 =  0.33362E+00; g0 =  0.10018E+01; // 10 Hz
ACC 80sps SP0.3          h0 = -0.17948E+00; f0 =  0.33015E+01; g0 =  0.97590E+00; // 10 Hz
ACC 80sps SP1.0          h0 =  0.10775E-01; f0 =  0.10017E+01; g0 =  0.10029E+01; // 10 Hz
ACC 80sps SP3.0          h0 =  0.36948E-01; f0 =  0.33352E+00; g0 =  0.10013E+01; // 10 Hz
ACC 100sps SP0.3         h0 = -0.15381E+00; f0 =  0.33093E+01; g0 =  0.98247E+00; // 10 Hz
ACC 100sps SP1.0         h0 =  0.18627E-01; f0 =  0.10014E+01; g0 =  0.10025E+01; // 10 Hz
ACC 100sps SP3.0         h0 =  0.39532E-01; f0 =  0.33347E+00; g0 =  0.10007E+01; // 10 Hz
ACC 200sps SP0.3         h0 = -0.23143E-02; f0 =  0.33405E+01; g0 =  0.10034E+01; // 10 Hz
ACC 200sps SP1.0         h0 =  0.34320E-01; f0 =  0.10007E+01; g0 =  0.10013E+01; // 10 Hz
ACC 200sps SP3.0         h0 =  0.44867E-01; f0 =  0.33344E+00; g0 =  0.10013E+01; // 10 Hz

// 0.05 damping filter is unstable near end of trace, builds amp, so try different damping seed?
VEL 10 80sps SP0.3       h0 = -0.80995E-01; f0 =  0.33454E+01; g0 =  0.10031E+01; // 10Hz -0.05 damping seed
VEL 10 100sps SP0.3      h0 = -0.54777E-01; f0 =  0.33446E+01; g0 =  0.10041E+01; // 10Hz -0.05 damping seed
ACC 10 80sps SP0.3       h0 = -0.80882E-01; f0 =  0.33453E+01; g0 =  0.10018E+01; // 10Hz -0.05 damping seed
ACC 10 100sps SP0.3      h0 = -0.54703E-01; f0 =  0.33445E+01; g0 =  0.10033E+01; // 10Hz -0.05 damping seed

VEL 10 20sps SP1.0       h0 = -0.10772E+00; f0 =  0.10036E+01; g0 =  0.99294E+00; // 10Hz -0.05 damping seed
VEL 10 40sps SP1.0       h0 = -0.28654E-01; f0 =  0.10029E+01; g0 =  0.10027E+01; // 10Hz -0.05 damping seed
ACC 10 20sps SP1.0       h0 = -0.10706E+00; f0 =  0.10036E+01; g0 =  0.99876E+00; // 10Hz -0.05 damping seed
ACC 10 40sps SP1.0       h0 = -0.28519E-01; f0 =  0.10029E+01; g0 =  0.10037E+01; // 10Hz -0.05 damping seed

// like above but fit to input fmax = 7 Hz and fmaxOut = 10 Hz.  on 09/14/2007  - aww

// filter is unstable near end builds amp?
VEL 7 20sps SP1.0        h0 = -0.10745E+00; f0 =  0.10036E+01; g0 =  0.99718E+00; // 7Hz -0.05 damping seed
ACC 7 20sps SP1.0        h0 = -0.10705E+00; f0 =  0.10036E+01; g0 =  0.99896E+00; // 7Hz -0.05 damping seed
ACC 7 40sps SP1.0        h0 = -0.28515E-01; f0 =  0.10029E+01; g0 =  0.10037E+01; // 7Hz -0.05 damping seed
VEL 7 40sps SP1.0        h0 = -0.28609E-01; f0 =  0.10029E+01; g0 =  0.10034E+01; // 7Hz -0.05 damping seed

VEL 7 80sps SP0.3        h0 = -0.80957E-01; f0 =  0.33455E+01; g0 =  0.10037E+01; // 7Hz -0.05 damping seed
VEL 7 100sps SP0.3       h0 = -0.54752E-01; f0 =  0.33446E+01; g0 =  0.10045E+01; // 7Hz -0.05 damping seed
ACC 7 80sps SP0.3        h0 = -0.80866E-01; f0 =  0.33453E+01; g0 =  0.10020E+01; // 7Hz -0.05 damping seed
ACC 7 100sps SP0.3       h0 = -0.54695E-01; f0 =  0.33445E+01; g0 =  0.10034E+01; // 7Hz -0.05 damping seed

//VEL 20sps SP0.3          h0 = -0.53701E+00; f0 =  0.31006E+01; g0 =  0.80926E+00; // 7 Hz  - aww previously -0.03 damping seed for stability?
// for f0 closer to 3.333 h0 = -0.46005E+00; f0 =  0.32635E+01; g0 =  0.89647E+00; // 7 Hz  -.08 damping 20sps SP0.3 but is this on stable?
VEL 20sps SP1.0          h0 = -0.20463E+00; f0 =  0.98810E+00; g0 =  0.96655E+00; // 7 Hz
VEL 20sps SP3.0          h0 = -0.24322E-02; f0 =  0.33402E+00; g0 =  0.10020E+01; // 7 Hz
VEL 40sps SP0.3          h0 = -0.21115E+00; f0 =  0.33381E+01; g0 =  0.98811E+00; // 7 Hz
VEL 40sps SP1.0          h0 = -0.12792E+00; f0 =  0.99505E+00; g0 =  0.98780E+00; // 7 Hz
VEL 40sps SP3.0          h0 =  0.23834E-01; f0 =  0.33370E+00; g0 =  0.10018E+01; // 7 Hz
VEL 50sps SP0.3          h0 = -0.15931E+00; f0 =  0.33434E+01; g0 =  0.99670E+00; // 7 Hz
VEL 50sps SP1.0          h0 = -0.12856E-01; f0 =  0.10025E+01; g0 =  0.10034E+01; // 7 Hz
VEL 50sps SP3.0          h0 =  0.29074E-01; f0 =  0.33362E+00; g0 =  0.10015E+01; // 7 Hz
VEL 80sps SP0.3          h0 = -0.17943E+00; f0 =  0.33020E+01; g0 =  0.97774E+00; // 7 Hz
VEL 80sps SP1.0          h0 =  0.10752E-01; f0 =  0.10017E+01; g0 =  0.10028E+01; // 7 Hz
VEL 80sps SP3.0          h0 =  0.36935E-01; f0 =  0.33351E+00; g0 =  0.10012E+01; // 7 Hz
VEL 100sps SP0.3         h0 = -0.15378E+00; f0 =  0.33098E+01; g0 =  0.98367E+00; // 7 Hz
VEL 100sps SP1.0         h0 =  0.18613E-01; f0 =  0.10014E+01; g0 =  0.10024E+01; // 7 Hz
VEL 100sps SP3.0         h0 =  0.39553E-01; f0 =  0.33346E+00; g0 =  0.10010E+01; // 7 Hz
VEL 200sps SP0.3         h0 = -0.23261E-02; f0 =  0.33405E+01; g0 =  0.10037E+01; // 7 Hz
VEL 200sps SP1.0         h0 =  0.34317E-01; f0 =  0.10007E+01; g0 =  0.10014E+01; // 7 Hz
VEL 200sps SP3.0         h0 =  0.44768E-01; f0 =  0.33345E+00; g0 =  0.10004E+01; // 7 Hz

ACC 20sps SP0.3          h0 = -0.45854E+00; f0 =  0.32628E+01; g0 =  0.87314E+00; // 7 Hz
ACC 20sps SP1.0          h0 = -0.20500E+00; f0 =  0.98796E+00; g0 =  0.96808E+00; // 7 Hz
ACC 20sps SP3.0          h0 = -0.23133E-02; f0 =  0.33401E+00; g0 =  0.10034E+01; // 7 Hz
ACC 40sps SP0.3          h0 = -0.21077E+00; f0 =  0.33376E+01; g0 =  0.98158E+00; // 7 Hz
ACC 40sps SP1.0          h0 = -0.12800E+00; f0 =  0.99501E+00; g0 =  0.98806E+00; // 7 Hz
ACC 40sps SP3.0          h0 =  0.23861E-01; f0 =  0.33370E+00; g0 =  0.10021E+01; // 7 Hz
ACC 50sps SP0.3          h0 = -0.15907E+00; f0 =  0.33431E+01; g0 =  0.99247E+00; // 7 Hz
ACC 50sps SP1.0          h0 = -0.12796E-01; f0 =  0.10025E+01; g0 =  0.10036E+01; // 7 Hz
ACC 50sps SP3.0          h0 =  0.29092E-01; f0 =  0.33362E+00; g0 =  0.10017E+01; // 7 Hz
ACC 80sps SP0.3          h0 = -0.17950E+00; f0 =  0.33015E+01; g0 =  0.97611E+00; // 7 Hz
ACC 80sps SP1.0          h0 =  0.10775E-01; f0 =  0.10017E+01; g0 =  0.10029E+01; // 7 Hz
ACC 80sps SP3.0          h0 =  0.36943E-01; f0 =  0.33351E+00; g0 =  0.10013E+01; // 7 Hz
ACC 100sps SP0.3         h0 = -0.15382E+00; f0 =  0.33093E+01; g0 =  0.98260E+00; // 7 Hz
ACC 100sps SP1.0         h0 =  0.18628E-01; f0 =  0.10014E+01; g0 =  0.10025E+01; // 7 Hz
ACC 100sps SP3.0         h0 =  0.39555E-01; f0 =  0.33346E+00; g0 =  0.10010E+01; // 7 Hz
ACC 200sps SP0.3         h0 = -0.23125E-02; f0 =  0.33405E+01; g0 =  0.10035E+01; // 7 Hz
ACC 200sps SP1.0         h0 =  0.34319E-01; f0 =  0.10007E+01; g0 =  0.10014E+01; // 7 Hz
ACC 200sps SP3.0         h0 =  0.44785E-01; f0 =  0.33344E+00; g0 =  0.10007E+01; // 7 Hz

// Below added 2012/05/14 - aww
        h0 =  0.81515E-02; f0 =  0.33393E+01; g0 =  0.10032E+01; // VEL fmax=7 250sps SP0.3 
        h0 =  0.37453E-01; f0 =  0.10006E+01; g0 =  0.10013E+01; // VEL fmax=7 250sps SP1.0 
        h0 =  0.45795E-01; f0 =  0.33333E+00; g0 =  0.10002E+01; // VEL fmax=7 250sps SP3.0 
        h0 =  0.29091E-01; f0 =  0.33365E+01; g0 =  0.10019E+01; // VEL fmax=7 500sps SP0.3 
        h0 =  0.43749E-01; f0 =  0.10003E+01; g0 =  0.10005E+01; // VEL fmax=7 500sps SP1.0 
        h0 =  0.47835E-01; f0 =  0.33370E+00; g0 =  0.99759E+00; // VEL fmax=7 500sps SP3.0 

        h0 =  0.81605E-02; f0 =  0.33393E+01; g0 =  0.10031E+01; // ACC fmax=7 250sps SP0.3 
        h0 =  0.37454E-01; f0 =  0.10006E+01; g0 =  0.10013E+01; // ACC fmax=7 250sps SP1.0 
        h0 =  0.45778E-01; f0 =  0.33333E+00; g0 =  0.99998E+00; // ACC fmax=7 250sps SP3.0 
        h0 =  0.29093E-01; f0 =  0.33365E+01; g0 =  0.10018E+01; // ACC fmax=7 500sps SP0.3 
        h0 =  0.43754E-01; f0 =  0.10003E+01; g0 =  0.10006E+01; // ACC fmax=7 500sps SP1.0 
        h0 =  0.47952E-01; f0 =  0.33370E+00; g0 =  0.99945E+00; // ACC fmax=7 500sps SP3.0 


*/
  protected boolean setConstants(double sampleInterval, boolean isVelocity) {

    setSampleRate(1.0/sampleInterval);

    // check for valid sample rate
    if (!sampleRateIsLegal(getSampleRate()))  {
      System.err.println ("RSAFilter: invalid sample rate: "+getSampleRate());
      return false;
    }
    if (!outputAmpTypeIsLegal(outputAmpType))  {
      System.err.println ("RSAFilter: invalid amp type requested: "+outputAmpType);
      return false;
    }


    if (isVelocity) {
      if (getSampleRate() == 80) {
        if (outputAmpType == AmpType.SP03) {
          //h0 = -0.0868; f0 = 3.35; g0 = 0.990; // original RT code !!!
          //h0 = -0.17940E+00; f0 =  0.33020E+01; g0 =  0.97716E+00; // 10 Hz
          //h0 = -0.17982E+00; f0 =  0.33333E+01; g0 =  0.97162E+00; // 7Hz fixed freq
          //h0 = -0.17943E+00; f0 =  0.33020E+01; g0 =  0.97774E+00; // 7 Hz
          h0 = -0.80957E-01; f0 =  0.33455E+01; g0 =  0.10037E+01; // 7Hz seed -0.05 damping 


        } else if (outputAmpType == AmpType.SP10) {
          //h0 = 0.0063; f0 = 1.00; g0 = 0.988; // original RT code !!!
          //h0 =  0.10741E-01; f0 =  0.10017E+01; g0 =  0.10027E+01; // 10 Hz
          h0 =  0.10752E-01; f0 =  0.10017E+01; g0 =  0.10028E+01; // 7 Hz
        } else if (outputAmpType == AmpType.SP30) {
          //h0 = 0.0440; f0 = 0.334; g0 = 0.998; // original RT code !!!
          //h0 =  0.36927E-01; f0 =  0.33352E+00; g0 =  0.10011E+01; // 10 Hz
          h0 =  0.36935E-01; f0 =  0.33351E+00; g0 =  0.10012E+01; // 7 Hz
        }
      } else if (getSampleRate() == 100) { // "vsp with delta-t = 0.01 sec"
        if (outputAmpType == AmpType.SP03) { 
          //h0 = -0.0583; f0 = 3.34; g0 = 0.996; // from paper 
          //h0 = -0.15375E+00; f0 =  0.33098E+01; g0 =  0.98330E+00; // 10 Hz
          //h0 = -0.15391E+00; f0 =  0.33333E+01; g0 =  0.97976E+00; // 7Hz fixed freq
          //h0 = -0.15378E+00; f0 =  0.33098E+01; g0 =  0.98367E+00; // 7 Hz
          h0 = -0.54752E-01; f0 =  0.33446E+01; g0 =  0.10045E+01; // 7 Hz seed -0.05 damping
        } else if (outputAmpType == AmpType.SP10) {
          //h0 = 0.0160; f0 = 1.00; g0 = 0.988; // from paper 
          //h0 =  0.18606E-01; f0 =  0.10014E+01; g0 =  0.10023E+01; // 10 Hz
          h0 =  0.18613E-01; f0 =  0.10014E+01; g0 =  0.10024E+01; // 7 Hz
        } else if (outputAmpType == AmpType.SP30) {
          //h0 = 0.0440; f0 = 0.334; g0 = 0.999; // from paper 
          //h0 =  0.39526E-01; f0 =  0.33347E+00; g0 =  0.10006E+01; // 10 Hz
          h0 =  0.39553E-01; f0 =  0.33346E+00; g0 =  0.10010E+01; // 7 Hz
        }
      } else if (getSampleRate() == 50) { // NEW added 09/14/2007 
        if (outputAmpType == AmpType.SP03) { // 7 Hz below
          //h0 = -0.15941E+00; f0 =  0.33433E+01; g0 =  0.99502E+00; // 10 Hz
            h0 = -0.15931E+00; f0 =  0.33434E+01; g0 =  0.99670E+00; // 7 Hz
        } else if (outputAmpType == AmpType.SP10) {
          //h0 = -0.12881E-01; f0 =  0.10025E+01; g0 =  0.10030E+01; // 10 Hz
            h0 = -0.12856E-01; f0 =  0.10025E+01; g0 =  0.10034E+01; // 7 Hz
        } else if (outputAmpType == AmpType.SP30) {
          //h0 =  0.29054E-01; f0 =  0.33362E+00; g0 =  0.10014E+01; // 10 Hz
            h0 =  0.29074E-01; f0 =  0.33362E+00; g0 =  0.10015E+01; // 7 Hz
        }
      } else if (getSampleRate() == 20) { // in paper as "vbb with delta-t = 0.05 sec"
        if (outputAmpType == AmpType.SP03) {
          //h0 = -0.461; f0 = 3.26; g0 = 0.875; // from paper like 10 Hz with "n" df steps = 50
          //h0 = -0.46096E+00; f0 =  0.32615E+01; g0 =  0.87998E+00; // 10 Hz
          // h0 = -0.53701E+00; f0 =  0.31006E+01; g0 =  0.80926E+00; // 7 Hz 01/15/2008 replaced by below
          h0 = -0.46005E+00; f0 =  0.32635E+01; g0 =  0.89647E+00; // VEL 7 Hz 20sps (-.08 damping seed) SP0.3 stability unknown

        } else if (outputAmpType == AmpType.SP10) {
          //g0 = 0.988; f0 = 1.00; h0 = -0.108; // from paper 
          //h0 = -0.20437E+00; f0 =  0.98814E+00; g0 =  0.96269E+00; // 10 Hz
          //h0 = -0.20463E+00; f0 =  0.98810E+00; g0 =  0.96655E+00; // 7 Hz
            h0 = -0.10745E+00; f0 =  0.10036E+01; g0 =  0.99718E+00; // 7Hz -0.05 damping seed
        } else if (outputAmpType == AmpType.SP30) {
          //g0 = 0.986; f0 = 0.333; h0 = -0.021; // from paper
          //h0 = -0.26340E-02; f0 =  0.33404E+00; g0 =  0.99978E+00; // 10 Hz
          h0 = -0.24322E-02; f0 =  0.33402E+00; g0 =  0.10020E+01; // 7 Hz
        }
      } else if (getSampleRate() == 200) { // NEW added 09/14/2007 
        if (outputAmpType == AmpType.SP03) { // 7 Hz below 
         //h0 = -0.23323E-02; f0 =  0.33405E+01; g0 =  0.10037E+01; // 10 Hz
           h0 = -0.23261E-02; f0 =  0.33405E+01; g0 =  0.10037E+01; // 7 Hz
        }else if (outputAmpType == AmpType.SP10) {
         //h0 =  0.34316E-01; f0 =  0.10007E+01; g0 =  0.10013E+01; // 10 Hz
           h0 =  0.34317E-01; f0 =  0.10007E+01; g0 =  0.10014E+01; // 7 Hz
        } else if (outputAmpType == AmpType.SP30) {
         //h0 =  0.44844E-01; f0 =  0.33345E+00; g0 =  0.10010E+01; // 10 Hz
           h0 =  0.44768E-01; f0 =  0.33345E+00; g0 =  0.10004E+01; // 7 Hz
        }
      } else if (getSampleRate() == 40) { // NEW added 09/14/2007 
        if (outputAmpType == AmpType.SP03) { // 7 Hz below
          //h0 = -0.21132E+00; f0 =  0.33379E+01; g0 =  0.98537E+00; // 10 Hz
          h0 = -0.21115E+00; f0 =  0.33381E+01; g0 =  0.98811E+00; // 7Hz
        } else if (outputAmpType == AmpType.SP10) {
          //h0 = -0.12787E+00; f0 =  0.99505E+00; g0 =  0.98712E+00; // 10 Hz
          //h0 = -0.12792E+00; f0 =  0.99505E+00; g0 =  0.98780E+00; // 7Hz
          h0 = -0.28609E-01; f0 =  0.10029E+01; g0 =  0.10034E+01; // 7Hz -0.05 damping seed
        } else if (outputAmpType == AmpType.SP30) {
          //h0 =  0.23796E-01; f0 =  0.33370E+00; g0 =  0.10015E+01; // 10 Hz
          h0 =  0.23834E-01; f0 =  0.33370E+00; g0 =  0.10018E+01; // 7Hz
        }
      } else if (getSampleRate() == 250) { // NEW added 05/14/2012 
        if (outputAmpType == AmpType.SP03) { // 7 Hz below
          h0 =  0.81515E-02; f0 =  0.33393E+01; g0 =  0.10032E+01; // VEL fmax=7 250sps SP0.3 
        } else if (outputAmpType == AmpType.SP10) {
          h0 =  0.37453E-01; f0 =  0.10006E+01; g0 =  0.10013E+01; // VEL fmax=7 250sps SP1.0 
        } else if (outputAmpType == AmpType.SP30) {
          h0 =  0.45795E-01; f0 =  0.33333E+00; g0 =  0.10002E+01; // VEL fmax=7 250sps SP3.0 
        }
      } else if (getSampleRate() == 500) { // NEW added 05/14/2012 
        if (outputAmpType == AmpType.SP03) { // 7 Hz below
          h0 =  0.29091E-01; f0 =  0.33365E+01; g0 =  0.10019E+01; // VEL fmax=7 500sps SP0.3 
        } else if (outputAmpType == AmpType.SP10) {
          h0 =  0.43749E-01; f0 =  0.10003E+01; g0 =  0.10005E+01; // VEL fmax=7 500sps SP1.0 
        } else if (outputAmpType == AmpType.SP30) {
          h0 =  0.47835E-01; f0 =  0.33370E+00; g0 =  0.99759E+00; // VEL fmax=7 500sps SP3.0 
        }
      }

    // Acceleration (low gain)
    } else {

      if (getSampleRate() == 80) {
        if (outputAmpType == AmpType.SP03) {
         //h0 = -0.0800; f0 = 3.34; g0 = 1.00; // from original RT code
         //h0 = -0.17948E+00; f0 =  0.33015E+01; g0 =  0.97590E+00; // 10 Hz
         //h0 = -0.18085E+00; f0 =  0.33333E+01; g0 =  0.98877E+00; // 7Hz fixed freq
         //h0 = -0.17950E+00; f0 =  0.33015E+01; g0 =  0.97611E+00; // 7 Hz
         h0 = -0.80868E-01; f0 =  0.33453E+01; g0 =  0.10020E+01; // 7 Hz seeded -0.05 damping
        } else if (outputAmpType == AmpType.SP10) {
         //h0 = 0.0100; f0 = 1.00; g0 = 1.00; // from original RT code
         //h0 =  0.10775E-01; f0 =  0.10017E+01; g0 =  0.10029E+01; // 10 Hz
         h0 =  0.10775E-01; f0 =  0.10017E+01; g0 =  0.10029E+01; // 7 Hz
        } else if (outputAmpType == AmpType.SP30) {
         //h0 = 0.0430; f0 = 0.334; g0 = 1.00; // from original RT code
         //h0 =  0.36948E-01; f0 =  0.33352E+00; g0 =  0.10013E+01; // 10 Hz
         h0 =  0.36943E-01; f0 =  0.33351E+00; g0 =  0.10013E+01; // 7 Hz
        }
      } else if (getSampleRate() == 100) {
        if (outputAmpType == AmpType.SP03) {
          //h0 = -0.0541; f0 = 3.34; g0 = 1.00; // from paper
          //h0 = -0.15381E+00; f0 =  0.33093E+01; g0 =  0.98247E+00; // 10 Hz
          //h0 = -0.54759E-01; f0 =  0.33333E+01; g0 =  0.99396E+00; // 7Hz fixed freq
          //h0 = -0.15382E+00; f0 =  0.33093E+01; g0 =  0.98260E+00; // 7 Hz
          h0 = -0.54694E-01; f0 =  0.33445E+01; g0 =  0.10034E+01; // 7 Hz seeded -0.05 damping
        } else if (outputAmpType == AmpType.SP10) {
          //h0 = 0.0180; f0 = 1.00; g0 = 1.00; // from paper
          //h0 =  0.18627E-01; f0 =  0.10014E+01; g0 =  0.10025E+01; // 10 Hz
            h0 =  0.18628E-01; f0 =  0.10014E+01; g0 =  0.10025E+01; // 7 Hz
        } else if (outputAmpType == AmpType.SP30) {
          //h0 = 0.0440; f0 = 0.334; g0 = 1.00; // from paper
          //h0 =  0.39532E-01; f0 =  0.33347E+00; g0 =  0.10007E+01; // 10 Hz
            h0 =  0.39555E-01; f0 =  0.33346E+00; g0 =  0.10010E+01; // 7 Hz
        }
      } else if (getSampleRate() == 200) { // from a Hiroo email, 09/01/2004
          if (outputAmpType == AmpType.SP03) { 
            //h0 = -0.102; f0 = 3.32; g0 = 0.993; // from a Hiroo email, 09/01/2004, doesn't seem to produce plottable ts for some channels?
            //h0 = -0.23100E-02; f0 =  0.33402E+01; g0 =  0.10034E+01; // 10 Hz
            h0 = -0.23125E-02; f0 =  0.33405E+01; g0 =  0.10035E+01; // 7 Hz value 0.05 damping (seems to resonate for certain traces, then values to high)
          } else if (outputAmpType == AmpType.SP10) {
            //h0 = 0.0345; f0 = 1.00; g0 = 1.00; // from a Hiroo email, 09/01/2004
            //h0 =  0.34320E-01; f0 =  0.10007E+01; g0 =  0.10013E+01; // 10 Hz
            h0 =  0.34319E-01; f0 =  0.10007E+01; g0 =  0.10014E+01; // 7 Hz
          } else if (outputAmpType == AmpType.SP30) {
            //h0 = 0.0445; f0 = 0.333; g0 = 1.00; // from a Hiroo email, 09/01/2004
            //h0 =  0.44867E-01; f0 =  0.33344E+00; g0 =  0.10013E+01; // 10 Hz
            h0 =  0.44785E-01; f0 =  0.33344E+00; g0 =  0.10007E+01; // 7 Hz
          }
      } else if (getSampleRate() == 50) { // NEW added 09/14/2007 
           if (outputAmpType == AmpType.SP03) { // 7 Hz below
          //h0 = -0.15911E+00; f0 =  0.33430E+01; g0 =  0.99189E+00; // 10 Hz
            h0 = -0.15907E+00; f0 =  0.33431E+01; g0 =  0.99247E+00; // 7 Hz
          } else if (outputAmpType == AmpType.SP10) {
          //h0 = -0.12797E-01; f0 =  0.10025E+01; g0 =  0.10036E+01; // 10 Hz
            h0 = -0.12796E-01; f0 =  0.10025E+01; g0 =  0.10036E+01; // 7 Hz
          } else if (outputAmpType == AmpType.SP30) {
          //h0 =  0.29094E-01; f0 =  0.33362E+00; g0 =  0.10018E+01; // 10 Hz
            h0 =  0.29092E-01; f0 =  0.33362E+00; g0 =  0.10017E+01; // 7 Hz
          }
      } else if (getSampleRate() == 20) { // NEW added 09/14/2007 
          if (outputAmpType == AmpType.SP03) {
          //h0 = -0.45895E+00; f0 =  0.32616E+01; g0 =  0.86527E+00; // 10 Hz
            h0 = -0.45854E+00; f0 =  0.32628E+01; g0 =  0.87314E+00; // 7 Hz
          } else if (outputAmpType == AmpType.SP10) {
          //h0 = -0.20499E+00; f0 =  0.98796E+00; g0 =  0.96789E+00; // 10 Hz
          //h0 = -0.20500E+00; f0 =  0.98796E+00; g0 =  0.96808E+00; // 7 Hz
            h0 = -0.10705E+00; f0 =  0.10036E+01; g0 =  0.99896E+00; // 7Hz -0.05 damping seed
          } else if (outputAmpType == AmpType.SP30) {
          //h0 = -0.23077E-02; f0 =  0.33402E+00; g0 =  0.10034E+01; // 10 Hz
            h0 = -0.23133E-02; f0 =  0.33401E+00; g0 =  0.10034E+01; // 7 Hz
          }
      } else if (getSampleRate() == 40) { // NEW added 09/14/2007 
          if (outputAmpType == AmpType.SP03) {
          //h0 = -0.21083E+00; f0 =  0.33375E+01; g0 =  0.98061E+00; // 10 Hz
            h0 = -0.21077E+00; f0 =  0.33376E+01; g0 =  0.98158E+00; // 7 Hz
          } else if (outputAmpType == AmpType.SP10) {
          //h0 = -0.12800E+00; f0 =  0.99502E+00; g0 =  0.98805E+00; // 10 Hz
          //h0 = -0.12800E+00; f0 =  0.99501E+00; g0 =  0.98806E+00; // 7 Hz
            h0 = -0.28515E-01; f0 =  0.10029E+01; g0 =  0.10037E+01; // 7Hz -0.05 damping seed
          } else if (outputAmpType == AmpType.SP30) {
          //h0 =  0.23859E-01; f0 =  0.33369E+00; g0 =  0.10021E+01; // 10 Hz
            h0 =  0.23861E-01; f0 =  0.33370E+00; g0 =  0.10021E+01; // 7 Hz
          }
      } else if (getSampleRate() == 250) { // NEW added 05/14/2012 
          if (outputAmpType == AmpType.SP03) {
            h0 =  0.81605E-02; f0 =  0.33393E+01; g0 =  0.10031E+01; // ACC fmax=7 250sps SP0.3 
          } else if (outputAmpType == AmpType.SP10) {
            h0 =  0.37454E-01; f0 =  0.10006E+01; g0 =  0.10013E+01; // ACC fmax=7 250sps SP1.0 
          } else if (outputAmpType == AmpType.SP30) {
            h0 =  0.45778E-01; f0 =  0.33333E+00; g0 =  0.99998E+00; // ACC fmax=7 250sps SP3.0 
          }
      } else if (getSampleRate() == 500) { // NEW added 05/14/2012 
          if (outputAmpType == AmpType.SP03) {
            h0 =  0.29093E-01; f0 =  0.33365E+01; g0 =  0.10018E+01; // ACC fmax=7 500sps SP0.3 
          } else if (outputAmpType == AmpType.SP10) {
            h0 =  0.43754E-01; f0 =  0.10003E+01; g0 =  0.10006E+01; // ACC fmax=7 500sps SP1.0 
          } else if (outputAmpType == AmpType.SP30) {
            h0 =  0.47952E-01; f0 =  0.33370E+00; g0 =  0.99945E+00; // ACC fmax=7 500sps SP3.0 
          }
      }

    }

    calcC1C2();

    return true;

  }

 /**
  * Convert a peak spectral velocity to a peak spectral acceleration by multiplying by
  * "omega" (natural angular frequency = 2 * PI * 1/period).<p>
  *
  * Accel = Vel * 2 * PI * (freq)
  *<p>
  * Formula from Bruce Worden.
  * @param  spectral velocity
  * @param  natural period
  * @return spectral acceleration
  */
  static double vel2Accel (double period, double vel) {
    return vel * getOmega(period);
  }

  /**
   * Return "omega" (natural angular frequency = 2 * PI * 1/period).<p>
   *
   * omega = 2 * PI * freq   or   2 * PI * 1/period
   *<p>
   * Formula from Bruce Worden.
   * @param  natural period
   * @return omega
   */
  static double getOmega (double period) {
    return 2.0 * Math.PI * (1.0/period);
  }
  /** Return if the requested amp is for a legal type: AmpType.SP03, AmpType.SP10, or AmpType.SP30*/
  protected boolean outputAmpTypeIsLegal (int amptype) {
    return ( amptype == AmpType.SP03 ||
             amptype == AmpType.SP10 ||
             amptype == AmpType.SP30 );
  }
//
// WHERE THE FILTERING ACTUALLY GETS DONE
//
  /** Return recursively filtered timeseries given a timeseries.
   * If there are fewer than 2 points in the array the original array is returned.
   * The orginal is unchanged.
   *
   * Based on Hiroo's equation (3)
   * */
    protected float[] filterTimeSeries (float[] ts) {

      float sp[] = super.filterTimeSeries(ts);
      if (sp == null) return null;  // handle unknown type rejection case -aww 03/24/2006

      float omega = (float) getOmega(AmpType.getPeriod(outputAmpType));
      omega *= omega; // do omega square here, for displacement to acceleration - aww 01/13/2010

      // Convert peak spectral displacement to peak spectral acceleration
      for (int i = 2; i < ts.length -3; i++) {
        sp[i] *= omega;
      }

      return sp;
    }
  /** Return recursively filtered timeseries given a timeseries.
   * If there are fewer than 2 points in the array the original array is returned.
   * The orginal is unchanged.
   *
   * Based on Hiroo's equation (3)
   * */
   /*
    protected float[] filterTimeSeries (float[] ts) {
      if (ts.length < 2) return ts;

      if (getApplyTaper()) ts = AmplitudeCosineTaper.taper(ts, cosineTaperAlpha);

      float wa[] = new float[ts.length];

      double dt = getSampleInterval();
      double c1x2 = 2.0 * c1;

      // ACCELERATION
      if (wfIn != null && wfIn.getChannelObj().isAcceleration()
            && wfIn.getAmpUnits() == Units.COUNTS) {

          double b0 = 2.0 / (1 + qHighPass);
          double b0xgf = getGainFactor() * b0;
          double a2 = 0.0, a3 = 0.0;
          double gwa_dt2 = gwa * dt * dt;                            // accel

          // convert to real ground motion: cm/sec^2

          for (int i = 2; i < ts.length -3; i++) {
            a3 = ((ts[i]-ts[i-1])/ b0xgf) + (qHighPass * a2);   // convert accel cnt's to grnd mo + filter
            wa[i] = (float) (( (a3 * gwa_dt2) + (c1x2 * wa[i-1]) - wa[i-2] ) / c2);
            a2=a3;
          }

//          float acc[] = convertToGroundMotion(ts);
//           for (int i = 2; i < ts.length -3; i++) {
//            wa[i] = (float) (( acc[i] * gwa_dt2 + c1x2 * wa[i-1] - wa[i-2] ) / c2);
//          }

      // VELOCITY
          } else if (wfIn != null && wfIn.getChannelObj().isVelocity()
                     && wfIn.getAmpUnits() == Units.COUNTS){

            double mult = (gwa/getGainFactor()) * dt ;              // velocity

            // apply the filter - note use of wa[i-2] means we start at i=2
            for (int i = 2; i < ts.length -3; i++) {
              wa[i] = (float) (( (ts[i]-ts[i-1]) * mult + (c1x2 * wa[i-1] - wa[i-2] )) / c2);
            }

          } else {
            System.err.println("WAFilter: illegal time series type.");
            return wa;  // array of 0.0's
          }

          return wa;

    }
*/
}
