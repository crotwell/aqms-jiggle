package org.trinet.filters;

public interface WaveFilter {

/** Return recursively filtered timeseries given a timeseries. */
       public int[] calcValue (int[] ts) ;
} 