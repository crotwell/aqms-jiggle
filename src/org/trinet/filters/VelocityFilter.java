package org.trinet.filters;

import java.util.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import org.trinet.jasi.*;
import org.trinet.util.*;

/** Recursive integration filter to velocity.
 * If the input timeseries is ACCELERATION it will be transformed
 * to cm/sec from counts and integrated to velocity.
 * If input is already a VELOCITY it will be transformed to cm/sec from counts,
 * high pass filtered and the bias will be removed.
 * Returns acceleration in cm/sec. Only intended for use on 20, 80, and 100 sps data.<p>
 *
 * Implimentation of the technique
 * developed by Hiroo Kanamori described in "Continuous Monitoring of Ground-Motion
 * Parameters", BSSA, V89, pp. 311-316, Feb. 1999 as implemented in Hiroo Kanimori's
 * FORTRAN subroutine recres6().
 *
 * @see AbstractWaveformFilter
 * */
public class VelocityFilter extends AbstractWaveformFilter {

  public static final String FILTER_NAME = "Velocity";
  protected static boolean DEFAULT_APPLY_TAPER = true; // 12/04/2006 used to be false -aww 

  public VelocityFilter() {
    this(FilterTypes.VELOCITY, 0);
  }

  public VelocityFilter(int sampleRate) {
    this(FilterTypes.VELOCITY, sampleRate);
  }

  public VelocityFilter(int type, int sampleRate) {
    setDescription(VelocityFilter.FILTER_NAME);
    setApplyTaper(VelocityFilter.DEFAULT_APPLY_TAPER);
    //setCosineTaperAlpha(0.5);    // normally 0.05 - IS THIS RIGHT? we need to check with Kanamori -aww
    setType(type);
    setSampleRate(sampleRate);
  }

/** Returns this same segment with the time-series Filtered.*/
  protected WFSegment filterSegment (WFSegment seg) {

    setWFSegment(seg);

    // Remove bias: note 'scanIt' is 'false' because we don't yet want to recalc bias, max/min, etc.
    seg.setTimeSeries( demean(seg.getTimeSeries()), false );

    // velocity in counts
    if ( seg.getChannelObj().isVelocity()
         && wfIn != null && wfIn.getAmpUnits() == Units.COUNTS) {

      seg.setTimeSeries( convertToGroundMotion(seg.getTimeSeries()) );  // raw vel -> vel
      seg.setIsFiltered(true);       // flag segment as filtered

    // acceleration in counts
    } else if ( seg.getChannelObj().isAcceleration()
                && wfIn != null && wfIn.getAmpUnits() == Units.COUNTS)  {
      seg.setTimeSeries( accelRawToVel(seg.getTimeSeries()) );          // raw accel -> accel
      seg.setIsFiltered(true);       // flag segment as filtered

    } else {
      System.err.println("VelocityFilter illegal input waveform, check gain units for: "+ seg.getChannelObj().toDelimitedSeedNameString());
      seg.setIsFiltered(false);       // flag segment as unfiltered
    }

    return seg;
  }

  /** Set the units for the resulting (filtered) waveform. */
  public void setUnits(Waveform wf) {
    wf.setAmpUnits(Units.CMS);         // accel = cm/sec
  }

  /** Return true if the input amplitude units type (of a waveform) are legal for this filter. */
  public boolean unitsAreLegal(int units) {
    return (units == Units.COUNTS ||
            units == Units.CMS ||
            units == Units.CMSS );
  }
  /** Returns true if the given sample rate is legal for this filter.
   * Overrides parent method because ALL rates are legal for velocity filter. */
  protected boolean sampleRateIsLegal (double rate) {
    return true ;
  }

/** Return velocity by recursively integrating an acceleration timeseries.
 * Input timeseries must be ACCELERATION in units of counts
 * Returns velocity in cm/sec.<p>
 *
 * Applies a High Pass filter.<p>
 *
 * Based on Hiroo Kanimori's FORTRAN subroutine recres6() and equation 17 in the paper.
 *
 * */
  public float[] accelRawToVel (float[] ts) {

    if (ts.length < 3) return ts;

    // <1> High pass filter and convert from counts to cm/sec^2
    ts = convertToGroundMotion(ts);

    if (getApplyTaper()) ts = AmplitudeCosineTaper.taper(ts, cosineTaperAlpha);

    // <2> integrate to velocity (yes, and filter again)
    double dt = (float) getSampleInterval();
    double mult = ( (1 + qHighPass)/2.0 ) * dt;
    float vel[] = new float[ts.length];

    for (int i = 1; i < ts.length -2; i++) {
      vel[i] = (float) ( (ts[i] + ts[i-1])/2.0 * mult + (qHighPass * vel[i-1]) );
    }

    return vel;

  }
}
