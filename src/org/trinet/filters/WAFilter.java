package org.trinet.filters;

import org.trinet.jasi.*;
import org.trinet.util.*;

/**
 * Recursive filter to create Wood-Anderson waveform. Implimentation of the technique
 * developed by Hiroo Kanamori described in "Continuous Monitoring of Ground-Motion
Parameters", BSSA, V89, pp. 311-316, Feb. 1999.<p>
*
* This is only valid for 20, 40, 50, 80, or 100, or 200 sample/sec data.<p>
*
* The default magnification is 2800 but it can be set to 2080 with setWAmagnification().
*
* Gain Factor:<p>
* Richter originally stated that a standard Wood-Anderson tortion seismometer has
* a natural period of 0=0.8s, a damping constant of 0.8 and magnification of x2800
* <it>(Richter, Elementry Seismology, 1958, pg.222)</it>. These values are deeply entrenched
* in the literature of seismology. Work by Bob Urhammer of BSL says the magnification
* should be 2080 (see <it>Urhammer and Collins, 1990, Bulletin of the Seismological
* Society of America, v. 80, p. 702-716)</it>.<p>
* After discussions Caltech agreed to adopt this value. This decision does not
* affect historic readings taken directly from real W-A records. It also does not
* affect historic magnitude calibrations. It DOES affect amplitudes or magnitudes
* based on SYNTHETIC W-A records that were created with a 2800 magnification.
* E.g. all TerraScope & TriNet amps calculated before adopting this class. <p>
* Because amplitudes from SYNTHETIC W-A records that assumed a 2800 magnification
* were empirically corrected against "real" W-A records the resulting correction would
* "fix" the effects for the incorrect magnification. The amplitudes are wrong but
* the magnitudes are right.
*
* <p>See also: <it>Lahr, John, HYPOELLIPSE: A Computer Program for Determining Local
* Earthquake Hypocentral Parameters, Magnitude, and First-Motion Pattern,
* USGS Open-File Report 99-23.</it>
*/

// DDG 09/15/2003 -- 2080 gain 

public class WAFilter extends AbstractWaveformFilter implements Cloneable, NoiseFilterIF {

  public static final int WA_MAGNIFICATION_2800 = 0;
  public static final int WA_MAGNIFICATION_2080 = 1;

    /* This is where the WA magnification is set for everything else!
  * Changed from 2800 to 2080 on 9/15/03. Berkeley convinced us that this
  * was the true magnification (some are more convinced than others)
     */
  public static int WA_MAGNIFICATION_DEFAULT = WA_MAGNIFICATION_2080;

  /** Flag to set the magnification for the synthetic W-A waveform produced.
   * It should be either 2080 or 2800. Note that corrections must match the
   * type of synthetic produced or it must be corrected.
   * @see: setWAmagnification() */
  //int WAmagnification = WA_MAGNIFICATION_2800;     // Default
  int WAmagnification = WA_MAGNIFICATION_DEFAULT;     // Default

  public static String FILTER_NAME = "WOOD-ANDERSON";
  protected static boolean DEFAULT_APPLY_TAPER = true; // 12/04/2006 used to be false -aww 

  public static boolean doLookUp = true; // added static flag, false => no db query, use only cached values -aww 2010/08/19

  /** */
//    float fac2800_2080 = 2800.0f/2080.0f;
  float fac2080_2800 = 2080.0f/2800.0f;

  // Below are the WA consts set in setConstants()
  // NOTE the names of these constants were changed from h,f0,gwa to h0,f0,g0  09/14/2007 -aww
  /** Wood-Anderson damping constant */
  protected double h0;
  /** natural angular frequency constant*/
  protected double f0;
  /** Wood-Anderson gain factor*/
  protected double g0;
  /** Filter constant "c1" see equation (4) */
  protected double c1;
  /** Filter constant "c2" see equation (5) */
  protected double c2;

  protected boolean isVel = false;
  protected boolean isAcc = false;;

  protected ButterworthFilterSMC noiseFilter = null;

  public WAFilter() {
    this(FilterTypes.WOOD_ANDERSON, 0);
  }

  public WAFilter(int sampleRate) {
    this(FilterTypes.WOOD_ANDERSON, sampleRate);
  }

  public WAFilter(int type, int sampleRate) {
    setDescription(WAFilter.FILTER_NAME);
    setApplyTaper(WAFilter.DEFAULT_APPLY_TAPER); // normally 0.05 - IS THIS RIGHT? we need to check with Kanamori -aww
    setSampleRate(sampleRate);
    this.type = FilterTypes.WOOD_ANDERSON; // don't use setType(type) since subclass RSAFilter overrides method
  }

  public WaveformBandpassFilterIF getNoiseFilter() {
      return noiseFilter;
  }

  public FilterIF getFilter(String type) {
    return new WAFilter();
  }
  public FilterIF getFilter(int sampleRate) {
        WAFilter f = null;
        f = (WAFilter) this.clone(); // uses current type settings
        f.setSampleRate(sampleRate);
        return f; 
    //return new WAFilter(sampleRate);
  }
  public FilterIF getFilter(int type, int sampleRate) {
    return new WAFilter(type, sampleRate);
  }

  /** Returns this same segment with the time-series Filtered. If the timeseries
   *  cannot be filtered (because it is at a sample rate this filter can't handle)
   *  the original WFSegment is return, unchanged. */
  protected WFSegment filterSegment(WFSegment seg) {

    // set magic numbers, will return false is invalid sample rate
    if (setConstants(seg)) {

      // note we DON'T call setSamples because we don't want to recalc bias, etc.
      float ts[] = demean(seg.getTimeSeries());

      // replace original with new filtered time series
      // setTimeSeries() recalcs bias, max, min.
      seg.setTimeSeries( filterTimeSeries(ts) );
      seg.setIsFiltered(true);       // flag segment as filtered

    } else {

      System.out.println("Filter: invalid sample rate = "+getSampleRate()+
                         " for "+ seg.getChannelObj().toDelimitedSeedNameString()+
                         " Must be 20, 40, 50, 80, 100, 200, 250, or 500 sps.");
      seg.setIsFiltered(false);       // flag segment as not filtered
    }
    return seg;
  }

  /** Set the units for the resulting waveform after filtering. */
  public void setUnits(Waveform wf) {
    wf.setAmpUnits(Units.CM);         // velocity = cm/sec
  }

  /** Return true if the input units of the waveform are legal for this filter.
   * I.e. are "counts". */
  public boolean unitsAreLegal(int units) {
    return (units == Units.COUNTS );
  }
  /** Returns true if the given sample rate is legal for this filter.
   * Override this if your filter is valid for other than 20, 40, 50, 80, 100, & 200 sps.*/
  protected boolean sampleRateIsLegal(double rate) {
    // added 200sps 9/1/04 - constants from Hiroo.
    // added 40sps 08/18/06 - constants from Hiroo -aww.
    // added 50sps 09/14/07 - constants from Hiroo -aww.
    //return (rate  ==  100.0 || rate == 40 || rate  ==  80.0 || rate  ==  20.0 || rate  == 50.0 || rate  == 200.0 ) ;
    return (    Math.round(rate)  == 100l || Math.round(rate)  ==  80l || Math.round(rate)  == 200l ||
                Math.round(rate)  == 250l || Math.round(rate)  == 500l || Math.round(rate)  == 50l ||
                Math.round(rate)  == 40l  || Math.round(rate)  ==  20l 
           );
  }

  /** Set the magnification using a static flag value.
   * Either: WAFilter.WA_MAGNIFICATION_2800 or WAFilter.WA_MAGNIFICATION_2080.
   * If an invalid value is passed this method has no effect. */
  public boolean setWAmagnification(int magnificationFlag) {
    if (magnificationFlag == WA_MAGNIFICATION_2800 ||
        magnificationFlag == WA_MAGNIFICATION_2080) {

      WAmagnification = magnificationFlag;
      return true;

    } else {
      System.out.println("WAFilter ERROR: setWAmagnification invalid input flag: " + magnificationFlag +
              "  now using flag: " + WAmagnification);
      return false;
    }
  }

  /** Get the magnification using a static flag value.
   * Either: WAFilter.WA_MAGNIFICATION_2800 or WAFilter.WA_MAGNIFICATION_2080.  */
  public int getWAmagnification() {
    return WAmagnification;
  }

  /** Set adjusted values of  h0, f0, g0, c1, c2 which depend on the sample rate
   * and gain. This is only valid for 20, 40, 50, 80, 100, or 200 sample/sec data.
   * Returns false if sample rate is invalid.
   * These values are from Table 1: Kanamori, Maechling, and Hauksson, 1999. <p>
   *
   * h   = the damping constant (Uncorrected = 0.8) <br>
   * w0  = natural angular frequency (Uncorrected = 2(pi)/period or 2(pi) * freq. <br>
   * gwa = gain factor of a Wood-Anderson instrument (Uncorrected = 2800)<br>
   */
  protected boolean setConstants(WFSegment seg) {
    //isVel = seg.getChannelObj().isVelocity(new DateTime(seg.getEpochStart(), true)); // why redo it, just use setWaveform set value? -aww 2010/08/19
    return setConstants(seg.getSampleInterval(), isVel); 
  }
  /** Set adjusted values of  h0, f0, g0, c1, c2 which depend on the sample rate
   * and the gain (high or low). Sample interval is the sampling interval in
   * seconds per sample, e.g. 0.01 for 100 sample/sec data. The boolean argument 'isVelocity'
   * is 'true' for velocity timeseries and false acceleration.
   * Procedure is only valid for 20, 40, 80, 100, or 200
   * sample/sec data. Returns false if sample rate is invalid.
   * These values are from Table 1: Kanamori, Maechling, and Hauksson, 1999. <p>
   *
   * h   = the damping constant (Uncorrected = 0.8) <br>
   * w0  = natural angular frequency (Uncorrected = 2(pi)/period or 2(pi) * freq. <br>
   * gwa = gain factor of a Wood-Anderson instrument (Uncorrected = 2800)<br><p>
   *
   * Important Note:<br>
   * All the constants here were calculated assuming an W-A magnification of <b>2800</b>.
   */
// added 200sps 09/01/04 - constants from Hiroo
/*
// Decreasing number of freq steps in fit (n, nobs) effects fit for coefs of
// low sample rate data most (20 sps). n=500 steps is maximum allowed by code.
// Filter coefficient below are h0 = damping, f0 natural frequency, g0 gain
// output from one pass through Hiroo's difcdet least squares program with
// inputs of damping= .05 for SP types, and .8 for WA and 400 Freq steps for
// fit to input fmax = 10 Hz and fmaxOut = 10 Hz.  09/14/2007  - aww

VEL 20spsWA2800          h0 =  0.57034E+00; f0 =  0.13924E+01; g0 =  0.31175E+04;
VEL 40spsWA2800          h0 =  0.72958E+00; f0 =  0.13398E+01; g0 =  0.31401E+04;
VEL 50spsWA2800          h0 =  0.74985E+00; f0 =  0.13238E+01; g0 =  0.30929E+04;
VEL 80sps WA2800         h0 =  0.77412E+00; f0 =  0.12976E+01; g0 =  0.29996E+04;
VEL 100sps WA2800        h0 =  0.78067E+00; f0 =  0.12884E+01; g0 =  0.29635E+04;
VEL 200sps WA2800        h0 =  0.79167E+00; f0 =  0.12695E+01; g0 =  0.28851E+04;

ACC 20spsWA2800          h0 =  0.67044E+00; f0 =  0.13636E+01; g0 =  0.32957E+04;
ACC 40spsWA2800          h0 =  0.75220E+00; f0 =  0.13331E+01; g0 =  0.31774E+04;
ACC 50spsWA2800          h0 =  0.76394E+00; f0 =  0.13194E+01; g0 =  0.31152E+04;
ACC 80sps WA2800         h0 =  0.77937E+00; f0 =  0.12958E+01; g0 =  0.30073E+04;
ACC 100sps WA2800        h0 =  0.78401E+00; f0 =  0.12873E+01; g0 =  0.29683E+04;
ACC 200sps WA2800        h0 =  0.79245E+00; f0 =  0.12691E+01; g0 =  0.28861E+04;

// like above but fit to input fmax = 7 Hz and fmaxOut = 10 Hz.  on 09/14/2007  - aww
VEL 20spsWA2800          h0 =  0.63382E+00; f0 =  0.14094E+01; g0 =  0.33662E+04;
VEL 40spsWA2800          h0 =  0.74306E+00; f0 =  0.13422E+01; g0 =  0.31859E+04;
VEL 50spsWA2800          h0 =  0.75820E+00; f0 =  0.13251E+01; g0 =  0.31203E+04;
VEL 80sps WA2800         h0 =  0.77721E+00; f0 =  0.12980E+01; g0 =  0.30093E+04;
VEL 100sps WA2800        h0 =  0.78262E+00; f0 =  0.12886E+01; g0 =  0.29694E+04;
VEL 200sps WA2800        h0 =  0.79215E+00; f0 =  0.12695E+01; g0 =  0.28865E+04;

ACC 20spsWA2800          h0 =  0.68405E+00; f0 =  0.13829E+01; g0 =  0.34011E+04;
ACC 40spsWA2800          h0 =  0.75422E+00; f0 =  0.13357E+01; g0 =  0.31913E+04;
ACC 50spsWA2800          h0 =  0.76517E+00; f0 =  0.13210E+01; g0 =  0.31235E+04;
ACC 80sps WA2800         h0 =  0.77980E+00; f0 =  0.12963E+01; g0 =  0.30101E+04;
ACC 100sps WA2800        h0 =  0.78427E+00; f0 =  0.12876E+01; g0 =  0.29700E+04;
ACC 200sps WA2800        h0 =  0.79251E+00; f0 =  0.12692E+01; g0 =  0.28865E+04;


// 250, 500 sps Added 05/14/2012 -aww 

ACC fmax=7 250sps WA2800   h0 =  0.79409E+00; f0 =  0.12654E+01; g0 =  0.28695E+04;
VEL fmax=7 250sps WA2800   h0 =  0.79384E+00; f0 =  0.12656E+01; g0 =  0.28695E+04;

ACC fmax=7 500sps WA2800   h0 =  0.79711E+00; f0 =  0.12578E+01; g0 =  0.28349E+04;
VEL fmax=7 500sps WA2800   h0 =  0.79702E+00; f0 =  0.12578E+01; g0 =  0.28349E+04;

*/
  protected boolean setConstants(double sampleInterval, boolean isVelocity) {

    // Filter fits response best if sps freq < .5*Nyquist freq
    // so for 80 sps that's 20 Hz, for 40 sps, 10 Hz, and 20 sps, 5 Hz
    setSampleRate(1.0f/sampleInterval);

    // check for valid sample rate
    if (!sampleRateIsLegal(getSampleRate()))  return false;

    // defaults : "vsp with delta-t = 0.01 sec" = short period velocity
    // h0   = 0.781; f0  = 1.29; g0 = 2963.0;
    h0 =  0.78262E+00; f0 =  0.12886E+01; g0 =  0.29694E+04; // 7 Hz

    if (isVelocity) {
      if (getSampleRate() == 100) {
        // Velocity : "vsp with delta-t = 0.01 sec" = short period velocity
        //h0   = 0.781; f0  = 1.29; g0 = 2963.0; // from paper, like 10 Hz
        //h0 =  0.78067E+00; f0 =  0.12884E+01; g0 =  0.29635E+04; // 10 Hz
        h0 =  0.78262E+00; f0 =  0.12886E+01; g0 =  0.29694E+04; // 7 Hz

      } else if (getSampleRate() == 200) { // NEW added 09/14/2007 aww 
        //h0 =  0.79245E+00; f0 =  0.12691E+01; g0 =  0.28861E+04; // 10 Hz
        h0 =  0.79251E+00; f0 =  0.12692E+01; g0 =  0.28865E+04; // 7 Hz

      } else if (getSampleRate() == 80) { // NEW added 09/14/2007 aww
        //h0 =  0.77412E+00; f0 =  0.12976E+01; g0 =  0.29996E+04; // 10 Hz
        h0 =  0.77721E+00; f0 =  0.12980E+01; g0 =  0.30093E+04; // 7 Hz

      } else if (getSampleRate() == 40) {
        //h0   = 0.743; f0  = 1.342; g0 = 3186.0; //  from Hiroo 08/18/06, like 7 HZ value
        //h0 =  0.72958E+00; f0 =  0.13398E+01; g0 =  0.31401E+04; // 10 Hz
        h0 =  0.74306E+00; f0 =  0.13422E+01; g0 =  0.31859E+04; // 7 Hz

      } else if (getSampleRate() == 50) { // NEW added 09/14/2007 aww
        //h0 =  0.74985E+00; f0 =  0.13238E+01; g0 =  0.30929E+04; // 10 Hz
        h0 =  0.75820E+00; f0 =  0.13251E+01; g0 =  0.31203E+04; // 7 Hz

      } else if (getSampleRate() == 20) {
        // Velocity : "vbb with delta-t = 0.05 sec" = broad-band velocity
        //h0   = 0.568; f0  = 1.39; g0 = 3110.0; // from paper, like 10 Hz
        //h0 =  0.57034E+00; f0 =  0.13924E+01; g0 =  0.31175E+04; // 10 Hz
        h0 =  0.63382E+00; f0 =  0.14094E+01; g0 =  0.33662E+04; // 7 Hz

      } else if (getSampleRate() == 250) {
         h0 =  0.79384E+00; f0 =  0.12656E+01; g0 =  0.28695E+04; // VEL fmax=7 250sps WA2800

      } else if (getSampleRate() == 500) {
         h0 =  0.79702E+00; f0 =  0.12578E+01; g0 =  0.28349E+04; // VEL fmax=7 500sps WA2800
      }

    } else { // accelerometer values
      if (getSampleRate() == 100) {
        // Acceleration : "lg with delta-t = 0.01 sec" = "low gain" acceleration
        //h0   = 0.781; f0  = 1.29; g0 = 2963.0; // from paper, like 10 Hz?
        //h0 =  0.78401E+00; f0 =  0.12873E+01; g0 =  0.29683E+04; // 10 Hz
        h0 =  0.78427E+00; f0 =  0.12876E+01; g0 =  0.29700E+04; // 7 Hz

      } else if (getSampleRate() == 200) {
        //BUG ? g0 in line below appears to be 2080 value for 10Hz not the 2800 value 09/14/2007
        //h0 =  0.793; f0  = 1.27; g0 = 2144.0; // from Hiroo 9/1/04
        //h0 =  0.79245E+00; f0 =  0.12691E+01; g0 =  0.28861E+04; // 10 Hz
        h0 =  0.79251E+00; f0 =  0.12692E+01; g0 =  0.28865E+04; // 7 Hz

      } else if (getSampleRate() == 40) {
        //h0   = 0.754; f0  = 1.336; g0 = 3191.0; // from Hiroo 08/18/06, like 7 HZ 
        //h0 =  0.75220E+00; f0 =  0.13331E+01; g0 =  0.31774E+04; // 10 Hz
        h0 =  0.75422E+00; f0 =  0.13357E+01; g0 =  0.31913E+04; // 7 Hz

      } else if (getSampleRate() == 50) { // NEW added 09/14/2007 aww
        //h0 =  0.76394E+00; f0 =  0.13194E+01; g0 =  0.31152E+04; // 10 Hz
        h0 =  0.76517E+00; f0 =  0.13210E+01; g0 =  0.31235E+04; // 7 Hz

      } else if (getSampleRate() == 80) {
        //h0   = 0.781;  f0  = 1.29; g0 =  2963.0; // from paper
        //h0   = 0.774;  f0  = 1.30; g0 =  2999.0; // from RT code
        //h0 =  0.77937E+00; f0 =  0.12958E+01; g0 =  0.30073E+04; // 10 Hz
        h0 =  0.77980E+00; f0 =  0.12963E+01; g0 =  0.30101E+04; // 7 Hz

      } else if (getSampleRate() == 20) { // NEW added 09/14/2007 aww
        //h0 =  0.67044E+00; f0 =  0.13636E+01; g0 =  0.32957E+04; // 10 Hz
        h0 =  0.68405E+00; f0 =  0.13829E+01; g0 =  0.34011E+04; // 7 Hz

      } else if (getSampleRate() == 250) { // NEW added 09/14/2007 aww
        h0 =  0.79409E+00; f0 =  0.12654E+01; g0 =  0.28695E+04; // ACC fmax=7 250sps WA2800

      } else if (getSampleRate() == 500) { // NEW added 09/14/2007 aww
        h0 =  0.79711E+00; f0 =  0.12578E+01; g0 =  0.28349E+04; // ACC fmax=7 500sps WA2800
      }
    }

    calcC1C2();

    return true;
  }

  protected void calcC1C2 () {
    // natural freq. should = 2(pi)/0.8 sec^-1 OR 2(pi) * 1.25
    // the value of the f0 is the "adjusted" value to correct for
    // the recursive filter  natural angular frequency
    double wdt = 2.0 * Math.PI * f0 * getSampleInterval();
    c1 = 1.0 + (h0 * wdt);                       // equation (4)
    c2 = 1.0 + (2.0 * h0 * wdt) + (wdt*wdt);     // equation (5)
  }

  /// Override tests for acceleration or velocity to reject input
    public Waveform setWaveform(Waveform wf) {
        wfIn = super.setWaveform(wf);
        if (wfIn == null) {
           isAcc = false; 
           isVel = false;
           System.err.println("Filter: "+getDescription()+" setWaveform input waveform null, channel not velocity or acceleration");
        }
        else {
           DateTime dt = new DateTime(wfIn.getEpochStart(), true);
           gain = wfIn.getChannelObj().getGain(dt, doLookUp); // added static flag, false => no db query, use only cached values  -aww 2010/08/19
           isAcc = gain.isAcceleration(); 
           isVel = gain.isVelocity(); 
           if (!isVel && !isAcc) {
               System.err.println("Filter: "+getDescription()+" Error Channel is not velocity or acceleration: " +
                      wfIn.getChannelObj().toDelimitedSeedNameString() + " gain: " + gain );
               wfIn = null;
               gain = null;
           }
        }
        return wfIn;
    }
//
// WHERE THE FILTERING ACTUALLY GETS DONE
//
/** Return recursively filtered timeseries given a timeseries.
 * If there are fewer than 2 points in the array the original array is returned.
 * The orginal is unchanged.
 *
 * Based on Hiroo's equation (3)
 * */
  protected float[] filterTimeSeries(float[] ts) {

      if (ts.length < 2) return ts;

      if (getApplyTaper()) ts = AmplitudeCosineTaper.taper(ts, cosineTaperAlpha);

      float wa[] = new float[ts.length];

      double dt = getSampleInterval();
      double c1x2 = 2.0 * c1;
  
      // ACCELERATION
      if (wfIn != null && wfIn.getAmpUnits() == Units.COUNTS && isAcc) {

        double b0gwa = g0 * (2.0 / (1 + qHighPass));
//        double a2 = 0.0, a3 = 0.0;
        double gwa_dt2 = g0 * dt * dt;                            // accel

        // convert to real ground motion: cm/sec^2
        float acc[] = convertToGroundMotion(ts);

        for (int i = 2; i < ts.length -3; i++) {
//          a3 = (ts[i]-ts[i-1])/ b0gwa + qHighPass * a2;   // convert accel cnt's to grnd mo + filter
          wa[i] = (float) (( acc[i] * gwa_dt2 + c1x2 * wa[i-1] - wa[i-2] ) / c2);
//          a2=a3;
        }
    // VELOCITY
      } else if (wfIn != null && wfIn.getAmpUnits() == Units.COUNTS && isVel ) {

          double mult = (g0/getGainFactor()) * dt ;              // velocity

          // apply the filter - note use of wa[i-2] means we start at i=2
          for (int i = 2; i < ts.length -3; i++) {
            wa[i] = (float) (( (ts[i]-ts[i-1]) * mult + (c1x2 * wa[i-1] - wa[i-2] )) / c2);
          }

      } else if (wfIn == null) {
          System.err.println("WAFilter: filterTimeSeries input waveform NULL"); 
          return ts;
      } else {
          System.err.println("WAFilter: illegal waveform type : "  + wfIn.getChannelObj().toDelimitedSeedNameString() +
              " Amp Units: " + wfIn.getAmpUnits() + " == " + Units.COUNTS + " gain: " +
               wfIn.getChannelObj().getGain(new DateTime(wfIn.getEpochStart(), true), doLookUp) + " isAcc: " + isAcc + " isVel: " + isVel);
          return ts;
      }

      /* Apply gain factor difference if necessary. Can't do it in the loop
      * above because that screws up the filter, which looks back in the time series.
      * (DDG 5/8/03)
      * All the constants Hiroo calculated for this were done assuming 2800
      * so this 'correction' must be applied. Actually changed to use 2080
      * from 2800 on 9/15/03 */
      if (getWAmagnification() == WA_MAGNIFICATION_2080) {   // else must be 2800
          for (int i = 0; i < ts.length; i++) {
            wa[i] *= fac2080_2800;
          }
      }

      return wa;

  }
  
  public Object clone() {
      WAFilter f = null;
      try {
          f = (WAFilter) super.clone();
          if (noiseFilter != null) f.noiseFilter = (ButterworthFilterSMC) noiseFilter.clone(); // uses current type settings
      }
      catch (CloneNotSupportedException ex) { ex.printStackTrace();}
      return f;
  }

} // end of class
