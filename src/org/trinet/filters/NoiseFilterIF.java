package org.trinet.filters;
import org.trinet.util.WaveformBandpassFilterIF;
public interface NoiseFilterIF {
  public WaveformBandpassFilterIF getNoiseFilter();
} 
