package org.trinet.filters;

import java.util.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import org.trinet.jasi.*;
import org.trinet.util.*;

/** Recursive filter to acceleration.
 * If the input timeseries is VELOCITY in units of counts with a gain factor it will be transformed
 * to cm/sec then to acceleration. If input is already an ACCELERATION it will be
 * high pass filtered and the bias will be removed.
 * Returns acceleration in cm/sec^2. Only intended for use on 20, 80, and 100 sps data.<p>
 *
 * Implimentation of the technique
 * developed by Hiroo Kanamori described in "Continuous Monitoring of Ground-Motion
 * Parameters", BSSA, V89, pp. 311-316, Feb. 1999 as implemented in Hiroo Kanimori's
 * FORTRAN subroutine recres6().
 *
 * @see: AbstractWaveformFilter()
 * */
public class AccelFilter extends AbstractWaveformFilter {

  public static final String FILTER_NAME = "Acceleration";
  protected static boolean DEFAULT_APPLY_TAPER = true;

  public AccelFilter() {
    this(FilterTypes.ACCELERATION, 0);
  }

  public AccelFilter(int sampleRate) {
    this(FilterTypes.ACCELERATION, sampleRate);
  }

  public AccelFilter(int type, int sampleRate) {
    setDescription(AccelFilter.FILTER_NAME);
    setApplyTaper(AccelFilter.DEFAULT_APPLY_TAPER);
    setType(type);
    setSampleRate(sampleRate);
  }

/** Returns this same segment with the time-series Filtered.*/
  protected WFSegment filterSegment (WFSegment seg) {

    setWFSegment(seg);

    // Remove bias: note 'scanIt' is 'false' because we don't yet want to recalc bias, max/min, etc.
    seg.setTimeSeries( demean(seg.getTimeSeries()), false );

    // filter time series
    if ( seg.getChannelObj().isVelocity()
         && wfIn != null && wfIn.getAmpUnits() == Units.COUNTS) {

      seg.setTimeSeries( velRawToAccel(seg.getTimeSeries()) );
      seg.setIsFiltered(true);       // flag segment as filtered

    } else if ( seg.getChannelObj().isAcceleration()
                && wfIn != null && wfIn.getAmpUnits() == Units.COUNTS )  {

      seg.setTimeSeries( convertToGroundMotion(seg.getTimeSeries()) );
      seg.setIsFiltered(true);       // flag segment as filtered

    } else {
      System.err.println("AccelFilter: illegal input waveform, check gain units for : " + seg.getChannelObj().toDelimitedSeedNameString());
      seg.setIsFiltered(false);     // flag segment as unfiltered
    }

    return seg;
  }

  /** Set the units for the resulting (filtered) waveform. */
  public void setUnits(Waveform wf) {
    wf.setAmpUnits(Units.CMSS);         // accel = cm/sec^2
  }

  /** Return true if the input amplitude units type (of a waveform) are legal for this filter. */
  public boolean unitsAreLegal(int units) {
    return (units == Units.COUNTS);
  }

  /** Returns true if the given sample rate is legal for this filter.
   * Overrides parent method because ALL rates are legal for acceleration filter. */
  protected boolean sampleRateIsLegal (double rate) {
    return true ;
  }

/** Return acceleration by recursively differentiating a velocity timeseries.
 * Input timeseries must be VELOCITY in units of counts.
 * Returns acceleration in cm/sec^2.<p>
 *
 * Does not apply a High Pass filter
 *
 * Based on Hiroo Kanimori's FORTRAN subroutine recres6() and equation 1' in the paper.
 *
 * */
  private float[] velRawToAccel (float[] ts) {

    if (ts.length < 3) return ts;

    // Hiroo's magic numbers from equation (1')
    // for "recusive differentiator" from velocity to acceleration
    final double c0 = -0.7721187782;
    final double c1 = -0.3788779578;
    final double c2 =  1.1509967359;

    final double dd0 =  0.102101057;
    final double dd1 = -0.8593897;

    float acc[] = new float[ts.length];

    double dt   = getSampleInterval();
    double dtgf = dt * getGainFactor();

    // set first sample value
    acc[0] = 0;
    acc[1] = 0;

    if (getApplyTaper()) ts = AmplitudeCosineTaper.taper(ts, cosineTaperAlpha);

    for (int i = 2; i < ts.length -3; i++) {

      //This is the more complex (and accurate) equation 1' version
      acc[i] =(float) (
      (c2*ts[i] + c1*ts[i-1] + c0*ts[i-2]) /
       dtgf + (dd1*acc[i-1])+(dd0*acc[i-2])
      );
    }

    return acc;

  }

}
