// TODO: abstract generic behavior and split apart graphics as override implementation
//need to isolate gui dependence here, perhaps by notification of listeners via event?
//perhaps no graphics owner, or headless graphics
package org.trinet.mung;
import java.awt.*;
import java.awt.event.*;
import java.beans.*;
import java.sql.*;
import java.util.*;
import javax.swing.*;
import org.trinet.jasi.*;
import org.trinet.jasi.coda.*;
import org.trinet.jasi.engines.*;
import org.trinet.jasi.magmethods.*;
import org.trinet.jdbc.*;
import org.trinet.jiggle.WorkerStatusDialog;
import org.trinet.util.*;
import org.trinet.util.gazetteer.*;
import org.trinet.util.locationengines.*;
import org.trinet.util.graphics.*;

public final class MungHypoMagEngineDelegate extends DefaultHypoMagEngineDelegate
  implements GraphicalHypoMagEngineDelegateIF, ItemListener {

    static private AbstractAction propAction;

    public MungHypoMagEngineDelegate() { }

    public MungHypoMagEngineDelegate(String propFileName) {
      setDelegateProperties(propFileName);
    }
    public MungHypoMagEngineDelegate(GenericPropertyList props) {
      setDelegateProperties(props);
    }

    // implementation of abstract method in super class
    protected void reportMessage(String titleType, String message) {
        super.reportMessage(titleType, message);
        InfoDialog.informUser(null, titleType, message, null); // remove if iterative call annoying
    }

    public boolean calcMagFromWaveforms(Solution aSol, String magType) {
        solveSuccess = false;
        assocSol = aSol;
        if (aSol == null) return false;

        // Get prior preferred magnitude of type, if any
        assocMag = aSol.getPrefMagOfType(magType.toLowerCase());
        // Perhaps missing a prefmag table, so we need to check event preferred for type
        if (assocMag == null) assocMag = aSol.getPreferredMagnitude(); // pre-EVENPREFMAG table? -aww

        if (! resolveLocation(aSol)) return false;

        MagnitudeEngineIF magEng = initMagEngineForType(aSol, magType);
        if (magEng == null || ! magEng.isEngineValid()) {
          statusString = "EngineDelegate can't calculate a magnitude of type: " + magType;
          reportStatusMessage("ERROR");
          return false;
        }

        if (assocMag != null && assocMag.getTypeString().equalsIgnoreCase(magType)) {
          logMagnitudeSummary("Prior Magnitude:", true, "<OLD>");
        }

        needsMagnitudeSolveUpdate = true;

        new MagEngSwingWorker(magEng, assocSol).start();  // do calculation in new thread

        return false; // false by default, solve status not known until thread completes
    }

// Action
    private JMenu createActionMenu() {
        JMenu menu = new JMenu();  // new JPopupMenu();
        menu.addSeparator();
        JCheckBoxMenuItem cbMenuItem =
          new JCheckBoxMenuItem("print MagEng ReadingList");
        cbMenuItem.setActionCommand("printMagEngReadingList");
        cbMenuItem.setSelected(myProps.getBoolean("printMagEngReadingList"));
        cbMenuItem.addItemListener(this);
        menu.add(cbMenuItem);
        cbMenuItem =
          new JCheckBoxMenuItem("print LocEngine phases");
        cbMenuItem.setActionCommand("printLocEngPhaseList");
        cbMenuItem.setSelected(myProps.getBoolean("printLocEngPhaseList"));
        cbMenuItem.addItemListener(this);
        menu.add(cbMenuItem);
        cbMenuItem =
          new JCheckBoxMenuItem("print Solution phases");
        cbMenuItem.setActionCommand("printSolutionPhaseList");
        cbMenuItem.setSelected(myProps.getBoolean("printSolutionPhaseList"));
        cbMenuItem.addItemListener(this);
        menu.add(cbMenuItem);
        cbMenuItem =
          new JCheckBoxMenuItem("print Solutions");
        cbMenuItem.setActionCommand("printSolutions");
        cbMenuItem.setSelected(myProps.getBoolean("printSolutions"));
        cbMenuItem.addItemListener(this);
        menu.add(cbMenuItem);
        cbMenuItem =
          new JCheckBoxMenuItem("print Magnitudes");
        cbMenuItem.setActionCommand("printMagnitudes");
        cbMenuItem.setSelected(myProps.getBoolean("printMagnitudes"));
        cbMenuItem.addItemListener(this);
        menu.add(cbMenuItem);
        //
        menu.addSeparator();
        //
        final JCheckBoxMenuItem magEngVerboseCbMenuItem =
          new JCheckBoxMenuItem("verbose magEng");
        magEngVerboseCbMenuItem.setActionCommand("verboseMagEng");
        magEngVerboseCbMenuItem.setSelected(verbose);
        magEngVerboseCbMenuItem.addItemListener(
          new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
              verbose = (e.getStateChange() == ItemEvent.SELECTED);
              setVerboseOfMagnitudeEngine(verbose);
            }
          }
        );
        menu.add(magEngVerboseCbMenuItem);
        //
        final JCheckBoxMenuItem locEngVerboseCbMenuItem =
          new JCheckBoxMenuItem("verbose locEng");
        locEngVerboseCbMenuItem.setActionCommand("verboseLocEng");
        locEngVerboseCbMenuItem.setSelected(verbose);
        locEngVerboseCbMenuItem.addItemListener(
          new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
              verbose = (e.getStateChange() == ItemEvent.SELECTED);
              setVerboseOfLocationEngine(verbose);
            }
          }
        );
        menu.add(locEngVerboseCbMenuItem);
        //
        cbMenuItem =
          new JCheckBoxMenuItem("debug magEng");
        cbMenuItem.setActionCommand("debugMagEng");
        cbMenuItem.setSelected(debug);
        cbMenuItem.addItemListener(
          new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
              debug = (e.getStateChange() == ItemEvent.SELECTED);
              setDebugOfMagnitudeEngine(debug);
              if (debug) magEngVerboseCbMenuItem.setSelected(true);
            }
          }
        );
        menu.add(cbMenuItem);
        //
        cbMenuItem =
          new JCheckBoxMenuItem("debug locEng");
        cbMenuItem.setActionCommand("debugLocEng");
        cbMenuItem.setSelected(debug);
        cbMenuItem.addItemListener(
          new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
              debug = (e.getStateChange() == ItemEvent.SELECTED);
              setDebugOfLocationEngine(debug);
              if (debug) locEngVerboseCbMenuItem.setSelected(true);
            }
          }
        );
        menu.add(cbMenuItem);
        //
        menu.addSeparator();
        menu.add("No change");
        //
        return menu;
    }
    public Action createEnginePropertyAction() {
        if (propAction == null) {
          final JMenu menu = createActionMenu();
          propAction = new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
              Component aComp = (Component) e.getSource();
              menu.getPopupMenu().show(aComp, aComp.getWidth()/2, aComp.getHeight()/2);
            }

          };
          propAction.putValue(AbstractAction.NAME, "EngineProps");
          propAction.putValue(AbstractAction.SHORT_DESCRIPTION, "Configure solution engine properties.");
        }
        return propAction;
    }
    public void itemStateChanged(ItemEvent e) {
        JCheckBoxMenuItem source = (JCheckBoxMenuItem) e.getSource();
        boolean selected = (e.getStateChange() == ItemEvent.SELECTED);
        String cmd = source.getActionCommand();
        myProps.setProperty(cmd, String.valueOf(selected));
    }

    private class MagEngSwingWorker extends org.trinet.util.SwingWorker {

        final MagnitudeEngineIF magEng;
        final Solution sol;
        final WorkerStatusDialog workStatus = new WorkerStatusDialog(null, true);

        public MagEngSwingWorker(MagnitudeEngineIF magEng, Solution sol) {
          super("MagEngSwingWorkerWFLoad");
          this.magEng = magEng;
          this.sol = sol;
        }

        // NO GRAPHICS CALLS, HERE UNLESS by SwingUtilities.invokeLater(...)
        public Object construct() {
          workStatus.setBeep(WorkerStatusDialog.BEEP_ON);
          // Create a popup for the duration of the run, a progress object would be better
          StringBuffer sb = new StringBuffer(132);
          sb.append("Calculating ").append("M").append(magEng.getMagMethod().getMagnitudeTypeString());
          sb.append(" for event ").append(sol.getId()).append("\n");
          workStatus.pop("Magnitude", sb.toString(), true);
          //
          // Blow away the existing reading data associated with the solution,
          // otherwise repeated calls will accumulate copies of the same data!
          // Could instead try to do list additions by invoking addOrReplace - aww
          // sol.getListFor(magEng.getMagMethod().getReadingClass()).clear(); // let engine clear it ? 
          //
          if (debug) System.out.println("DEBUG Mung mag calc type: "+ magEng.getMagMethod().getMethodName()
                                 +" from waveforms for: "+sol.toString());
          java.util.List wfList = magEng.getSolutionWaveformList(sol);
          Magnitude newMag = (magEng.solveFromWaveforms(sol, wfList)) ? magEng.getSummaryMagnitude() : null;
          return null;
        }

        //Runs on the event-dispatching thread, so graphics calls OK.
        public void finished() {
          workStatus.unpop();
          reportEngineCalculationStatus(magEng);
          solveSuccess = magEng.success();
          if (solveSuccess) assocMag = magEng.getSummaryMagnitude(); // get summary magnitude and reassign here aww
        }

    } //end of MagEngSwingWorker class
}
