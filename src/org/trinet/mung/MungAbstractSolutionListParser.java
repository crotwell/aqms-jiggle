package org.trinet.mung;
import org.trinet.util.graphics.table.*;
import java.io.*;
import java.util.*;
import org.trinet.jasi.*;
import org.trinet.util.*;

abstract public class MungAbstractSolutionListParser extends AbstractSolutionListParser {
  protected AbstractJasiAssociationTableModel jmltm;
  protected AbstractJasiAssociationTableModel jpltm;
  protected AbstractJasiMagAssocTableModel jaltm;
  protected AbstractJasiMagAssocTableModel jcltm;

  public MungAbstractSolutionListParser() {
    initParser();
  }

  public MungAbstractSolutionListParser(GenericPropertyList gpl) {
    super(gpl);
    initParser();
  }

  abstract protected void initParser();

  protected int getDataType(String s) {
    if (s == null || s.trim().length() == 0) return UNKNOWN_TYPE;
    return getDataType(s.charAt(0));
  }
  protected int getDataType(char c) {
    char type = c;
    if (Character.toUpperCase(type) == 'P') {
      return PHASE_TYPE;
    }
    else if ( Character.toUpperCase(type) == 'A') {
      return AMP_TYPE;
    }
    else if ( Character.toUpperCase(type) == 'M') {
      return MAG_TYPE;
    }
    else if ( Character.toUpperCase(type) == 'S') {
      return SOL_TYPE;
    }
    else if ( Character.toUpperCase(type) == 'C') {
      return CODA_TYPE;
    }
    else {
      return UNKNOWN_TYPE;
    }
  }
  protected boolean isDataTypeDelimiter(String s) {
    return ( (s.length() == 1) && (UNKNOWN_TYPE != getDataType(s)) );
  }
  protected String removeDataTypeDelimiter(String s) {
    return s.substring(1);
  }
  protected String getDataTypeDelimiter(String s) {
    return s.substring(0,1);
  }
  public Solution parseSolution(BufferedReader bfReader)
    throws IOException {
      Solution sol = (Solution) parseRow(bfReader, solParser, SOL_TYPE);
      if (sol == null) return sol;

      if (! atEOD) {   // get phases, if any
        jpltm.setAssocSolution(sol);
        parsePhaseList(bfReader, sol);
      }
      if (! atEOD) {   // get sol amps assoc with new local mag, in any
        jaltm.setAssocSolution(sol);
        parseAmpList(bfReader, sol);
      }
      if (! atEOD) {  // get mags
        jmltm.setAssocSolution(sol);
        parseMagList(bfReader, sol);
      }
      setAmpTime(sol);
      return sol;
  }
  protected AmpList parseAmpList(BufferedReader bfReader, Magnitude mag) throws IOException {
    //jaltm.setAssocSolution(mag.getAssociatedSolution());
    jaltm.setAssocMagnitude(mag); // 03/03 does both sol and mag aww
    return super.parseAmpList(bfReader,mag);
  }

  private class AmplitudeData {
    Amplitude amp = null;
    boolean haveS = false;
    boolean haveP = false;
  }

  protected void setAmpTime(Solution aSol) {
      AmpList ampList = aSol.getAmpList();
      int ampSize = ampList.size();
      if (ampSize == 0) return;
      PhaseList phaseList = aSol.getPhaseList();
      int phaseSize = phaseList.size();
      if (phaseSize == 0) return;

      ArrayList dataList = new ArrayList(ampSize);
      for (int idx=0; idx < ampSize; idx++) {
          AmplitudeData ampData = new AmplitudeData();
          ampData.amp = (Amplitude) ampList.get(idx);
          dataList.add(ampData);
      }
      for (int idx=0; idx < ampSize; idx++) {
          AmplitudeData ampData = (AmplitudeData) dataList.get(idx);
          Channel chan = ampData.amp.getChannelObj();
          for (int jdx = 0; jdx < phaseSize; jdx++) {
            Phase aPhase = (Phase) phaseList.get(jdx);
            if (aPhase.getChannelObj().getChannelName().sameStationAs(chan.getChannelName())) {
              if (aPhase.description.iphase.equals("P")) {
                if (! ampData.haveP) {
                  double wTime = ampData.amp.windowStart.doubleValue();
                  wTime += 1.73 *(aPhase.getTime() - wTime);
                  ampData.amp.setTime(wTime);
                  ampData.haveP = true;
                }
              }
              else if (aPhase.description.iphase.equals("S")) {
                if (! ampData.haveS) {
                  ampData.amp.setTime(aPhase.getTime());
                  ampData.haveS = true;
                }
              }
            }
          }
      }
  }
}
