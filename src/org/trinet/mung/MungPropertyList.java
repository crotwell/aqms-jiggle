package org.trinet.mung;

import java.util.*;

import org.trinet.jasi.*;
import org.trinet.jasi.engines.HypoMagDelegatePropertyList;

/*
* Mung program properties. See PropertyList Class for more info.
* Environment variables should be defined by system property mechanism when invoking java runtime
* with the (-D<system_variable_name>=<value> switch.
* "MUNG_HOME" variable which defines the root directory of the application's default properties file.
* A "MUNG_USER_HOMEDIR" variable which defines the root directory path for a subdirectory named
* ".mung" where the user's properties file should be located.
*/
public class MungPropertyList extends HypoMagDelegatePropertyList {

    /**
     *  Constructor: reads no property file empty list.
     */
    public MungPropertyList() { }

    /**
     *  Constructor: Makes a COPY of a property list. Doesn't reread the properties files,
     * assumes that was already done at instantiation of passed MungPropertyList object.
     * @see: MungPropertyList(String fileName)
     */
    public MungPropertyList(MungPropertyList props) {
        super(props);
        privateSetup();
    }

    /**
     *  Constructor: input file is a name relative to the default user and system paths
     *  which are defined by System properties appending a default subdirectory of type ".mung".
     */
    public MungPropertyList(String fileName) {
        super(fileName);  // does a reset()
        privateSetup();
    }

    /**
    *  Constructor: input filenames are not appended to constructed paths, but rather are
    *  interpreted as specified (either relative to current working directory or a complete path). 
    */
    public MungPropertyList(String userPropFileName, String defaultPropFileName) {
        super(userPropFileName, defaultPropFileName);
        privateSetup();
    }

    public String getFiletype() {
        return (filetype == "") ? "mung" : filetype;  // property user home subdir type
    }

    /**
     * Setup class static, instance, and/or environment variables from current properties.
     * This method should be called after the pertinent properties are initially read
     * or changed through updates.
     */
    public boolean setup() {
      boolean status = super.setup();
      return (privateSetup() && status);
    }

    private boolean privateSetup() {

      boolean status = true;

      /* setup PhaseDescriptor 1st motion resolution based on properties
      if (isSpecified("firstMoQualityMin"))
        PhaseDescription.setFmQualCut(getDouble("firstMoQualityMin"));
      if (isSpecified("firstMoOnS"))
        PhaseDescription.setFmOnS(getBoolean("firstMoOnS"));
      if (isSpecified("firstMoOnHoriz"))
        PhaseDescription.setFmOnHoriz(getBoolean("firstMoOnHoriz"));
      */

      // test for existance of dynamically loaded classes defined by input properties
      status = checkForRuntimeClassOf("WhereIsEngine");   // if none defaults

      status = checkForRuntimeClassOf("LocationEngine");
      if (!status) System.err.println("ERROR: Mung engine class not defined for property: LocationEngine.");

      status = checkForRuntimeClassOf("magEngine");
      if (!status) System.err.println("ERROR: Mung engine class not defined by property: magEngine.");

      return status;
    }

    /** Return a List of property names used
     * by application classes utilizing this class.
     * */
    public List getKnownPropertyNames() {
        List aList = super.getKnownPropertyNames();
        List myList = classDeclaredPropertyNames();
        if (aList instanceof ArrayList)
            ((ArrayList)aList).ensureCapacity(aList.size() + myList.size());
        aList.addAll(myList);
        Collections.sort(aList);
        return aList;
    }

    public static List classDeclaredPropertyNames() {
        ArrayList myList = new ArrayList(0);
        //
        //Just use the defaults for firstMo:
        //
        //myList.add("firstMoOnHoriz");
        //myList.add("firstMoQualityMin");
        //myList.add("firstMoOnS");
        //
        // BELOW moved to hypomag delegate properties:
        //
        //myList.add("locationEngine");
        //myList.add("locationEngineAddress");
        //myList.add("locationEnginePort");
        //myList.add("locationEngineTimeoutMillis");
        //myList.add("magEngine");
        //myList.add("mcMagEngineProps");
        //myList.add("mcMagMethod");
        //myList.add("mcMagMethodProps");
        //myList.add("mdMagEngineProps");
        //myList.add("mdMagMethod");
        //myList.add("mdMagMethodProps");
        //myList.add("mlMagEngineProps");
        //myList.add("mlMagMethod");
        //myList.add("mlMagMethodProps");
        //myList.add("mhMagEngineProps");
        //myList.add("mhMagMethod");
        //myList.add("mhMagMethodProps");
        //myList.add("mcDisable");
        //myList.add("mdDisable");
        //myList.add("mlDisable");
        //myList.add("mhDisable");
        //myList.add("printLocEngPhaseList");
        //myList.add("printMagEngReadingList");
        //myList.add("useTrialLocation");
        myList.add("WhereIsEngine");
        myList.add("autoSolve");
        myList.add("customSolutionListParserName");
        myList.add("dateRangeYearsBack");
        myList.add("dateRangeYearsFuture");
        myList.add("defaultChannelSrc");
        myList.add("defaultDataSrc");
        myList.add("defaultDataAuth");
        myList.add("defaultChannelLocation");
        myList.add("dummyStaCode");
        myList.add("dummySeedchanCode");
        myList.add("packWindowOnSolutionLoad"); // repack Mung GUI after SolutionList DateRange dialog load
        return myList;
    }

} // end of MungPropertyList


