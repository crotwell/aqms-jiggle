package org.trinet.formats;

import java.io.*;
import org.trinet.jasi.*;
import org.trinet.util.*;
/**
Read and parse the CISN Ad Hoc station list format into jasi Channel objects.
Note this class does NOT read the 5th column which represents the Cosmos
Table 6. station type code or the long channel name. Neither exist in Channel. <p>

<pre>
STAT  NC CHN LC  SC  LATITUDE  LONGITUDE  ELEV STATIONNAME
1835  NP HNZ      4  38.44210 -122.60680   123 CA: Santa Rosa Firestation 7
57064 CE HNN      4  37.530   -121.919      93 Fremont - Mission San Jose
RFSB  BK HL1      6  37.91608 -122.33610  -118 Richmond Field Station
</pre>
*
*/

/*
STAT  NC CHN LC  SC  LATITUDE  LONGITUDE  ELEV STATIONNAME
1835  NP HNZ      4  38.44210 -122.60680   123 CA: Santa Rosa Firestation 7
1835  NP HNN      4  38.44210 -122.60680   123 CA: Santa Rosa Firestation 7
1835  NP HNE      4  38.44210 -122.60680   123 CA: Santa Rosa Firestation 7
AAR   NC VHZ      3  39.27593 -121.02697   911 Airport Road Site
AAS   NC VHZ      3  38.43014 -121.10959    31 Arroyo Seco
BBG   NC VHZ      3  36.57796 -121.03918  1065 Big Mountain
BBN   NC VHZ 03   3  36.50953 -121.07709   412 San Benito
BHR   NC VHZ      3  36.72747 -121.26654   175 Hodges Ranch
BJC   NC VHZ 02   3  36.54715 -121.39387   173 Johnson Canyon

PKD   BK HLE      4  35.94517 -120.54160   583 Bear Valley Ranch
PKD   BK HLN      4  35.94517 -120.54160   583 Bear Valley Ranch
PKD   BK HLZ      4  35.94517 -120.54160   583 Bear Valley Ranch
POTR  BK HLE     20  38.20263 -121.93535    20 Potrero Hills
POTR  BK HLN     20  38.20263 -121.93535    20 Potrero Hills
POTR  BK HLZ     20  38.20263 -121.93535    20 Potrero Hills
RFSB  BK HL1      6  37.91608 -122.33610  -118 Richmond Field Station

57950 CE HNU      4  37.469   -121.927       3 Fremont - Kato & Page
57950 CE HNZ      4  37.469   -121.927       3 Fremont - Kato & Page
57950 CE HNV      4  37.469   -121.927       3 Fremont - Kato & Page
57064 CE HNN      4  37.530   -121.919      93 Fremont - Mission San Jose
57064 CE HNZ      4  37.530   -121.919      93 Fremont - Mission San Jose
57064 CE HNE      4  37.530   -121.919      93 Fremont - Mission San Jose
45045 CE HN2      4  36.734   -119.486     118 Fresno - Centerville
45045 CE HNZ      4  36.734   -119.486     118 Fresno - Centerville
45045 CE HN3      4  36.734   -119.486     118 Fresno - Centerville
45010 CE HNN      4  36.813   -119.746     101 Fresno - State University

WTT   CI HLZ 01   4  33.94895 -118.25529     3 Watts
WVP   CI EHZ 01   4  35.94945 -117.81724  1435 Volcano Peak
WWP   CI EHZ 01   4  35.73556 -118.08806  1117 Walker Pass
XTL   CI EHZ 01   4  34.29597 -117.86223  1643 Crystal Lake
YEG   CI EHZ 01   4  35.43658 -119.96009   907 Yeguas Mtn
*/
public class AdHocFormat {

/** Min usable line length. Enough to get elevation but doesn't require (or read) long name.
 * Used to prevent parse errors. */
  protected static final int MIN_LINE_LENGTH = 46;

/** The file comment character "#". */
  protected static final String COMMENT_CHAR = "#";

  /*
  public static void main(String[] args) {
    String filename = "C:\\Documents and Settings\\doug\\My Documents\\Trinet\\StaInfo\\Ad Hoc Lists\\AllNets.adhoc";
    System.out.println("Reading "+filename+"...");
    BenchMark bm = new BenchMark();
    ChannelList list =  readInFile(filename);
    bm.print("Read in "+list.size());
    System.out.println(list.toString());

  }
  */

  /** Read in an Ad Hoc station file. */
    public static ChannelList readInFile(File file) {
      try {
        return readInFile(new FileReader(file));
      }
      catch (FileNotFoundException ex) {
        ex.printStackTrace(System.err);
        return null;
      }
  }

/** Read in an Ad Hoc station file with this name. */
  public static ChannelList readInFile(String filename) {
    try {
      return readInFile(new FileReader(filename));
    }
    catch (FileNotFoundException ex) {
      ex.printStackTrace(System.err);
      return null;
    }
  }

/** Read in an Ad Hoc station file from this Reader. */
  public static ChannelList readInFile(Reader reader) {

    ChannelList list = new ChannelList();
    Channelable chan ;

    try {
      BufferedReader in = new BufferedReader(reader);
      String instr;
      // Continue to read lines while there are still some left to read
      while ((instr = in.readLine()) != null) {
        if (!instr.startsWith(COMMENT_CHAR)){
         if (instr.length() > MIN_LINE_LENGTH) {   // skip comments
          chan = parse(instr);
//          if (chan != null) list.add(chan);    // checking dups was time consuming
          // list.add() checks for instanceof Channelable plus a MasterList
          if (chan != null) list.addWithoutCheck(chan);
         }
        }
      }

      reader.close();

    } catch (Exception ex) {

      System.err.println("* AdHocFormat Error: File input error");
      ex.printStackTrace(System.err);

    }
    return list;
  }
/** Parse one line of an Ad Hoc station file. Returns null if the line is invalid. */
  //          1         2         3         4         5         6
  //0123456789012345678901234567890123456789012345678901234567890123456789
  //RFSB  BK HL1      6  37.91608 -122.33610  -118 Richmond Field Station
  public static Channelable parse (String str) {

    double lat, lon, z;
    Channel ch = Channel.create();
    // REMEMBER: subtring() end-index is EXCLUSIVE
    try {
      ch.setNet (str.substring(6, 9).trim());
      ch.setSta(str.substring(0, 6).trim());
      ch.setChannel(str.substring(9, 12).trim());
      ch.setLocation(str.substring(13, 15));
      ch.setSeedchan(ch.getChannel());	// kludge

      lat = ParseString.getDouble(str, 21, 30);
      lon = ParseString.getDouble(str, 30, 40);
      z   = ParseString.getDouble(str, 40, 47) /1000.0;   // m -> km

      ch.getLatLonZ().set(lat, lon, z);
    }
    catch (Exception ex) {   // java.lang.StringIndexOutOfBoundsException: String index out of range
      System.err.println("//"+str+"//");
      ex.printStackTrace(System.err);
      return null;
    }
    ch.setDateRange(new DateRange("1900/01/01 00:00:00.000","3000/01/01 00:00:00.000","yyyy/MM/dd HH:mm:ss.SSS"));
    return ch;
  }

}
