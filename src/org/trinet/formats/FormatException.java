package org.trinet.formats;

/**
 * Exception to be thrown when a format is bad.
 * @author Doug Given
 * @version 1.0
 *
 * org.trinet.formats.DataFormatException
 */

public class FormatException extends Exception {

  protected int lineNo = -1;
  protected String line = null;

  public FormatException() {
  }
  public FormatException(String msg) {
    super(msg);
  }
  /** Descriptive message and line number of text containing incorrectly formatted data. */
  public FormatException(String msg, int lineNumber) {
    super(msg);
    lineNo = lineNumber;
  }
  /** Descriptive message and line of text containing incorrectly formatted data. */
  public FormatException(String msg, String line) {
    super(msg);
    this.line = line;
  }
  /** Descriptive message and line number and line of text containing incorrectly formatted data. */
  public FormatException(String msg, int lineNumber, String line) {
    super(msg);
    lineNo = lineNumber;
    this.line = line;
  }
  /** Descriptive message and if defined, the line number and/or line of text containing the incorrectly formatted data. */
  public String toString() {
      String s = super.toString();
      if (lineNo > -1) s += "\n Error at line no. "+lineNo;
      if (line != null) s += "\n"+line;
      return s;
    }

}
