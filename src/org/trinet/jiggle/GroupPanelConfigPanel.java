package org.trinet.jiggle;

import java.awt.*;
import javax.swing.*;
import org.trinet.util.graphics.ColumnLayout;
import org.trinet.util.graphics.IntegerChooser;
import javax.swing.border.TitledBorder;

import java.awt.event.*;

/** Allow user to specify look and feel of WFGroupPanel */
public class GroupPanelConfigPanel extends JPanel {
    JPanel jPanel1 = new JPanel();
    JPanel jPanel2 = new JPanel();
    JLabel jLabel1 = new JLabel();
    JComboBox traceCountComboBox = new JComboBox();

    JLabel jLabel2 = new JLabel();
    //JComboBox secComboBox = new JComboBox();
    ZoomScaleComboBox secComboBox = null;
    org.trinet.util.graphics.ColumnLayout vertLayout = new org.trinet.util.graphics.ColumnLayout();

    // list of values in the combobox, reserve the 0th entry for non-list values
    String countList[] = {"5", "10", "15", "20", "25", "30", "35","40"};
    String timeList[] = {
      "ALL",
      "1.0",
      "5.0",
      "10.0",
      "15.0",
      "20.0",
      "30.0",
      "45.0",
      "60.0",
      "90.0",
      "120.0",
      "180.0",
      "240.0",
      "300.0"};

    // initialize to default values
    int wfCount = 10;
    double secInView = 90.0;
    boolean allSecsFlag = false;


    public GroupPanelConfigPanel(int wfInView, double secs) {
       this();
       setChannelCount(wfInView);
       setTimeWindow(secs);
    }

    public GroupPanelConfigPanel() {
        try  {
            jbInit();
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    public int getChannelCount() {
        String str = (String) traceCountComboBox.getSelectedItem();
        int selected = 0;
        try {
            selected = Integer.parseInt(str);
        }
        catch (NumberFormatException e) {
            System.out.println(" % Bad number format in IntegerChooser: " + "\""+  str + "\"" );
            // fall back to previously selected value
            traceCountComboBox.setSelectedItem( String.valueOf(wfCount) );
            selected = wfCount;
        }

        return selected;
    }

    public void setChannelCount(int val) {
        wfCount = val;
        traceCountComboBox.setSelectedItem(""+wfCount);
    }
     
    public double getTimeWindow() {
        double value = secComboBox.getValue();
        allSecsFlag = (value <= 0.);
        return (allSecsFlag) ? -secInView : value;
    }

    public void setTimeWindow(double sec) {
        secInView = Math.abs(sec);
        allSecsFlag = (sec < 0);
        //secComboBox.setCurrentValue(secInView);
        if (sec < 0) secComboBox.setCurrentValue("ALL");
        else secComboBox.setCurrentValue(secInView);
    }

    private void jbInit() throws Exception {
        this.setLayout(vertLayout);
 
        this.setBorder(new TitledBorder("Group Panel Setup"));
 
        jLabel1.setPreferredSize(new Dimension(100, 17));
        jLabel1.setText("Channels in view");
        jLabel2.setPreferredSize(new Dimension(100, 17));
        jLabel2.setText("Seconds in view");

        traceCountComboBox.setPreferredSize(new Dimension(80, 24));
 
        for (int i = 0; i < countList.length; i++) {
            traceCountComboBox.addItem(countList[i]);
        }
        traceCountComboBox.setEditable(true);
        traceCountComboBox.setSelectedItem(""+wfCount);
 
        secComboBox = new ZoomScaleComboBox(timeList);
        secComboBox.setCurrentValue(secInView);
        secComboBox.setPreferredSize(new Dimension(80, 24));

        this.add(jPanel1, null);
        jPanel1.add(jLabel1, null);
        jPanel1.add(traceCountComboBox, null);
 
        this.add(jPanel2, null);
        jPanel2.add(jLabel2, null);
        jPanel2.add(secComboBox, null);
    }
 
}
