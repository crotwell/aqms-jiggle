package org.trinet.jiggle;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.StringTokenizer;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;

import org.trinet.jiggle.JiggleSolutionSolverDelegate;
import org.trinet.jiggle.JiggleProperties;

import org.trinet.util.StringList;
import org.trinet.util.graphics.text.JTextClipboardPopupMenu;
import org.trinet.util.locationengines.*;

/*
// Solution server instructions:
// Need action buttons for get/set, init configuration through property settings (locationServerXXX)
// allow user to edit properties them in GUI, then use buttons to change:
|GetInstName| - get name of the current instruction file (send command by button action return to field?)
|GetInstFileList| - return list of available instruction files, one per line (to JList or JTextArea)
|GetInstFile| - get contents of currently set instruction file (to JTextArea)
|GetInstFile| <filename> - return contents of this instruction file (to JTextArea)
|PutInstFile| <filename> - write text until |EOT| or |EOF to "filename" instruction file (text from JTextArea) << -- Dont' allow user to do this ?
|SetInstFile| <filename> - set use of named instruction file for the duration of the connection (from JList or field)
|SetInstFile| - set instruction file back to default (send command by button action)

Managing Instruction files 
Default instruction file is .hypinst in its working directory the name is reserved for the default file and may not be modified.
Things to consider: 
Each solserver instance (port number) can have a different default file
All users sharing a solserver host will have access to all the instruction files.
All users can modify these shared files so you cant be sure a file hasnt been changed since you used it last.
All users can overwrite files (use the same filename)
*/
public class DPHypinst extends JPanel {

    private JiggleProperties props = null;

    private static String DEFAULT_HYPINV_CMD_FILE = "hypinst.";

    private String selectedInstFile = null;
    private JiggleSolutionSolverDelegate delegate = null;

    private JTextArea instTextArea = null;
    private JComboBox instFileNamesBox = null;
    private JButton setInstButton = null;

    //private JLabel serviceLabel = new JLabel();

    public DPHypinst(JiggleSolutionSolverDelegate delegate, JiggleProperties properties) {

        props = properties;
        this.delegate = delegate;

        try {
          initGraphics();
        }
        catch(Exception ex) {
          ex.printStackTrace();
        }
    }

    private LocationEngineHypoInverse getLocationService() {
        return (delegate != null && delegate.hasLocationService()) ?
                (LocationEngineHypoInverse) delegate.getLocationEngine() : null;
    }

    private void initGraphics() throws Exception {
        setLayout(new BorderLayout());
        // HYP2000 hypinst config
        this.add(makeInstPanel(), BorderLayout.SOUTH);
    }

    // HYP2000 hyp(inst) instruction file graphics:
    private JPanel makeInstPanel() {    

        DEFAULT_HYPINV_CMD_FILE =  props.getProperty("hypoinvCmdFile.default", DEFAULT_HYPINV_CMD_FILE);
        //System.out.println("HYP CMD INFO: hypoinvCmdFile.default :" +DEFAULT_HYPINV_CMD_FILE);
        selectedInstFile =  DEFAULT_HYPINV_CMD_FILE;

        // ComboBox list of commands files from server
        String [] filenames = new String [] { DEFAULT_HYPINV_CMD_FILE };
        LocationEngineHypoInverse locationService = getLocationService();
        if (locationService != null)  {
          // |GetInstFileList| - return list of available instruction files, one per line
          String str = locationService.getInstFileList();
          if (str == null || str.indexOf("FAILED") > 0) filenames = new String [] { DEFAULT_HYPINV_CMD_FILE };
          else {
             filenames = new StringList(str, "\n").toArray();
          }
        }
        instFileNamesBox = new JComboBox(filenames);
        instFileNamesBox.setToolTipText("List of command files that are available to location service");
        instFileNamesBox.setEditable(false); // don't allow user to create new files on server? 
        instFileNamesBox.setSelectedItem(selectedInstFile); 
        //System.out.println("HYP CMD INFO: set selected to selectedInstFile :" +selectedInstFile);

        // text area for hyp2000 commands
        instTextArea = new JTextArea();
        instTextArea.setToolTipText("Hypinverse commands (comment text is text after a '/' on a line or any line starting with '*'");
        instTextArea.setEditable(true); // until we come up with property parser for text -aww 09/20/2007
        instTextArea.setBackground(Color.white);
        instTextArea.setRows(24);
        instTextArea.setColumns(60);
        new JTextClipboardPopupMenu(instTextArea);
        //instTextArea.setPreferredSize(new Dimension(600,500));
        if (locationService != null)  {
            String selectedInstText = (selectedInstFile.equals(DEFAULT_HYPINV_CMD_FILE)) ?  // looks in default directory
                locationService.getInstFileContents() : locationService.getInstFileContents(selectedInstFile); // looks in custom directory
            updateInstTextArea(selectedInstText);
        }

        // add listener AFTER box's list is populated else adding items fires events
        instFileNamesBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JComboBox src = (JComboBox) e.getSource();
                String filename = (String) src.getSelectedItem();
                selectedInstFile = (filename == null) ? DEFAULT_HYPINV_CMD_FILE : filename;
                if (setInstButton != null) {
                    setInstButton.setEnabled(!selectedInstFile.equals(DEFAULT_HYPINV_CMD_FILE));
                }
                // |SetInstFile| - set instruction file back to default hypinst.
                // |SetInstFile| <filename> - set use of named instruction file for the duration of the connection
                // input current instFilename value (current JComboBox list selection)
                LocationEngineHypoInverse locationService = getLocationService();
                boolean status = false;
                if (locationService != null) {
                  status = (selectedInstFile.equals(DEFAULT_HYPINV_CMD_FILE)) ?
                                locationService.setInstFile() : locationService.setInstFile(selectedInstFile);
                }

                // recover selected file contents from server here and populate text area
                // |GetInstFile| - return contents of currently set instruction file
                // |GetInstFile| <filename> - return contents of specified instruction file
                String selectedInstText = "";
                if (status) {
                  selectedInstText = (selectedInstFile.equals(DEFAULT_HYPINV_CMD_FILE)) ?
                      locationService.getInstFileContents() : locationService.getInstFileContents(selectedInstFile);
                  updateInstTextArea(selectedInstText);
                }
                else {
                    String msg = (locationService == null) ?
                           "Location service null, or not yet initialized." :
                           "No hypoinverse command file named: " + selectedInstFile;
                     JOptionPane.showMessageDialog(getTopLevelAncestor(), msg,
                           "Command File Load", JOptionPane.ERROR_MESSAGE);
                    instTextArea.setText("");
                    //instTextArea.selectAll();
                }
            }
        });

        //Set combobox selection to the location service command file 
        synchInstNamesBoxSelectionWithService(); // ??

        // Control buttons and their actions
        ActionListener instButtonListener =  new GetSetSaveLoadActionHandler();

        setInstButton = new JButton("Put");
        setInstButton.addActionListener(instButtonListener);
        setInstButton.setToolTipText("Overwrite selected command file on server with the text area commands (not default, custom file only)");
        setInstButton.setEnabled(!selectedInstFile.equals(DEFAULT_HYPINV_CMD_FILE));

        JButton getInstButton = new JButton("Get");
        getInstButton.addActionListener(instButtonListener);
        getInstButton.setToolTipText("Replace text area commands with contents of select server command file");

        JButton loadInstButton = new JButton("Load");
        loadInstButton.addActionListener(instButtonListener); 
        loadInstButton.setToolTipText("Replace text area commands with contents of local disk file (no change to server file)");

        JButton saveInstButton = new JButton("Save");
        saveInstButton.addActionListener(instButtonListener); 
        saveInstButton.setToolTipText("Write text area commands to a local disk file (no changes to server file)");

        JButton addInstButton = new JButton("Add");
        addInstButton.addActionListener(instButtonListener); 
        addInstButton.setToolTipText("Write text area commands to a new server file, set the active server command file to this new file");

        Box instButtonBox = Box.createHorizontalBox();
        instButtonBox.add(Box.createHorizontalGlue());
        instButtonBox.add(getInstButton);
        instButtonBox.add(loadInstButton);
        instButtonBox.add(Box.createHorizontalStrut(8));
        instButtonBox.add(saveInstButton);
        instButtonBox.add(Box.createHorizontalStrut(8));
        instButtonBox.add(setInstButton);
        instButtonBox.add(addInstButton);
        instButtonBox.add(Box.createHorizontalGlue());

        // Add components to panels
        JPanel instFilePanel = new JPanel(new BorderLayout());
        Box hbox = Box.createHorizontalBox();
        JLabel jlbl = new JLabel("Command file set to: ");
        jlbl.setToolTipText("select file from list to be used by current service connection"); 
        hbox.add(jlbl);
        instFileNamesBox.setPreferredSize(new Dimension(150,30));
        instFileNamesBox.setMaximumSize(new Dimension(150,30));
        hbox.add(instFileNamesBox);
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        hbox.add(Box.createHorizontalGlue());
        instFilePanel.add(hbox,BorderLayout.NORTH);

        JPanel mainInstPanel = new JPanel();
        TitledBorder titledBorder =
            new TitledBorder(BorderFactory.createEtchedBorder(Color.white, 
                        new Color(148, 145, 140)),"Hypoinverse Configuration");
        mainInstPanel.setBorder(titledBorder);
        mainInstPanel.setLayout(new BorderLayout());
        mainInstPanel.add(instFilePanel, BorderLayout.NORTH);
        JScrollPane jsp = new JScrollPane(instTextArea);
        jsp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        mainInstPanel.add(jsp, BorderLayout.CENTER);
        mainInstPanel.add(instButtonBox, BorderLayout.SOUTH);

        return mainInstPanel;
    }

    //Set combobox selection to the location service command file 
    private void synchInstNamesBoxSelectionWithService() {
        SwingUtilities.invokeLater( new Runnable() {
            public void run() {
                int oldIdx = instFileNamesBox.getSelectedIndex(); // current selection
                int newIdx = -1;
                LocationEngineHypoInverse locationService = getLocationService();
                if (locationService != null) {
                   // |GetInstName| - returns the name of the current instruction file
                   String name = locationService.getInstName();
                   if (name != null) {
                     // remove leading directory path
                     int ii = name.lastIndexOf("/", name.length());
                     if (ii < 0) ii = name.lastIndexOf("\\", name.length());
                     if (ii > 0) {
                       name = name.substring(ii+1);
                     }
                   }
                   //System.out.println("DEBUG synchInstNamesBoxSelectionWithService name: " + name);
                   DefaultComboBoxModel model = (DefaultComboBoxModel) instFileNamesBox.getModel();
                   newIdx = model.getIndexOf(name);
                }
                if (oldIdx != newIdx && newIdx >= 0) {
                    instFileNamesBox.setSelectedIndex(newIdx);
                    selectedInstFile = (String) instFileNamesBox.getSelectedItem();
                }
            }
        });
    }

    // Overwrite currently set hypoinverse command file with the text area commands
    private boolean doSetServerInstFile() {
        boolean status = true;
        if (selectedInstFile != null) {
            int yn = JOptionPane.showConfirmDialog(getTopLevelAncestor(), "Overwrite server's command file " + selectedInstFile +
                    " with the text area contents?",
                    "Update Hypoinverse Command File", JOptionPane.OK_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE);
            if (yn == JOptionPane.YES_OPTION) {
                System.out.println("HYP CMD INFO: Writing text area commands to hypoinverse command file: " + selectedInstFile);
                // |PutInstFile| <filename> - write text to "filename" instruction file
                // input args are textArea contents plus current selected inst filename
                LocationEngineHypoInverse locationService = getLocationService();
                status = (locationService == null) ? false : locationService.putInstFile(instTextArea.getText(), selectedInstFile);
                if (status) synchInstNamesBoxSelectionWithService();
            }
        }
        return status;
    }

    /*
    private void setSelectedInstFile(String filename) {
        instFileNamesBox.setSelectedItem(filename);
    }
    */

    public String getSelectedInstFile() {
        return selectedInstFile;
    }

    private void updateInstTextArea(String inputText) {
        instTextArea.setText(inputText);
        instTextArea.setCaretPosition(0);
    }

    private class GetSetSaveLoadActionHandler implements ActionListener {

        public void actionPerformed(ActionEvent evt) {

          if (selectedInstFile == null) return;

          String cmd = evt.getActionCommand();

          LocationEngineHypoInverse locationService = getLocationService();
          if (locationService == null && (cmd == "Put" || cmd == "Get" || cmd == "Add")) { 
              JOptionPane.showMessageDialog(getTopLevelAncestor(),
                  "No location service available", cmd, JOptionPane.ERROR_MESSAGE);
              return;
          }

          boolean success = true;

          try {
            if (cmd == "Put") {
              success = doSetServerInstFile();
            }
            else if (cmd == "Get") {
              String selectedInstText = (selectedInstFile.equals(DEFAULT_HYPINV_CMD_FILE)) ?
                      locationService.getInstFileContents() : locationService.getInstFileContents(selectedInstFile);
              updateInstTextArea(selectedInstText);
            }
            else if (cmd == "Add") {
                  String filename = JOptionPane.showInputDialog(getTopLevelAncestor(),
                          "New hypoinverse command filename ", "Add to server", JOptionPane.PLAIN_MESSAGE);
                  if (filename != null) {
                      int idx = ((DefaultComboBoxModel) instFileNamesBox.getModel()).getIndexOf( filename );
                      if (idx < 0) { // not in list
                          // make new file on server, don't add if already in list, E.G. DEFAULT_HYPINV_CMD_FILE
                          success = locationService.putInstFile(instTextArea.getText(), filename);
                          if (success) { // ok
                              success = locationService.setInstFile(filename); // set server to use new instruction file
                              if (success) instFileNamesBox.addItem(filename); // add new name to box
                              synchInstNamesBoxSelectionWithService();
                          }
                      }
                  }
                  else {
                        JOptionPane.showMessageDialog(getTopLevelAncestor(),
                             filename + " is already on server, try another name", "Add Command file", JOptionPane.ERROR_MESSAGE);
                       return;
                  }
            }
            else {

              JFileChooser jfc = new JFileChooser(props.getUserFilePath());
              String filename = selectedInstFile;
              File file = null;
              if (filename != null) {
                  jfc.setSelectedFile(new File(filename));
              }

              if (cmd == "Save") {
                String osname = System.getProperty("os.name","");
                System.out.println("DEBUG osname = " + osname);
                if (osname.toLowerCase().indexOf("win") >= 0) {
                   JOptionPane.showMessageDialog(getTopLevelAncestor(),
                       "FYI: A Windows OS filename cannot start or end with a '.' character", "Save Command file", JOptionPane.INFORMATION_MESSAGE);
                }
                if (jfc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
                    file = jfc.getSelectedFile();
                    filename = file.getAbsolutePath();
                    // Option to set the server's file contents to text area
                    //success = doSetServerInstFile();
                    // save local file
                    System.out.println("HYP CMD INFO: Saving text area (hypoinverse commands) to local disk file:\n" + filename);
                    success = saveInstTextAreaToFile(filename);
                }
              }
              else if (cmd == "Load") {
                if (jfc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                    file = jfc.getSelectedFile();
                    filename = file.getAbsolutePath();
                    if (! file.exists()) {
                        JOptionPane.showMessageDialog(getTopLevelAncestor(),
                             filename + " local disk file D.N.E. !", "Load Local Command file", JOptionPane.ERROR_MESSAGE);
                       return;
                    }

                    System.out.println("HYP CMD INFO: Reading hypoinverse commands into text area from local file:\n" + filename);

                    // buffered file reader here to get file text
                    success = updateInstTextAreaFromFile(filename);
                    // Option to set the server's file contents to text area
                    //if (success) {
                    //    success = doSetServerInstFile();
                    //}
                }
              }
            }
            if (! success) notifyInstCmdFailure(cmd);

          }
          catch(Exception ex) {
            ex.printStackTrace();
            notifyInstCmdFailure(cmd);
          }
        }

        private boolean updateInstTextAreaFromFile(String filename) {
            
            if (filename == null) {
                return false;
            }

            boolean status = true;

            instTextArea.setText(""); // clear current text

            int count = 0; // line counter
            BufferedReader idStream = null;
            try {
              idStream = new BufferedReader(new FileReader(filename));
              String instruction = null;
              while (true) {
                  instruction = idStream.readLine();
                  if (instruction == null) break;
                  count++;
                  if (instruction.length() == 0) continue;
                  instTextArea.append(instruction);
                  instTextArea.append("\n");
              }
              instTextArea.setCaretPosition(0);
            } // upon error do what ? 
            catch (FileNotFoundException ex) {
              ex.printStackTrace();
              System.err.println("Error: Input file not found: " + filename);
              status = false;
            }
            catch (IOException ex) {
              ex.printStackTrace();
              System.err.println("Error: i/o for input file: " + filename + " at line: " + count);
              status = false;
            }
            finally {
              try {
                if (idStream != null) idStream.close();
              }
              catch (IOException ex2) { }
            }
            return status;
        } 

        private boolean saveInstTextAreaToFile(String filename) {

            if (filename == null) {
                return false;
            }

            boolean status = true;

            int count = 0; // line counter
            BufferedWriter idStream = null;
            String idString = null;
            try {
              idStream = new BufferedWriter(new FileWriter(filename));
              StringTokenizer strToke = new StringTokenizer(instTextArea.getText(), "\n\r");
              while (strToke.hasMoreTokens()) {
                  idStream.write(strToke.nextToken());
                  idStream.newLine();
                  count++;
              }
            } // upon error do what ? 
            catch (FileNotFoundException ex) {
              ex.printStackTrace();
              System.err.println("Error: Output file not found: " + filename);
              status = false;
            }
            catch (IOException ex) {
              ex.printStackTrace();
              System.err.println("Error: i/o for output file: " + filename + " at line: " + count);
              status = false;
            }
            finally {
              try {
                if (idStream != null) {
                    idStream.flush();
                    idStream.close();
                }
              }
              catch (IOException ex2) { 
                  status = false;
              }
            }
            return status;
        } 

        private void notifyInstCmdFailure(String cmd) {
            JOptionPane.showMessageDialog(getTopLevelAncestor(),
                cmd + " failed , check command text area and logged messages!",
                "Action Failure", JOptionPane.PLAIN_MESSAGE);
        }
    }
}
