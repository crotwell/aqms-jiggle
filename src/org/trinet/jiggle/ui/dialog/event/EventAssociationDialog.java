package org.trinet.jiggle.ui.dialog.event;

import java.awt.Dimension;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import org.trinet.jasi.DataSource;
import org.trinet.jasi.EnvironmentInfo;
import org.trinet.jiggle.common.AlertUtil;
import org.trinet.jiggle.common.JiggleSingleton;
import org.trinet.jiggle.common.LogUtil;
import org.trinet.jiggle.common.type.AssocEvent;
import org.trinet.jiggle.common.type.RemarkType;
import org.trinet.jiggle.common.type.SolutionCoreType;
import org.trinet.jiggle.database.comment.RemarkDb;
import org.trinet.jiggle.database.event.EventAssocDb;
import org.trinet.jiggle.ui.common.dialog.DialogOkCancel;
import org.trinet.jiggle.ui.panel.event.EventAssociation;

public class EventAssociationDialog extends DialogOkCancel {
    private boolean isNew;
    private long updatedMasterEventId;
    
    public EventAssociationDialog(java.awt.Frame parent, boolean modal, boolean isNew) {
        super(parent, modal);
        this.isNew = isNew;
        this.setPreferredSize(new Dimension(900, 600));
    }

    /**
     * Handle user clicking on "OK" button by save/edit events
     * @param evt 
     */
    protected void btnOkActionPerformed(java.awt.event.ActionEvent evt) {
        EventAssociation assocUI = (EventAssociation)this.getPanContent().getComponent(0);
        
        if (isDataValid(assocUI)) {
            
            if (this.isNew) {
                createNewAssociation(assocUI);
            } else {
                // edit association
                if (isDataChanged(assocUI)) {
                    editAssociation(assocUI);
                } else {
                    // no change. Close dialog.
                    this.dispose();
                }
            }
        }
    }

    /**
     * Check if selected list has changed since the dialog was opened
     * @param assocUI
     * @return 
     */
    private boolean isDataChanged(EventAssociation assocUI) {
        ArrayList<AssocEvent> userSelected = getUserSelectedAssoc(assocUI);
        ArrayList<AssocEvent> originalList = assocUI.getOriginalAssocList();
        boolean isChanged = false;
        
        if (originalList.size() != userSelected.size()) {
            // different size
            isChanged = true;
        } else {
            // Same size. Check all element is in userSelected
            int idx = 0;
            while (!isChanged && idx < originalList.size()) {
                if (!userSelected.contains(originalList.get(idx))) {
                    isChanged = true;
                }
                
                idx++;
            }
        }
        return isChanged;
    }
    
    /** 
     * Edit event association
     * @param assocUI 
     */
    private void editAssociation(EventAssociation assocUI) {
        // Delete and reinsert records to avoid having to compute before/after
        ArrayList<AssocEvent> userSelected = getUserSelectedAssoc(assocUI);

        long commentId = AssocEvent.DEFAULT_ID;

        AssocEvent eventComment = new EventAssocDb(DataSource.getConnection()).getComment(assocUI.getSelectedMasterEvent().getEventId().longValue());
        RemarkType remark = getComment();
        if (eventComment != null && eventComment.getCommentId() > 0) {
            // if there is a valid comment
            remark.setCommentId(eventComment.getCommentId());
            commentId = eventComment.getCommentId();
            new RemarkDb(DataSource.getConnection()).updateRemark(remark.getCommentId(), remark.getComment());
        } else {
            // Comment not found. Add new and update the association
            remark = new RemarkDb(DataSource.getConnection()).insertRemark(remark);

            if (remark != null) {
                commentId = remark.getCommentId();
            }
        }

        EventAssocDb eventDb = new EventAssocDb(DataSource.getConnection());
        boolean isDeleted = eventDb.deleteAssociatedEvent(assocUI.getSelectedMasterEvent().getEventId().longValue());

        if (isDeleted) {
            for (AssocEvent event: userSelected) {
                event.setCommentId(commentId);
                eventDb.insertAssociatedEvent(event);
            }
            eventDb.commit();

            // Set the master event id that was updated
            this.setUpdatedMasterEventId(assocUI.getSelectedMasterEvent().getEventId().longValue());
        } else {
            eventDb.rollback();
            AlertUtil.displayError("Database Error", "Failed to edit event association in the database");
        }
        this.dispose();        
    }

    /**
     * Create and return Remark/comment for the event association
     */
    private RemarkType getComment() {
        String comment = "Jiggle event association by " + EnvironmentInfo.getUsername();
        RemarkType remark = new RemarkType(comment);
        return remark;
    }
    
    /**
     * Create new event association
     * @param assocUI 
     */
    private void createNewAssociation(EventAssociation assocUI) {
        // insert comment and get the comment id
        RemarkType remark = new RemarkDb(DataSource.getConnection()).insertRemark(getComment());
        ArrayList<AssocEvent> userSelected = getUserSelectedAssoc(assocUI);

        boolean isValid = true;
        EventAssocDb eventDb = new EventAssocDb(DataSource.getConnection());
        for (AssocEvent event: userSelected) {

            if (remark != null) {
                event.setCommentId(remark.getCommentId());
            } else {
                LogUtil.error("Could not retrieve a remark record for the event association. master: " + event.getMasterEventId() +
                              "associated: " + event.getAssocEventId());
            }
            boolean insertOk = eventDb.insertAssociatedEvent(event);
            if (!insertOk) {
                isValid = false;
            }
        }
        
        if (isValid) {
            eventDb.commit();
            // Set the master event id that was updated
            this.setUpdatedMasterEventId(assocUI.getSelectedMasterEvent().getEventId().longValue());
            this.dispose();        
        } else {
            // Issue with DB
            eventDb.rollback();
        }

    }
    
    /**
     * Get all user selected associated events
     * @param assocUI
     * @return 
     */
    public ArrayList<AssocEvent> getUserSelectedAssoc(EventAssociation assocUI) {
        ArrayList<AssocEvent> result = new ArrayList<>();
        
        // Get master event id
        SolutionCoreType selectedMasterEvent = assocUI.getSelectedMasterEvent();        
        long masterEventId = selectedMasterEvent.getEventId().longValue();
        
        // Get all user associated events
        ArrayList<SolutionCoreType> selectedEventList = assocUI.getSelectedEvents();

        AssocEvent assocEvent = null;
        for (int idx = 0; idx < selectedEventList.size(); idx++) {
            assocEvent = new AssocEvent();
            assocEvent.setMasterEventId(masterEventId);
            assocEvent.setAssocEventId(selectedEventList.get(idx).getEventId().longValue());
            
            if (selectedEventList.get(idx).getAssocEvent() != null) {
                assocEvent.setCommentId(selectedEventList.get(idx).getAssocEvent().getCommentId());
            }
            
            result.add(assocEvent);
        }
        return result;
    }
    
    /**
     * Validate user selected data before saving to the database
     * @param assocUI
     * @return 
     */
    public boolean isDataValid(EventAssociation assocUI) {                
        // Master event has been selected
        if (assocUI.getSelectedMasterEvent() == null) {
            JOptionPane.showMessageDialog(JiggleSingleton.getInstance().getMainJFrame(),
                    "Please select a master event to continue.",
                    "Select Master Event", 
                    JOptionPane.INFORMATION_MESSAGE);
            
            return false;            
        }
        
        // At least one event has been moved to selected list
        if (assocUI.getMultiSelectPanel().getSelectedListModel().size() < 1) {
            JOptionPane.showMessageDialog(JiggleSingleton.getInstance().getMainJFrame(),
                    "Please select at least one event to continue.",
                    "Select Events", 
                    JOptionPane.INFORMATION_MESSAGE);
            
            return false;
        }
        
        AssocEvent event = isAssociationValid(assocUI);
        if (event != null) {
            JOptionPane.showMessageDialog(JiggleSingleton.getInstance().getMainJFrame(),
                    "Event association already exists between event ID " + event.getMasterEventId() + 
                    " and event ID " + event.getAssocEventId() + "." +
                    "\n\nPlease remove the association.",
                    "Invalid Association", 
                    JOptionPane.INFORMATION_MESSAGE);

            return false;
        }
        
        return true;
    }

    /**
     * Check if all selected events are not already in the database
     * @param assocUI
     * @return 
     */
    public AssocEvent isAssociationValid(EventAssociation assocUI) {
        boolean isValid = true;
        
        ArrayList<AssocEvent> originalList = assocUI.getOriginalAssocList();
        ArrayList<AssocEvent> userSelectedEvents = getUserSelectedAssoc(assocUI);
        EventAssocDb eventDb = new EventAssocDb(DataSource.getConnection());
        
        int idx = 0;
        AssocEvent tempEvent = null;
        while (isValid && idx < userSelectedEvents.size()) {
            if (this.isNew) {
                tempEvent = eventDb.isExist(userSelectedEvents.get(idx).getMasterEventId(), userSelectedEvents.get(idx).getAssocEventId());
                if (tempEvent != null) {
                    // Found an existing association that violates DB constraint
                    isValid = false;
                }                
            } else {
                if (!originalList.contains(userSelectedEvents.get(idx))) {
                    // check new association
                    tempEvent = eventDb.isExist(userSelectedEvents.get(idx).getMasterEventId(), userSelectedEvents.get(idx).getAssocEventId());
                    if (tempEvent != null) {
                        // Found an existing association that violates DB constraint
                        isValid = false;
                    }
                }                
            }
            
            idx++;
        }
        
        return tempEvent;
    }
    
    public long getUpdatedMasterEventId() {
        return updatedMasterEventId;
    }

    public void setUpdatedMasterEventId(long updatedEventId) {
        this.updatedMasterEventId = updatedEventId;
    }
}
