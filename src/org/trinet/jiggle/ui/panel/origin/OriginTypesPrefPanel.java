package org.trinet.jiggle.ui.panel.origin;

import org.trinet.jiggle.JiggleProperties;
import org.trinet.jiggle.common.type.OriginTypeList;
import org.trinet.jiggle.ui.common.model.OriginTypeTableModel;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.trinet.jiggle.common.type.OriginType;
import java.awt.Dialog;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import org.trinet.jiggle.ui.common.dialog.OriginTypeDialog;

/**
 * Panel used in Preference Dialog. Uses NetBeans designer for UI layout.
 */
public class OriginTypesPrefPanel extends javax.swing.JPanel {
    private JiggleProperties newProps = null; // new properties changed by this dialog
    private OriginTypeTableModel dataModel;
    private Dialog parent;

    private final int IDX_TYPE = 0;
    private final int IDX_DESC = 1;
    private final int IDX_DEFAULT = 2;

    private final int DEFAULT_COL_WIDTH = 100;

    public OriginTypesPrefPanel(JiggleProperties props, Dialog parent) {
        initComponents();
        initPanel();
        loadTable(props);
        this.parent = parent;
        this.newProps = props;
    }

    private void setButtonState(boolean state) {
        this.btnEditOrigin.setEnabled(state);
        this.btnDeleteOrigin.setEnabled(state);
    }

    private boolean isTableEmpty() {
        return this.dataModel.getRowCount() > 0 ? false : true;
    }

    private void refreshTable(){
        this.tblData.revalidate();
        this.tblData.repaint();
    }

    private void loadTable(JiggleProperties props){
        OriginTypeList originList = new OriginTypeList(props, true);

        this.dataModel = new OriginTypeTableModel(new String[] {"Type", "Description", "Default"});
        this.dataModel.loadData(originList);
        this.tblData.setModel(dataModel);

        setSortOrder();

        this.tblData.getColumnModel().getColumn(IDX_TYPE).setMaxWidth(DEFAULT_COL_WIDTH);
        this.tblData.getColumnModel().getColumn(IDX_DEFAULT).setMaxWidth(DEFAULT_COL_WIDTH);
        this.tblData.getColumnModel().getColumn(IDX_TYPE).setPreferredWidth(DEFAULT_COL_WIDTH);
        this.tblData.getColumnModel().getColumn(IDX_DEFAULT).setPreferredWidth(DEFAULT_COL_WIDTH);

        if (!this.isTableEmpty()) {
            this.tblData.setRowSelectionInterval(0,0);
        } else {
            setButtonState(false);
        }

        this.tblData.addMouseListener(new DoubleClickListener());

        this.refreshTable();
    }

    private void initPanel(){
        btnNewOrigin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNewOriginActionPerformed(evt);
            }
        });
        btnEditOrigin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditOriginActionPerformed(evt);
            }
        });
        btnDeleteOrigin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteOriginActionPerformed(evt);
            }
        });

        tblData.setAutoCreateRowSorter(true);
        tblData.getSelectionModel().addListSelectionListener(new RowListener());
    }

    private void setSortOrder(){
        tblData.getRowSorter().toggleSortOrder(IDX_TYPE);
    }
    
    private void btnNewOriginActionPerformed(java.awt.event.ActionEvent evt) {                                             
        OriginTypeDialog dialog = new OriginTypeDialog(this.parent,true);
        dialog.setTitle("New Origin Type");
        dialog.pack();
        dialog.setLocationRelativeTo(this.parent);
        dialog.setVisible(true);
        
        if (!dialog.isDialogCanceled()) {
            OriginType newType = dialog.getUserEnteredOriginType();
            this.newProps.setProperty(newType.getPropName(newType.getCode()), newType.getDesc());
            this.dataModel.addRow(newType);

            this.refreshTable();

            findRow(newType);
        }
        
        dialog.dispose();
    }

    private void btnEditOriginActionPerformed(java.awt.event.ActionEvent evt) {
        OriginTypeDialog dialog = new OriginTypeDialog(this.parent,true, this.getSelectedRow());
        dialog.setTitle("Edit Origin Type");
        dialog.pack();
        dialog.setLocationRelativeTo(this.parent);
        dialog.setVisible(true);

        if (!dialog.isDialogCanceled()) {
            OriginType newType = dialog.getUserEnteredOriginType();
            this.newProps.setProperty(newType.getPropName(newType.getCode()), newType.getDesc());
            this.dataModel.updateRow(newType);

            this.refreshTable();
        }

        dialog.dispose();
    }

    /**
     * Find the row originType from the table
     */
    private void findRow(OriginType originType) {
        for (int i = 0; i < this.dataModel.getRowCount(); i++) {
            int idxModel = tblData.convertRowIndexToModel(i);
            OriginType temp = dataModel.getDataAtRow(idxModel);

            if (temp.getCode().equals(originType.getCode())) {
                tblData.setRowSelectionInterval(i,i);
                break;
            }
        }
    }

    private void btnDeleteOriginActionPerformed(java.awt.event.ActionEvent evt) {                                                
        OriginType origin = getSelectedRow();
        if (origin != null) {
            if (origin.isDefault()) {
                JOptionPane.showMessageDialog(parent,
                        "Cannot delete a default origin type. Please delete the description to disable the default origin type.",
                        "Cannot Delete Origin Type",
                        JOptionPane.WARNING_MESSAGE);
            } else {
                int option = JOptionPane.showConfirmDialog(
                        parent,
                        "Delete Origin Type " + origin.getDesc() + "?",
                        "Delete Origin Type ",
                        JOptionPane.WARNING_MESSAGE);

                if (option == JOptionPane.OK_OPTION) {
                    int idxSelected = getSelectedRowModelIndex();
                    if (idxSelected != -1) {
                        this.dataModel.removeRow(idxSelected);
                        this.deleteOriginTypeInProp(origin);

                        if (isTableEmpty()) {
                            setButtonState(false);
                        } else {
                            this.tblData.setRowSelectionInterval(0,0);
                        }
                        this.refreshTable();
                    }
                }
            }
        }
    }

    private void deleteOriginTypeInProp(OriginType origin) {
        this.newProps.remove(origin.getPropName(origin.getCode()));
    }

    /**
     * Get the Origin Type of the selected row
     */
    private OriginType getSelectedRow() {
        OriginType origin = null;
        int idxSelected = tblData.getSelectedRow();
        
        if (idxSelected != -1) {
            int idxModel = tblData.convertRowIndexToModel(idxSelected);
            origin = this.dataModel.getDataAtRow(idxModel);
        }
        
        return origin;
    }

    /**
     * Find a user selected row and return the model's row index
     */
    private int getSelectedRowModelIndex() {
        int idxModel = -1;
        int idxSelected = tblData.getSelectedRow();
        if (idxSelected != -1) {
            idxModel = tblData.convertRowIndexToModel(idxSelected);
        }
        return idxModel;
    }

    private class RowListener implements ListSelectionListener {
        public void valueChanged(ListSelectionEvent evt) {
            if (evt.getValueIsAdjusting())
                return;

            // Listen to row selection and disable buttons
            int[] idxSelected = tblData.getSelectedRows();
            if (idxSelected.length > 0) {
                setButtonState(true);
            } else {
                setButtonState(false);
            }
        }
    }

    private class DoubleClickListener extends MouseAdapter {
        public void mouseClicked(MouseEvent e) {
            if (e.getClickCount() == 2) {
                if (getSelectedRow() != null) {
                    btnEditOriginActionPerformed(null);
                }
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnNewOrigin = new javax.swing.JButton();
        btnEditOrigin = new javax.swing.JButton();
        btnDeleteOrigin = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblData = new org.trinet.jiggle.ui.common.BaseJTable();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();

        setBorder(javax.swing.BorderFactory.createTitledBorder("List of Origin Types"));

        btnNewOrigin.setText("New...");

        btnEditOrigin.setText("Edit...");

        btnDeleteOrigin.setText("Delete...");

        tblData.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3"
            }
        ));
        jScrollPane1.setViewportView(tblData);

        jLabel2.setText("To disable a default origin type, remove the description for the origin type.");

        jLabel3.setText("Note: Origin type must be defined in the \"Origin\" database table.");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnNewOrigin, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(btnEditOrigin, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btnDeleteOrigin, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap())
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 481, Short.MAX_VALUE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, 481, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 304, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnNewOrigin)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnEditOrigin)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnDeleteOrigin)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnDeleteOrigin;
    private javax.swing.JButton btnEditOrigin;
    private javax.swing.JButton btnNewOrigin;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private org.trinet.jiggle.ui.common.BaseJTable tblData;
    // End of variables declaration//GEN-END:variables
}
