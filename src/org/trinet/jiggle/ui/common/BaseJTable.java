package org.trinet.jiggle.ui.common;

import javax.swing.*;

public class BaseJTable extends JTable{
    public BaseJTable() {
        super();

        this.setColumnSelectionAllowed(false);
        this.setRowSelectionAllowed(true);
        this.getTableHeader().setReorderingAllowed(false);
        this.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }
}
