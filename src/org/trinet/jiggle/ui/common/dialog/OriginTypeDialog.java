package org.trinet.jiggle.ui.common.dialog;

import java.awt.Cursor;
import java.awt.Dialog;
import java.awt.Frame;
import org.trinet.jiggle.common.type.OriginType;
import org.trinet.jiggle.ui.panel.origin.OriginTypeDetailPanel;

public class OriginTypeDialog extends DialogOkCancel {
    OriginType originType;
    OriginTypeDetailPanel panel;
    boolean isNew = false;

    public OriginTypeDialog(Frame parent, boolean modal) {
        super(parent, modal);
        this.originType = new OriginType();
        panel = new OriginTypeDetailPanel();

        this.setPanContent(panel);
        this.isNew = true;
    }

    public OriginTypeDialog(Dialog parent, boolean modal) {
        super(parent, modal);
        this.originType = new OriginType();
        panel = new OriginTypeDetailPanel();
        
        this.setPanContent(panel);
        this.isNew = true;
    }

    public OriginTypeDialog(Dialog parent, boolean modal, OriginType userOriginType) {
        super(parent, modal);
        isNew = false; // in edit mode

        this.originType = userOriginType;
        panel = new OriginTypeDetailPanel(userOriginType);
        panel.setTypeFieldState(false); // disable origin type field so that it can be used as a lookup key
        this.setPanContent(panel);
    }


    @Override
    protected void btnOkActionPerformed(java.awt.event.ActionEvent evnt) {
        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        
        this.originType.setCode(this.panel.getUserOriginTypeName());
        this.originType.setDesc(this.panel.getUserOriginDesc());

        this.setVisible(false);
        this.setCursor(Cursor.getDefaultCursor());        
    }

    public OriginType getUserEnteredOriginType() {
        return this.originType;
    }
}
