package org.trinet.jiggle.ui.common.dialog;

import org.trinet.jiggle.common.type.EventDisplayType;
import org.trinet.jiggle.common.type.GeoType;
import org.trinet.jiggle.ui.panel.EventGeoTypePanel;

import java.awt.*;

public class RegionalEqDialog extends DialogOkCancel {

    EventGeoTypePanel panel;

    public RegionalEqDialog(Frame parent, boolean modal, EventDisplayType event, GeoType gtype, String comment) {
        super(parent, modal);

        panel = new EventGeoTypePanel();
        panel.getCmbEventType().setSelectedItem(event);
        panel.getCmbGType().setSelectedItem(gtype);
        panel.getTxtComment().setText(comment);

        this.setPanContent(panel);
    }

    @Override
    protected void btnOkActionPerformed(java.awt.event.ActionEvent evnt) {
        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

        this.setVisible(false);
        this.setCursor(Cursor.getDefaultCursor());
    }

    public EventDisplayType getSelectedEvent(){
        return (EventDisplayType) this.panel.getCmbEventType().getSelectedItem();
    }

    public GeoType getSelectedGeographicType() {
        return (GeoType) this.panel.getCmbGType().getSelectedItem();
    }

    public String getUserComment() {
        return this.panel.getTxtComment().getText().trim();
    }

}
