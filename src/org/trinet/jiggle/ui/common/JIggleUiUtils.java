package org.trinet.jiggle.ui.common;

import javax.swing.JTextPane;
import javax.swing.text.JTextComponent;

public class JIggleUiUtils {
  /**
   * Get a message text component for a dialog that the user may copy text from.
   * 
   * @param text the text.
   * @return the message text component.
   */
  public static JTextComponent getMessage(String text) {
    JTextPane message = new JTextPane();
    message.setContentType("text/plain");
    message.setEditable(false);
    message.setBackground(null);
    message.setBorder(null);
    message.setText(text);
    return message;
  }
}
