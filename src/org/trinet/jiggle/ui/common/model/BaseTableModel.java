package org.trinet.jiggle.ui.common.model;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

public abstract class BaseTableModel extends AbstractTableModel {
    protected ArrayList cache;
    protected int colCount;
    protected String [] headers;
    
    public BaseTableModel(){
        super();
    }
    
    @Override
    public String getColumnName(int i) {
        return headers[i];
    }
    
    @Override
    public int getRowCount() {
        return this.cache.size();
    }
    
    @Override
    public int getColumnCount() {
        return this.colCount;
    }
    
    
}
