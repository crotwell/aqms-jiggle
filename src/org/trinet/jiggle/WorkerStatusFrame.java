package org.trinet.jiggle;
import java.awt.*;
import javax.swing.*;
import org.trinet.util.graphics.*;

/**
 * A popup frame used to show the status of an operation. Doesn't really work
 * well for showing dynamic progress because the threading isn't handled right.
 */
public class WorkerStatusFrame extends StatusFrame {

     public WorkerStatusFrame() {
         super();
     }

     public WorkerStatusFrame(String title, String text) {
         super(title, text);
     }

     public WorkerStatusFrame(String title, String text, Icon icon) {
         super(title, text, icon);
     }

/** Puts the frame creation job on the GUI event queue and returns. */
    public void pop(String title, String message, boolean indeterminate) {
        final String s1 = title;
        final String s2 = message;
        final boolean tf = indeterminate;

        SwingUtilities.invokeLater(
          new Runnable() {
              public void run() {
                setProgressIndeterminate(tf);
                setProgressStringPainted(! tf);
                set(s1, s2);
                setVisible(true);
                if (beep == BEEP_ON || beep == BEEP_ON_OPEN) getToolkit().beep(); // make noise
              }
          }
        );
    }
    public void unpop() {
      SwingUtilities.invokeLater(
          new Runnable() {
            public void run() {
                setProgressIndeterminate(false);
                setProgressStringPainted(true);
                setVisible(false);
                if (beep == BEEP_ON || beep == BEEP_ON_CLOSE) getToolkit().beep(); // make noise
            }
          }
      );
    }

/*
public static class Tester {
     public static void main(String[] args) {
        ImageIcon defIcon = new ImageIcon("images/gearsAnim.gif");
        WorkerStatusFrame statusFrame = new WorkerStatusFrame();
        statusFrame.pop("Test frame", "Something's happening", true);
     }
  } // end of Tester
*/
}
