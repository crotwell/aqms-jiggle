package org.trinet.jiggle;

/**
 * Used to provide globally accessible view of the Unpick mode.
 */

public class UnpickMode  {

    public static final String PHASE = "P";
    public static final String AMP = "A";
    public static final String CODA = "C";
    private static String mode = PHASE;

    private static boolean on = false;

    UnpickMode() { }

    /** Sets mode's current on state, t or f */
    public static void set(boolean tf) {
        on = tf;
    }

    /** Returns mode's current state, t or f */
    public static boolean get() {
	return on;
    }

    /** More gramatical method. Same as get() */
    public static boolean isTrue() {
	return on;
    }

    public static void setMode(String newmode) {
        if (PHASE.equals(newmode)) mode = PHASE;
        else if (AMP.equals(newmode)) mode = AMP;
        else if (CODA.equals(newmode)) mode = CODA;
    }
    public static String getMode() {
        return mode;
    }
}
