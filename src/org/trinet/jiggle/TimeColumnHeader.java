package org.trinet.jiggle;

import java.text.*;
import java.util.*;
import java.io.*;

import java.awt.*;
import javax.swing.*;
import org.trinet.util.graphics.ColorList;
import org.trinet.util.*;

/*
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Can't get this to work ! Doesn't always repaint. Seems to contain
random scratch graphics.
 */

/**
 * Create a panel with a time scale for use in the ColumnHeader of the
 * ZoomePanel scrollpane.
 */
// TODO: add modes for tick levels, change color, etc.

public class TimeColumnHeader extends JLabel {

    //TODO: look this up from scrollPane or pass as arg.

    // Set default colors
    Color foregroundColor = Color.black;
    Color backgroundColor = ColorList.buff;

    //    Dimension panelSize;
    WFPanel wfp = null;

    TimeSpan timeSpan = null;
    double pixelsPerSec = 0.;
    double startTime = 0.;
    double duration = 0.;
    int headerHeight = 18;      // default height

    int tick = 0;
    int x = 0;
    double sec = 0.;

    String str;

    // default font
    int fontSize = 12;
    //    Font font = new Font("Serif",Font.ITALIC | Font.BOLD, fontSize);
    Font font = new Font("Serif", Font.BOLD, fontSize);

    /**
     * Create the time TimeColumnHeader for this WFPanel.
     */
    public TimeColumnHeader(WFPanel wfp) {

        this.wfp = wfp;

        this.setDoubleBuffered(true);  // another hopefull thrash //dg added // aww 11/24 

        setBackground(backgroundColor);

        //not opaque implies background fill not done, transparent
        setOpaque(false); // false is default for JLabel, to see BombSight - aww

    }

    /** used by super.paint() to set size of lable = panel */
    public Dimension getPreferredSize() {
        return new Dimension(wfp.getWidth(), headerHeight);
    }

    /**
     * Paint the scale
     */
    public void paintComponent(Graphics g) {

        super.paintComponent(g);    // give super and delegate chance to paint

        setSize(getPreferredSize());

        duration  = wfp.panelBox.getTimeSpan().size();
        // Temp kludge here to prevent loop hangup of drawing too many time tics when ending timestamp bad -aww 2015/03/20
        if (duration > 3600.) {
            System.err.println("WARNING: TimeColumnHeader skipped drawing of time scale: check waveform times, duration > 3600 s");
            return;
        }
        startTime = wfp.panelBox.getTimeSpan().getStart();
        pixelsPerSec = (double) wfp.getWidth() / duration;

        // fill background with proper color
        // without this the painting is irratic
        Graphics g2 = g.create();
        g2.setColor(backgroundColor);
        //g2.fillRect(0,0,getWidth(),getHeight());
        //Rectangle r = g2.getClipBounds();          // get damaged region // dg removed // aww 11/24
        //g2.fillRect(r.x, r.y, r.width, r.height);

        g2.setColor(foregroundColor);
        g2.setFont(font);

        double wholeSec = Math.floor(startTime);
        int height = getHeight();
        // the above values are NOT the cause of the repaint problem
        // they are all unchanged between a good and bad repaint

        // TODO: optimize by only plotting scale within getClipBounds()
        //TODO: make ticks at 0.1 or 0.01 secs if scale is small?
        String label = null;
        int isec = 0;
        for (int tsec=1; tsec < duration; tsec++) {

          sec = wholeSec + (double) tsec;
          x = ((int) ((sec - startTime) * pixelsPerSec));  //pixel of time

          label = LeapSeconds.trueToString (sec, "HH:mm:ss"); // UTC seconds now - aww 2008/02/10
          isec = Integer.parseInt(label.substring(6));
          tick = height/8;                  // 1 sec ticks
          if (isec%60 == 0) {
              tick = height;  // do 60 and 00 both long
              if (isec == 0) { // start of next minute
                  //label minute
                  g2.drawString(label.substring(0,5), x+2, tick-4);   // bad magic numbers
              }
          }
          else if (isec%10 == 0) tick = height/2;   // 10 sec
          else if (isec% 5 == 0) tick = height/4;   // 5 sec

          // draw minute tick here
          g2.drawLine(x, 0, x, tick);
       }

       // if scale is really big make 0.1 sec ticks
      if (pixelsPerSec > 40) {
        tick = height/12;
        for (double fsec=0.1; fsec < duration; fsec += 0.1) { // fsec start now 0.1 instead of 1. sec? -aww 2008/02/10
            sec = wholeSec + fsec;
            x = ((int) ((sec - startTime) * pixelsPerSec));  //pixel of time
            g2.drawLine(x, 0, x, tick);
        }
      }

      g2.dispose();
  }

}
