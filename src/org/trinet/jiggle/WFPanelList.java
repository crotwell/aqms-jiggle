package org.trinet.jiggle;

import java.text.*;
import java.awt.*;
import java.util.*;
import javax.swing.event.*;
import java.awt.event.*;
import javax.swing.*;

import org.trinet.jasi.*;

/** A list of WFPanels. Amoung other functions this class can be a listener to
* the SelectedWFViewModel and set
* a newly selected WFView's WFPanel as selected. This way we don't have to
* put a SelectedWFViewModel ChangeListener on every WFPanel (there can be 1200+).*/
public class WFPanelList extends ArrayList {

     MasterView mv;

     private WFPanel selectedWFPanel = null;

     SelectedWFViewListener selectedWFViewListener =
                            new SelectedWFViewListener();

     public WFPanelList() {
     }

     public WFPanelList(MasterView mv) {
       setMasterView(mv);
     }
     
     protected WFPanel getSelectedPanel() {
         synchronized (this) {
             return selectedWFPanel;
         }
     }

     public void clear() {
        WFPanel[] wfp = getArray();
        for ( int i=0; i < wfp.length; i++)  {
          wfp[i].clear();
        }
        selectedWFPanel = null;
        mv = null; // added 2009/03/18 -aww
        super.clear();   // empty list, added 2009/03/18 -aww
     }

     public void setMasterView(MasterView mview) {
       removeListeners(mv);  // nuke old listeners
       mv = mview;

       addListeners(mv);
     }

    /** Add model listeners to this MasterView's models. */
    private void addListeners(MasterView mv) {
     if (mv != null && mv.masterWFViewModel != null) {
         mv.masterWFViewModel.removeChangeListener(selectedWFViewListener);
         mv.masterWFViewModel.addChangeListener(selectedWFViewListener);
     }
    }
    /** Remove model listeners to this MasterView's models. */
    protected void removeListeners(MasterView mv) {
     if (mv != null && mv.masterWFViewModel != null) {
         mv.masterWFViewModel.removeChangeListener(selectedWFViewListener);
     }
    }

    /** Set value of 'showPhaseCues' for whole list. Repaints. */
    public void setShowPhaseCues(boolean tf) {
      WFPanel[] mwfp = getArray();
      for ( int i=0; i < mwfp.length; i++)  {
        mwfp[i].setShowPhaseCues(tf);
      }
    }

    public void setShowPickFlags(boolean tf) {
      WFPanel[] mwfp = getArray();
      for ( int i=0; i < mwfp.length; i++)  {
        mwfp[i].setShowPickFlags(tf);
      }
    }

    public void setShowResiduals(boolean tf) {
      WFPanel[] mwfp = getArray();
      for ( int i=0; i < mwfp.length; i++)  {
        mwfp[i].setShowResiduals(tf);
      }
    }

    public void setShowDeltaTimes(boolean tf) {
      WFPanel[] mwfp = getArray();
      for ( int i=0; i < mwfp.length; i++)  {
        mwfp[i].setShowDeltaTimes(tf);
      }
    }

/**
 * Return the WFPanel that contains this WFView. Returns null if not found.
 */
public WFPanel get(WFView wfv) {

//  synchronized (this) {
    WFPanel[] mwfp = getArray();
    for ( int i=0; i < mwfp.length; i++)  {
        if (mwfp[i].wfv == wfv) return mwfp[i];
    }

    return null;
//  }
}

public int indexOf(WFView wfv) {
    WFPanel[] mwfp = getArray();
    for ( int i=0; i < mwfp.length; i++)  {
      if (mwfp[i].wfv == wfv) return i;
    }
    return -1;
}

/**
 * Return the WFPanel that contains this Channel. Returns null if not found.
 */
public WFPanel get(Channelable chan) {
//  synchronized (this) {
    WFPanel[] mwfp = getArray();
    for ( int i=0; i < mwfp.length; i++)  {
        if (mwfp[i].wfv.getChannelObj().equalsChannelId((ChannelIdIF)chan.getChannelObj())) return mwfp[i];
    }

    return null;
//  }
}

/**
 * Return List of WFPanel found in this list that belong to the input Channelable's triaxial group.
 * Returns empty list if none found.
 */
public ArrayList getTriaxialList(Channelable chan) {
    WFPanel[] mwfp = getArray();
    ArrayList aList = new ArrayList(6);
    for ( int i=0; i < mwfp.length; i++)  {
        if (mwfp[i].wfv.getChannelObj().sameTriaxialAs((Channelable)chan)) aList.add(mwfp[i]);
    }
    return aList;
}

/** Return list as array of WFPanels. */
public WFPanel[] getArray() {
  return (WFPanel[]) this.toArray(new WFPanel[this.size()]);
}

/** Return list containing waveforms found in the WFPanels. */
public java.util.List getWfList() {
  WFPanel [] wfps = getArray();
  ArrayList wfList = new ArrayList(wfps.length);
  Waveform wf = null;
  for ( int ii =0; ii < wfps.length; ii++) {
      wf = wfps[ii].getWf();
      if (wf != null) wfList.add(wf);
  }
  return wfList;
}

public void resetWfColor() {
    WFPanel[] wfpArray = getArray();
    for (int idx = 0; idx < wfpArray.length; idx++) {
        wfpArray[idx].resetWfColor();
    }
}

public void resetAmpScaleFactor(double factor) {
    WFPanel[] wfpArray = getArray();
    for (int idx = 0; idx < wfpArray.length; idx++) {
        wfpArray[idx].setAmpScaleFactor(factor);
    }
}

/* Temp test
public void setPanelBoxSize() {
      WFPanel[] mwfp = getArray();
      for ( int i=0; i < mwfp.length; i++)  {
        mwfp[i].setPanelBoxSize();
      }
}
*/

/** INNER CLASS: Handle changes to the selected WFView. */
      class SelectedWFViewListener implements ChangeListener {
         public void stateChanged(ChangeEvent changeEvent) {

            synchronized (this) {
                // unselect the old one, queues a repaint
                if (selectedWFPanel != null) selectedWFPanel.setSelected(false);
                WFView newWFV = ((SelectedWFViewModel) changeEvent.getSource()).get();
                // select the new one, queues a repaint
                selectedWFPanel = get(newWFV);
                if (selectedWFPanel != null) selectedWFPanel.setSelected(true);
            }

          }
      }  // end of WFViewListener
} 
