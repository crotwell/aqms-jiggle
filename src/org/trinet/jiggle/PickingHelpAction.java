package org.trinet.jiggle;
import java.awt.Font;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import javax.swing.*;
import org.trinet.util.graphics.text.JTextClipboardPopupMenu;

public class PickingHelpAction extends AbstractAction {

    private JDialog jd = null;

    public PickingHelpAction() {
        super("Panel hot key help");
    }

    public void actionPerformed(ActionEvent e) {
        makeHelpDialog();
    }

    public void makeHelpDialog() {

        if (jd != null && jd.isVisible()) {
            return;
        }
  
        JTextArea jta = new JTextArea(34,80); 
        jta.setEditable(false);
        jta.setFont( new Font("Monospaced", Font.PLAIN, 12));
        jta.append("                   HOT KEY ACTION MAPPINGS\n");
        jta.append("\nTyping one of the below listed keys,\n");
        jta.append("effects the pick closest to the window's centertime.\n");
        jta.append(" x         = delete\n");
        jta.append(" r         = add +5 to weight\n");
        jta.append(" f         = flip polarity\n");
        jta.append(" p         = popup pick menu\n");
        jta.append("\nNOTE: pick flag's color when its weight=0 is 'color.pick.unused' property,\n      value default is dark pink.\n");

        jta.append("\nTyping one of the below listed keys make a P-pick\n");
        jta.append("at the window's centertime with the listed descriptor,\n");
        jta.append("holding down ALT or SHIFT makes it a S-pick.\n");
        jta.append(" `         = 'iP0'\n");
        jta.append(" NUMPAD0   = 'iP0'\n");
        jta.append(" NUMPAD1   = 'iP1'\n");
        jta.append(" NUMPAD2   = 'eP2'\n");
        jta.append(" NUMPAD3   = 'eP3'\n");
        jta.append(" NUMPAD4   = 'eP4'\n");

        jta.append("\nWhen property pickingPanelHotKeyNum2Wt=false (default),\n");
        jta.append("typing one of the below listed keys makes a P-pick\n");
        jta.append("at the window's centertime with the listed descriptor,\n");
        jta.append("holding down ALT or SHIFT make it a S-pick, otherwise\n");
        jta.append("the weight of the pick closest to centertime is set to the value.\n");
        jta.append(" 0         = 'iP0'\n");
        jta.append(" 1         = 'iP1'\n");
        jta.append(" 2         = 'eP2'\n");
        jta.append(" 3         = 'eP3'\n");
        jta.append(" 4         = 'eP4'\n");

        jta.append("\nKeys that change pick descriptor weight:\n");
        jta.append(" F1 1\n");
        jta.append(" F2 2\n");
        jta.append(" F3 3\n");
        jta.append(" F4 4\n");
        jta.append(" F5 0\n");

        jta.append("\nKeys that change pick descriptor onset:\n");
        jta.append(" i\n");
        jta.append(" e\n");
        //jta.append(" w\n"); // Used for WOOD-ANDERSON filter

        jta.append("\nKeys that change pick descriptor first motion:\n");
        jta.append(" space or . (none, undetermined)\n");
        jta.append(" c (up, compression high SNR)\n");
        jta.append(" + (up, compression low SNR)\n"); // if approved
        jta.append(" d (down, dilation high SNR)\n");
        jta.append(" - (down, dilation low SNR)\n"); // if approved
        jta.append(" . none\n");
        jta.append("   (space, none)\n");

        jta.append("\nKeys that change the zoom panel viewport:\n");
        jta.append(" [ Time Zoom-out (contract)\n"); 
        jta.append(" ] Time Zoom-in (expand)\n");
        jta.append(" ; Time Full Range (collapse)\n");
        jta.append(" : Time and Amp Full Range (collapse)\n");
        jta.append("\n");
        jta.append(" * Amp Zoom-in (expand)\n");
        jta.append(" | Amp Zoom-in (expand)\n");
        jta.append(" / Zoom-out (contract)\n");
        jta.append(" \\ Zoom-out (contract)\n");
        jta.append(" ' Show Full (collapse)\n");
        jta.append(" \" Show Full Time and Full Amp Range (collapse)\n");
        jta.append("\n");
        jta.append(" t toggles time tick border at top/bottom of zoom viewport\n");
        jta.append(" h toggles visibility of all pick, coda, amp flags\n");
        jta.append(" H toggles visibility of the pick residual and delta time bars\n"); 

        jta.append("\nKeys that change the selected waveform view:\n");
        jta.append(" UP        = move selection up   1 trace\n");
        jta.append(" DOWN      =                down 1 trace\n");
        jta.append(" PAGE_UP   =                up   1 page\n");
        jta.append(" PAGE_DOWN =                down 1 page\n");
        jta.append(" HOME      =                first  trace\n");
        jta.append(" END       =                last   trace\n");


        jta.append("\nKeys that delete parametric data:\n");
        jta.append(" x Deletes the picks closest to centertime for the selected channel\n");
        jta.append(" X Deletes all picks (P and S) on the selected channel\n");
        jta.append(" x+ALT Deletes ALL automatic picks for the EVENT (Human timed remain)\n");
        jta.append(" x+CTRL Deletes ALL picks for the EVENT\n");
        jta.append("\n");
        jta.append(" For the selected channel view and current event:\n");
        jta.append("  BACKSPACE+SHIFT Deletes all picks, amp, and coda\n");
        jta.append("  BACKSPACE+CTRL  Deletes coda\n");
        jta.append("  BACKSPACE+ALT   Deletes amp\n");
        jta.append("  DELETE First deletes coda, if none, the amp, if none, the picks\n");
        jta.append("  DELETE+SHIFT deletes picks, if none, the amp, if none, the coda\n");
        //jta.append("  DELETE+ALT   Deletes the amp\n");
        //jta.append("  DELETE+CTRL  Deletes the coda\n");
        jta.append("\n");
        jta.append(" For the current event:\n");
        jta.append("  F9+SHIFT Deletes ALL event picks, amps, and codas\n");
        jta.append("  F9+ALT   Deletes ALL event amps\n");
        jta.append("  F9+CTRL  Deletes ALL event codas\n");

        jta.append("\nKeys that restore deleted parametric data:\n");
        jta.append(" z+SHIFT Restores picks deleted by last pick delete action\n");
        jta.append(" z+ALT   Restores amps deleted by last amp delete action\n");
        jta.append(" z+CTRL  Restores codas deleted by last coda delete action\n");

        jta.append("\nKeys that control tab pane navigation:\n");
        jta.append(" C or c Selects Catalog tab (EXCEPTION: for Wavefrom tab, 'c' changes fm)\n");
        jta.append(" L or l Selects Location tab\n");
        jta.append(" M or m Selects Magnitude tab\n");
        jta.append(" T or t Selects Message tab (EXCEPTION: for Wavefrom tab, 't' toggles time scale)\n");
        jta.append(" W or w Selects Waveform tab\n");
        jta.append("\n");

        jta.append("\nKeys that control waveform filtering for selected panel:\n");
        jta.append(" o Toggles current waveform filter on/off\n");
        jta.append(" b Highpass Butterworth filter on\n");
        jta.append(" B Bandpass Butterworth filter on\n");
        jta.append(" b+ALT Lowpass Butterworth filter on\n");
        jta.append("\n w Wood-Anderson filter on\n");
        jta.append(" W Bandpass Wood-Anderson filter on\n");
        jta.append(" w+ALT Highpass Wood-Anderson filter on\n");
        //jta.append(" w+CTRL EHZ Wood-Anderson filter on\n");

        jta.append("\nKeys that control flags:\n");
        jta.append(" F6+SHIFT Hides currently selected panel in lower scroller panel\n");
        jta.append("\n");
        jta.append(" F7+SHIFT Toggles highlighted panel's view's selection flag\n");
        jta.append(" F7+ALT Flags all unhidden views in scroller as 'selected'\n");
        jta.append(" F7+CTRL Flags all unhidden views in scroller as 'unselected'\n");
        jta.append("\n");
        jta.append(" F8+SHIFT Toggles selected waveform's clip flag\n");
        jta.append("\n");

        jta.setBackground(Jiggle.jiggle.getBackground());
  
        jta.setCaretPosition(0);

        new JTextClipboardPopupMenu(jta);
  
        JOptionPane jop = new JOptionPane(new JScrollPane(jta), JOptionPane.PLAIN_MESSAGE, JOptionPane.DEFAULT_OPTION);
        jd = jop.createDialog(Jiggle.jiggle, "Panel hot key help");
        jd.setModal(false);
        jd.pack();
        Dimension screenSize = Jiggle.jiggle.getToolkit().getScreenSize();
        //jd.setSize(new Dimension((int)(.75*screenSize.width), (int)(.75*screenSize.height)));
        jd.setSize(new Dimension(screenSize.width/2, screenSize.height/2));
        Dimension size = jd.getSize();
        jd.setLocation((int)(screenSize.width - size.width)/2, (int)(screenSize.height - size.height)/2);
        jd.show();
    }
}
