package org.trinet.jiggle;

import java.util.*;
import javax.swing.*;


/**
 * Manage the caching of waveforms. Allows only a fixed
 * number of cached objects (waveforms) to exist in memory. <p>
 *
 * Because the Cache Manager really manages the WFViewList it should only be
 * started after the WFViewList is created. */

public class WFCacheManager {

    WFViewList wfvList;

    int headroom = 20;  // changed init default from 100 to 20 -aww 2010/08/06
    int footroom = 20;  // changed init default from 100 to 20 -aww 2010/08/06

    int oldIndex = -1;

    CacheMgrThread cacheMgrThread = null;

    /** Flag to signal current CacheMgr thread to stop. */
    boolean stopCacheMgr = false;

    //public static boolean debug = true;
    public static boolean debug = false;

    public WFCacheManager(WFViewList list) {

        if (debug) System.out.println("DEBUG WFCacheMgr: Starting.");

        setWFViewList(list);
    }

    public WFCacheManager(WFViewList list, int above, int below) {

        this(list);

        setCacheSize(above, below);

    }

    public WFCacheManager(WFViewList list, int above, int below, int index) {

        this(list);

        setCacheSize(above, below);

        setIndex(index);

    }

    public void setWFViewList(WFViewList list) {
        this.wfvList = list;
        setCacheSize(list.cacheAbove, list.cacheBelow);
    }

/** Specify the size of the cache by setting the number of panels above and below
 * the index. The current index is considered part of the below value so that
 * the Cache size = above + below. */

    public void setCacheSize(int above, int below) {

        headroom = above;
        footroom = below;

        if (debug) System.out.println("DEBUG WFCacheMgr: setCacheSize:  "+headroom +" / "+footroom);

        //adjust to new size, leave index alone.
        int idx = 0;
        if (cacheMgrThread != null) idx = cacheMgrThread.cacheIndex;
        restartCacheMgr(idx);
    }

    /** Returns true if the index is withing the range of the WFViewList. */
    public boolean isLegalIndex(int index) {
        return (index > -1 && index <= (wfvList.size()-1));
    }

    public int getCacheSize() {
        return headroom + footroom;
    }

/** Move the logical index of the waveform cache to the new position. This stops
 any previous CacheMgr thread and starts a new one to do the work in
 background. Returns true if is was sucessful. */
    public boolean setIndex(int index) {

        //System.out.println("DEBUG WFCacheMgr: setIndex("+index+") oldIndex: " + oldIndex);
        //if (index != -1 && index == oldIndex) return true; // removed -aww 2009/04/18
        // "hiding" view does unloadwf, restart even when index unchanged, to handle case of (reactivated) "hidden" view - aww 2009/04/19
        if (index == -1) index = 0;

        if (isLegalIndex(index)) {
            if (debug) System.out.println("DEBUG WFCacheMgr: setIndex: "+index);

            restartCacheMgr(index);

            oldIndex = index;

            return true;

        } else {
            if (debug) System.out.println("DEBUG WFCacheMgr: setIndex no-op, illegal index value: "+index);
            return false;
        }

    }

/** This stops any previous CacheMgr thread and starts a new one to do the work
 in background. */
    public void restartCacheMgr(int index) {

        if (debug) System.out.println("DEBUG WFCacheMgr: restart");
        stopCacheMgr();
        cacheMgrThread = new CacheMgrThread(wfvList, index);

    }

/** Stop the waveforms loader thread. Thread will stop after next waveform
    load is done. Caller will block until old CacheMgr thread stops. */
    public void stopCacheMgr() {

        if (debug) System.out.println("DEBUG WFCacheMgr: stop");

        if (cacheMgrThread != null && cacheMgrThread.thread != null) {

            cacheMgrThread.setStopFlag(true);          // kill predecessor

            // wait for shutdown
            // NOTE: don't call stopCacheMgr() in cacheMgrThread.thread exception block or you get infinite loop here -aww
            while (cacheMgrThread != null && cacheMgrThread.thread.isAlive()) {
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {}
            }

            cacheMgrThread = null;
        }

    }

    public String toString() {
        return "WFCacheManager: "+
            " headroom = "+ headroom +" "+
            " footroom = "+ footroom +" "+
            " wfvList-size = " + wfvList.size();
    }
//////// INNER CLASS
class CacheMgrThread implements Runnable {

    WFViewList wfvList = null;
    public Thread thread = null;
    boolean stopFlag = false;
    int cacheIndex = 0;
    int top = 0;
    int bottom = 0;;
    int loadIdx = 0;

    // constructor - also starts thread
    public CacheMgrThread(WFViewList wfvList, int index) {

        cacheIndex = index;

        this.wfvList = wfvList;
        if (wfvList.size() == 0) return;        // short circuit on blank list

        String threadName = "CacheMgr"+cacheIndex;

        thread = new Thread(this, threadName);

        stopFlag = false;

        if (debug) System.out.println("DEBUG WFCacheMgr: creating thread "+ threadName);

        thread.start();
    }

    public synchronized void setStopFlag(boolean tf) {
        stopFlag = tf;
    }
    public synchronized boolean getStopFlag() {
        return stopFlag;
    }

    // real work is done here
    public void run() {
      try {
        // set thread priority to 1/2 normal
        thread.setPriority(Thread.NORM_PRIORITY/2);

        int i = 0; // instantiate only once for loops (more efficient?)

        if (debug) System.out.println("DEBUG WFCacheMgr run: wfvList length = "+wfvList.size());

        WFView wfv[] = wfvList.getArray();

        // Added this to handle case of "hidden" views -aww 2009/04/18
        ArrayList aList = new ArrayList(wfv.length);
        int activeViewCnt = 0;
        for (i = 0; i< wfv.length; i++) {
          if (wfv[i].isActive) {
              aList.add(wfv[i]);
              if (cacheIndex == i) loadIdx = activeViewCnt;
              activeViewCnt++;
          }
          else unloadwf(wfv[i]); // unload "inactive" views, those not to show  -aww 2009/04/18
        }
        wfv = (WFView []) aList.toArray(new WFView[aList.size()]); // make new array of active views -aww 2009/04/18

        // recalc the cache bounds
        int cacheSize = headroom + footroom;
        int lastIdx = wfv.length - 1;

        if (cacheSize > wfv.length) {        // cache is bigger then data
            top = 0;
            bottom = lastIdx;
        } else {
            top    = loadIdx - headroom;
            bottom = top + cacheSize;
            if (top < 0) {
                top = 0;
                bottom = cacheSize;
            }
            if (bottom > lastIdx) {
                top = lastIdx - cacheSize;
                if (top < 0) top = 0;  // avoid negative value -aww 12/04/2007
                bottom = lastIdx;
            }
        }

        if (debug) System.out.println("DEBUG WFCacheMgr run: "+
                                       "headroom= " + headroom + " "+
                                       "footroom= " + footroom + " "+
                                       "top= "+ top + " "+
                                       "bottom= "+ bottom + " " +
                                       "cacheIndex= " + cacheIndex + " "+
                                       "loadIdx= " + loadIdx + " "+
                                       "wfv.length= "+ wfv.length);

        // release space for stuff now outside the cache bounds
        // above
        for (i = 0; i < top; i++) {
            unloadwf(wfv[i]);
            //new WfUnloader(wfv[i]);
        }

        if (getStopFlag()) return;

        // below
        for (i = bottom+1; i < wfv.length ; i++) {
            unloadwf(wfv[i]);
            //new WfUnloader(wfv[i]);
        }

        if (getStopFlag()) return;

        // load new data from pointer down, this insures that the Waveforms in view
        // are loaded first.

        /*  wfv[i].loadTimeSeries() does not reload if its already loaded.
        NOTE: if time-series is unavailable for some reason, loading will be
        attempted each time thru this loop. This could cause slow cache
        performance if it takes a long time for a data read to fail. */
        for (i = loadIdx; i <= bottom; i++) {
            loadwf(wfv[i]);
            //new WfLoader(wfv[i]);
            thread.yield();                // allow other threads to execute
            if (getStopFlag()) return;
        }

        // load from pointer up
        for (i = loadIdx-1; i >= top; i--) {
            loadwf(wfv[i]);
            //new WfLoader(wfv[i]);
            thread.yield();                // allow other threads to execute
            if (getStopFlag()) return;
        }

        if (debug) System.out.println ("DEBUG WFCacheMgr: DONE end of run()");
      }
      catch (Exception ex) {
        System.err.println ("Error WFCacheMgrThread run() : "+
                                       "headroom= " + headroom + " "+
                                       "footroom= " + footroom + " "+
                                       "top= "+ top + " "+
                                       "bottom= "+ bottom + " " +
                                       "cacheIndex= " + cacheIndex + " "+
                                       "loadIdx= " + loadIdx + " "+
                                       "wfvList.size= "+ wfvList.size());
        ex.printStackTrace();
        //System.err.println("       WFCacheMgr now attempting restart at index: " + cacheIndex);
        //WFCacheManager.this.restartCacheMgr(cacheIndex); // infinite loop if exception is generated internal to this method repeatedly
        System.err.println("       WFCacheMgr stopping...");
        //WFCacheManager.this.stopCacheMgr(); // removed because of infinite Thread.sleep loop - aww 01/17/2008
        setStopFlag(true);          // so try this instead   - aww 01/17/2008
        WFCacheManager.this.cacheMgrThread = null; //this too, before return from run() - aww 01/17/2008
       }

    } // end of run()

    protected boolean loadwf(WFView wfv) {
      if (debug) System.out.println("DEBUG WFCacheMgr: loaded=   " +
              wfv.getChannelObj().toDelimitedSeedNameString() + " km: " + wfv.getChannelObj().getHorizontalDistance());
      return wfv.loadTimeSeries();
    }
    protected boolean unloadwf(WFView wfv) {
      if (wfvList.selectedWFView == wfv) {
          if (debug) System.out.println("DEBUG WFCacheMgr: selected wfv NOT unloaded= "+ wfv.getChannelObj().toDelimitedSeedNameString());
          return false;  // don't unload the "selected" view, if any
      }
      if (debug) System.out.println("DEBUG WFCacheMgr: unloaded= " +
              wfv.getChannelObj().toDelimitedSeedNameString() + " km: " + wfv.getChannelObj().getHorizontalDistance());
      return wfv.unloadTimeSeries();
    }
// Did the following in an attempt to track dow a VM crash
// Thought maybe the load/unload of a wf fired an event that was being
// executed in this thread and causing conflicts.
// Turns out that wf inherits its fireevent() method form JasiObject
// that already call Swing.invokeLater(), so this was a waste of time
// and didn't fix the VM crash.
/*
class WfUnloader implements Runnable {
  WFView wfv;
  protected WfUnloader(WFView wfv) {
    this.wfv = wfv;
    SwingUtilities.invokeLater(this);
  }
    public void run() {
      wfv.unloadTimeSeries();
      if (debug) System.out.println("Cache unloaded= "+
                wfv.getChannelObj().toDelimitedSeedNameString());
   }
}
class WfLoader implements Runnable {
  WFView wfv;
  protected WfLoader(WFView wfv) {
    this.wfv = wfv;
    SwingUtilities.invokeLater(this);
  }
    public void run() {
      wfv.loadTimeSeries();
      if (debug) System.out.println("Cache loaded=   "+
                wfv.getChannelObj().toDelimitedSeedNameString());
   }
}
*/
} // end of inner class
} // WFCacheMgr
