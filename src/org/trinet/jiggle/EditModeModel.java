package org.trinet.jiggle;

/** Model to keep track of what edit mode we are in */
public class EditModeModel extends ChangeModel {

       static final int PICK_MODE = 0;
       static final int AMP_MODE  = 1;
       static final int CODA_MODE  = 1;

     public EditModeModel() {
     }
} 