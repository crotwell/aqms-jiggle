package org.trinet.jiggle;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*; 

import org.trinet.jdbc.*;
import org.trinet.jasi.*;
import org.trinet.util.*;


/**
 * Graphical representation of a Phase pick. 
 * A PickFlag is static as opposed to a PickMarker which is active.
 */
public class PickFlag
{
    /** The WFPanel that contain this PickFlag */
    WFPanel wfp;

    /** The phase represented by this PickFlag */
    Phase ph;

    /** position and size of label flag (in pixels) */
    Rectangle flag;

    // Default size of flag in pixels
    static String DEFAULT_FONT_NAME = "SansSerif";
    static int DEFAULT_FONT_STYLE = Font.PLAIN; 
    static int DEFAULT_FONT_SIZE = 10; 
    static Font DEFAULT_FONT = new Font(DEFAULT_FONT_NAME, DEFAULT_FONT_STYLE, DEFAULT_FONT_SIZE); // -aww 2008/02/09

    static int DEFAULT_FLAG_WIDTH = 27; // was 32, made smaller - aww  2009/04/28
    static int DEFAULT_FLAG_HEIGHT= 10; // was 12, made smaller - aww  2009/04/28

    //static Color DEFAULT_DELTIM_COLOR = new Color(255,153,0,255); // orange opaque FFFF9900
    //static Color DEFAULT_DELTIM_COLOR = new Color(255,153,0,100); // orange transparent 40FF9900
    static Color DEFAULT_DELTIM_COLOR = new Color(0,102,51,100); // green transparent 40006633

    static Color DEFAULT_UNUSED_PICK_COLOR = new Color(246,206,206); // grayed pink FFF6CECE

/** The color of the text in the flag */
    Color textColor   = Color.black;

/** Color of deltatime, pick uncertainty marker */
    //Color dtColor = new Color(markerColor.getRed(), markerColor.getGreen(), markerColor.getBlue(), 100);
    static Color dtColor = DEFAULT_DELTIM_COLOR;

/** Color of non-contributing pick */
    static Color upColor = DEFAULT_UNUSED_PICK_COLOR;

/** Color of residual marker */
    //static Color residualColor = dtColor;

    /** Used to set 'handedness' of a PickFlag */
    public final static int LEFT  = 0;
    /** Used to set 'handedness' of a PickFlag */
    public final static int RIGHT = 1;

    /** 'handedness' of a PickFlag: either "RIGHT" or "LEFT" */
    int handedness = LEFT;

    /** If true, paint the phase description in the "flag". If false, paint
     * the "pole" but not the flag */
    boolean showDescription = true;

    /** If true, show the phase deltatime on the "flag". */
    boolean showDeltaTime = true;

    /** If true, show the phase residual on the "flag". */
    boolean showResidual = true; //TODO:  PROBLEM WITH RES FOR COPIED PHASES = NaN! SET FALSE UNTIL FIXED

    /** Flag width */
    int width  = DEFAULT_FLAG_WIDTH;
    /** Flag height */
    int height = DEFAULT_FLAG_HEIGHT;
    /** Font */
    Font font = DEFAULT_FONT;

/** The current color of the flag */
    Color markerColor = Color.red;

    static protected boolean setMarkerColorByPhase = false;
    static protected Color colorP = Color.red;
    static protected Color colorS = Color.magenta;
    static protected Color colorPtext = Color.black;
    static protected Color colorStext = Color.black;

/**
 * Create a PickFlag of an existing phase
 */
  public PickFlag(WFPanel wfPanel, Phase phase)
  {
    wfp = wfPanel;
    ph  = phase;

    if (wfp.getHeight() < 20)  {
           width = 20; 
           height = 10; 
           font = new Font(DEFAULT_FONT_NAME, Font.PLAIN, 8);
    }

    flag = rectAtTime(ph.getTime());

  }

/**
 * Create a PickFlag and create a new Phase at the given point in the WFPanel
 */
  public PickFlag(WFPanel wfPanel, Point pnt) {

    wfp = wfPanel;

    ph = Phase.create();    // empth phase

    ph.setTime(wfp.dtOfPixel(pnt)); // set time

    markerColor = Color.red;

    flag = rectAtPixel(pnt.x);

  }

       /** Set the marker color. */
       public void  setColor(Color color) {
          this.markerColor = color;
       }

       /** Return the marker color. */
       public Color getColor() {
          return markerColor;
       }

       /** Set the marker size. */
       public void setSize(Dimension dim) {
          width  = (int) dim.getWidth();
          height = (int) dim.getHeight();
       }

       public static final void setBigFont() { // can be set via JiggleProperties - aww 2008/02/09 
           DEFAULT_FLAG_WIDTH = 36;
           DEFAULT_FLAG_HEIGHT= 15;
           DEFAULT_FONT_SIZE = 14; 
           DEFAULT_FONT_STYLE = Font.BOLD; 
           DEFAULT_FONT = new Font(DEFAULT_FONT_NAME, DEFAULT_FONT_STYLE, DEFAULT_FONT_SIZE); // -aww 2008/02/09
       }

       public static final void setSmallFont() { // the initial default -  aww 2008/02/09 
           DEFAULT_FLAG_WIDTH = 27; // was 32, made smaller - aww  2009/04/28
           DEFAULT_FLAG_HEIGHT= 10; // was 12, made smaller - aww  2009/04/28
           DEFAULT_FONT_SIZE = 10;
           DEFAULT_FONT_STYLE = Font.PLAIN; 
           DEFAULT_FONT = new Font(DEFAULT_FONT_NAME, DEFAULT_FONT_STYLE, DEFAULT_FONT_SIZE); // -aww 2008/02/09
       }

       /** Return the marker size. */
       public Dimension getSize() {
          return new Dimension(width, height);
       }

    /**
     * Return the rectangle part of the PickFlag
     */
    Rectangle rectAtTime(double datetime) {

        //        System.out.println("rectAtTime: "+datetime+" -> "+datetime);

        return rectAtPixel(wfp.pixelOfTime(datetime));
    }

    /**
     * Return the rectangle part of the PickFlag
     */
    Rectangle rectAtPixel(int x) {

        int offset = 0;  //offset to upper-right corner of the flag
        if (handedness == LEFT) offset = -width;

        // If the wfPanel is zoomable keep flag within viewport so we can see it
        int y = wfp.getVisibleRect().y;

        return new Rectangle(x+offset, y, width, height);
    }

/**
 * Set/reset the phase for this marker
 */
    public void setPhase(Phase phase)
    {
        ph = phase;
        redraw();
    }

    /** 
     * Set the size of the flag.
     */
    public void setSize(int width, int height) {
        this.width  = width;
        this.height = height;
        redraw();
    }


    /** 
     * Set the 'handedness' of a PickFlag: either "RIGHT" or "LEFT".
     * Default is 'LEFT"
     */
    public void setHandedness(int handedness) {
        this.handedness = handedness;
        redraw();
    }

    /** If true, paint the phase description in the "flag". If false, paint
     * the "pole" but not the flag */
    public void setShowDescription(boolean tf) {
        showDescription = tf;
    }

    /** If true, show the phase deltatime on the "flag". */
    public void setShowDeltaTime(boolean tf) {
        showDeltaTime = tf;
    }

    /** If true, show the phase residual on the "flag". */
    public void setShowResidual(boolean tf) {
        showResidual = tf;
    }

/**
 * Put marker at this location in parent container coordinates (pixels). Only the x-axis
 * matters.
 */
    public void setLocation(int x, int y)
    {
        flag.setLocation(x, y);
    }
/**
 * Put marker at this location in parent container coordinates (pixels). Only the x-axis
 * matters.
 */
    public void setLocation(Point pnt)
    {
        flag.setLocation(pnt);
    }
/**
 * 
 */
    public int getX()
    {
        return flag.x;
    }
/**
 * 
 */
    public int getY()
    {
        return flag.y;
    }
/**
 * 
 */
    public void setBackgroundColor(Color clr)
    {
        markerColor = clr;
    }
/**
 * 
 */
    public void setTextColor(Color clr)
    {
        textColor = clr;
    }

/**
 * 
 */
    public boolean contains(int x, int y)
    {
      return flag.contains(x, y);
    }
/**
 * 
 */
    public boolean contains(Point pt)
    {
      return flag.contains(pt);
    }

/**
 * Force a redraw. If phase has changed reset position, color, and description. 
 */
    public void redraw()
    {
        setLocation( wfp.pixelOfTime(ph.getTime()) );
    }

/**
 * Move the pick marker to this location on the x-axis and repaint to show the move.
 * Note that the WFPanel.paint() method does the actual drawing.
 */
    public void setLocation(int x) {
    // calc. the union rectangle to minimize the repaint
        Rectangle newFlag = rectAtPixel(x);                // new flag

        Rectangle uRect = flag.union(newFlag);                // old flag
        wfp.repaint( uRect );

    // move the vertical line
        uRect = new Rectangle(flag.x, 0, 1, wfp.getHeight());
        wfp.repaint( uRect );

        flag = newFlag;
    }
/**
 * Translate the pick marker by this (x) offset and repaint to show the move.
 */
    public void translate(int x) {
        setLocation(flag.x+x);
    }


/*
 * Draw the PickFlag in the Graphics window. Check the Viewport limits to insure
 * that the flag is always in view in a zoomable WFPanel. If Phase.isDeleted()
 * == true the flag will NOT be drawn. */
// TODO: handle case where flags overlap

    public void draw(Graphics g) {

        //System.out.println("DEBUG: pickFlag ph.isDeleted: "+ ph.isDeleted() + " flag: "+ toString());

        // don't draw a delete phase (later may want to add ability to show these)
        if (ph.isDeleted()) return; 


        // MUST RECALC. THIS EVERY TIME BECAUSE FRAME MIGHT HAVE RESCALED!
     // Returns flag.y=0 so must call before setting flag.y
        flag = rectAtTime(ph.getTime()) ;

        // lookup origin's color code
        markerColor = wfp.wfv.mv.solList.getColorOf(ph.getAssociatedSolution());
        
        if (setMarkerColorByPhase) {
            markerColor = ( ph.isP() ) ? colorP : colorS;
            textColor = ( ph.isP() ) ? colorPtext : colorStext;
        }

        Color oldColor = g.getColor();
        g.setColor(markerColor);

        // "pole" part
        int xpole = flag.x;

        // flip flag to other side of pole if left handed
        if (handedness == LEFT) xpole += width;
            g.drawLine(xpole, 0, xpole, wfp.getHeight());

        // the flag part
        if (showDescription) {
            // flag part        
            if (ph.isReject())  // aww 06/22/2006
              g.draw3DRect(flag.x, flag.y, flag.width, flag.height, false);
            else {
              if (!ph.contributes()) g.setColor(DEFAULT_UNUSED_PICK_COLOR);
              g.fillRect(flag.x, flag.y, flag.width, flag.height);
              g.setColor(markerColor);
              if (!ph.contributes()) g.draw3DRect(flag.x, flag.y, flag.width, flag.height, false);
            }

            // write phase description text in the flag
            g.setColor(textColor);

            Font oldFont = g.getFont(); // -aww 2008/02/09
            g.setFont(font); // -aww 2008/02/09
            //g.drawString(ph.description.toShortString(),
            //g.drawString(ph.description.toString(ph.isReject()),  // aww 06/22/2006
            if (ph.isAuto()) g.drawString("*"+ph.description.toString(), flag.x+2, flag.y+flag.height-2);
            else g.drawString(ph.description.toString(), flag.x+2, flag.y+flag.height-2);
            g.setFont(oldFont); // -aww 2008/02/09
        }

        // draw residual mark (negative residuals are too early, so residual must be
     // SUBTRACTED from observed
        int resX = 0;
        int resY = 0;
        Rectangle rec = wfp.getVisibleRect();
        double resid = ph.residual.doubleValue();
        if (showResidual && !Double.isNaN(resid) ) {
            g.setColor(dtColor);
            resY = rec.y + rec.height - 3;
            resX = wfp.pixelOfTime(ph.getTime() - resid);
            resX = (int) Math.round(wfp.getPixelsPerSecond() * resid);
            if (resX > 0) g.fillRect(xpole-resX, resY, resX, 2);
            else g.fillRect(xpole, resY, Math.abs(resX), 2);
        }
                
        if (showDeltaTime && !ph.deltaTime.isNull() && ph.getDeltaTime() > 0.)  {
            // TEST code -aww 2010/01/24
            g.setColor(dtColor);
            //resY = (int) (wfp.getVisibleRect().height/2. + 2.);
            resY = (int) Math.round(rec.y + rec.height/2. + 1.);
            resX = (int) Math.round(wfp.getPixelsPerSecond() * ph.getDeltaTime());
            g.fillRect(xpole-resX/2, resY, resX, 2);
            // END OF TEST code -aww 2010/01/24
        }

        g.setColor(oldColor);
    }

/*
 * Force redraw of the part of the parent container where a deleted pick used to be.
 * This depends on the WFPanels paint() method recognizing and NOT repainting a deleted pick.
 */
    public void undraw() {
        Rectangle uRect = flag.union(flag);
        wfp.repaint( uRect );

    // move the vertical line
        uRect = new Rectangle(flag.x-1, 0, 2, wfp.getHeight());
        wfp.repaint( uRect );
    }


    public String toString() {
        return flag.toString()+ " " + ph.description.toString() + " color="+markerColor;
    }

} // end of class

