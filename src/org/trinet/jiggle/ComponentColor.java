package org.trinet.jiggle;

import org.trinet.util.graphics.ColorList;
import java.awt.Color;
import java.util.*;
import org.trinet.jasi.Channel;
import org.trinet.jasi.Channelable;

/**
 * ComponentColor.java
 */

// 4/7/2007 - DDG - added HN_ channels and changed BH_ to navyBlue because medBrown
//                  was used for acceleration channels
// 5/18/07 - DDG - changed to use only the 1st 2-characters of the SEED channel
//                 Downholes like HH1,2,3 weren't recognized. 
//                 Also, added HG_.
//                 Also, changed 'gray' to 'dark_gray' because the cursor disappeared
//                 behind gray waveforms. (Because that was declared a foreground color?)

public class ComponentColor {

    private static Color colorDefault = Color.darkGray;    
    private static boolean oldStyle = false;
    static Color DEFAULT_FG = colorDefault;    
    static Color DEFAULT_BG = Color.white;    

    static Color DEFAULT_SELECTED = ColorList.skyBlue;
    static Color DEFAULT_HIGHLIGHT = Color.yellow;
    static Color DEFAULT_DRAG = Color.orange;
    static Color DEFAULT_SEGMENT = Color.cyan;

    static Color DEFAULT_CUE = ColorList.mint;

    static Color DEFAULT_SIGHTLINE = Color.gray;
 
    static final HashMap colorMap = new HashMap(41);

    static {
        //
        colorMap.put("BH", ColorList.navyBlue);
        colorMap.put("HH", Color.black);
        colorMap.put("HL", ColorList.medBrown);
        colorMap.put("HN", ColorList.medBrown);
        colorMap.put("HG", ColorList.medBrown);
        colorMap.put("VH", Color.blue);
        colorMap.put("VL", ColorList.medBrown);
        colorMap.put("EH", Color.blue);
        colorMap.put("EL", ColorList.medBrown);
        colorMap.put("SH", Color.darkGray);
        //
        colorMap.put("BH.wave", ColorList.navyBlue);
        colorMap.put("HH.wave", Color.black);
        colorMap.put("HL.wave", ColorList.medBrown);
        colorMap.put("HN.wave", ColorList.medBrown);
        colorMap.put("HG.wave", ColorList.medBrown);
        colorMap.put("VH.wave", Color.blue);
        colorMap.put("VL.wave", ColorList.medBrown);
        colorMap.put("EH.wave", Color.blue);
        colorMap.put("EL.wave", ColorList.medBrown);
        colorMap.put("SH.wave", Color.darkGray);
        //
        colorMap.put("BH.panel", DEFAULT_BG);
        colorMap.put("HH.panel", DEFAULT_BG);
        colorMap.put("HL.panel", DEFAULT_BG);
        colorMap.put("HN.panel", DEFAULT_BG);
        colorMap.put("HG.panel", DEFAULT_BG);
        colorMap.put("VH.panel", DEFAULT_BG);
        colorMap.put("VL.panel", DEFAULT_BG);
        colorMap.put("EH.panel", DEFAULT_BG);
        colorMap.put("EL.panel", DEFAULT_BG);
        colorMap.put("SH.panel", DEFAULT_BG);
        //
    }

    /** Return the drawing color component.  */
    private static Color get(Channelable chan) {
        if (chan == null) return DEFAULT_FG;
        return get(chan.getChannelObj());
    }

    /** Return the drawing color for component.  */
    private static Color get(Channel chan) {
        if (chan == null) return DEFAULT_FG;
        return get(chan.getSeedchan());
    }

    /** * Return the drawing color for component.  */
    private static Color get(String seedchan) {
        if (seedchan == null || seedchan.length() < 2) return DEFAULT_FG;
        Color color = (Color) colorMap.get(seedchan.substring(0,2));
        if (color == null) color = DEFAULT_FG;
        return color;
    }

    /** Return the drawing color component.  */
    public static Color getBackground(Channelable chan) {
        if (chan == null) return DEFAULT_BG;
        return getBackground(chan.getChannelObj());
    }

    /** Return the drawing color for component.  */
    public static Color getBackground(Channel chan) {
        if (chan == null) return DEFAULT_BG;
        return getBackground(chan.getSeedchan());
    }

    /** * Return the drawing color for component.  */
    public static Color getBackground(String seedchan) {
        if (seedchan == null || seedchan.length() < 2) return DEFAULT_BG;
        Color color = (Color) colorMap.get(seedchan.substring(0,2)+".panel");
        if (color == null) color = DEFAULT_BG;
        return color;
    }
    /** Return the drawing color component.  */
    public static Color getForeground(Channelable chan) {
        if (chan == null) return DEFAULT_FG;
        return getForeground(chan.getChannelObj());
    }

    /** Return the drawing color for component.  */
    public static Color getForeground(Channel chan) {
        if (chan == null) return DEFAULT_FG;
        return getForeground(chan.getSeedchan());
    }

    /** * Return the drawing color for component.  */
    public static Color getForeground(String seedchan) {
        if (seedchan == null || seedchan.length() < 2) return DEFAULT_FG;
        Color color = (Color) colorMap.get(seedchan.substring(0,2)+".wave");
        if (color == null) color = get(seedchan); // backwards compatible ? -aww
        if (color == null) color = DEFAULT_FG;
        return color;
    }

    public static void put(String seedchan, Color c) {
        colorMap.put(seedchan,c);
    }

    public static String toPropertyString() {
        Set set = colorMap.entrySet();
        int size = set.size();
        StringBuffer sb = new StringBuffer(32*(size + 20));
        Iterator iter = set.iterator();
        Map.Entry me = null;
        Color c = null;
        String hexValue = null; 

        sb.append("color.pick.sightline=" + Integer.toHexString(ComponentColor.DEFAULT_SIGHTLINE.getRGB())).append("\n");

        while (iter.hasNext()) {
            me = (Map.Entry) iter.next();
            c = (Color) me.getValue();
            hexValue = (c == null) ? "" : Integer.toHexString(c.getRGB()); 
            sb.append("color.seedchan.").append((String)me.getKey()).append(" = ").append(hexValue).append("\n");
        }

        sb.append("color.wfpanel.default.bg=" + Integer.toHexString(ComponentColor.DEFAULT_BG.getRGB())).append("\n");
        sb.append("color.wfpanel.default.fg=" + Integer.toHexString(ComponentColor.DEFAULT_FG.getRGB())).append("\n");
        sb.append("color.wfpanel.default.selected=" + Integer.toHexString(ComponentColor.DEFAULT_SELECTED.getRGB())).append("\n");
        sb.append("color.wfpanel.default.highlight=" + Integer.toHexString(ComponentColor.DEFAULT_HIGHLIGHT.getRGB())).append("\n");
        sb.append("color.wfpanel.default.drag=" + Integer.toHexString(ComponentColor.DEFAULT_DRAG.getRGB())).append("\n");
        sb.append("color.wfpanel.default.segment=" + Integer.toHexString(ComponentColor.DEFAULT_SEGMENT.getRGB())).append("\n");
        sb.append("color.wfpanel.default.cue=" + Integer.toHexString(ComponentColor.DEFAULT_CUE.getRGB())).append("\n");

        return sb.toString();
    }

    public static void fromProperties(JiggleProperties props) {
        Color c = props.getColor("color.wfpanel.default.fg");
        if (c != null) ComponentColor.DEFAULT_FG = c;

        c = props.getColor("color.wfpanel.default.bg");
        if (c != null) ComponentColor.DEFAULT_BG = c;
        WFPanel.unselectedColor = ComponentColor.DEFAULT_BG;

        c = props.getColor("color.wfpanel.default.selected");
        if (c != null) ComponentColor.DEFAULT_SELECTED = c;
        WFPanel.selectedColor = ComponentColor.DEFAULT_SELECTED;

        c = props.getColor("color.wfpanel.default.highlight");
        if (c != null) ComponentColor.DEFAULT_HIGHLIGHT = c;
        WFPanel.highlightColor = ComponentColor.DEFAULT_HIGHLIGHT;

        c = props.getColor("color.wfpanel.default.drag");
        if (c != null) ComponentColor.DEFAULT_DRAG = c;
        WFPanel.dragColor = ComponentColor.DEFAULT_DRAG;

        c = props.getColor("color.wfpanel.default.segment");
        if (c != null) ComponentColor.DEFAULT_SEGMENT = c;
        WFPanel.segmentEdgeColor = ComponentColor.DEFAULT_SEGMENT;

        c = props.getColor("color.wfpanel.default.cue");
        if (c != null) ComponentColor.DEFAULT_CUE = c;
        PhaseCue.setDefaultColor(ComponentColor.DEFAULT_CUE);

        c = props.getColor("color.pick.sightline");
        if (c != null) ComponentColor.DEFAULT_SIGHTLINE = c;
        ZoomPanel.bombSightColor = ComponentColor.DEFAULT_SIGHTLINE;

        String key = null;
        for (Enumeration e = props.propertyNames(); e.hasMoreElements();)  {
            key = (String) e.nextElement();
            if ( ! key.startsWith("color.seedchan")) continue; // discriminate from "catalog" or other color type -aww 2008/06/11
            // TEMP logic here - switch old style "color.seedchan.XX" type to XX.wave prop for input -aww 2008/08/02
            c = props.getColor(key); // getColor() returns null if unparseable
            if (key.length() == 17) {
                key = key.substring(15) + ".wave";
                oldStyle = true;
            }
            else key = key.substring(15);
            ComponentColor.put(key.intern(), c);
        }
    }

    public static void toProperties(JiggleProperties props) {
        Set set = colorMap.entrySet();
        int size = set.size();
        if (size == 0) return;

        Iterator iter = set.iterator();
        Map.Entry me = null;
        Color c = null;
        String key = null; 
        String value = null; 

        props.setProperty("color.pick.sightline", DEFAULT_SIGHTLINE);

        while (iter.hasNext()) {
            me = (Map.Entry) iter.next();
            c = (Color) me.getValue();
            key = (String) me.getKey();
            if (! oldStyle && key.length()  < 3) continue; // TEMP here - skip old style "color.seedchan.XX" type props for output -aww 2008/08/02
            else if (key.indexOf(".") == -1) key = key+".wave";

            value = (c == null) ? "" : Integer.toHexString(c.getRGB()); 
            if (value != "") props.setProperty("color.seedchan."+key, value);
        }

        props.setProperty("color.wfpanel.default.bg", DEFAULT_BG); 
        props.setProperty("color.wfpanel.default.fg", DEFAULT_FG); 

        props.setProperty("color.wfpanel.default.selected", DEFAULT_SELECTED);
        props.setProperty("color.wfpanel.default.highlight", DEFAULT_HIGHLIGHT);
        props.setProperty("color.wfpanel.default.drag", DEFAULT_DRAG);
        props.setProperty("color.wfpanel.default.segment", DEFAULT_SEGMENT);
        props.setProperty("color.wfpanel.default.cue", DEFAULT_CUE);

    }

} // ComponentColor
