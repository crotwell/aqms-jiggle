package org.trinet.jiggle;

import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.net.URL;

public class JiggleAuthenticator extends Authenticator {
  /**
   * Setup the authenticator that will be used by the networking code when an HTTP
   * server asks for authentication.
   * 
   * @param props the properties.
   */
  public static void setup(JiggleProperties props) {
    final String path = props.getProperty("focmec.URL.path", "");
    if (path.isEmpty()) {
      return;
    }
    final String userName = props.getProperty("focmec.URL.user", "");
    if (userName.isEmpty()) {
      return;
    }
    final char[] password = props.getProperty("focmec.URL.password", "").toCharArray();
    final PasswordAuthentication passwordAuthentication = new PasswordAuthentication(userName, password);
    Authenticator.setDefault(new JiggleAuthenticator(passwordAuthentication, path));
  }

  private final PasswordAuthentication passwordAuthentication;
  private final String path;

  private JiggleAuthenticator(PasswordAuthentication passwordAuthentication, String path) {
    this.passwordAuthentication = passwordAuthentication;
    this.path = path;
  }

  @Override
  protected PasswordAuthentication getPasswordAuthentication() {
    final URL url = getRequestingURL();
    if (url != null && url.toString().startsWith(path)) {
      return passwordAuthentication;
    }
    return super.getPasswordAuthentication();
  }
}
