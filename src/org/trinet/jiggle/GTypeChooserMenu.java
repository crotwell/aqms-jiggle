package org.trinet.jiggle;

/**
 * A Menu item chooser for the masterview's selected solution event type.
 */
import java.util.*;
import java.awt.Dimension;
import javax.swing.*;
import javax.swing.event.*;
import java.awt.event.*;
import org.trinet.jasi.*;
import org.trinet.util.*;

public class GTypeChooserMenu extends JPopupMenu implements ListDataStateListener {

    private GType [] gtypes = GTypeMap.getGTypes();
    private SolutionPanel solPanel = null;

    protected GType selectedGType = null;

    /** Create an GTypeChooserMenu. Set the current selection to item given. */
    public GTypeChooserMenu(SolutionPanel solPanel, String selectedTypeId) {
        super();
        this.solPanel = solPanel;
        initMenu(selectedTypeId);
    }

    protected void initMenu(String selectedTypeId) {

        if (getComponentCount() > 0) removeAll();

        ActionListener al = new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                setSelectedItemByDbType(evt.getActionCommand());
            }
        };

        String dbCode = (selectedTypeId == null) ? GTypeMap.N : selectedTypeId;

        JMenuItem jmi = null;
        for (int i = 0; i< gtypes.length; i++) {

            if (gtypes[i].code.equalsIgnoreCase(dbCode)) selectedGType = gtypes[i];
            jmi = new JMenuItem(gtypes[i].code.toUpperCase());
            jmi.setToolTipText(gtypes[i].name);
            jmi.addActionListener(al);
            add(jmi);

            if ( i != gtypes.length-1) addSeparator();
        }

    }

    public void setSelectedItemByJasiType(String name) {
        //System.out.println("GTypeChooserMenu setSelectedItemByJasiType name: " + name);
        for (int idx = 0; idx < gtypes.length; idx++) {
            if (gtypes[idx].name.equals(name)) {
                selectedGType = gtypes[idx];
                if (solPanel != null) {
                    solPanel.setOriginGType(null);
                }
                return;
            }
        }
        System.out.println("GTypeChooserMenu setSelectedItemByJasiType FAILED TO FIND TYPE: " + name);
    }

    public void setSelectedItemByDbType(String code) {
        //System.out.println("GTypeChooserMenu setSelectedItemByDbType code: " + code);
        for (int idx = 0; idx < gtypes.length; idx++) {
            if (gtypes[idx].code.equalsIgnoreCase(code)) {
                selectedGType = gtypes[idx];
                if (solPanel != null) {
                    solPanel.setOriginGType(null);
                }
                return;
            }
        }
        System.out.println("GTypeChooserMenu setSelectedItemByDbType, FAILED TO FIND TYPE: " + code);
    }

    protected void updateGTypeSelection(Solution sol) {
      final String code = GTypeMap.toDbType(sol.getOriginGTypeString());
      //System.out.println("GTypeChooserMenu updateGTypeSelection name: " + name);
      if (selectedGType.code.equals(code)) return; // aww
      SwingUtilities.invokeLater(
          new Runnable() {
              public void run() {
                  setSelectedItemByDbType(code);
              }
          }
      );
    }

    // ListDataStateListener for list changes -aww
    public void intervalAdded(ListDataStateEvent e) {
       Object obj = e.getSource();
       if (! (obj instanceof SolutionList)) return;
       updateGTypeSelection((Solution)((SolutionList)obj).getSelected());
    }
    public void intervalRemoved(ListDataStateEvent e) {
       Object obj = e.getSource();
       if (! (obj instanceof SolutionList)) return;
       updateGTypeSelection((Solution)((SolutionList)obj).getSelected());
    }
    public void contentsChanged(ListDataStateEvent e) {
       Object obj = e.getSource();
       if (! (obj instanceof SolutionList)) return;
       updateGTypeSelection((Solution)((SolutionList)obj).getSelected());
    }
    public void orderChanged(ListDataStateEvent e) {
       Object obj = e.getSource();
       if (! (obj instanceof SolutionList)) return;
       updateGTypeSelection((Solution)((SolutionList)obj).getSelected());
    }
    public void stateChanged(ListDataStateEvent e) {
       Object obj = e.getSource();
       if (! (obj instanceof SolutionList)) return;
       updateGTypeSelection((Solution)((SolutionList)obj).getSelected());
    }

} // GTypeChooserMenu
