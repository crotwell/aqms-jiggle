package org.trinet.jiggle;

import gov.usgs.swarm.JiggleSwarmConfig;
import gov.usgs.swarm.Metadata;
import gov.usgs.swarm.data.CachedDataSource;
import gov.usgs.swarm.data.GulperListener;
import gov.usgs.util.CurrentTime;
import gov.usgs.util.Util;
import gov.usgs.vdx.data.heli.HelicorderData;
import gov.usgs.vdx.data.wave.Wave;

import java.text.DecimalFormat;
import java.util.List;
import java.util.ArrayList;

import org.trinet.jasi.ChannelGain;
import org.trinet.jasi.ChannelName;
import org.trinet.jasi.Waveform;
import org.trinet.jasi.WFSegment;
import org.trinet.jasi.Units;
import org.trinet.util.LeapSeconds;
import org.trinet.util.TimeSpan;
import org.trinet.util.gazetteer.LatLonZ;

public class JiggleSwarmDataSource extends CachedDataSource {
    public Jiggle jiggle = null;
    private String scnl = null;
    private String filterName = null;
    private WFView wfv = null;
    private ChannelGain gain = null;
    public static JiggleSwarmConfig config = JiggleSwarmConfig.createConfig(new String [0]);

    DecimalFormat df = new DecimalFormat("####0.0#");
    DecimalFormat df2 = new DecimalFormat("0.##");
    DecimalFormat dsci = new DecimalFormat("0.##E0");

    public JiggleSwarmDataSource(Jiggle jiggle) {
            this.jiggle = jiggle;
            setWF();
    }

    public void flush() {
        flushWaves();
        flushHelicorders();
    }

    public String getFilterName() {
        return filterName;
    }

    public void setWF() {
        try {
            if (! isEmpty()) {
                //System.out.println("JSDS debug setWF() flush old wave");
                flush();
            }
            this.wfv = jiggle.mv.masterWFViewModel.get();
            //System.out.println("setWF wfv: " + ((wfv  == null) ? "null" : wfv.getChannelObj().getChannelName()));
            if (wfv != null) {
              ChannelName cn = wfv.getChannelObj().getChannelName();
              this.scnl = cn.getSta() + " " + cn.getSeedchan() + " " + cn.getNet() + " " + cn.getLocation();
              //Waveform wf = wfv.getWaveform();
              Waveform wf = jiggle.pickPanel.zwfp.getWf();
              if (wf != null) {
                Metadata md = config.getMetadata(this.scnl, true);
                if (md != null) {
                  LatLonZ llz = wf.getChannelObj().getLatLonZ();
                  if (! llz.isNull()) {
                    md.updateLatitude(llz.getLat());
                    md.updateLongitude(llz.getLon());
                    md.updateUnits(org.trinet.jasi.Units.getString(wf.getAmpUnits()));
                  }
                }
                this.filterName = wf.getFilterName();

                this.gain = wfv.getChannelObj().getGain(wf.getEpochStart());
                if (! this.gain.isNull()) {
                    /*
                    if (md != null ) {
                        md.updateLinearCoefficients(1./gain.doubleValue(), -wf.getBias());
                        md.updateUnits(gain.getUnitsString());
                    }
                    */

                }

                boolean hasTS = wf.hasTimeSeries();
                if (! hasTS) {
                    hasTS = wf.loadTimeSeries();
                }
                if (hasTS) {
                  WFSegment [] wfs = wf.getArray();
                  float [] ts = null;
                  int   [] its = null;
                  Wave wave = null;
                  int units = wf.getAmpUnits();
                  //System.out.println("DEBUG JSDS getWF units are COUNTS");
                  if (units == Units.COUNTS) {
                    for (int i=0; i < wfs.length; i++) {
                      ts = wfs[i].getTimeSeries();
                      its = new int [ts.length];
                      //System.out.println("JSDS debug setWF() timeseries length: " + its.length);
                      for (int idx = 0; idx < its.length; idx++) {
                         its[idx] = (int) ts[idx]; // assumes integral amp in counts ?
                      }
                      wave = new Wave(its,
                              Util.ewToJ2K(LeapSeconds.trueToNominal(wfs[i].getEpochStart())), // convert from UTC to J2K
                              (double)Math.round(1./wfs[i].getSampleInterval()));
                      cacheWaveAsHelicorder(scnl, wave);
                      putWave(scnl, wave);
                    }
                  }
                  //
                  //NOTE: to use jiggle.pickPanel.zwfp.getWf() for "filtering" need listener to filtering button action
                  else {
                    units = Units.getCGSunit(units);
                    if (units != 0) {
                      String str = "Unit:"+org.trinet.jasi.Units.getString(wf.getAmpUnits()) + " x 1.E06";
                      //System.out.println(str);
                      md.interpret(str);
                      for (int i=0; i < wfs.length; i++) {
                        ts = wfs[i].getTimeSeries();
                        its = new int [ts.length];
                        //System.out.println("JSDS debug setWF() timeseries length: " + its.length);
                        for (int idx = 0; idx < its.length; idx++) {
                          its[idx] = (int) Math.round(1.E6 * Units.convertToCGS(ts[idx], units));
                        }
                        wave = new Wave(its,
                                  Util.ewToJ2K(LeapSeconds.trueToNominal(wfs[i].getEpochStart())), // convert from UTC to J2K
                                  (double)Math.round(1./wfs[i].getSampleInterval()));
                        cacheWaveAsHelicorder(scnl, wave);
                        putWave(scnl, wave);
                      }
                    }
                  }
                  //
                }
                else System.out.println("JSDS setWF() " + scnl + " waveform has no timeseries.");
              }
            }
            setName(scnl + "; jiggle");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
     
    public HelicorderData getHelicorder(String scnl, double t1, double t2, GulperListener gl)
    {
        double dt = t2 - t1;
        double now = CurrentTime.getInstance().nowJ2K(); // times used here J2K,  not UTC ?
        if (Math.abs(now - t2) < 3600) // what's this about?
        {
            t2 = Util.ewToJ2K(LeapSeconds.trueToNominal(wfv.getWaveform().getEpochEnd())); // convert from UTC to J2K
            t1 = t2 - dt;
        }
        return getHelicorder(scnl, t1, t2, gl);
    }
    
    public List<String> getChannels()
    {
        List<String> list = new ArrayList<String>();
        list.add(scnl);
        return list;
    }

    public TimeSpan getWaveformTimeSpan()
    {
        //What timespan do we use here ?
        TimeSpan ts = null;
        ts = jiggle.mv.masterWFWindowModel.getTimeSpan();
        //System.out.println("JSDS timespan: " + ts.toString());
        //ts = wfv.getWaveform().getTimeSpan();
        return new TimeSpan(
                     Util.ewToJ2K( LeapSeconds.trueToNominal(ts.getStart())),
                     Util.ewToJ2K( LeapSeconds.trueToNominal(ts.getEnd()))
                   ); // convert from UTC to J2K
    }

    public Wave getWave(String scnl, double t1, double t2) 
    {
          return getBestWave(scnl, t1, t2);
    }
    
    public String toConfigString()
    {
        return scnl + ";jiggle:gain:" +  gain.toString();
    }
    

    public MasterView getMasterView() {
        return jiggle.mv;
    }

    public String toTitleString() {
        String header = scnl + ((gain == null) ? "" : gain.toString());
        if (wfv != null) {
          Waveform wf = wfv.getWaveform();
          if ( wf != null && wf.hasTimeSeries() ) {
            DecimalFormat fmt = df;
            double maxAmp = Math.abs( wf.getMaxAmp() ); 
            double bias = wf.getBias();
            if (maxAmp < 1.0) fmt = dsci;  // use Sci. Notatation for small numbers
            else if (maxAmp < 10) fmt = df2;
            header +=
               " " + df.format(wfv.getHorizontalDistance()) + " Km" +
               " (bias, +units/-units) = (" + fmt.format((int) bias ) +
               ", " + fmt.format(maxAmp-bias) + "/" + fmt.format(wf.getMinAmp()-bias) + ") ";
             //filterName = wf.getFilterName();
             if (filterName != null && filterName.trim().length() > 0 ) header += " filter= " + filterName;
          } else {
            header += " No time series.";
          }
        }

        return header;
    }

    
    public void flushHelicorders() {
        helicorderCache.clear();// = new HashMap();
        if (jiggle.getProperties().getBoolean("debug")) System.out.println("Helicorder Cache Flushed");
    }
    
    public void flushWaves() {
        waveCache.clear();// = new HashMap();    
        if (jiggle.getProperties().getBoolean("debug")) System.out.println("Wave Cache Flushed");
    }


}
