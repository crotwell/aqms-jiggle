package org.trinet.jiggle;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*; 
import javax.swing.border.*;

import org.trinet.jdbc.*;
import org.trinet.jasi.*;

/**
 * This is the Pop-up dialog that appears after a new phase pick is made or when
 * you want to edit an existing phase pick.  */

public class PhaseDialog extends JDialog {

    WFPanel wfp = null;

    Phase ph = null;
    MasterView mv = null;

    int wt = 0;        

    //String phaseChoice[] = { "P", "S", "Pn", "Sn", "Pg"};
    String phaseChoice[] = { "P", "S"};

    JComboBox phaseCombo = null;
    SolutionListComboBox solListCombo = null; // to select association
    JLabel phLabel = null;
    RadioListener radioListener = new RadioListener(); // all buttons will register same instance
    JRadioButton buttonXfm = null;

    protected boolean phaseDescWtCheck = true;
    protected boolean alwaysResetLowQualFm = false;

    /**
     * Need Phase, MasterView and WFView to completely handle all the bookkeeping for changing, adding, deleting phases
     */
    public PhaseDialog(Frame frame, Component comp, WFPanel wfp, MasterView mv, Phase inPh) {

        super(frame, "Phase Description", true);

        //Set our location (0,0) relative to the given component
        //setLocationRelativeTo(comp);

        this.wfp = wfp;
        this.ph = inPh;
        this.mv = mv;

        // the main panel
        JPanel mainPanel = new JPanel(); // the main dialog panel
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
        mainPanel.add(createDescPanel());
        mainPanel.add(createAssocPanel());
        mainPanel.add(createExpressPanel());
        mainPanel.add(createRejectPanel()); // aww 06/22/2006
        mainPanel.add(createButtonPanel());

        getContentPane().add(mainPanel);
        pack();
        show();
    }

    JComponent createAssocPanel() {
        JPanel assocPanel = new JPanel();
        assocPanel.setBorder( new TitledBorder("Association") );
        solListCombo = new SolutionListComboBox(mv.solList);
        // Set selected Solution in the comboBox to the one in the Masterview's model
        solListCombo.addActionListener(new SolutionComboHandler());
        assocPanel.add(solListCombo);
        return assocPanel;
    }

    JComponent createRejectPanel() {
        Box rejectPanel = Box.createHorizontalBox();
        final JCheckBox wtCheckBox = new JCheckBox("Reject from location");
        wtCheckBox.setSelected(ph.isReject());
        wtCheckBox.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ph.setReject(wtCheckBox.isSelected());
            }
        });
        wtCheckBox.setHorizontalTextPosition(SwingConstants.LEADING);
        rejectPanel.add(wtCheckBox);
        return rejectPanel;
    }

    JComponent createDescPanel() {
        Box descPanel = Box.createVerticalBox();
        descPanel.setBorder( new TitledBorder("Phase Description") );
        descPanel.add(createPhPanel());
        descPanel.add(createIePanel());
        descPanel.add(createFmPanel());
        descPanel.add(createWtPanel());
        return descPanel;
    }

    JComponent createButtonPanel() {
        // define the buttons: [OK] [Delete]       [Cancel]?
        JPanel buttonPanel = new JPanel();        
        JButton okButton = new JButton("OK");
        okButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                OKPressed();
            }
        });
        buttonPanel.add( okButton );
        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                CancelPressed();
            }
        });
        buttonPanel.add( cancelButton );
        return buttonPanel;
    }

    JComponent createPhPanel() {
        JPanel panel = new JPanel(new GridLayout(1,2)); // Box.createHorizontalBox();
        panel.add(new JLabel("Phase ", JLabel.LEFT));

        phaseCombo = makeComboBox(phaseChoice);
        phaseCombo.setEditable(true);
        phaseCombo.setToolTipText("Only phase type, don't type a 1st motion, qual, or wt here"); 
        phaseCombo.setPreferredSize(new Dimension(50,20));
        phaseCombo.setMaximumSize(new Dimension(50,20));
        // set default values to match current phase
        phaseCombo.setSelectedItem(ph.description.iphase);
        phaseCombo.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                ph.description.iphase = (String) phaseCombo.getSelectedItem();
                phLabel.setText(ph.description.toString());
            }
        });
        Box box = Box.createHorizontalBox();
        box.add(phaseCombo);
        box.add(Box.createHorizontalStrut(10));
        phLabel = new JLabel(ph.description.toString());
        box.add(phLabel);
        box.add(Box.createHorizontalGlue());
        panel.add(box);
        return panel;
    }

    JComponent createIePanel() {
        JRadioButton button1 = makeButton("i", 'i');
        JRadioButton button2 = makeButton("e", 'e');
        JRadioButton button3 = makeButton("w", 'w');
        JRadioButton button4 = makeButton(" ", ' ');

        // Group the radio buttons.
        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(button1);
        buttonGroup.add(button2);
        buttonGroup.add(button3);
        buttonGroup.add(button4);
        if (ph.description.ei.equals("i")) button1.setSelected(true);
        else if (ph.description.ei.equals("e")) button2.setSelected(true);
        else if (ph.description.ei.equals("w")) button3.setSelected(true);
        else button4.setSelected(true);

        JPanel panel = new JPanel(new GridLayout(1,2));
        panel.add(new JLabel("Onset ", JLabel.LEFT) );
        Box box = Box.createHorizontalBox();
        box.add(button1);
        box.add(button2);
        //box.add(button3); // don't show w
        //box.add(button4); // don't show blank
        panel.add(box);
        return panel;
    }

    JComponent createFmPanel() {
        JRadioButton buttonC = makeButton("c", 'c');
        JRadioButton buttonD = makeButton("d", 'd');
        JRadioButton buttonP = makeButton("+", '+');
        JRadioButton buttonM = makeButton("-", '-');
        JRadioButton buttonU = makeButton("u", 'u');
        JRadioButton buttonR = makeButton("r", 'r');
        buttonXfm = makeButton(".", '.');

        // Group the radio buttons.
        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(buttonC);
        buttonGroup.add(buttonD);
        buttonGroup.add(buttonP);
        buttonGroup.add(buttonM);
        buttonGroup.add(buttonU);
        buttonGroup.add(buttonR);
        buttonGroup.add(buttonXfm);

        JPanel panel = new JPanel(new GridLayout(1,2));
        panel.add(new JLabel("1st Mo ", JLabel.LEFT) );

        Box box = Box.createHorizontalBox();
        box.add(buttonC);
        box.add(buttonD);
        box.add(buttonP);
        box.add(buttonM);
        //box.add(buttonU); // don't show long-period up
        //box.add(buttonR); // don't show long-period down
        box.add(buttonXfm);
        panel.add(box);

        if      (ph.description.fm.startsWith("c")) buttonC.setSelected(true);
        else if (ph.description.fm.startsWith("d")) buttonD.setSelected(true);
        else if (ph.description.fm.startsWith("+")) buttonP.setSelected(true);
        else if (ph.description.fm.startsWith("-")) buttonM.setSelected(true);
        else if (ph.description.fm.startsWith("u")) buttonU.setSelected(true);
        else if (ph.description.fm.startsWith("r")) buttonR.setSelected(true);
        else buttonXfm.setSelected(true); // punt as unknown

        return panel;

    }

    JComponent createWtPanel() {
        JRadioButton button0 = makeButton("0", '0');
        JRadioButton button1 = makeButton("1", '1');
        JRadioButton button2 = makeButton("2", '2');
        JRadioButton button3 = makeButton("3", '3');
        JRadioButton button4 = makeButton("4", '4');

       // Group the radio buttons.
        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(button0);
        buttonGroup.add(button1);
        buttonGroup.add(button2);
        buttonGroup.add(button3);
        buttonGroup.add(button4);

        JPanel panel = new JPanel(new GridLayout(1,2));
        panel.add(new JLabel("Weight ", JLabel.LEFT) );

        Box box = Box.createHorizontalBox();
        box.add(button0);
        box.add(button1);
        box.add(button2);
        box.add(button3);
        box.add(button4);
        panel.add(box);

        // convert 0->1 quality to 0-4 weight
        wt = ph.description.getWeight();
        switch (wt) {
          case 0:
              button0.setSelected(true); // default
              break;
          case 1:
              button1.setSelected(true);
              break;
          case 2:
              button2.setSelected(true);
              break;
          case 3:
              button3.setSelected(true);
              break;
          case 4:
              button4.setSelected(true);
              break;
          default:
            button0.setSelected(true); // default
        }

        return panel;
    }

    /**
     * Make the subPanel that has one-touch express buttons like 'IP0' ...
     */
    JComponent createExpressPanel() {
        JPanel pPanel = new JPanel();        
        JPanel sPanel = new JPanel();

        pPanel.setLayout(new BoxLayout(pPanel, BoxLayout.X_AXIS));
        sPanel.setLayout(new BoxLayout(sPanel, BoxLayout.X_AXIS));

        addExpressButton(new JButton("iP0"), pPanel);
        addExpressButton(new JButton("iP1"), pPanel);
        addExpressButton(new JButton("eP2"), pPanel);
        addExpressButton(new JButton("eP3"), pPanel);
        addExpressButton(new JButton("eP4"), pPanel);

        addExpressButton(new JButton("iS0"), sPanel);
        addExpressButton(new JButton("iS1"), sPanel);
        addExpressButton(new JButton("eS2"), sPanel);
        addExpressButton(new JButton("eS3"), sPanel);
        addExpressButton(new JButton("eS4"), sPanel);

        JPanel expPanel = new JPanel();
        expPanel.setLayout(new BoxLayout(expPanel, BoxLayout.Y_AXIS));

        expPanel.setBorder( new TitledBorder("Express Buttons") );

        expPanel.add(pPanel);
        expPanel.add(sPanel);

        return expPanel;
    }

    void addExpressButton(JButton btn, JPanel panel) {
        btn.addActionListener(new ExpressButtonHandler() );
        panel.add(btn);
    }


    class ExpressButtonHandler implements ActionListener {    
        public void actionPerformed (ActionEvent evt) {
          String str = evt.getActionCommand();

          // parse the short Phase descriptor string from the button (e.g. "eP2")
          String ei     = str.substring(0,1);
          String iphase = str.substring(1,2);        
          String weight = str.substring(2,3); 
          wt = 4; // note this resets as instance wt but closes dialog
          if (weight != null) {
            wt = Integer.parseInt(weight);
          }
          // Express Buttons don't have fm, keeps existing? 
          String fm = ph.description.fm;

          // Test fm wt quality
          if (ph.description.toQuality(wt) < PhaseDescription.getFmQualCut() && !fm.equals(PhaseDescription.DEFAULT_FM)) {
              boolean tf = false;
              if (phaseDescWtCheck) {
                  int ans = JOptionPane.showConfirmDialog(wfp.getTopLevelAncestor(),
                     "Weight below quality for first motion, remove first motion?", "Quality Check",
                     JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE);
                  tf = (ans == JOptionPane.YES_OPTION);
              }
              else tf = alwaysResetLowQualFm; 
              if (tf) {
                  ph.description.fm = PhaseDescription.DEFAULT_FM;
                  buttonXfm.setSelected(true);
              }
          }
          //ph.description.set(iphase, ei, fm, wt, true); // added test wt threshold for fm  -aww 2009/09/22
          ph.description.set(iphase, ei, fm, wt, false); // tf test threshold for fm  -aww 2011/04/12

          // ? Option: Add logic for fm quality test and comfirm dialog popup confirmation here ?
          ph.qualityToDeltaTime();

          // set new association to currently selected solution
          //Solution sol =  mv.getSelectedSolution(); // removed aww - 06/26/2006
          //ph.assign( sol ); // associate adds it to solution phaseList, see below addOrReplace // removed aww - 06/26/2006

          // add the phase to the WFView
          //sol.phaseList.addOrReplace(ph); //aww - removed 06/22/2006
          //sol.phaseList.addOrReplaceAllOfSameStaType(ph);  // aww added 06/22/2006 test
          mv.addReadingToCurrentSolution(ph);  // aww added 06/26/2006 test

          if (mv.getOwner() instanceof Jiggle) {
            Jiggle jiggle = (Jiggle) mv.getOwner(); 
            jiggle.updateStatusBarCounts();
          }

          //TODO: calculate and set dmin, residual, etc. OR leave null until location is recalc'ed.
          setVisible(false);

        }
    }


    JRadioButton makeButton(String cmd, char mnem) {
        JRadioButton rb = new JRadioButton(cmd);
        rb.setMnemonic(mnem);
        rb.setActionCommand(cmd);
        rb.addActionListener(radioListener);
        return rb;
    }

    public JComboBox makeComboBox(String[] choiceList) {
        JComboBox cb = new JComboBox(); 

        for (int i = 0; i< choiceList.length; i++) {
            cb.addItem(choiceList[i]);
        }

        cb.setEditable(true);                    // allow freeform user input
        cb.setSelectedItem(phaseChoice[0]);      // default selection
        cb.setMaximumRowCount(4);                // items displayed in scrolling window

        cb.setEditable(false);

        Dimension size = cb.getSize();

        cb.setSize(20, size.height);

        return cb;
    }

    public void CancelPressed() {
        this.setVisible(false);
    }

    public void OKPressed() {
        // time is already set by the caller
        //ph.description.iphase = (String) phaseCombo.getSelectedItem();
        // set new association to selected solution
        //ph.assign((Solution) mv.solList.getSelected() ); // associate adds it phaseList, see below addOrReplace - removed aww
        // Add it in Solution's list so listeners know
        //mv.getSelectedSolution().phaseList.addOrReplace(ph); //aww - removed 06/22/2006
        //mv.getSelectedSolution().phaseList.addOrReplaceAllOfSameStaType(ph);  // aww added 06/22/2006 test - removed aww
        mv.addReadingToCurrentSolution(ph);  // aww added 06/26/2006 test

        if (mv.getOwner() instanceof Jiggle) {
            Jiggle jiggle = (Jiggle) mv.getOwner(); 
            jiggle.updateStatusBarCounts();
        }

        // fm, qual & quality are changed dynamically by the radio buttons
 
        //TODO: calculate and set dmin, residual, etc. OR leave null until location is recalc'ed.

        this.setVisible(false);
    }

    class RadioListener implements ActionListener  {

        public void actionPerformed(ActionEvent e) {        // source is a radio button
   
            if (e.getActionCommand().equals("i"))  ph.description.ei = "i";
            else if (e.getActionCommand().equals("e"))  ph.description.ei = "e"; 
            else if (e.getActionCommand().equals("w"))  ph.description.ei = "w"; 
            else if (e.getActionCommand().equals(" "))  ph.description.ei = PhaseDescription.DEFAULT_TYPE; 

            else if (e.getActionCommand().equals("c"))  ph.description.fm = "c."; 
            else if (e.getActionCommand().equals("d"))  ph.description.fm = "d."; 
            else if (e.getActionCommand().equals("u"))  ph.description.fm = ".u"; 
            else if (e.getActionCommand().equals("r"))  ph.description.fm = ".r"; 
            else if (e.getActionCommand().equals("+"))  ph.description.fm = "+."; 
            else if (e.getActionCommand().equals("-"))  ph.description.fm = "-."; 
            else if (e.getActionCommand().equals("."))  ph.description.fm = PhaseDescription.DEFAULT_FM; //  ".."  
            // NOTE: DEFAULT new pick descriptor in Jiggle is writing "  " not ".." to db (this is apparent in the PickFlag string too) !

            else if (e.getActionCommand().equals("0"))  wt = 0; 
            else if (e.getActionCommand().equals("1"))  wt = 1; 
            else if (e.getActionCommand().equals("2"))  wt = 2; 
            else if (e.getActionCommand().equals("3"))  wt = 3; 
            else if (e.getActionCommand().equals("4"))  wt = 4; 

            // force quality cutoff test here -aww 11/03/05
            //ph.description.setQuality(PhaseDescription.toQuality(wt), true);  // convert to "quality", test for fm threshold
            ph.description.setQuality(PhaseDescription.toQuality(wt), false);  // removed test wt threshold for fm  -aww 2011/04/12
            ph.qualityToDeltaTime();  // resets deltim

            //Test fm on S
            if (ph.description.isS()) {
                 if (!PhaseDescription.getFmOnS() && !ph.description.fm.equals(PhaseDescription.DEFAULT_FM)) {
                   //int ans = JOptionPane.showConfirmDialog(wfp.getTopLevelAncestor(),
                   //  "First motion on S, remove first motion?", "Quality Check", JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE);
                   //if (ans == JOptionPane.YES_OPTION) 
                     ph.description.fm = PhaseDescription.DEFAULT_FM;
                     buttonXfm.setSelected(true);
                 }
            }
            //Test fm on horiz
            if (wfp.wfv.getChannelObj().isHorizontal()) {  // remove if fm not allowed on horiz 
               if (!PhaseDescription.getFmOnHoriz() && !ph.description.fm.equals(PhaseDescription.DEFAULT_FM)) {
                 //int ans = JOptionPane.showConfirmDialog(wfp.getTopLevelAncestor(),
                 //    "First motion on horizontal, remove first motion?", "Quality Check", JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE);
                 //if (ans == JOptionPane.YES_OPTION)
                     ph.description.fm = PhaseDescription.DEFAULT_FM;
                     buttonXfm.setSelected(true);
               }
            }
            // Test fm wt quality
            if (ph.description.getQuality() < PhaseDescription.getFmQualCut() && !ph.description.fm.equals(PhaseDescription.DEFAULT_FM)) {
                boolean tf = false;
                if (phaseDescWtCheck) {
                  int ans = JOptionPane.showConfirmDialog(wfp.getTopLevelAncestor(),
                     "Weight below quality for first motion, remove first motion?", "Quality Check",
                     JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE);
                  tf = (ans == JOptionPane.YES_OPTION);
                }
                else tf = alwaysResetLowQualFm; 
                if (tf) {
                  ph.description.fm = PhaseDescription.DEFAULT_FM;
                  buttonXfm.setSelected(true);
                }
            }

            //System.out.println("ph.description: " + ph.description.toString() + " 2hypostr: " + ph.description.toHypoinverseString());
            phLabel.setText(ph.description.toString());

        }
    }

    /** Handle changes to the selected Solution that are made in a SolutionCombobox. */
    class SolutionComboHandler implements ActionListener {
          // this is the action taken when the selectedOrigin is changed
          public void actionPerformed(ActionEvent e) {
              SolutionListComboBox jc = (SolutionListComboBox) e.getSource();
              mv.solList.setSelected( jc.getSelectedSolution() );        
          }
    }

}

