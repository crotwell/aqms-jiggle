package org.trinet.jiggle;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.trinet.jasi.ActiveList;
import org.trinet.util.StringList;
import org.trinet.util.graphics.IconImage;
import org.trinet.util.graphics.table.CatalogTable;

/**
 * Panel for insertion into dialog box for selecting catalog row colors
 * based upon event type (etype), origin (gtype), subsource (RT, Jiggle), processing state (rflag) etc.
 */
public class DPcatColors extends JPanel {

    private JiggleProperties newProps;    // new properties as changed by this dialog
    private CatalogRowColorChooserPanel rowColorChooserPanel = null;

    protected boolean colorChanged = false;

    public DPcatColors(JiggleProperties props) {
      newProps = props;
      rowColorChooserPanel = new CatalogRowColorChooserPanel();
      try {
        initGraphics();
      }
      catch(Exception e) {
        e.printStackTrace();
      }
    }

    private void initGraphics() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS)); 
        rowColorChooserPanel.initGraphics();
        this.add(rowColorChooserPanel);
    }

    public void setColorProperties() {
        if (rowColorChooserPanel != null) rowColorChooserPanel.setColorProperties();
    }

    class CatalogRowColorChooserPanel extends JPanel { 

        private Color colorBck    = CatalogTable.defaultColorBck;

        private Color colorLoc    = CatalogTable.defaultColorLoc;
        private Color colorReg    = CatalogTable.defaultColorReg;
        private Color colorTel    = CatalogTable.defaultColorTel;

        private Color colorEQ     = CatalogTable.defaultColorEQ;
        private Color colorQry    = CatalogTable.defaultColorQry;
        private Color colorTrg    = CatalogTable.defaultColorTrg;
        private Color colorSon    = CatalogTable.defaultColorSon;
        private Color colorNuke   = CatalogTable.defaultColorNuke;
        private Color colorLong   = CatalogTable.defaultColorLong;
        private Color colorExpl   = CatalogTable.defaultColorExpl;

        private Color colorJiggle = CatalogTable.defaultColorJiggle;
        private Color colorRT     = CatalogTable.defaultColorRT;
        private Color colorMung   = CatalogTable.defaultColorMung;
        private Color colorCUSP   = CatalogTable.defaultColorCUSP;
        private Color colorSEDAS  = CatalogTable.defaultColorSEDAS;
        private Color colorOther  = CatalogTable.defaultColorOther;
        private Color colorUnkSrc = CatalogTable.defaultColorUnk;
        private Color colorUnkTyp = CatalogTable.defaultColorUnk;

        private Color colorA = CatalogTable.defaultColorA;
        private Color colorH = CatalogTable.defaultColorH;
        private Color colorI = CatalogTable.defaultColorI;
        private Color colorF = CatalogTable.defaultColorF;
        private Color colorC = CatalogTable.defaultColorC;

        private ActiveList catalogButtonList = new ActiveList();
        private MyColorButton lastButton = null;

        private void initGraphics() {

            Box mainVbox = Box.createVerticalBox();
            mainVbox.setBorder(BorderFactory.createTitledBorder("Catalog Row Color Selection"));

            // Color by button box
            Box hbox = Box.createHorizontalBox();
            hbox.setAlignmentX(Component.CENTER_ALIGNMENT);
            ButtonGroup bg = new ButtonGroup();
            String str = newProps.getProperty("colorCatalogByType", "0");
            hbox.add(new JLabel("Color by "));
            ActionListener al3 = new CatalogColorCategoryAction();
            JRadioButton jrb = new JRadioButton("Event etype");
            jrb.setSelected(str.equals("1"));
            jrb.addActionListener(al3);
            bg.add(jrb);
            hbox.add(jrb);
            jrb = new JRadioButton("Origin gtype");
            jrb.setSelected(str.equals("4"));
            jrb.addActionListener(al3);
            bg.add(jrb);
            hbox.add(jrb);
            jrb = new JRadioButton("Processing state");
            jrb.setSelected(str.equals("2"));
            jrb.addActionListener(al3);
            bg.add(jrb);
            hbox.add(jrb);
            jrb = new JRadioButton("Subsource");
            jrb.setSelected(str.equals("3"));
            jrb.addActionListener(al3);
            bg.add(jrb);
            hbox.add(jrb);
            jrb = new JRadioButton("Uniform background");
            jrb.setSelected(str.equals("0"));
            jrb.addActionListener(al3);
            bg.add(jrb);
            hbox.add(jrb);
            mainVbox.add(hbox);

            setupColors(); // sets value of class instance field colors used below, like colorBck

            Box catColorHbox = Box.createHorizontalBox();

            Box vboxUS = Box.createVerticalBox();
            vboxUS.setAlignmentY(Component.TOP_ALIGNMENT);

            Box vboxU = Box.createVerticalBox();
            vboxU.setBorder(BorderFactory.createTitledBorder("Uniform Background"));
            makeColorPropertyComponent(vboxU, "color.catalog.uniform", colorBck);
            vboxUS.add(vboxU);

            catalogButtonList.clear();

            Box vboxS = Box.createVerticalBox();
            vboxS.setBorder(BorderFactory.createTitledBorder("Processing State"));
            addEventStateColorButtons(vboxS);
            vboxUS.add(vboxS);

            final Box vboxT = Box.createVerticalBox();
            vboxT.setAlignmentY(Component.TOP_ALIGNMENT);
            vboxT.setBorder(BorderFactory.createTitledBorder("Event EType"));
            JButton jb = new JButton("Add new etype");
            jb.setToolTipText("Press to add new event etype color");
            jb.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    addNewEventTypeButton(vboxT);
                }
            });
            vboxT.add(jb);
            addEventTypeColorButtons(vboxT);

            final Box vboxG = Box.createVerticalBox();
            vboxG.setAlignmentY(Component.TOP_ALIGNMENT);
            vboxG.setBorder(BorderFactory.createTitledBorder("Origin GType"));
            jb = new JButton("Add new gtype");
            jb.setToolTipText("Press to add new origin gtype color");
            jb.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    addNewOriginGTypeButton(vboxG);
                }
            });
            vboxG.add(jb);
            addOriginGTypeColorButtons(vboxG);

            final Box vboxSS = Box.createVerticalBox();
            vboxSS.setAlignmentY(Component.TOP_ALIGNMENT);
            vboxSS.setBorder(BorderFactory.createTitledBorder("Subsource"));
            jb = new JButton("Add new subsource");
            jb.setToolTipText("Press to add new event subsource color");
            jb.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    addNewEventSourceButton(vboxSS);
                }
            });
            vboxSS.add(jb);
            addEventSourceColorButtons(vboxSS);

            catColorHbox.add(vboxT);
            catColorHbox.add(vboxG);
            catColorHbox.add(vboxSS);
            catColorHbox.add(vboxUS);

            mainVbox.add(catColorHbox);
            this.add(mainVbox);
        }

        private class CatalogColorCategoryAction implements ActionListener {
            public void actionPerformed(ActionEvent e) {
                String cmd = e.getActionCommand();
                String value = "0";
                if (cmd.equals("Event etype")) {
                    value = "1";
                }
                else if (cmd.equals("Processing state")) {
                    value = "2";
                }
                else if (cmd.equals("Subsource")) {
                    value = "3";
                }
                else if (cmd.equals("Origin gtype")) {
                    value = "4";
                }
                colorChanged = true; // assume changed, though user may have just toggled back to original value
                newProps.setProperty("colorCatalogByType",value);
            }
        }

        protected void setupColors() {

            Color c = newProps.getColor("color.catalog.uniform");
            if (c != null) colorBck = c;
            else newProps.setProperty("color.catalog.uniform", colorBck);

            // origin gtypes
            c = newProps.getColor("color.catalog.gtype.local");
            if (c != null) colorLoc = c;
            else newProps.setProperty("color.catalog.gtype.local", colorLoc);

            c = newProps.getColor("color.catalog.gtype.regional");
            if (c != null) colorReg = c;
            else newProps.setProperty("color.catalog.gtype.regional", colorReg);

            c = newProps.getColor("color.catalog.gtype.teleseism");
            if (c != null) colorTel = c;
            else newProps.setProperty("color.catalog.gtype.teleseism", colorTel);

            // event etypes
            c = newProps.getColor("color.catalog.etype.earthquake");
            if (c != null) colorQry = c;
            else newProps.setProperty("color.catalog.etype.earthquake", colorEQ);

            c = newProps.getColor("color.catalog.etype.quarry");
            if (c != null) colorQry = c;
            else newProps.setProperty("color.catalog.etype.quarry", colorQry);

            c = newProps.getColor("color.catalog.etype.trigger");
            if (c != null) colorTrg = c;
            else newProps.setProperty("color.catalog.etype.trigger", colorTrg);

            c = newProps.getColor("color.catalog.etype.sonic");
            if (c != null) colorSon = c;
            else newProps.setProperty("color.catalog.etype.sonic", colorSon);

            c = newProps.getColor("color.catalog.etype.nuclear");
            if (c != null) colorNuke = c;
            else newProps.setProperty("color.catalog.etype.nuclear", colorNuke);

            c = newProps.getColor("color.catalog.etype.longperiod");
            if (c != null) colorLong = c;
            else newProps.setProperty("color.catalog.etype.longperiod", colorLong);

            c = newProps.getColor("color.catalog.etype.explosion");
            if (c != null) colorExpl = c;
            else newProps.setProperty("color.catalog.etype.explosion", colorExpl);

            c = newProps.getColor("color.catalog.etype.unknown");
            if (c != null) colorUnkTyp = c;
            else newProps.setProperty("color.catalog.etype.unknown", colorUnkTyp);

            // processing state types
            c = newProps.getColor("color.catalog.state.A");
            if (c != null) colorA = c;
            else newProps.setProperty("color.catalog.state.A", colorA);

            c = newProps.getColor("color.catalog.state.H");
            if (c != null) colorH = c;
            else newProps.setProperty("color.catalog.state.H", colorH);

            c = newProps.getColor("color.catalog.state.I");
            if (c != null) colorI = c;
            else newProps.setProperty("color.catalog.state.I", colorI);

            c = newProps.getColor("color.catalog.state.F");
            if (c != null) colorF = c;
            else newProps.setProperty("color.catalog.state.F", colorF);

            c = newProps.getColor("color.catalog.state.C");
            if (c != null) colorC = c;
            else newProps.setProperty("color.catalog.state.C", colorC);

            // subsource types
            c = newProps.getColor("color.catalog.subsrc.RT");
            if (c != null) colorRT = c;
            else newProps.setProperty("color.catalog.subsrc.RT", colorRT);

            c = newProps.getColor("color.catalog.subsrc.Jiggle");
            if (c != null) colorJiggle = c;
            else newProps.setProperty("color.catalog.subsrc.Jiggle", colorJiggle);

            c = newProps.getColor("color.catalog.subsrc.mung");
            if (c != null) colorMung = c;
            else newProps.setProperty("color.catalog.subsrc.mung", colorMung);

            c = newProps.getColor("color.catalog.subsrc.CUSP");
            if (c != null) colorCUSP = c;
            else newProps.setProperty("color.catalog.subsrc.CUSP", colorCUSP);

            c = newProps.getColor("color.catalog.subsrc.sedas");
            if (c != null) colorSEDAS = c;
            else newProps.setProperty("color.catalog.subsrc.sedas", colorSEDAS);

            c = newProps.getColor("color.catalog.subsrc.other");
            if (c != null) colorOther = c;
            else newProps.setProperty("color.catalog.subsrc.other", colorOther);

            c = newProps.getColor("color.catalog.subsrc.unknown");
            if (c != null) colorUnkSrc = c;
            else newProps.setProperty("color.catalog.subsrc.unknown", colorUnkSrc);

        }

        protected void setColorProperties() {
            JButton jb = null;
            for (int idx = 0; idx < catalogButtonList.size(); idx++) {
               jb = (JButton) catalogButtonList.get(idx);
               newProps.setProperty(jb.getActionCommand(), jb.getBackground());
            }
        }

        protected void setDefaultColorProperties() {

            newProps.setProperty("color.catalog.uniform", colorBck);

            newProps.setProperty("color.catalog.gtype.local", colorLoc);
            newProps.setProperty("color.catalog.gtype.regional", colorReg);
            newProps.setProperty("color.catalog.gtype.teleseism", colorTel);

            newProps.setProperty("color.catalog.etype.earthquake", colorEQ);
            newProps.setProperty("color.catalog.etype.quarry", colorQry);
            newProps.setProperty("color.catalog.etype.trigger", colorTrg);
            newProps.setProperty("color.catalog.etype.sonic", colorSon);
            newProps.setProperty("color.catalog.etype.nuclear", colorNuke);
            newProps.setProperty("color.catalog.etype.longperiod", colorLong);
            newProps.setProperty("color.catalog.etype.explosion", colorExpl);
            newProps.setProperty("color.catalog.etype.unknown", colorUnkTyp);

            newProps.setProperty("color.catalog.state.A", colorA);
            newProps.setProperty("color.catalog.state.H", colorH);
            newProps.setProperty("color.catalog.state.I", colorI);
            newProps.setProperty("color.catalog.state.F", colorF);
            newProps.setProperty("color.catalog.state.C", colorC);

            newProps.setProperty("color.catalog.subsrc.RT", colorRT);
            newProps.setProperty("color.catalog.subsrc.Jiggle", colorJiggle);
            newProps.setProperty("color.catalog.subsrc.mung", colorMung);
            newProps.setProperty("color.catalog.subsrc.CUSP", colorCUSP);
            newProps.setProperty("color.catalog.subsrc.sedas", colorSEDAS);
            newProps.setProperty("color.catalog.subsrc.other", colorOther);
            newProps.setProperty("color.catalog.subsrc.unknown", colorUnkSrc);
        }

        private class MyColorButton extends JButton {
            public boolean equals(Object obj) {
                if (obj instanceof MyColorButton) {
                   return getActionCommand().equals(((MyColorButton)obj).getActionCommand());
                }
                return false;
            }
        }

        private void addNewEventTypeButton(JComponent comp) {
            String input =
                JOptionPane.showInputDialog(getTopLevelAncestor(),
                    "Enter new event etype",
                    "New Event Type Color",
                    JOptionPane.PLAIN_MESSAGE);

            if (input != null && input.length() > 1) { // processing state are single letters, so skip
                String label = "color.catalog.etype."+ input;
                MyColorButton jb = null;
                boolean inList = false;
                for (int idx = 0; idx < catalogButtonList.size(); idx++) {
                  jb = (MyColorButton) catalogButtonList.get(idx);
                  if (jb.getActionCommand().equals(label)) {
                      inList = true;
                      break;
                  }
                }

                if (inList) jb.doClick();
                else {
                    makeColorPropertyComponent(comp, label, Color.white);
                    comp.revalidate();
                    if (lastButton != null) lastButton.doClick();
                }
            }
        }

        private void addNewOriginGTypeButton(JComponent comp) {
            String input =
                JOptionPane.showInputDialog(getTopLevelAncestor(),
                    "Enter new origin gtype",
                    "New Origin Gtype Color",
                    JOptionPane.PLAIN_MESSAGE);

            if (input != null && input.length() > 1) { // processing state are single letters, so skip
                String label = "color.catalog.gtype."+ input;
                MyColorButton jb = null;
                boolean inList = false;
                for (int idx = 0; idx < catalogButtonList.size(); idx++) {
                  jb = (MyColorButton) catalogButtonList.get(idx);
                  if (jb.getActionCommand().equals(label)) {
                      inList = true;
                      break;
                  }
                }

                if (inList) jb.doClick();
                else {
                    makeColorPropertyComponent(comp, label, Color.white);
                    comp.revalidate();
                    if (lastButton != null) lastButton.doClick();
                }
            }
        }

        private void addNewEventSourceButton(JComponent comp) {
            String input =
                JOptionPane.showInputDialog(getTopLevelAncestor(),
                    "Enter new event subsource",
                    "New Event Subsource Color",
                    JOptionPane.PLAIN_MESSAGE);

            if (input != null && input.length() > 1) { // processing state are single letters, so skip
                String label = "color.catalog.subsrc."+ input;
                MyColorButton jb = null;
                boolean inList = false;
                for (int idx = 0; idx < catalogButtonList.size(); idx++) {
                  jb = (MyColorButton) catalogButtonList.get(idx);
                  if (jb.getActionCommand().equals(label)) {
                      inList = true;
                      break;
                  }
                }

                if (inList) jb.doClick();
                else {
                    makeColorPropertyComponent(comp, label, Color.white);
                    comp.revalidate();
                    if (lastButton != null) lastButton.doClick();
                }
            }
        }
        
        private JComponent makeColorPropertyComponent(JComponent comp, final String label, final Color color) {
            Box hbox = Box.createHorizontalBox();
            hbox.add(makeColorButtonLabel(label));
            hbox.add(makeColorButton(label, color));
            hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
            comp.add(hbox);
            return comp;
        }

        private JLabel makeColorButtonLabel(String label) {
            int idx = label.lastIndexOf('.');
            JLabel jlbl = new JLabel(label.substring(idx+1));
            jlbl.setPreferredSize(new Dimension(100,16));
            jlbl.setMaximumSize(new Dimension(100,16));
            jlbl.setToolTipText("Row type, press color to change its color");
            return jlbl;
        }
            
        private MyColorButton makeColorButton(final String label, final Color color) {
            final MyColorButton jb = new MyColorButton();
            jb.setActionCommand(label);
            jb.setBackground(color);
            jb.setToolTipText(Integer.toHexString(color.getRGB()));

            jb.addActionListener( new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    try {
                      final JColorChooser jcc = new JColorChooser(color);
                      final class MyJLabel extends JLabel implements ChangeListener {

                          public MyJLabel() { super(); }
                          public MyJLabel(String str) { super(str); }

                          public void stateChanged(ChangeEvent evt) {
                              MyJLabel.this.setBackground(jcc.getSelectionModel().getSelectedColor());
                              //MyJLabel.this.setForeground(jcc.getSelectionModel().getSelectedColor());
                              MyJLabel.this.repaint();
                          }
                      };
                      final MyJLabel jlbl2 = new MyJLabel("1234567 2008-08-01:23:45:56 local 3.75 l Jiggle");
                      JPanel jp = new JPanel();
                      jp.setBorder(BorderFactory.createTitledBorder(jb.getActionCommand()));
                      jlbl2.setForeground(Color.black);
                      jlbl2.setBackground(color);
                      jlbl2.setOpaque(true);
                      jlbl2.setPreferredSize(new Dimension(260,20));
                      jp.add(jlbl2);
                      jcc.getSelectionModel().addChangeListener(jlbl2);
                      jcc.setPreviewPanel(jp);
                      JDialog jd = jcc.createDialog(DPcatColors.this.getParent(), "Select row color", true, jcc,
                              new ActionListener () {
                                  public void actionPerformed(ActionEvent evt) {
                                      Color selectedColor = jcc.getColor();
                                      if (! selectedColor.equals(color)) {
                                          colorChanged = true;
                                          newProps.setProperty(label, selectedColor);
                                          jb.setBackground(selectedColor);
                                          jb.setToolTipText(Integer.toHexString(selectedColor.getRGB()));
                                      }
                                  }
                              },
                              new ActionListener () {
                                  public void actionPerformed(ActionEvent evt) { }
                              }
                      );
                      jd.pack();
                      jd.setSize(500,600);
                      jd.setVisible(true);
                    }
                    catch(HeadlessException ex) {}
                }
            });

            lastButton = jb;
            catalogButtonList.addOrReplace(jb);

            return jb;
        }

        private void addEventStateColorButtons(JComponent comp) {
            String key = "color.catalog.state.A";
            makeColorPropertyComponent(comp, key, newProps.getColor(key));
            key = "color.catalog.state.H";
            makeColorPropertyComponent(comp, key, newProps.getColor(key));
            key = "color.catalog.state.I";
            makeColorPropertyComponent(comp, key, newProps.getColor(key));
            key = "color.catalog.state.F";
            makeColorPropertyComponent(comp, key, newProps.getColor(key));
            key = "color.catalog.state.C";
            makeColorPropertyComponent(comp, key, newProps.getColor(key));
        }

        private void addEventTypeColorButtons(JComponent comp) {
            int idx = -1;
            String key = null;
            for (Enumeration e = newProps.keys(); e.hasMoreElements();)  { // alpha sorted order key
                key = (String) e.nextElement();
                idx = key.indexOf("color.catalog.etype");
                if ( idx >= 0 ) {
                   makeColorPropertyComponent(comp, key, newProps.getColor(key)); // getColor() returns null if unparseable
                }
            }
        }

        private void addOriginGTypeColorButtons(JComponent comp) {
            int idx = -1;
            String key = null;
            for (Enumeration e = newProps.keys(); e.hasMoreElements();)  { // alpha sorted order key
                key = (String) e.nextElement();
                idx = key.indexOf("color.catalog.gtype");
                if ( idx >= 0 ) {
                   makeColorPropertyComponent(comp, key, newProps.getColor(key)); // getColor() returns null if unparseable
                }
            }
        }

        private void addEventSourceColorButtons(JComponent comp) {
            int idx = -1;
            String key = null;
            for (Enumeration e = newProps.keys(); e.hasMoreElements();)  { // alpha sorted order key
                key = (String) e.nextElement();
                idx = key.indexOf("color.catalog.subsrc");
                if ( idx >= 0) {
                   makeColorPropertyComponent(comp, key, newProps.getColor(key)); // getColor() returns null if unparseable
                }
            }
        }

    } // CatalogRowColorChooserPanel

} // DPcatColors
