package org.trinet.jiggle;
import java.awt.*;
import java.lang.reflect.InvocationTargetException;
import javax.swing.*;
import org.trinet.util.graphics.*;

/**
 * A popup frame used to show the status of an operation. Doesn't really work
 * well for showing dynamic progress because the threading isn't handled right.
 */
public class WorkerStatusDialog extends StatusDialog {

     public WorkerStatusDialog() {
         super();
     }
     public WorkerStatusDialog(Frame owner, boolean modal) {
         super(owner, modal);
     }
     public WorkerStatusDialog(Frame owner, boolean modal, String title, String text) {
         super(owner, modal, title, text);
     }
     public WorkerStatusDialog(Frame owner, boolean modal, String title, String text, Icon icon) {
         super(owner, modal, title, text, icon);
     }

/** Puts the frame creation job on the GUI event queue and returns. */
    public void pop(String title, String message, boolean indeterminate) {
        final String s1 = title;
        final String s2 = message;
        final boolean tf = indeterminate;

        SwingUtilities.invokeLater(
            new Runnable() {
              public void run() {
                setProgressIndeterminate(tf);
                setProgressStringPainted(! tf);
                set(s1, s2);
                show();
                toFront();
                if (beep == BEEP_ON || beep == BEEP_ON_OPEN) getToolkit().beep(); // make noise
              }
            }
        );
    }
    // Note: since delayed in queue, don't invoke before dispose or close, may cause exception
    public void unpop() {
          SwingUtilities.invokeLater(
            new Runnable() {
              public void run() {
                setProgressIndeterminate(false);
                setProgressStringPainted(true);
                hide();
                toBack();
                if (beep == BEEP_ON || beep == BEEP_ON_CLOSE) getToolkit().beep(); // make noise
              }
            }
          );
    }

/** Set the text in the text panel. */
    public void setText(final String text) {
        if (SwingUtilities.isEventDispatchThread()) super.setText(text);
        else {
          SwingUtilities.invokeLater(
            new Runnable() {
              public void run() {
                textLabel.setText(text);
                pack();
              }
            }
          );
        }
    }
/*
public static class Tester {
     public static void main(String[] args) {
        ImageIcon defIcon = new ImageIcon("images/gearsAnim.gif");
        WorkerStatusDialog statusFrame = new WorkerStatusDialog();
        statusFrame.pop("Test frame", "Something's happening", true);
     }
  } // end of Tester
*/
}
