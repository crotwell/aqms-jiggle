package org.trinet.jiggle;

import org.trinet.jasi.Amplitude;

import java.awt.*;
import org.trinet.util.graphics.ColorList;

/**
* A graphical marker to show where a phase energy is expected. Its just a
* colored rectangle stripe centered around the predicted arrival time. 
*/

public class PhaseCue implements MarkerIF {

    // Default size of symbol in pixels
    public final static int minWidth = 8;
    public final static int minHeight= 8;

    private int width  = minWidth;
    private int height = minHeight;
    private double widthSecs = 0.5;

    /** Position of the marker in pixel space. Centered on the calculated time and
    spanning the Y-dimension of the WFPanel or viewport. */
    private Point pos = new Point();

    /** Center time represented by this flag. */
    private double dtime;

    /** WFPanel where flag will be drawn. */
    private WFPanel wfp;

    /** Current color of the flag. */
    private static Color color = ColorList.mint;   // set default;

    /**
     * Create an amplitude flag representing this Amplitude
     * to be displayed in this WFPanel.
     */
     public PhaseCue() { }

    /**
     * Create an amplitude flag representing this Amplitude
     * to be displayed in this WFPanel.
     */
     public PhaseCue(WFPanel wfp, double epochTime) {
         set(wfp, epochTime);
     }

     public void set(WFPanel wfp, double epochTime) {
         this.wfp = wfp;
         this.dtime = epochTime;
     }

     /** Set the marker color. */
     public static void setDefaultColor(Color aColor) {
         color = aColor;
     }

     /** Set the marker color. */
     public void setColor(Color aColor) {
         color = aColor;
     }

     /** Return the marker color. */
     public Color getColor() {
         return color;
     }

       /** Set the marker size. */
     public void setSize(Dimension dim) {
         width  = (int) dim.getWidth();
         height = (int) dim.getHeight();
     }
       /** Return the marker size. */
     public Dimension getSize() {
         return new Dimension (width, height);
     }

     public void setWidthSecs(double seconds) {
         widthSecs = seconds;
     }
     public double getWidthSecs() {
         return widthSecs;
     }

    /*
     * Draw the PhaseCue in the Graphics window.
    */
    public void draw (Graphics g) {

        if (wfp == null) return;
     
        // MUST RECALC. THIS EVERY TIME BECAUSE FRAME MIGHT HAVE RESCALED!
        int x = wfp.pixelOfTime(dtime) ;

        // lookup origin's color code
        //color = wfp.wfv.mv.solList.getColorOf(amp.getAssociatedSolution());

        Color oldColor = g.getColor();
        g.setColor(color);

        // this makes a filled rectangle centered on the given time. Its width is
        // 'width' and its height is the whole height of the WFPanel
        width = (int) Math.max(minWidth, widthSecs*wfp.getPixelsPerSecond());
        int halfWidth = width/2;

        height = (int) Math.max(minHeight, wfp.getHeight());

        g.fillRect(x-halfWidth, 0, width, wfp.getHeight());
        g.setColor(oldColor);

    }

}

