package org.trinet.jiggle;

import javax.swing.event.*;
import org.trinet.jasi.*;
import org.trinet.jasi.coda.*;
import org.trinet.util.*;

/** When an instance of this class is added as a listener to a JasiReadingList
* it keeps local JasiReadingLists for each WFView in the WFViewList in synch with
* the main list.
* There are three types of JasiReadingLists: PhaseList, AmpList, and CodaList.
* This is done with the WFViewList rather than making each WFView a listener for
* efficiency. We don't want to call 1,000 listeners when one reading changes. */
public class JasiReadingListModel implements ChangeListener, ListDataStateListener {

     //private JasiReadingList readingList;
     private WFViewList wfvList;

     public JasiReadingListModel(WFViewList wfvList) {
       setWFViewList(wfvList);
     }

     /** Sets the WFViewList that will be updated in response to a JasiReading
     * change event. */
     public void setWFViewList(WFViewList wfvList) {
        this.wfvList = wfvList;
     }

     public void clearWFViewList() {
        if (wfvList != null) wfvList.clear();
     }

    /** Respond to a list change. Update the appropriate list or lists. Tries to
    * minimize the work done. */
    public void stateChanged(ChangeEvent changeEvent) {
        updateWFViewList(changeEvent.getSource());
    }

    private void updateWFViewList(Object src) {
         // source is a single reading
        if (src instanceof JasiReading) {
            updateOneWFView((JasiReading)src);
        }
        // source is a list
        else if (src instanceof JasiReadingList)  {
            updateAllWFViews((JasiReadingList) src);
        }
    }
    private void updateOneWFView(JasiReadingIF jr) {
        if (jr == null) return;
        WFView wfv = wfvList.get(jr.getChannelObj());
        if (wfv == null) return;

        if (jr instanceof Phase) {
            wfv.updatePhaseList();
        } else if (jr instanceof Amplitude) {
            wfv.updateAmpList();
        } else if (jr instanceof Coda) {
            wfv.updateCodaList();
        }
     }
    private void updateAllWFViews(JasiReadingList jrl) {
        WFView wfv[] = wfvList.getArray();
        if (wfv == null) return;

        if (jrl instanceof PhaseList) {
            for (int i = 0; i<wfv.length; i++)  {
                wfv[i].updatePhaseList();
            }
        } else if (jrl instanceof AmpList) {
            for (int i = 0; i<wfv.length; i++)  {
                wfv[i].updateAmpList();
            }
        } else if (jrl instanceof CodaList) {
            for (int i = 0; i<wfv.length; i++)  {
                wfv[i].updateCodaList();
            }
        }
     }

// IF ListDataStateListener
//TODO: refine actions based upon the StateName of and objects type being updated
//Possible return of the listDataStateEvent.getStateChange().getValue() is:
//  null, all the elements of the list = event.getSource() are effected
//  a single effected list element
//  an array of effected list elements
  // For the time being don't discriminate, do all types the same way:
  public void intervalAdded(ListDataStateEvent e) {
     processListDataStateEvent(e);
  }
  public void intervalRemoved(ListDataStateEvent e) {
     processListDataStateEvent(e);
  }
  public void contentsChanged(ListDataStateEvent e) {
     processListDataStateEvent(e);
  }
  public void stateChanged(ListDataStateEvent e) {
     processListDataStateEvent(e);
  }
  public void orderChanged(ListDataStateEvent e) {
     // no-op processListDataStateEvent(e); // 2005/08/09 aww order not relevent
  }
  private void processListDataStateEvent(ListDataStateEvent e) {
      // Lazy approach, do all event the same way, until some
      // other methods are implemented in class to discriminate types
      JasiReadingList src = (JasiReadingList) e.getSource();
      int first  = e.getIndex0();
      int last   = e.getIndex1();
      StateChange sc = e.getStateChange();
      Object data = sc.getValue();
      /*
      System.out.println(
        "DEBUG JasiReadingListModel stateChange in: " +src.getClass().getName() +
        " type: " + sc.getStateName() + " from " + first + " to " + last
        + " value is : " + ((data == null) ? "null" : data.getClass().getName())
      );
      */
      if (data == null) {
        updateAllWFViews((JasiReadingList)src); // punt do them all
      }
      else if (first > -1 && last == first) {
        //For "stateChanged" event case, the StateChange data is new value of state,
       // not src list element(s)changed, must use index to lookup element.
        JasiReadingIF jr = null;
        if (data instanceof JasiReadingIF) {
          jr = (JasiReadingIF) data;
        } else {
          jr = src.getJasiReading(first);
        }
        if (jr == null) updateAllWFViews((JasiReadingList)src); // punt do them all
        else updateOneWFView(jr);  // do single element
      }
      else { // assume data is an array of list elements
        Object [] jcArray = (Object []) data;
        for (int idx=0; idx < jcArray.length; idx++) {
          updateOneWFView((JasiReading) jcArray[idx]);
        }
      }
  }
}
