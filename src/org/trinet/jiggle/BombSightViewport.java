package org.trinet.jiggle;

import java.awt.*;
import javax.swing.*;

/**
 * Overrides JViewport's paint() method to draw a phase picking "bomb sight" in  the center of the Viewport.
 */
public class BombSightViewport extends JViewport {

    protected Color bombSightColor = Color.green;

    boolean fireEvents = true;

    public BombSightViewport() {
        // this means: just repaint area newly scrolled into view
        // should improve performance
      setScrollMode(JViewport.BLIT_SCROLL_MODE);
      setOpaque(true);  // aww, if false then scroller repaint not as efficient
    
      //System.out.println("Creating BombSightViewport...");
    }
    public BombSightViewport(Color clr) {
        this();
        setBombSightColor(clr);
    }
    
    public Color getBackground() {
        return (getView() == null) ?
            super.getBackground() : getView().getBackground();
    }
    public void paintChildren(Graphics g) {
        super.paintChildren(g);
        makeBombSight(g);
    }
    private void makeBombSight(Graphics g) {
        // add the vertical "bombsite"
        int center = getWidth()/2 ;
        Color oldColor = g.getColor(); // save original context
        g.setColor (getBombSightColor());
        g.drawLine (center, 0 , center, getHeight()-1); // outer bnd = h-1 
        g.setColor(oldColor); // restore original
    }
    
    /** Set the color of the vertical line in the bombsite. */
    public void setBombSightColor(Color clr) {
      bombSightColor = clr;
    }
    /** Return the color of the vertical line in the bombsite. */
    public Color getBombSightColor() {
      return bombSightColor;
    }
    /** Enable/disable firing of change events. */
    public void enableChangeEvents(boolean tf) {
      fireEvents = tf;
    }
    /** Return true if firing of change events is enabled. */
    public boolean changeEventsEnabled() {
      return fireEvents;
    }

    /**
    * Overrides JViewport.fireStateChanged() to support disabling of
    * events via enableChangeEvents().
    * Notifies all <code>ChangeListeners</code> when the view's
    * size, position, or the viewport's extent size has changed if
    * enableChangeEvents() == true.
    *
    * @see JViewport.addChangeListener
    * @see JViewport.removeChangeListener
    * @see EventListenerList
    */
    protected void fireStateChanged(){
        if (changeEventsEnabled()) super.fireStateChanged();
    }
}
