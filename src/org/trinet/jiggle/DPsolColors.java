package org.trinet.jiggle;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.trinet.jasi.ActiveList;
import org.trinet.util.graphics.ColorList;

/**
 * Panel for insertion into dialog box to edit solution list clone colors.
 */
public class DPsolColors extends JPanel {

    private JiggleProperties newProps = null;

    private ActiveList jbList = new ActiveList();

    private Box vbox = null;

    private MyColorButton lastButton = null;

    protected boolean colorChanged = false;

    public DPsolColors(JiggleProperties props) {

      newProps = props;
      ColorList.fromProperties(newProps); // get user defs
      ColorList.toProperties(newProps); // set user defs

      try {
        initGraphics();
      }
      catch(Exception e) {
        e.printStackTrace();
      }
    }

    private void initGraphics() {

        Box mainBox = Box.createVerticalBox();
        mainBox.setBorder(BorderFactory.createTitledBorder("Pick Flag Colors"));

        Box hbox = Box.createHorizontalBox();
        hbox.setAlignmentX(Component.CENTER_ALIGNMENT);
        ButtonGroup bg = new ButtonGroup();
        boolean tf = newProps.getBoolean("pickFlag.colorByPhase", false);
        String tooltip = "Use event or P/S phase colors for pick flags";
        JLabel jlbl = new JLabel("Color by ");
        jlbl.setToolTipText(tooltip);
        hbox.add(jlbl);
        ActionListener al = new PickFlagColorByAction();
        JRadioButton jrb = new JRadioButton("event");
        tooltip = "Pick flags are event color";
        jrb.setToolTipText(tooltip);
        jrb.setSelected(! tf);
        jrb.addActionListener(al);
        bg.add(jrb);
        hbox.add(jrb);
        jrb = new JRadioButton("phase");
        tooltip = "Pick flags are P or S phase color";
        jrb.setToolTipText(tooltip);
        jrb.setSelected(tf);
        jrb.addActionListener(al);
        bg.add(jrb);
        hbox.add(jrb);
        mainBox.add(hbox);
        mainBox.add(Box.createVerticalStrut(5));

        vbox = Box.createVerticalBox();
        vbox.setBorder(BorderFactory.createTitledBorder("Event # color"));
        JButton jb = new JButton("Add Next...");
        jb.setToolTipText("Press to add the next cloned event# pick flag's color");
        jb.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                addNewSolutionButton();
            }
        });

        jb.setPreferredSize(new Dimension(100,16));
        jb.setMaximumSize(new Dimension(100,16));
        jb.setAlignmentX(Component.CENTER_ALIGNMENT);
        jb.setAlignmentY(Component.TOP_ALIGNMENT);

        vbox.add(jb);
        vbox.add(Box.createVerticalStrut(2));
        addEventColorButtons(vbox);

        Box colorBox = Box.createHorizontalBox();
        colorBox.add(vbox);

        vbox = Box.createVerticalBox();
        vbox.setBorder(BorderFactory.createTitledBorder("Phase color"));
        colorBox.add(Box.createHorizontalStrut(5));

        // pick flag coloring type button box
        String propStr ="pickFlag.colorP";
        Color flagColor = newProps.getColor(propStr);
        if (flagColor == null) flagColor = PickFlag.colorP;
        vbox.add(makePickFlagColorPropertyComponent(propStr, flagColor));

        propStr ="pickFlag.colorP.text";
        flagColor = newProps.getColor(propStr);
        if (flagColor == null) flagColor = PickFlag.colorPtext;
        vbox.add(makePickFlagColorPropertyComponent(propStr, flagColor));

        propStr ="pickFlag.colorS";
        flagColor = newProps.getColor(propStr);
        if (flagColor == null) flagColor = PickFlag.colorS;
        vbox.add(makePickFlagColorPropertyComponent(propStr, flagColor));

        propStr ="pickFlag.colorS.text";
        flagColor = newProps.getColor(propStr);
        if (flagColor == null) flagColor = PickFlag.colorStext;
        vbox.add(makePickFlagColorPropertyComponent(propStr, flagColor));
        vbox.add(Box.createVerticalGlue());
        colorBox.add(vbox);

        mainBox.add(colorBox);

        this.add(mainBox);

    }

    private class MyColorButton extends JButton {
        public boolean equals(Object obj) {
            if (obj instanceof MyColorButton) {
               return getActionCommand().equals(((MyColorButton)obj).getActionCommand());
            }
            return false;
        }
    }

    private void addNewSolutionButton() {
        String label = "color.solution."+ String.valueOf(jbList.size()); 
        vbox.add(makeEventColorPropertyComponent(label, Color.white));
        vbox.revalidate();
        if (lastButton != null) lastButton.doClick();
    }
    
    private JComponent makeEventColorPropertyComponent(final String label, final Color color) {
        Box comp = Box.createHorizontalBox(); 
        comp.add(makeEventColorButtonLabel(label));
        comp.add(makeColorButton(label, color));
        return comp;
    }

    private JLabel makeEventColorButtonLabel(String label) {
        //int idx = label.lastIndexOf('.');
        int idx = 15; // "color.solution."
        JLabel jlbl = new JLabel("Event # " + label.substring(idx));
        jlbl.setPreferredSize(new Dimension(64,16));
        jlbl.setMaximumSize(new Dimension(64,16));
        jlbl.setAlignmentX(Component.LEFT_ALIGNMENT);
        jlbl.setToolTipText("Press color to change the event#'s pick flag color");
        return jlbl;
    }

    private MyColorButton makeColorButton(final String label, final Color color) {
        final MyColorButton jb = new MyColorButton();
        jb.setActionCommand(label);
        jb.setBackground(color);
        jb.setPreferredSize(new Dimension(32,16));
        jb.setMaximumSize(new Dimension(32,16));
        jb.setAlignmentX(Component.LEFT_ALIGNMENT);
        jb.setToolTipText(Integer.toHexString(color.getRGB()));
        jb.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                try {
                  final JColorChooser jccFG = new JColorChooser(color);
                  final JColorChooser jccBG = new JColorChooser(new Color((int)Long.parseLong("EEEEEE",16)));
                  final class MyJLabel extends JLabel implements ChangeListener {

                      public MyJLabel() { super(); }
                      public MyJLabel(String str) { super(str); }

                      public void stateChanged(ChangeEvent evt) {
                          MyJLabel.this.setForeground(jccFG.getSelectionModel().getSelectedColor());
                          MyJLabel.this.setBackground(jccBG.getSelectionModel().getSelectedColor());
                          MyJLabel.this.repaint();
                      }
                  };
                  final MyJLabel jlbl2 = new MyJLabel("+-*=<>+-*=<>+-*=<>+-*=<>+-*=<>+-*=<>+-*=<>+-*=<>+-*=<>");
                  JPanel jp = new JPanel();
                  jp.setBorder( BorderFactory.createTitledBorder(jb.getActionCommand()) );
                  jlbl2.setForeground(jccFG.getColor());
                  jlbl2.setBackground(jccBG.getColor());
                  jlbl2.setOpaque(true);
                  jlbl2.setPreferredSize(new Dimension(275,20));
                  jp.add(jlbl2);

                  JButton jbPreviewBG = new JButton("BG");
                  jbPreviewBG.setToolTipText("Press to change this preview's background color"); 
                  jccBG.getSelectionModel().addChangeListener(jlbl2);
                  jccBG.setPreviewPanel(new JPanel());
                  final JDialog dialogBG = jccBG.createDialog(DPsolColors.this.getParent(), "Preview background for " + label, true, jccBG, null, null);
                  jbPreviewBG.addActionListener(new ActionListener() {
                      public void actionPerformed(ActionEvent evt) {
                          dialogBG.setVisible(true);
                      }
                  });
                  jp.add(jbPreviewBG);

                  jccFG.getSelectionModel().addChangeListener(jlbl2);
                  jccFG.setPreviewPanel(jp);
                  JDialog jd = jccFG.createDialog(DPsolColors.this.getParent(), "Select color for "+label, true, jccFG,
                          new ActionListener () {
                              public void actionPerformed(ActionEvent evt) {
                                  Color selectedColor = jccFG.getColor();
                                  if (! selectedColor.equals(color)) {
                                      colorChanged = true;
                                      newProps.setProperty(label, selectedColor);
                                      jb.setBackground(selectedColor);
                                      jb.setToolTipText(Integer.toHexString(selectedColor.getRGB()));
                                  }
                              }
                          },
                          new ActionListener () {
                              public void actionPerformed(ActionEvent evt) { }
                          }
                  );
                  jd.pack();
                  jd.setSize(500,600);
                  jd.setVisible(true);
                }
                catch(HeadlessException ex) {}
            }
        });

        lastButton = jb;
        jbList.addOrReplace(jb);

        return jb;
    }

    private void addEventColorButtons(Box box) {
        int idx = -1;
        String key = null;
        jbList.clear();

        for (Enumeration e = newProps.keys(); e.hasMoreElements();)  { // alpha sorted order key
            key = (String) e.nextElement();
            if (key.length() < 16) continue; // skip old style entry
            idx = key.indexOf("color.solution");
            if (idx >= 0) {
                // getColor() returns null if unparseable
                box.add(makeEventColorPropertyComponent(key, newProps.getColor(key)));
            }
        }
    }

    protected void setColorProperties() {
        JButton jb = null;
        for (int idx=0; idx < jbList.size(); idx++) {
            jb = (JButton) jbList.get(idx);
            newProps.setProperty(jb.getActionCommand(), jb.getBackground());
        }
    }

    protected void resetColorList() {
        Color [] cArray = new Color [jbList.size()];
        JButton jb = null;
        for (int idx=0; idx < jbList.size(); idx++) {
            jb = (JButton) jbList.get(idx);
            cArray[idx] = jb.getBackground();
        }
        ColorList.colorArray = cArray;
    }

    /*
    protected void resetColorList() {
        newProps.loadColorList();
        //System.out.println(ColorList.toPropertyString());
    }
    */

    //## color by phase not event
    //#pickFlag.colorByPhase=true
    //## red
    //pickFlag.colorP = ffff0000
    //## fuschia
    //#pickFlag.colorS = ffff00ff
    private JComponent makePickFlagColorPropertyComponent(String label, Color inColor) {
        Box comp = Box.createHorizontalBox(); 
        comp.add(makePickFlagColorButtonLabel(label));
        comp.add(makeColorButton(label, inColor));
        comp.setAlignmentX(Component.LEFT_ALIGNMENT);
        return comp;
    }
    private JLabel makePickFlagColorButtonLabel(String label) {
        int idx = 14; // pickFlag.color.
        JLabel jlbl = new JLabel(label.substring(idx));
        jlbl.setPreferredSize(new Dimension(100,16));
        jlbl.setMaximumSize(new Dimension(100,16));
        jlbl.setToolTipText("Press color to change flag color");
        return jlbl;
    }
        
    private class PickFlagColorByAction implements ActionListener {
            public void actionPerformed(ActionEvent e) {
                String cmd = e.getActionCommand();
                boolean tf = false;
                if (cmd.equals("event")) {
                    tf = false;
                }
                else if (cmd.equals("phase")) {
                    tf = true;
                }
                colorChanged = true; // assume changed, though user may have just toggled back to original value
                newProps.setProperty("pickFlag.colorByPhase",tf);
            }
    }

}
