package org.trinet.jiggle.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.trinet.jasi.DataSource;
import org.trinet.jdbc.datasources.DbaseConnectionDescription;
import org.trinet.jiggle.common.JiggleExceptionHandler;
import org.trinet.jiggle.common.JiggleSingleton;
import org.trinet.jiggle.common.LogUtil;

public class JiggleDb {
    private Connection conn; 
    protected PreparedStatement ps = null;
    protected ResultSet rs = null;
    
    public JiggleDb(Connection conn) {
        this.conn = conn; // use getConnection() when executing query
    }
    
    /**
     * Get database connection. Try reconnect if null.
     * @return 
     */
    public Connection getConnection() {
        try {
            if (this.conn == null || this.conn.isClosed()) {
                // Simplified db reconnect based on Jiggle.makeDataSourceConnection
                // Allows user to continue when the DB connection is restored instead of restarting Jiggle
                DbaseConnectionDescription dbDesc = JiggleSingleton.getInstance().getJiggleProp().getDbaseDescription();
                new DataSource().set(dbDesc);
                this.conn = DataSource.getConnection();
            }
        } catch (SQLException ex) {
            JiggleExceptionHandler.handleDbException(conn, ex, "Cannot get database connection in JiggleDb.getConnection. ", "Database Connection");
        }

        return this.conn;
    }
    
    public void dbCleanUp() {
        try {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        } catch (SQLException ex2) {
            JiggleExceptionHandler.handleDbException(this.conn, ex2);
        }
    }
    
    public void commit() {
        if (this.conn != null) {
            try {
                this.conn.commit();
            } catch (SQLException ex) {
                JiggleExceptionHandler.handleDbException(this.conn, ex, "JiggleDb commit error.", "JiggleDb commit error.: ");
            }
        } else {
            LogUtil.warning("Database JiggleDb error. Cannot commit with null database connection");
        }
    } 
    
    public void rollback() {
        if (this.conn!=null) {
            try {
                this.conn.rollback();
            } catch (SQLException ex) {
                JiggleExceptionHandler.handleDbException(this.conn, ex, "JiggleDb commit error.", "JiggleDb commit error.: ");
            }
        } else {
            LogUtil.warning("Database JiggleDb error. Cannot rollback with null database connection");
        }
    }
    
}
