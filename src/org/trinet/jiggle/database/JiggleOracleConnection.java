package org.trinet.jiggle.database;

import org.trinet.util.gazetteer.TN.OracleLatLon;

import java.sql.Connection;
import java.sql.SQLException;

public class JiggleOracleConnection extends JiggleConnection {
    @Override
    public String getSequenceQuery(String seqName) {
        return "Select column_value from table (sequence.getSeqTable('" + seqName + "',?))";
    }

    public String getTableExistQuery(){
        return "select 1 from all_tables where table_name = ?";
    }

    public StringBuffer setRowLimitCondition(StringBuffer sb, int maxRow) {
        return sb.insert(0," AND (ROWNUM<=" + maxRow + ") ");
    }

    public java.sql.Array getCoordinate(OracleLatLon[] latLon, Connection conn) throws SQLException {
        // Oracle jdbc qualifies type with user's schema if not fully qualified -aww
        oracle.sql.ArrayDescriptor aDesc = new oracle.sql.ArrayDescriptor("PUBLIC.COORDINATES", conn); // used to be CODE. -aww 2008/04/22
        return new oracle.sql.ARRAY(aDesc, conn, latLon);
        // Requires db version 11.2.0.5.0 or higher
        //myArray = ((OracleConnection) conn).createOracleArray("PUBLIC.COORDINATES", ll);
    }

}
