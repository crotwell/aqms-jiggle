package org.trinet.jiggle.database.event;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import org.trinet.jasi.JasiDatabasePropertyList;
import org.trinet.jiggle.common.AlertUtil;
import org.trinet.jiggle.common.JiggleExceptionHandler;
import org.trinet.jiggle.common.LogUtil;
import org.trinet.jiggle.common.type.AssocEvent;
import org.trinet.jiggle.database.JiggleDb;

public class EventAssocDb extends JiggleDb {

    public EventAssocDb(Connection conn) {
        super(conn);
    }
    
    /**
     * Get associated events for the event id
     * @param eventId
     * @return 
     */
    public ArrayList<AssocEvent> getAssociatedEvents(long eventId) {
        ArrayList<AssocEvent> result = new ArrayList<>();
        AssocEvent event;
        final String sql = "SELECT * FROM ASSOCEVENTS WHERE evid = ?";
        try {
            ps = this.getConnection().prepareStatement(sql);
            ps.setLong(1, eventId);
            rs = ps.executeQuery();
            
            while (rs != null && rs.next()) {
                event = new AssocEvent();
                event.setMasterEventId(rs.getLong("evid"));
                event.setAssocEventId(rs.getLong("evidassoc"));
                event.setCommentId(rs.getLong("commid"));
                
                if (rs.wasNull()) {
                    event.setCommentId(AssocEvent.DEFAULT_ID);
                }
                
                result.add(event);
            }
        } catch (SQLException ex) {
            JiggleExceptionHandler.handleDbException(this.getConnection(), ex, "Error getAssociatedEvents()", "Error getting event association in getAssociatedEvents(): ");
        } finally {
            JasiDatabasePropertyList.debugSQL(sql, ps);
            this.dbCleanUp();
        }
        return result;
    }
    
    /**
     * Delete all event association for the eventId
     * @param eventId
     * @return 
     */
    public boolean deleteAssociatedEvent(long eventId) {
        int numRows = -1;
        final String sql = "DELETE FROM ASSOCEVENTS WHERE evid = ? ";
        try {
            ps = this.getConnection().prepareStatement(sql);
            ps.setLong(1, eventId);
            numRows = ps.executeUpdate();
        } catch (SQLException ex) {
            JiggleExceptionHandler.handleDbException(this.getConnection(), ex, "Error deleteAssociatedEvent()", "Error deleteAssociatedEvent: ");
        } finally {
            JasiDatabasePropertyList.debugSQL(sql, ps);
            this.dbCleanUp();
        }
        return numRows > 0;        
    }
    
    public boolean insertAssociatedEvent(AssocEvent event) {
        int numRows = -1;
        final String sql = "INSERT INTO ASSOCEVENTS (evid, evidassoc, commid) VALUES (?, ?, ?)";
        try {
            ps = this.getConnection().prepareStatement(sql);
            ps.setLong(1, event.getMasterEventId());
            ps.setLong(2, event.getAssocEventId());
            
            if (event.getCommentId()==AssocEvent.DEFAULT_ID) {
                ps.setNull(3, java.sql.Types.BIGINT);
            } else {
                ps.setLong(3, event.getCommentId());
            }
            
            numRows = ps.executeUpdate();
            this.commit();
        } catch (SQLException ex) {
            if (ex.getMessage().toLowerCase().contains("unique_combinations")) {
                // Duplicate entry
                AlertUtil.displayError("Duplicate Association", 
                        "Association already exists between master event Id " + event.getMasterEventId() + " and event id " + event.getAssocEventId(),
                        "Please remove the event and try again.");
            } else {
                JiggleExceptionHandler.handleDbException(this.getConnection(), ex, "Error insertAssociatedEvent()", "Error insertAssociatedEvent: ");
            }
        } finally {
            JasiDatabasePropertyList.debugSQL(sql, ps);
            this.dbCleanUp();
        }
        return numRows > 0;        
    }

    /**
     * Get all unique event ids, both master and associated, used in event association
     * @return 
     */
    public HashSet getAllEventId() {
        HashSet<Long> result = new HashSet<>();
        final String sql = "SELECT * FROM ASSOCEVENTS";
        try {
            LogUtil.debug(JasiDatabasePropertyList.debugSQL, sql);
            
            ps = this.getConnection().prepareStatement(sql);
            rs = ps.executeQuery();
            
            while (rs != null && rs.next()) {
                long masterEventId = rs.getLong("evid");
                long assocEventId = rs.getLong("evidassoc");
                                
                result.add(masterEventId);
                result.add(assocEventId);
            }
        } catch (SQLException ex) {
            JiggleExceptionHandler.handleDbException(this.getConnection(), ex, "Error getAllEventId()", "Error getAllEventId(): ");
        } finally {
            JasiDatabasePropertyList.debugSQL(sql, ps);
            this.dbCleanUp();
        }
        return result;        
    }
    
    /**
     * Check if combination of master event and associated event exists in the database
     * @param masterEventId
     * @param assocEventId
     * @return 
     */
    public AssocEvent isExist(long masterEventId, long assocEventId) {
        AssocEvent event=null;
        try {
            String sql = "SELECT * FROM ASSOCEVENTS " + 
                "WHERE (evid = ? AND evidassoc = ?) " + 
                " OR (evid = ? AND evidassoc = ?)";

            LogUtil.debug(JasiDatabasePropertyList.debugSQL, sql);
            
            // Check both combination as DB unique constraint uses greatest/least
            ps = this.getConnection().prepareStatement(sql);
            ps.setLong(1, masterEventId);
            ps.setLong(2, assocEventId);
            
            // switch order of event id
            ps.setLong(3, assocEventId);
            ps.setLong(4, masterEventId);
            rs = ps.executeQuery();
            
            while (rs != null && rs.next()) {
                event = new AssocEvent();
                event.setMasterEventId(rs.getLong("evid"));
                event.setAssocEventId(rs.getLong("evidassoc"));
                event.setCommentId(rs.getLong("commid"));
                
                if (rs.wasNull()) {
                    event.setCommentId(AssocEvent.DEFAULT_ID);
                }
            }
        } catch (SQLException ex) {
            JiggleExceptionHandler.handleDbException(this.getConnection(), ex, "Error isExist()", "Error checking existing event in isExist(): ");
        } finally {
            this.dbCleanUp();
        }
        return event;        
    }

    /**
     * Get all unique event ids, both master and associated, used by the eventId
     */
    public HashSet getAllEventId(long eventId) {
        HashSet<Long> result = new HashSet<>();
        final String sql = "SELECT * FROM ASSOCEVENTS WHERE (evid = ? OR evidassoc = ?) ";
        try {
            LogUtil.debug(JasiDatabasePropertyList.debugSQL, sql);

            ps = this.getConnection().prepareStatement(sql);
            ps.setLong(1, eventId);
            ps.setLong(2, eventId);
            rs = ps.executeQuery();

            while (rs != null && rs.next()) {
                long masterEventId = rs.getLong("evid");
                long assocEventId = rs.getLong("evidassoc");

                result.add(masterEventId);
                result.add(assocEventId);
            }
        } catch (SQLException ex) {
            JiggleExceptionHandler.handleDbException(this.getConnection(), ex, "Error getAllEventId(eventiId)", "Error getAllEventId(eventiId): ");
        } finally {
            JasiDatabasePropertyList.debugSQL(sql, ps);
            this.dbCleanUp();
        }
        return result;
    }

    /**
     * Get the current comment id on the event id
     */
    public AssocEvent getComment(long eventId) {
        AssocEvent result = null;
        try {
            String sql = "SELECT * FROM ASSOCEVENTS WHERE evid = ? AND commid IS NOT NULL";

            LogUtil.debug(JasiDatabasePropertyList.debugSQL, sql);

            ps = this.getConnection().prepareStatement(sql);
            ps.setLong(1, eventId);
            rs = ps.executeQuery();

            if (rs != null && rs.next()) {
                result = new AssocEvent();
                long masterEventId = rs.getLong("evid");
                long assocEventId = rs.getLong("evidassoc");
                long commentId = rs.getLong("commid");

                // Get one row as they should share the same comment id
                result.setMasterEventId(masterEventId);
                result.setAssocEventId(assocEventId);
                result.setCommentId(commentId);
            }
        } catch (SQLException ex) {
            JiggleExceptionHandler.handleDbException(this.getConnection(), ex, "Error getComment()", "Error getComment(): ");
        } finally {
            this.dbCleanUp();
        }
        return result;
    }
}
