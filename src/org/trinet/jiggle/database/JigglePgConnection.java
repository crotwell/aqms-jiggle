package org.trinet.jiggle.database;

import org.trinet.util.gazetteer.TN.OracleLatLon;

import java.math.BigDecimal;
import java.sql.Array;
import java.sql.Connection;
import java.sql.SQLException;

public class JigglePgConnection extends JiggleConnection {
    @Override
    public String getSequenceQuery(String seqName) {
        return "Select * from sequence.getSeqTable('" + seqName + "',?)";
    }

    public String getTableExistQuery(){
        return "select 1 from information_schema.tables where LOWER(table_name) = LOWER(?)";
    }

    @Override
    public Array getCoordinate(OracleLatLon[] latLon, Connection conn) throws SQLException {
        BigDecimal[][] pgLatLong = new BigDecimal[latLon.length][];

        for (int i=0; i< latLon.length; i++) {
            pgLatLong[i] = new BigDecimal[] {latLon[i].getLat(), latLon[i].getLon()};
        }

        return conn.createArrayOf("float8", pgLatLong); // type for double precision array
        // return conn.createArrayOf("decimal", pgLatLong); // type for numeric array
    }

    @Override
    public StringBuffer setRowLimitCondition(StringBuffer sb, int maxRow) {
        return sb.append(" OFFSET 0 ROWS FETCH NEXT ").append(maxRow).append(" ROWS ONLY");
    }



}
