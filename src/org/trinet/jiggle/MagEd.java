package org.trinet.jiggle;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

import org.trinet.jasi.*;

/**
 * Simple utility dialog to change the magnitude attributes of a solution.
 * Connects to default data source.<p>
 * Usage: MagEd <evid>
 */

public class MagEd {

  public MagEd() {
  }
      public static void main(String args[])
    {
	int evid = 0;

	if (args.length <= 0)	// no args
	{
	  System.out.println
	  ("Usage: MagEd <evid>");
	  System.exit(0);

	}
	  Integer val = Integer.valueOf(args[0]);
	  evid = (int) val.intValue();

        System.out.println ("Making connection...");
	DataSource init = TestDataSource.create();  // make connection
	init.setWriteBackEnabled(true);

        Solution sol = Solution.create().getById(evid);

        if (sol == null ) {
          System.out.println ("No such solution, evid = "+evid);
	  System.exit(0);
        }

        System.out.println (sol.toString());

        JFrame frame = new JFrame("Main");
        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {System.exit(0);}
        });

        frame.pack();
        frame.setVisible(true);

        EventEditDialog dialog = new EventEditDialog(sol);

	boolean status =true;

         if (dialog.getButtonStatus() == JOptionPane.OK_OPTION) {

            sol = dialog.getSolution();
            if (dialog.hasChangedMag()) sol.setPreferredMagnitude(dialog.getMagnitude());

	     try {
	        status = sol.commit();
	     } catch (JasiCommitException ex) {

		System.out.println ("Error during commit, status = "+ status);
		System.exit(-1);
	    }

	    System.out.println ("*Committed*");
	    System.out.println (sol.toString());

         } else {
	    System.out.println ("No change committed.");
	    System.exit(0);
         }
    }
}
