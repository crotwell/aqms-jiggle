package org.trinet.jiggle;
import org.trinet.jasi.SolutionWfEditorPropertyList;
public interface WaveformDisplayIF {
    public WFScroller getWFScroller();
    public ZoomPanel getZoomPanel();
    public SolutionWfEditorPropertyList getWfPanelProperties();
}
