package org.trinet.jiggle;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import org.trinet.util.graphics.text.JTextClipboardPopupMenu;
import org.trinet.jdbc.datasources.AbstractSQLDataSource;

/**
  * This dialog allows user to choose Jiggle preferences
  */
public class DPdatabase extends JPanel {

    JiggleProperties newProps = null; // new properties as changed by this dialog
  
    JTextField driverField = null;

    JTextField dbaseDomainField = null;
    JTextField dbasePortField = null;

    JTextField dbaseTNSField = null;
    JTextField dbaseHostField = null;
    JTextField dbaseNameField = null;

    JTextField userNameField = null;
    JPasswordField passwordField = null;   // don't echo password text

    //JTextField jasiTypeField = null; // removed - aww 2008/06/16
    JCheckBox loginDialogCheckBox = null; // added -aww 2008/06/16

    public DPdatabase(JiggleProperties props) {
        newProps = props;

        try  {
             initGraphics();
        }
        catch(Exception ex) {
             ex.printStackTrace();
        }
    }

    /**
    * Creates the dbase panel which will contain all the fields for the connection information.
    */
    private void initGraphics() throws Exception {

        Box dataPanel = Box.createVerticalBox();
        dataPanel.setBorder( new TitledBorder("Dbase connection info") );

        Dimension dd = new Dimension(110,20);

        // oci or thin ?
        Box hbox = Box.createHorizontalBox();
        JLabel jlbl = new JLabel("Subprotocol ",JLabel.RIGHT);
        jlbl.setPreferredSize(dd);
        hbox.add(jlbl);
        ButtonGroup bg = new ButtonGroup();
        ActionListener al = new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                String str = evt.getActionCommand();
                newProps.setProperty("dbaseSubprotocol", str);
                if (str.contains("oracle")) {
                   resetFieldsByProtocol(! str.equals("oracle:oci"));
                }
            }
        };

        boolean isOracle = newProps.getProperty("dbaseSubprotocol", 
                AbstractSQLDataSource.DEFAULT_DS_SUBPROTOCOL).contains("oracle");
        boolean isThin = newProps.getProperty("dbaseSubprotocol",
                AbstractSQLDataSource.DEFAULT_DS_SUBPROTOCOL ).equals(AbstractSQLDataSource.DEFAULT_DS_SUBPROTOCOL);;
        JRadioButton jrb = new JRadioButton(AbstractSQLDataSource.DEFAULT_DS_SUBPROTOCOL); 
        jrb.setSelected(isOracle && isThin);
        jrb.addActionListener(al);
        bg.add(jrb);
        hbox.add(jrb);

        jrb = new JRadioButton("oracle:oci");
        jrb.addActionListener(al);
        jrb.setSelected(isOracle && !isThin);
        bg.add(jrb);
        hbox.add(jrb);

        jrb = new JRadioButton("postgresql");
        jrb.addActionListener(al);
        jrb.setSelected(!isOracle);
        bg.add(jrb);
        hbox.add(jrb);

        hbox.add(Box.createHorizontalGlue());
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        dataPanel.add(hbox);
        dataPanel.add(Box.createVerticalStrut(10));

        Box connPanel = Box.createVerticalBox(); 
        hbox = Box.createHorizontalBox();
        jlbl = new JLabel("TNS alias ", JLabel.RIGHT);
        jlbl.setPreferredSize(dd);
        hbox.add(jlbl);
        dbaseTNSField = new JTextField(newProps.getProperty("dbaseTNSname",""));
        new JTextClipboardPopupMenu(dbaseTNSField);
        dbaseTNSField.setPreferredSize(new Dimension(220,20));
        dbaseTNSField.setMaximumSize(dbaseTNSField.getPreferredSize());
        dbaseTNSField.setToolTipText("Use for a TNSNAMES.ora alias or its connection definition (host, domain, dbname, port fields must be blank)"); 
        dbaseTNSField.getDocument().addDocumentListener(new MiscDocListener("dbaseTNSname", dbaseTNSField));
        hbox.add(dbaseTNSField);
        hbox.add(Box.createHorizontalGlue());
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        connPanel.add(hbox);
        hbox = Box.createHorizontalBox();


        jlbl = new JLabel("Database host ", JLabel.RIGHT);
        jlbl.setPreferredSize(dd);
        hbox.add(jlbl);
        dbaseHostField = new JTextField(newProps.getProperty("dbaseHost",""));
        new JTextClipboardPopupMenu(dbaseHostField);
        dbaseHostField.setPreferredSize(dd);
        dbaseHostField.setMaximumSize(dbaseHostField.getPreferredSize());
        dbaseHostField.setToolTipText("Name of computer hosting database"); 
        dbaseHostField.getDocument().addDocumentListener(new MiscDocListener("dbaseHost", dbaseHostField));
        hbox.add(dbaseHostField);
        hbox.add(Box.createHorizontalGlue());
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        connPanel.add(hbox);
        hbox = Box.createHorizontalBox();

        jlbl = new JLabel("Database domain ", JLabel.RIGHT);
        jlbl.setPreferredSize(dd);
        hbox.add(jlbl);
        dbaseDomainField = new JTextField(newProps.getProperty("dbaseDomain",""));
        new JTextClipboardPopupMenu(dbaseDomainField);
        dbaseDomainField.setPreferredSize(dd);
        dbaseDomainField.setMaximumSize(dbaseDomainField.getPreferredSize());
        dbaseDomainField.getDocument().addDocumentListener(new MiscDocListener("dbaseDomain", dbaseDomainField));
        dbaseDomainField.setToolTipText("Host computer ip domain, like \"gps.caltech.edu\""); 
        hbox.add(dbaseDomainField);
        hbox.add(Box.createHorizontalGlue());
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        connPanel.add(hbox);
        hbox = Box.createHorizontalBox();

        jlbl = new JLabel("Database name ", JLabel.RIGHT);
        jlbl.setPreferredSize(dd);
        hbox.add(jlbl);
        dbaseNameField = new JTextField(newProps.getProperty("dbaseName",""));
        new JTextClipboardPopupMenu(dbaseNameField);
        dbaseNameField.setPreferredSize(new Dimension(80,20));
        dbaseNameField.setMaximumSize(dbaseNameField.getPreferredSize());
        dbaseNameField.getDocument().addDocumentListener(new MiscDocListener("dbaseName", dbaseNameField));
        hbox.add(dbaseNameField);
        hbox.add(Box.createHorizontalGlue());
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        connPanel.add(hbox);
        hbox = Box.createHorizontalBox();

        jlbl = new JLabel("Database Port # ", JLabel.RIGHT); // 1521
        jlbl.setPreferredSize(dd);
        hbox.add(jlbl);
        dbasePortField = new JTextField(newProps.getProperty("dbasePort",""));
        new JTextClipboardPopupMenu(dbasePortField);
        dbasePortField.setPreferredSize(new Dimension(40,20));
        dbasePortField.setMaximumSize(dbasePortField.getPreferredSize());
        dbasePortField.getDocument().addDocumentListener(new MiscDocListener("dbasePort", dbasePortField));
        hbox.add(dbasePortField);
        hbox.add(Box.createHorizontalGlue());
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        connPanel.add(hbox);
        hbox = Box.createHorizontalBox();

        jlbl = new JLabel("Username ", JLabel.RIGHT);
        jlbl.setPreferredSize(dd);
        hbox.add(jlbl);
        userNameField = new JTextField(newProps.getProperty("dbaseUser",""));
        new JTextClipboardPopupMenu(userNameField);
        userNameField.setPreferredSize(new Dimension(80,20));
        userNameField.setMaximumSize(userNameField.getPreferredSize());
        userNameField.getDocument().addDocumentListener(new MiscDocListener("dbaseUser", userNameField));
        hbox.add(userNameField);
        hbox.add(Box.createHorizontalGlue());
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        connPanel.add(hbox);
        hbox = Box.createHorizontalBox();

        jlbl = new JLabel("Password ", JLabel.RIGHT);
        jlbl.setPreferredSize(dd);
        hbox.add(jlbl);
        passwordField = new JPasswordField(newProps.getProperty("dbasePasswd",""));  // don't echo password text
        new JTextClipboardPopupMenu(passwordField);
        passwordField.setPreferredSize(dd);
        passwordField.setMaximumSize(passwordField.getPreferredSize());
        passwordField.getDocument().addDocumentListener(new MiscPasswordDocListener("dbasePasswd", passwordField));
        hbox.add(passwordField);
        hbox.add(Box.createHorizontalGlue());
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        connPanel.add(hbox);
        hbox = Box.createHorizontalBox();

        jlbl = new JLabel("Driver ", JLabel.RIGHT);
        jlbl.setPreferredSize(dd);
        hbox.add(jlbl);
        driverField = new JTextField(newProps.getProperty("dbaseDriver", org.trinet.jdbc.datasources.AbstractSQLDataSource.DEFAULT_DS_DRIVER));
        new JTextClipboardPopupMenu(driverField);
        driverField.setPreferredSize(new Dimension(180,20));
        driverField.setMaximumSize(driverField.getPreferredSize());
        driverField.getDocument().addDocumentListener(new MiscDocListener("dbaseDriver", driverField));
        hbox.add(driverField);
        hbox.add(Box.createHorizontalGlue());
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        connPanel.add(hbox);
        hbox = Box.createHorizontalBox();

        if ( isOracle ) {
            resetFieldsByProtocol(isThin);
        }

        dataPanel.add(connPanel);

        loginDialogCheckBox = new JCheckBox("Use login dialog on startup");
        loginDialogCheckBox.setSelected(newProps.getBoolean("useLoginDialog"));
        /*
        loginDialogCheckBox.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                JCheckBox jcb = (JCheckBox) evt.getSource();
                if (jcb.isSelected()) {
                   dbaseHostField.setText("");
                   dbaseNameField.setText("");
                   userNameField.setText("");
                   passwordField.setText("");
                }
            }
        });
        */
        dataPanel.add(Box.createVerticalStrut(10));
        loginDialogCheckBox.addItemListener(new BooleanPropertyCheckBoxListener("useLoginDialog"));
        loginDialogCheckBox.setToolTipText("Check to use connection dialog (clears username, password, host, dbname property fields for security");
        loginDialogCheckBox.setAlignmentX(Component.LEFT_ALIGNMENT);
        dataPanel.add(loginDialogCheckBox); // added -aww 2008/06/16

        this.add(dataPanel);

    }

    private void resetFieldsByProtocol(boolean isThin) {

        //if (isThin) dbaseTNSField.setText("");
        //dbaseTNSField.setEnabled(! isThin);
        //dbaseTNSField.setBackground(isThin ? Color.gray : Color.white);

        if (!isThin) dbaseDomainField.setText("");
        dbaseDomainField.setEnabled(isThin);
        dbaseDomainField.setBackground(isThin ? Color.white: Color.gray);

        if (!isThin) dbaseHostField.setText("");
        dbaseHostField.setEnabled(isThin);
        dbaseHostField.setBackground(isThin ? Color.white : Color.gray);

        if (!isThin) dbaseNameField.setText("");
        dbaseNameField.setEnabled(isThin);
        dbaseNameField.setBackground(isThin ? Color.white : Color.gray);

        if (!isThin) dbasePortField.setText("");
        dbasePortField.setEnabled(isThin);
        dbasePortField.setBackground(isThin ? Color.white : Color.gray);
    }

    private class MiscDocListener implements DocumentListener {
        private JTextField jtf = null;
        private String prop = null;

        public MiscDocListener(String prop, JTextField jtf) {
            this.jtf = jtf;
            this.prop = prop;
        }

        public void insertUpdate(DocumentEvent e) {
            setValues(e);
        }
        public void removeUpdate(DocumentEvent e) {
            setValues(e);
        }
        public void changedUpdate(DocumentEvent e) {
            // we won't ever get this with a PlainDocument
        }

        private void setValues(DocumentEvent e) {
            // no sanity checking on numbers !
            newProps.setProperty(prop, jtf.getText().trim());
        }
    }

    private class MiscPasswordDocListener implements DocumentListener {
        private JPasswordField jtf = null;
        private String prop = null;

        public MiscPasswordDocListener(String prop, JPasswordField jtf) {
            this.jtf = jtf;
            this.prop = prop;
        }

        public void insertUpdate(DocumentEvent e) {
            setValues(e);
        }
        public void removeUpdate(DocumentEvent e) {
            setValues(e);
        }
        public void changedUpdate(DocumentEvent e) {
            // we won't ever get this with a PlainDocument
        }

        private void setValues(DocumentEvent e) {
            newProps.setProperty(prop, new String(jtf.getPassword()).trim());
        }
    }

    private class BooleanPropertyCheckBoxListener implements ItemListener {
        private String prop = null;

        public BooleanPropertyCheckBoxListener(String prop) {
            this.prop = prop;
        }

        public void itemStateChanged(ItemEvent e) {
            boolean isSelected = (e.getStateChange() == ItemEvent.SELECTED);
            newProps.setProperty(prop, isSelected);
            if (isSelected) {
                   dbaseHostField.setText("");
                   dbaseNameField.setText("");
                   userNameField.setText("");
                   passwordField.setText("");
            }
        }
    }
}
