package org.trinet.jiggle;
/**
 * Jiggle specific FileChooser extends JFileChooser.
 */

import java.util.*;

// Note: if you use "import java.io.*" the abstract interface FileFilter is found
// by the compiler and you get a compiler error that you can't extend an interface.
// The Filefilter *CLASS* is in javax.swing.filechooser.

//import java.io.*;
import java.io.File;

import java.awt.event.*;
import javax.swing.*;
import javax.swing.filechooser.FileFilter;

import org.trinet.jasi.*;

public class JiggleFileChooser extends JFileChooser {

/**
 * Constructor: needs reference to main frame
 */
    public JiggleFileChooser() {
      super();
    }
/**
 * Constructor: needs reference to main frame
 */
    public JiggleFileChooser(String rootDir) {

        super(rootDir);

        setDialogTitle("Jiggle File Chooser");

     // !!! Multiple selection does NOT work !!! This is a known bug.
     // So don't let chooser pretend it can do it.
     //setMultiSelectionEnabled(false);
     setMultiSelectionEnabled(true); // 5/2002 now works in 1.3

        addChoosableFileFilter(new FileFilterSeed1());
        addChoosableFileFilter(new FileFilterGopher());
        // default to "All Files (*.*)"
        setFileFilter(getAcceptAllFileFilter());

    }

/**
 * pop up the dialog and handle fileChooser interaction
 */
    public int showDialog() {

        return showDialog(null, "Add Files");        // customize "OK" button title

// Handle response

/*
// [Ok] button was clicked
        if (result == JFileChooser.APPROVE_OPTION)
             {

                File selectedFile = getSelectedFile();

                String fileName = selectedFile.getName();
                FileName fnameObj = new FileName(fileName);
                String wholeName = selectedFile.getPath();

                if ( fnameObj.itsSeed1() )
                {
                    SeedReader seed =
                        new SeedReader(wholeName, 512);

                    jig.viewList = null;        // test

                    jig.viewList = seed.getViewList();        // get the ViewList

                    jig.viewList.matchSiteNames(jig.siteList);

                    jig.frameTitle =
                        fileName + ": " + jig.viewList.summaryString();

                    jig.resetGUI();

                }
                else if ( fnameObj.itsSeed2() )
                {
                    SeedReader seed =
                        new SeedReader(wholeName, 4096);

                    jig.viewList = null;        // test

                    jig.viewList = seed.getViewList();        // get the ViewList

                    jig.viewList.matchSiteNames(jig.siteList);

                    jig.frameTitle =
                        fileName + ": " + jig.viewList.summaryString();

                    jig.resetGUI();

                }
                else if (fnameObj.itsGopher())
                {
                    SeedReader seed =
                        new SeedReader(wholeName, 4096);

                    jig.viewList = null;        // test

                    jig.viewList = seed.getViewList();        // get the ViewList

                    jig.viewList.matchSiteNames(jig.siteList);

//                    jig.frameTitle = fileName;
                    jig.frameTitle =
                        fileName + ": " + jig.viewList.summaryString();

                    jig.resetGUI();

                }
                else                        // unknown file type
                {
                    JOptionPane.showMessageDialog(null,
                    "Not a valid data file type: " + selectedFile,
                    "Warning",
                    JOptionPane.ERROR_MESSAGE);

                }


// [Cancel] button was clicked
             } else {
//                System.out.println
//                   ("No file was chosen or an error occured");
             };


    // set fileChooserDir property to where ever we navigated to with the chooser
//v1.2        jig.props.setProperty("fileChooserDir", getCurrentDirectory().toString() );
        jig.props.put("fileChooserDir", getCurrentDirectory().toString() );
*/

    }

    /*
    public static void main(String args[]) {

      JiggleFileChooser chooser = new JiggleFileChooser("e:");
      int status = chooser.showDialog();

        if (status == JFileChooser.APPROVE_OPTION)  {

      File file= chooser.getSelectedFile();

      System.out.println(" status = "+ status);
        System.out.println(file.toString());
      } else {
        System.out.println("Cancelled.");
      }
    }
    */

//////////// Inner Class file filters for JFileChooser
/**
 * JFileChooser file filter for .minfo files
 */
/*
class FileFilterMinfo extends FileFilter
 {
    final String minfo = "minfo";

    // Accept all directories and minfo files.
    public boolean accept(File f) {
        if(f.isDirectory()) {
            return true;
        }
        String s = f.getName();
        int i = s.lastIndexOf('.');
        if(i > 0 &&  i < s.length() - 1) {
            String extension = s.substring(i+1).toLowerCase();
            if (extension.equals(minfo)) {
                return true;
            } else {
                return false;
            }
        };
        return false;
    }

    // The desctiption of this filter
    public String getDescription() {
        return "minfo files (*.minfo)";
    }
 } // end of FileFilterMinfo
*/

/**
 * JFileChooser file filter for one channel Seed files
 */
class FileFilterSeed1 extends FileFilter
 {
    // Accept all directories and minfo files.
    public boolean accept(File f)
    {
        if(f.isDirectory()) return true;

        FileName fname = new FileName( f.getName() );
        if ( fname.itsSeed1() ) return true;

        return false;
    }

    // The desctiption of this filter

    public String getDescription()
    {
//        return "One channel Seed files \n\t (STA.NT.CMP.D.JDY.HRMN)";
        return "One channel Seed files";
    }

 } // end of FileFilterSeed1

/**
 * JFileChooser file filter for one channel Seed files made by
 * the WF_retrieve program
 */
class FileFilterSeed2 extends FileFilter
 {
    // Accept all directories and minfo files.
    public boolean accept(File f)
    {
        if(f.isDirectory()) return true;

        FileName fname = new FileName( f.getName() );
        if ( fname.itsSeed2() ) return true;

        return false;
    }

    // The desctiption of this filter

    public String getDescription()
    {
//        return "WF_retrieve files (SEED) \n\t (X.STA.NT.CMP.k.1)";
        return "WF_retrieve files (SEED)";
    }

 } // end of FileFilterSeed1

 /**
 * JFileChooser file filter for 3 channel Gopher Seed files
 */
class FileFilterGopher extends FileFilter
 {
    // Accept all directories and minfo files.
    public boolean accept(File f)
    {
        if(f.isDirectory()) return true;

        FileName fname = new FileName( f.getName() );
        if ( fname.itsGopher() ) return true;

        return false;
    }

    // The desctiption of this filter

    public String getDescription()
    {
//        return "3 channel Gopher files \n\t (yrmodyhrmnsc.sta.cmp.k.1)";
        return "3 channel Gopher files";
    }

 } // end of FileFilterGopher


} // end of class

// ////////////////////////////////////////////////////////////////////////////
/**
 * Allow various manipulations of a file name not available in the
 * File or String Classes
 */
class FileName //extends String
{
 String filename;

 public FileName(File file)
 {
    filename = file.getName();
 }

 public FileName(String fn)
 {
    filename = fn;
 }

/**
 * Return file type
 */
    public String getFileType() {
        int i = filename.lastIndexOf('.');
        return ( filename.substring(i+1) );
    }
/**
 * True if the file name matches the pattern of a one channel Seed file
 *  1) length >= 26 characters
 *  2) contains exactly 6 "."s
 */
// Example: MWC.CI.HHZ.D.1998.194.1700
    public boolean itsSeed1() {

            if (filename.length() < 26) return false;    // < 26 char long

        if (countThese (filename, ".") != 6) return false; // must have 6 "."'s

        return true;
    }
/**
 * True if the file name matches the pattern of a one channel Seed
 * file created by the WF_retrieve program
 *  1) starts with "X." and ends with ".k.1"
 *  2) contains exactly 5 "."s
 */
// Example: X.CI.WIN.VLN.k.1

    public boolean itsSeed2()
    {
        if (!filename.endsWith(".k.1")) return false;

        if (!filename.startsWith("X.")) return false;

        if (countThese (filename, ".") != 5) return false; // must have 6 "."'s

        return true;
    }
/**
 * True if the file name matches the pattern of a Gopher Seed file
 * These contain 3-components, Z,N,E for a given site/instrument type
 *  1) length >= 22 characters
 *  2) contains exactly 4 "."s
 *  3) ends in ".1"
 *  4) 1st . is at position 12
 */
//           0123456789012345678901234
// Example:  980812063443.rvr.vsp.k.1
// Example:  980812181648.vtv.lg.k.1
//
    public boolean itsGopher() {

        if (filename.length() < 22) return false;

        if (filename.indexOf(".") != 12) return false;

        if (!filename.endsWith(".1")) return false;

        if (countThese (filename, ".") != 4) return false; // must have 4 "."'s

        return true;
    }
/**
 * Count occurances of 'these' in 'str'
 */
   public int countThese(String str, String these) {

        int nDots = 0;
        int lastIndex = -1;
        while (true)
        {
          lastIndex = str.indexOf(these, lastIndex+1);
          if (lastIndex == -1) return nDots;
          nDots++;
        }
//        return 0;        // will never be reached but prevents compiler warning
    }
}
