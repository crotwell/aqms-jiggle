package org.trinet.jiggle;

/**
 * A chooser for event types. The containing object must add an actionListener
 * to handle changes made with this chooser.
 *
 * @see org.trinet.jasi.GTypeMap
 *
 */

import java.util.*;
import java.awt.Dimension;
import javax.swing.*;            // JFC "swing" library
import javax.swing.event.*;
import java.awt.event.*;
import org.trinet.jasi.*;
import org.trinet.util.*;

public class GTypeChooser extends JComboBox implements ChangeListener, ListDataStateListener {

    /** List of event type choices which is defined in org.trinet.jasi.GTypeMap. */
    //String typeChoice[] = GTypeMap.getJasiTypeArray();
    String typeChoice[] = GTypeMap.getDbTypeArray();

    /** Create an GTypeChooser. Set the current selection to the default
        which is the first item in the list. */
    public GTypeChooser() {
        super();
        makeComboBox(typeChoice, typeChoice[0]);
    }

    /** Create an GTypeChooser. Set the current selection to item given. */
    public GTypeChooser(String selectedType) {
        super();
        makeComboBox(typeChoice, selectedType);
    }

    /** Create an GTypeChooser. Set the current selection to item given. */
    public GTypeChooser(String [] choiceList, String selectedType) {
        super();
        ArrayList myChoices = new ArrayList(choiceList.length);
        for (int i = 0; i< choiceList.length; i++) {
          for (int j = 0; j< typeChoice.length; j++) {
            if (typeChoice[j].equals(choiceList[i])) {
                myChoices.add(choiceList[i]);
            }
          }
        }
        String choice = (selectedType == null) ? choiceList[0] : selectedType;
        makeComboBox((String []) myChoices.toArray(new String [myChoices.size()]), choice);
    }

    private void makeComboBox(String [] choiceList, String selectedItem) {
        for (int i = 0; i< choiceList.length; i++) {
            addItem(choiceList[i].toUpperCase());
        }
        setEditable(false);                    // don't allow freeform user input
        setSelectedItem(selectedItem);            // default selection
        setMaximumRowCount(choiceList.length);  // # items displayed in scrolling window
    }

/** Handle external solution change. */
    public void stateChanged(ChangeEvent e) {
      Object obj = e.getSource();
      Solution sol;
      if (obj instanceof Solution) {
         sol = (Solution)obj;
      } else if (obj instanceof SolutionList) {
         sol = (Solution)((SolutionList)obj).getSelected();
      } else {
         return;   // noop
      }
      updateGTypeSelection(sol);
    }
    protected void updateGTypeSelection(Solution sol) {
      //final String sGType = sol.getOriginGTypeString();
      final String sGType = GTypeMap.toDbType(sol.getOriginGTypeString());
      if (getSelectedItem().equals(sGType)) return; // aww
      SwingUtilities.invokeLater(
          new Runnable() {
              public void run() {
                  setSelectedItem(sGType);
              }
          }
      );
    }
// ListDataStateListener for list changes -aww
    public void intervalAdded(ListDataStateEvent e) {
       Object obj = e.getSource();
       if (! (obj instanceof SolutionList)) return;
       updateGTypeSelection((Solution)((SolutionList)obj).getSelected());
    }
    public void intervalRemoved(ListDataStateEvent e) {
       Object obj = e.getSource();
       if (! (obj instanceof SolutionList)) return;
       updateGTypeSelection((Solution)((SolutionList)obj).getSelected());
    }
    public void contentsChanged(ListDataStateEvent e) {
       Object obj = e.getSource();
       if (! (obj instanceof SolutionList)) return;
       updateGTypeSelection((Solution)((SolutionList)obj).getSelected());
    }
    public void orderChanged(ListDataStateEvent e) {
       Object obj = e.getSource();
       if (! (obj instanceof SolutionList)) return;
       updateGTypeSelection((Solution)((SolutionList)obj).getSelected());
    }
    public void stateChanged(ListDataStateEvent e) {
       Object obj = e.getSource();
       if (! (obj instanceof SolutionList)) return;
       updateGTypeSelection((Solution)((SolutionList)obj).getSelected());
    }

} // GTypeChooser
