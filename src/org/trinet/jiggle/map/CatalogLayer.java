package org.trinet.jiggle.map;

import java.beans.PropertyChangeEvent;

import org.trinet.jasi.SolutionList;
import org.trinet.jiggle.CatalogPanel;
import org.trinet.jiggle.Jiggle;
import org.trinet.jiggle.JiggleMapBean;

/**
* CatalogLayer displays data from  a Jiggle CatalogPanel SolutionList.
* Listens for a property change from JiggleMapBean for new CatalogPanel SolutionList.
*/
public class CatalogLayer extends SolutionListLayer {

    public CatalogLayer() {
        super();
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getSource() instanceof JiggleMapBean) {
          String name = evt.getPropertyName();
          //System.out.println(getClass().getName() + " DEBUG propertyChange name = " + name); 
          if (name.equals(Jiggle.NEW_CP_PROPNAME)) {
            //SolutionList sl =  ((CatalogPanel)evt.getNewValue()).getSolutionList();
            //System.out.println("CatalogPanel solution list is null ? " + ((sl == null) ? "null" : String.valueOf(sl.size())));
            setSolutionList(((CatalogPanel)evt.getNewValue()).getSolutionList() );
          }
        }
        super.propertyChange(evt);
    }

    @Override
    public void findAndInit(Object obj) {
        if (obj instanceof JiggleMapBean) {
            //System.out.println("CatalogLayer findAndInit ");
            JiggleMapBean mapBean = (JiggleMapBean) obj;
            mapBean.addPropertyChangeListener(this);
            setSolutionList(mapBean.getCpSolutionList());
            this.jiggle = mapBean.jiggle;
            mapBean.catalogLayer = this;
        }
        super.findAndInit(obj);
    }

}
