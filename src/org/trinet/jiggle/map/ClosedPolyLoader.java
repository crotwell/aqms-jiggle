package org.trinet.jiggle.map;

import com.bbn.openmap.omGraphics.EditableOMGraphic;
import com.bbn.openmap.omGraphics.EditableOMPoly;
import com.bbn.openmap.omGraphics.GraphicAttributes;
import com.bbn.openmap.omGraphics.OMGraphic;
import com.bbn.openmap.omGraphics.OMPoly;
import com.bbn.openmap.omGraphics.labeled.EditableLabeledOMPoly;
import com.bbn.openmap.tools.drawing.OMPolyLoader;
import com.bbn.openmap.tools.drawing.EditClassWrapper;
import com.bbn.openmap.tools.drawing.EditToolLoader;

/**
 * Loader that knows how to create/edit OMPoly objects.
 */
public class ClosedPolyLoader extends OMPolyLoader{

    public ClosedPolyLoader() {
        init();
    }

    /**
     * Give the classname of a graphic to create, returning an
     * EditableOMGraphic for that graphic. The GraphicAttributes
     * object lets you set some of the initial parameters of the poly,
     * like poly type and rendertype.
     */
    @Override
    public EditableOMGraphic getEditableGraphic(String classname,
                                                GraphicAttributes ga) {
        if (classname.intern() == graphicClassName) {
            EditableOMPoly eomg =  new EditableOMPoly(ga);
            eomg.setEnclosed(true);
            return eomg;
        }
        if (classname.intern() == labeledClassName) {
            EditableLabeledOMPoly eomg = new EditableLabeledOMPoly(ga);
            eomg.setEnclosed(true);
            return eomg;
        }
        return null;
    }

    /**
     * Give an OMGraphic to the EditToolLoader, which will create an
     * EditableOMGraphic for it.
     */
    @Override
    public EditableOMGraphic getEditableGraphic(OMGraphic graphic) {
        if (graphic instanceof OMPoly) {
            EditableOMPoly eomg =  new EditableOMPoly((OMPoly) graphic);
            eomg.setEnclosed(true);
            return eomg;
        }
        return null;
    }
}
