package org.trinet.jiggle.map;

import java.awt.Color;
import java.util.Properties;
import java.util.Iterator;
import java.util.ArrayList;

import com.bbn.openmap.dataAccess.shape.EsriGraphicList;
import com.bbn.openmap.dataAccess.shape.DbfTableModel;
import com.bbn.openmap.omGraphics.OMColor;
import com.bbn.openmap.omGraphics.OMGraphic;
import com.bbn.openmap.plugin.esri.EsriLayer;
import com.bbn.openmap.util.PropUtils;
import com.bbn.openmap.util.ColorFactory;

/**
 */
public class SeismicHazardLayer extends EsriLayer {
  private static Color [] colors = new Color [20];
  static {
      int ii = 0;
      colors[ii++] = ColorFactory.createColor(0xffffffff);
      colors[ii++] = ColorFactory.createColor(0xffd3cecd);
      colors[ii++] = ColorFactory.createColor(0xffb1dbac);
      colors[ii++] = ColorFactory.createColor(0xff959291);
      colors[ii++] = ColorFactory.createColor(0xff6c6968);
      colors[ii++] = ColorFactory.createColor(0xff03d1cb);
      colors[ii++] = ColorFactory.createColor(0xff03b5d0);
      colors[ii++] = ColorFactory.createColor(0xff029f9b);
      colors[ii++] = ColorFactory.createColor(0xff028985);
      colors[ii++] = ColorFactory.createColor(0xff7ffd4a);
      colors[ii++] = ColorFactory.createColor(0xffcefd4c);
      colors[ii++] = ColorFactory.createColor(0xfff1fd25);
      colors[ii++] = ColorFactory.createColor(0xffe1e304);
      colors[ii++] = ColorFactory.createColor(0xffe3b604);
      colors[ii++] = ColorFactory.createColor(0xffe38c03);
      colors[ii++] = ColorFactory.createColor(0xffe36203);
      colors[ii++] = ColorFactory.createColor(0xffe32c01);
      colors[ii++] = ColorFactory.createColor(0xfffb0a03);
      colors[ii++] = ColorFactory.createColor(0xffc50802);
      colors[ii++] = ColorFactory.createColor(0xffa30602);
  }

  int valCol = -1;

  public SeismicHazardLayer() { }

  @Override
  public Properties getProperties(Properties props) {
        props = super.getProperties(props);
        return getProperties(propertyPrefix, props);
  }

  public Properties getProperties(String prefix, Properties props) {
      props = super.getProperties(props);
      prefix = PropUtils.getScopedPropertyPrefix(prefix);
      return props;
  }

  @Override
  public Properties getPropertyInfo(Properties list) {
    Properties props = super.getPropertyInfo(list);
    return props;
  }

  @Override
  public void setProperties(String prefix, Properties properties) {

    super.setProperties(prefix, properties);
    
    prefix = PropUtils.getScopedPropertyPrefix(prefix);

    //setup the colors
    final DbfTableModel model = getModel();
    if (model != null)  {
      final int columnCount = model.getColumnCount();
      //determine the columns for the rgb value
      String columnName = null;
      for (int column = 0; column < columnCount; column++) {
        columnName = model.getColumnName(column);
        if (columnName.equals("ACC_VAL")) valCol = column;
      }
      setColors(getEsriGraphicList());
    }
  }

  int count = 0;
  private void setColor(OMGraphic omg) {
      //get the application object which should be the record of dbf file (1..N)
      Object appObj = omg.getAppObject();
      Color c = OMColor.clear;
      if (appObj instanceof ArrayList) {
        ArrayList row = (ArrayList) appObj;
        int val = ((Number) row.get(valCol)).intValue();
        int idx = 0;
        if (val < 0) idx = 0;
        else if (val < 10 ) idx = val;
        else if (val < 30 ) idx = val/5 + 8;
        else if (val < 40 ) idx = val/10 + 11;
        else idx = val/20 + 13;
        if (idx >= colors.length) idx = colors.length - 1;
        //System.out.println("SeismicHazard val: " + val + " color idx: " + idx);
        c = colors[idx];
      }
      else System.out.println("SeismicHazard no appObject for color value.");
      omg.setLinePaint(Color.black);
      omg.setFillPaint(c);
      count++;
  }
  private void setColor(EsriGraphicList list) {
      list.setTraverseMode(EsriGraphicList.LAST_ADDED_ON_TOP);
      Object appObj = list.getAppObject();
      Iterator it = list.iterator();
      while (it.hasNext()) {
        final Object element = it.next();
        if (element instanceof OMGraphic) {
          OMGraphic omg = (OMGraphic) element;
          omg.setAppObject(appObj);
          setColor(omg);
        }
        else if (element instanceof EsriGraphicList) {
          EsriGraphicList omg = (EsriGraphicList) element;
          omg.setAppObject(appObj);
          setColor(omg);
        }
      }
  }

  /**
   * Sets the colors for the layer graphics.
  */
  private void setColors(EsriGraphicList listObj) {
      if (listObj == null) return;
      //given list object is OK reverse the traverse mode
      listObj.setTraverseMode(EsriGraphicList.LAST_ADDED_ON_TOP);

      Iterator it = listObj.iterator();
      while (it.hasNext()) {
        final Object element = it.next();
        if (element instanceof EsriGraphicList) {
          setColor((EsriGraphicList)element);
        }
        else if (element instanceof OMGraphic) {
          setColor((OMGraphic) element);
        }
      }
      System.out.println("Total count from SeismicHazardLayer = " + count);
    }

  @Override
    public String getInfoText(OMGraphic omg) {
      // Do we want this displayed, probably not
      if (valCol < 0) return null;
      int index = getEsriGraphicList().indexOf(omg);
      if (index >= 0) return (String) getModel().getValueAt(index,valCol);
      return null;
    }

    @Override
    public String getToolTipTextFor(OMGraphic omg) {
      return null;
    }
 
}
