package org.trinet.jiggle.map;

import java.awt.Color;
import java.util.Properties;

import org.trinet.util.GenericPropertyList;

public class MapPropertyList extends GenericPropertyList {

    public static final String BACKGROUND_COLOR_PROPERTY  = "backGroundColor";
    public static final String MIN_ZOOM_SCALE_PROPERTY    = "minZoomScale";
    public static final String ZOOM_SCALE_FACTOR_PROPERTY = "zoomScaleFactor";
    public static final String USER_CENTERLAT_PROPERTY    = "userCenterLat";
    public static final String USER_CENTERLON_PROPERTY    = "userCenterLon";
    public static final String USER_MAPSCALE_PROPERTY     = "userMapScale";
    public static final String RESET_MAPSCALE_PROPERTY    = "resetMapScale";
    public static final String RESET_CENTERLAT_PROPERTY   = "resetCenterLat";
    public static final String RESET_CENTERLON_PROPERTY   = "resetCenterLon";

    public static final float DEFAULT_MIN_ZOOM_SCALE = 12207.03125f;   // (1000 feet) ??
    public static final float DEFAULT_ZOOM_FACTOR = 2.f;
    public static final Color DEFAULT_BACKGROUND_COLOR = Color.white;
    public static final float DEFAULT_RESET_CENTER_LAT = 35.f; 
    public static final float DEFAULT_RESET_CENTER_LON = -116.f; 
    public static final float DEFAULT_RESET_MAP_SCALE = 2000000.f;
    public static final float DEFAULT_USER_CENTER_LAT = DEFAULT_RESET_CENTER_LAT; 
    public static final float DEFAULT_USER_CENTER_LON = DEFAULT_RESET_CENTER_LON; 
    public static final float DEFAULT_USER_MAP_SCALE = DEFAULT_RESET_MAP_SCALE;

/**
 */

    public MapPropertyList(Properties props) {
        super(props);
    }

/**
 *  Constructor: copies a property list. Doesn't read the files.
 */
    public MapPropertyList(MapPropertyList props) {
        super(props);
        // copy class instance data members:
        this.userPropertiesFileName = props.userPropertiesFileName;
        this.defaultPropertiesFileName = props.defaultPropertiesFileName;
        this.filename = props.filename;
        this.filetype = props.filetype;
    }
/**
 *  Constructor: copies a property list then reads the files.
 */
    public MapPropertyList(MapPropertyList props, String dpfn, String upfn) {
        this(props);
        this.userPropertiesFileName = upfn;
        this.defaultPropertiesFileName = dpfn;
        reset();
    }
/**
 *  Constructor: loads specified properties then loads from specified file.
 */
    public MapPropertyList(Properties props, String filename) {
        super(props);
        setFilename(filename);
        reset();
    }
/**
 *  Constructor: reads the default files specified.
 */
    public MapPropertyList(String filename) {
        this((Properties) null,filename);
    }

    public float getMinZoomScale() {
        float zs = getFloat(MIN_ZOOM_SCALE_PROPERTY);
        return Float.isNaN(zs) ? DEFAULT_MIN_ZOOM_SCALE : zs;
    }

    public float getZoomScaleFactor() {
        float zs = getFloat(ZOOM_SCALE_FACTOR_PROPERTY);
        return Float.isNaN(zs) ? DEFAULT_ZOOM_FACTOR : zs;
    }

    public Color getBackgroundColor() {
        Color c = DEFAULT_BACKGROUND_COLOR;
        if (isSpecified(BACKGROUND_COLOR_PROPERTY)) {
            c = getColor(BACKGROUND_COLOR_PROPERTY);
        }
        return c;
    }

    public float getUserCenterLat() {
        float f = getFloat(USER_CENTERLAT_PROPERTY);
        return Float.isNaN(f) ? DEFAULT_USER_CENTER_LAT : f;
    }

    public float getUserCenterLon() {
        float f = getFloat(USER_CENTERLON_PROPERTY);
        return Float.isNaN(f) ? DEFAULT_USER_CENTER_LON : f;
    }

    public float  getUserMapScale() {
        float f = getFloat(USER_MAPSCALE_PROPERTY);
        return Float.isNaN(f) ? DEFAULT_USER_MAP_SCALE : f;
    }

    public float getResetCenterLat() {
        float f = getFloat(RESET_CENTERLAT_PROPERTY);
        return Float.isNaN(f) ? DEFAULT_RESET_CENTER_LAT : f;
    }

    public float getResetCenterLon() {
        float f = getFloat(RESET_CENTERLON_PROPERTY);
        return Float.isNaN(f) ? DEFAULT_RESET_CENTER_LON : f;
    }

    public float getResetMapScale() {
        float f = getFloat(RESET_MAPSCALE_PROPERTY);
        return Float.isNaN(f) ? DEFAULT_RESET_MAP_SCALE : f;
    }
}
