package org.trinet.jiggle.map;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.io.Serializable;

import com.bbn.openmap.MapBean;
import com.bbn.openmap.gui.OMToolComponent;
import com.bbn.openmap.proj.Mercator;
import com.bbn.openmap.proj.Projection;
import com.bbn.openmap.proj.ProjectionFactory;

public class ResetMapViewTool extends OMToolComponent implements Serializable {
    private MapBean map = null;
    private JButton setViewButton = null;
    private JButton goToViewButton = null;
    // NOTE: need to clone projection when getting/setting else aliased
    private Projection myProj; 
    private static final String defaultKey = "ResetMapView";

    public ResetMapViewTool() {
        setKey(defaultKey);
        setViewButton = new JButton("Set");
        setViewButton.setMaximumSize(new Dimension(80,30));
        setViewButton.setEnabled(false);
        setViewButton.setToolTipText("Cache current view (bounds and scale).");
        setViewButton.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                if (map != null) {
                    myProj = map.getProjection().makeClone();
                }
            }
        });

        goToViewButton = new JButton("View");
        goToViewButton.setMaximumSize(new Dimension(80,30));
        goToViewButton.setEnabled(false);
        goToViewButton.setToolTipText("Display last cached view (bounds and scale).");
        goToViewButton.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                if (map == null) return;
                if (myProj == null) {
                    myProj = map.getProjection().makeClone();
                }
                else if ( ! myProj.equals(map.getProjection()) ) {
                    map.setProjection(myProj.makeClone());
                }
            }
        });
        Box box = Box.createHorizontalBox();
        box.add(Box.createHorizontalStrut(5));
        box.add(new JLabel("<html><div align=\"center\"><p>Area<p>of<p>Interest</div></html>"));
        Box vbox = Box.createVerticalBox();
        vbox.add(goToViewButton);
        vbox.add(setViewButton);
        box.add(vbox);
        box.add(Box.createHorizontalStrut(5));
        add(box);
        //setPreferredSize(new Dimension(125,60));
    }

    @Override
    public void findAndUndo(Object someObj) {
        super.findAndUndo(someObj);
        if (someObj instanceof MapBean) {
            if (getMap() == (MapBean) someObj) {
                setMap(null);
            }
        }
    }

    @Override
    public void findAndInit(Object someObj) {
        super.findAndInit(someObj);
        if (someObj instanceof MapBean) {
            setMap((MapBean) someObj);
        }
    }

    public void setMap(MapBean mb) {
        map = mb;
        setViewButton.setEnabled((map != null));
        goToViewButton.setEnabled((map != null));
    }

    public MapBean getMap() {
        return map;
    }

}
