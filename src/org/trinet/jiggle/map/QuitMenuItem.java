package org.trinet.jiggle.map;

import java.awt.Container;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class QuitMenuItem extends JMenuItem {
    private Container w = null; 

    public QuitMenuItem() {
        super("Quit");
        addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }
        
    public QuitMenuItem(Container c) {
        super("Quit");
        setQuitContainer(c);
        addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }

    public void setQuitContainer(Container c) {
        this.w = c;
    }

    private void dispose() {
        if (w == null) return;
        else if (w instanceof Window)
            ((Window)w).dispose();
        else if (w instanceof JInternalFrame)
            ((JInternalFrame)w).dispose();
    }
}
