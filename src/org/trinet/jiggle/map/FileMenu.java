package org.trinet.jiggle.map;

import javax.swing.*;

import com.bbn.openmap.*;
import com.bbn.openmap.gui.AbstractOpenMapMenu;
import com.bbn.openmap.gui.menu.*;

/**
 * FileMenu creates AboutMenuItem, SavePropertiesMenuItem,
 * SaveImageMenuItem  It only adds AboutMenuItem if
 * runing as an Applet, all otherwise. These menu items are added by
 * default.
 */
public class FileMenu extends AbstractOpenMapMenu {

    private String defaultText = "File";
    private int defaultMnemonic = 'F';

    /**
     * Create and add menuitems(About, SaveProperties, SaveAsImage
     * 
     */
    public FileMenu() {
        super();
        setText(defaultText);
        setMnemonic(defaultMnemonic);

        add(new AboutMenuItem());

        if (!Environment.isApplet()) {
            add(new JSeparator());
            add(new SavePropertiesMenuItem());
            add(new LoadPropertiesMenuItem());
            add(new JSeparator());
            add(new SaveAsMenu());
            add(new MapBeanPrinterMenuItem());
            add(new JSeparator());
        }
    }
}
