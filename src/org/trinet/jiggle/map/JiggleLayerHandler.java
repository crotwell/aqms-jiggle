// Layer tooltip/status functionality adopted from the IstiLayerHandler class
//
package org.trinet.jiggle.map;

import java.util.*;
import com.bbn.openmap.Layer;
import com.bbn.openmap.LayerHandler;
import com.bbn.openmap.util.PropUtils;
import com.bbn.openmap.event.LayerStatusListener;
import com.bbn.openmap.event.LayerStatusEvent;  

import com.isti.util.gui.ToolTipInformation;

public class JiggleLayerHandler extends LayerHandler implements LayerStatusListener, ToolTipInformation {

  public static final String TOOLTIP_PROPERTY = "showMapToolTip";

  private final boolean DEFAULT_TOOLTIP_STATE = false;

  private boolean toolTipEnabled = DEFAULT_TOOLTIP_STATE;

  private Vector listenerList = new Vector();

  public JiggleLayerHandler() {
      super();
  }

  public JiggleLayerHandler(Properties props) {
      super(props);
  }

  public JiggleLayerHandler(String prefix, Properties props) {
     super(prefix, props);
  }

  /**
   * Remove a layer from the list of potentials.
   */
  public synchronized void removeLayer(Layer layer)
  {
    //remove this layer handler as a layer status listener
    layer.removeLayerStatusListener(this);
    super.removeLayer(layer);
  }

  /**
   * PropertyConsumer method, to fill in a Properties object,
   * reflecting the current values of all of the layers.
   *
   * @param props a Properties object to load the PropertyConsumer
   * properties into.  If props equals null, then a new Properties
   * object should be created.
   * @return Properties object containing PropertyConsumer property
   * values.
  */
  @Override
  public Properties getProperties(Properties props) {
    return getProperties(propertyPrefix, props);
  }

  public Properties getProperties(String prefix, Properties props) {
    props = super.getProperties(props);
    prefix = PropUtils.getScopedPropertyPrefix(prefix);
    props.put(prefix + TOOLTIP_PROPERTY, String.valueOf(toolTipEnabled));
    return props;
  }

  @Override
  public void setProperties(String prefix, Properties props) {
    prefix = PropUtils.getScopedPropertyPrefix(prefix);
    toolTipEnabled =
        PropUtils.booleanFromProperties(props, prefix + TOOLTIP_PROPERTY, DEFAULT_TOOLTIP_STATE);
    super.setProperties(prefix, props);
  }

  /**
   * Adds a listener for <code>LayerStatusEvent</code>s.
   * @param aLayerStatusListener LayerStatusListener
   */
  public void addLayerStatusListener(LayerStatusListener aLayerStatusListener) {
    listenerList.add(aLayerStatusListener);
  }

  /**
   * Removes a LayerStatusListene from this Layer.
   * @param aLayerStatusListener the listener to remove
   */
  public void removeLayerStatusListener(LayerStatusListener aLayerStatusListener) {
    listenerList.remove(aLayerStatusListener);
  }

  /**
   * Sends a status update to the LayerStatusListener.
   * @param evt  the <code>LayerStatusEvent</code> object
   */
  public void fireStatusUpdate(LayerStatusEvent evt) {
    final Object[] listenersObj = listenerList.toArray();
    // Process the listeners last to first, notifying
    // those that are interested in this event
    for (int i = listenersObj.length-1; i >= 0; i--) {
      ((LayerStatusListener)listenersObj[i]).updateLayerStatus(evt);
    }
  }

  /**
   * Update the Layer status.
   * @param evt LayerStatusEvent
   */
  @Override
  public void updateLayerStatus(LayerStatusEvent evt) {
    fireStatusUpdate(evt);
  }

  @Override
  public synchronized void setLayerList(List<Layer> layers) {
    //for each of the previous layers remove this as a layer status listener
    for (Layer l : allLayers) {
      l.removeLayerStatusListener(this);
    }

    super.setLayerList(layers);

    //for each of the new layers add this as a layer status listener
    for (Layer l : allLayers) {
      //notify the layer of the current tool tip enabled flag
      notifyToolTipEnabled(l, toolTipEnabled);
      //add this layer handler as a layer status listener
      l.addLayerStatusListener(this);
    }

  }

  /**
   * Notifies all of the layers of the current tool tip enabled flag.
   */
  protected void notifyToolTipEnabled() {
    for (Layer l : allLayers) {
      //notify the layer of the current tool tip enabled flag
      notifyToolTipEnabled(l, toolTipEnabled);
    }
  }

  /**
   * Notifies the specified layer of the current tool tip enabled flag.
   * @param layer the layer.
   */
  protected void notifyToolTipEnabled(Layer layer) {
    notifyToolTipEnabled(layer, toolTipEnabled);
  }

  /**
   * Notifies the specified layer of the tool tip enabled flag.
   * @param layer the layer.
   * @param b true if the tool tip is enabled, false otherwise.
   */
  protected static void notifyToolTipEnabled(Layer layer, boolean tf) {
    if (layer instanceof ToolTipInformation) {
        ( (ToolTipInformation) layer).setToolTipEnabled(tf);
    }
    //else if (layer != null) System.out.println("DEBUG JiggleLayerHandler layer not instanceof ToolTipInformation: " + layer.getClass().getName());
  }

  //----------------------------------------------------------------
  // ToolTipInformation interface methods
  //----------------------------------------------------------------

  /**
   * Determines if the tool tip is enabled or not.
   * @return true if the tool tip is enabled, false otherwise.
   */
  @Override
  public boolean isToolTipEnabled() {
    return toolTipEnabled;
  }

  /**
   * Enables or disables the tool tip.
   * @param b true if the tool tip is enabled, false otherwise.
   */
  @Override
  public void setToolTipEnabled(boolean tf) {
    toolTipEnabled = tf;
    //notify all of the layers of the current tool tip enabled flag
    notifyToolTipEnabled();
  }

}
