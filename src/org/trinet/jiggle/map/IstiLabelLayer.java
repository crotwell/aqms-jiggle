//Adapted from original source in package com.isti.quakewatch.omutil.IstiLabelLayer
//which is a label layer based on "openmap/layer/LabelLayer.java".
package org.trinet.jiggle.map;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.text.*;

import javax.swing.*;

import com.bbn.openmap.Layer;
import com.bbn.openmap.event.*;
import com.bbn.openmap.omGraphics.OMText;
import com.bbn.openmap.proj.Projection;
import com.bbn.openmap.util.PropUtils;
import com.bbn.openmap.util.ColorFactory;

/**
 * Layer that displays a label.
 * This layer understands the following properties:
 * <code><pre>
 * # like XWindows geometry: [+-]X[+-]Y, `+' indicates relative to
 * # left edge or top edges, `-' indicates relative to right or bottom
 * # edges, XX is x coordinate, YY is y coordinate
 * label.xyPosition=+20-30
 * # background rectangle color (ARGB)
 * label.backgroundColor=ffffffff
 * # foreground text color (ARGB)
 * label.foregroundColor=ff000000
 * # label text
 * label.text=The Graph
 * </pre></code>
 * <p>
 * In addition to the previous properties, you can get this layer to
 * work with the OpenMap viewer by adding/editing the additional
 * properties in your <code>openmap.properties</code> file:
 * <code><pre>
 * # layers
 * openmap.layers=label ...
 * label.class=org.trinet.jiggle.map.IstiLabelLayer
 * label.prettyName=Label Layer
 * </pre></code>
 * NOTE: the color properties do not support alpha value if running on
 * JDK 1.1...
 */
public class IstiLabelLayer extends Layer
    implements MapMouseListener, ComponentListener
{
  // property keys
  public final static transient String fgColorProperty = "foregroundColor";
  public final static transient String bgColorProperty = "backgroundColor";
  public final static transient String geometryProperty = "XY.position";
  public final static transient String labelProperty = "text";

  // properties
  protected Color fgColor = Color.black;
  protected Color bgColor = Color.white;
  protected String geometryString = "+30+30";
  protected String labelText = "";

  protected OMText text;

  protected int xpos=10;
  protected int ypos=10;
  protected String xgrav=geometryString.substring(0,1);
  protected String ygrav=geometryString.substring(3,4);

  private int dragX;
  private int dragY;
  private boolean dragging = false;

  /**
   * Construct the IstiLabelLayer.
   */
  public IstiLabelLayer()
  {
    text = new OMText(0, 0, "uninitialized",
                      OMText.DEFAULT_FONT, OMText.JUSTIFY_RIGHT);
    text.setLinePaint(fgColor);
    text.setMatted(true);
    text.setMattingPaint(bgColor); // ?? -aww
    //text.setBoundsLineColor(bgColor); // method not in class -aww
    //text.setBoundsFillColor(bgColor); // method not in class -aww

    addComponentListener(this);
  }

  /**
   * Sets the properties for the <code>Layer</code>.
   * @param prefix the token to prefix the property names
   * @param props the <code>Properties</code> object
   */
  @Override
  public void setProperties(String prefix, Properties props)
  {
    super.setProperties(prefix, props);
    prefix = PropUtils.getScopedPropertyPrefix(prefix);

    fgColor = ColorFactory.parseColorFromProperties(
        props, prefix+fgColorProperty,
        colorToPropertyString(fgColor));

    bgColor = ColorFactory.parseColorFromProperties(
        props, prefix+bgColorProperty,
        colorToPropertyString(bgColor));

    geometryString = props.getProperty(prefix+geometryProperty,
                                       geometryString);
    parseGeometryString();

    labelText = props.getProperty(
        prefix+labelProperty, labelText);

    // reset the property values
    text.setLinePaint(fgColor);
    text.setMattingPaint(bgColor); // ?? -aww
    //text.setBoundsLineColor(bgColor);
    //text.setBoundsFillColor(bgColor);
  }

  /**
   * Converts the given integer color value to a property string name
   * corresponding to the color.
   * @param colorObj the color.
   * @return A property string representing the color.
   */
  public static String colorToPropertyString(Color colorObj) {
    String hexString = Integer.toHexString(colorObj.getRGB());
    for (int len = hexString.length(); len < 8; len++) hexString = "0" + hexString;
    return hexString.toUpperCase();
  }
  
  /**
   * PropertyConsumer method, to fill in a Properties object,
   * reflecting the current values of the layer.  If the
   * layer has a propertyPrefix set, the property keys should
   * have that prefix plus a separating '.' prepended to each
   * propery key it uses for configuration.
   *
   * @param props a Properties object to load the PropertyConsumer
   * properties into.  If props equals null, then a new Properties
   * object should be created.
   * @return Properties object containing PropertyConsumer property
   * values.  If getList was not null, this should equal getList.
   * Otherwise, it should be the Properties object created by the
   * PropertyConsumer.
   */
  @Override
  public Properties getProperties(Properties props)
  {
    props = super.getProperties(props);
    String prefix = PropUtils.getScopedPropertyPrefix(propertyPrefix);

    props.setProperty(prefix+fgColorProperty,
                      colorToPropertyString(fgColor));

    props.setProperty(prefix+bgColorProperty,
                      colorToPropertyString(bgColor));

    props.setProperty(prefix+geometryProperty, geometryString);

    props.setProperty(prefix+labelProperty, labelText);
    return props;
  }

  /**
   * Method to fill in a Properties object with values reflecting
   * the properties able to be set on this PropertyConsumer.  The
   * key for each property should be the raw property name (without
   * a prefix) with a value that is a String that describes what the
   * property key represents, along with any other information about
   * the property that would be helpful (range, default value,
   * etc.).  For Layer, this method should at least return the
   * 'prettyName' property.
   *
   * @param list a Properties object to load the PropertyConsumer
   * properties into.  If getList equals null, then a new Properties
   * object should be created.
   * @return Properties object containing PropertyConsumer property
   * values.  If getList was not null, this should equal getList.
   * Otherwise, it should be the Properties object created by the
   * PropertyConsumer.
   */
  @Override
  public Properties getPropertyInfo(Properties list)
  {
    list = super.getPropertyInfo(list);

    list.setProperty(fgColorProperty, "Foreground color");

    list.setProperty(bgColorProperty, "Background color");

    list.setProperty(geometryProperty, "[+-]X[+-]Y  where '+' is relative to left/top and '-' is relative to right/bottom");

    return list;
  }

  // parse X-like geometry string
  protected void parseGeometryString()
  {
    int i=0;
    byte[] bytes = geometryString.getBytes();
    xgrav = new String(bytes, 0, 1);
    for (i=2; i<bytes.length; i++)
    {
      if ((bytes[i] == '-') || (bytes[i] == '+'))
        break;
    }
    if (i == bytes.length)
      return;
    ygrav = (bytes[i] == '-') ? "-" : "+";
    xpos = Integer.parseInt(new String(bytes, 1, i-1));
    ++i;
    ypos = Integer.parseInt(new String(bytes, i, bytes.length-i));
  }

  // position the text graphic
  protected synchronized void positionText(int w, int h)
  {
    int xoff, yoff, justify;
    if (xgrav.equals("+"))
    {
      xoff = xpos;
      justify = OMText.JUSTIFY_LEFT;
    }
    else
    {
      xoff = w - xpos;
      justify = OMText.JUSTIFY_RIGHT;
    }
    if (ygrav.equals("+"))
    {
      yoff = ypos;
    }
    else
    {
      yoff = h - ypos;
    }
    text.setX(xoff);
    text.setY(yoff);
    text.setJustify(justify);
  }

  /**
   * Set the text to display
   * @param s String
   */
  public void setLabelText(String s)
  {
    labelText = s;
  }

  /**
   * Get the String to display
   * @return String
   */
  public String getLabelText()
  {
    return labelText;
  }

  /**
   * Invoked when the projection has changed or this Layer has been
   * added to the MapBean.
   * @param e ProjectionEvent
   */
  @Override
  public synchronized void projectionChanged(ProjectionEvent e)
  {
    setProjection(e);
    repaint();
  }

  /**
   * Implementing the ProjectionPainter interface.
   * @param proj Projection
   * @param g Graphics
   */
  @Override
  public synchronized void renderDataForProjection(Projection proj, Graphics g){
    if (proj != null){
      setProjection(proj.makeClone());
      paint(g);
    }
  }

  /**
   * Paints the layer.
   * @param g the Graphics context for painting
   */
  @Override
  public void paint(Graphics g)
  {
    Projection p = getProjection();

    if (p == null) return;

    positionText(p.getWidth(), p.getHeight());
    text.setData(labelText);
    text.generate(p);//to get bounds

    // render graphics
    text.render(g);
  }

  /**
   * This method is set up to receive the RedrawCmd to redraw the layer.
   * @param e ActionEvent
   */
  @Override
  public void actionPerformed(ActionEvent e)
  {
    super.actionPerformed(e);
    String cmd = e.getActionCommand();
    if (cmd == RedrawCmd)
    {
      repaint();
    }
  }

  //----------------------------------------------------------------------
  // MapMouseListener Interface
  //----------------------------------------------------------------------

  /**
   * Returns the MapMouseListener object that handles the mouse
   * events.
   * @return MapMouseListener this
   */
  @Override
  public MapMouseListener getMapMouseListener()
  {
    return this;
  }

  /**
   * Return a list of the modes that are interesting to the
   * MapMouseListener.
   * @return String[] { SelectMouseMode.modeID }
   */
  @Override
  public String[] getMouseModeServiceList()
  {
    return new String[]
    {
      SelectMouseMode.modeID
    };
  }

  // Mouse Listener events
  ////////////////////////

  /**
   * Invoked when a mouse button has been pressed on a component.
   * @param e MouseEvent
   * @return false
   */
  @Override
  public boolean mousePressed(MouseEvent e){
    int x = e.getX();
    int y = e.getY();
    if (text.distance(x, y) <= 0f)
    {
      dragging = true;
      dragX = x;
      dragY = y;
      return true;
    }
    return false; // did not handle the event
  }

  /**
   * Invoked when a mouse button has been released on a component.
   * @param e MouseEvent
   * @return false
   */
  @Override
  public boolean mouseReleased(MouseEvent e){
    dragging = false;
    return false;
  }

  /**
   * Invoked when the mouse has been clicked on a component.
   * @param e MouseEvent
   * @return false
   */
  @Override
  public boolean mouseClicked(MouseEvent e){
    return false;
  }

  /**
   * Invoked when the mouse enters a component.
   * @param e MouseEvent
   */
  @Override
  public void mouseEntered(MouseEvent e){
  }

  /**
   * Invoked when the mouse exits a component.
   * @param e MouseEvent
   */
  @Override
  public void mouseExited(MouseEvent e){
  }

  // Mouse Motion Listener events
  ///////////////////////////////

  /**
   * Invoked when a mouse button is pressed on a component and then
   * dragged.  The listener will receive these events if it
   * @param e MouseEvent
   * @return false
   */
  @Override
  public boolean mouseDragged(MouseEvent e){
    Projection proj = getProjection();
    int w = proj.getWidth();
    int h = proj.getHeight();
    int x = e.getX();
    int y = e.getY();

    // limit coordinates
    if (x < 0) x = 0;
    if (y < 0) y = 0;
    if (x > w) x = w;
    if (y > h) y = h;

    // calculate deltas
    int dx = x-dragX;
    int dy = y-dragY;

    if (dragging)
    {
      // reset dragging parms
      dragX = x;
      dragY = y;
      // reset graphics positions
      text.setX(text.getX()+dx);
      text.setY(text.getY()+dy);
      repaint();
      return true;
    }
    return false;
  }

  /**
   * Invoked when the mouse button has been moved on a component
   * (with no buttons down).
   * @param e MouseEvent
   * @return false
   */
  @Override
  public boolean mouseMoved(MouseEvent e){
    return false;
  }

  /**
   * Handle a mouse cursor moving without the button being pressed.
   * Another layer has consumed the event.
   */
  @Override
  public void mouseMoved(){
  }

  //----------------------------------------------------------------------
  // ComponentListener implementation
  //----------------------------------------------------------------------

  /**
   * Invoked when component has been resized.
   * @param e ComponentEvent
   */
  @Override
  public void componentResized(ComponentEvent e)
  {
    Rectangle bounds = e.getComponent().getBounds();

    // move to home coordinates if out-of-bounds
    Polygon poly = text.getPolyBounds();
    if (poly != null)
    {
      if (!bounds.intersects(poly.getBounds()))
      {
        positionText(bounds.width, bounds.height);
      }
    }
  }

  /**
   * Invoked when component has been moved.
   * @param e ComponentEvent
   */
  @Override
  public void componentMoved(ComponentEvent e)
  {
  }

  /**
   * Invoked when component has been shown.
   * @param e ComponentEvent
   */
  @Override
  public void componentShown(ComponentEvent e)
  {
  }

  /**
   * Invoked when component has been hidden.
   * @param e ComponentEvent
   */
  @Override
  public void componentHidden(ComponentEvent e)
  {
  }
}
