package org.trinet.jiggle.map;

import java.io.IOException;
import java.io.InputStream;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;
import java.util.Vector;

import com.bbn.openmap.Environment;
import com.bbn.openmap.PropertyHandler;
import com.bbn.openmap.plugin.PlugIn;
import com.bbn.openmap.util.Debug;
import com.bbn.openmap.util.PropUtils;

import org.trinet.jiggle.Jiggle;

public class JiggleMapPropertyHandler extends PropertyHandler {

    private Jiggle jiggle = null;

    public JiggleMapPropertyHandler(Jiggle jiggle)  throws MalformedURLException, IOException {
        this(jiggle, null);
    }

    public JiggleMapPropertyHandler(Jiggle jiggle, String urlString) throws MalformedURLException, IOException {

        DEBUG = Debug.debugging("properties"); // What about toggling when jiggle debug flag is set true too? -aww

        this.jiggle = jiggle;
        String location = urlString;
        if ( jiggle != null && location == null) {
            location = jiggle.getProperties().getUserFileNameFromProperty("mapPropFilename");
        }

        URL url = PropUtils.getResourceOrFileOrURL(location);

        InputStream is = null;

        if (url != null) {
            // Open URL to read in properties
            is = url.openStream();
        }

        Properties tmpProperties = new Properties();
        if (is != null) {
            tmpProperties.load(is);
        }

        Vector debugList = PropUtils.parseSpacedMarkers(tmpProperties.getProperty(Environment.DebugList));
        int size = debugList.size();
        for (int i = 0; i < size; i++) {
            String debugMarker = (String) debugList.elementAt(i);
            Debug.put(debugMarker);
            if (DEBUG) {
                Debug.output("JiggleMapPropertyHandler: adding " + debugMarker
                        + " to Debug list.");
            }
        }
        DEBUG = Debug.debugging("properties") || Debug.debugging("propertiesdetail");


        init(tmpProperties, "URL");
        Environment.init(getProperties());
    }

    //protected void init(Properties props, String howString) { super.init(props, howString); }

    protected Properties getIncludeProperties(String markerList, Properties props) {
        Properties newProps = new Properties();
        Properties tmpProps = new Properties();
        Vector includes = PropUtils.parseSpacedMarkers(markerList);
        int size = includes.size();
        if (size > 0) {

            if (Debug.debugging("propertiesdetail")) {
                Debug.output("JiggleMapPropertyHandler: handling include files: "
                        + includes);
            }

            for (int i = 0; i < size; i++) {
                String includeName = (String) includes.elementAt(i);
                String includeProperty = includeName + ".URL";
                String include = props.getProperty(includeProperty);

                File file = new File(include);
                if (! file.isAbsolute() && jiggle != null) {
                    include = jiggle.getProperties().getUserFilePath() + System.getProperty("file.separator") + include;
                    file = new File(include);
                }

                try {
                  if (include == null || ! file.canRead()) {
                    Debug.error("JiggleMapPropertyHandler.getIncludeProperties(): Failed to locate include name \""
                            + includeName + "\" with URL \"" + includeProperty + "\"\n  Skipping include file \"" + include + "\"");
                    continue;
                  }
                }
                catch (SecurityException ex) { System.err.println(ex.getMessage()); }

                try {
                    tmpProps.clear();
                    // Open URL to read in properties
                    URL tmpInclude = PropUtils.getResourceOrFileOrURL(null,
                            include);

                    if (tmpInclude == null) {
                        continue;
                    }

                    InputStream is = tmpInclude.openStream();
                    tmpProps.load(is);
                    if (DEBUG) {
                        Debug.output("JiggleMapPropertyHandler.getIncludeProperties(): located include properties file URL: "
                                + include);
                    }
                    // Include properties noted in resources
                    // properties - a little recursive action,
                    // here.
                    Properties includeProperties = getIncludeProperties(tmpProps.getProperty(includeProperty),
                            tmpProps);
                    merge(includeProperties,
                            newProps,
                            "include file properties",
                            "within " + include);

                    merge(tmpProps,
                            newProps,
                            "include file properties",
                            include);

                } catch (MalformedURLException e) {
                    Debug.error("JiggleMapPropertyHandler: malformed URL for include file: |"
                            + include + "| for " + includeName);
                } catch (IOException ioe) {
                    Debug.error("JiggleMapPropertyHandler: IOException processing "
                            + include + "| for " + includeName);
                }
            }
        } else {
            Debug.message("properties",
                    "JiggleMapPropertyHandler.getIncludeProperties(): no include files found.");
        }
        return newProps;
    }

}
