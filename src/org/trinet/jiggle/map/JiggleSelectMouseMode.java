//package com.bbn.openmap.event;
package org.trinet.jiggle.map;
import javax.swing.Timer;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import com.bbn.openmap.MapBean;
import java.awt.event.MouseWheelEvent;
import com.bbn.openmap.event.SelectMouseMode;
import com.bbn.openmap.event.ZoomEvent;

public class JiggleSelectMouseMode extends SelectMouseMode {

    private MapBean mb = null;

    private int scrollEvtCnt = 0;

    javax.swing.Timer timer = null;

    { // using timer to collect wheel scroll "roll clicks" over 1 sec then fire zoom update of mapbean
      timer = new javax.swing.Timer(1000, new ActionListener() {
                  public void actionPerformed(ActionEvent evt) {
                      if (scrollEvtCnt == 0 || mb == null) return;
                      else if (scrollEvtCnt > 0) {
                        mb.zoom(new ZoomEvent(mb, ZoomEvent.RELATIVE, (float)Math.pow(1.1, scrollEvtCnt)));
                      }
                      else {
                        mb.zoom(new ZoomEvent(mb, ZoomEvent.RELATIVE, (float)Math.pow(.9f, Math.abs(scrollEvtCnt))));
                      }

                      timer.stop();
                      scrollEvtCnt = 0;
                }
            }
        );
    }

    public JiggleSelectMouseMode() {
        this(true);
    }

    public JiggleSelectMouseMode(boolean consumeEvents) {
        this(SelectMouseMode.modeID, consumeEvents);
    }

    public JiggleSelectMouseMode(String id, boolean consumeEvents) {
        super(id, consumeEvents);
    }

    @Override
    protected Class getClassToUseForIconRetrieval() {
       return com.bbn.openmap.event.SelectMouseMode.class;
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        int rot = e.getWheelRotation();
        if (e.getSource() instanceof MapBean) {
            //System.out.println(e.paramString());
            timer.start();
            mb = (MapBean) e.getSource();
            if (rot > 0) { // Positive, zoom out
                scrollEvtCnt++;
            } else {
                scrollEvtCnt--;
            }
        }
    }
}
