package org.trinet.jiggle.map; 

import java.awt.Component;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Properties;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;

import com.bbn.openmap.LightMapHandlerChild;
import com.bbn.openmap.PropertyConsumer;
import com.bbn.openmap.util.PropUtils;
import com.bbn.openmap.gui.*;

/**
 * An all-in-one panel that holds an overview
 * map, pan and zoom buttons, projection stack buttons, scale text
 * field and a LayersPanel. All of the sub-components share the same
 * property prefix as the panel, all have access to
 * components in the MapHandler. The sub-components are not given to
 * the MapHandler themselves, however.
 */
public class MapControlPanel extends OMComponentPanel implements MapPanelChild {

    LinkedList children = new LinkedList();

    public MapControlPanel() {

        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        JPanel navBox = new JPanel();
        navBox.setBorder(BorderFactory.createEmptyBorder(0, 10, 10, 10));
        navBox.setLayout(new BorderLayout());

        OverviewMapHandler overviewMap = new OverviewMapHandler();
        overviewMap.setUseAsTool(false);
        overviewMap.setPreferredSize(new Dimension(100, 100));
        overviewMap.setBorder(BorderFactory.createRaisedBevelBorder());
        overviewMap.setPropertyPrefix("OverviewMapHandler");
        children.add(overviewMap);

        MapNavigatePanel navPanel = new MapNavigatePanel();
        navPanel.setPropertyPrefix("NavigatePanel");
        MapZoomPanel zoomPanel = new MapZoomPanel();
        zoomPanel.setPropertyPrefix("ZoomPanel");
        ProjectionStackTool projStack = new ProjectionStackTool();
        projStack.setPropertyPrefix("ProjectionStackTool");
        ScaleTextPanel scalePanel = new ScaleTextPanel();
        scalePanel.setPropertyPrefix("ScaleTextPanel");
        scalePanel.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 0));

        JPanel navBoxRN = new JPanel();
        children.add(navPanel);
        navBoxRN.add((Component)navPanel);
        navBoxRN.add(Box.createHorizontalGlue());
        children.add(zoomPanel);
        navBoxRN.add(zoomPanel);

        JPanel navBoxRS = new JPanel();
        navBoxRS.setLayout(new BorderLayout());
        children.add(projStack);
        children.add(scalePanel);
        navBoxRS.add(projStack, BorderLayout.NORTH);
        navBoxRS.add(scalePanel, BorderLayout.SOUTH);

        JPanel navBoxR = new JPanel();
        navBoxR.setLayout(new BorderLayout());
        navBoxR.add(navBoxRN, BorderLayout.NORTH);
        navBoxR.add(navBoxRS, BorderLayout.SOUTH);

        navBox.add(overviewMap, BorderLayout.CENTER);
        navBox.add(navBoxR, BorderLayout.EAST);

        add(navBox);

        LayersPanel layersPanel = new LayersPanel();
        layersPanel.setPropertyPrefix("LayersPanel");
        layersPanel.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 10));
        children.add(layersPanel);
        add(layersPanel);
        validate();
    }

    @Override
    public void setProperties(String prefix, Properties props) {
        super.setProperties(prefix, props);

        prefix = PropUtils.getScopedPropertyPrefix(prefix);
        String pl = props.getProperty(prefix + PreferredLocationProperty);
        if (pl != null) {
            setPreferredLocation(pl);
        }

        Iterator it = children.iterator();
        while (it.hasNext()) {
            Object obj = it.next();
            if (obj instanceof PropertyConsumer) {
                // Each property prefix will be set with the marker
                // name for the ControlPanel plus the class name
                // already set as property prefix in the constructor.
                String newPrefix = prefix
                        + ((PropertyConsumer) obj).getPropertyPrefix();
                ((PropertyConsumer) obj).setProperties(newPrefix, props);
            }
        }
    }

    @Override
    public Properties getProperties(Properties props) {
        props = super.getProperties(props);

        props.put(PropUtils.getScopedPropertyPrefix(this)
                + PreferredLocationProperty, getPreferredLocation());

        Iterator it = children.iterator();
        while (it.hasNext()) {
            Object obj = it.next();
            if (obj instanceof PropertyConsumer) {
                ((PropertyConsumer) obj).getProperties(props);
            }
        }
        return props;
    }

    @Override
    public Properties getPropertyInfo(Properties props) {
        props = super.getPropertyInfo(props);

        props.put(PreferredLocationProperty,
                "The preferred BorderLayout direction to place this component.");

        Iterator it = children.iterator();
        while (it.hasNext()) {
            Object obj = it.next();
            if (obj instanceof PropertyConsumer) {
                ((PropertyConsumer) obj).getPropertyInfo(props);
            }
        }
        return props;
    }

    @Override
    public void findAndInit(Object someObj) {
        Iterator it = children.iterator();
        while (it.hasNext()) {
            Object obj = it.next();
            if (obj instanceof LightMapHandlerChild) {
                ((LightMapHandlerChild) obj).findAndInit(someObj);
            }
        }
    }

    @Override
    public void findAndUndo(Object someObj) {
        Iterator it = children.iterator();
        while (it.hasNext()) {
            Object obj = it.next();
            if (obj instanceof LightMapHandlerChild) {
                ((LightMapHandlerChild) obj).findAndUndo(someObj);
            }
        }
    }

    /**
     * BorderLayout.WEST by default for this class.
     */
    protected String preferredLocation = java.awt.BorderLayout.WEST;

    /**
     * MapPanelChild method.
     */
    @Override
    public void setPreferredLocation(String value) {
        preferredLocation = value;
    }

    /**
     * MapPanelChild method.
     */
    @Override
    public String getPreferredLocation() {
        return preferredLocation;
    }

    public MapNavigatePanel getNavigatePanel() {
        Iterator it = children.iterator();
        MapNavigatePanel p = null;
        while (it.hasNext()) {
            Object obj = it.next();
            if (obj instanceof MapNavigatePanel ) {
                p = (MapNavigatePanel) obj;
            }
        }
        return p;
    }
    public ZoomPanel getZoomPanel() {
        Iterator it = children.iterator();
        ZoomPanel p = null;
        while (it.hasNext()) {
            Object obj = it.next();
            if (obj instanceof ZoomPanel ) {
                p = (ZoomPanel) obj;
            }
        }
        return p;
    }

    @Override
    public String getParentName() {
      return "";
    }
}

