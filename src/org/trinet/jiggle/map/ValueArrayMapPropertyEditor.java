//ValueArrayMapPropertyEditor.java adapted from original source in package com.isti.util.propertyeditor;
package org.trinet.jiggle.map;

import java.beans.PropertyEditorSupport;
import java.beans.*;
import java.awt.event.ActionListener;
import java.awt.Component;
import java.awt.event.*;
import javax.swing.*;
import java.util.Vector;
import java.util.Enumeration;
import org.trinet.util.StringList;

//import com.isti.util.UtilFns;

/**
 * A PropertyEditor for a value array.
 */
public class ValueArrayMapPropertyEditor  {
  /** The GUI component of this editor. */
  protected final JComboBox comboBox;
  private final boolean useListFlag;  //true to use list

  /**
   * Creates A PropertyEditor for a value array.
   */
  public ValueArrayMapPropertyEditor()
  {
    comboBox = new JComboBox();
    useListFlag = true;  //use list
  }

  /**
   * Creates A PropertyEditor for a value array.
   * @param validValuesArr valid values array.
   */
  public ValueArrayMapPropertyEditor(Object [] validValuesArr)
  {
    comboBox = new JComboBox(validValuesArr);
    useListFlag = false;  //use single value
  }

  /**
   * Determines whether the propertyeditor can provide a custom editor.
   * @return true
   */
  public boolean supportsCustomEditor()
  {
    return true;
  }

  /**
   * Returns the editor GUI.
   * @return component for editor.
   */
  public Component getCustomEditor()
  {
    final JPanel panel = new JPanel();
    panel.add(comboBox);
    return panel;
  }

  /**
   * Sets value.
   * @param someObj value
   */
  public void setValue(Object someObj)
  {
    if (someObj == null)
      return;

    //select as string
    setAsText(someObj.toString());
  }

  /**
   * Sets value as text.
   * @param text value
   */
  public void setAsText(String text)
  {
    if (text == null)
      return;

    if (useListFlag) {
      //use the list and use the default of the first item selected
      //final Object[] items = UtilFns.listStringToVector(text).toArray();
      final Object [] items = new StringList(text).toArray();
      comboBox.setModel(new DefaultComboBoxModel(items));
      return;
    }

    //try to select the item
    setSelectedItem(text);

    //exit if selection matches
    if (comboBox.getSelectedItem().equals(text))
      return;

    //look for the text in the combo box items
    for (int i = 0; i < comboBox.getItemCount(); i++)
    {
      //if an item with the text was found
      final Object someObj = comboBox.getItemAt(i);
      if (someObj.toString().equalsIgnoreCase(text))
      {
        //set that item and exit
        setSelectedItem(someObj.toString());
        return;
      }
    }
  }

  /**
   * Returns value.
   * @return the value
   */
  public Object getValue() {

    final Object selectedItem = comboBox.getSelectedItem();

    if (useListFlag) {
      //add all items to the list and
      // put selected item first so that it remains selected
      final int itemCount = comboBox.getItemCount();
      Vector items = new Vector(itemCount);
      Object obj = null;
      items.add(selectedItem);
      for (int i = 0; i < itemCount; i++) {
        obj = comboBox.getItemAt(i);
        if (!obj.equals(selectedItem)) items.add(obj);
      }
      //return UtilFns.enumToListString(items.elements());
      Enumeration e = items.elements();
      boolean loop = e.hasMoreElements();
      StringBuffer sb = new StringBuffer(); //create string buffer
      while (loop) { //more than zero elements available
        obj = e.nextElement();
        if (obj != null) sb.append(obj.toString()); //if not null then add it
        if (e.hasMoreElements()) sb.append(", ");
        else break;
      }
      return sb.toString();       //return string version of buffer

    }

    return selectedItem;
  }

  /**
   * Returns value as a String.
   * @return the value as a string
   */
  public String getAsText()
  {
    return getValue().toString();
  }

  /**
   * Adds an <code>ActionListener</code> to the button.
   * @param l the <code>ActionListener</code> to be added
   */
  public void addActionListener(ActionListener l)
  {
    comboBox.addActionListener(l);
  }

  /**
   * Removes an <code>ActionListener</code> from the button.
   * If the listener is the currently set <code>Action</code>
   * for the button, then the <code>Action</code>
   * is set to <code>null</code>.
   *
   * @param l the listener to be removed
   */
  public void removeActionListener(ActionListener l)
  {
    comboBox.removeActionListener(l);
  }

  /**
   * Sets the selected item in the <code>JComboBox</code> by
   * specifying the object in the list.
   * If <code>anObject</code> is in the list, the list displays with
   * <code>anObject</code> selected.
   *
   * @param anObject  the list object to select
   */
  protected void setSelectedItem(String anObject)
  {
    comboBox.setSelectedItem(anObject);
  }
}
