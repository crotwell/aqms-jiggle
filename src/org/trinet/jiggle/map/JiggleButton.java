package org.trinet.jiggle.map;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import com.bbn.openmap.gui.OMToolComponent;
import org.trinet.jiggle.Jiggle;
import org.trinet.jiggle.JiggleMapBean;
import org.trinet.util.graphics.IconImage;

public class JiggleButton extends OMToolComponent {

    private static final String defaultKey = "jiggleButtonTool";
    private Jiggle jiggle = null;
    private JButton jb = new JButton();

    public JiggleButton() {
        super();
        setKey(defaultKey);
        jb.addActionListener(
           new ActionListener() {
               public void actionPerformed(ActionEvent e) {
                   if (jiggle != null) {
                       jiggle.toFront();
                       jiggle.setExtendedState(Frame.NORMAL); // restored from iconified -aww
                   }
               }
           }
        );
        jb.setActionCommand(key);
        jb.setIcon(new ImageIcon(IconImage.getImage("JiggleLogo.gif")));
        jb.setMaximumSize(new Dimension(40,40));
        jb.setPreferredSize(new Dimension(40,40));
        add(jb);
    }

    @Override
    public void findAndInit(Object obj) {
        super.findAndInit(obj);
        if (obj instanceof JiggleMapBean) {
            jiggle = ((JiggleMapBean) obj).jiggle;
        }
    }

    @Override
    public void findAndUndo(Object obj) {
        super.findAndUndo(obj);
        if (obj instanceof JiggleMapBean) {
          jiggle = null;
        }
    }
}
