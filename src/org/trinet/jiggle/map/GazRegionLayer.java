    /*
    protected OMGraphic createPolygon(MysqlPolygon myPoly) {
        Vector v = myPoly.getRings();
        int size = v.size();
        OMGraphic ret = null;
        OMPoly ompoly = null;
        OMGraphicList subList = null;
        if (size > 1) {
            subList = new OMGraphicList();
            ret = subList;
        }
        for (int i = 0; i < size; i++) {
            ompoly = new OMPoly(DoubleToFloat((double[]) v.elementAt(i)), OMGraphic.DECIMAL_DEGREES, OMGraphic.LINETYPE_STRAIGHT);
            drawingAttributes.setTo(ompoly);
            if (subList != null) {
                subList.add(ompoly);
            } else {
                ret = ompoly;
            }
        }
        return ret;
    }
*/

package org.trinet.jiggle.map;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import com.bbn.openmap.Environment;
import com.bbn.openmap.I18n;
//import com.bbn.openmap.dataAccess.shape.EsriShapeExport;
import com.bbn.openmap.event.*;
import com.bbn.openmap.layer.OMGraphicHandlerLayer;
//import com.bbn.openmap.layer.DrawingToolLayer;
import com.bbn.openmap.omGraphics.*;
//import com.bbn.openmap.omGraphics.event.MapMouseInterpreter;
import com.bbn.openmap.util.*;
//import com.bbn.openmap.proj.Length;
import com.bbn.openmap.proj.ProjMath;
import com.bbn.openmap.proj.Projection;
//import com.bbn.openmap.tools.drawing.DrawingTool;

import java.awt.*;
import java.awt.event.*;
import java.util.Properties;
import javax.swing.*;

import org.trinet.jiggle.Jiggle;
import org.trinet.jiggle.JiggleProperties;
import org.trinet.jiggle.JiggleMapBean;
import org.trinet.jasi.TN.SolutionTN;
import org.trinet.jasi.DataSource;
//import org.trinet.util.DoubleRange;
//import org.trinet.util.GenericPropertyList;
import org.trinet.util.StringList;
//import org.trinet.util.gazetteer.LatLonZ;

/**
 * This layer can receive graphics from the OMDrawingToolLauncher, and
 * also sent it's graphics to the OMDrawingTool for editing.
 * <P>
 * 
 * The projectionChanged() and paint() methods are taken care of in
 * the OMGraphicHandlerLayer superclass.
 * <P>
 * 
 * This class responds to all the properties that the
 * OMGraphicHandlerLayer repsonds to, including the mouseModes
 * property. If the mouseModes property isn't set, the
 * SelectMouseMode.modeID mode ID is set. When the MapMouseInterpreter
 * calls select(OMGraphic), the OMGraphic is passed to the
 * DrawingTool. This class also responds to the showHints property
 * (true by default), which dictates if tooltips and information
 * delegator text is displayed when the layer's contents are moused
 * over.
 *    
 *     # The Edge or Line color
 *     lineColor=AARRGGBB (Hex ARGB Color, black is default)
 *
 *     # The Fill color for 2D shapes
 *     fillColor=AARRGGBB (Hex ARGB Color, clean is default)
 *
 *     # A highlight color to switch a graphic to when &quot;selected&quot;.
 *     selectColor=AARRGGBB (Hex ARGB Color, black is default)
 *
 *     # A file or URL that can be used for a fill pattern, in place of the fill color.
 *     fillPattern=file://file (default is N/A)
 *
 *     # The line width of the edge of the graphic
 *     lineWidth=int (1 is default)
 *
 *     # A pattern to use for a dashed line, reflected as a
 *     # space-separated list of numbers, which are interpreted as on dash
 *     # length, off dash length, on dash length, etc.  
 *     dashPattern=10 5 3 5 (5 5 is the default if an error occurs reading the numbers, a non-dashed line is the default.)  
 *
 *     The phase for the dash pattern,
 *     dashPhase=0.0f (0 is the default)
 *
 *     # The scale to use for certain measurements, so that fill patterns
 *     # can be scaled depending on the map scale compaired to the
 *     # baseScale.
 *     baseScale=XXXXXX (where 1:XXXXXX is the scale to use.  N/A for the default).
 *
 *     # Set whether any OMPoints that are given to the DrawingAttributes object are oval or rectangle.
 *     pointOval=false
 *
 *     # Set the pixel radius of any OMPoint given to the DrawingAttributes object.
 *     pointRadius=2
 *   
 *
 */

public class GazRegionLayer extends OMGraphicHandlerLayer implements PropertyChangeListener {
//public class GazRegionLayer extends DrawingToolLayer implements PropertyChangeListener {

    protected Jiggle jiggle = null;
    protected GraphicAttributes defaultGraphicAttributes = new GraphicAttributes();

    //public int maxHorNumLoaderButtons = -1;
    //protected RegionFilterLauncher launcher = null;
    //private boolean querySave = false;

    //public static final String REGION_CATEGORY_PROPERTY = "regionCategory";
    //public static final String DEFAULT_REGION_CATEGORY = "*";
    //protected String regionCategory = DEFAULT_REGION_CATEGORY;

    public static final String REGION_TYPE_PROPERTY = "regionGraphicType";
    public static final String DEFAULT_REGION_TYPE = "polygon";
    private String regionType = DEFAULT_REGION_TYPE;

    public static final String REGION_NAMES_PROPERTY = "regionNamesProperty";
    public static final String DEFAULT_REGION_NAMES_PROPERTY = "regionNames";
    protected String regionNamesProperty = DEFAULT_REGION_NAMES_PROPERTY;
    private String [] regionNames = null;

    // DrawingAttributes
    public static final String LINE_COLOR_PROPERTY = "lineColor";
    public static final String SELECT_LINE_COLOR_PROPERTY = "selectColor";
    public static final String ACTIVE_LINE_COLOR_PROPERTY = "activeColor";
    //private static final String defaultLineColorString =  "000000";
    //private static final String defaultSelectLineColorString = "ffffff00";
    private static final String defaultActiveLineColorString = "ff0000ff";
    //private static Color defaultFillColor =  OMGraphic.clear;
    //Color lineColor = ColorFactory.parseColor(defaultLineColorString, true);
    //Color selectLineColor = ColorFactory.parseColor(defaultSelectLineColorString, true);
    Color activeLineColor = ColorFactory.parseColor(defaultActiveLineColorString, true);

    private static final String LINE_DASH_PROPERTY = "dashPattern";

    private static final String LINE_WIDTH_PROPERTY = "lineWidth";
    //private static final int DEFAULT_LINE_WIDTH = 1;
    //int lineWidth = DEFAULT_LINE_WIDTH;

    public final static String VIEW_NONE_SCALE_PROPERTY = "viewNoneScale";
    //public final static String VIEW_NAME_SCALE_PROPERTY = "viewNameScale";
    //public final static String FONT_NAME_PROPERTY = "fontName";
    //public final static String FONT_STYLE_PROPERTY = "fontStyle";

    //public final static float DEFAULT_VIEW_NAME_SCALE =  1000000.f;
    //private float viewNameScale = DEFAULT_VIEW_NAME_SCALE;
    public final static float DEFAULT_VIEW_NONE_SCALE =  10000000.f;
    private float viewNoneScale = DEFAULT_VIEW_NONE_SCALE;

    //public final static String DEFAULT_FONT_NAME = "Arial Narrow";
    //public final static String DEFAULT_FONT_STYLE = "PLAIN";
    //private static int defaultFontSize = 10;
    //private int minScaleFontSize = defaultFontSize;
    //private int maxScaleFontSize = minScaleFontSize;

    //private String fontName = DEFAULT_FONT_NAME;
    //private String fontStyle = DEFAULT_FONT_STYLE;
    //private int iStyle = Font.PLAIN;

    private static boolean GAZ_DEBUG = false; // test

    public GazRegionLayer() {
        super();
        //setProjectionChangePolicy(new com.bbn.openmap.layer.policy.ListResetPCPolicy(this));
        defaultGraphicAttributes.setRenderType(OMGraphic.RENDERTYPE_LATLON);
        defaultGraphicAttributes.setLineType(OMGraphic.LINETYPE_GREATCIRCLE);
        setList(new OMGraphicList());
        setAddToBeanContext(true);
        GAZ_DEBUG = Debug.debugging("gaz");
    }

    /** * DrawingToolRequestor method.  */
    public void drawingComplete(OMGraphic omg, OMAction action) {
        if (GAZ_DEBUG) {
            String cname = (omg == null) ? "null" : omg.getClass().getName();
            int lastPeriod = cname.lastIndexOf('.');
            if (lastPeriod != -1) {
                cname = cname.substring(lastPeriod + 1);
            }
            Debug.output("GazRegionLayer: drawingComplete for omg: " + cname + " > " + action);
        }
        // First thing, release DrawingTool proxy MapMouseMode, if there is one.
        //releaseProxyMouseMode();  // NOTE: uncomment line if this is a subclass of DrawingToolLayer

        // GRP, assuming that selection is off.
        OMGraphicList omgl = new OMGraphicList();
        if (omg != null) omgl.add(omg);
        deselect(omgl);

        OMGraphicList list = getList();
        if (list != null) {
            if (list.size() == 0 && omg != null) { // new list, empty
                if (GAZ_DEBUG) System.out.println("DEBUG GazRegionLayer: new list");
                doAction(omg, action);
            }
            else { // have previous graphic in layerlist
              OMGraphic omgOld = list.getOMGraphicAt(0);
              if (omgOld instanceof OMGraphicList) { // assume POLYLIST 
                if (GAZ_DEBUG) System.out.println("DEBUG GazRegionLayer: have old polylist");
                if (omgOld != omg && (omg instanceof OMGraphicList)) { // not the same, so remove old, so we only have 1 
                  if (GAZ_DEBUG) Debug.output("GazRegionLayer OMGraphicList doAction(omgOld, DELETE_GRAPHIC_MASK)");
                  if (GAZ_DEBUG) System.out.println("DEBUG GazRegionLayer: delete old polylist");
                  doAction(omgOld, new OMAction(OMGraphicConstants.DELETE_GRAPHIC_MASK));
                  if (omg != null) {
                      if (GAZ_DEBUG) System.out.println("DEBUG GazRegionLayer: add new polylist:" + action);
                      doAction(omg, action);
                  }
                }
                else {
                  if (GAZ_DEBUG) System.out.println("DEBUG GazRegionLayer: add polygon to old polylist action:" + action);
                  // This bypasses FilterSupport, otherwise we must implement FS subclass, and create subclass upon init that handles this
                  Object name = omg.getAppObject();
                  if (name == null) {
                      // name the new "region" as a number, the list size
                      name = String.valueOf(((OMGraphicList)omgOld).size());
                      omg.setAppObject(name);
                  }
                  ((OMGraphicList) omgOld).doAction(omg, action);
                 //
                 //if (GAZ_DEBUG)  System.out.println("DEBUG GazRegionLayer: delete old polylist");
                  if (GAZ_DEBUG) Debug.output("GazRegionLayer OMGraphicList doAction(omgOld, DELETE_GRAPHIC_MASK)");
                  doAction(omgOld, new OMAction(OMGraphicConstants.DELETE_GRAPHIC_MASK));
                  if (omg != null) { // assume its a polygon inside old list 
                    if (GAZ_DEBUG) System.out.println("DEBUG GazRegionLayer: add omg:" + action);
                    doAction(omg, action);
                  }
                  //
                }
              }
            }
            doPrepare();

            //repaint();
        } else {
            Debug.error("Layer " + getName() + " received graphic: " + omg + ", and action: " + action +
                    ", with no list ready");
        }
        //querySave = true;
    }

    @Override
    public OMGraphicList getList() {

        OMGraphicList omgl = super.getList();

        Projection p = getProjection(); // null, if projection not yet defined 
        boolean tf = ! (p == null || p.getScale() > viewNoneScale);
        if (GAZ_DEBUG) System.out.println("DEBUG GazRegionLayer getList() omgl.setVisible("+tf+")");

        if (omgl != null) omgl.setVisible(tf);

        /*
        //System.out.println("DEBUG GazRegionLayer getList size: " +
               ((omgl == null) ? "NULL" :  String.valueOf(omgl.size())));
        if (omgl != null && omgl.size() > 0) {
            OMPoly omp = (OMPoly) omgl.getOMGraphicAt(0);
            StringBuffer sb = new StringBuffer(256);
            sb.append(omp.toString());
            float [] ll = omp.getLatLonArray();
            sb.append(" Pts: " + ll.length);
            for (int ii = 0; ii < ll.length; ii++) {
              sb.append(" ").append(ll[ii]);
            }
            sb.append(" RenType: " + omp.getRenderType());
            sb.append(" Width: " + ((BasicStroke)omp.getStroke()).getLineWidth());
            sb.append(" Color: " + omp.getLineColor().toString());
            //System.out.println(sb.toString());

        } 
        */
        return omgl;
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        String name = evt.getPropertyName();
        if (GAZ_DEBUG) Debug.output(getClass().getName() + " DEBUG propertyChange name = " + name); 
        if (evt.getSource() instanceof JiggleMapBean) {
          //
          if ( name.equals(Jiggle.NEW_VMODEL_PROPNAME) ) {
              if (GAZ_DEBUG)
                Debug.output(getClass().getName() + " DEBUG propertyChange new model = " + evt.getNewValue() + " old model: " + evt.getOldValue());

              if (jiggle != null) setGraphicsFromProperties(jiggle.getProperties());
          }
        }
    }

    protected void setRegionNamesPropertyName(String propName) {
        regionNamesProperty = propName;
    }

    protected void setGraphicsFromProperties(JiggleProperties props) {

        if (props == null) {
            if (GAZ_DEBUG) Debug.output("DEBUG GazRegionLayer setGraphicsFromProperties, input properties is NULL");
            return;
        }
        else if (GAZ_DEBUG) Debug.output("DEBUG GazRegionLayer setGraphicsFromProperties regionNamesProperty: " + regionNamesProperty);


        //if (launcher != null) launcher.resetRegionGraphicButtons();

        //String sprefix = PropUtils.getScopedPropertyPrefix(propertyPrefix);


        regionNames = PropUtils.stringArrayFromProperties(props, regionNamesProperty, " ,\t\n\r");

        OMGraphic omg = null;
        if (regionNames != null && regionNames.length > 0) {
              double [] ll = null;
              omg = new OMGraphicList();
              OMGraphic omg2 = null;
              for (int idx=0; idx < regionNames.length; idx++) {
                if (GAZ_DEBUG) Debug.output("DEBUG GazRegionLayer regionName["+idx+"]=\""+regionNames[idx]+"\"");
                omg2 = null;
                ll = closeArray(SolutionTN.getRegionByName(DataSource.getConnection(), regionNames[idx]));
                if (ll != null) {
                  omg2 = new OMPoly(ll, OMGraphicConstants.DECIMAL_DEGREES, OMGraphicConstants.LINETYPE_GREATCIRCLE);
                  if (omg2 != null) {
                      omg2.setAppObject(regionNames[idx]);
                      defaultGraphicAttributes.setTo(omg2); // sets poly 
                      if (jiggle != null) {
                        String modelname = // which alternative ?
                            props.getVelocityModelList().getSelectedModel().getName();
                            //props.getDefaultVelocityModelName();  // alternative ?
                            //TravelTime.getInstance().getModel().getName();  // alternative ?
                        if ( regionNames[idx].equals(modelname)) {
                            omg2.setLinePaint(activeLineColor);
                        }
                      }

                      if (GAZ_DEBUG) Debug.output("DEBUG GazRegionLayer graphic added for region: "+regionNames[idx]);

                      ((OMGraphicList)omg).add(omg2);
                  }
                }
              }
        }
        else {
            if (GAZ_DEBUG) Debug.output("DEBUG GazRegionLayer setGraphicsFromProperties regionNames NULL or empty");
            omg = new OMGraphicList(); // try this as empty list to see what happens re filter
        }

        drawingComplete(omg, new OMAction(OMGraphicConstants.ADD_GRAPHIC_MASK));
    }

    private double [] closeArray(double [] ll) {
        if (ll == null) return null;

        int size  = ll.length;
        if (size < 3) {
            Debug.error("ERROR Map GazRegionLayer input regionPolygon has fewer than 3 vertices, check data!");
            return null;
        } 
        int lastIdx = size-1;
        // Check to see if first and last point are close, if not create new array and adding 1st pt to end.
        final double LATLON_RESOLUTION = .00001;
        if ( (Math.abs(ll[0] - ll[lastIdx-1]) > LATLON_RESOLUTION) ||
             (Math.abs(ll[1]-ll[lastIdx]) > LATLON_RESOLUTION) ) {
            double [] ll_new = new double[size+2];
            System.arraycopy(ll, 0, ll_new, 0, size);
            ll_new[size] = ll[0];
            ll_new[size+1] = ll[1];
            ll = ll_new;
        }
        return ll;
    }

    @Override
    public void findAndInit(Object someObj) {
        if (someObj instanceof JiggleMapBean) {
            JiggleMapBean mapBean = (JiggleMapBean) someObj;
            mapBean.addPropertyChangeListener(this);
            this.jiggle = mapBean.jiggle;
            if (this.jiggle != null) setGraphicsFromProperties(jiggle.getProperties());
        }
        /*
        else if (someObj instanceof RegionFilterLauncher) {
            this.launcher = (RegionFilterLauncher) someObj;
            defaultGraphicAttributes = launcher.getDefaultGraphicAttributes();
            if (this.jiggle != null) setGraphicsFromProperties(jiggle.getProperties());
        }
        */
        super.findAndInit(someObj);
    }

    /**
     * BeanContextMembershipListener method. Called when a new object
     * is removed from the BeanContext of this object.
     */
    @Override
    public void findAndUndo(Object someObj) {
        if (someObj instanceof JiggleMapBean) {
            JiggleMapBean mapBean = (JiggleMapBean) someObj;
            mapBean.removePropertyChangeListener(this);
            setList(null);
            this.jiggle = null;
        }
        /*
        if (this.launcher == someObj) {
            this.launcher = null;
        }
        */
        super.findAndUndo(someObj);
    }

    // Appropriate code is not yet implemented in this method
    protected void setPropertiesFromGraphics() {

        if (jiggle == null) {
            if (GAZ_DEBUG) Debug.output("DEBUG GazRegionLayer Unable to setPropertiesFromGraphics jiggle == null");
            return;
        }
        else if (GAZ_DEBUG) Debug.output("DEBUG GazRegionLayer setPropertiesFromGraphics");

        OMGraphicList omgList = getList();
       //if (GAZ_DEBUG)  System.out.println("DEBUG GazRegionLayer setPropertiesFromGraphics size: " + omgList.size());

        JiggleProperties props = jiggle.getProperties();
        if (omgList == null || omgList.size() == 0) {
            if (GAZ_DEBUG) Debug.output("GazRegionLayer has no region graphic to set Jiggle properties!");
            return;
        }
        else {
          if (omgList.size() > 1) {
            Debug.error("WARNING: GazRegionLayer has more than one graphic in list");
          }
          OMGraphic omg = omgList.getOMGraphicAt(0);
          //omg.generate(getProjection()); // ??

          if (GAZ_DEBUG)
              Debug.output("DEBUG GazRegionLayer graphic: " + omgList.getOMGraphicAt(0).getClass().getName());

          if (omg instanceof OMGraphicList) {
            OMGraphicList ompList = (OMGraphicList) omg;
            if (GAZ_DEBUG)
                 Debug.output("DEBUG GazRegionLayer OMGraphicList ompList.size() : " + ompList.size());

            OMPoly omp = null;
            String name = null;
            StringList regionNameList = new StringList(); // props.getStringList(regionNamesProperty);

            for (int idx=0; idx < ompList.size(); idx++) {
                omp = (OMPoly) ompList.getOMGraphicAt(idx);
                name = (String) omp.getAppObject();
                //
                double [] latlon = omp.getLatLonArray(); // radians
                StringBuffer sb = new StringBuffer(256);
                for (int ii = 0; ii < latlon.length; ii ++) {
                    sb.append(String.valueOf(ProjMath.radToDeg(latlon[ii]))).append(" ");
                }
                if (GAZ_DEBUG) 
                    Debug.output("DEBUG GazRegionLayer setPropertiesFromGraphics: " +name+ "= " + sb.toString());
                //
                //props.setNamedRegionPolygon(name, sb.toString());
                //
                if (! regionNameList.contains(name) ) regionNameList.add(name);
            }
            // props.setProperty(regionNamesProperty, regionNameList.toArray()); // REMOVED for testing

          }
          /* REMOVED for testing
          if (GAZ_DEBUG) {
              Debug.output("DEBUG GazRegionLayer jiggle.eventProps after update ...");
              props.dumpProperties(); 
              Debug.output("DEBUG GazRegionLayer end of properties dump");
          }
          */
        }    

        /* REMOVED for testing
        if (querySave) {
            int yn = JOptionPane.showConfirmDialog(
                       null, "Save edited region properties to file?",
                       "Save Region Filter Properties",
                       JOptionPane.YES_NO_OPTION);

            if (yn == JOptionPane.YES_OPTION) {
                String str = JOptionPane.showInputDialog("Save to filename: ", props.getFilename());
                if (str != null && str.length() > 0) {
                    props.setFilename(str);
                    props.saveProperties();
                    querySave = false;
                }
            }
        }
        */

    }

    // Called externally e.g. Drawing editor dialog tool button action
    // Only if modifying and saving altered list of names and/or their coordinates
    //protected void resetJiggleProperties() {
    //    setPropertiesFromGraphics(); // would need to change jiggle properties and db ?
    //    //jiggle.? e.g. invoke a scan of velocity models for loaded event ? 
    //}

    /**
     * Query for what text should be placed over the information bar
     * when the mouse is over a particular OMGraphic.
     */
    @Override
    public String getInfoText(OMGraphic omg) {
        //DrawingTool dt = getDrawingTool();
        //if (dt != null && dt.canEdit(omg.getClass())) {
        //    return "Click to edit " + omg.getAppObject().toString();
        //} else {
            return omg.getAppObject().toString();
        //}
    }

    /**
     * Query for what text should be placed over the information bar
     * when the mouse is over a particular OMGraphic.
     */
    @Override
    public String getToolTipTextFor(OMGraphic omg) {
        //System.out.println("GazRegionLayer getToolTipTextFor(omg) : " + omg.getAppObject().toString());
        //DrawingTool dt = getDrawingTool();
        //if (dt != null && dt.canEdit(omg.getClass())) {
        //    return "Click to edit " + omg.getAppObject().toString();
        //} else {
            return omg.getAppObject().toString();
        //}
    }

    @Override
    public void setProperties(String prefix, Properties props) {

        super.setProperties(prefix, props);

        // Use props to set default lineColor, width, and type of GraphicsAttributes
        // Note: input properties prefix can't end in "."

        if (GAZ_DEBUG) Debug.output("DEBUG GazRegionLayer setProperties prefix:\""+prefix+"\" props.size():" + props.size() );

        defaultGraphicAttributes.setProperties(prefix, props);

        String sprefix = PropUtils.getScopedPropertyPrefix(prefix);

        // Reset RENDERTYPE and LINETYPE if not defined in input properties,
        // else GraphicAttributes.setProperties above resets them and OMPoly won't render
        if (props.getProperty(sprefix+defaultGraphicAttributes.renderTypeProperty) == null) 
            defaultGraphicAttributes.setRenderType(OMGraphic.RENDERTYPE_LATLON);
        if (props.getProperty(sprefix+defaultGraphicAttributes.lineTypeProperty) == null) 
            defaultGraphicAttributes.setLineType(OMGraphic.LINETYPE_GREATCIRCLE);

        regionType = props.getProperty(sprefix + REGION_TYPE_PROPERTY, DEFAULT_REGION_TYPE);

        // cf. DrawingAttributes lineWidth
        //lineWidth = PropUtils.intFromProperties(props, sprefix + LINE_WIDTH_PROPERTY, DEFAULT_LINE_WIDTH); // is this covered already by Stroke ??
        // cf. DrawingAttributes lineColor
        //lineColor = (Color) PropUtils.parseColorFromProperties(props, sprefix + LINE_COLOR_PROPERTY, defaultLineColorString);
        // cf. DrawingAttributes selectColor
        //selectLineColor = (Color) PropUtils.parseColorFromProperties(props, sprefix + SELECT_LINE_COLOR_PROPERTY, defaultSelectLineColorString);

        activeLineColor = (Color) PropUtils.parseColorFromProperties(props, sprefix + ACTIVE_LINE_COLOR_PROPERTY, defaultActiveLineColorString);

        viewNoneScale = PropUtils.floatFromProperties(props, sprefix + VIEW_NONE_SCALE_PROPERTY, DEFAULT_VIEW_NONE_SCALE);
        //viewNameScale = PropUtils.floatFromProperties(props, sprefix + VIEW_NAME_SCALE_PROPERTY, DEFAULT_VIEW_NAME_SCALE);

        //fontName = props.getProperty(sprefix+FONT_NAME_PROPERTY, DEFAULT_FONT_NAME);
        //fontStyle = props.getProperty(sprefix+FONT_STYLE_PROPERTY, DEFAULT_FONT_STYLE);
        //if (fontStyle.equalsIgnoreCase("PLAIN")) iStyle = Font.PLAIN;
        //else if (fontStyle.equalsIgnoreCase("BOLD")) iStyle = Font.BOLD;
        //else if (fontStyle.equalsIgnoreCase("ITALIC")) iStyle = Font.ITALIC;

        //minScaleFontSize = PropUtils.intFromProperties(props, sprefix+"minFontSize", defaultFontSize);
        //maxScaleFontSize = PropUtils.intFromProperties(props, sprefix+"maxFontSize", defaultFontSize);

        regionNamesProperty = props.getProperty(sprefix+"regionNamesProperty", DEFAULT_REGION_NAMES_PROPERTY);

        if (regionNamesProperty != null) {
            if (GAZ_DEBUG) Debug.output("DEBUG GazRegionLayer setProperties \"regionNamesProperty\" is NULL");
        }
        regionNames = PropUtils.stringArrayFromProperties(props, sprefix+regionNamesProperty, " ,\t\n\r");

        if (GAZ_DEBUG) {
            System.out.println("DEBUG GazRegionLayer setProperties regionNamesProperty "+
                    regionNamesProperty +"="+ JiggleProperties.toPropertyString(regionNames));

        }
    }

    /**
     * Get the associated properties object.
     */
    @Override
    public Properties getProperties(Properties props) {
        return getProperties(propertyPrefix, props);
    }

    /**
     * Get the associated properties object. This method creates a
     * Properties object if necessary and fills it with the relevant
     * data for this layer.
     */
    public Properties getProperties(String prefix, Properties props) {

        props = super.getProperties(props);

        //
        // get defaultGraphicAttributes here  or at end ?
        // Render and Line Type ?
        //

        String sprefix = PropUtils.getScopedPropertyPrefix(prefix);

        props.put(sprefix + REGION_TYPE_PROPERTY, regionType);

        // Instead of below use values from DrawingAttributes
        //props.put(sprefix + LINE_WIDTH_PROPERTY, String.valueOf(lineWidth)); // is this covered already by Stroke ?

        String colorString = null;

        //colorString = (lineColor == null) ?  defaultLineColorString : Integer.toHexString(lineColor.getRGB());
        //props.put(sprefix + LINE_COLOR_PROPERTY, colorString);

        //colorString = (selectLineColor == null) ?  defaultSelectLineColorString : Integer.toHexString(selectLineColor.getRGB());
        //props.put(sprefix + SELECT_LINE_COLOR_PROPERTY, colorString);

        colorString = (activeLineColor == null) ?  defaultActiveLineColorString : Integer.toHexString(activeLineColor.getRGB());
        props.put(sprefix + ACTIVE_LINE_COLOR_PROPERTY, colorString);

        props.put(sprefix + VIEW_NONE_SCALE_PROPERTY, String.valueOf(viewNoneScale));
        //props.put(sprefix + VIEW_NAME_SCALE_PROPERTY, String.valueOf(viewNameScale));
        
        //props.put(sprefix+FONT_NAME_PROPERTY, fontName);
        //props.put(sprefix+FONT_STYLE_PROPERTY, fontStyle);

        props.put(sprefix+REGION_TYPE_PROPERTY, regionType);

        //props.put(sprefix+regionNamesProperty, JiggleProperties.toPropertyString(regionNames));
        props.put(sprefix+REGION_NAMES_PROPERTY, regionNamesProperty);

        //instead of these:
        //props.put(sprefix+defaultGraphicAttributes.renderTypeProperty, defaultGraphicAttributes.getRenderType());
        //props.put(sprefix+defaultGraphicAttributes.lineTypeProperty, defaultGraphicAttributes.getLineType());
        // etc.
        // do this:
        defaultGraphicAttributes.setPropertyPrefix(prefix); // NOTE not scoped here, no ending "."
        defaultGraphicAttributes.getProperties(props);

        return props;
    }

    /**
     * Supplies the propertiesInfo object associated with this
     * GazRegionLayer object. Contains the human readable
     * descriptions of the properties and the
     * <code>initPropertiesProperty</code> (see Inspector class.)
     */
    @Override
    public Properties getPropertyInfo(Properties info) {
    /*
     Simple space saving implementation of common I18n Property Info setting.
     returns Properties object passed in, or new one if null Properties passed in:
     info PropUtils.setI18NPropertyInfo(
                                         I18n i18n,           // i18n object to use to search for internationalized strings.
                                         Properties info,     // the properties class being used to set information into.
                                         Class classToSetFor, // class to use for i18n search.
                                         String propertyName, // property to set for.
                                         String label,        // label to use for GUI (can be null if N/A).
                                         String tooltip,      // tooltip to use for GUI (can be null if N/A).
                                         String editor        // editor class string to use for GUI (can be null if N/A).
                                        ); 
            if (tooltip != null) {
                String internString = i18n.get(classToSetFor, propertyName, I18n.TOOLTIP, tooltip);
                info.put(propertyName, internString);
            }
            if (label != null) {
                String internString = i18n.get(classToSetFor, propertyName, label); // See method descr below (defaults to "TEXT" for type param)
                info.put(propertyName + PropertyConsumer.LabelEditorProperty, internString);
            }
            if (editor != null) info.put(propertyName + PropertyConsumer.ScopedEditorProperty, editor);

        * Get the string associated with the given object/field/type.
        * where requestor is class of containing the code requesting the (typically some.class)
        * field (e.g. property name)
        * slot in the Swing object the string pertains to:  TEXT, TOOLTIP or MNEMONIC.
        * defaultString what to use if the resource can't be found
          iString  = I18n.get(Class requestor, String field, int type, String defaultString);
     */

        info = super.getPropertyInfo(info);

        I18n i18n = Environment.getI18n();

        //String dummyMarker = PropUtils.getDummyMarkerForPropertyInfo(getPropertyPrefix(), null);

        info.put(initPropertiesProperty, 
                  REGION_NAMES_PROPERTY + " " +
                  REGION_TYPE_PROPERTY + " " +
                  // dummyMarker + " " +
                  LINE_WIDTH_PROPERTY + " " +
                  LINE_COLOR_PROPERTY + " " +
                  LINE_DASH_PROPERTY + " " +
                  SELECT_LINE_COLOR_PROPERTY + " " +
                  ACTIVE_LINE_COLOR_PROPERTY + " " +
                  //FONT_NAME_PROPERTY + " " +
                  //FONT_STYLE_PROPERTY + " " +
                  //VIEW_NAME_SCALE_PROPERTY + " " +
                  VIEW_NONE_SCALE_PROPERTY 
        );

        String iString = null;
        
        // Jiggle property listing names of regions to plot
        iString = i18n.get(GazRegionLayer.class, REGION_NAMES_PROPERTY, i18n.TOOLTIP, "Property in user's Jiggle properties (not Openmap)");
        info.put(REGION_NAMES_PROPERTY, iString);
        iString = i18n.get(GazRegionLayer.class, REGION_NAMES_PROPERTY, "Jiggle property listing region names");
        info.put(REGION_NAMES_PROPERTY + LabelEditorProperty, iString);

        // For each named property can info.put up to 3 attributes: tooltip, label, editor
        iString = i18n.get(GazRegionLayer.class, REGION_TYPE_PROPERTY, i18n.TOOLTIP, "Region type");
        info.put(REGION_TYPE_PROPERTY, iString); // tooltip
        // label defaults to property name
        info.put(REGION_TYPE_PROPERTY+ ScopedEditorProperty,
                "com.bbn.openmap.util.propertyEditor.NonEditablePropertyEditor");

        /*
        info = PropUtils.setI18NPropertyInfo(i18n, info, GazRegionLayer.class, dummyMarker,
                "Rendering Attributes", "Attributes that determine how the shapes will be drawn.",
                "com.bbn.openmap.omGraphics.DrawingAttributesPropertyEditor");

        */

        info.put(LINE_WIDTH_PROPERTY, "Region border width (pixels)"); // tooltip
        // label defaults to property name
        // text editor

        info.put(LINE_DASH_PROPERTY, "Region border dash pattern template (repeated on off pixel interval (e.g. 5 5)"); // tooltip
        // label defaults to property name
        // text editor

        iString = i18n.get(GazRegionLayer.class, LINE_COLOR_PROPERTY, i18n.TOOLTIP,
                "Region border Color (inactive)");
        info.put(LINE_COLOR_PROPERTY, iString);
        iString = i18n.get(GazRegionLayer.class, LINE_COLOR_PROPERTY, "Region lineColor");
        info.put(LINE_COLOR_PROPERTY + LabelEditorProperty, iString);
        info.put(LINE_COLOR_PROPERTY + ScopedEditorProperty,
                "com.bbn.openmap.util.propertyEditor.ColorPropertyEditor");

        iString = i18n.get(GazRegionLayer.class,
                SELECT_LINE_COLOR_PROPERTY,
                i18n.TOOLTIP,
                "Region Border Color upon mouseover");
        info.put(SELECT_LINE_COLOR_PROPERTY, iString);
        iString = i18n.get(GazRegionLayer.class, SELECT_LINE_COLOR_PROPERTY, "Region selectColor");
        info.put(SELECT_LINE_COLOR_PROPERTY + LabelEditorProperty, iString);
        info.put(SELECT_LINE_COLOR_PROPERTY + ScopedEditorProperty,
                "com.bbn.openmap.util.propertyEditor.ColorPropertyEditor");
        //

        iString = i18n.get(GazRegionLayer.class,
                ACTIVE_LINE_COLOR_PROPERTY,
                i18n.TOOLTIP,
                "Region Border Color if active");
        info.put(ACTIVE_LINE_COLOR_PROPERTY, iString);
        iString = i18n.get(GazRegionLayer.class, ACTIVE_LINE_COLOR_PROPERTY, "Region activeColor");
        info.put(ACTIVE_LINE_COLOR_PROPERTY + LabelEditorProperty, iString);
        info.put(ACTIVE_LINE_COLOR_PROPERTY + ScopedEditorProperty,
                "com.bbn.openmap.util.propertyEditor.ColorPropertyEditor");

        info.put(VIEW_NONE_SCALE_PROPERTY, "Max zoom scale for showing regions.");
        // label defaults to property name, text editor

        //info.put(VIEW_NAME_SCALE_PROPERTY, "Max zoom scale showing polygon labels.");
        //info.put(FONT_NAME_PROPERTY, "Region label font name");
        //info.put(FONT_STYLE_PROPERTY, "Region label font style (PLAIN, BOLD, ITALIC)");

        return info;
    }

    /** The user interface palette for layer. */
    protected Box palette = null;

    /**
     * Gets the gui controls associated with the layer.
     * 
     * @return Component
     */

    /** Creates the interface palette. */
    @Override
    public java.awt.Component getGUI() {
        //
        // regionNamesProperty ??
        //
        // REGION_TYPE_PROPERTY="regionGraphicType"
        //
        // LINE_WIDTH_PROPERTY = "lineWidth";
        // LINE_COLOR_PROPERTY = "lineColor";
        // SELECT_LINE_COLOR_PROPERTY = "selectColor";
        // ACTIVE_LINE_COLOR_PROPERTY = "activeColor";
        // VIEW_NONE_SCALE_PROPERTY = "viewNoneScale";
        //
        // disabled:
        // VIEW_NAME_SCALE_PROPERTY = "viewNameScale";
        // FONT_NAME_PROPERTY = "fontName";
        // FONT_STYLE_PROPERTY = "fontStyle";

        if (palette == null) {
            if (com.bbn.openmap.util.Debug.debugging("gaz"))
                System.out.println("GazRegionLayer: creating GUI Property Palette.");

            palette = Box.createVerticalBox();

            JPanel layerPanel = PaletteHelper.createPaletteJPanel(i18n.get(GazRegionLayer.class,
                    "layerPanel", "Gazetteer Region Layer"));

            ActionListener al = new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                  GazRegionLayer.this.actionPerformed(e);
                  String command = e.getActionCommand();
                  if (command == RedrawCmd) {
                      //System.out.println("DEBUG GazRegionLayer REDRAW lineColor: " + lineColor.toString());
                      if (getList() != null) getList().clear(); // clear old graphics list
                      //setList(null); // blow it away
                      if (isVisible()) {
                          setGraphicsFromProperties(jiggle.getProperties());
                          GazRegionLayer.this.doPrepare();
                      }
                  }
                }
            };

            JPanel subbox = new JPanel(new GridLayout(0, 1));

            JButton setProperties = new JButton(i18n.get(GazRegionLayer.class,
                    "setProperties", "Preferences"));
            setProperties.setActionCommand(DisplayPropertiesCmd);
            setProperties.addActionListener(al);
            subbox.add(setProperties);

            JButton redraw = new JButton(i18n.get(GazRegionLayer.class,
                    "redraw", "Redraw Region Layer"));
            redraw.setActionCommand(RedrawCmd);
            redraw.addActionListener(al);
            subbox.add(redraw);
            layerPanel.add(subbox);

            palette.add(layerPanel);
        }
        return palette;
    }

}
