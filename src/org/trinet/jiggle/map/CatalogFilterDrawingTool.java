package org.trinet.jiggle.map;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;

import com.bbn.openmap.omGraphics.EditableOMGraphic;
import com.bbn.openmap.omGraphics.OMAction;
import com.bbn.openmap.omGraphics.OMGraphic;
//import com.bbn.openmap.omGraphics.OMGraphicConstants;
import com.bbn.openmap.tools.drawing.OMDrawingTool;
import com.bbn.openmap.util.Debug;

import org.trinet.jasi.EventSelectionProperties;
import org.trinet.util.StringList;
import org.trinet.util.DoubleRange;

public class CatalogFilterDrawingTool extends OMDrawingTool {

    CatalogFilterLayer catalogFilterLayer = null;

    public CatalogFilterDrawingTool() {
        super();
    }

    @Override
    public JPopupMenu createPopupMenu() {
        JPopupMenu pum = new JPopupMenu();

        JMenuItem jmi = new JMenuItem("Change Appearance");
        jmi.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                EditableOMGraphic eomg = getCurrentEditable();
                if (eomg != null) {
                    boolean previous = eomg.getShowGUI();
                    eomg.setShowGUI(true);
                    setVisible(true);
                    if (!getUseAsTool()) {
                        showInWindow();
                    }
                    eomg.setShowGUI(previous);
                    eomg.getStateMachine().setSelected();
                }
            }
        });

        //      JMenuItem reset = new JMenuItem("Undo Changes");
        //      reset.setEnabled(false);
        //      reset.addActionListener(new ActionListener() {
        //              public void actionPerformed(ActionEvent ae) {
        //                  if (currentEditable != null) {
        //                      currentEditable.reset();
        //                  }
        //              }
        //          });
        //      pum.add(reset);

        if (isMask(SHOW_GUI_BEHAVIOR_MASK | GUI_VIA_POPUP_BEHAVIOR_MASK)
                && !getUseAsTool()) {
            pum.add(jmi);
        } else {
            Debug.output("Not adding Change Appearance to popup: guiViaPopup("
                    + isMask(SHOW_GUI_BEHAVIOR_MASK) + ") isTool("
                    + getUseAsTool() + ")");
        }

        // Menu item below added -aww 2008/04/24
        jmi = new JMenuItem("Change name");
        jmi.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                EditableOMGraphic eomg = getCurrentEditable();
                if (eomg != null) {
                    OMGraphic g = eomg.getGraphic();
                    if (g != null) {
                        String name = (String) g.getAppObject();
                        name = JOptionPane.showInputDialog("Change region name ", name);
                        if (name != null && name.length() > 0) {
                            g.setAppObject(name);
                            notifyListener(g, new OMAction(OMGraphic.UPDATE_GRAPHIC_MASK));
                        }
                    }
                }

                if (isActivated()) {
                    deactivate(); // ??
                }

            }
        });
        pum.add(jmi);

        jmi = new JMenuItem("Delete");
        jmi.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                OMAction action = new OMAction();
                action.setMask(OMGraphic.DELETE_GRAPHIC_MASK);
                EditableOMGraphic eomg = getCurrentEditable();
                if (eomg != null) {
                    OMGraphic g = eomg.getGraphic();
                    if (g != null) {
                        notifyListener(g, action);
                    }
                }
                setCurrentEditable(null);
                deactivate();
                // Added option to save properties and a reset of catalog view
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        if (catalogFilterLayer != null) catalogFilterLayer.resetCatalog();
                    }
                });
            }
        });
        pum.add(jmi);

        jmi = new JMenuItem("Filter Catalog");
        jmi.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                if (isActivated()) {
                    deactivate(); // ??
                }

                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        if (catalogFilterLayer != null) catalogFilterLayer.resetCatalog();
                    }
                });
            }
        });
        pum.add(jmi);

        jmi = new JMenuItem("Set event selection region property");
        jmi.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                if (isActivated()) {
                    deactivate(); // ??
                }
                if (catalogFilterLayer != null) catalogFilterLayer.setPropertiesFromGraphics();
            }
        });
        pum.add(jmi);

        if (catalogFilterLayer != null) {
          if ( EventSelectionProperties.REGION_POLYLIST.equals(catalogFilterLayer.jiggle.getEventProperties().getRegionType()) ) {
            jmi = new JMenuItem("Set mag value range ...");
            jmi.addActionListener(new ActionListener() {
              public void actionPerformed(ActionEvent ae) {
                EditableOMGraphic eomg = getCurrentEditable();
                if (eomg != null) {
                    OMGraphic g = eomg.getGraphic();
                    if (g != null) {
                      String name = (String) g.getAppObject();
                      if (name != null && name.length() > 0) {
                        String magValues = JOptionPane.showInputDialog("Enter "+name+ " magnitude range (min max) ", "-1.0 9.0");
                        if (magValues != null && magValues.length() > 0) {
                          String [] list = new StringList(magValues).toArray();
                          if (list.length == 2) {
                            DoubleRange dr = new DoubleRange(Double.parseDouble(list[0]),Double.parseDouble(list[1]));
                            //System.out.println(" CFDT debug " + name + " " + dr);
                            catalogFilterLayer.jiggle.getEventProperties().setNamedRegionMagValueRange(name, dr);
                          }
                        }
                      }
                    }
                }

                if (isActivated()) {
                    deactivate(); // ??
                }
              }

            });
            pum.add(jmi);

            jmi = new JMenuItem("Set depth range ...");
            jmi.addActionListener(new ActionListener() {
              public void actionPerformed(ActionEvent ae) {
                EditableOMGraphic eomg = getCurrentEditable();
                if (eomg != null) {
                    OMGraphic g = eomg.getGraphic();
                    if (g != null) {
                      String name = (String) g.getAppObject();
                      if (name != null && name.length() > 0) {
                        String magValues = JOptionPane.showInputDialog("Enter "+name+ " depth range (min max) ", "-9.0 999.0");
                        if (magValues != null && magValues.length() > 0) {
                          String [] list = new StringList(magValues).toArray();
                          if (list.length == 2) {
                            DoubleRange dr = new DoubleRange(Double.parseDouble(list[0]),Double.parseDouble(list[1]));
                            //System.out.println(" CFDT debug " + name + " " + dr);
                            catalogFilterLayer.jiggle.getEventProperties().setNamedRegionOrgDepthRange(name, dr);
                          }
                        }
                      }
                    }
                }

                if (isActivated()) {
                    deactivate(); // ??
                }
              }

            });
            pum.add(jmi);
          }
        }

        return pum;
    }

    @Override
    public void findAndInit(Object someObj) {
        super.findAndInit(someObj);
        if (someObj instanceof CatalogFilterLayer) {
            catalogFilterLayer = (CatalogFilterLayer) someObj;
        }
    }

    @Override
    public void findAndUndo(Object someObj) {
        super.findAndUndo(someObj);
        if (someObj == catalogFilterLayer) {
            catalogFilterLayer = null;
        }
    }
}
