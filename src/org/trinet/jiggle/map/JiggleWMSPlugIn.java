package org.trinet.jiggle.map;

import java.util.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import com.bbn.openmap.omGraphics.OMGraphicList;
import com.bbn.openmap.omGraphics.OMRaster;
import com.bbn.openmap.plugin.wms.WMSPlugIn;
import com.bbn.openmap.image.WMTConstants;
import com.bbn.openmap.proj.Projection;
import com.bbn.openmap.util.Debug;
import com.bbn.openmap.util.PropUtils;

public class JiggleWMSPlugIn extends com.bbn.openmap.plugin.wms.WMSPlugIn {

    // REMEMBER TO ADD PROPERTIES like those below the "initProperties" property value list
    // if you want to edit them in the default gui invoked by the Bean Inspector
    public static final String WMSMinScaleProperty = "min.scale";
    public static final String WMSMaxScaleProperty = "max.scale";
    public static final String WMSTimeoutProperty = "timeout";
    public static final String WMSRefSysProperty = "srscrs"; // 1.1.0 srs 1.3.0 crs

    protected long minScale = 4000;
    protected long maxScale = 40000000;
    protected int timeout = 10000;

    protected static final String DEFAULT_SRS = "EPSG:4326";
    protected String refsys = DEFAULT_SRS; // WGS84 datum
    //For Projection in WMS queries (1.1.1 1.3.0) map options:
    // LLXY or CADRG SRS=EPSG:4326, CRS=CRS:84
    // Mercator      SRS=EPSG:4326, CRS=AUTO2:42001
    // Orthographic  SRS=EPSG:4326, CRS=AUTO2:42003
    // Gnomonic no WMS support


    protected int majorVersion = 1;
    protected int midVersion = 0;
    protected int minorVersion = 0;

    public JiggleWMSPlugIn() {}

    public Properties getPropertyInfo(Properties props) {
        props = super.getPropertyInfo(props);

        props.put(initPropertiesProperty, WMSServerProperty + " "
                + WMSVersionProperty + " " + LayersProperty + " "
                + StylesProperty + " " + VendorSpecificNamesProperty + " "
                + VendorSpecificValuesProperty + " " + ImageFormatProperty + " "
                + TransparentProperty + " " + BackgroundColorProperty + " "
                + WMSMinScaleProperty + " " + WMSMaxScaleProperty + " " + WMSTimeoutProperty + " "
                + WMSRefSysProperty);

        props.put(WMSServerProperty,
                "URL to the server script that responds to WMS map requests");
        props.put(ImageFormatProperty, "Image format (GIF, PNG, JPEG)");
        props.put(TransparentProperty,
                "Flag to indicate that background of image should be tranparent");
        props.put(TransparentProperty + ScopedEditorProperty,
                "org.trinet.jiggle.map.WMSBooleanPropertyEditor");
        props.put(BackgroundColorProperty, "The Background color for the image");
        props.put(BackgroundColorProperty + ScopedEditorProperty,
                "org.trinet.jiggle.map.WMSColorPropertyEditor");
        props.put(WMSVersionProperty, "The WMS specification version");
        props.put(LayersProperty, "A list of layers to use in the query");
        props.put(StylesProperty, "A list of layer styles to use in the query");
        props.put(VendorSpecificNamesProperty,
                "Vendor-specific capability names to use in the query");
        props.put(VendorSpecificValuesProperty,
                "Vendor-specific capability values for the names");

        props.put(WMSMinScaleProperty, "Min zoom scale for image.");
        props.put(WMSMaxScaleProperty, "Max zoom scale for image.");
        props.put(WMSTimeoutProperty, "Timeout ms for server to return requested image rectangle.");
        props.put(WMSRefSysProperty, "CRS/SRS Spatial Reference.");

        return props;
    }

    private class WMSSwingWorker extends org.trinet.util.SwingWorker {

        java.net.HttpURLConnection urlc = null;
        String urlString = null; 

        public Object construct() {

          java.net.URL url = null;
          OMGraphicList list = new OMGraphicList();

          try {
            urlString = createQueryString(currentProjection);
            if (Debug.debugging("plugin")) Debug.output("WebImagePlugIn.getRectangle() with \"" + urlString + "\"");
            if (urlString == null) return list;

            url = new java.net.URL(urlString);
            urlc = (java.net.HttpURLConnection) url.openConnection();

            if (Debug.debugging("plugin")) {
                Debug.output("url content type: " + urlc.getContentType());
            }

            if (urlc == null || urlc.getContentType() == null) {
                if (layer != null) {
                    layer.fireRequestMessage(getName()
                            + ": unable to connect to " + getServerName());
                } else {
                    Debug.error(getName() + ": unable to connect to "
                            + getServerName());
                }
                return list;
            }

            // text
            if (urlc.getContentType().startsWith("text")) {
                java.io.BufferedReader bin = new java.io.BufferedReader(new java.io.InputStreamReader(urlc.getInputStream()));
                String st;
                String message = "";
                while ((st = bin.readLine()) != null) {
                    message += st;
                }

                // Debug.error(message);
                // How about we toss the message out to the user
                // instead?
                if (layer != null) {
                    layer.fireRequestMessage(message);
                }

                // image
            } else if (urlc.getContentType().startsWith("image")) {
                // disconnect and reconnect in ImageIcon is very
                // expensive
                // urlc.disconnect();
                // ImageIcon ii = new ImageIcon(url);

                // this doesn't work always
                // ImageIcon ii = new ImageIcon((Image)
                // urlc.getContent());

                // the best way, no reconnect, but can be an
                // additional 'in memory' image
                InputStream in = urlc.getInputStream();
                // ------- Testing without this
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                int buflen = 2048; // 2k blocks
                byte buf[] = new byte[buflen];
                int len = -1;
                while ((len = in.read(buf, 0, buflen)) != -1) {
                    out.write(buf, 0, len);
                }
                out.flush();
                out.close();
                ImageIcon ii = new ImageIcon(out.toByteArray());

                // -------- To here, replaced by two lines below...

                // DFD - I've seen problems with these lines below handling PNG
                // images, and with some servers with some coverages, like there
                // was something in the image under certain conditions that made
                // it tough to view. So while it might be more memory efficient
                // to do the code below, we'll error on the side of correctness
                // until we figure out what's going on.

                // FileCacheImageInputStream fciis = new
                // FileCacheImageInputStream(in, null);
                // BufferedImage ii = ImageIO.read(fciis);

                OMRaster image = new OMRaster((int) 0, (int) 0, ii);
                list.add(image);
            } // end if image
          } catch (java.net.MalformedURLException murle) {
            murle.printStackTrace();
            Debug.error("WebImagePlugIn: URL \"" + urlString + "\" is malformed.");
          } catch (java.io.IOException ioe) {
            ioe.printStackTrace();
          }
          return list;
        }

    } // end of swingWorker

    @Override
    public OMGraphicList getRectangle(Projection p) {

        if (p.getScale() > maxScale) {
            layer.fireRequestInfoLine("  The scale is too large for viewing.");
            Debug.error("WMSPlugin getRectangle(p): scale (" + p.getScale()
                    + ") is greater than maximum (" + maxScale + ") allowed.");
            return new OMGraphicList();
        }
        if (p.getScale() < minScale) {
            layer.fireRequestInfoLine("  The scale is too small for viewing.");
            Debug.error("WMSPlugin getRectangle(p): scale (" + p.getScale()
                    + ") is smaller than minimum (" + minScale + ") allowed.");
            return new OMGraphicList();
        }

        this.currentProjection = p;

        final Thread spawningThread = Thread.currentThread(); // aka SwingWorker for PluginLayer type created by OMGraphicLayerHandler

        final WMSSwingWorker swingWorker = new WMSSwingWorker();
        swingWorker.start();

        java.util.Timer timer = new java.util.Timer(true);
        timer.schedule(
                new TimerTask() {
                  public void run() {
                      System.out.println("INFO: JiggleWMSplugin timer interrupting map layer loading after : " + timeout + " ms timeout");
                      swingWorker.interrupt(); // worker inside this instance
                      spawningThread.interrupt(); // the OMGraphicHandlerLayer worker
                  }
                },
                (long) timeout
        );

        OMGraphicList list = (OMGraphicList) swingWorker.get();
        timer.cancel();
        timer = null;

        if (list != null) list.generate(p);
        else {
          final String name = getName();
          final String server = getServerName();
          /*
          SwingUtilities.invokeLater(new Runnable() {
              public void run() {
                  JOptionPane.showMessageDialog(null, name + ":\n\nCouldn't connect to " +
                          server, "Connection Problem", JOptionPane.PLAIN_MESSAGE);
              }
          });
          */
          System.out.println("INFO: Map layer " + name + ": couldn't connect to " + server); 
          list = new OMGraphicList();
        }

        return list;

    } 

    /** * Get the minimum scale for when the DTED images will be shown.  */
    public long getMinScale() {
        return minScale;
    }

    public void setMinScale(long scale) {
        if (scale < 1000) {
            Debug.error("WMSLayer: min scale setting unreasonably small (" + scale + "), setting to 1000");
            scale = 1000;
        }
        minScale = scale;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int ms) {
        /*
        if (ms < 5000) {
            Debug.error("WMSLayer: timeout setting unreasonably short (" + ms + "), setting to 5000");
            ms = 5000;
        }
        */
        timeout = ms;
    }

    public long getMaxScale() {
        return maxScale;
    }

    public void setMaxScale(long scale) {
        maxScale = scale;
        if (maxScale <= minScale) {
            Debug.error("WMSLayer: max scale setting < minScale (" + scale + "), setting to minScale");
            maxScale = minScale;
        }
    }

    @Override
    public void setProperties(String prefix, java.util.Properties properties) {
        super.setProperties(prefix, properties);
        prefix = PropUtils.getScopedPropertyPrefix(this);

        setMinScale(PropUtils.longFromProperties(properties, prefix
                + WMSMinScaleProperty, getMinScale()));
        setMaxScale(PropUtils.longFromProperties(properties, prefix
                + WMSMaxScaleProperty, getMaxScale()));
        setTimeout(PropUtils.intFromProperties(properties, prefix
                + WMSTimeoutProperty, getTimeout()));

        refsys = properties.getProperty(prefix + WMSRefSysProperty, DEFAULT_SRS);

        java.util.StringTokenizer st = new java.util.StringTokenizer(wmsVersion, ".");
        majorVersion = Integer.parseInt(st.nextToken());
        midVersion = Integer.parseInt(st.nextToken());
        minorVersion = Integer.parseInt(st.nextToken());

    }

    @Override
    public Properties getProperties(Properties props) {
        props = super.getProperties(props);
        return getProperties(getPropertyPrefix(), props);
    }

    public Properties getProperties(String prefix, Properties props) {
        props = super.getProperties(props);
        prefix = PropUtils.getScopedPropertyPrefix(prefix);
        props.put(prefix + WMSMinScaleProperty, String.valueOf(minScale));
        props.put(prefix + WMSMaxScaleProperty, String.valueOf(maxScale));
        props.put(prefix + WMSTimeoutProperty, String.valueOf(timeout));
        props.put(prefix + WMSRefSysProperty, refsys);
        return props;
    }
    /**
     * Create the query to be sent to the server, based on current
     * settings.
     */
    @Override
    public String createQueryString(Projection p) {

        if (queryHeader == null) {
            return null;
        }

        String bbox = "undefined";
        String height = "undefined";
        String width = "undefined";

        if (p != null) {
            bbox = Double.toString(p.getUpperLeft().getX()) + ","
                    + Double.toString(p.getLowerRight().getY()) + ","
                    + Double.toString(p.getLowerRight().getX()) + ","
                    + Double.toString(p.getUpperLeft().getY());
            height = Integer.toString(p.getHeight());
            width = Integer.toString(p.getWidth());
        }

        StringBuffer buf = new StringBuffer(256);
        buf.append(queryHeader);
        // Add delimiter for start of parameter list
        if (buf.indexOf("?") < 0) buf.append("?");
        // else delimiter to append to end of parameter list
        else if (buf.lastIndexOf("&") < buf.length()-1) buf.append("&");

        buf.append(WMTConstants.VERSION + "=" + wmsVersion + "&");
        buf.append(WMTConstants.REQUEST + "=" + mapRequestName + "&");
        String paramName = (majorVersion > 1 || midVersion >= 3) ? "CRS" : "SRS";
        buf.append(paramName + "=" + refsys + "&");
        buf.append(WMTConstants.BBOX + "=" + bbox + "&");
        buf.append(WMTConstants.HEIGHT + "=" + height + "&");
        buf.append(WMTConstants.WIDTH + "=" + width + "&");
        buf.append(WMTConstants.EXCEPTIONS + "=" + errorHandling);

        if (imageFormat != null) {
            buf.append("&" + FORMAT + "=" + imageFormat);

            String baseImageFormat = imageFormat;
            if (baseImageFormat.indexOf('/') > 0)
                baseImageFormat = baseImageFormat.substring(baseImageFormat.indexOf('/'));
            if (baseImageFormat.equals(WMTConstants.IMAGEFORMAT_JPEG)) {
                buf.append("&quality=" + imageQuality);
            }
        }

        if (transparent != null) {
            buf.append("&" + TRANSPARENT + "=" + transparent);
        }

        if (backgroundColor != null) {
            buf.append("&" + BGCOLOR + "=" + backgroundColor);
        }

        if (layers != null) {
            buf.append("&" + LAYERS + "=" + layers);
        }

        if (styles != null) {
            buf.append("&" + STYLES + "=" + styles);
        }

        /*
         * Included to allow for one or more vendor specific
         * parameters to be specified such as ESRI's ArcIMS's
         * "ServiceName" parameter.
         */
        if (vendorSpecificNames != null) {
            if (vendorSpecificValues != null) {
                StringTokenizer nameTokenizer = new StringTokenizer(vendorSpecificNames, ",");
                StringTokenizer valueTokenizer = new StringTokenizer(vendorSpecificValues, ",");
                paramName = null;
                String paramValue = null;
                while (nameTokenizer.hasMoreTokens()) {
                    try {
                        paramName = nameTokenizer.nextToken();
                        paramValue = valueTokenizer.nextToken();
                        buf.append("&" + paramName + "=" + paramValue);
                    } catch (NoSuchElementException e) {
                        if (Debug.debugging("wms")) {
                            Debug.output("WMSPlugIn.createQueryString(): "
                                    + "parameter \"" + paramName
                                    + "\" has no value");
                        }
                    }
                }
            }
        }

        if (Debug.debugging("wms")) {
            Debug.output("query string: " + buf);
        }

        return buf.toString();
    }

} //end WMSPlugin
