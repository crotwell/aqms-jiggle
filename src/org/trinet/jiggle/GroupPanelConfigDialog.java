package org.trinet.jiggle;

import org.trinet.jasi.DataSource;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

/** Dialog to set attributes of the Scrolled GroupPanel. */
public class GroupPanelConfigDialog extends CenteredDialog {

     JPanel mainPanel = new JPanel();
     BorderLayout borderLayout1 = new BorderLayout();
     JPanel buttonPanel = new JPanel();
     JButton okButton = new JButton();
     JButton cancelButton = new JButton();
     JPanel centerPanel = new JPanel();

     GroupPanelConfigPanel gpcp;

     public GroupPanelConfigDialog(Frame frame, String title, boolean modal) {
          super(frame, title, modal);
          try  {
               jbInit();
               pack();
          }
          catch(Exception ex) {
               ex.printStackTrace();
          }
     }

     public GroupPanelConfigDialog() {
          this(null, "", false);
     }

     public void setChannelCount(int val) {
        gpcp.setChannelCount(val);
     }
     public int getChannelCount() {
        return gpcp.getChannelCount();
     }
     public void setTimeWindow(double sec) {
        gpcp.setTimeWindow(sec);
     }
     public double getTimeWindow() {
        return gpcp.getTimeWindow();
     }

     // Build the GUI: built with JBuilder tool
     void jbInit() throws Exception {
          mainPanel.setLayout(borderLayout1);
          okButton.setText("OK");
          okButton.addActionListener(new java.awt.event.ActionListener() {

               public void actionPerformed(ActionEvent e) {
                    okButton_actionPerformed(e);
               }
          });
          cancelButton.setText("Cancel");
          cancelButton.addActionListener(new java.awt.event.ActionListener() {

               public void actionPerformed(ActionEvent e) {
                    cancelButton_actionPerformed(e);
               }
          });
          getContentPane().add(mainPanel);
          mainPanel.add(buttonPanel, BorderLayout.SOUTH);
          buttonPanel.add(okButton, null);
          buttonPanel.add(cancelButton, null);

          //
          gpcp = new GroupPanelConfigPanel();
          centerPanel.add(gpcp);

          mainPanel.add(centerPanel, BorderLayout.CENTER);
     }

     void okButton_actionPerformed(ActionEvent e) {
        returnValue = JOptionPane.OK_OPTION;
           this.setVisible(false);            // dismiss dialog
     }

     void cancelButton_actionPerformed(ActionEvent e) {
        returnValue = JOptionPane.CANCEL_OPTION;
           this.setVisible(false);            // dismiss dialog
     }

/* Main for testing
    public static void main(String s[]) {

        JFrame frame = new JFrame("Main");
        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {System.exit(0);}
        });

        frame.pack();
        frame.setVisible(true);

     GroupPanelConfigDialog dialog =
                         new GroupPanelConfigDialog(frame, "TEST", true );
     dialog.setChannelCount(10);
     dialog.setTimeWindow(111.0);

     dialog.setVisible(true);

     if (dialog.getButtonStatus() == JOptionPane.OK_OPTION) {

        System.out.println(dialog.getChannelCount() +"  "+
                                                     dialog.getTimeWindow());

     } else {
        System.out.println("Cancelled");
     }

    } // end of main
*/
}




