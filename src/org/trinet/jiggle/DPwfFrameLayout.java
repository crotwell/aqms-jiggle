package org.trinet.jiggle;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.text.*;

import javax.swing.*;
import javax.swing.event.*;

import org.trinet.jasi.PhaseDescription;

/**
 * Panel for insertion into dialog box to edit GUI layout of controls, waveform panels, etc.
 */
public class DPwfFrameLayout extends AbstractPreferencesPanel {

    public DPwfFrameLayout(JiggleProperties props) {

        super(props);

    }

    protected void initGraphics() {

        this.setLayout( new ColumnLayout() );

        DecimalFormat df1 = new DecimalFormat ("###0");
        DecimalFormat df2 = new DecimalFormat ( "#0.00" );

        Box hbox = null;
        Box vbox = null;
        JLabel jlbl = null;
        JTextField jtf = null;
        JCheckBox jcb = null;
        ButtonGroup bg = null;
        JRadioButton jrb = null;
        ActionListener al = null;

// group waveform panel
        vbox = Box.createVerticalBox();
        vbox.setBorder(BorderFactory.createTitledBorder("Waveform Panel Frame Layout"));

        // group waveform panel split
        hbox = Box.createHorizontalBox();
        al = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String cmd = e.getActionCommand();
                if (cmd == "Split Pane") {
                    newProps.setProperty("waveformInTab", false);
                }
                else if (cmd == "Tab Pane") {
                    newProps.setProperty("waveformInTab", true);
                }
                updateGUI = true;
                resetGUI = true;
            }
        };
        jlbl = new JLabel("Waveform group panel in ");
        hbox.add(jlbl);

        bg = new ButtonGroup();
        jrb = new JRadioButton("Split Pane");
        jrb.setSelected(! newProps.getBoolean("waveformInTab"));
        jrb.addActionListener(al);
        bg.add(jrb);
        hbox.add(jrb);

        jrb = new JRadioButton("Tab Pane");
        jrb.setSelected(newProps.getBoolean("waveformInTab"));
        jrb.addActionListener(al);
        bg.add(jrb);
        hbox.add(jrb);
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);

        // Scroller hide panel corner button location
        al = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String cmd = e.getActionCommand();
                if (cmd.equals("Top")) {
                    newProps.setProperty("groupPanelFilterButtonLocation", "top");
                }
                else if (cmd.equals("Bottom")) {
                    newProps.setProperty("groupPanelFilterButtonLocation", "bottom");
                }
                resetGUI = true;
            }
        };
        hbox = Box.createHorizontalBox();
        jlbl = new JLabel("Scroller panel hide button corner location");
        hbox.add(jlbl);
        jrb = new JRadioButton("Top");
        jrb.setSelected(newProps.getProperty("groupPanelFilterButtonLocation","").equalsIgnoreCase("top"));
        jrb.addActionListener(al);
        bg = new ButtonGroup();
        bg.add(jrb);
        hbox.add(jrb);
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        jrb = new JRadioButton("Bottom");
        jrb.setSelected(newProps.getProperty("groupPanelFilterButtonLocation","bottom").equalsIgnoreCase("bottom"));
        jrb.addActionListener(al);
        bg.add(jrb);
        hbox.add(jrb);
        vbox.add(hbox);

        // mainSplit orientation
        hbox = Box.createHorizontalBox();
        jlbl = new JLabel("Split pane orientation ");
        hbox.add(jlbl);
        jrb = new JRadioButton("Horizontal");
        int splitVal = newProps.getInt("mainSplitOrientation", 0); 
        jrb.setSelected((splitVal == JSplitPane.HORIZONTAL_SPLIT)); 
        al = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String cmd = e.getActionCommand();
                if (cmd == "Horizontal") {
                    newProps.setProperty("mainSplitOrientation", JSplitPane.HORIZONTAL_SPLIT);
                }
                else if (cmd == "Vertical") {
                    newProps.setProperty("mainSplitOrientation", JSplitPane.VERTICAL_SPLIT);
                }
                updateGUI = true;
            }
        };

        bg = new ButtonGroup();
        jrb.addActionListener(al);
        bg.add(jrb);
        hbox.add(jrb);
        jrb = new JRadioButton("Vertical");
        jrb.setSelected((splitVal == JSplitPane.VERTICAL_SPLIT)); 
        jrb.addActionListener(al);
        bg.add(jrb);
        hbox.add(jrb);
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);

        jcb = makeUpdateCheckBox("Swarm utility in waveform tab pane",
                "checked = display in wave tab pane; unchecked= display in separate window",
                "swarmInWaveformTabPane");
        vbox.add(jcb);

        //  WFGroupPanel horizontal unit scroll pixels
        hbox = Box.createHorizontalBox();
        jlbl = new JLabel("Group panel horz unit scroll pixels ");
        hbox.add(jlbl);
        jtf = new JTextField(df1.format(newProps.getInt("group.horz.unitScrollPixels", 5)));
        jtf.getDocument().addDocumentListener(new MiscDocListener("group.horz.unitScrollPixels", jtf));
        jtf.setMaximumSize(new Dimension(30,20));
        hbox.add(jtf);
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);

        //Box vbox2 = Box.createVerticalBox();
        //vbox2.setBorder(BorderFactory.createTitledBorder("Waveform Panel Scrolling"));
        //  WFGroupPanel horizontal block scroll factor (fraction like 0.01 - 1.0)
        hbox = Box.createHorizontalBox();
        jlbl = new JLabel("Group panel horz block scroll fraction ");
        jlbl.setToolTipText("fraction of viewport width to scroll (0.01 - 1.0)");
        hbox.add(jlbl);
        jtf = new JTextField(df2.format(newProps.getDouble("group.horz.blockScrollFactor", .10)));
        jtf.getDocument().addDocumentListener(new MiscDocListener("group.horz.blockScrollFactor", jtf));
        jtf.setMaximumSize(new Dimension(30,20));
        hbox.add(jtf);
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);

        hbox = Box.createHorizontalBox();
        jlbl = new JLabel("Zoom panel horz block scroll fraction ");
        jlbl.setToolTipText("fraction of viewport width to scroll (0.01 - 1.0)");
        hbox.add(jlbl);
        jtf = new JTextField(df2.format(newProps.getDouble("zoom.horz.blockScrollFactor", .05)));
        jtf.getDocument().addDocumentListener(new MiscDocListener("zoom.horz.blockScrollFactor", jtf));
        jtf.setMaximumSize(new Dimension(30,20));
        hbox.add(jtf);
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);

        hbox = Box.createHorizontalBox();
        jlbl = new JLabel("Zoom panel vert block scroll fraction  ");
        jlbl.setToolTipText("fraction of viewport height to scroll (0.01 - 1.0)");
        hbox.add(jlbl);
        jtf = new JTextField(df2.format(newProps.getDouble("zoom.vert.blockScrollFactor", .10)));
        jtf.getDocument().addDocumentListener(new MiscDocListener("zoom.vert.blockScrollFactor", jtf));
        jtf.setMaximumSize(new Dimension(30,20));
        hbox.add(jtf);
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);

        hbox = Box.createHorizontalBox();
        jlbl = new JLabel("Zoom panel seedchan to filter ");
        jlbl.setToolTipText("seedchan for which to toggle filter on when the waveform is loaded");
        hbox.add(jlbl);
        jtf = new JTextField(newProps.getProperty("displayFilteredSeedchan"));
        jtf.getDocument().addDocumentListener(new MiscDocListener("displayFilteredSeedchan", jtf));
        jtf.setMaximumSize(new Dimension(180,20));
        hbox.add(jtf);
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);

        // triaxial in zoom window 
        hbox = Box.createHorizontalBox();
        jcb = new JCheckBox("Zoom panel triaxial selection locked"); 
        jcb.setToolTipText("When selected, a change of station does not toggle the triaxial button off");
        jcb.setSelected(newProps.getBoolean("triaxialSelectionLocked")); 
        jcb.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                JCheckBox jcb = (JCheckBox) evt.getSource();
                newProps.setProperty("triaxialSelectionLocked", jcb.isSelected());
            }
        });
        hbox.add(jcb);
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);

        // triaxial in zoom window 
        hbox = Box.createHorizontalBox();
        jcb = new JCheckBox("Zoom panel triaxial selection plot Z on H");
        jcb.setToolTipText("When selected, the vertical timeseries is plotted on the horizontal channel view"); 
        jcb.setSelected(newProps.getBoolean("triaxialZonH")); 
        jcb.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                JCheckBox jcb = (JCheckBox) evt.getSource();
                newProps.setProperty("triaxialZonH", jcb.isSelected());
            }
        });
        hbox.add(jcb);
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);

        // triaxial group scroll, scrolls zoom selected time window 
        hbox = Box.createHorizontalBox();
        jcb = new JCheckBox("Triaxial mode group/zoom scroll synch"); 
        jcb.setToolTipText("In triaxial mode, a horizontal scroll of group panel scrolls upper zoom panel's window");
        jcb.setSelected(newProps.getBoolean("triaxialScrollZoomWithGroup")); 
        jcb.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                JCheckBox jcb = (JCheckBox) evt.getSource();
                WFScroller.triaxialScrollZoomWithGroup = jcb.isSelected(); 
                newProps.setProperty("triaxialScrollZoomWithGroup", jcb.isSelected());
            }
        });
        hbox.add(jcb);
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);

        //jcb = makeUpdateCheckBox("Zoom panel bias button toggled on", "zoomBiasButtonOn");
        jcb = new JCheckBox("Zoom panel bias button toggled on");
        jcb.setToolTipText("By default, show waveform trace bias line in zoom panel");
        jcb.setSelected(newProps.getBoolean("zoomBiasButtonOn")); 
        jcb.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                JCheckBox jcb = (JCheckBox) evt.getSource();
                newProps.setProperty("zoomBiasButtonOn", jcb.isSelected());
            }
        });
        hbox.add(jcb);
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(jcb);

        jcb = new JCheckBox("Zoom panel cursor shows amplitude physical units");
        jcb.setToolTipText("By default, physical units are not displayed, only digital counts");
        jcb.setSelected(newProps.getBoolean("zoom.cursorShowsPhyUnits")); 
        jcb.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                JCheckBox jcb = (JCheckBox) evt.getSource();
                newProps.setProperty("zoom.cursorShowsPhyUnits", jcb.isSelected());
                updateGUI = true;
            }
        });
        hbox.add(jcb);
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(jcb);

        //vbox.add(Box.createVerticalStrut(10));

        //vbox.add(Box.createVerticalStrut(10));

//pickingPanelArrowLayout=S
        hbox = Box.createHorizontalBox();
        jlbl = new JLabel("Waveform scroller button panel location");
        jlbl.setToolTipText("Location relative to top zoom picking panel");
        hbox.add(jlbl);
        al = new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                String value = null;
                if (evt.getActionCommand().equals("E")) {
                    value = "E";
                }
                else if (evt.getActionCommand().equals("S")) {
                    value = "S";
                }
                else if (evt.getActionCommand().equals("W")) {
                    value = "W";
                }
                else if (evt.getActionCommand().equals("N")) {
                    value = "N";
                }
                newProps.setProperty("pickingPanelArrowLayout", value);
                updateGUI = true;
                resetGUI = true;
            }
        };

        String value = newProps.getProperty("pickingPanelArrowLayout","").toUpperCase();

        bg = new ButtonGroup();
        jrb = new JRadioButton("E");
        jrb.setSelected(value.equals("E"));
        jrb.addActionListener(al);
        bg.add(jrb);
        hbox.add(jrb);

        jrb = new JRadioButton("S");
        jrb.setSelected(value.equals("S"));
        jrb.addActionListener(al);
        bg.add(jrb);
        hbox.add(jrb);

        jrb = new JRadioButton("W");
        jrb.setSelected(value.equals("W"));
        jrb.addActionListener(al);
        bg.add(jrb);
        hbox.add(jrb);

        jrb = new JRadioButton("N");
        jrb.setSelected(value.equals("N"));
        jrb.addActionListener(al);
        bg.add(jrb);

        hbox.add(jrb);
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);

        jcb = makeResetCheckBox("Mini toolbar buttons", "miniToolBarButtons");
        vbox.add(jcb);

        jcb = makeResetCheckBox("Stack picking panel buttons", "stackPickingPanelButtons");
        vbox.add(jcb);

        // done with creating Waveform components add result
        this.add(vbox);

    }
}
