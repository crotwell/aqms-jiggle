package org.trinet.jiggle;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;

import org.trinet.util.*;
import org.trinet.util.graphics.*;

public class GraphicDialog extends CenteredDialog {

     JLabel textLabel = new JLabel();

     // make 0-100%
     JProgressBar progressBar = new JProgressBar(0, 100);
     JLabel graphicLabel = new JLabel();
     JPanel mainPanel = new JPanel();

     public GraphicDialog() {
       this ("", "", null);
     }

     public GraphicDialog(String title, String text, Icon icon) {

        getContentPane().add(graphicLabel, BorderLayout.CENTER);

          set (title, text, icon);
          
          pack();
          setVisible(true);
     }

     public void set(String title, String text,  Icon icon) {
        setTitle(title);
        setText(text);
        setIcon(icon);
        setProgress(0);

        pack();
     }
/** Set the text in the text panel. */
     public void setText(String text) {
       textLabel.setText(text);
       pack();
     }

     public void setIcon (Icon icon) {
        graphicLabel = new JLabel(icon);
        pack();
     }

     public void setProgress (int percent) {
        progressBar.setValue(percent);
        progressBar.setString(percent+"%");
     }

     public void setProgress (double percent) {
        setProgress( (int) percent);
     }

     /*
    public static void main(String args[]) {
        GraphicDialog dia =
         new GraphicDialog("Test of animation.", "test",
               new ImageIcon("images/gearsAnim.gif"));

    }
    */

}
