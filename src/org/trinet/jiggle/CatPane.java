// TODO: rework this class, buttons, listeners, and make use of JasiSolutionListPanel like Mung?
package org.trinet.jiggle;

import java.awt.*;
import java.util.*;
import java.io.*;
import java.awt.event.*;

import javax.swing.*;      // JFC "swing" library
import javax.swing.event.*;      // JFC "swing" library

import org.trinet.jdbc.*;
import org.trinet.jasi.*;
import org.trinet.util.*;
import org.trinet.util.gazetteer.WhereIsEngine;
import org.trinet.util.graphics.*;
import org.trinet.util.graphics.table.*;

/**
 * Container for the catalogPanel that supports a button to push to load the
 * selected event.
 */
public class CatPane extends JPanel {

    org.trinet.jiggle.CatalogPanel catPanel;

    Jiggle jiggle;		// parent

    // Can't make it yet because properties aren't loaded yet
    EventSelectionDialog eventSelectionDialog =  null;

    public CatPane () {}

    public CatPane(org.trinet.jiggle.CatalogPanel catPanel, Jiggle jiggle) {

        setLayout(new BorderLayout());
        this.catPanel = catPanel;
        this.jiggle = jiggle;
        setWhereIsEngine(jiggle.getWhereIsEngine());

        JButton loadButton = new JButton("LOAD");
        loadButton.addActionListener (new LoadButtonHandler() );
        loadButton.setToolTipText("Load waveform data to view for this event");

        JButton refreshButton = new JButton("REFRESH");
        refreshButton.addActionListener (new RefreshButtonHandler() );
        refreshButton.setToolTipText("Refresh the catalog view");

        /* Removed - experimented on viewing .GIF of data, was too slow and
           needed to be local to .GIF file
        JButton previewButton = new JButton("PREVIEW");
        previewButton.addActionListener (new PreviewButtonHandler() );
        previewButton.setToolTipText("View snapshot of event.");
        box.add(previewButton);
        */

        JButton filterButton = new JButton("FILTER");
        filterButton.addActionListener (new FilterButtonHandler() );
        filterButton.setToolTipText("Define event selection criteria");

        JButton deleteButton = new JButton("DELETE");
        deleteButton.addActionListener (new DeleteButtonHandler() );
        deleteButton.setToolTipText("Delete event from data source");

        JPanel buttonPanel = catPanel.getButtonPanel();
        buttonPanel.add(deleteButton);
        buttonPanel.add(filterButton);
        buttonPanel.add(refreshButton);
        buttonPanel.add(loadButton);
        buttonPanel.setMinimumSize(new Dimension(25,25));

/*	JPanel box = new JPanel();		// want flow layout
        box.add(deleteButton);
        box.add(filterButton);
        box.add(refreshButton);
        box.add(loadButton);
        add(box, BorderLayout.NORTH);;
*/
        add(catPanel, BorderLayout.CENTER);
    }

    /*
    private Component makeTimeComboBox () {
 //       DeltaTimePanel panel =  new DeltaTimePanel();
//        return (Component) panel;

      JPanel panel = new JPanel();

      JComboBox cb = new JComboBox();

      cb.addItem("1");
      cb.addItem("2");
      cb.addItem("4");
      cb.addItem("12");
      cb.addItem("24");
      cb.addItem("48");
      cb.addItem("72");

      cb.setEditable(true);		    // allow freeform user input
      String sel = ""+jiggle.props.hoursBack;
      cb.setSelectedItem(sel);		    // default selection
      cb.setMaximumRowCount(10);	    // # items displayed in scrolling window

      cb.addActionListener(new TimeComboHandler());

      panel.add(cb);
      panel.add(new JLabel(" hours back"));

      return panel;
    }
      */
//-----------------------------------------------------------------
// Event handling
//-----------------------------------------------------------------


/** Handle change in time combo box.  */
/*
    class TimeComboHandler implements ActionListener
    {
      public void actionPerformed(ActionEvent e)
         {
            JComboBox jc = (JComboBox) e.getSource();

            String selItem = (String) jc.getSelectedItem();
            double val = Double.valueOf(selItem).doubleValue();

         // keep the property up to date
//	    jiggle.props.hoursBack = val;
         jiggle.setCatalogTimeWindow(val);
            jiggle.resetCatPanel();

            ///


         }
    }
 */
/** Set event selection criteria */
class FilterButtonHandler implements ActionListener {

    public void actionPerformed (ActionEvent evt) {

      if (eventSelectionDialog == null) {
          eventSelectionDialog = new EventSelectionDialog((JFrame)jiggle,
                                "Event Selection", true,
                                 jiggle.eventProps);
         }

         // note: this is a modal dialog
      eventSelectionDialog.setVisible(true);

      // if OK button was hit save the new properties
      if (eventSelectionDialog.getButtonStatus() == JOptionPane.OK_OPTION) {

         System.out.println(jiggle.eventProps.listToString() );
         jiggle.setEventProperties(eventSelectionDialog.getEventSelectionProperties());
         System.out.println("After ......\n"+jiggle.eventProps.listToString() );

         // save?
//         eventPropsSaveDialog();

         // refresh the catalog from DataSource snapshot satisfying filter 
            jiggle.resetCatPanel();
      }

    }
}
/**
 * Pop dialog asking if event selection preferences should be saved to a file.
 */
    void eventPropsSaveDialog() {

    //pop confirming  yes/no dialog:
           int yn = JOptionPane.showConfirmDialog(
                   null, "Save these event properties to startup file?",
                   "Save Event Properties?",
                   JOptionPane.YES_NO_OPTION);

            if (yn == JOptionPane.YES_OPTION) {
                   jiggle.eventProps.saveProperties();
            }
    }

    /** Load a preview .gif of the event */
    /*
class PreviewButtonHandler implements ActionListener {

    public void actionPerformed (ActionEvent evt) {

        //	String action =  evt.getActionCommand();
        Solution sol[] = CatPane.this.catPanel.getSelectedSolutions();

        if (sol != null) {
        // TODO: handle multiple selected events
            if (sol.length >= 1) {

                // String gifPath = props.getProperty("previewPath")

                // works on LOCAL .HTML files only
                String gifPath = "/home/tpp/www/eventfiles/done/";
                String gifFile = gifPath + sol[0].id.toString()+".html";

                System.out.println ("Preview: "+ gifFile);

                HTMLHelp help = new HTMLHelp(gifFile, gifFile);

            }
        }

    }
}
*/

/** Load a selected event */
class LoadButtonHandler implements ActionListener {

    public void actionPerformed (ActionEvent evt) {

        //	String action =  evt.getActionCommand();
        Solution sol[] = CatPane.this.catPanel.getSelectedSolutions();

        // TODO: handle multiple selected events
        if (sol != null && sol.length > 0 ) {
                CatPane.this.jiggle.loadSolution(sol[0]);
        }
        else {
           JOptionPane.showMessageDialog(getTopLevelAncestor(),
                           "No CATALOG event id selected, must select an id to load",
                           "Load Waveforms", JOptionPane.ERROR_MESSAGE);
        }
// MV crash here? (prob. in other thread)
    }
}
/** Delete a selected event */
class DeleteButtonHandler implements ActionListener {

    public void actionPerformed (ActionEvent evt) {

        Solution sol[] = CatPane.this.catPanel.getSelectedSolutions();

        if (sol != null && sol.length > 0) {
          // must to by id # and NOT by sol object because the solution objects
          // in the CatalogList are different from those in the MasterView.
                CatPane.this.jiggle.deleteSolution(sol[0].id.longValue());
                // refresh the DataSource Catalog snapshot to show change 
                jiggle.resetCatPanel();
     }

    }
}
    /** Refresh the catalog view */
class RefreshButtonHandler implements ActionListener {

    public void actionPerformed (ActionEvent evt) {

        // show latest snapshot of the DataSource catalog
        jiggle.resetCatPanel();

        // need a repaint??

    }
}

protected void setWhereIsEngine(WhereIsEngine eng) {
    catPanel.setWhereIsEngine(eng);
}

protected void update() { // force kludge update of CatalogPanel after solved - aww 11/03
    catPanel.update();
}

public SolutionList getSolutionList() {
    return (catPanel == null) ? null : catPanel.getSolutionList();
}

} // CatPane
