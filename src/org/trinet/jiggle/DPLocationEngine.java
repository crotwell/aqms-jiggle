package org.trinet.jiggle;

import java.awt.*;
import java.awt.event.*;
import javax.swing.JPanel;

import java.awt.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;
//import javax.swing.border.TitledBorder;

import org.trinet.util.graphics.ColumnLayout;

public class DPLocationEngine extends JPanel {

  JiggleProperties props;

    JTextField locEngType;
    JTextField locEngUrl;
    JTextField locEngPort;

    JCheckBox trialLocCk;
    JCheckBox trialTimeCk;
    JCheckBox fixQuarryCk;

public DPLocationEngine(JiggleProperties properties) {

    props = properties;

    try {
      initGraphics();
    }
    catch(Exception ex) {
      ex.printStackTrace();
    }
  }

  private void initGraphics() throws Exception {

    String type = props.getProperty("locationEngineType");
    String url  = props.getProperty("locationEngineAddress");
    String port = props.getProperty("locationEnginePort");

    // A document listener will change props values as fields are edited
    LocServerDocListener locServerDocListener = new LocServerDocListener(this);

        // Create the labels and text fields.
        JLabel     label1 = new JLabel("Type: ", JLabel.RIGHT);
        locEngType = new JTextField(type);
    locEngType.getDocument().addDocumentListener(locServerDocListener);

        JLabel     label2 = new JLabel("IP-address: ", JLabel.RIGHT);
        locEngUrl = new JTextField(url);   // don't echo password text
    locEngUrl.getDocument().addDocumentListener(locServerDocListener);

        JLabel     label3 = new JLabel("Port #: ", JLabel.RIGHT);
        locEngPort = new JTextField(port);
    locEngPort.getDocument().addDocumentListener(locServerDocListener);

//        JPanel connectionPanel = new JPanel(false);
        setLayout(new BoxLayout(this, BoxLayout.X_AXIS));

    setBorder( new TitledBorder("Location Engine Setup") );

        JPanel namePanel = new JPanel();
        namePanel.setLayout(new GridLayout(0, 1));
        namePanel.setMinimumSize(new Dimension(67, 25));
    namePanel.add(label1);
        namePanel.add(label2);
        namePanel.add(label3);

        JPanel fieldPanel = new JPanel();
        fieldPanel.setLayout(new GridLayout(0, 1));
        fieldPanel.add(locEngType);
        fieldPanel.add(locEngUrl);
        fieldPanel.add(locEngPort);

//  JPanel URLpanel = new JPanel();
//  URLpanel.setLayout(new BoxLayout(URLpanel, BoxLayout.X_AXIS));
//        URLpanel.add(namePanel);
//        URLpanel.add(fieldPanel);

    JPanel checkPanel = new JPanel();
    checkPanel.setLayout(new GridLayout(0, 1));
    checkPanel.setBorder(BorderFactory.createEtchedBorder());
    trialLocCk = new JCheckBox("Use Trial Location", props.getBoolean("useTrialLocation"));
    trialTimeCk = new JCheckBox("Use Trial OriginTime", props.getBoolean("useTrialOriginTime"));
    fixQuarryCk = new JCheckBox("Fix Quarry Depths", props.getBoolean("fixQuarryDepth"));

        CheckBoxListener checkBoxListener = new CheckBoxListener();
        fixQuarryCk.addItemListener(checkBoxListener);
        trialLocCk.addItemListener(checkBoxListener);
        trialTimeCk.addItemListener(checkBoxListener);

    checkPanel.add(trialLocCk);
    checkPanel.add(trialTimeCk);
    checkPanel.add(fixQuarryCk);
    add(checkPanel);
    add(Box.createHorizontalGlue());

    add(namePanel);
        add(fieldPanel);

//  add(URLpanel);

    return;
    }

class CheckBoxListener implements ItemListener {

    public void itemStateChanged(ItemEvent e) {

        Object source = e.getItemSelectable();

        if (source == trialLocCk) {
         props.setProperty("useTrialLocation",
                  trialLocCk.isSelected() );
        } else if (source == trialTimeCk) {
         props.setProperty("useTrialOriginTime",
                  trialTimeCk.isSelected() );
    } else if (source == fixQuarryCk) {
         props.setProperty("fixQuarryDepth",
                  fixQuarryCk.isSelected() );
    }

    }
}

    /**
     * This is better then an actionListener because it updates
     * the values as they are edited and does not require that the user
     * hit a <CR> to trigger the listener.
     */
    class LocServerDocListener implements DocumentListener {

    DPLocationEngine dia;

    // Passes reference to the caller so we can see and change some stuff
    public LocServerDocListener(DPLocationEngine pd)
    {
        dia = pd;  // need reference to see other variables
    }
         public void insertUpdate(DocumentEvent e) {
             setValues(e);
         }
         public void removeUpdate(DocumentEvent e) {
             setValues(e);
         }
         public void changedUpdate(DocumentEvent e) {
             // we won't ever get this with a PlainDocument
         }

     private void setValues(DocumentEvent e) {

         // don't know what is being editied so just read 'em all
         // Location engine
         dia.props.setProperty("locationEngineType",
                  dia.locEngType.getText().trim() );
         dia.props.setProperty("locationEngineAddress",
                  dia.locEngUrl.getText().trim() );
         dia.props.setProperty("locationEnginePort",
                  dia.locEngPort.getText().trim() );

      }
    }
/*
    public static void main(String s[]) {

        JFrame frame = new JFrame("Main");
        frame.addWindowListener(new WindowAdapter()
    {
            public void windowClosing(WindowEvent e) {System.exit(0);}
        });

    JiggleProperties props = new JiggleProperties("properties");

    System.out.println("----------- Current Properties -------------");
    props.dumpProperties();

    DPLocationEngine dp = new DPLocationEngine(props);

    frame.getContentPane().add(dp);

        frame.pack();
        frame.setVisible(true);

//  System.exit(0);

    }
*/
}

