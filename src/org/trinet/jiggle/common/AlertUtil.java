package org.trinet.jiggle.common;

import javax.swing.*;

public final class AlertUtil {

    /**
     * Alert users with a message dialog
     */
    public static void displayError(String title, String errorMsg) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                final String errorDesc = errorMsg + JiggleConstant.NEWLINE +
                        JiggleConstant.NEWLINE +
                        "Please restart the application if the problem persists!";

                JOptionPane.showMessageDialog(null, errorDesc, title, JOptionPane.WARNING_MESSAGE);
                LogUtil.error(errorMsg);
            }
        });
    }

    public static void displayError(String title, String errorMsg, String actionMsg) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                final String errorDesc = errorMsg + JiggleConstant.NEWLINE +
                        JiggleConstant.NEWLINE +
                        actionMsg;

                JOptionPane.showMessageDialog(null, errorDesc, title, JOptionPane.ERROR_MESSAGE);
                LogUtil.error(errorMsg);
            }
        });
    }

    /**
     * Display error with JOptionPane message type
     */
    public static void displayError(String title, String errorMsg, String actionMsg, int messageType) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                final String errorDesc = errorMsg + JiggleConstant.NEWLINE +
                        JiggleConstant.NEWLINE +
                        actionMsg;

                JOptionPane.showMessageDialog(null, errorDesc, title, messageType);
                LogUtil.error(errorMsg);
            }
        });
    }

    /**
     * Display error and block for user input
     */
    public static int displayCrticialError(String title, String errorMsg) {
        final int[] response = new int[1];
        try {
            SwingUtilities.invokeAndWait(new Runnable() {
                @Override
                public void run() {
                    final String errorDesc = errorMsg + JiggleConstant.NEWLINE +
                            JiggleConstant.NEWLINE +
                            "Continue processing?";

                    response[0] = JOptionPane.showConfirmDialog(JiggleSingleton.getInstance().getMainJFrame(),
                            errorDesc,
                            title,
                            JOptionPane.YES_NO_OPTION,
                            JOptionPane.QUESTION_MESSAGE);
                    LogUtil.warning(errorMsg + " User response is " + response[0]);
                }
            });
        } catch (Exception ex) {
            JiggleExceptionHandler.handleException(ex, "Error Displaying Dialog", "Error in displayCrticialError. ");
        }
        return response[0];
    }

}
