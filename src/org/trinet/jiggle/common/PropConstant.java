package org.trinet.jiggle.common;

public final class PropConstant {
    public static final String DB_PASSWD = "dbasePasswd";

    public static final String MASK_VALUE = "*****"; // Replace sensitive value. Hardcoded to not provide length of string.

    // Origin types
    public static final String ORIGIN_TYPE_PROP_NAME = "origin.type.";

    public static final String ORIGIN_KEY_HYPOCENTER = "origin.type.h";
    public static final String ORIGIN_KEY_CENTROID = "origin.type.c";
    public static final String ORIGIN_KEY_AMP = "origin.type.a";
    public static final String ORIGIN_KEY_DBLDIFF = "origin.type.d";
    public static final String ORIGIN_KEY_UNKNOWN = "origin.type.u";
    public static final String ORIGIN_KEY_NONLOCATABLE = "origin.type.n";

    public static final String ORIGIN_DESC_HYPOCENTER = "Hypocenter";
    public static final String ORIGIN_DESC_CENTROID = "Centroid";
    public static final String ORIGIN_DESC_AMP = "Amplitude";
    public static final String ORIGIN_DESC_DBLDIFF = "Double Difference";
    public static final String ORIGIN_DESC_UNKNOWN = "Unknown";
    public static final String ORIGIN_DESC_NONLOCATABLE = "Non Locatable";

    public static final String EARTHQUAKE_REGIONAL_COMMENT = "comment.earthquake.regional";

    // Scope
    public static final String SCOPE_REQUIRE_NEW_EVENT_ON_FINAL = "scope.finalRequireNewEvent";
    public static final String SCOPE_USE_START_TIME = "scope.useStartTime";

    // Event Association
    public static final String EVENT_ASSOCIATION_TIME_RANGE = "eventAssociationTimeRange";
    
    // Debug flag
    public static final String DEBUG = "debug";

    // Waveform
    public static final String WAVEFORM_DESC_FORMAT_FILTER = "filter";
    public static final String WAVEFORM_DESC_FORMAT_AZIMUTH = "azimuth";
    public static final String WAVEFORM_DESC_FORMAT_AZIMUTH_UNIT = "deg";
}
