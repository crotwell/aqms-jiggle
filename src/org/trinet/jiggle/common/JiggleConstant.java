package org.trinet.jiggle.common;

public final class JiggleConstant {
    public static String NEWLINE = System.getProperty("line.separator");

    public static String POPUP_MENU_REMOVE_EVENT_LOCK = "Remove Event Lock...";
    public static String ERROR_RESTART_JIGGLE = "Please retry or restart Jiggle!";

    public static String POPUP_MENU_ASSOCIATION = "Event Association";
    public static String POPUP_MENU_NEW_ASSOCIATION = "New Association...";
    public static String POPUP_MENU_EDIT_ASSOCIATION = "Edit Association...";
    public static String POPUP_MENU_DELETE_ASSOCIATION = "Delete Association...";
    
    public final class Icons {
        public static final String JiggleLogoIcon = "JiggleLogo.gif";
    }
}
