package org.trinet.jiggle.common.type;

/** Type class for trinetdb.remark **/

public class RemarkType {
    private long commentId;
    private int lineNumber;
    private String comment;

    static final public long DEFAULT_ID = -1;
    static final public int DEFAULT_LINE_NUMBER = 1;

    public RemarkType(String comment) {
        this.commentId = DEFAULT_ID;
        this.lineNumber = DEFAULT_LINE_NUMBER;
        this.comment = comment;
    }

    public long getCommentId() {
        return commentId;
    }

    public void setCommentId(long commentId) {
        this.commentId = commentId;
    }

    public int getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(int lineNumber) {
        this.lineNumber = lineNumber;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
