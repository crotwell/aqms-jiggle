package org.trinet.jiggle.common.type;

import org.trinet.jasi.GType;
import org.trinet.jasi.GTypeMap;
import java.util.Objects;

/**
 * Extends GType for UI display
 */
public class GeoType extends GType {

    public GeoType(GType gt) {
        super(gt);
    }

    public String toString() {
        if (!this.name.equalsIgnoreCase(GTypeMap.NULL)) {
            return this.name.substring(0, 1).toUpperCase() + this.name.substring(1); // capitalize first letter.
        } else {
            return GTypeMap.N;
        }
    }

    public boolean equals(Object obj) {
        if (! (obj instanceof GeoType)) return false;
        GeoType gt = (GeoType) obj;
        return code.equalsIgnoreCase(gt.code);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code);
    }
}
