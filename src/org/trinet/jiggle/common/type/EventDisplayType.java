package org.trinet.jiggle.common.type;

import org.trinet.jasi.EventType;
import java.util.Objects;

public class EventDisplayType extends EventType implements Comparable<EventDisplayType> {

    public EventDisplayType(EventType et) {
        super(et);
    }

    public String toString() {
        return this.name.substring(0, 1).toUpperCase() + this.name.substring(1); // capitalize first letter.
    }

    public int compareTo(EventDisplayType type) {
        return this.name.compareToIgnoreCase(type.name);
    }

    public boolean equals(Object obj) {
        if (! (obj instanceof EventDisplayType)) return false;
        EventDisplayType gt = (EventDisplayType) obj;
        return code.equalsIgnoreCase(gt.code);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code);
    }

}
