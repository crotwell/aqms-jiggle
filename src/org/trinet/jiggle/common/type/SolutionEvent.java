package org.trinet.jiggle.common.type;

import org.trinet.jasi.Solution;
import java.util.EventObject;

public class SolutionEvent extends EventObject {
    Solution solution;

    public SolutionEvent(Solution solution){
        super(solution);
        this.solution = solution;
    }

    public Solution getSolution(){
        return solution;
    }
}
