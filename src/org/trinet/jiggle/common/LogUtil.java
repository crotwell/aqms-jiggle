package org.trinet.jiggle.common;

import java.time.Instant;

public final class LogUtil {
    private static String DEBUG = "DEBUG";
    private static String INFO = "INFO";
    private static String ERROR = "ERROR";
    private static String WARN = "WARN";

    public static void debug(boolean debug, String msg){
        if (debug) {
            log(DEBUG, msg);
        }
    }

    public static void info(String msg) {
        log(INFO, msg);
    }

    public static void error(String msg) {
        log(ERROR, msg);
        System.err.println(msg); //keep this in case error is redirected
    }

    public static void warning(String msg) {
        log(WARN, msg);
    }

    public static void log(String level, String msg) {
        Instant instant = Instant.now();

        // Jiggle uses UTC
        System.out.println(instant + " - " + level + " - " + msg);
    }
}
