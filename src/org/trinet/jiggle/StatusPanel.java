package org.trinet.jiggle;

import java.applet.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.text.*;
import javax.swing.event.*;
import javax.swing.border.*;

/**
 * This component has two parts:<p>
 *  o A text label that describes the current action that's happening. <p>
 *  o A progress bar that shows the progress of the current action.<p>
 * It is used to show the progress of a time consuming opperation like
 * loading data. It is usually added to the JiggleStatusBar.

 */
public class StatusPanel extends JPanel {

    private JLabel statusLabel = new JLabel();
    //private JProgressBar progressBar = new JProgressBar();

    static int min = 0;
    static int max = 100;

    public StatusPanel() {

      Box hbox = Box.createHorizontalBox();
      //hbox.setBorder(new EtchedBorder());
      // Text label
      statusLabel.setText("Status text shows here");
      statusLabel.setToolTipText("Shows current action.");
      statusLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
      statusLabel.setMinimumSize(new Dimension(64, 16));
      hbox.add(statusLabel);

/*
      progressBar.setOrientation(JProgressBar.HORIZONTAL);
      progressBar.setBorderPainted(true);
      progressBar.setStringPainted(true);
      progressBar.setPreferredSize(new Dimension(200, progressBar.getHeight()));
      progressBar.setBorder(new EtchedBorder());
      progressBar.setToolTipText("Shows progress of slow actions.");
      add(progressBar);
*/
      hbox.add(Box.createHorizontalGlue());
      this.add(hbox);

      clear();

    } // end of createPanel


    void reset(String str) {
        statusLabel.setText(str);
        //jdk1.4 progressBar.setIndeterminate(false); // aww 02/24/2005 
        /*
        progressBar.setMinimum(min);
        progressBar.setMaximum(max);
        progressBar.setValue(min);
        progressBar.setForeground(Color.blue);
        */
    }

    public void clear() {
        reset("");
    }

    // kludge to avoid old thread finished clearing new thread setText, check:
    public void clearOnlyOnMatch(String str) {
        if (statusLabel.getText().equals(str)) reset("");
    }

    public void setText(String str) {
        statusLabel.setText(str);
        statusLabel.validate();
    }

    //Set the progress bar value. A value of -1 means set to max value.
    public void setProgressBarValue(int val) {
        //if (val == -1) val = progressBar.getMaximum();
        //progressBar.setValue(val);
        //progressBar.validate();
    }

    public void setProgressBarValue(int val, String str) {
        //setProgressBarValue(val);
        setText(str);
    }

    public void setProgressStringPainted(boolean tf) {
        //progressBar.setStringPainted(tf);
    }

    public void setProgressIndeterminate(boolean tf) {
        //jdk1.4 progressBar.setIndeterminate(tf); // aww 02/24/2005 
    }

/*
    public static void main(String s[])
    {

      JFrame frame = new JFrame("Main");
      frame.addWindowListener(new WindowAdapter()
      {
        public void windowClosing(WindowEvent e) {System.exit(0);}
      });


      StatusPanel statusPanel =  new StatusPanel();
      frame.getContentPane().add(statusPanel);


      frame.pack();
      frame.setVisible(true);


      statusPanel.setText("Starting");

      for (int i = 1; i<100; i++) {
        statusPanel.setProgressBarValue( i, "counted to: "+i);

        try {
          Thread.sleep(100);
          } catch (InterruptedException ex) {}
      }

    }
*/
} // end of class

