package org.trinet.jiggle;

import java.text.*;
import java.awt.*;
import java.util.*;
import javax.swing.event.*;
import java.awt.event.*;
import javax.swing.*;

import org.trinet.jdbc.*;
import org.trinet.jasi.*;
import org.trinet.filters.*;
import org.trinet.util.DateTime;
import org.trinet.util.FilterIF;
import org.trinet.util.BandpassFilterIF;
import org.trinet.util.LeapSeconds;
import org.trinet.util.TimeSpan;
import org.trinet.util.SwingWorker;
import org.trinet.util.graphics.NumberChooser;
import org.trinet.util.graphics.IconImage;
import org.trinet.util.graphics.text.NumberTextField;
import org.trinet.util.graphics.text.NumberTextFieldFactory;

/**
 * ScrollPane that contains the WFGroupPanel. Is a 'view' in the MVC group for
 * selectionModel so that it can keep the selected WFPanel visible in the
 * viewport. <p>
 *
 * If the view object has focus this component responds to the default keyboard actions
 * for JScrollPane as defined in the Java Look and Feel:<p>
 <tt>
JScrollPane (Java L&F) (Navigate refers to focus)
Navigate out forward      |  Tab
Navigate out backward     |  Shift+Tab
Move up/down              |  Up, Down
Move left/right           |  Left, Right
Move to start/end of data |  Ctrl+Home, Ctrl+End
Block move up/down        |  PgUp, PgDn
Block move right          |  Ctrl+PgDn
Block move left           |  Ctrl+PgUp
</tt>
*/

/*
 *** RUN-AWAY SCROLL BAR BEHAVIOR ****

If you press and hold the vertical scroll arrow the scroller runs-away. It
appears that holding the arrow causes many scroll events to be queued and the
scroller scrolls until they are all serviced. This seems to be a general bug in
the JScrollPane. It can happen wiht the JTable in the CatalogPanel also.
I don't know how to fix this weirdness. DDG 11/19/99 */

public class WFScroller extends JScrollPane {

  public static boolean HIDE_EHG = false;
  public static boolean ENABLE_HIDE = false;
  public static String buttoncorner = ScrollPaneConstants.LOWER_RIGHT_CORNER;

  protected static final int SCROLL_TO_VISIBLE = 0;
  protected static final int SCROLL_TO_CENTER = 1;

  private static final int DEFAULT_PANEL_COUNT = 10;

  protected static int scrollerMode = SCROLL_TO_VISIBLE;

  protected JDialog jdHide = null;

  private MasterView mv;
  private WFView lastSelectedView = null; // added so an enabled "cursorTime" line is removed when master view selection changes -aww 2009/04/15
  private boolean showFilteredWfGroup = false; 

  /** The underlying panel that shows thru the viewport. */
  public WFGroupPanel groupPanel;
  private ChannelLabelViewport jvp = null;

  /** How many panels are visible in the viewport */
  protected int viewsPerPage = DEFAULT_PANEL_COUNT; // initial view count when not viewing triaxial

  private double secondsInViewport = 90.0;
  private boolean showFullTime = false;    // flag for "show all"
  private boolean showRowHeader = false;
  private int channelLabelMode = 0;

// listen for changes to selected channel or time/amp window
  private WFWindowListener wfwChangeListener = new WFWindowListener();
  private WFViewListener   wfvChangeListener = new WFViewListener();
  private VerticalScrollListener vertScrollListener = new VerticalScrollListener();
  private HorizontalScrollListener horzScrollListener = new HorizontalScrollListener();
  private GroupPanelComponentListener groupPanelCompListener = new GroupPanelComponentListener();

  private int hiddenCount = 0;
  protected JButton hideButton = null;
  protected JButton filterButton = null;
  private Icon filterOnIcon = null;
  private Icon filterOffIcon = null;
  private Icon hideOnIcon = null;
  private Icon hideOffIcon = null;
  
  protected boolean hiddenReadings = false;

  protected static boolean triaxialScrollZoomWithGroup = false; // -aww 2010/01/31
  protected int triaxPanelsInView = 3; // -aww 2010/02/07

  protected boolean triaxialMode = false; // -aww 2010/01/25
  protected JRadioButtonMenuItem jbTriaxial = null;

  /** Null constructor */
  public WFScroller() {
  }

  /**
   * Constructor with placeholder string. This is used to create a valid WFScroller
   * when there is no data loaded yet.
   */
  public WFScroller(String str) {
    add(new JLabel(str));
  }

  /**
   * Create a WFScroller containing a WFGroupPanel that has one passive
   * WFPanel for each WFView in the MasterView. By default 10 WFPanels will be
   * visible in the viewport.  */
  public WFScroller(MasterView mv) {
    this(mv, DEFAULT_PANEL_COUNT, false);
  }

  /**
   * Create a WFScroller containing a WFGroupPanel that has one WFPanel
   * for each WFView in the MasterView. If the second argument is 'true'
   * the WFPanels will be active and will respond to mouse events and
   * notify the MasterWFVModel. By default 10 WFPanels will be visible in
   * the viewport.
   */
  public WFScroller(MasterView mv, boolean activePanels) {

    this(mv, DEFAULT_PANEL_COUNT, activePanels);
  }
  /**
   * Create a WFScroller containing a WFGroupPanel that has one WFPanel
   * for each WFView in the MasterView. The second argument is the number of
   * WFPanels visible in the scroller's viewport. If the third argument is 'true'
   * the WFPanels will be active and will respond to mouse events and
   * notify the MasterWFVModel.<p>
   * Note that a fraction of a WFPanel may be visible at the bottom of the view.
   * This is because the viewport size (in pixels)  may not be perfectly
   * divisible by the value of 'Jiggle.props.tracesPerPage' and the "remainder" is extra pixels
   * that show at the bottom as the next panel.
   */
  public WFScroller(MasterView mv, int panelsVisible, boolean activePanels) {


      this.mv = mv;

      addListeners(mv);

      // make a panel with all the WFPanels in it this is what will be scrolled and show thru the viewport
      viewsPerPage = panelsVisible;

      setWFGroupPanel(new WFGroupPanel(mv, activePanels)); // NOTE: viewsPerPage is reset to a smaller value if it scales panel size too small


      // show row header?
      if (getShowRowHeader()) 
          setRowHeaderView(getWFGroupPanel().getRowHeaderPanel());
      setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
      setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS); // added to keep corner hide button -aww 2009/04/01

      Image image = IconImage.getImage("hidden-panel.png");
      hideOnIcon = (image == null) ? null : new ImageIcon(image);
      image = IconImage.getImage("nohidden-panel.png");
      hideOffIcon = (image == null) ? null : new ImageIcon(image);

      hideButton = new JButton(hideOffIcon);
      hideButton.setToolTipText("Hide waveform panels by criteria...");
      hideButton.addActionListener(
        new ActionListener() {
            JPopupMenu hidePopup = new HideMenu(); // created once, not for every action
            public void actionPerformed(ActionEvent e) {
                if (jbRefresh != null) jbRefresh.setEnabled(hiddenReadings);
                if (jbShowAll != null)  jbShowAll.setEnabled(hiddenReadings);
                hidePopup.show((Component)e.getSource(),0,0);
            }
        }
      );
      hideButton.setEnabled(ENABLE_HIDE);

      image = IconImage.getImage("filter_yellow.gif");
      filterOffIcon = (image == null) ? null : new ImageIcon(image);
      image = IconImage.getImage("filter_red.png");
      filterOnIcon = (image == null) ? null : new ImageIcon(image);
      filterButton = new JButton(filterOffIcon);

      // group filter here before ScaleFilterMenu create below for filterOff menu item set as off.  - aww 2016/03/02
      if (mv.getOwner() instanceof Jiggle) {
        if (((Jiggle)mv.getOwner()).getProperties().getBoolean("showFilteredWfGroup")) {
          showFilteredWfGroup = true; 
          int type = FilterTypes.getType( ((Jiggle)mv.getOwner()).getProperties().getProperty("defaultWfGroupFilter","BANDPASS") );
          groupPanel.setFilter(FilterTypes.getFilter(type, 100, false)); // does the filtering
          groupPanel.setShowAlternateWf(true);
          if (filterButton != null && filterOnIcon != null) filterButton.setIcon(filterOnIcon);
        }
      }


      // group filter here before ScaleFilterMenu create below for filterOff menu item set as off.  - aww 2016/03/02
      if (mv.getOwner() instanceof Jiggle) {
        if (((Jiggle)mv.getOwner()).getProperties().getBoolean("showFilteredWfGroup")) {
          showFilteredWfGroup = true; 
          int type = FilterTypes.getType( ((Jiggle)mv.getOwner()).getProperties().getProperty("defaultWfGroupFilter","BANDPASS") );
          groupPanel.setFilter(FilterTypes.getFilter(type, 100, false)); // does the filtering
          groupPanel.setShowAlternateWf(true);
          if (filterButton != null && filterOnIcon != null) filterButton.setIcon(filterOnIcon);
        }
      }

      filterButton.setToolTipText("Scale or filter waveforms...");
      filterButton.addActionListener(
        new ActionListener() {
            JPopupMenu filterPopup = new ScaleFilterMenu(); // created once, not for every action
            public void actionPerformed(ActionEvent e) {
                filterPopup.show((Component)e.getSource(),0,0);
            }
        }
      );
      filterButton.setEnabled(ENABLE_HIDE);

      if (buttoncorner == ScrollPaneConstants.LOWER_RIGHT_CORNER) {
          setColumnHeaderView(new JLabel("  ")); // need columnHeader to make space for upper right corner button -aww
          setCorner(ScrollPaneConstants.LOWER_RIGHT_CORNER, hideButton);
          setCorner(ScrollPaneConstants.UPPER_RIGHT_CORNER, filterButton);
      }
      else {
          setColumnHeaderView(new JLabel("  ")); // need columnHeader to make space for upper right corner button -aww
          setCorner(ScrollPaneConstants.UPPER_RIGHT_CORNER, hideButton);
          setCorner(ScrollPaneConstants.LOWER_RIGHT_CORNER, filterButton);
      }

  }

  protected void clear() {
      if (groupPanel != null) groupPanel.clear();
  }

  private int panelsViewable() {
      WFPanelList wfpList = groupPanel.wfpList;
      int panelCnt = wfpList.size();
      int cnt = 0;
      for (int idx = 0; idx < panelCnt; idx++) {
          if (((WFPanel)wfpList.get(idx)).isVisible()) cnt++;
      }
      return cnt;
  }

  /** Scales the groupPanel to fit right number of WFPanels in the viewport */
  public void setPanelsInView(int panelsVisible) {

    if (panelsVisible > groupPanel.getPanelCount()) { // don't exceed max available panels
      panelsVisible = groupPanel.getPanelCount();
    }

    if (panelsVisible < 1) return; // positive values only!

    viewsPerPage = panelsVisible;

    // use viewport size to set dimensions of ONE wfpanel in the group panel
    Dimension d = getViewport().getExtentSize();
    int height = d.height/viewsPerPage;
    if (height == 0) return;

    if (height < 10) {
        height = 10;
        viewsPerPage = (int) Math.floor(d.height/height);
        height = d.height/viewsPerPage;
        Object obj = mv.getOwner();
        if (obj instanceof WaveformDisplayIF) {
            WaveformDisplayIF wfd = (WaveformDisplayIF) obj;
            JOptionPane.showMessageDialog(getTopLevelAncestor(), 
                String.format("Traces per pages %d, too large, resetting to %d", panelsVisible, viewsPerPage),
                "Panels in View", JOptionPane.PLAIN_MESSAGE);
            wfd.getWfPanelProperties().setProperty("tracesPerPage", viewsPerPage);
        }
    }

    // tell the model so it can jump pages
    mv.masterWFViewModel.setViewsPerPage(viewsPerPage);

    int topIndex = getTopIndex(); // index at scroll value

    int width = d.width;
    if (! getShowFullTime() ) { // handle auto vs fixed time scale
        width = (int)(((double)width/getSecondsInViewport()) * mv.getViewSpan().getDuration());
    }

    Dimension panelSize = new Dimension(width,height); 
    if (! groupPanel.getSinglePanelSize().equals(panelSize)) {
        // This scales the whole WFGroupPanel
        groupPanel.setSinglePanelSize(width, height); // does a revalidate through setSize()
    }

    // keep scroll at same top panel
    setTopIndex(topIndex);
    //System.out.println(".... setPanelsInView done .... width: " + width + " secs: " + getSecondsInViewport() );

  }

  /** Add model listeners to instance MasterView's models. */
  protected void addListeners() {
      addListeners(mv);
  }
  /** Add model listeners to input MasterView's models. */
  protected void addListeners(MasterView mv) {

    // add this panel as an observer of the MVC model of the MasterView
    // to which the WFView belongs
    if (mv != null) {

      // scroll listener to manage waveform cache
      this.getVerticalScrollBar().addAdjustmentListener(vertScrollListener);
      this.getHorizontalScrollBar().addAdjustmentListener(horzScrollListener);

      // listener for selected panel changes
      if (mv.masterWFViewModel != null) {
        mv.masterWFViewModel.removeChangeListener(wfvChangeListener);
        //wfvChangeListener = new WFViewListener(); // removed -aww 2009/03/12
        mv.masterWFViewModel.addChangeListener(wfvChangeListener);
      }
      // listener for selected time window changes
      if (mv.masterWFWindowModel != null) {
        mv.masterWFWindowModel.removeChangeListener(wfwChangeListener);
        mv.masterWFWindowModel.addChangeListener(wfwChangeListener);
      }

    }
  }
  /** Remove model listeners to this MasterView's models. */
  protected void removeListeners(MasterView mv) {

    // add this panel as an observer of the MVC model of the MasterView
    // to which the WFView belongs
    if (mv != null) {

      this.getVerticalScrollBar().removeAdjustmentListener(vertScrollListener);
      this.getHorizontalScrollBar().removeAdjustmentListener(horzScrollListener);

      // listener for selected panel changes
      if (mv.masterWFViewModel != null) {
        mv.masterWFViewModel.removeChangeListener(wfvChangeListener);
      }
      // listener for selected time window changes
      if (mv.masterWFWindowModel != null) {
        mv.masterWFWindowModel.removeChangeListener(wfwChangeListener);
      }

      if (groupPanel != null) groupPanel.wfpList.removeListeners(mv);

    }
  }

  /** Return the value, in pixels, to be used as the vertical scrolling unit
  increment. Jump one trace */
  public int getUnitHeight() {
      return groupPanel.singlePanelSize.height;
  }

  /** Return the value, in pixels, to be used as the vertical scrolling block
  increment. Jump one screen minus one trace */
  public int getBlockHeight() {
      return getViewport().getHeight() - groupPanel.singlePanelSize.height;
  }

  private void setWFGroupPanel(WFGroupPanel panel) {

    if (groupPanel != null) groupPanel.removeComponentListener(groupPanelCompListener);

    groupPanel = panel;

    /*
    groupPanel.addComponentListener(
        new ComponentAdapter() {
            public void componentResized(ComponentEvent e) {
                setPanelsInView(panelsVisible); // instead of paintComponent -aww 2009/04/02

                //JScrollBar vbar = getVerticalScrollBar();
                //vbar.setUnitIncrement(getUnitHeight());
                //vbar.setBlockIncrement(getBlockHeight());
            }
        }
    );
    */

    this.addComponentListener(
        new ComponentAdapter() {
            public void componentResized(ComponentEvent e) {
                //System.out.println("..... listener for wfScroller resize.......");
                if (triaxialMode) setPanelsInView(Math.min(triaxPanelsInView, viewsPerPage)); // -aww 2010/01/25
                else setPanelsInView(viewsPerPage); // instead of paintComponent below -aww 2009/04/02
            }
        }
    );

    groupPanel.addComponentListener(groupPanelCompListener);

    // NOTE: need to set after component listener full time view toggle to work 
    jvp = new ChannelLabelViewport(this);
    setViewport(jvp);
    getViewport().setView(groupPanel);
    // call below scales the groupPanel to fit requested number of WFPanels in the viewport -aww 2009/04/02
    setPanelsInView(viewsPerPage);

  }

  class GroupPanelComponentListener extends ComponentAdapter {
            public void componentResized(ComponentEvent e) {
                SwingUtilities.invokeLater( new Runnable() {
                    public void run() {
                        //System.out.println("..... listener for groupPanel resize.......");
                        centerViewportOnTime(groupPanel.wfpList.getSelectedPanel());
                    }
                });
            }
  }

  public WFGroupPanel getWFGroupPanel() {
      return groupPanel;
  }

  /* Don't need to override paintComponent - REMOVED 2009/04/02 -aww 
  // Override paint() so that vertical scrollbar increments will be reset if the panel is resized 
  public void paintComponent(Graphics g) {

    //JScrollBar vbar = getVerticalScrollBar(); // moved to component adapter resize listener - 01/29/2007  aww
    //vbar.setUnitIncrement(getUnitHeight());   // moved to component adapter resize listener
    //vbar.setBlockIncrement(getBlockHeight()); // moved to component adapter resize listener

    // resizes WFGroupPanel wfpanels 
    setPanelsInView(viewsPerPage); // THIS is needed here to see the scroller display, not sure why though -aww

    // now do normal repaint
    super.paintComponent(g);
  }
  */

  /**
   * Return the index of the wfPanel at the top of the viewport
   */
  public int getTopIndex() {
      return (getScrollPixelsPerPanel() <= 0) ? 0 : (int)Math.round((double)getVerticalScrollBar().getValue()/(double)getScrollPixelsPerPanel());
  }

  /**
   * Set the top visible WFPanel to this index value of the WFGroupPanel. The move is
   * done via the scrollbar so that the scroll bar stays in synch.
   */
  public void setTopIndex(int index) {
    getVerticalScrollBar().setValue(((index < 0) ? 0 : index) * getScrollPixelsPerPanel());
  }

  /**
   * Set the center visible WFPanel to this index value of the
   * WFGroupPanel. The move is done via the scrollbar so that the scroll bar
   * stays in synch.  */
  public void setCenterIndex(int index) {
    int idx = index - (viewsPerPage/2);
    if (idx < 0) idx = 0;
    setTopIndex(idx);
  }


  /** Return how many scroll bar pixels represent on WFPanel in the viewport */
  public int getScrollPixelsPerPanel() {
    JScrollBar vbar = getVerticalScrollBar();
    int cnt = groupPanel.getComponentCount();
    return (cnt == 0) ? 0 : (int) ((double)vbar.getMaximum()/(double)cnt);
  }

  /**
   * Make this WFPanel visible in the viewPort. If it's already visible do nothing.
   */
  public void makePanelVisible(WFPanel wfp) {
      makePanelVisible(wfp, 0);
  }

  public void makePanelVisible(final WFPanel wfp, final int mode) {
    if (wfp == null) {
        System.err.println("WFScroller ERROR: makePanelVisible input wfp is NULL!");
        return;
    }
    //if (wfp.wfv != null) System.out.println("DEBUG WFScroller makePanelVisible mode: " + mode + " for:" + wfp.wfv.getChannelObj().toString());
    //if (panelIsVisible(wfp)) {
        //System.out.println(">> DEBUG WFScroller makePanelVisible WFP IS already VISIBLE!"); 
        //return;
    //}

    if (triaxialMode) { // -aww 2010/01/25

        Dimension vps = null;

        Object obj = mv.getOwner();
        if (obj instanceof WaveformDisplayIF) {
            WaveformDisplayIF wfd = (WaveformDisplayIF) obj;
            vps = wfd.getZoomPanel().bsvport.getExtentSize();
        }

        final TimeSpan ts = mv.masterWFWindowModel.getTimeSpan();
        double dur = ts.getDuration();

        double newSecs = 0.;
        if (vps == null) {
            newSecs = dur;
        }
        else {  // scroller viewport wider than zoom viewport so....
            double pixPerSec = vps.width/dur;
            vps = getViewport().getExtentSize(); // whats showing?
            newSecs = vps.width/pixPerSec;
        }

        if (Math.abs(newSecs - secondsInViewport) > .001) {
            //System.out.println("     WFScroller makePanelVisible old: " + newSecs + " new : " +  secondsInViewport); 
            secondsInViewport = newSecs;
            setPanelsInView(Math.min(triaxPanelsInView, viewsPerPage)); // triaxialMode displays only 3 panels in group panel scroller viewport
        }
            Runnable runner = new Runnable() {
              public void run() {
                Runnable task = new Runnable() {
                  public void run() {
                    //makePanelVisible(wfp, mode);
                    centerViewportOnTime(wfp); // try to match zoom window time
                    /*
                    Point p =getViewport().getViewPosition(); 
                    //System.out.println("WFScroller makePanelVisible set start time to: " + org.trinet.util.LeapSeconds.trueToString(ts.getStart()));
                    p.x = wfp.pixelOfTime(ts.getStart());
                    getViewport().setViewPosition(p);
                    */
                  }
                };
                try {
                  Thread.sleep(200); // Sleep millisec
                  EventQueue.invokeLater(task);
                } catch (InterruptedException ie) {
                  // ignore
                  return;
                }
              }
            };
            // Start delayed task
            new Thread(runner).start();

        return;
    }
    else if (mode > 0 && mv.getAlignmentMode() != MasterView.AlignOnTime) {
       Solution sol = mv.getSelectedSolution();
       if (sol != null && !sol.isDummy() && sol.hasLatLonZ()) {
         TravelTime tt = TravelTime.getInstance();
         double dist = wfp.wfv.getHorizontalDistance(); 
         if (dist != Channel.NULL_DIST) {
           double centerTime = mv.masterWFWindowModel.getCenterTime();
           if (mv.getAlignmentMode() == MasterView.AlignOnP) {
             centerTime = mv.getSelectedSolution().getTime()+tt.getTTp(dist, sol.mdepth.doubleValue()); // use mdepth -aww 2015/10/10
           }
           else if (mv.getAlignmentMode() == MasterView.AlignOnS) {
             centerTime = mv.getSelectedSolution().getTime()+tt.getTTs(dist, sol.mdepth.doubleValue()); // use mdepth -aww 2015/10/10
           }
           else if (mv.getAlignmentMode() == MasterView.AlignOnV) {
             centerTime = mv.getSelectedSolution().getTime()+dist/mv.alignmentVelocity;
           }
           //System.out.println("WFScroller makePanelVisible set center time to: " + org.trinet.util.LeapSeconds.trueToString(centerTime));
           if (centerTime != mv.masterWFWindowModel.getCenterTime()) mv.masterWFWindowModel.setCenterTime(centerTime);
         }
       }
    }
    //
    if (scrollerMode == SCROLL_TO_CENTER) {
      centerViewportOnPanel(wfp);
    }
    else {
      if (wfp.isShowing() && panelIsVisible(wfp)) {
          return; // only move is necessary
      }

      Rectangle rec = groupPanel.getVisibleRect();
      if (rec.height == 0 && rec.width == 0) {
          return;
      }
      TimeSpan ts = mv.masterWFWindowModel.getTimeSpan();
      rec = new Rectangle(wfp.pixelOfTime(ts.getStart()), 0, (int)(ts.getDuration()*wfp.getPixelsPerSecond()), wfp.getHeight());
      wfp.scrollRectToVisible(rec);

      //System.out.println("Scrolled viewRect: " + getViewport().getViewRect());
      //System.out.println();
      //groupPanel.scrollRectToVisible(new Rectangle(wfp.getX(),wfp.getY(),wfp.getWidth(),wfp.getHeight());
    }
  }


  /** Set the width of the viewport in seconds. If 'secs' <= 0.0 then the width
   *  is set to the legth of the data. Note that if the value set here is greater
   *  than the duration of the data in the view getSecondsInViewport() will return
   *  the data duration rather than this value. This prevents a "short" view with
   *  gray space at the end. */
  public void setSecondsInViewport(double secs) {
      setShowFullTime((secs <= 0.0));
      secondsInViewport = secs;
  }

  /** Return the width of the Viewport in seconds. This value is set with
  * setSecondsInViewport(). If the value passed to setSecondsInViewport() is <= 0
  * this method returns the width of the MasterView's viewspan.
  * The returned value will never be greater than the duration of the data even if
  * setSecondsInViewport() is used to set it to a larger value.
  */
  public double getSecondsInViewport() {
      // below should work ok:
      //return (getShowFullTime()) ?  mv.getViewSpan().getDuration() : Math.min(Math.abs(secondsInViewport), mv.getViewSpan().getDuration());
      // But just in case NaN:
      double aa = Math.abs(secondsInViewport);
      if (Double.isNaN(aa)) aa = 999.;
      double bb = mv.getViewSpan().getDuration(); 
      if (Double.isNaN(bb)) bb = 999.;
      return (getShowFullTime()) ?  bb : Math.min(aa, bb);
  }

  /** Set true to show the fill time series in the window. */
  public void setShowFullTime(boolean tf) {
    showFullTime = tf;
    secondsInViewport = (showFullTime) ? -Math.abs(secondsInViewport) : Math.abs(secondsInViewport);
  }

  /** Returns true if flag is set to show the fill time series in the window. */
  public boolean getShowFullTime() {
    return showFullTime;
  }
  /** Set true to show the WFPanel row headers in the scroller. */
  public void setShowRowHeader(boolean tf) {

    if (getShowRowHeader() == tf) return;    // no change

    showRowHeader = tf;
    if (getShowRowHeader()) {
      setRowHeaderView(getWFGroupPanel().getRowHeaderPanel());
    } else {
      setRowHeaderView(null);
    }
  }
  /** Returns true if flag is set to show the WFPanel row headers in the scroller. */
  public boolean getShowRowHeader() {
    return showRowHeader;
  }

  public int getChannelLabelMode() {
      return channelLabelMode;
  }

  /**
   * Center this WFPanel in the viewPort. Sets as the selected view.
   */

  public void centerViewportOnPanel(WFPanel wfp) {

    if (wfp == null) return;

    Point viewXY = getViewport().getViewPosition();        // get ViewPort origin
    int unitH = getUnitHeight();
    // Center in panels in view
    int cnt = (viewsPerPage+1)/2;
    cnt *= 2;
    int halfway = wfp.getY()-cnt*unitH/2;

    if (halfway <= 0) {
        halfway = 0; // near top can't scroll trace down
    }
    //else if (halfway >= (groupPanel.getHeight()-((viewsPerPage+1)*unitH)/2)) {
    else if (halfway >= groupPanel.getHeight()-viewsPerPage*unitH) {
        halfway = viewXY.y; // near bottom scroll cant' scroll trace up
    }

    //Point newPoint = new Point(viewXY.x, halfway);
    //getViewport().setViewPosition(newPoint);

    // Center selected window time
    TimeSpan ts = mv.masterWFWindowModel.getTimeSpan();
    
    int ix0 = wfp.pixelOfTime(ts.getStart());
    int ix1 = wfp.pixelOfTime(ts.getEnd());
    Dimension extent = getViewport().getExtentSize();
    int iviewx = ix0;
    if (iviewx > 0) {
      if (ix1-ix0 > extent.width) iviewx=ix0; // set to earliest window time pixel
      else iviewx = iviewx - ((extent.width)-(ix1-ix0))/2; // center in viewport width
    }
    if (iviewx < 0) iviewx = 0; // can't scroll before the start
    
    Dimension d = getViewport().getViewSize();
    if (iviewx > d.width-extent.width/2) iviewx = d.width-extent.width; // location after 1/2 extent width before ending view width
    Point newPoint = new Point(iviewx, halfway);
    getViewport().setViewPosition(newPoint);
    repaint();
  }

  public void centerViewportOnTime(WFPanel wfp) {

    if (wfp == null) return;

    Point viewXY = getViewport().getViewPosition();        // get ViewPort origin

    // Center selected window time
    TimeSpan ts = mv.masterWFWindowModel.getTimeSpan();
    
    int ix0 = wfp.pixelOfTime(ts.getStart());
    int ix1 = wfp.pixelOfTime(ts.getEnd());
    //System.out.println("centerViewportOnTime mv time span: " + ts + " mv window time pixels: " + ix0 + ", " + ix1);
    //System.out.println("      wfp pixelsPerSec: " + wfp.getPixelsPerSecond());
    //System.out.println("      viewXY origin: " + viewXY + " secs: " + viewXY.x/wfp.getPixelsPerSecond() + " wfp.view start: " + wfp.wfv.viewSpan);
    Dimension extent = getViewport().getExtentSize();
    //System.out.println("      view extent: " + extent);
    int iviewx = ix0;
    if (iviewx > 0) {
      if (ix1-ix0 > extent.width) iviewx=ix0; // set to earliest window time pixel
      else iviewx = iviewx - ((extent.width)-(ix1-ix0))/2; // center in viewport width
    }
    //System.out.println("centerViewportOnTime iviewx:"+ iviewx);
    if (iviewx < 0) iviewx = 0; // can't scroll before the start
    
    Dimension d = getViewport().getViewSize();
    if (iviewx > d.width-extent.width/2) iviewx = d.width-extent.width; // location after 1/2 extent width before ending view width
    //System.out.println("centerViewportOnTime iviewx:"+ iviewx);
    Point newPoint = new Point(iviewx, viewXY.y);
    getViewport().setViewPosition(newPoint);
    repaint();
  }

  /**
   * Center this Channel's WFPanel in the viewPort.
   */

  public void centerViewportOnPanel(Channel chan) {

    WFView wfview = mv.wfvList.get(chan);

    if (wfview == null) return;

    WFPanel wfp = groupPanel.wfpList.get(wfview);

    if (wfp == null) return;

    centerViewportOnPanel(wfp);

  }

  /** Return true if the given WFPanel is visible in the viewport */
  public boolean panelIsVisible(WFPanel wfp) {

    Rectangle viewRect = getViewport().getViewRect();                // whats showing?
    TimeSpan ts = mv.masterWFWindowModel.getTimeSpan();
    int ix1 = wfp.pixelOfTime(ts.getStart());
    int ix2 = wfp.pixelOfTime(ts.getEnd());
    if ( viewRect.contains(ix1, wfp.getY() ) &&                        // top corner
         viewRect.contains(ix2, wfp.getY()+ wfp.getHeight() )        // bottom corner
         ) return true;

    return false;

  }

  // Buttons for Filter Scaling
    JMenuItem jbFilterCustom = null;
    JMenuItem jbScaleUp = null;
    JMenuItem jbScaleDown = null;
    JMenuItem jbScaleOff = null;

    JRadioButtonMenuItem jbScrollSynch = null;

    JRadioButtonMenuItem jbFilterOff = null;
    JRadioButtonMenuItem jbFilterHP = null;
    JRadioButtonMenuItem jbFilterBP = null;
    JRadioButtonMenuItem jbFilterWA = null;
    JRadioButtonMenuItem jbFilterWA_HP = null;
    JRadioButtonMenuItem jbFilterWA_BP = null;
    JRadioButtonMenuItem jbFilterWA_EH = null;
    JRadioButtonMenuItem jbFilterVEL = null;
    JRadioButtonMenuItem jbFilterACC = null;
    JRadioButtonMenuItem jbFilterDIS = null;
    JRadioButtonMenuItem jbFilterSP03 = null;
    JRadioButtonMenuItem jbFilterSP10 = null;
    JRadioButtonMenuItem jbFilterSP30 = null;
    JRadioButtonMenuItem jbFilterEHG = null;

private class ScaleFilterMenuListener implements ActionListener {
    public void actionPerformed(ActionEvent evt) {
        int imods = evt.getModifiers(); // SHIFT = 1,  CTRL = 2,  ALT = 8
        String cmd = evt.getActionCommand();
        if ( cmd.startsWith("Filter") && ((imods & ActionEvent.SHIFT_MASK) == ActionEvent.SHIFT_MASK) ) {
            SwingUtilities.invokeLater( new Runnable() {
               public void run() {
                   jbFilterOff.doClick();
               }
            });
        }
        else doActionCmd2(evt.getActionCommand(), false);
    }

}

class ScaleFilterMenu extends JPopupMenu {

    ScaleFilterMenu() {
      super();

      ScaleFilterMenuListener scaleFilterActionListener = new ScaleFilterMenuListener();

      JMenuItem mi = new JMenuItem("Return");
      mi.setActionCommand("ReturnF");
      mi.addActionListener(scaleFilterActionListener);
      add(mi);
      addSeparator();

      boolean makeEHGFilterItem = false;
      if (mv.getOwner() instanceof Jiggle) {

           makeEHGFilterItem = ((Jiggle)mv.getOwner()).getProperties().getBoolean("addEHGFilterButton"); 
          
          jbScrollSynch = new JRadioButtonMenuItem("Triaxial group scroll synch");
          jbScrollSynch.setSelected(WFScroller.triaxialScrollZoomWithGroup);
          jbScrollSynch.addActionListener(new ActionListener() {
              public void actionPerformed(ActionEvent evt) {
                  boolean tf = jbScrollSynch.isSelected();
                  if (tf && getShowRowHeader()) {
                      JOptionPane.showMessageDialog(getTopLevelAncestor(), 
                          "First, disable channel headers (main menu view wave menu item) to enable scroll synch", 
                               "Triaxial Group Panel Scroller Time Synch",
                          JOptionPane.PLAIN_MESSAGE);
                      jbScrollSynch.setSelected(false);
                      return;
                  }
                  triaxialScrollZoomWithGroup = tf;
                  Object obj = mv.getOwner();
                  if (obj instanceof WaveformDisplayIF) {
                      ((WaveformDisplayIF)obj).getWfPanelProperties().setProperty("triaxialScrollZoomWithGroup", triaxialScrollZoomWithGroup);
                  }
              }
          });
          jbScrollSynch.setToolTipText("group panel horizontal scroll also scrolls upper zoom panel window");
          add(jbScrollSynch);
          addSeparator();
      }

      jbScaleOff = new JMenuItem("Scale Off");  // added scaling 2009/03/26 -aww
      jbScaleOff.addActionListener(scaleFilterActionListener);
      jbScaleOff.setToolTipText("Scale panel amps 1x");
      add(jbScaleOff);

      jbScaleUp = new JMenuItem("Scale Up");
      jbScaleUp.addActionListener(scaleFilterActionListener);
      jbScaleUp.setToolTipText("Scale panel amps x 2");
      add(jbScaleUp);

      jbScaleDown = new JMenuItem("Scale Down");
      jbScaleDown.addActionListener(scaleFilterActionListener);
      jbScaleDown.setToolTipText("Scale panel amps x 1/2");
      add(jbScaleDown);
      addSeparator();

      //JMenu filter = new JMenu("Filter...", true);
      JPopupMenu filter = this;

      jbFilterCustom = new JMenuItem("Customize filter...");
      jbFilterCustom.addActionListener(scaleFilterActionListener);
      jbFilterCustom.setSelected(false);
      jbFilterCustom.setEnabled(false);
      filter.add(jbFilterCustom);
      filter.addSeparator();

      ButtonGroup bg = new ButtonGroup();
      jbFilterOff = new JRadioButtonMenuItem("Filter Off");
      jbFilterOff.setToolTipText("Pressing SHIFT and clicking on any filter button also turns off filtering");
      boolean tf = (groupPanel != null && groupPanel.wfpList != null  && groupPanel.wfpList.size() > 0);
      jbFilterOff.setSelected( (tf) ? ! ((WFPanel)groupPanel.wfpList.get(0)).showAlternateWf : true ); // off ? true ? 
      jbFilterOff.addActionListener(scaleFilterActionListener);
      filter.add(jbFilterOff);
      filter.addSeparator();
      bg.add(jbFilterOff);

      jbFilterHP = new JRadioButtonMenuItem("FilterHP");
      jbFilterHP.addActionListener(scaleFilterActionListener);
      jbFilterHP.setSelected(false);
      bg.add(jbFilterHP);
      filter.add(jbFilterHP);

      jbFilterBP = new JRadioButtonMenuItem("FilterBP");
      jbFilterBP.addActionListener(scaleFilterActionListener);
      jbFilterBP.setSelected(false);
      bg.add(jbFilterBP);
      filter.add(jbFilterBP);
      filter.addSeparator();

      jbFilterWA = new JRadioButtonMenuItem("FilterWA");
      jbFilterWA.addActionListener(scaleFilterActionListener);
      jbFilterWA.setSelected(false);
      bg.add(jbFilterWA);
      filter.add(jbFilterWA);

      jbFilterWA_HP = new JRadioButtonMenuItem("FilterWA_HP");
      jbFilterWA_HP.addActionListener(scaleFilterActionListener);
      jbFilterWA_HP.setSelected(false);
      bg.add(jbFilterWA_HP);
      filter.add(jbFilterWA_HP);

      jbFilterWA_BP = new JRadioButtonMenuItem("FilterWA_BP");
      jbFilterWA_BP.addActionListener(scaleFilterActionListener);
      jbFilterWA_BP.setSelected(false);
      bg.add(jbFilterWA_BP);
      filter.add(jbFilterWA_BP);

      jbFilterWA_EH = new JRadioButtonMenuItem("FilterWA_EH");
      jbFilterWA_EH.addActionListener(scaleFilterActionListener);
      jbFilterWA_EH.setSelected(false);
      bg.add(jbFilterWA_EH);
      filter.add(jbFilterWA_EH);
      filter.addSeparator();

      jbFilterACC = new JRadioButtonMenuItem("FilterACC");
      jbFilterACC.addActionListener(scaleFilterActionListener);
      jbFilterACC.setSelected(false);
      bg.add(jbFilterACC);
      filter.add(jbFilterACC);
      jbFilterVEL = new JRadioButtonMenuItem("FilterVEL");
      jbFilterVEL.addActionListener(scaleFilterActionListener);
      jbFilterVEL.setSelected(false);
      bg.add(jbFilterVEL);
      filter.add(jbFilterVEL);
      jbFilterDIS = new JRadioButtonMenuItem("FilterDIS");
      jbFilterDIS.addActionListener(scaleFilterActionListener);
      jbFilterDIS.setSelected(false);
      bg.add(jbFilterDIS);
      filter.add(jbFilterDIS);

      filter.addSeparator();
      jbFilterSP03 = new JRadioButtonMenuItem("FilterSP03");
      jbFilterSP03.addActionListener(scaleFilterActionListener);
      jbFilterSP03.setSelected(false);
      bg.add(jbFilterSP03);
      filter.add(jbFilterSP03);
      jbFilterSP10 = new JRadioButtonMenuItem("FilterSP10");
      jbFilterSP10.addActionListener(scaleFilterActionListener);
      jbFilterSP10.setSelected(false);
      bg.add(jbFilterSP10);
      filter.add(jbFilterSP10);
      jbFilterSP30 = new JRadioButtonMenuItem("FilterSP30");
      jbFilterSP30.addActionListener(scaleFilterActionListener);
      jbFilterSP30.setSelected(false);
      bg.add(jbFilterSP30);
      filter.add(jbFilterSP30);

      if ( makeEHGFilterItem ) { 
          addSeparator();
          jbFilterEHG = new JRadioButtonMenuItem("FilterEHG");
          jbFilterEHG.addActionListener(scaleFilterActionListener);
          jbFilterEHG.setSelected(false);
          bg.add(jbFilterEHG);
          filter.add(jbFilterEHG);
      }

      //add(filter);
      addSeparator();

      mi = new JMenuItem("Return");
      mi.setActionCommand("ReturnF");
      mi.addActionListener(scaleFilterActionListener);
      add(mi);
      //addSeparator();
    }
}

// Buttons to Hide panels
    JMenuItem jbRefresh = null;
    JMenuItem jbShowAll = null;

    JRadioButton jbUnselected = null;
    JRadioButton jbSelected = null;
    ButtonGroupS bgSel = null;

    JRadioButton jbUnclipped = null;
    JRadioButton jbClipped = null;
    ButtonGroupS bgClp = null;

    JRadioButton jbVert = null;
    JRadioButton jbHorz = null;
    JRadioButton jbNorth = null;
    JRadioButton jbEast = null;

    JRadioButton jbA = null;
    JRadioButton jbNA = null;
    ButtonGroupS bgAmp = null;

    JRadioButton jbNPA = null;
    JRadioButton jbNP = null;
    JRadioButton jbP = null;
    ButtonGroupS bgPick = null;

    JRadioButton jbNC = null;
    JRadioButton jbC = null;
    ButtonGroupS bgCoda = null;

    JRadioButton jbHG = null;
    JRadioButton jbLG = null;
    ButtonGroupS bgGain = null;

    JRadioButton jbVel = null;
    JRadioButton jbAcc = null;
    ButtonGroupS bgVA = null;

    JRadioButton jbNoTT = null;
    JRadioButton jbEHG = null;

    //ButtonGroupS bgWf = null;
    //JRadioButton jbWf = null;
    JRadioButton jbNoWf = null;

    JRadioButton jbB = null;
    JRadioButton jbE = null;
    JRadioButton jbH = null;
    JRadioButton jbS = null;
    JRadioButton jbL = null;
    JRadioButton jbD = null;

    JRadioButton jbShowB = null;
    JRadioButton jbShowE = null;
    JRadioButton jbShowH = null;
    JRadioButton jbShowS = null;
    JRadioButton jbShowL = null;
    JRadioButton jbShowD = null;

    ArrayList iHideList = new ArrayList(25);

    HideButtonPanel hideButtonPanel = null;

class ButtonGroupS extends ButtonGroup {

    // This hidden radio button is used for "unselecting any other" added to this same group
    final JRadioButton jrb = new JRadioButton();

    public ButtonGroupS() {
        super();
        add(jrb);
    }

    public void clearSelection() {
        setSelected(jrb.getModel(), true);
        //jrb.setSelected(true);
    }
}

private class HideTracesMenuListener implements ActionListener {

    public void actionPerformed(ActionEvent e) {
        doActionCmd(e.getActionCommand(), false);
    }

}

class HideMenu extends JPopupMenu {

    HideMenu() {

      HideTracesMenuListener hideTracesActionListener = new HideTracesMenuListener();

      JMenuItem mi = new JMenuItem("Return");
      mi.setActionCommand("ReturnH");
      mi.addActionListener(hideTracesActionListener);
      add(mi);
      addSeparator();

      jbTriaxial = new JRadioButtonMenuItem("Show Triaxial");
      jbTriaxial.addActionListener(hideTracesActionListener);
      jbTriaxial.setToolTipText("Show only triaxial components associated with zoom panel");
      add(jbTriaxial);
      iHideList.add(jbTriaxial);
      addSeparator();

      if (mv.getOwner() instanceof Jiggle) {
          HIDE_EHG = ((Jiggle)mv.getOwner()).getProperties().getBoolean("addEHGFilterButton"); 
          jbScrollSynch = new JRadioButtonMenuItem("Triaxial group scroll synch");
          jbScrollSynch.setSelected(WFScroller.triaxialScrollZoomWithGroup);
          jbScrollSynch.addActionListener(new ActionListener() {
              public void actionPerformed(ActionEvent evt) {
                  boolean tf = jbScrollSynch.isSelected();
                  if (tf && getShowRowHeader()) {
                      JOptionPane.showMessageDialog(getTopLevelAncestor(), 
                          "First, disable channel headers (main menu view wave menu item) to enable scroll synch", 
                               "Triaxial Group Panel Scroller Time Synch",
                          JOptionPane.PLAIN_MESSAGE);
                      jbScrollSynch.setSelected(false);
                      return;
                  }
                  triaxialScrollZoomWithGroup = tf;
                  Object obj = mv.getOwner();
                  if (obj instanceof WaveformDisplayIF) {
                      ((WaveformDisplayIF)obj).getWfPanelProperties().setProperty("triaxialScrollZoomWithGroup", triaxialScrollZoomWithGroup);
                  }
              }
          });
          jbScrollSynch.setToolTipText("group panel horizontal scroll also scrolls upper zoom panel window");
          add(jbScrollSynch);
          addSeparator();
      }

      jbShowAll = new JMenuItem("Show all");
      jbShowAll.setToolTipText("Make hidden panels visible again");
      jbShowAll.addActionListener(hideTracesActionListener);
      add(jbShowAll);
      addSeparator();

      //jbHideAgain = new JMenuItem("Hide");
      jbRefresh = new JMenuItem("Hide/Refresh"); 
      jbRefresh.setToolTipText("Update hiding of panels after a status change");
      jbRefresh.addActionListener(hideTracesActionListener);
      add(jbRefresh);
      addSeparator();

      //mi.addActionListener(hideTracesActionListener);
      //add(mi);
      //addSeparator();

      mi = new JMenuItem("Hide setup...");
      mi.addActionListener(
         new ActionListener() {

             public void actionPerformed(ActionEvent evt) {
                 // Popup a dialog panel with detailed selection buttons
                 //int ans = JOptionPane.showConfirmDialog(getTopLevelAncestor(), new HideButtonPanel(),
                 //   "Select Panel Types To Hide in Group Viewport",
                 //    JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE);
                 // After dialog returns "Ok" or "Yes" then do hiding action
                 //if (ans == JOptionPane.YES_OPTION) {
                 //    doActionCmd(evt.getActionCommand(), false);
                 //}

                 if (jdHide != null) {
                     jdHide.setVisible(true);
                     jdHide.toFront();
                     return;
                 }

                 jdHide = new JDialog( (Frame)
                     getTopLevelAncestor(),
                     "Select Panel Types To Hide in Group View",
                     false
                 );
                 Object obj = mv.getOwner();
                 if (obj instanceof Window) {
                     Window w = (Window) obj;
                     w.addWindowListener( new WindowAdapter() {
                        public void windowClosing() {
                            if (jdHide != null) {
                                jdHide.dispose();
                                jdHide = null;
                            }
                        } 
                     });
                 }

                 Box hbox = Box.createHorizontalBox();
                 hbox.add(Box.createHorizontalGlue());
                 JButton okButton = new JButton("Hide(Open)");
                 okButton.setToolTipText("Hide panels matching selected types but keep dialog open");
                 okButton.addActionListener(new ActionListener() {
                     public void actionPerformed(ActionEvent evt) {
                         WFScroller.this.doActionCmd("Hide...", false);
                     }
                 });
                 hbox.add(okButton);

                 JButton closeButton = new JButton("Hide(Close)");
                 closeButton.setToolTipText("Hide panels matching selected types and close dialog");
                 closeButton.addActionListener(new ActionListener() {
                     public void actionPerformed(ActionEvent evt) {
                         //SwingUtilities.invokeLater(new Runnable() {
                         //   public void run() {
                                jdHide.dispose();
                         //    }
                         // });
                         WFScroller.this.doActionCmd("Hide...", false);
                     }
                 });
                 hbox.add(closeButton);

                 JButton clearButton = new JButton("Clear");
                 clearButton.setToolTipText("Unset hide selections");
                 clearButton.addActionListener(new ActionListener() {
                     public void actionPerformed(ActionEvent evt) {
                         WFScroller.this.unsetButtons();
                         Object obj = WFScroller.this.mv.getOwner();
                         if (obj instanceof Jiggle) {
                           Jiggle jiggle = (Jiggle) obj;
                           ((Jiggle)jiggle).toolBar.resetHidePanelToggleButtons();
                         }
                     }
                 });
                 hbox.add(clearButton);
                 hbox.add(Box.createHorizontalGlue());

                 if (hideButtonPanel == null) hideButtonPanel = new HideButtonPanel();
                 hideButtonPanel.add(hbox,BorderLayout.SOUTH);
                 jdHide.setContentPane(hideButtonPanel);
                 jdHide.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
                 jdHide.pack();
                 jdHide.setLocationRelativeTo(hideButton);
                 jdHide.setVisible(true);
             }
         }
      );
      add(mi);
      addSeparator();

      mi = new JMenuItem("Return");
      mi.setActionCommand("ReturnH");
      mi.addActionListener(hideTracesActionListener);
      add(mi);

      //addSeparator();

    }
}

class UnsetGroupListener implements ActionListener {
    ButtonGroupS bgs = null;
    public UnsetGroupListener(ButtonGroupS bg) {
        bgs = bg;
    }
    public void actionPerformed(ActionEvent evt) {
        int imods = evt.getModifiers();
        // SHIFT = 1,  CTRL = 2,  ALT = 8
        //System.out.println("DEBUG PickingPanel unset button group action evt modifiers: " + imods);
        if ( (imods & ActionEvent.SHIFT_MASK) == ActionEvent.SHIFT_MASK ) {
            bgs.clearSelection();
        }
    }
}

class HideButtonPanel extends JPanel {

    HideButtonPanel() {
      // Hide Panel By Type Filter buttons
      setLayout(new BorderLayout());

      add(new JLabel("NOTE: For some groups, to clear item, press SHIFT+item"), BorderLayout.NORTH);

      Box hbox = Box.createHorizontalBox();
      hbox.setBorder(BorderFactory.createTitledBorder("Hide Panels"));
      Box vbox = Box.createVerticalBox();
      ButtonGroup bg = null;

      //!!! NOTE: Add a ButtonGroup for every group of buttons between separators !!!

      jbVert = new JRadioButton("Vert");
      jbHorz = new JRadioButton("Horiz");
      jbNorth = new JRadioButton("North");
      jbEast = new JRadioButton("East");
      //jbVert.addActionListener(ugl);
      jbVert.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent evt) {
              if (jbVert.isSelected()) jbHorz.setSelected(false);
          }
      });
      vbox.add(jbVert);
      iHideList.add(jbVert);
      //jbHorz.addActionListener(ugl);
      jbHorz.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent evt) {
              if (jbHorz.isSelected()) {
                  jbVert.setSelected(false);
                  jbEast.setSelected(false);
                  jbNorth.setSelected(false);
              }
          }
      });
      vbox.add(jbHorz);
      iHideList.add(jbHorz);
      addJSeparator(vbox);

      //jbNorth.addActionListener(ugl);
      jbNorth.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent evt) {
              if (jbNorth.isSelected() && jbHorz.isSelected()) jbHorz.setSelected(false);
          }
      });
      vbox.add(jbNorth);
      iHideList.add(jbNorth);

      //jbEast.addActionListener(ugl);
      jbEast.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent evt) {
              if (jbEast.isSelected() && jbHorz.isSelected()) jbHorz.setSelected(false);
          }
      });
      vbox.add(jbEast);
      iHideList.add(jbEast);
      addJSeparator(vbox);

      bgGain = new ButtonGroupS();
      UnsetGroupListener ugl = new UnsetGroupListener(bgGain);
      jbLG = new JRadioButton("Low gain");
      bgGain.add(jbLG);
      jbLG.setToolTipText("low gain seedchan _N_ _L_ or _G_");
      jbLG.addActionListener(ugl);
      vbox.add(jbLG);
      iHideList.add(jbLG);

      jbHG = new JRadioButton("High gain");
      bgGain.add(jbHG);
      jbHG.setToolTipText("high gain seedchan _H_");
      jbHG.addActionListener(ugl);
      vbox.add(jbHG);
      iHideList.add(jbHG);
      addJSeparator(vbox);

      bgVA = new ButtonGroupS();
      ugl = new UnsetGroupListener(bgVA);
      jbVel = new JRadioButton("Vel");
      bgVA.add(jbVel);
      jbVel.addActionListener(ugl);
      vbox.add(jbVel);
      iHideList.add(jbVel);

      jbAcc = new JRadioButton("Acc");
      bgVA.add(jbAcc);
      jbAcc.addActionListener(ugl);
      vbox.add(jbAcc);
      iHideList.add(jbAcc);
      addJSeparator(vbox);
      
// HIDE READING FILTER BUTTONS
      // For Scope app don't show this 
      Object obj = mv.getOwner();
      if (obj instanceof Jiggle) {

          bgPick = new ButtonGroupS();
          ugl = new UnsetGroupListener(bgPick);
          jbNPA = new JRadioButton("No A-Picks");
          jbNPA.setToolTipText("Panel has no auto pick");
          bgPick.add(jbNPA);
          jbNPA.setActionCommand("NoPicksA");
          jbNPA.addActionListener(ugl);
          iHideList.add(jbNPA);
          //nopicks.add(jbNPA);
          vbox.add(jbNPA);
          //readings.add(nopicks);

          //JMenu readings = new JMenu("Readings...", true);
          //JMenu nopicks = new JMenu("NoPicks");
          jbNP = new JRadioButton("No Picks");
          bgPick.add(jbNP);
          jbNP.setActionCommand("NoPicksB");
          jbNP.addActionListener(ugl);
          //nopicks.add(jbNP);
          vbox.add(jbNP);
          iHideList.add(jbNP);

          jbP = new JRadioButton("Picks");
          bgPick.add(jbP);
          jbP.addActionListener(ugl);
          vbox.add(jbP);
          iHideList.add(jbP);
          addJSeparator(vbox);

          bgAmp = new ButtonGroupS();
          ugl = new UnsetGroupListener(bgAmp);
          jbNA = new JRadioButton("No Amps");
          bgAmp.add(jbNA);
          jbNA.addActionListener(ugl);
          vbox.add(jbNA);
          iHideList.add(jbNA);
          jbA = new JRadioButton("Amps");
          bgAmp.add(jbA);
          jbA.addActionListener(ugl);
          vbox.add(jbA);
          iHideList.add(jbA);
          addJSeparator(vbox);

          bgCoda = new ButtonGroupS();
          ugl = new UnsetGroupListener(bgCoda);
          jbNC = new JRadioButton("No Codas");
          bgCoda.add(jbNC);
          jbNC.addActionListener(ugl);
          vbox.add(jbNC);
          iHideList.add(jbNC);
          jbC = new JRadioButton("Codas");
          bgCoda.add(jbC);
          jbC.addActionListener(ugl);
          vbox.add(jbC);
          iHideList.add(jbC);
          //add(readings);
      }

      //main Horiz box 1st column ?
      vbox.setAlignmentY(Component.TOP_ALIGNMENT);
      vbox.setBorder(BorderFactory.createLineBorder(Color.black));
      hbox.add(vbox);
      hbox.add(Box.createHorizontalStrut(5));

      vbox = Box.createVerticalBox();

      //JMenu seedchan = new JMenu("Seedchan...", true);
      bg = new ButtonGroup();
      jbB = new JRadioButton("B_");
      jbB.setToolTipText("all B.. seedchan");
      //jbB.addActionListener(ugl);
      vbox.add(jbB);
      bg.add(jbB);
      iHideList.add(jbB);

      JRadioButton jb = new JRadioButton("BL");
      //jb.addActionListener(ugl);
      vbox.add(jb);
      bg.add(jb);
      iHideList.add(jb);

      jb = new JRadioButton("BH");
      //jb.addActionListener(ugl);
      vbox.add(jb);
      bg.add(jb);
      iHideList.add(jb);

      jbShowB = new JRadioButton("Show B");
      //jbShowB.addActionListener(ugl);
      jbShowB.setSelected(true);
      bg.add(jbShowB);
      vbox.add(jbShowB);
      addJSeparator(vbox);

      bg = new ButtonGroup();
      jbE = new JRadioButton("E_");
      jbE.setToolTipText("all E.. seedchan");
      //jbE.addActionListener(ugl);
      vbox.add(jbE);
      bg.add(jbE);
      iHideList.add(jbE);

      jb = new JRadioButton("EL");
      //jb.addActionListener(ugl);
      vbox.add(jb);
      bg.add(jb);
      iHideList.add(jb);

      jb = new JRadioButton("EH");
      //jb.addActionListener(ugl);
      vbox.add(jb);
      bg.add(jb);
      iHideList.add(jb);

      jbShowE = new JRadioButton("Show E");
      //jbShowE.addActionListener(ugl);
      jbShowE.setSelected(true);
      bg.add(jbShowE);
      vbox.add(jbShowE);
      addJSeparator(vbox);

      bg = new ButtonGroup();
      jbH = new JRadioButton("H_");
      jbH.setToolTipText("all H.. seedchan");
      //jbH.addActionListener(ugl);
      vbox.add(jbH);
      bg.add(jbH);
      iHideList.add(jbH);

      jb = new JRadioButton("HL");
      //jb.addActionListener(ugl);
      vbox.add(jb);
      bg.add(jb);
      iHideList.add(jb);

      jb = new JRadioButton("HH");
      //jb.addActionListener(ugl);
      vbox.add(jb);
      bg.add(jb);
      iHideList.add(jb);

      jbShowH = new JRadioButton("Show H");
      //jbShowH.addActionListener(ugl);
      jbShowH.setSelected(true);
      bg.add(jbShowH);
      vbox.add(jbShowH);

      //main Horiz box 2nd column ?
      vbox.setAlignmentY(Component.TOP_ALIGNMENT);
      vbox.setBorder(BorderFactory.createLineBorder(Color.black));
      hbox.add(vbox);
      hbox.add(Box.createHorizontalStrut(5));

      vbox = Box.createVerticalBox();
      bg = new ButtonGroup();
      jbS = new JRadioButton("S_");
      jbS.setToolTipText("all S.. seedchan");
      //jbS.addActionListener(ugl);
      vbox.add(jbS);
      bg.add(jbS);
      iHideList.add(jbS);

      jb = new JRadioButton("SL");
      //jb.addActionListener(ugl);
      vbox.add(jb);
      bg.add(jb);
      iHideList.add(jb);

      jb = new JRadioButton("SH");
      //jb.addActionListener(ugl);
      vbox.add(jb);
      bg.add(jb);
      iHideList.add(jb);

      jbShowS = new JRadioButton("Show S");
      //jbShowS.addActionListener(ugl);
      jbShowS.setSelected(true);
      bg.add(jbShowS);
      vbox.add(jbShowS);
      addJSeparator(vbox);

      bg = new ButtonGroup();
      jbD = new JRadioButton("D_");
      jbD.setToolTipText("all D.. seedchan");
      //jbD.addActionListener(ugl);
      vbox.add(jbD);
      bg.add(jbD);
      iHideList.add(jbD);

      jbShowD = new JRadioButton("Show D");
      //jbShowD.addActionListener(ugl);
      jbShowD.setSelected(true);
      bg.add(jbShowD);
      vbox.add(jbShowD);
      addJSeparator(vbox);

      //
      bg = new ButtonGroup();
      jbL = new JRadioButton("L_");
      jbL.setToolTipText("all L.. seedchan");
      //jbL.addActionListener(ugl);
      vbox.add(jbL);
      bg.add(jbL);
      iHideList.add(jbL);

      jbShowL = new JRadioButton("Show L");
      //jbShowL.addActionListener(ugl);
      jbShowL.setSelected(true);
      bg.add(jbShowL);
      vbox.add(jbShowL);
      addJSeparator(vbox);
      //

      //main Horiz box 3rd column ?
      //vbox.setAlignmentY(Component.TOP_ALIGNMENT);
      //vbox.setBorder(BorderFactory.createLineBorder(Color.black));
      //hbox.add(vbox);
      //hbox.add(Box.createHorizontalStrut(5));

      bgSel = new ButtonGroupS();
      ugl = new UnsetGroupListener(bgSel);
      jbSelected = new JRadioButton("Selected");
      jbSelected.setToolTipText("Panel view is flagged selected");
      bgSel.add(jbSelected);
      jbSelected.addActionListener(ugl);
      vbox.add(jbSelected);
      iHideList.add(jbSelected);
      jbUnselected = new JRadioButton("Unselected");
      jbUnselected.setToolTipText("Panel view is not flagged selected");
      bgSel.add(jbUnselected);
      jbUnselected.addActionListener(ugl);
      vbox.add(jbUnselected);
      iHideList.add(jbUnselected);
      addJSeparator(vbox);

      bgClp = new ButtonGroupS();
      ugl = new UnsetGroupListener(bgClp);
      jbClipped = new JRadioButton("Clipped");
      jbClipped.setToolTipText("Panel waveform is flagged clipped");
      bgClp.add(jbClipped);
      jbClipped.addActionListener(ugl);
      vbox.add(jbClipped);
      iHideList.add(jbClipped);
      jbUnclipped = new JRadioButton("Unclipped");
      jbUnclipped.setToolTipText("Panel waveform is not flagged clipped");
      bgClp.add(jbUnclipped);
      jbUnclipped.addActionListener(ugl);
      vbox.add(jbUnclipped);
      iHideList.add(jbUnclipped);
      addJSeparator(vbox);

      if (HIDE_EHG) {
        jbEHG = new JRadioButton("EHG");
        jbEHG.setToolTipText("Gain scaling channel panel");
        vbox.add(jbEHG);
        iHideList.add(jbEHG);
        addJSeparator(vbox);
      }

      jbNoTT = new JRadioButton("No time gaps");
      jbNoTT.setToolTipText("Panel without time-tears, contiguous timeseries");
      vbox.add(jbNoTT);
      iHideList.add(jbNoTT);
      addJSeparator(vbox);

      //
      // NOTE: Hiding panels without wfs will not be implemented:
      //   All waveform views have non-null waveforms with a desired timespan to view.
      //   But WFCache mangager only loads a limited range of timeseries in list plus
      //   Wwaveservers are requeried too: may get data on a subsequent server poll
      // TO implement: you would have to get a list of time-ranges available from WS and see if
      //               the waveform's span is included. Polling waveserver has latency overhead.
      //
      //bgWf = new ButtonGroupS();
      //ugl = new UnsetGroupListener(bgWf);
      //jbWf = new JRadioButton("Wfs");
      //jbWf.setToolTipText("Panel with a timeseries");
      //bgWf.add(jbWf);
      //jbWf.addActionListener(ugl);
      //vbox.add(jbWf);
      //iHideList.add(jbWf);
      jbNoWf = new JRadioButton("No Wfs");
      jbNoWf.setToolTipText("Panels without a timeseries");
      //bgWf.add(jbNoWf);
      //jbNoWf.addActionListener(ugl);
      vbox.add(jbNoWf);
      iHideList.add(jbNoWf);
      addJSeparator(vbox);
      //

      //main Horiz box 4rd column ?
      vbox.setAlignmentY(Component.TOP_ALIGNMENT);
      vbox.setBorder(BorderFactory.createLineBorder(Color.black));
      hbox.add(vbox);

      hbox.add(Box.createHorizontalStrut(5));
      hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
      hbox.setAlignmentY(Component.TOP_ALIGNMENT);

      add(hbox, BorderLayout.CENTER);

    }

    private void addJSeparator(Box c) {
      //
      JSeparator jsep = new JSeparator(SwingConstants.HORIZONTAL);
      jsep.setPreferredSize(new Dimension(80,2));
      jsep.setMaximumSize(new Dimension(80,2));
      jsep.setAlignmentX(Component.LEFT_ALIGNMENT);
      jsep.setAlignmentY(Component.TOP_ALIGNMENT);
      c.add(jsep);
      //
    }
}

    protected void unsetButtons() {

        jbVert.setSelected(false);
        jbHorz.setSelected(false);
        jbNorth.setSelected(false);
        jbEast.setSelected(false);

        bgSel.clearSelection();
        //jbUnselected.setSelected(false);
        //jbSelected.setSelected(false);

        bgClp.clearSelection();
        //jbClipped.setSelected(false);
        //jbUnclipped.setSelected(false);

        bgPick.clearSelection();
        //jbNP.setSelected(false);
        //jbNPA.setSelected(false);
        //jbP.setSelected(false);

        bgAmp.clearSelection();
        //jbA.setSelected(false);
        //jbNA.setSelected(false);

        bgCoda.clearSelection();
        //jbC.setSelected(false);
        //jbNC.setSelected(false);

        bgGain.clearSelection();
        //jbHG.setSelected(false);
        //jbLG.setSelected(false);

        bgVA.clearSelection();
        //jbVel.setSelected(false);
        //jbAcc.setSelected(false);

        //bgWf.clearSelection();
        jbNoWf.setSelected(false);

        jbNoTT.setSelected(false);

        jbTriaxial.setSelected(false);

        jbShowB.setSelected(true);
        jbShowE.setSelected(true);
        jbShowH.setSelected(true);
        jbShowS.setSelected(true);
        jbShowD.setSelected(true);
        jbShowL.setSelected(true);

        jbEHG.setSelected(false);

        //jbRefresh.setEnabled(false);
        //jbShowAll.setEnabled(false);
        hiddenReadings = false; // try this reset ? -aww 2013/02/15 test
    }
    private void setButtons() {
        //jbUnselected.setSelected(true);
        //jbSelected.setSelected(true);
        //jbUnclipped.setSelected(true);
        //jbClipped.setSelected(true);
        //jbVert.setSelected(true);
        //jbHorz.setSelected(true);
        //jbNorth.setSelected(true);
        //jbEast.setSelected(true);

        //jbHG.setSelected(true);
        //jbLG.setSelected(true);
        //jbNoTT.setSelected(true);
        //jbVel.setSelected(true);
        //jbAcc.setSelected(true);

        jbB.setSelected(true);
        jbE.setSelected(true);
        jbH.setSelected(true);
        jbS.setSelected(true);
        jbD.setSelected(true);
        //jbL.setSelected(true);
    }

  private void setupFilter(WorkerStatusDialog workStatus) {
        WFPanelList wfpList = groupPanel.wfpList;
        int panelCnt = wfpList.size();
        if (panelCnt == 0) return;

        WFPanel wfp = null;
        wfp = (WFPanel) wfpList.get(0);
        if (wfp == null) return;

        FilterIF f = wfp.filter;    

        if (f instanceof NoiseFilterIF) {
          f = ((NoiseFilterIF)f).getNoiseFilter();
        }
        if (f == null) return;

        if (! (f instanceof BandpassFilterIF)) return;
        BandpassFilterIF bf = (BandpassFilterIF) f;

        NumberTextField f1 = NumberTextFieldFactory.createInputField(6, false, "0.##", getFont(), true);
        NumberTextField f2 = NumberTextFieldFactory.createInputField(6, false, "0.##", getFont(), true);
        JComboBox combo = new JComboBox(new Integer [] {
                    new Integer(1),new Integer(2),new Integer(3),new Integer(4), new Integer(5),
                    new Integer(6),new Integer(7),new Integer(8)
        });
        combo.setSelectedIndex(bf.getOrder()-1);
        Box b = Box.createHorizontalBox();
        f1.setValue(bf.getLoHz());
        f2.setValue(bf.getHiHz());
        b.add(new JLabel("LoHz:"));
        b.add(f1);
        if (bf.getType() == FilterTypes.BANDPASS) {
          b.add(new JLabel("HiHz:"));
          b.add(f2);
        }
        b.add(new JLabel("Order:"));
        b.add(combo);
        JPanel jp = new JPanel();
        jp.setLayout(new BorderLayout());
        jp.add(b, BorderLayout.CENTER);
        JCheckBox reverseChk = new JCheckBox("Reverse");
        reverseChk.setSelected(bf.isReversed());
        jp.add(reverseChk, BorderLayout.SOUTH);
        int ans = JOptionPane.showConfirmDialog(getTopLevelAncestor(), jp, FilterTypes.getType(bf.getType()), JOptionPane.YES_NO_OPTION);

        if (ans != JOptionPane.YES_OPTION) return;

        double loCut = f1.getDoubleValue();
        double hiCut = f2.getDoubleValue();
        int order = combo.getSelectedIndex()+1;
        boolean reverse  = reverseChk.isSelected();

        String desc = "";
        workStatus.pop("Filter", "Filtering data", true);
        for (int i = 0; i < panelCnt; i++) {
            wfp = (WFPanel) wfpList.get(i);
            f = wfp.filter;
            if (f instanceof NoiseFilterIF) {
              if (f instanceof WAFilter) desc = "WAS_";
              f = ((NoiseFilterIF)f).getNoiseFilter();
            }
            if (f instanceof ButterworthFilterSMC) { 
                bf = (ButterworthFilterSMC) f;
                bf.setFilter(bf.getSampleRate(), loCut, hiCut, order, reverse); 
                bf.setDescription(desc + bf.getDescription());
            }
            wfp.setFilter(bf);
        }
    }


    protected void doActionCmd(String command, final boolean loadWF) {

        final String cmd = command; 

        if (cmd.startsWith("Return")) return; // no-op close popup

        groupPanel.setVisible(false); // hide group panel to reduce graphics updates flashing?
        if (getShowRowHeader()) getRowHeader().getView().setVisible(false);

        org.trinet.util.SwingWorker worker = new org.trinet.util.SwingWorker() {

          WorkerStatusDialog workStatus = new WorkerStatusDialog((Frame)mv.getOwner(), true);

          public Object construct() {
            //WFScroller.this.setRowHeaderView(null); // added to disable row header -aww 2009/03/25
            //WFScroller.this.showRowHeader = false; // added to disable row header -aww 2009/03/25

            workStatus.pop("Hide Waveforms Panels", "Running hide filter, please wait...", true);

            if (cmd.equals("Show Triaxial")) { // -aww 2010/01/25
                   boolean tf = jbTriaxial.isSelected();
                   if (tf) {
                       if (triaxialScrollZoomWithGroup && getShowRowHeader()) {

                           SwingUtilities.invokeLater( new Runnable() {
                               public void run() {
                                   JOptionPane.showMessageDialog(getTopLevelAncestor(), 
                                       "NOTE: Triaxial mode aborted, synched time scrolling option requires panel's channel headers to be turned off\n" + 
                                       "Disable either channel row headers (via main menubar) or triaxial group scroll synch (via group panel menu)",
                                       "Triaxial Group Panel Failure",
                                       JOptionPane.PLAIN_MESSAGE);
                               }
                           });

                           jbTriaxial.setSelected(false);
                           return null;
                       }
// Pechmann ?
                       horzScrollCnt = 0; // kludge here, need counter because of undesired cascade of scroll events via model cause time update
                       setShowFullTime(false); // we want similar scaling of pixels/sec in both the group and zoom viewport's display
                       Object obj = mv.getOwner();
                       if (obj instanceof Jiggle) {
                           Jiggle jiggle = (Jiggle) obj;
                           ((Jiggle)jiggle).toolBar.updateFullTimeButton();
                       }
                   }
                   triaxialMode = tf;
            }
//

            WFPanelList wfpList = groupPanel.wfpList;
            WFPanel wfp = null;
            int panelCnt = wfpList.size();
            hiddenCount = 0;

            Thread t = Thread.currentThread();
            t.setName("WFScrollerWorkerWFLoad");
            int topIdx = mv.wfvList.indexOf(mv.masterWFViewModel.get()); // selected view index or -1 for top
            int vppMax = topIdx + viewsPerPage*3;
            int vppMin = topIdx - viewsPerPage*2;

            for (int i = 0; i < panelCnt; i++) {

              wfp = (WFPanel) wfpList.get(i);

              // Next two skip through list short-circuiting the logic below
              if (cmd.equals("Show all")) {
                wfp.setVisible(true);
                wfp.wfv.isActive=true;
                continue;
              }
              //
              //else if (cmd.equals("Hide")) {
              //  wfp.setVisible(false);
              //  wfp.wfv.isActive=false;
              //  hiddenCount++;
              //  continue;
              //}
              //
    
              // Do hide by details of button selections
              //
              boolean hide = false; // by default show waveform panel
              AbstractButton jb = null;
              Channel ch = wfp.wfv.getChannelObj();
              String seedchan = ch.getSeedchan();
              Solution aSol = mv.getSelectedSolution();
              Waveform wf = wfp.getWf();
              String tmpStr = null;
              JasiReading jr = null; // added -aww 2009/05/14
    
              /*
              hiddenReadings = (jbNC.isSelected() || jbNA.isSelected() || jbNP.isSelected() || jbNPA.isSelected());
              hiddenReadings |= (jbC.isSelected() || jbA.isSelected() || jbP.isSelected());
              hiddenReadings |= (jbSelected.isSelected() || jbUnselected.isSelected());
              hiddenReadings |= (jbClipped.isSelected() || jbUnclipped.isSelected());
              hiddenReadings |= (jbVert.isSelected() || jbHorz.isSelected() || jbNorth.isSelected() || jbEast.isSelected());
              hiddenReadings |= jbH.isSelected() || jbL.isSelected() || jbVe.isSelected() || jbAc.isSelected() ||
              hiddenReadings |= jbT.isSelected();
              hiddenReadings |= (!jbShowB.isSelected() || !jbShowE.isSelected() || !jbShowH.isSelected());
              hiddenReadings |=  (!jbShowS.isSelected() || !jbShowD.isSelected()); 
              hiddenReadings |=  (!jbShowL.isSelected();
              */


              // Loop over known buttons
              String cmd2 = null;
              for (int ii=0; ii < iHideList.size(); ii++) {
    
                  jb = (AbstractButton) iHideList.get(ii);
                  cmd2 = jb.getActionCommand();
    
                  // Hide those "without" the reading
                  if (aSol != null) {

                    if (cmd2.startsWith("NoPicks")) {
                      //if (aSol.getPhaseList().getIndexOf(ch) < 0) { // changed to below is ignore deleted -aww 2009/05/14
                      jr = (JasiReading) aSol.getPhaseList().getByChannel(ch);
                      if (cmd2.endsWith("B")) {
                        if (jr == null || jr.isDeleted()) {
                          if (jb.isSelected()) hide = true;
                          continue;
                        }
                      }
                      else if (cmd2.endsWith("A")) {
                        if (jr == null || jr.isDeleted() || !jr.isAuto()) {
                          if (jb.isSelected()) hide = true;
                          continue;
                        }
                      }
                    }
                    if (cmd2.equals("No Amps")) {
                      //if (aSol.getAmpList().getIndexOf(ch) < 0) { // changed to below is ignore deleted -aww 2009/05/14
                      jr = (JasiReading) aSol.getAmpList().getByChannel(ch);
                      if (jr == null || jr.isDeleted()) {
                          if (jb.isSelected()) hide = true;
                          continue;
                      }
                    }
                    if (cmd2.equals("No Codas")) {
                      //if (aSol.getCodaList().getIndexOf(ch) < 0) { // changed to below is ignore deleted -aww 2009/05/14
                      jr = (JasiReading) aSol.getCodaList().getByChannel(ch);
                      if (jr == null || jr.isDeleted()) {
                          if (jb.isSelected()) hide = true;
                          continue;
                      }
                    }

                    // Hide those "with" the reading
                    if (cmd2.equals("Picks")) {
                      //if (aSol.getPhaseList().getIndexOf(ch) < 0) { // changed to below is ignore deleted -aww 2009/05/14
                      jr = (JasiReading) aSol.getPhaseList().getByChannel(ch);
                      if (jr != null && !jr.isDeleted()) {
                          if (jb.isSelected()) hide = true;
                          continue;
                      }
                    }
                    if (cmd2.equals("Amps")) {
                      //if (aSol.getAmpList().getIndexOf(ch) < 0) { // changed to below is ignore deleted -aww 2009/05/14
                      jr = (JasiReading) aSol.getAmpList().getByChannel(ch);
                      if (jr != null && !jr.isDeleted()) {
                          if (jb.isSelected()) hide = true;
                          continue;
                      }
                    }
                    if (cmd2.equals("Codas")) {
                      //if (aSol.getCodaList().getIndexOf(ch) < 0) { // changed to below is ignore deleted -aww 2009/05/14
                      jr = (JasiReading) aSol.getCodaList().getByChannel(ch);
                      if (jr != null && !jr.isDeleted()) {
                          if (jb.isSelected()) hide = true;
                          continue;
                      }
                    }
                  }
    
                  if (cmd2.equals("Show Triaxial")) { // those in same group of 3, not all by grouping of HH_ BH_ ...
                      if (!ch.sameTriaxialAs(mv.masterWFViewModel.get())) {  // all channels with first 2-char of seedchan the same
                          if (jb.isSelected()) hide = true;
                          continue;
                      }
                  }
                  else if (cmd2.startsWith("Selected")) {
                      if (wfp.wfv.isSelected()) {
                          if (jb.isSelected()) hide = true;
                          continue;
                      }
                  }
                  else if (cmd2.startsWith("Unselected")) {
                      if (!wfp.wfv.isSelected()) {
                          if (jb.isSelected()) hide = true;
                          continue;
                      }
                  }
                  else if (cmd2.startsWith("Unclipped")) {
                      if (wfp.wfv.getWaveform() != null && !wfp.wfv.getWaveform().isClipped()) {
                          if (jb.isSelected()) hide = true;
                          continue;
                      }
                  }
                  else if (cmd2.startsWith("Clipped")) {
                      if (wfp.wfv.getWaveform() != null && wfp.wfv.getWaveform().isClipped()) {
                          if (jb.isSelected()) hide = true;
                          continue;
                      }
                  }
                  else if (cmd2.equals("Vert")) {
                      tmpStr = seedchan.substring(2,3);
                      if (tmpStr.matches("[Z147]")) {
                          if (jb.isSelected()) hide = true;
                          continue;
                      }
                  }
                  else if (cmd2.equals("Horiz")) {
                      tmpStr = seedchan.substring(2,3);
                      if (tmpStr.matches("[EN235689]")) {
                          if (jb.isSelected()) hide = true;
                          continue;
                      }
                  }
                  else if (cmd2.equals("East")) {
                      tmpStr = seedchan.substring(2,3);
                      if (tmpStr.matches("[E369]")) {
                          if (jb.isSelected()) hide = true;
                          continue;
                      }
                  }
                  else if (cmd2.equals("North")) {
                      tmpStr = seedchan.substring(2,3);
                      if (tmpStr.matches("[N258]")) {
                          if (jb.isSelected()) hide = true;
                          continue;
                      }
                  }
                  else if (cmd2.startsWith("High")) {
                      if (ch.isHighGain()) {
                          if (jb.isSelected()) hide = true;
                          continue;
                      }
                  }
                  else if (cmd2.startsWith("Low")) {
                      if (ch.isLowGain()) {
                          if (jb.isSelected()) hide = true;
                          continue;
                      }
                  }
                  else if (cmd2.startsWith("No time")) {
                      /*
                      // Alot of overhead doing this need to be in SwingWorker wrapper thread with popup status dialog
                      boolean localLoad = false;
                      if (!wf.hasTimeSeries())  {
                        if (wf.loadTimeSeries()) { // try to load it
                          //System.out.println("DEBUG LOCAL LOAD of timeseries for: "+ wf.getChannelObj().toDelimitedSeedNameString(" "));
                          localLoad = true;
                        } else {
                          //System.out.println("DEBUG NO Scan NO timeseries: "+ wf.getChannelObj().toDelimitedSeedNameString(" "));
                        }
                      }
                      */
                      //System.out.println("DEBUG time gaps?: "+ wf.getChannelObj().toDelimitedSeedNameString(" ") + " " + wf.hasTimeTears());
                      if (wf != null && ! wf.hasTimeTears()) {
                          if (jb.isSelected()) hide = true;
                          //if (localLoad) wf.unloadTimeSeries();
                          continue;
                      }
                      //else System.out.println("DEBUG has time gaps: "+ wf.toString());
                      //if (localLoad) wf.unloadTimeSeries();
                  }
                  //
                  // Does not work well, doing in SwingWorker like above takes too long, particularly with waveservers
                  // better to query those for span times like status = waveClient.getTimes(net, sta, chn, loc, theList);
                  else if (cmd2.startsWith("Wfs")) {
                      /* Getting large arrays of channel time ranges is not efficient, takes very long time for server to respond
                      if (wfp.wfv.hasWaveform() && (AbstractWaveform.getWaveDataSourceType() == AbstractWaveform.LoadFromWaveServer) )  {
                        org.trinet.waveserver.rt.WaveClient wc = (org.trinet.waveserver.rt.WaveClient) ((WaveServerGroup)wf.getWaveSource()).getWaveClient();
                        System.err.println("WC GETTIMES START");
                        ArrayList theList = new ArrayList(2048);
                        //theList.clear();
                        int status = wc.getTimes(ch.getNet(), ch.getSta(), ch.getSeedchan(),ch.getLocation(), theList);
                        System.err.println("    WC waveClient.getTimes(String... List) status: " + wc.getStatusMessage(status));
                        int listSize = theList.size();
                        System.err.println("    WC TimeRange list size:" + listSize + " WC theList:");
                        for (int index = 0; index < listSize; index++) {
                            System.err.println(((org.trinet.waveserver.rt.TimeRange) theList.get(index)).toString());
                        }
                        System.err.println("    WC GETTIMES DONE");
                      }
                      // Alot of overhead doing wf loads, need to be in SwingWorker wrapper thread with popup status dialog
                      int jdx = mv.wfvList.getIndexOf(ch);
                      if (jdx < vppMin || jdx > vppMax) continue;

                      if (wf.hasTimeSeries())  {
                          if (jb.isSelected()) hide = true;
                          continue;
                      }
                      boolean localLoad = true;
                      wf.loadTimeSeries(); // try to load it
                      if (wf.hasTimeSeries())  {
                          if (jb.isSelected()) hide = true;
                          wf.unloadTimeSeries();
                          continue;
                      }
                      */
                  }
                  else if (cmd2.startsWith("No Wfs")) {
                      int jdx = mv.wfvList.getIndexOf(ch);
                      if (jdx < vppMin || jdx > vppMax) continue;

                      boolean localLoad = false;
                      if (wf != null && !wf.hasTimeSeries())  {
                        if (wf.loadTimeSeries()) { // try to load it
                          localLoad = true;
                        }
                      }
                      if (wf != null && !wf.hasTimeSeries())  {
                          if (jb.isSelected()) hide = true;
                          continue;
                      }
                      if (localLoad) wf.unloadTimeSeries();
                  }
                  //
                  else if (cmd2.equals("Vel")) {
                      boolean isVel = (wf == null) ? ch.isVelocity() : ch.isVelocity(new DateTime(wf.getEpochStart(), true));
                      if (isVel) {
                          if (jb.isSelected()) hide = true;
                          continue;
                      }
                  }
                  else if (cmd2.equals("Acc")) {
                      boolean isAcc = (wf == null) ? ch.isAcceleration() : ch.isAcceleration(new DateTime(wf.getEpochStart(), true));
                      if (isAcc) {
                          if (jb.isSelected()) hide = true;
                          continue;
                      }
                  }
                  else if (cmd2.equals("EHG")) {
                      if ( ch.getSeedchan().equals("EHG") ) { 
                          if (jb.isSelected()) hide = true;
                          continue;
                      }
                  }
                  else {
                      if (seedchan.startsWith(cmd2.substring(0,1))) {
                        tmpStr = cmd2.substring(1,2);
                        if (tmpStr.equals("_")) {
                          if (jb.isSelected()) hide = true;
                          continue;
                        }
                        else if ((tmpStr.equals("L") || tmpStr.equals("N") || tmpStr.equals("G")) && ch.isLowGain()) { // -aww 2009/07/28
                          if (jb.isSelected()) hide = true;
                          continue;
                        }
                        else if (tmpStr.equals("H") && ch.isHighGain()) {
                          if (jb.isSelected()) hide = true;
                          continue;
                        }
                      }
                  }
              } // inner hide loop
    
              //System.out.println("InputCmd: "+cmd+" "+cmd2+ " chan: "+seedchan+ " hide: "+hide+" hiddenCount: "+hiddenCount+"\n");
              wfp.setVisible(!hide);
              wfp.wfv.isActive= !hide;
              //if (!hide) System.out.println("cmd: " + cmd + " not hidden: " + wfp.wfv.getChannelObj().toDelimitedSeedNameString());
              if (hide) hiddenCount++;
    
            } // outer for loop

            //if (cmd.equals("Show all")) {
            //    if (hideButtonPanel != null) unsetButtons();
            //    //mv.wfvList.setCacheIndex(mv.wfvList.indexOf(mv.masterWFViewModel.get())); // selected view index or -1 for top
            //}
            //else if (cmd.equals("Hide All")) {
            //    if (hideButtonPanel != null) setButtons();
            //}

            return null;
        }

        //Runs on the event-dispatching thread so graphics calls are OK here.
        public void finished() {
            doFinished(workStatus, cmd, false, loadWF, "hide");
        }

      }; // SwingWorker class declaration

      worker.start();

    } // listener method actionPerformed

    protected void doActionCmd2(String command, final boolean loadWF) {

        final String cmd = command; 

        if (cmd.startsWith("Return")) return; // no-op close popup

        groupPanel.setVisible(false); // hide group panel to reduce graphics updates flashing?
        if (getShowRowHeader()) getRowHeader().getView().setVisible(false);

        org.trinet.util.SwingWorker worker = new org.trinet.util.SwingWorker() {

          WorkerStatusDialog workStatus = new WorkerStatusDialog((Frame)mv.getOwner(), true);
          // Would probably better to split this up into several separate action listeners for: scaling, filtering, hiding
          boolean isFiltering = false;
          boolean isScaling  = false;

          public Object construct() {


            if (cmd.equals("Customize filter...")) {
                isFiltering = true;
                setupFilter(workStatus);
                return null;
            }
            else workStatus.pop("Filter", "Filtering data", true);

            WFPanelList wfpList = groupPanel.wfpList;
            WFPanel wfp = null;
            int panelCnt = wfpList.size();
//
// Scaling section
//
            if (cmd.startsWith("Scale")) { // added scaling - aww 2009/03/26
              isScaling = true;
              double scale = 1.;
              if (cmd.equals("Scale Up")) scale =  2.;
              else if (cmd.equals("Scale Down")) scale =.5;
              Waveform wf = null;
              for (int i = 0; i < panelCnt; i++) {
                wfp = (WFPanel) wfpList.get(i);
                wf = wfp.getWf();
                if (wf != null && wf.getBias() == 0. && wf.hasTimeSeries()) {
                  wf.scanForBias();
                }
                if (scale == 1.) wfp.setAmpScaleFactor(1.);
                else wfp.setAmpScaleFactor(wfp.getAmpScaleFactor()*scale);
                wfp.scalePanel();
              }

              if (wfp != null) { // color menu item background gray when scale is not 1.0
                 double factor = wfp.getAmpScaleFactor();
                 if (factor == 1.) {
                  jbScaleUp.setBackground(WFScroller.this.getBackground());
                  jbScaleDown.setBackground(WFScroller.this.getBackground());
                 }
                 else if (factor < 1.) {
                  jbScaleDown.setBackground(Color.lightGray);
                  jbScaleUp.setBackground(WFScroller.this.getBackground());
                 }
                 else if (factor > 1.) {
                  jbScaleUp.setBackground(Color.lightGray);
                  jbScaleDown.setBackground(WFScroller.this.getBackground());
                 }
              }

              return null;
            }
// Done scaling section

// Filtering section
            for (int i = 0; i < panelCnt; i++) {

              wfp = (WFPanel) wfpList.get(i);

              // test filtering  here  -aww
              if ( cmd.startsWith("Filter") ) {

                if (cmd.equals("Filter Off")) {
                    isFiltering = true;
                    wfp.setShowAlternateWf(! jbFilterOff.isSelected());
                    wfp.setFilter(null);  // always setFilter(null) to destroy alternate waveform -aww 2009/04/08
                    continue;
                }

                isFiltering = true;
                wfp.setShowAlternateWf(false); // needed to have a reset of wfpanel amp scaling when setShow... called below
                wfp.setFilter(null);  // always setFilter(null) to destroy alternate waveform -aww 2009/04/08

                try {
                  int rate = (int) wfp.wfv.getChannelObj().getSampleRate(); 
                  if (rate <= 0) rate = 80;
                  int itype = -1;

                  if (cmd.equals("FilterHP")) {
                    if (jbFilterHP.isSelected()) {
                        itype = FilterTypes.HIGHPASS;
                    }
                  }
                  else if (cmd.equals("FilterBP")) {
                    if (jbFilterBP.isSelected()) {
                        itype = FilterTypes.BANDPASS;
                    }
                  }
                  else if (cmd.equals("FilterWA")) {
                    if (jbFilterWA.isSelected()) {
                        itype = FilterTypes.WOOD_ANDERSON;
                    }
                  }
                  else if (cmd.equals("FilterWA_HP")) {
                    if (jbFilterWA_HP.isSelected()) {
                        itype = FilterTypes.WOOD_ANDERSON_HP;
                    }
                  }
                  else if (cmd.equals("FilterWA_BP")) {
                    if (jbFilterWA_BP.isSelected()) {
                        itype = FilterTypes.WOOD_ANDERSON_BP;
                    }
                  }
                  else if (cmd.equals("FilterWA_EH")) {
                    if (jbFilterWA_EH.isSelected()) {
                        itype = FilterTypes.WOOD_ANDERSON_EH;
                    }
                  }
                  else if (cmd.equals("FilterVEL")) {
                    if (jbFilterVEL.isSelected()) {
                        itype = FilterTypes.VELOCITY;
                    }
                  }
                  else if (cmd.equals("FilterDIS")) {
                    if (jbFilterDIS.isSelected()) {
                        itype = FilterTypes.DISPLACEMENT;
                    }
                  }
                  else if (cmd.equals("FilterACC")) {
                    if (jbFilterACC.isSelected()) {
                        itype = FilterTypes.ACCELERATION;
                    }
                  }
                  else if (cmd.equals("FilterSP03")) {
                    if (jbFilterSP03.isSelected()) {
                        itype = FilterTypes.SP03;
                    }
                  }
                  else if (cmd.equals("FilterSP10")) {
                    if (jbFilterSP10.isSelected()) {
                        itype = FilterTypes.SP10;
                    }
                  }
                  else if (cmd.equals("FilterSP30")) {
                    if (jbFilterSP30.isSelected()) {
                        itype = FilterTypes.SP30;
                    }
                  }
                  else if (cmd.equals("FilterEHG")) {
                    if (jbFilterEHG.isSelected()) {
                        doEhgTypeFilter(wfp, isFiltering);
                        continue;
                    }
                  }

                  if (itype >= 0) {
                      wfp.setFilter(FilterTypes.getFilter(itype, rate, true)); // does the filtering
                      wfp.setShowAlternateWf(isFiltering); // call does a dataBox.set(getWf()) and setPanelBoxSize();
                  }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                continue;

              } // Filter... command

            } // outer for loop

            return null;
        }
                  
        // create the EHG filter for the applicable seedchan types for NM or ET nets - CERI Memphis TN
        private boolean doEhgTypeFilter(WFPanel wfp, boolean isFiltering) {

            Channel oldCh = (Channel) wfp.getWf().getChannelObj();
            if ( !(oldCh.getNet().equals("NM") || oldCh.getNet().equals("ET")) ||  // Network doesn't have known G-chan type
                 !oldCh.getSeedchan().substring(0,2).equals("EH") || // it's the wrong seedchan group
                 oldCh.getSeedchan().substring(2,3).equals("G") // A gain channel doesn't correct itself
               ) {
                    System.out.println("EHG INFO: filtering is a no-op for channel:" + oldCh.toDelimitedSeedNameString());
                    return false;
            }

            Channel ehgChan = (Channel) oldCh.clone();
            //Note set seedchan gain channel and search loaded group wfList
            ehgChan.setSeedchan("EHG"); // for testing, you could comment out this line to pass raw zwfp wf

            System.err.println("EHG INFO: searching wf list for EHG gain channel:" + ehgChan.toDelimitedSeedNameString());
            WFPanel wfp2 = WFScroller.this.groupPanel.wfpList.get(ehgChan);
            if ( wfp2 == null) {
                System.err.println("EHG NOOP: the gain channel is missing from wf list:" + ehgChan.toDelimitedSeedNameString());
                return false;
            }

            EHGFilter myFilter = new EHGFilter( wfp2.getRawWf() ); // the trace with the gain scaling data
            wfp.setFilter(myFilter); // does the filtering
            wfp.setShowAlternateWf(isFiltering); // call does a dataBox.set(getWf()) and setPanelBoxSize();
            System.err.println("EHG INFO: set EHG gain channel:" + isFiltering + " " + oldCh.toDelimitedSeedNameString());

            return true;
        }

        //Runs on the event-dispatching thread so graphics calls are OK here.
        public void finished() {
            doFinished(workStatus, cmd, (isScaling || isFiltering), loadWF, "filter");
        }

      }; // SwingWorker class declaration

      worker.start();

    } // listener method actionPerformed

    private void doFinished(WorkerStatusDialog workStatus, final String cmd, boolean doRepaint, boolean loadWF, final String src) {
            workStatus.dispose(); // don't invoke unpop before dispose (may cause exception)
            if (triaxialMode) { // -aww 2010/01/25
               // triaxPanelsInView = panelsViewable(); // do we want to see more than 3, what is there are 6, 9 or more -aww 2010/02/07
               double d =mv.masterWFWindowModel.getTimeSize();
               // use same pixels/sec scaling as zoom panel
               if (d != secondsInViewport) secondsInViewport = d;
               // show only 3 triaxial channels associated with zoom panel channel in lower group panel
               setPanelsInView(Math.min(triaxPanelsInView, viewsPerPage));
            }
            else {
               Object obj = mv.getOwner();
               if (obj instanceof WaveformDisplayIF) {
                 WaveformDisplayIF wfd = (WaveformDisplayIF) obj;
                 setSecondsInViewport(wfd.getWfPanelProperties().getDouble("secsPerPage"));
                 viewsPerPage = wfd.getWfPanelProperties().getInt("tracesPerPage");
               }
               setPanelsInView(viewsPerPage); // set from property "tracesPerPage"
               //System.out.println("FINISHED: views " + viewsPerPage + " secs: " + secondsInViewport);

               //makePanelVisible(groupPanel.wfpList.selectedWFPanel);
               centerViewportOnTime(groupPanel.wfpList.getSelectedPanel());
            }

            boolean filterOn = false;
            boolean hideOn = false;
            if (groupPanel.getPanelCount() > 0) {
                WFPanel wfp = null;
                for ( int idx =0; idx<groupPanel.wfpList.size(); idx++) {
                   wfp = (WFPanel) groupPanel.wfpList.get(idx);
                   filterOn |= (wfp.getShowAlternateWf() || wfp.getAmpScaleFactor() != 1.);
                   if (filterOn) break; 
                }
                hideOn |= (hiddenCount > 0 || ! jbFilterOff.isSelected());
            }
            hideButton.setIcon((hideOn) ? hideOnIcon : hideOffIcon);
            filterButton.setIcon((filterOn) ? filterOnIcon : filterOffIcon);
            jbFilterCustom.setEnabled(!jbFilterOff.isSelected());

            groupPanel.setVisible(true); // needed here if it was made invisible before thread start to reduce graphics update flashing? 
            if (getShowRowHeader()) getRowHeader().getView().setVisible(true);

            // try these if group panel resize/repainting is screwy
            //groupPanel.revalidate();
            //revalidate(); // WFScroller
            //repaint(); // WFScroller

            if (doRepaint || (!loadWF && triaxialMode == true)) { // no hiding or unhiding action
                jvp.repaint(); // call is needed for repainting filtering label and the scaled up/down timeseries 
            }
            else { // showing/hiding other panels, so we need to reposition "selected view" in scroller
              //System.out.println("DEBUG WFScroller corner menu hide/show selection finished, setting mv.wfvList.setCacheIndex...");
              mv.wfvList.setCacheIndex(mv.wfvList.indexOf(mv.masterWFViewModel.get())); // make sure panels around selected view index loadwf 
              // NOTE: if not "invokeLater" thread here, the scroller doesn't scroll to selected view 
              SwingUtilities.invokeLater( new Runnable() {
                public void run() {
                    WFPanel wfp = groupPanel.wfpList.getSelectedPanel();
                    //System.out.println("later run ... (wfp==null)? " + (wfp == null));
                    if (wfp != null) {
                        // centerViewportOnPanel(wfp); // decided not to, because the WFCacheMgr reloads traces above the selected view last
                                                       // which is a noticeable in graphics repaint -aww
                        // so instead do this:
                        // first logic prevents bogus double "image" a wfp at top of viewport as well as below because can't scroll further down
                        if (wfp.getY() >= (groupPanel.getHeight() - viewsPerPage*getUnitHeight())) {
                            makePanelVisible(wfp);
                            //System.out.println("later run ... makePanelVisible");
                        }
                        else { // we can scroll selected panel to top of viewport, since cache loaded wfs are "first" painted below
                            centerViewportOnTime(wfp); // -aww 2010/01/25
                            // Then scroll selection panel to top
                            Point viewXY = getViewport().getViewPosition();  // get ViewPort origin
                            viewXY.y= wfp.getY();                            // get top of selected panel
                            getViewport().setViewPosition(viewXY);
                            //System.out.println("later run ... setViewPosition");
                        }
                        repaint(); // tidy up ?
                    }
                }
              });
            }
            //
            //hiddenReadings = (hiddenCount > 0);
            hiddenReadings |= (hiddenCount > 0); // try this reset ? -aww 2013/02/15 test
            jbRefresh.setEnabled(hiddenReadings);
            jbShowAll.setEnabled(hiddenReadings);

            if (!src.equals("hide") && !cmd.startsWith("Return")) {
                if (!cmd.equals("Show Triaxial")) {
                  // Do button again, after giving event queue a chance to flush
                  SwingUtilities.invokeLater( new Runnable() {
                    public void run() {
                        if (src.equals("hide")) hideButton.doClick(); // re-open corner hide button menu
                        else if (src.equals("filter")) filterButton.doClick(); // re-open corner filterbutton menu
                    }
                  });
                }
            }
   } // doFinished(workStatus, cmd, filter|scaling, loadWF)

  /** INNER CLASS: Handle changes to the selected WFView. */
  class WFViewListener implements ChangeListener {
    public void stateChanged(ChangeEvent changeEvent) {

      final WFView newWFV = ((SelectedWFViewModel) changeEvent.getSource()).get();
      if (lastSelectedView != null && lastSelectedView != newWFV) {
        WFPanel wfp = groupPanel.wfpList.get(lastSelectedView);
        if (wfp != null) {
            if (wfp.getShowCursorTimeAsLine() || wfp.getShowCursorAmpAsLine()) {
                wfp.cursorTime = Double.NaN;  
                wfp.cursorAmp = Double.NaN;  
                wfp.repaint();
            }
        }
      }
      lastSelectedView = newWFV;
      //System.out.println("--->> DEBUG WFScroller WFViewListener:" + newWFV.getChannelObj().toString());

      final int mode = (changeEvent instanceof ViewChangeEvent) ? ((ViewChangeEvent)changeEvent).mode : 0;

      final WFPanel wfp = groupPanel.wfpList.get(newWFV);
      if (wfp == null) return;

      if (SwingUtilities.isEventDispatchThread()) {
          if (groupPanel != null &&  groupPanel.wfpList != null) {
            makePanelVisible(wfp, mode);
            jvp.repaint();
          }
      }
      else {
        SwingUtilities.invokeLater(
            new Runnable() {
                public void run() {
                  if (groupPanel != null &&  groupPanel.wfpList != null) {
                    makePanelVisible(wfp, mode);
                    jvp.repaint();
                  }
                }
            }
        );
      }

    }
  }  // end of WFViewListener

  /** INNER CLASS: Handle changes to the selected time/amp window. */
  private class WFWindowListener implements ChangeListener {
    public void stateChanged(ChangeEvent changeEvent) {

      // NOOP yet, keep time window in view
      final WFPanel wfp = groupPanel.wfpList.get(mv.masterWFViewModel.get());
      if (wfp == null) return;

      if (SwingUtilities.isEventDispatchThread()) {
        if (groupPanel != null &&  groupPanel.wfpList != null) {
          makePanelVisible(wfp, 0);
          //groupPanel.revalidate();
          jvp.repaint();
        }
      }
      else {
        SwingUtilities.invokeLater(
            new Runnable() {
                public void run() {
                    if (groupPanel != null &&  groupPanel.wfpList != null) {
                        WFPanel wfp = groupPanel.wfpList.get(mv.masterWFViewModel.get());
                        makePanelVisible(wfp, 0);
                        //groupPanel.revalidate();
                        jvp.repaint();
                    }
                }
            }
        );
      }
    }
  }  // end of WFWindowListener

  /** Handle filter button actions. */
/*
class CornerButtonHandler implements ActionListener {

     double viewSpan = secondsInViewport;

  public void actionPerformed(ActionEvent evt) {

  // toggle state
         setShowFullTime(!getShowFullTime());
         repaint();

         AbstractButton but = (AbstractButton) evt.getSource();
         if (getShowFullTime()) {
//             but.setBackground(Color.red);
           but.setText("X");
         } else {
//             but.setBackground(Color.gray);
           but.setText("O");
         }
     }
}
*/
    /**
    * Keep the waveform cache centered on the area in view. This was done via the
    * repaint() method until v1.3 when it stopped working! Seems someone optimized
    * the code to reduce repaints.
    * Note: Adjustment events are triggered by BOTH mouse down and mouse up.
    * So use isAdjusting()to reduce hyperactivity.
    * They are also fired by a frame resize but the 'value' is 0.
    */
    private class VerticalScrollListener implements AdjustmentListener {
        /** Keep WFViewList.cacheMgr, if there is one, in synch with the scroller. */
        public void adjustmentValueChanged(AdjustmentEvent e) {
            if ( ((JScrollBar)e.getSource()).getValueIsAdjusting() == false) {
              //if (mv.wfvList.cacheMgr != null) { // might be null if stopped by Magnitude solve
                 int index = -1;
                 // if WFPanels are "hidden", skipped over, we need to find the offset of topmost visible -aww 2009/04/02
                 JViewport jvp = getViewport();
                 if (jvp != null) {
                   Component c = jvp.getView();
                   if (c != null) c = c.getComponentAt(jvp.getViewPosition());
                   if (c != null) {
                       index = groupPanel.wfpList.indexOf(c);
                       //System.out.println("GetComponentAt:"+((ActiveWFPanel)c).wfv.getChannelObj().toString()+" index: " +i);
                   }
                   if (index < 0) index = getTopIndex();
                 } // end of added logic -aww 2009/04/02

                 if (mv.wfvList.getWFCacheManager() != null && mv.wfvList.getWFCacheManager().oldIndex == index)  return;

                 //System.out.println("DEBUG WFScroller VerticalScrollListener setting mv.wfvList.setCacheIndex: "+index);
                 Thread[] tarray = new Thread[25];
                 int tcnt = Thread.enumerate(tarray);
                 boolean resetWFCache = true;
                 for (int ii=0;ii<tcnt;ii++) {
                     //System.out.println("Thread " + ii + " name: " + tarray[ii].getName());
                     if (tarray[ii].getName().indexOf("WFLoad") >= 0)  {
                         // don't doit while mag engine, or other thread which may be scanning of wfs to avoid "unloads"
                         resetWFCache =false;
                         break;
                     }
                 }
                 if (resetWFCache) mv.wfvList.setCacheIndex(index);

              //} // removed 2010/08/06 -aww
            }
        }
    }
    long horzScrollCnt = 0l;
    private class HorizontalScrollListener implements AdjustmentListener {
        public void adjustmentValueChanged(AdjustmentEvent e) {
            //if ( ((JScrollBar)e.getSource()).getValueIsAdjusting() == false) {
            // Scroll zoom with scroll of lower group ?
            if (triaxialMode && triaxialScrollZoomWithGroup) { // -aww 2010/01/28 
                /*
                TimeSpan ts = mv.masterWFWindowModel.getTimeSpan();
                double d = ts.getDuration();
                WFPanel wfp = groupPanel.wfpList.getSelectedPanel();

                //System.out.println("     WFScroller horiz scroll old timespan: " + ts);
                //System.out.println("     WFScroller viewX: " + getViewport().getViewPosition().x + " time: " + LeapSeconds.trueToString(wfp.dtOfPixel(getViewport().getViewPosition().x)) + " pix/sec: " + wfp.getPixelsPerSecond());
                ts.setStart( wfp.dtOfPixel(getViewport().getViewPosition().x) );
                ts.setEnd(ts.getStart()+d);
                    System.out.println("     WFScroller horiz scroll new timespan: " + ts);
                mv.masterWFWindowModel.setTimeSpan(ts);
                */
                //
              horzScrollCnt++;
              //System.out.println("HORZ SCROLLER COUNT : " + horzScrollCnt);
              if (horzScrollCnt >= 2) {
                Rectangle viewRect = getViewport().getViewRect();// wfp.getVisibleRect() whats showing?
                double centerTime = groupPanel.wfpList.getSelectedPanel().dtOfPixel(viewRect.x+viewRect.width/2);// ?
                if (Math.abs(mv.masterWFWindowModel.getCenterTime() - centerTime) > .001) {
                    //System.out.println("     WFScroller horiz scroll moved to new centertime: " + LeapSeconds.trueToString(centerTime));
                    mv.masterWFWindowModel.setCenterTime(centerTime);
                }
              }
              //
            }
        }
    }

    protected void showChannelLabels(int mode) {
        switch (mode) {
            case 0 : // start
              WFPanel.CHANNEL_LABEL_MODE = 0;
              showChannelLabelLeader(true);
              break;
            case 1 : // leading
              showChannelLabelLeader(true);
              WFPanel.CHANNEL_LABEL_MODE = 1;
              break;
            case 2 : // trailing
              WFPanel.CHANNEL_LABEL_MODE = 2;
              showChannelLabelLeader(true);
              break;
            case 3 : // both
              WFPanel.CHANNEL_LABEL_MODE = 3;
              showChannelLabelLeader(true);
              break;
            case 4 : // none
              WFPanel.CHANNEL_LABEL_MODE = 4;
              showChannelLabelLeader(false);
              break;
        }
        channelLabelMode  = WFPanel.CHANNEL_LABEL_MODE;
    }

    /*
    private void showChannelLabelTrailer(boolean tf) {
        jvp.setDrawLabels(tf);
    }
    */

    private void showChannelLabelLeader(boolean tf) {
        WFPanelList wfpList = groupPanel.wfpList;
        int count = groupPanel.wfpList.size(); 
        for (int ii = 0; ii < count; ii++ ) {
          ((WFPanel) wfpList.get(ii)).setShowChannelLabel(tf);
        }
    }
/// --------------------------------------------------------------------------
/*
  //Main for testing
  public final static class Tester {
      public static void main(String args[])  {
    
        int evid;
    
        if (args.length <= 0)        // no args
        {
          System.out.println("Usage: java WFScroller [evid])");
    
          evid = 9655069;
          System.out.println("Using evid "+ evid+" as a test...");
    
        } else {
    
          Integer val = Integer.valueOf(args[0]);            // convert arg String to 'double'
          evid = (int) val.intValue();
        }
    
    // make a frame
        JFrame frame = new JFrame("Main");
        frame.addWindowListener(new WindowAdapter() {
          public void windowClosing(WindowEvent e) {System.exit(0);}
        });
    
        System.out.println("Making connection...");
        DataSource init = TestDataSource.create();  // make connection
    
        // NOTE: this demonstrates making a static Channel list.
        System.out.println("Reading in station list...");
        //        Channel.setList(Channel.getAllList());
    
        System.out.println("Making MasterView for evid = "+evid);
    
        MasterView mv = new MasterView();
        mv.setWaveFormLoadMode(MasterView.Cache);
        mv.setAlignmentMode(MasterView.AlignOnTime);
        mv.defineByDataSource(evid);
    
        final WFScroller wfs = new WFScroller(mv) ;            // make scroller
    
        NumberChooser numberChooser =
            new NumberChooser(5, 30, 5, wfs.viewsPerPage);
    
        Container contentPane = frame.getContentPane();
    
        contentPane.add(numberChooser, BorderLayout.NORTH);
    
        numberChooser.addActionListener(
          new ActionListener() {
              public void actionPerformed(ActionEvent e) {
                NumberChooser nc = (NumberChooser) e.getSource();
                int val = (int) nc.getSelectedValue();
                System.out.println(" set panels in view = " + val);
                wfs.setPanelsInView(val);
             }
          }
        );
    
        frame.getContentPane().add( wfs );            // add scroller to frame
        contentPane.add(wfs, BorderLayout.CENTER);
    
        frame.pack();
        frame.setVisible(true);
    
        frame.setSize(800, 800);        // must be done AFTER setVisible
      }  

  } // end of Tester
*/
} // end of WFScroller class
