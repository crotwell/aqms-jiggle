package org.trinet.jiggle;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.trinet.jasi.EventType;
import org.trinet.jasi.EventTypeMap3;
import org.trinet.jasi.ActiveArrayList;
import org.trinet.util.graphics.IconImage;

public class DPeventType extends JPanel {

    private JiggleProperties newProps;    // new properties as changed by this dialog
    private MenuTypeChooserPanel chooserPanel = null;

    private static final EventType [] EVENT_TYPES = EventTypeMap3.getMap().getEventTypes();

    protected boolean typeChanged = false;

    public DPeventType(JiggleProperties props) {
      newProps = props;
      chooserPanel = new MenuTypeChooserPanel();
      try {
        initGraphics();
      }
      catch(Exception e) {
        e.printStackTrace();
      }
    }

    private void initGraphics() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS)); 
        chooserPanel.initGraphics();
        this.add(chooserPanel);
    }

    protected void updateEventTypes() {
        ArrayList aList = chooserPanel.getChoosen();
        String oldChoices = newProps.getProperty("eventTypeChoices","");
        if (aList == null || aList.isEmpty()) {
            newProps.setProperty("eventTypeChoices", ""); // remove property
            typeChanged = (oldChoices.length() > 0);
            return;
        }

        StringBuffer sb = new StringBuffer(512);
        String [] names = (String []) aList.toArray(new String[aList.size()]);
        for (int idx = 0; idx < names.length; idx++) {
          for (int jdx = 0; jdx < EVENT_TYPES.length; jdx++) {
            if (names[idx].equals(EVENT_TYPES[jdx].name)) sb.append(EVENT_TYPES[jdx].code).append(" ");
          }
        }
        newProps.setProperty("eventTypeChoices", sb.toString());

        typeChanged = ! newProps.getProperty("eventTypeChoices", "").equals(oldChoices);

    }
    

  class MenuTypeChooserPanel extends JPanel {

    private final HashMap toolTipMap = new HashMap(31);

    private java.util.List allItems = null;
    private java.util.List rejectItems = null;;
    private java.util.List chooseItems = null;
    private javax.swing.JList chooseList = null; 

    public MenuTypeChooserPanel() { 

        Arrays.sort(EVENT_TYPES, new Comparator() {
            public int compare(Object o1, Object o2) {
                return ((EventType) o1).name.compareTo(((EventType) o2).name);
            }
            public boolean equals(Object o) {
                return (o == this);
            }
        });
        int size = EVENT_TYPES.length; 

        for (int idx = 0; idx<size; idx++) { 
          toolTipMap.put(EVENT_TYPES[idx].name, EVENT_TYPES[idx].desc);
        }

        
        java.util.List list = new ActiveArrayList();
        for (int idx = 0; idx < size; idx++) {
            list.add(EVENT_TYPES[idx].name);
        }
        setAll(list);

        list = new ActiveArrayList();
        String [] dbCodes = newProps.getStringArray("eventTypeChoices");
        for (int idx = 0; idx < dbCodes.length; idx++) {
          for (int jdx = 0; jdx < size; jdx++) {
            if (EVENT_TYPES[jdx].code.equals(dbCodes[idx])) list.add(EVENT_TYPES[jdx].name);
          } 
        }

        if (list == null || list.isEmpty()) list = new ActiveArrayList(allItems);
        setChoosen(list);
    }

    public void setAll(java.util.List all) {
        allItems = all;
    }

    public ArrayList getChoosen() {
      Object [] names = ((DefaultListModel)chooseList.getModel()).toArray();
      ArrayList aList = new ArrayList(names.length);
      for (int idx = 0; idx < names.length; idx++) {
        aList.add(names[idx]);
      }
      return aList;
    }

    public void setChoosen(java.util.List choose) {
        ArrayList aList = new ArrayList(allItems);
        aList.removeAll(choose);
        rejectItems = aList;
        aList = new ArrayList(choose);
        aList.removeAll(rejectItems);
        chooseItems = aList;
    }

    public void setRejected(java.util.List reject) {
        ArrayList aList = new ArrayList(allItems);
        aList.removeAll(reject);
        chooseItems = aList;
        aList = new ArrayList(allItems);
        aList.removeAll(chooseItems);
        rejectItems = aList;
    }

    public void initGraphics() {
        Box hbox = Box.createHorizontalBox();
        hbox.setBorder(BorderFactory.createTitledBorder("Event type selection menu items"));
        DefaultListModel dlm  = new DefaultListModel();
        for (int idx = 0; idx < rejectItems.size(); idx++) dlm.addElement(rejectItems.get(idx));
        final JList rejectList = new JList(dlm);
        MyToolTipListCellRenderer ttr = new MyToolTipListCellRenderer();
        rejectList.setCellRenderer(ttr);
        rejectList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        rejectList.setPreferredSize(new Dimension(80,600));
        JScrollPane jsp = new JScrollPane(rejectList);
        //jsp.getViewport().setViewSize(new Dimension(60,250));
        jsp.setBorder(BorderFactory.createTitledBorder("Available to add"));
        hbox.add(jsp);

        Box vbox = Box.createVerticalBox();
        JButton jb = makeButton(">","Add selected available type(s) to menu choices","e.gif");
        final ActionListener al = new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                String cmd = evt.getActionCommand();
                if (cmd.equals(">")) {
                    Object [] selected = rejectList.getSelectedValues();
                    DefaultListModel cmodel = (DefaultListModel) chooseList.getModel();
                    DefaultListModel rmodel = (DefaultListModel) rejectList.getModel();
                    int selectIdx = chooseList.getSelectedIndex();
                    if (selectIdx < 0) selectIdx = cmodel.size()-1;
                    for (int idx = 0; idx < selected.length; idx++) {
                        cmodel.add(++selectIdx, selected[idx]);
                        rmodel.removeElement(selected[idx]);
                    } 
                }
                else if (cmd.equals("<")) {
                    Object [] selected = chooseList.getSelectedValues();
                    DefaultListModel cmodel = (DefaultListModel) chooseList.getModel();
                    DefaultListModel rmodel = (DefaultListModel) rejectList.getModel();
                    ArrayList rlist = new ArrayList(Arrays.asList(rmodel.toArray()));
                    rlist.addAll(Arrays.asList(selected));
                    Collections.sort(rlist);
                    rmodel = new DefaultListModel();
                    for (int idx = 0; idx < rlist.size(); idx++) {
                        rmodel.addElement(rlist.get(idx));
                    }
                    for (int idx = 0; idx < selected.length; idx++) {
                        cmodel.removeElement(selected[idx]);
                    }
                    rejectList.setModel(rmodel);
                }
            }
        };
        jb.addActionListener(al);
        vbox.add(jb);
        jb = makeButton("<","Remove selected type(s) from menu choices", "w.gif");
        jb.addActionListener(al);
        vbox.add(jb);
        hbox.add(vbox);
        
        dlm  = new DefaultListModel();
        for (int idx = 0; idx < chooseItems.size(); idx++) dlm.addElement(chooseItems.get(idx));
        chooseList = new JList(dlm);
        chooseList.setCellRenderer(ttr);
        chooseList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        chooseList.setPreferredSize(new Dimension(80,600));
        jsp = new JScrollPane(chooseList);
        //jsp.getViewport().setViewSize(new Dimension(60,250));
        jsp.setBorder(BorderFactory.createTitledBorder("Menu Choice"));
        hbox.add(jsp);

        vbox = Box.createVerticalBox();
        jb = makeButton("-","Move selected type up in menu choices","n.gif");
        final ActionListener al2 = new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                String cmd = evt.getActionCommand();
                int selectIdx = chooseList.getSelectedIndex();
                Object selected = (selectIdx >= 0) ?  chooseList.getModel().getElementAt(selectIdx) : null;
                if (selected == null) return;

                DefaultListModel cmodel = (DefaultListModel) chooseList.getModel();

                if (cmd.equals("+")) {
                  if (selectIdx+1 < cmodel.size()) {
                    cmodel.removeElementAt(selectIdx);
                    cmodel.insertElementAt(selected, selectIdx+1);
                    chooseList.setSelectedIndex(selectIdx+1);
                  }
                }
                else if (cmd.equals("-")) {
                  if (selectIdx-1 >= 0) {
                    cmodel.removeElementAt(selectIdx);
                    cmodel.insertElementAt(selected, selectIdx-1);
                    chooseList.setSelectedIndex(selectIdx-1);
                  }
                }
            }
        };
        jb.addActionListener(al2);
        vbox.add(jb);
        jb = makeButton("+","Move selected type down in menu choices","s.gif");
        jb.addActionListener(al2);
        vbox.add(jb);
        hbox.add(vbox);
        this.add(hbox);
    }
 

    private JButton makeButton(String labelStr, String tooltip, String imageStr) {
        Image image = IconImage.getImage(imageStr);
        JButton btn = null;
        if (image == null) { // handle situation when .gif file can't be found
            btn = new JButton(labelStr);
        } else {
            btn = new JButton(new ImageIcon(image));
        }
        btn.setActionCommand(labelStr);
        btn.setPreferredSize(new Dimension(20,20));
        btn.setMaximumSize(new Dimension(20,20));
        btn.setToolTipText(tooltip);
        return btn;
    }

    class MyToolTipListCellRenderer extends JLabel implements ListCellRenderer {
       public Component getListCellRendererComponent( JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
         String s = value.toString();
         setText(s);
         if (isSelected) {
             setBackground(list.getSelectionBackground());
             setForeground(list.getSelectionForeground());
         }
         else {
             setBackground(list.getBackground());
             setForeground(list.getForeground());
         }
         setToolTipText((String)toolTipMap.get(value));
         setEnabled(list.isEnabled());
         setFont(list.getFont());
         setOpaque(true);
         return this;
      }
    }

  }
}
