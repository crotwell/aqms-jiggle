package org.trinet.pcs;

  /**
   * Delete transtion rows
   */
class DelTransitions
{

  public static void main (String args[])
    {
        TransitionRow t = new TransitionRow();
	int ndel = 0;

	//NOTE: 
	// this tr1, tr2, tr3 nonsense was made necessary by compiler errors
	// "Variable tr may not have been initialized"

	switch (args.length) {

	case 4:
	  t.groupOld  = args[0];
	  t.sourceOld = args[1];
	  t.stateOld  = args[2];
	  t.resultOld = Integer.valueOf(args[3]).intValue();

	  t.delete();

	  break;

	case 3:
	  t.groupOld  = args[0];
	  t.sourceOld = args[1];
	  t.stateOld  = args[2];
	  ndel = TransitionRow.delete (t.groupOld, t.sourceOld, t.stateOld);

	  break;

	case 2:
	  t.groupOld  = args[0];
	  t.sourceOld = args[1];
	  ndel = TransitionRow.delete (t.groupOld, t.sourceOld);    

	  break;

	case 1:
	  t.groupOld  = args[0];
	  ndel = TransitionRow.delete (t.groupOld);    

	  break;

	default:
	  System.out.println("SYNTAX:  DelTransitions <group> [<source>] [<state>] [<result>]");
	  return ;
	}

	System.out.println (ndel + " table rows deleted.");

    }


}
