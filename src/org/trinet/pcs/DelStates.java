package org.trinet.pcs;
  /**
   * Put an data object in the given state/rank.
   *  % SYNTAX: DelStates <group> <source> <id> <state>
   */
class DelStates {

  public static void main(String args[]) {
        StateRow sr = new StateRow();
        int ndel = 0;

        switch (args.length) {

        case 4:
          sr.controlGroup = args[0];
          sr.sourceTable  = args[1];
          sr.id = Long.parseLong(args[2]);
          sr.state  = args[3];
          ndel = sr.delete(sr.controlGroup, sr.sourceTable, sr.state, sr.id);

          break;

        case 3:
          sr.controlGroup = args[0];
          sr.sourceTable  = args[1];
          sr.id = Long.parseLong(args[2]);
          ndel = sr.delete(sr.controlGroup, sr.sourceTable, sr.id);

          break;

        case 2:
          sr.controlGroup = args[0];
          sr.sourceTable  = args[1];
          ndel = sr.delete(sr.controlGroup, sr.sourceTable);    

          break;

        case 1:
          sr.controlGroup = args[0];
          ndel = sr.delete(sr.controlGroup);    

          break;

          /* Too dangerous
        case 0:
          ndel = sr.delete();    

           break;
          */

        default:
          System.out.println("SYNTAX: delstates <group> [<source>] [<id>] [<state>]");
          
        }
        System.out.println(ndel + " table rows deleted.");
    }

}
