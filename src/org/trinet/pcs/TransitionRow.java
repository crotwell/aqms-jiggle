package org.trinet.pcs;
import java.sql.*;
import java.util.*;
import org.trinet.util.EpochTime;

/**
 * Represents rows in the the PCS_TRANSITION table
 */
public class TransitionRow {

    protected static boolean debug = false;

    protected static final String TRANSITION_TABLE = "PCS_TRANSITION";

    protected String groupOld;        // NOT NULL, no empty strings allowed
    protected String sourceOld;       // NOT NULL, no empty strings allowed
    protected String stateOld;        // NOT NULL, no empty strings allowed
    protected int resultOld;          // must be non-zero, 0 implies unprocessed StateRow
    
    protected String groupNew;        // not NULL
    protected String sourceNew;       // not NULL
    protected String stateNew;        // not NULL
    protected int rankNew;            // force positive int only?
    protected int resultNew;          // force positive int only?

    protected String auth;            // NOT NULL?? do we intend to use this?
    protected String subsource;       // likewise ??
    protected java.sql.Date lddate;

    public TransitionRow() {};

  /**
   * Select from table row matching this instance's 
   * groupOld,sourceOld,stateOld,stateNew,and resultOld value
   * which should be unique for a row in the table. 
   */
    public TransitionRow select() {
        return select(false);
    }
  /**
   * Select from table row matching this instance's 
   * groupOld,sourceOld,stateOld,stateNew,and resultOld value
   * which should be unique for a row in the table. 
   * If input forUpdate is true then the table row is locked until commit.
   */
    public TransitionRow select(boolean forUpdate) {
        StringBuffer sb = new StringBuffer(256);
        sb.append("SELECT * FROM ").append(TRANSITION_TABLE);
        sb.append(" WHERE ");
        sb.append("groupOld=").append(ProcessControl.toStringSQL(groupOld));
        sb.append(" AND sourceOld=").append(ProcessControl.toStringSQL(sourceOld));
        sb.append(" AND stateOld=").append(ProcessControl.toStringSQL(stateOld));
        sb.append(" AND resultOld=").append(resultOld);
        sb.append(" AND stateNew=").append(ProcessControl.toStringSQL(stateNew));
        sb.append(" AND sourceNew=").append(ProcessControl.toStringSQL(sourceNew));
        sb.append(" AND groupNew=").append(ProcessControl.toStringSQL(groupNew));
        if (forUpdate) sb.append(" FOR UPDATE");

        TransitionRow [] tr = doQuery(sb.toString());
        return (tr == null) ? null : tr[0];
    }

  /**
   * Insert row for this Transition into the dbase
   */
  public int insert() {

      if (! meetsConstraints() ) {
          if (debug) System.out.println("DEBUG TransitionRow  insert FAILED, meetsConstraints() == FALSE");
          return 0;  // could just let it throw SQL exception?
      }

      // Note an insert for a duplicate key results in SQLException
      // Should first check for an existing row with matching key
      // Then do we an update in lieu of insert? -aww
      // could check here for match with current values of ?
      // if matching values then either skip or update, else insert?
      // do we want a new posting to overwrite any old results?
      TransitionRow tr = select(true);
      String sql = (tr == null) ? getInsertString() : getUpdateString();

      return doUpdate(sql);
  }

  private boolean meetsConstraints() {
      return ! (
                ProcessControl.isBlank(stateOld) ||
                ProcessControl.isBlank(sourceOld) ||
                ProcessControl.isBlank(groupOld) ||
                // can't have zero result (zero => not processed)
                resultOld == 0 ||
                //ok here, else force rank > 0
                (!ProcessControl.isBlank(stateNew) && rankNew <=0)
               );
  }

/** 
 * For use with UPDATE.
 */
  private String getUpdateString() {
      StringBuffer sb = new StringBuffer(256); 
      sb.append("UPDATE ").append(TRANSITION_TABLE).append(" SET ");
      sb.append("rankNew=").append(ProcessControl.znull(rankNew));
      sb.append(",resultNew=").append(ProcessControl.znull(resultNew));
      sb.append(",auth=").append(ProcessControl.toStringSQL(auth));
      sb.append(",subsource=").append(ProcessControl.toStringSQL(subsource));
      sb.append(",lddate=SYS_EXTRACT_UTC(CURRENT_TIMESTAMP)"); // change is like a new row, reset date here -aww  2008/06/03
      sb.append(" WHERE ");
      sb.append("groupOld=").append(ProcessControl.toStringSQL(groupOld));
      sb.append(" AND sourceOld=").append(ProcessControl.toStringSQL(sourceOld));
      sb.append(" AND stateOld=").append(ProcessControl.toStringSQL(stateOld));
      sb.append(" AND resultOld=").append(ProcessControl.znull(resultOld));
      sb.append(" AND stateNew=").append(ProcessControl.toStringSQL(stateNew));
      sb.append(" AND sourceNew=").append(ProcessControl.toStringSQL(sourceNew));
      sb.append(" AND groupNew=").append(ProcessControl.toStringSQL(groupNew));

      return sb.toString();
  } 

/** 
 * For use with INSERT.
 */
  private String getInsertString() {
      StringBuffer sb = new StringBuffer(256); 
      sb.append("INSERT INTO ").append(TRANSITION_TABLE).append(" ");
      sb.append("(groupOld,sourceOld,stateOld,resultOld,groupNew,sourceNew,stateNew,rankNew,resultNew,auth,subsource)"); 
      sb.append(" VALUES (");
      sb.append(ProcessControl.toStringSQL(groupOld)).append(",");
      sb.append(ProcessControl.toStringSQL(sourceOld)).append(",");
      sb.append(ProcessControl.toStringSQL(stateOld)).append(",");
      sb.append(ProcessControl.znull(resultOld)).append(",");
      sb.append(ProcessControl.toStringSQL(groupNew)).append(",");
      sb.append(ProcessControl.toStringSQL(sourceNew)).append(",");
      sb.append(ProcessControl.toStringSQL(stateNew)).append(",");
      sb.append(ProcessControl.znull(rankNew)).append(",");
      sb.append(ProcessControl.znull(resultNew)).append(",");
      sb.append(ProcessControl.toStringSQL(auth)).append(",");
      sb.append(ProcessControl.toStringSQL(subsource));
      //sb.append(",SYS_EXTRACT_UTC(CURRENT_TIMESTAMP)"); // remove lddate value, use default for table -aww 2008/06/03
      sb.append(")");

      return sb.toString();
  } 

  /**
   * Delete this row
   */
  public int delete() {
      StringBuffer sb = new StringBuffer(256);
      sb.append("DELETE FROM ").append(TRANSITION_TABLE);
      sb.append(" WHERE ");
      sb.append("groupOld=").append(ProcessControl.toStringSQL(groupOld));
      sb.append(" AND sourceOld=").append(ProcessControl.toStringSQL(sourceOld));
      sb.append(" AND stateOld=").append(ProcessControl.toStringSQL(stateOld));
      sb.append(" AND stateNew=").append(ProcessControl.toStringSQL(stateNew));
      sb.append(" AND sourceNew=").append(ProcessControl.toStringSQL(sourceNew));
      sb.append(" AND groupNew=").append(ProcessControl.toStringSQL(groupNew));
      sb.append(" AND (");
        sb.append("resultOld=").append(resultOld);
        if (resultOld == 0) sb.append(" OR resultOld IS NULL");
      sb.append(")");

      return doUpdate(sb.toString());

  }

  /**
   * Delete all rows matching old state description with input result 
   */
  public int delete(int result) {
      StringBuffer sb = new StringBuffer(256);
      sb.append("DELETE FROM ").append(TRANSITION_TABLE);
      sb.append(" WHERE ");
      sb.append("groupOld=").append(ProcessControl.toStringSQL(groupOld));
      sb.append(" AND sourceOld=").append(ProcessControl.toStringSQL(sourceOld));
      sb.append(" AND stateOld=").append(ProcessControl.toStringSQL(stateOld));
      sb.append(" AND (");
        sb.append("resultOld=").append(resultOld);
        if (resultOld == 0) sb.append(" OR resultOld IS NULL");
      sb.append(")");

      return doUpdate(sb.toString());

  }

  /**
   * Delete rows
   */
  public static int delete(String cGroup, String sTable, String oState) {
      StringBuffer sb = new StringBuffer(256);
      sb.append("DELETE FROM ").append(TRANSITION_TABLE);
      sb.append(" WHERE ");
      sb.append("groupOld=").append(ProcessControl.toStringSQL(cGroup));
      sb.append(" AND sourceOld=").append(ProcessControl.toStringSQL(sTable));
      sb.append(" AND stateOld=").append(ProcessControl.toStringSQL(oState));

      return doUpdate(sb.toString());
  }
  /**
   * Delete rows
   */
  public static int delete(String cGroup, String sTable) {
      StringBuffer sb = new StringBuffer(256);
      sb.append("DELETE FROM ").append(TRANSITION_TABLE);
      sb.append(" WHERE ");
      sb.append("groupOld=").append(ProcessControl.toStringSQL(cGroup));
      sb.append(" AND sourceOld=").append(ProcessControl.toStringSQL(sTable));

      return doUpdate(sb.toString());
  }
  /**
   * Delete rows
   */
  public static int delete(String cGroup) {
      StringBuffer sb = new StringBuffer(256);
      sb.append("DELETE FROM ").append(TRANSITION_TABLE);
      sb.append(" WHERE ");
      sb.append("groupOld=").append(ProcessControl.toStringSQL(cGroup));

      return doUpdate(sb.toString());
  }

  public static TransitionRow[] get(String cGroup, String sTable, String oState, int pResult) { 
        StringBuffer sb = new StringBuffer(256);
        sb.append("SELECT * FROM ").append(TRANSITION_TABLE);
        sb.append(" WHERE ");
        sb.append("groupOld=").append(ProcessControl.toStringSQL(cGroup));
        sb.append(" AND sourceOld=").append(ProcessControl.toStringSQL(sTable));
        sb.append(" AND stateOld=").append(ProcessControl.toStringSQL(oState));
        sb.append(" AND resultOld=").append(ProcessControl.znull(pResult));
        sb.append(" ORDER BY groupOld, sourceOld, stateOld, groupNew, sourceNew, stateNew DESC");

      return doQuery(sb.toString());
  }

  public static TransitionRow[] get(String cGroup, String sTable, String oState) { 
        StringBuffer sb = new StringBuffer(256);
        sb.append("SELECT * FROM ").append(TRANSITION_TABLE);
        sb.append(" WHERE ");
        sb.append("groupOld=").append(ProcessControl.toStringSQL(cGroup));
        sb.append(" AND sourceOld=").append(ProcessControl.toStringSQL(sTable));
        sb.append(" AND stateOld=").append(ProcessControl.toStringSQL(oState));
        sb.append(" ORDER BY groupOld, sourceOld, stateOld, groupNew, sourceNew, stateNew DESC");

      return doQuery(sb.toString());
  }

  public static TransitionRow[] get(String cGroup, String sTable) { 
        StringBuffer sb = new StringBuffer(256);
        sb.append("SELECT * FROM ").append(TRANSITION_TABLE);
        sb.append(" WHERE ");
        sb.append("groupOld=").append(ProcessControl.toStringSQL(cGroup));
        sb.append(" AND sourceOld=").append(ProcessControl.toStringSQL(sTable));
        sb.append(" ORDER BY groupOld, sourceOld, stateOld, groupNew, sourceNew, stateNew DESC");
        return doQuery(sb.toString());
  }

  public static TransitionRow[] get(String cGroup) { 
        StringBuffer sb = new StringBuffer(256);
        sb.append("SELECT * FROM ").append(TRANSITION_TABLE);
        sb.append(" WHERE ");
        sb.append("groupOld=").append(ProcessControl.toStringSQL(cGroup));
        sb.append(" ORDER BY groupOld, sourceOld, stateOld, groupNew, sourceNew, stateNew DESC");
        return doQuery(sb.toString());
  }

  public static TransitionRow[] get() { 
        StringBuffer sb = new StringBuffer(256);
        sb.append("SELECT * FROM ").append(TRANSITION_TABLE);
        sb.append(" ORDER BY groupOld, sourceOld, stateOld, groupNew, sourceNew, stateNew DESC");
        return doQuery(sb.toString());
  }

  /** Comma delimited list of the attribute values of this instance. */
  public String toString() {
      String dlm = ","; 
      StringBuffer sb = new StringBuffer(512);
      sb.append(groupOld).append(dlm);
      sb.append(sourceOld).append(dlm);
      sb.append(stateOld).append(dlm);
      sb.append(resultOld).append(dlm);
      sb.append(groupNew).append(dlm);
      sb.append(sourceNew).append(dlm);
      sb.append(stateNew).append(dlm);
      sb.append(rankNew).append(dlm);
      sb.append(resultNew).append(dlm);
      sb.append(auth).append(dlm);
      sb.append(subsource).append(dlm);
      sb.append(EpochTime.dateToString(lddate));
      return sb.toString();
  } 

  /** Return a compact string representing instance values.
   * Values longer than alloted width are truncated so that
   * the text is column aligned for multiple row dumps.
   */
  public String toOutputString() {
      StringBuffer sb = new StringBuffer(512);
      ProcessControl.appendPaddedField(sb, groupOld, 12);
      ProcessControl.appendPaddedField(sb, sourceOld, 12);
      ProcessControl.appendPaddedField(sb, stateOld, 12);
      ProcessControl.appendPaddedField(sb, String.valueOf(resultOld), 6);
      ProcessControl.appendPaddedField(sb, groupNew, 12);
      ProcessControl.appendPaddedField(sb, sourceNew, 12);
      ProcessControl.appendPaddedField(sb, stateNew, 12);
      ProcessControl.appendPaddedField(sb, ProcessControl.znull(rankNew), 6);
      ProcessControl.appendPaddedField(sb, ProcessControl.znull(resultNew), 6);

      if (auth != null)
          ProcessControl.appendPaddedField(sb, auth, 12);
      if (subsource != null)
          ProcessControl.appendPaddedField(sb, subsource, 12);

      return sb.toString();
  } 

  public static void dump(TransitionRow tr[]) {
      if (tr == null) { 
          System.out.println(" * No entries *");
          return;
      }
      for (int i=0; i < tr.length; i++) dump(tr[i]);
  }

  public static void dump(TransitionRow tr) {
      if (tr == null) return;
      System.out.println(tr.toOutputString());
  }


  private static int doUpdate(String sql) {
      if (debug) System.out.println("DEBUG TransitionRow doUpdate(sql): " + sql);
      return ProcessControl.doUpdate(sql);
  }

  /**
   * Return all Process Table rows that satisfy this input query
   */
  private static TransitionRow[] doQuery(String sql) { 

      if (debug) System.out.println("DEBUG TransitionRow doQuery(sql): " + sql);

      ResultSet rs = null;
      Statement stmt = null;

      TransitionRow row;
      Vector v = new Vector() ;
      boolean status = true;

      try {

        // use existing or make new connection
        stmt = ProcessControl.getConnection().createStatement();

        rs = stmt.executeQuery(sql);

        while ( rs.next() ) {
          row = TransitionRow.parse(rs);
          if (row == null) break;
          v.addElement((Object) row);
        }

      } catch (SQLException ex) {
        ex.printStackTrace(System.out);
        status = false;
      }
      
      finally {
          try {
            if (rs != null) rs.close();
            if (stmt != null) stmt.close();
          }
          catch (SQLException ex) {}
      }

      TransitionRow tr[] = new TransitionRow [v.size()];
      if ( v.size() > 0 ) v.copyInto(tr);
      else status = false;

      return (status) ? tr : null;
      
  }

  /** Parse ResultSet into this TransitionRow */  
  private static TransitionRow parse(ResultSet rs) {
       TransitionRow tr = new TransitionRow();
        try {
          tr.groupOld       = ProcessControl.getStringSafe(rs, "groupOld");
          tr.sourceOld      = ProcessControl.getStringSafe(rs, "sourceOld");
          tr.stateOld       = ProcessControl.getStringSafe(rs, "stateOld");
          tr.resultOld      = rs.getInt("resultOld");
          tr.groupNew       = ProcessControl.getStringSafe(rs, "groupNew");
          tr.sourceNew      = ProcessControl.getStringSafe(rs, "sourceNew");
          tr.stateNew       = ProcessControl.getStringSafe(rs, "stateNew");
          tr.rankNew        = rs.getInt("rankNew");
          tr.resultNew      = rs.getInt("resultNew");

          tr.auth           = ProcessControl.getStringSafe(rs, "auth");
          tr.subsource      = ProcessControl.getStringSafe(rs, "subsource");
          tr.lddate         = rs.getDate("lddate");
          
        } catch ( NullPointerException ex) {
            System.err.println("NullPointerException in parseResults");
            ex.printStackTrace(System.err);
            return null;
        } catch ( SQLException ex) {
            System.err.println("SQLException");
            ex.printStackTrace(System.err);
            return null;
        }

        return tr;
  }

}
