package org.trinet.pcs;
import java.sql.*;
import java.util.*;
import org.trinet.util.EpochTime;
/**
* Structure of the Process Table 'PCS_STATE'
*/
public class StateRow {

    protected static boolean debug = false;

    private static boolean forUpdate = false;

    protected static final String STATE_TABLE = "PCS_STATE";

    protected long   id;                      // NOT NULL
    protected String controlGroup;            // NOT NULL, no empty strings
    protected String sourceTable;             // NOT NULL, no empty strings
    protected String state;                   // NOT NULL, no empty strings
    protected int    rank;                    // NOT NULL
    protected int    result;
    protected long   secondaryId;             // What's this attribute for?
    protected String secondarySourceTable;    // What's this attribute for?
    protected String auth;                    // What's this attribute for?
    protected String subsource;               // What's this attribute for?
    protected java.sql.Date lddate;

    public StateRow() {}        // null constructor

    /**
     * Copy a State Row
     */
    public StateRow(StateRow pr) {
      id =                   pr.id;                    // NOT NULL
      controlGroup =         pr.controlGroup;          // NOT NULL
      sourceTable =          pr.sourceTable;           // NOT NULL
      state =                pr.state;                 // NOT NULL
      rank =                 pr.rank;                  // NOT NULL
      result =               pr.result;
      secondaryId =          pr.secondaryId;
      secondarySourceTable = pr.secondarySourceTable;
      auth =                 pr.auth;
      subsource =            pr.subsource;
      lddate =               pr.lddate;

    }

  /**
   * Select from table row matching this instance's values which define the 
   * primary key for the table (controlGroup, sourceTable, state, id). 
   * If input forUpdate is true then the table row is locked until committed.
   */
    public StateRow select(boolean forUpdate) {
      StateRow [] sr = get(controlGroup, sourceTable, state, id, forUpdate);
      return (sr == null) ? null : sr[0];
    }

  /**
   * Select from table row matching this instance's values which define the 
   * primary key for the table (controlGroup, sourceTable, state, id). 
   */
    public StateRow select() {
        return select(false);
    }

    /**
     * Insert ONE process row into the dbase
     * If row matching key already exists, it is updated with current values.
     */
    public int insert() {
      // check NOT NULL fields
      if ( ! meetsConstraints() ) return 0;

      // Note an insert for a duplicate key results in SQLException
      // Should first check for an existing row with matching key
      // Then do we an update in lieu of insert? -aww
      StateRow sr = select(true);
      // could check here for match with current values of ?
      // if matching values then either skip or update, else insert?
      // do we want a new posting to overwrite any old results?
      if (sr == null) return doUpdate(getInsertString());
      return ((this.rank == sr.rank) && (this.result == sr.result)) ?
           1 : doUpdate(getUpdateString());
    }

    /**
     * For use with UPDATE.
     */
    private String getUpdateString() {
        StringBuffer sb = new StringBuffer(512);
        sb.append("UPDATE ").append(STATE_TABLE);
        sb.append(" SET ");
        sb.append("rank=").append(rank); // changed, used to null 0 -aww 05/10/2007
        sb.append(",result=").append(result); // changed, used to null 0 -aww 05/10/2007
        sb.append(",secondaryId=").append(ProcessControl.znull(secondaryId));
        sb.append(",secondarySourceTable=").append(ProcessControl.toStringSQL(secondarySourceTable));
        sb.append(",auth=").append(ProcessControl.toStringSQL(auth));
        sb.append(",subsource=").append(ProcessControl.toStringSQL(subsource));
        sb.append(",lddate=SYS_EXTRACT_UTC(CURRENT_TIMESTAMP)"); // update lddate here since it's a "new" posting rank or result -aww
        sb.append(" WHERE ");
        sb.append("controlGroup=").append(ProcessControl.toStringSQL(controlGroup));
        sb.append(" AND sourceTable=").append(ProcessControl.toStringSQL(sourceTable));
        sb.append(" AND state=").append(ProcessControl.toStringSQL(state));
        sb.append(" AND id=").append(id); // changed, used to null 0 -aww 05/10/2007

        return sb.toString();
    }

    /**
     * For use with INSERT.
     */
    private String getInsertString() {
        StringBuffer sb = new StringBuffer(512);
        sb.append("INSERT INTO ").append(STATE_TABLE).append(" ");
        sb.append("(id,controlGroup,sourceTable,state,rank,result,"); // wrap to field list
        sb.append("secondaryId,secondarySourceTable,auth,subsource)");
        sb.append(" VALUES (");
        sb.append(id).append(","); // changed, used to null 0 -aww 05/10/2007
        sb.append(ProcessControl.toStringSQL(controlGroup)).append(",");
        sb.append(ProcessControl.toStringSQL(sourceTable)).append(",");
        sb.append(ProcessControl.toStringSQL(state)).append(",");
        sb.append(rank).append(","); // changed, used to null 0 -aww 05/10/2007
        sb.append(result).append(","); // changed, used to null 0 -aww 05/10/2007
        sb.append(ProcessControl.znull(secondaryId)).append(",");
        sb.append(ProcessControl.toStringSQL(secondarySourceTable)).append(",");
        sb.append(ProcessControl.toStringSQL(auth)).append(",");
        sb.append(ProcessControl.toStringSQL(subsource));
        //sb.append(",SYS_EXTRACT_UTC(CURRENT_TIMESTAMP)"); // removed lddate use default value - aww 2008/06/03
        sb.append(")");

        return sb.toString();
    }

    /** Return true if this StateRow meets the NOT NULL constraints. This is a
     *  bit risky because the constraints in the dbase could be changed.*/
     /* This was added 1/14/2002 to stop insert() from causing constraint errors
     * when Transition processed events that were "terminal". That is, were not
     * posted to a new state.
     */
    private boolean meetsConstraints() {
      return ! (
                id <= 0l ||
                rank <= 0 ||
                ProcessControl.isBlank(state) ||
                ProcessControl.isBlank(sourceTable) ||
                ProcessControl.isBlank(controlGroup) 
               );
    }

    /**
     * Insert all these process rows into the dbase
   */
    public static int insertArray(StateRow pr[]) {
      int nrows=0;
      for (int i = 0; i < pr.length; i++) {
         nrows += pr[i].insert();
      }

      return nrows;
    }

  /**
   * Delete from table all rows matching this instance's group,table,state, and id, values.
   */
    public int delete() {
      StringBuffer sb = new StringBuffer(256);
      sb.append("DELETE FROM ").append(STATE_TABLE);
      sb.append(" WHERE ");
      sb.append("controlGroup=").append(ProcessControl.toStringSQL(controlGroup));
      sb.append(" AND sourceTable=").append(ProcessControl.toStringSQL(sourceTable));
      sb.append(" AND state=").append(ProcessControl.toStringSQL(state));
      sb.append(" AND id=").append(id); // changed, used to null 0 -aww 05/10/2007

      return doUpdate(sb.toString());
    }

  /**
   * Delete from table row matching the inputs.
   */
    public static int delete(String cGroup, String sTable, String sState, long eId) {
      StringBuffer sb = new StringBuffer(256);
      sb.append("DELETE FROM ").append(STATE_TABLE);
      sb.append(" WHERE ");
      sb.append("controlGroup=").append(ProcessControl.toStringSQL(cGroup));
      sb.append(" AND sourceTable=").append(ProcessControl.toStringSQL(sTable));
      sb.append(" AND state=").append(ProcessControl.toStringSQL(sState));
      sb.append(" AND id=").append(eId); // changed, used to null 0 -aww 05/10/2007

      return doUpdate(sb.toString());
    }

  /**
   * Delete from table all rows matching the inputs (all states for input id).
   */
    public static int delete(String cGroup, String sTable, String sState) {
      StringBuffer sb = new StringBuffer(256);
      sb.append("DELETE FROM ").append(STATE_TABLE);
      sb.append(" WHERE ");
      sb.append("controlGroup=").append(ProcessControl.toStringSQL(cGroup));
      sb.append(" AND sourceTable=").append(ProcessControl.toStringSQL(sTable));
      sb.append(" AND state=").append(ProcessControl.toStringSQL(sState));

      return doUpdate(sb.toString());
    }

  /**
   * Delete from table all rows matching the inputs (all states for input id).
   */
    public static int delete(String cGroup, String sTable, long eId) {
      StringBuffer sb = new StringBuffer(256);
      sb.append("DELETE FROM ").append(STATE_TABLE);
      sb.append(" WHERE ");
      sb.append("controlGroup=").append(ProcessControl.toStringSQL(cGroup));
      sb.append(" AND sourceTable=").append(ProcessControl.toStringSQL(sTable));
      sb.append(" AND id=").append(eId); // changed, used to null 0 -aww 05/10/2007

      return doUpdate(sb.toString());
    }

  /**
   * Delete from table all rows matching inputs (all states and ids).
   */
    public static int delete(String cGroup, String sTable) {
      StringBuffer sb = new StringBuffer(256);
      sb.append("DELETE FROM ").append(STATE_TABLE);
      sb.append(" WHERE ");
      sb.append("controlGroup=").append(ProcessControl.toStringSQL(cGroup));
      sb.append(" AND sourceTable=").append(ProcessControl.toStringSQL(sTable));

      return doUpdate(sb.toString());
    }

  /**
   * Delete from table all rows matching input (all tables, states and ids).
   */
    public static int delete(String cGroup) {
      StringBuffer sb = new StringBuffer(256);
      sb.append("DELETE FROM ").append(STATE_TABLE);
      sb.append(" WHERE ");
      sb.append("controlGroup=").append(ProcessControl.toStringSQL(cGroup));

      return doUpdate(sb.toString());
    }

  /**
   * Return state table rows sorted in <i>id, state, rank, result desc</i> order.
   * Return null if none.
   */
    public static StateRow[] get() {
        StringBuffer sb = new StringBuffer(256);
        sb.append("SELECT * FROM ").append(STATE_TABLE);
        if (forUpdate) sb.append(" FOR UPDATE");
        sb.append(" ORDER BY id, state, rank, result DESC");

        return doQuery(sb.toString());
    }

  /**
   * Return table rows for all "groups, tables, states" matching input event id sorted in
   * <i>result,rank</i> descending order.
   * Return null if none.
   */
    public static StateRow[] get(long eId) {
        StringBuffer sb = new StringBuffer(256);
        sb.append("SELECT * FROM ").append(STATE_TABLE);
        sb.append(" WHERE ");
        sb.append("id=").append(eId); // changed, used to null 0 -aww 05/10/2007
        if (forUpdate) sb.append(" FOR UPDATE");
        sb.append(" ORDER BY result DESC, rank DESC, controlGroup, sourceTable, state");

        return doQuery(sb.toString());
    }

    public static StateRow[] getInDateOrder(long eId, boolean descending) {
        StringBuffer sb = new StringBuffer(256);
        sb.append("SELECT * FROM ").append(STATE_TABLE);
        sb.append(" WHERE ");
        sb.append("id=").append(eId);
        sb.append(" ORDER BY LDDATE");
        if (descending) sb.append(" DESC");

        return doQuery(sb.toString());
    }

  /**
   * Return table rows for all "table, states, and ids" matching input group sorted in
   * <i>result</i> descending order.
   * Return null if none.
   */
    public static StateRow[] get(String cGroup) {
        StringBuffer sb = new StringBuffer(256);
        sb.append("SELECT * FROM ").append(STATE_TABLE);
        sb.append(" WHERE ");
        sb.append("controlGroup=").append(ProcessControl.toStringSQL(cGroup));
        if (forUpdate) sb.append(" FOR UPDATE");
        sb.append(" ORDER BY result DESC, sourcetable, rank DESC, state, id");

        return doQuery(sb.toString());
    }
  /**
   * Return table rows for all "ids and states" matching input group/table sorted in
   * <i>result</i> descending order.
   * Return null if none.
   */
    public static StateRow[] get(String cGroup, String sTable) {
        StringBuffer sb = new StringBuffer(256);
        sb.append("SELECT * FROM ").append(STATE_TABLE);
        sb.append(" WHERE ");
        sb.append("controlGroup=").append(ProcessControl.toStringSQL(cGroup));
        sb.append(" AND sourceTable=").append(ProcessControl.toStringSQL(sTable));
        if (forUpdate) sb.append(" FOR UPDATE");
        sb.append(" ORDER BY result DESC, rank DESC, state, id");

        return doQuery(sb.toString());
    }

 /** 
   * Return table rows for all "states" matching input group/table/ID sorted in
   * <i>rank</i> descending order.
   * Return null if none.
   */
    public static StateRow[] get(String cGroup, String sTable, long eId) {
        StringBuffer sb = new StringBuffer(256);
        sb.append("SELECT * FROM ").append(STATE_TABLE);
        sb.append(" WHERE ");
        sb.append("controlGroup=").append(ProcessControl.toStringSQL(cGroup));
        sb.append(" AND sourceTable=").append(ProcessControl.toStringSQL(sTable));
        sb.append(" AND id=").append(eId); // changed, used to null 0 -aww 05/10/2007
        if (forUpdate) sb.append(" FOR UPDATE");
        sb.append(" ORDER BY rank DESC");

        return doQuery(sb.toString());
    }

  /**
   * Return table rows for all "ids" matching input group/table/state sorted in
   * <i>result</i> descending order.
   * Return null if none.
   */
    public static StateRow[] get(String cGroup, String sTable, String pState) {
        StringBuffer sb = new StringBuffer(256);
        sb.append("SELECT * FROM ").append(STATE_TABLE);
        sb.append(" WHERE ");
        sb.append("controlGroup=").append(ProcessControl.toStringSQL(cGroup));
        sb.append(" AND sourceTable=").append(ProcessControl.toStringSQL(sTable));
        sb.append(" AND state=").append(ProcessControl.toStringSQL(pState));
        if (forUpdate) sb.append(" FOR UPDATE");
        sb.append(" ORDER BY result DESC, id");

        return doQuery(sb.toString());
    }

  /** 
   * Return a table row with matching input group/table/state/id key. 
   * @return single element StateRow array or null if no match. 
   */
    public static StateRow[] get(String cGroup, String sTable, String pState, long eId) {
        return get(cGroup, sTable, pState, eId, forUpdate);
    }

    public static StateRow[] get(String cGroup, String sTable, String pState, long eId, boolean forUpdate) {
        StringBuffer sb = new StringBuffer(256);
        sb.append("SELECT * FROM ").append(STATE_TABLE);
        sb.append(" WHERE ");
        sb.append("controlGroup=").append(ProcessControl.toStringSQL(cGroup));
        sb.append(" AND sourceTable=").append(ProcessControl.toStringSQL(sTable));
        sb.append(" AND state=").append(ProcessControl.toStringSQL(pState));
        sb.append(" AND id=").append(eId); // changed, used to null 0 -aww 05/10/2007
        if (forUpdate) sb.append(" FOR UPDATE");

        return doQuery(sb.toString());
    }

  /** 
   * Return table rows for all "ids"  matching input group/table/state/result code. 
   * Table result values of 0 and NULL are equivalent to input of 0.
   * @return single element StateRow array or null if no match. 
   */
    public static StateRow[] get(String cGroup, String sTable, String pState, int pResult) {
        return get(cGroup, sTable, pState, pResult, forUpdate);
    }

    public static StateRow[] get(String cGroup, String sTable, String pState, int pResult, boolean forUpdate) {
        StringBuffer sb = new StringBuffer(256);
        sb.append("SELECT * FROM ").append(STATE_TABLE);
        sb.append(" WHERE ");
        sb.append("controlGroup=").append(ProcessControl.toStringSQL(cGroup));
        sb.append(" AND sourceTable=").append(ProcessControl.toStringSQL(sTable));
        sb.append(" AND state=").append(ProcessControl.toStringSQL(pState));
        // NULL as 0 ? better to constrain column as not null and have default value = 0 -aww
        if (pResult == 0) sb.append(" AND (result=0 OR result IS NULL)");
        else sb.append(" AND result=").append(pResult);
        if (forUpdate) sb.append(" FOR UPDATE");

        return doQuery(sb.toString());
    }

  /**
   * Return array of StateRow's containing IDs for the given group/table/state.
   * Rows are sorted by <i>lddate'</i> so that the oldest entries come first.
   * Thus this acts as a FIFO. Returns null if none found.
   */
    public static StateRow[] getNextArray(String cGroup, String sTable, String pState) {
        return getNextArray(cGroup, sTable, pState, 0);
    }

  /**
   * Return array of StateRow's containing IDs for the given group/table/state
   * whose <i>lddate</i> values are older than lag seconds behind SYS_EXTRACT_UTC(CURRENT_TIMESTAMP).
   * Rows are sorted by <i>lddate'</i> so that the oldest entries come first.
   * Thus this acts as a FIFO. Returns null if none found.
   */
    public static StateRow[] getNextArray(String cGroup, String sTable, String pState, int lagSecs) {
        StringBuffer sb = new StringBuffer(256);
        sb.append("SELECT * FROM ").append(STATE_TABLE);
        sb.append(" ST "); // alias
        sb.append(" WHERE ");
        sb.append("controlGroup=").append(ProcessControl.toStringSQL(cGroup));
        sb.append(" AND sourceTable=").append(ProcessControl.toStringSQL(sTable));
        sb.append(" AND state=").append(ProcessControl.toStringSQL(pState));
        sb.append(" AND lddate <= (SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) - (").append(lagSecs).append("/86400.))");
        sb.append(" AND (result = 0 OR result IS NULL)");
        sb.append(" AND rank = (SELECT MAX(rank) FROM pcs_state WHERE id=ST.id)");
        if (forUpdate) sb.append(" FOR UPDATE");
        sb.append(" ORDER BY lddate");

        return doQuery(sb.toString());
    }

  /**
   * Returns <i>true</i> if a row exists in dbase PCS_STATE table matching input
   * <i>eId</i> and state description values with a zero or null result code
   * at a rank greater or equal to the maximum rank for all state row postings
   * for the same id (including those rows having other group, table values).
   */
    public static boolean canProcess(String cGroup, String sTable, String pState, long eId) {
        StringBuffer sb = new StringBuffer(256);
        sb.append("SELECT * FROM ").append(STATE_TABLE);
        sb.append(" ST "); // alias
        sb.append(" WHERE ");
        sb.append("controlGroup=").append(ProcessControl.toStringSQL(cGroup));
        sb.append(" AND sourceTable=").append(ProcessControl.toStringSQL(sTable));
        sb.append(" AND state=").append(ProcessControl.toStringSQL(pState));
        sb.append(" AND id=").append(eId);  // changed, used to null 0 -aww 05/10/2007
        sb.append(" AND (result = 0 OR result IS NULL)");
        sb.append(" AND rank = (SELECT MAX(rank) FROM pcs_state WHERE id=ST.id)");

        StateRow [] results = doQuery(sb.toString());
        return  (results != null && results.length >= 1);
    }

  /**
   * Return one StateRow containing an ID for the given group/table/state.
   * The lowest ID is returned.
   */
    public static StateRow getNext(String cGroup, String sTable, String pState) {
        return getNext(cGroup, sTable, pState, 0);
    }

  /**
   * Return one StateRow containing an ID for the given group/table/state
   * whose posting is older than lag seconds behind SYS_EXTRACT_UTC(CURRENT_TIMESTAMP).
   * The lowest ID is returned.
   */
    public static StateRow getNext(String cGroup, String sTable, String pState, int lagSecs) {
       StateRow[] sr = getNextArray(cGroup, sTable, pState, lagSecs);
       return (sr == null) ? null : sr[0];  // only return 1st in the list

     }
  /**
   * Return next ID for the given group/table/state.
   * If no id's returns 0.
   */
    public static long getNextId(String cGroup, String sTable, String pState) {
        return getNextId(cGroup, sTable, pState, 0);
    }

  /**
   * Return next ID for the given group/table/state
   * whose posting is older than lag seconds behind SYS_EXTRACT_UTC(CURRENT_TIMESTAMP).
   * If no id's returns 0.
   */
    public static long getNextId(String cGroup, String sTable, String pState, int lagSecs) {
       StateRow sr = getNext(cGroup, sTable, pState, lagSecs);
       return (sr == null) ? 0l : sr.id; 

     }

  /**
   * Return all State table rows with negative result codes. These are
   * likely to be processing errors 
   */
    public static StateRow[] getNegativeResults() {
        StringBuffer sb = new StringBuffer(256);
        sb.append("SELECT * FROM ").append(STATE_TABLE);
        sb.append(" WHERE (result < 0 )"); 
        if (forUpdate) sb.append(" FOR UPDATE");
        sb.append(" ORDER BY controlgroup, sourcetable, rank DESC, state, result DESC, id");

        return doQuery(sb.toString());
    }

  /**
   * Return all State table rows with non-zero result codes.
   * These are likely to require transitions.
   * All processing states found in table are included.
   */
    public static StateRow[] getNonNullResults(boolean forUpdate) {
        StringBuffer sb = new StringBuffer(256);
        sb.append("SELECT * FROM ").append(STATE_TABLE);
        sb.append(" WHERE (result <> 0 AND result IS NOT NULL)"); // bug fixed used to be "OR" - aww 05/16/2005
        if (forUpdate) sb.append(" FOR UPDATE");
        sb.append(" ORDER BY result DESC");

        return doQuery(sb.toString());
    }

  /**
   * Return all State table rows with non-zero result codes. These are
   * likely to require transitions.
   * Only those states in input group description are included.
   */
    public static StateRow[] getNonNullResults(String cGroup, String sTable, boolean forUpdate) {
        StringBuffer sb = new StringBuffer(256);
        sb.append("SELECT * FROM ").append(STATE_TABLE);
        sb.append(" WHERE ");
        sb.append("controlGroup=").append(ProcessControl.toStringSQL(cGroup));
        sb.append(" AND sourceTable=").append(ProcessControl.toStringSQL(sTable));
        sb.append(" AND (result <> 0 AND result IS NOT NULL)"); // bug fixed used to by "OR" - aww 05/16/2005
        if (forUpdate) sb.append(" FOR UPDATE");
        sb.append(" ORDER BY result DESC");

        return doQuery(sb.toString());
    }

  /**
   * Return all State table rows with non-zero result codes.
   * These are likely to require transitions.
   * Results for the input state (one state) are included.
   *
   */
    public static StateRow[] getNonNullResults(String cGroup, String sTable, String pState, boolean forUpdate) {
        StringBuffer sb = new StringBuffer(256);
        sb.append("SELECT * FROM ").append(STATE_TABLE);
        sb.append(" WHERE ");
        sb.append("controlGroup=").append(ProcessControl.toStringSQL(cGroup));
        sb.append(" AND sourceTable=").append(ProcessControl.toStringSQL(sTable));
        sb.append(" AND state=").append(ProcessControl.toStringSQL(pState));
        sb.append(" AND (result <> 0 AND result IS NOT NULL)"); // bug fixed used to be "OR" - aww 05/16/2005
        if (forUpdate) sb.append(" FOR UPDATE");
        sb.append(" ORDER BY result DESC");

        return doQuery(sb.toString());
    }

    private static int doUpdate(String sql) {
      if (debug) System.out.println ("DEBUG StateRow doUpdate(sql): "+sql);
      return ProcessControl.doUpdate(sql);
    }

  /**
   * Return all State Table rows that satisfy this 'sql' query
   */
    private static StateRow[] doQuery(String sql) {
      if (debug) System.out.println("StateRow doQuery(sql): " + sql);

      ResultSet rs = null;
      Statement stmt = null;

      StateRow prRow = null;
      Vector v = new Vector() ;
      boolean status = true;

      try {

        // use existing or make new connection
        stmt = ProcessControl.getConnection().createStatement();

        rs = stmt.executeQuery(sql);

        while ( rs.next() ) {
          prRow = StateRow.parse(rs);
          if (prRow == null) break;

          v.addElement((Object) prRow);
        }

      } catch (SQLException ex) {
        ex.printStackTrace(System.err);
        status = false;
      }
      finally {
          try {
            if (rs != null) rs.close();
            if (stmt != null) stmt.close();
          }
          catch (SQLException ex) {}
      }
  

      // Copy resulting vector into array for return
      StateRow [] pr = new StateRow [v.size()];

      if ( v.size() > 0) v.copyInto(pr);
      else status = false;

      return (status) ? pr : null;
    }

  /** Return a compact string representing instance values.
   * Values longer than alloted width are truncated so that
   * the text is column aligned for multiple row dumps.
   */
    public String toOutputString() {
        return toOutputString(false);
    }
    public String toOutputString(boolean includeDate) {
      StringBuffer sb = new StringBuffer(132);
      ProcessControl.appendPaddedField(sb, String.valueOf(id), 15);
      ProcessControl.appendPaddedField(sb, controlGroup, 15);
      ProcessControl.appendPaddedField(sb, sourceTable, 15);
      ProcessControl.appendPaddedField(sb, state, 15);
      ProcessControl.appendPaddedField(sb, String.valueOf(rank), 6);
      ProcessControl.appendPaddedField(sb, String.valueOf(result), 6);
      if (includeDate) {
          sb.append(" ");
          sb.append(EpochTime.dateToString(lddate).substring(0,19));
      }
      return sb.toString();
    }

    /* All String values have same maximum default width.
    public String toOutputString() {
      StringBuffer sb = new StringBuffer(132);
      ProcessControl.appendPaddedField(sb, String.valueOf(id));
      ProcessControl.appendPaddedField(sb, controlGroup);
      ProcessControl.appendPaddedField(sb, sourceTable);
      ProcessControl.appendPaddedField(sb, state);
      ProcessControl.appendPaddedField(sb, String.valueOf(rank));
      ProcessControl.appendPaddedField(sb, String.valueOf(result));
      return sb.toString();
    }
    */

    /** Comma delimited list of the attribute values of this instance. */
    public String toString() {
      StringBuffer sb = new StringBuffer(256);
      String dlm = ",";
      sb.append(id).append(dlm);
      sb.append(controlGroup).append(dlm);
      sb.append(sourceTable).append(dlm);
      sb.append(state).append(dlm);
      sb.append(rank).append(dlm);
      sb.append(result).append(dlm);
      sb.append(secondaryId).append(dlm);
      sb.append(secondarySourceTable).append(dlm);
      sb.append(auth).append(dlm);
      sb.append(subsource).append(dlm);
      sb.append(EpochTime.dateToString(lddate));
      return sb.toString();
    }

    public static void dump(StateRow sr[]) {
      if (sr == null) {
          System.out.println(" * No entries *");
          return;
      }
      for (int i=0; i < sr.length; i++) {
          dump(sr[i]);
      }
    }

    public static void dump(StateRow sr) {
      if (sr == null) return;
      System.out.println(sr.toOutputString());
    }

  /** Parse resultset into this StateRow */
    private static StateRow parse(ResultSet rs) {
      StateRow pr = new StateRow();
        try {

          pr.id                   = rs.getLong("id");
          pr.controlGroup         = ProcessControl.getStringSafe(rs, "controlGroup");
          pr.sourceTable          = ProcessControl.getStringSafe(rs, "sourceTable");
          pr.state                = ProcessControl.getStringSafe(rs, "state");
          pr.rank                 = rs.getInt("rank");
          pr.result               = rs.getInt("result");
          pr.secondaryId          = rs.getLong("secondaryId");

          pr.secondarySourceTable = ProcessControl.getStringSafe(rs, "secondarySourceTable");
          pr.auth                 = ProcessControl.getStringSafe(rs, "auth");
          pr.subsource            = ProcessControl.getStringSafe(rs, "subsource");
          pr.lddate               = rs.getDate("lddate");

        } catch ( NullPointerException ex) {
            System.err.println("NullPointerException in parseResults");
            ex.printStackTrace(System.err);
            return null;
        } catch ( SQLException ex) {
            System.err.println("SQLException");
            ex.printStackTrace(System.err);;
            return null;
        }

        return pr;
    }
}
