package org.trinet.pcs;
import java.util.*;
import org.trinet.jasi.*;
import org.trinet.jasi.engines.*;
import org.trinet.jasi.magmethods.MagnitudeMethodIF;
import org.trinet.util.gazetteer.*;
import org.trinet.util.gazetteer.TN.GazetteerType;
import org.trinet.util.locationengines.LocationEngineIF;
import org.trinet.util.BenchMark;
import org.trinet.util.DateTime;
import org.trinet.util.Format;
import org.trinet.util.GenericPropertyList;
import org.trinet.util.StringList;
import java.net.InetAddress;
import java.net.UnknownHostException;

import org.trinet.jasi.picker.AbstractPicker;
import org.trinet.jasi.picker.PickEW;
import org.trinet.jasi.picker.PickerParmIF;
import org.trinet.jasi.picker.PhasePickerIF;
import org.trinet.jasi.picker.TriaxialPickerIF;
import org.trinet.jasi.picker.TriaxialPicker2;

// Since loc creates stale prefmag, do we need a mode that does location and summary_mag combo for commit? 
/**
 * Processing mode String names: <p>
 * "LOCATE", "LOCATE_MAG", "MAG", "LOCATE_WF_MAG", "WF_MAG"<p>
 * are valid values set for property <i>pcsProcessorModeName</i> 
 * in the text file used to initialize the PcsPropertyList used by 
 * a HypoMagProcessingController object instance.
 * */
public class HypoMagSolutionProcessor extends AbstractSolutionChannelDataProcessor {

    protected static final BenchMark bm = new BenchMark(System.err);
    // class static data enumeration:
    public static final int LOCATE_MODE_ID             = 0;
    public static final int LOCATE_MAG_MODE_ID         = 1;
    public static final int MAG_MODE_ID                = 2;
    // scan waveforms for channel mag data:
    public static final int LOCATE_WF_MAG_MODE_ID      = 3;
    public static final int WF_MAG_MODE_ID             = 4;

    // auto picking + relocation
    public static final int PICK_LOC_MODE_ID           = 5;
    public static final int PICK_LOC_MAG_MODE_ID       = 6;
    public static final int PICK_LOC_WF_MAG_MODE_ID    = 7;

    int maxPwt = 4;
    int maxSwt = 4;

    double minLocZ = -9.;
    boolean autoSetQuarryType = false;
    double originDeltaToCommit = 0.;
    double fixQuarryDepth = Double.NaN;

    String allowedMagType = null;
    double minMag = -9.;
    double maxMag = 9.;
    double maxPickResid = -1.;
    boolean reloadParmsPerEvent = false;
    //double  reloadParmsSecs = 86400.;
    boolean useAltSuccessCode = false;
    boolean checkCommitRflag = false; // e.g. when running in continuous post-RT mode aww 2015/04/21

    protected PcsChannelListEngineDelegateIF delegate = null;

    protected boolean staleSolCommitOk = false;
    protected boolean staleMagCommitOk = false;
    protected boolean magStaleCommitNoop = false;

    protected boolean autoCommit = false;

    protected PhasePickerIF picker = null;
    private int pickCnt = 0;
    private int addCnt = 0;
    private ChannelTimeWindowModel ctwModel = null; // PowerLaw or DataSource

    //private long lastParmsReadTime = System.currentTimeMillis();

    // relocate only, create new origin
    public static final SolutionProcessingMode LOCATE = 
           new SolutionProcessingMode("LOCATE", LOCATE_MODE_ID);

    // relocate, no waveform scans, new summary mag derived from channelmag data in db
    public static final SolutionProcessingMode LOCATE_MAG = 
           new SolutionProcessingMode("LOCATE_MAG", LOCATE_MAG_MODE_ID);

    // no relocation, no waveform scans, new summary mag derived from channelmag data in db
    public static final SolutionProcessingMode MAG =
           new SolutionProcessingMode("MAG", MAG_MODE_ID);

    // relocate, if success, scan waveforms to derive new channel + summary mags
    public static final SolutionProcessingMode LOCATE_WF_MAG=
           new SolutionProcessingMode("LOCATE_WF_MAG", LOCATE_WF_MAG_MODE_ID);

    // no relocation, scan waveforms to derive new channel + summary mags
    public static final SolutionProcessingMode WF_MAG =
           new SolutionProcessingMode("WF_MAG", WF_MAG_MODE_ID);

    // repick, relocate
    public static final SolutionProcessingMode PICK_LOC =
           new SolutionProcessingMode("PICK_LOC", PICK_LOC_MODE_ID);

    // repick, relocate, no waveform scans, new summary mag derived from channelmag data in db
    public static final SolutionProcessingMode PICK_LOC_MAG =
           new SolutionProcessingMode("PICK_LOC_MAG", PICK_LOC_MAG_MODE_ID);

    // repick, relocate, and scan waveforms to derive new channel + summary mags
    public static final SolutionProcessingMode PICK_LOC_WF_MAG =
           new SolutionProcessingMode("PICK_LOC_WF_MAG", PICK_LOC_WF_MAG_MODE_ID);


    // for each instance:
    {
        modeMap.put(new Integer(LOCATE_MODE_ID), LOCATE);
        modeMap.put(new Integer(LOCATE_MAG_MODE_ID), LOCATE_MAG);
        modeMap.put(new Integer(MAG_MODE_ID), MAG);
        modeMap.put(new Integer(LOCATE_WF_MAG_MODE_ID), LOCATE_WF_MAG);
        modeMap.put(new Integer(WF_MAG_MODE_ID), WF_MAG);
        modeMap.put(new Integer(PICK_LOC_MODE_ID), PICK_LOC);
        modeMap.put(new Integer(PICK_LOC_MAG_MODE_ID), PICK_LOC_MAG);
        modeMap.put(new Integer(PICK_LOC_WF_MAG_MODE_ID), PICK_LOC_WF_MAG);
    }

    private WhereIsEngine whereEngine;
    private String [] qlist = null;

    /** No-op Constructor. */
    public HypoMagSolutionProcessor() { }

    /**
     * Constructor initializes HypoMagEngineDelegate with property list and
     * sets LOCATE_WF_MAG as the mode (default) for processing.
     */
    public HypoMagSolutionProcessor(SolutionWfEditorPropertyList props) {
        //this(props, SolutionProcessingMode.UNKNOWN);
        this(props, LOCATE_WF_MAG); // what should the default behavior be?
    }

    /**
     * Constructor initializes HypoMagEngineDelegate with input property list and
     * sets mode (default) for processing to the specified input mode.
     */
    public HypoMagSolutionProcessor(SolutionWfEditorPropertyList props, SolutionProcessingMode defaultMode) {
        super(props, defaultMode);
        initFromProperties();
        initDelegate(props);
    }

    protected boolean initFromProperties() {

        if (! super.initFromProperties()) return false;

        if (props.isSpecified("pcsAutoCommit"))
          autoCommit = props.getBoolean("pcsAutoCommit");

        if (props.isSpecified("solStaleCommitOk"))
          staleSolCommitOk = props.getBoolean("solStaleCommitOk");

        if (props.isSpecified("magStaleCommitOk"))
          staleMagCommitOk = props.getBoolean("magStaleCommitOk");

        // If processing cause the preferred magnitude to become stale setting
        // Magnitude.staleCommitOk=true && Magnitude.staleCommitNoop=true
        // results in mag.commit() doing a noop, returning true
        // this enables preservation of the old prefmag relation
        if (props.isSpecified("magStaleCommitNoop"))
          magStaleCommitNoop = props.getBoolean("magStaleCommitNoop");

        Magnitude.staleCommitNoop = magStaleCommitNoop;


        if (props.isSpecified("solCommitOriginError")) // ? aww 07/12/2006 is this needed or do we want to alway save ?
          Solution.commitOriginError = props.getBoolean("solCommitOriginError");

        if (verbose && autoCommit) {
              // Notify user about db processing configuration
              System.out.println("--- commit STALE solutions : " + props.getBoolean("solStaleCommitOk"));
              System.out.println("--- commit STALE magnitude : " + props.getBoolean("magStaleCommitOk"));
              System.out.println("--- commit STALE mag noop  : " + props.getBoolean("magStaleCommitNoop"));
              System.out.println("--- commit ALT  magnitudes : " + props.getBoolean("solCommitAllMags"));
              System.out.println("--- commit ORIGIN_ERROR    : " + props.getBoolean("solCommitOriginError"));
        }

        maxPwt = props.getInt("picker.maxPwt", 4);
        maxSwt = props.getInt("picker.maxSwt", 4);

        allowedMagType = props.getProperty("pcsProcessorMagTypes");
        minMag = props.getDouble("pcsMinMag", -9.);
        maxMag = props.getDouble("pcsMaxMag", 9.);
        minLocZ = props.getDouble("minLocationDepth", -9.); 
        fixQuarryDepth = props.getDouble("fixQuarryDepth");
        originDeltaToCommit = props.getDouble("pcsOriginDeltaToCommit", 0.);
        maxPickResid = props.getDouble("pickStripValue", -1.); 
        //reloadParmsSecs = props.getDouble("picker.reloadParmsSecs", 9999999.);

        autoSetQuarryType = props.getBoolean("autoSetQuarryType", false);
        if ( autoSetQuarryType )  {
            qlist = props.getStringList("autoQuarryNames").toArray();
        }

        reloadParmsPerEvent = props.getBoolean("picker.reloadParmsPerEvent", false); // for event event read the parms file
        useAltSuccessCode = props.getBoolean("pcsUseAltSuccessCode", false);
        checkCommitRflag = props.getBoolean("pcsCheckCommitRflag", false); // aww 2015/04/21

        return true;
    }

    /** Initialize a EngineDelegate with input properties. */
    protected boolean initDelegate(SolutionWfEditorPropertyList props) {
        //
        // if (props == null) return false; // force null pointer exception
        //
        if (delegate == null) {
          if (props.isSpecified("pcsEngineDelegate")) {
              delegate = (PcsChannelListEngineDelegateIF) JasiObject.newInstance(props.getProperty("pcsEngineDelegate"));
          }
          if (delegate == null)
              throw new IllegalStateException("initDelegate: delegate instance null, check properties!");
        }
        String fileName = null;
        if (props.isSpecified("pcsEngineDelegateProps")) 
            //fileName = props.getUserFileNameFromProperty("pcsEngineDelegateProps"); // only with delegate loadProperties
            fileName = props.getProperty("pcsEngineDelegateProps"); // use with overridden setDelegate method below

        boolean status = false;
        if (fileName != null)
            status = delegate.setDelegateProperties(fileName);  // NOTE: should declare velocity model props here
        else
            status = delegate.setDelegateProperties(props);  // otherwise, should declare velocity model props here

        if (verbose) dumpDelegateProperties();

        return status;
    }

    public boolean setProperties(SolutionEditorPropertyList props) {
        // Note props must be SolutionWfEditor subclass to use scanNoiseType and channelTimeWindowModel and waveserver props
        SolutionWfEditorPropertyList newProps = new SolutionWfEditorPropertyList();
        newProps.setProperties(props);
        boolean status = newProps.setup();
        if (status) status = super.setProperties(newProps);
        return ( status && initDelegate(newProps) );
    }

    //IF defer to delegate
    public boolean hasChannelList() {
        return delegate.hasChannelList();
    }

    //IF defer to delegate
    public void setChannelList(ChannelList chanList) {
        if (delegate != null) delegate.setChannelList(chanList);
        if (ctwModel != null) ctwModel.setCandidateList(((AbstractChannelListProcessingController)getProcessingController()).getChannelList());
        if (picker != null ) {
            if (ctwModel != null) picker.setCandidateList(ctwModel.getCandidateList());
            else picker.setCandidateList(((AbstractChannelListProcessingController)getProcessingController()).getChannelList());
        }
    }

    //IF
    /** Process input Solution with the HypoMagEngineDelegate as configured.*/
    public int processSolution(Solution sol)  {

        if (sol == null) return ProcessingResult.NULL_INPUT.getIdCode();

        if (delegate == null) {
            System.err.println(getClass().getName()+" ERROR: processSolution, null engine delegate!");
            return ProcessingResult.NO_ENGINE_DELEGATE.getIdCode();
        }

        long evid = sol.getId().longValue();

        if ( sol.isOriginGType(GTypeMap.TELESEISM) ) {
            System.out.println ("SKIP EVID: "+evid+", no processing done, event origin gtype is teleseism");
            System.out.println("-----------------------------------------------------");        
            return ProcessingResult.DISQUALIFIED.getIdCode();
        }

/*
        boolean doit = true; // by default -aww 2014/01/17
        boolean isPrimary = true;
        // Added logic to filter events, if running by AQMS app host role
        if (props.getBoolean("pcsCheckHostAppRole")) { // do we doit?
            String myhostname = "unknown";
            try {
              myhostname = InetAddress.getLocalHost().getHostName();
            } catch (UnknownHostException e) { }

            int idx = myhostname.indexOf(".");
            if (idx > 0) {
                myhostname = myhostname.substring(0, idx);
            }

            String myDbHostAppRole = getProcessingController().getHostAppRole(); // where role maps to mode of P, S, A or *
            if (debug) {
                System.out.println("DEBUG HypoMagSolutionProcessor myhostname : " + myhostname + " myDbAppRole: " + myDbHostAppRole);
            }

            isPrimary = DataSource.isPrimaryHost(myhostname);
            if ( isPrimary ) { // it's the primary host
                // if app mode is All, Any, or Primary doit, but don't doit if app mode is run on shadow host
                doit = !myDbHostAppRole.startsWith("S");
            }
            else { // host is the shadow standby 
                // if app mode is All, Any, or Shadow doit, but don't if app mode is run on primary host 
                doit = !myDbHostAppRole.startsWith("P");
            }
        }
        System.out.println("-----------------------------------------------------"); 
        System.out.println("EVID: "+evid+ " HypoMag processing start @ "+ new DateTime().toString()); // UTC time
        if (verbose) System.out.println (sol.toNeatString());

        if (!doit) { // a no-op, no processing, just return to result posting
            System.out.println ("SKIP EVID: "+evid+", no  processing done, db host app role: " + getProcessingController().getHostAppRole() +
                    " current host role: " + ((isPrimary) ? "P" : "S") );
            System.out.println("-----------------------------------------------------");        
            return ProcessingResult.DISQUALIFIED.getIdCode();
        }
*/

        // NOTE: event.prefmag magtype may be different than magtype to be processed by delegate for *MAG* mode
        sol.loadPrefMags();

        if ( ! sol.isEventDbType(EventTypeMap3.TRIGGER) ) {
          double mag = -9.;
          if (sol.magnitude != null) {
            mag = sol.magnitude.value.doubleValue();
          }
          if (debug) {
              System.out.println("DEBUG HypoMagSolutionProcessor event mag: " + mag + " pcsMinMag : " + minMag + " pcsMaxMag: " + maxMag);
          }

          if ( minMag > -9.  && (!Double.isNaN(mag) && mag < minMag) ) {
            System.out.println ("SKIP EVID: "+evid+", no processing done, event magnitude " + mag + " < " + minMag + " minMag acceptable");
            System.out.println("-----------------------------------------------------");        
            return ProcessingResult.DISQUALIFIED.getIdCode();
          }

          if ( !Double.isNaN(mag) && mag > maxMag ) {
            System.out.println ("SKIP EVID: "+evid+", no processing done, event magnitude " + mag + " > " + maxMag + " maxMag acceptable");
            System.out.println("-----------------------------------------------------");        
            return ProcessingResult.DISQUALIFIED.getIdCode();
          }

        }

        SolutionProcessingMode mode = getProcessingMode();

        // Do "one-shot" magnitude by type, otherwise "complex" error and
        // result state handling ensues, if multiple types are involved,
        // a commit per mag to might save intermediate results -aww
        String magType = allowedMagType;
        if (magType == null) {
            System.err.println(getClass().getName() +
                    " ERROR Missing input property: \"pcsProcessorMagTypes\"");
            return ProcessingResult.MISSING_PROPERTY.getIdCode();
        }

        boolean status = true;
        int resultCode = ProcessingResult.UNIT_SUCCESS.getIdCode();

        EnvironmentInfo.setApplicationName(getAppName());
        //System.out.println("DEBUG APPNAME before: " + EnvironmentInfo.getApplicationName());

        if (bm != null) bm.reset();

        if ( mode == LOCATE) {
            resultCode = doLocation(sol); // returns DISQUALIFIED if location change < threshold 
        }
        else if ( mode == LOCATE_MAG) { // relocate, no mag, beware stale commit!
            resultCode = doLocation(sol);
            if (resultCode == ProcessingResult.UNIT_SUCCESS.getIdCode() ) {
                resultCode = doSummaryMag(sol, magType); 
            }
        }
        else if ( mode == LOCATE_WF_MAG) { // relocate and scan waveforms for new mag
            resultCode = doLocation(sol);
            if (resultCode == ProcessingResult.UNIT_SUCCESS.getIdCode() ) {
                resultCode = doWaveformMag(sol, magType); 
            }
        }
        else if (mode == WF_MAG) { // scan waveforms for new mag
            resultCode = doWaveformMag(sol, magType); 
        }
        else if (mode == MAG) { // use existing channel mag data in db for new mag
            resultCode = doSummaryMag(sol, magType);
        }
        else if (mode == PICK_LOC) { // scan waveforms for new picks, relocate
            resultCode = doPicker(sol); // returns DISQUALIFIED if picks < threshold 
            if (resultCode == ProcessingResult.UNIT_SUCCESS.getIdCode() ) {
                resultCode = doLocation(sol); // returns DISQUALIFIED if location change < threshold 
                if (resultCode == ProcessingResult.UNIT_SUCCESS.getIdCode() ) {
                    resultCode = doStripRelocation(sol);
                }
            }
        } 
        else if (mode == PICK_LOC_MAG) { // scan waveforms for picks, relocate, and then use existing channel mag data in db for new mag
            resultCode = doPicker(sol); // returns DISQUALIFIED if picks < threshold 
            if (resultCode == ProcessingResult.UNIT_SUCCESS.getIdCode() ) {
                resultCode = doLocation(sol); // returns DISQUALIFIED if location change < threshold 
                if (resultCode == ProcessingResult.UNIT_SUCCESS.getIdCode() ) {
                    resultCode = doStripRelocation(sol);
                }
            }
            if (resultCode == ProcessingResult.UNIT_SUCCESS.getIdCode() ) {
                resultCode = doSummaryMag(sol, magType);
            }
        }
        else if (mode == PICK_LOC_WF_MAG) { // scan waveforms for picks, relocate, and then scan waveforms for new mags
            resultCode = doPicker(sol); // returns DISQUALIFIED if picks < threshold 
            if (resultCode == ProcessingResult.UNIT_SUCCESS.getIdCode() ) {
                resultCode = doLocation(sol); // returns DISQUALIFIED if location change < threshold 
                if (resultCode == ProcessingResult.UNIT_SUCCESS.getIdCode() ) {
                    resultCode = doStripRelocation(sol);
                }
            }
            if (resultCode == ProcessingResult.UNIT_SUCCESS.getIdCode() ) {
                resultCode = doWaveformMag(sol, magType); 
            }
        }
        else {
            System.err.println("ERROR Unknown processing mode: " + mode.toString());
            resultCode = ProcessingResult.UNKNOWN_PROCESSING_MODE.getIdCode();
        }

        if (resultCode == ProcessingResult.UNIT_SUCCESS.getIdCode() && autoCommit) {
            // if mode == ?? then what kind of commit? perhaps need to check solution's data state
            resultCode = checkCommitConditions(sol);
        }
        EnvironmentInfo.setApplicationName(getProcessingController().getAppName());
        //System.out.println("DEBUG APPNAME after: " + EnvironmentInfo.getApplicationName());

        //System.out.println("INFO HypoMag: processing for " +evid+ " ends @ "+ new DateTime().toString()); // UTC time
        bm.printTimeStamp("INFO HypoMag: processing for " + evid + " DONE, end result code: " + resultCode, System.out);
        bm.reset();

        return resultCode;

    }

    private int doStripRelocation(Solution sol) {

         int resultCode = ProcessingResult.UNIT_SUCCESS.getIdCode();

         if ( maxPickResid < 0. ) return resultCode;

         int knt = sol.getPhaseList().stripByResidual(maxPickResid);
         if ( knt <= 0 ) return resultCode;

         resultCode = doLocation(sol); // returns DISQUALIFIED if location change < threshold 

         knt = sol.getPhaseList().stripByResidual(maxPickResid);
         if ( knt <= 0 ) return resultCode;

         // do a second pass, in case location moved alot, the first time
         resultCode = doLocation(sol); // returns DISQUALIFIED if location change < threshold 

         return ( resultCode == ProcessingResult.DISQUALIFIED.getIdCode() ) ? 
             ProcessingResult.UNIT_SUCCESS.getIdCode() : resultCode;
    }

    private int checkCommitConditions(Solution sol) {

            //int resultCode = ProcessingResult.UNIT_SUCCESS.getIdCode();
            int resultCode = ( useAltSuccessCode ) ?
                ProcessingResult.ALT_SUCCESS.getIdCode() : ProcessingResult.UNIT_SUCCESS.getIdCode(); 

            if ( checkCommitRflag ) { // do another db query to get latest origin rflag value, reset result if its CANCELLED - aww 2015/04/21
                Solution checkSol = Solution.create().getById(sol.getId().longValue());
                if ( checkSol.isCancelled() ) resultCode = ProcessingResult.DB_COMMIT_RFLAG_CANCELLED.getIdCode();
            }
            else if (sol.isStale() && ! staleSolCommitOk) {
              resultCode = ProcessingResult.DB_COMMIT_SOLUTION_STALE.getIdCode();
            }
            else if (sol.hasStaleMagnitude() && ! staleMagCommitOk) {
              resultCode = ProcessingResult.DB_COMMIT_MAGNITUDE_STALE.getIdCode();
              sol.getMagList().dump();
            }

            return resultCode;
    }

    private int doWaveformMag(Solution sol, String magType) {
        //System.out.println("INFO HypoMag: doWaveformMag for " +evid+ " start @ "+ new DateTime().toString()); // UTC time
        //return ( ((HypoMagEngineDelegateIF) delegate).calcMagFromWaveforms(sol, magType) ) ?  ProcessingResult.UNIT_SUCCESS.getIdCode() : ProcessingResult.MAG_CALC_FAILURE.getIdCode();
        long evid = sol.getId().longValue(); 
        BenchMark bm = new BenchMark(System.err);
        bm.printTimeStamp("INFO HypoMag: doWaveformMag for " +evid+ " start");
        boolean status = ((HypoMagEngineDelegateIF) delegate).calcMagFromWaveforms(sol, magType);
        bm.printTimeStamp("INFO HypoMag: doWaveformMag for " +evid+ " end");
        return ( status ) ?  ProcessingResult.UNIT_SUCCESS.getIdCode() : ProcessingResult.MAG_CALC_FAILURE.getIdCode();
    }

    private int doSummaryMag(Solution sol, String magType) {
        return ( ((HypoMagEngineDelegateIF) delegate).calcMag(sol, magType) ) ?
            ProcessingResult.UNIT_SUCCESS.getIdCode() : ProcessingResult.MAG_CALC_FAILURE.getIdCode();
    }

    // Note: returns DISQUALIFIED if Solution location change < threshold defined by filter property 
    // Note: User can disable preferred magnitude stale commit by setting input property 
    private int doLocation(Solution sol) {
        // Doing location makes its associated preferred magnitude stale 
        // if staleOk && Magnitude.staleCommitNoop=true mag.commit() does noop (returns true)
        // for example, if solution is committed, but prefmag is not recalculated.
        // see AbstractSolutionProcessingController.processSolution(Solution) code.
        // cf.  Magnitude.staleCommitNoop = props.getBoolean("magStaleCommitNoop");

        int resultCode = ProcessingResult.UNIT_SUCCESS.getIdCode();

        // Save old location for comparison with relocation
        LatLonZ priorLatLonZ = sol.getLatLonZ();

        // Run new location
        BenchMark bm = new BenchMark(System.err);
        long evid = sol.getId().longValue(); 
        bm.printTimeStamp("INFO HypoMag: doLocation for " +evid+ " start");
        boolean tf = ((HypoMagEngineDelegateIF) delegate).locate(sol);
        bm.printTimeStamp("INFO HypoMag: doLocation for " +evid+ " end");

        if ( !tf ) { // failed return
            return ProcessingResult.LOC_CALC_FAILURE.getIdCode();
        }

        double newMdepth = sol.getModelDepth();  // should be set after parsing new location from arc summary -aww 2015/06/11

        // Is input event local/regional or other ?
        boolean locreg = ( sol.isOriginGType(GTypeMap.LOCAL) || sol.isOriginGType(GTypeMap.REGIONAL) );

        if ( locreg ) {
            if ( autoSetQuarryType ) { // check db to see if it matches quarry criteria
                if ( sol.isQuarry() ) {

                    System.out.println("INFO HypoMag solution: " +sol.getId().longValue() + "  set db event type to quarry (qb)\n");
                    locreg = false;

                    String placeString = null;
                    boolean doSetQuarry = false;

                    if (whereEngine == null) {
                      whereEngine = WhereIsEngine.create(props.getProperty("WhereIsEngine", "org.trinet.util.gazetteer.TN.WheresFrom"));
                        whereEngine.setConnection(DataSource.getConnection());
                    }

                    if ( whereEngine != null ) { //  have Where engine do db check
                      LatLonZ latlonz = sol.getLatLonZ();
                      whereEngine.setReference(latlonz.getLat(), latlonz.getLon(), 0.);
                      WhereSummary ws = (WhereSummary)whereEngine.whereSummaryType(GazetteerType.QUARRY);
                      if (ws == null) {
                           System.out.println("WARNING HypoMag for: " +sol.getId().longValue() + " unable to get GazetteerType.QUARRY item from db !!!");
                      }
                      else {
                         placeString = ws.fromPlaceString(false);
                         if ( qlist == null || qlist.length == 0 ) {
                             doSetQuarry = true;
                         }
                         else {
                             for ( int ii =0; ii<qlist.length; ii++ ) {
                                 if  (placeString.indexOf(qlist[ii]) >= 0 ) {
                                     doSetQuarry = true;
                                     break;
                                 }
                             }
                         }

                         if (doSetQuarry) { // set etype qb
                             sol.setEventType(EventTypeMap3.fromDbType(EventTypeMap3.QUARRY));
                             if ( placeString != null && placeString.length() > 0 ) { // have matching quarry place name
                                 Format f = new Format("%7.1f");
                                 placeString += " " + f.form(ws.getDistanceAzimuthElevation().getDistanceKm()).trim() + " km "
                                     + GeoidalConvert.directionString(ws.getDistanceAzimuthElevation().getAzimuth()).trim();
                                 sol.setComment(placeString);
                             }
                         }
                      }
                    }
                }
            }
        }

        // Don't relocate again unless depth changes below
        boolean reloc = false;

        // is event type local,region or qb
        locreg = ( sol.isOriginGType(GTypeMap.LOCAL) || sol.isOriginGType(GTypeMap.REGIONAL) );
        if ( locreg ) { // force depth to min depth ?
          if ( newMdepth < minLocZ ) {
            sol.mdepth.setValue(minLocZ); // depth in hypoinverse trial terminator record -aww 2015/06/11
            // NOTE: static Solution.calcOriginDatum should be enabled only for CRH CRT models (non-geoidal type), default false returns 0.
            double datum = sol.calcOriginDatumKm();
            // subtract datum correction to the minLocZ mdepth here -aww 2015/06/12
            sol.depth.setValue((Double.isNaN(datum)) ? minLocZ : minLocZ - datum); // -aww 2015/06/12 
            sol.depthFixed.setValue(true);
            reloc = true;
            if (verbose) 
                System.out.printf("INFO HypoMag solution: %d depth: %.2f reset to configured minLocationDepth - datum: %.2f-%.2f\n",
                       sol.getId().longValue(), priorLatLonZ.getZ(), minLocZ, datum);
          }
        }
        else if ( sol.isEventDbType(EventTypeMap3.QUARRY) && ! Double.isNaN(fixQuarryDepth) &&
                (sol.depthFixed.booleanValue() != true || sol.depth.doubleValue() != fixQuarryDepth) ) {
            sol.mdepth.setValue(fixQuarryDepth); // quarry depth is model depth - aww added 2015/06/11 
            // NOTE: static Solution.calcOriginDatum should be enabled only for CRH CRT models (non-geoidal type)
            double datum = sol.calcOriginDatumKm();
            // subtract datum correction to the fixQuarryDepth mdepth here -aww 2015/06/12
            sol.depth.setValue((Double.isNaN(datum)) ? fixQuarryDepth : fixQuarryDepth - datum); // -aww 2015/06/12 
            sol.depthFixed.setValue(true);
            reloc = true;
            if (verbose) 
                System.out.printf("INFO HypoMag solution: %d type: %s depth: %.2f reset to configured fixQuarryDepth - datum: %.2f-%.2f\n",
                    sol.getId().longValue(), EventTypeMap3.toDbType(sol.getEventTypeString()), priorLatLonZ.getZ(), fixQuarryDepth, datum);
        }

        // if depth changed to min depth fixed, relocate 
        if ( reloc ) { 
            tf = ((HypoMagEngineDelegateIF) delegate).locate(sol);
        }

        if ( tf ) {
            // is new solution location change less than delta KM from original input?
            double dist = sol.getLatLonZ().distanceFrom(priorLatLonZ);
            // property setting? what about 0.10 Km, as error resolution threshold ?
            if (dist >= originDeltaToCommit ) {
              resultCode = ProcessingResult.UNIT_SUCCESS.getIdCode();
            }
            else {
              if (verbose) 
                   System.out.println("INFO HypoMag solution: " +sol.getId().longValue()+
                         " relocation DISQUALIFIED for commit, LatLonZ changed < " +
                         originDeltaToCommit +  " km");
              resultCode = ProcessingResult.DISQUALIFIED.getIdCode();
            }
        }
        else resultCode = ProcessingResult.LOC_CALC_FAILURE.getIdCode();

        return resultCode;
    }

    public void dumpProperties() {
        if (props != null) {
            System.out.println("INFO HypoMag dumpProperties() " + getClass().getName() + " properties :");
            props.dumpProperties();
        }
        else System.out.println("INFO HypoMag dumpProperties() " + getClass().getName() + " properties : NULL");

        dumpDelegateProperties();
    }

    public void dumpDelegateProperties() {
        if (delegate != null) {
            HypoMagEngineDelegateIF hmd = (HypoMagEngineDelegateIF) delegate;
            System.out.println("INFO HypoMag dumpProperties() processor delegate properties :");
            delegate.getDelegateProperties().dumpProperties();
            LocationEngineIF locEng = hmd.getLocationEngine();
            if (locEng != null) {
                System.out.println("INFO HypoMag dumpProperties() delegate location engine properties :");
                locEng.getProperties().dumpProperties();
            }
            else System.out.println("INFO HypoMag dumpProperties() " + hmd.getClass().getName() + " LocationEngine : NULL");

            String magType = props.getProperty("pcsProcessorMagTypes");
            MagnitudeEngineIF magEng = hmd.initMagEngineForType(magType);
            if (magEng != null) {
                System.out.println("INFO HypoMag dumpProperties() delegate  magnitude engine properties :");
                magEng.getProperties().dumpProperties();
                MagnitudeMethodIF mm = magEng.getMagMethod();
                if (mm != null) {
                    System.out.println("INFO HypoMag dumpProperties() delegate magnitude method properties for : " + magType);
                    mm.getProperties().dumpProperties();
                }
                else System.out.println("INFO HypoMag dumpProperties() " + hmd.getClass().getName() + " MagnitudeMethod for " + magType + " : NULL");
            }
            else System.out.println("INFO HypoMag dumpProperties() " + hmd.getClass().getName() + " MagnitudeEngine : NULL");

        }
        else System.out.println("INFO HypoMag dumpProperties() " + getClass().getName() + " dumpProperties() processor delegate : NULL");
    }

    public boolean setupPicker() {
        return setupPicker(props);
    }

    public boolean setupPicker(GenericPropertyList props) {
        String pickerName = props.getProperty("picker.selected.name");
        if (pickerName == null) {
            System.out.println("INFO: HypoMag no picker class defined");
            return false;
        }
        String pickerClassName = props.getProperty("picker.class."+pickerName);
        String pickerPropsFilename = props.getUserFileNameFromProperty("picker.props."+pickerName);
        System.out.println("INFO HypoMag setupPicker " + pickerClassName + " loading properties from " + pickerPropsFilename);
        GenericPropertyList gpl = (pickerPropsFilename == null) ? props : new GenericPropertyList(pickerPropsFilename, (String)null);
        gpl.setFiletype("hypomag");
        return setupPicker(pickerClassName, gpl, true); 
    }

    public boolean setupPicker(String pickerClassName, GenericPropertyList props, boolean clearParameters) {

        if (pickerClassName == null) {
            System.err.println("Error HypoMag: setupPicker picker classname is not defined, check properties"); 
            return false;
        }

        // NOTE: make sure to set staParmsMap null after changing picker's sta properties file/data.
        if (clearParameters && picker != null) picker.clearChannelParms();

        boolean status = true;
        try {
          picker = (PhasePickerIF) Class.forName(pickerClassName).newInstance(); 
          picker.setProperties(props);
          //if (picker != null) picker.loadPickerChannelParms(); // autoLoadChannel parms is a picker property setting, default is false auto load

        }
        catch (Exception ex) {
            status = false;
            System.err.println("Error HypoMag: setupPicker failed to create instance of pickerClassName: " + pickerClassName); 
            ex.printStackTrace();
        }
        return status;
    }

    private int doPicker(Solution sol) {

        long evid = sol.getId().longValue(); 
        //System.out.println("INFO HypoMag: doPicker for " +evid+ " start @ "+ new DateTime().toString()); // UTC time
        BenchMark bm = new BenchMark(System.err);
        bm.printTimeStamp("INFO HypoMag: doPicker for " + evid+ " start");

        int resultCode = ProcessingResult.PICKER_FAILURE.getIdCode();

        SolutionWfEditorPropertyList wfProps = (SolutionWfEditorPropertyList) props; 
        // Get waveforms input mode 
        int wfMode = wfProps.getInt("waveformReadMode"); // =0 database, =1 waveserver

        if (wfMode == AbstractWaveform.LoadFromDataSource) { // from database
          System.err.println("HypoMag: Using DataSourceChannelTimeModel");
          AbstractWaveform.setWaveDataSource(wfProps.getWaveSource());
          ctwModel = new DataSourceChannelTimeModel(); // define the CTW model

        } else if (wfMode == AbstractWaveform.LoadFromWaveServer) { // from WaveServer
    
          System.err.println("HypoMag: Using PowerLawTimeWidowModel");
          ctwModel = new PowerLawTimeWindowModel(); // for waveserver

          // Try making a WaveClient
          try {
            org.trinet.jiggle.WaveServerGroup waveClient = wfProps.getWaveServerGroup();
    
            if (waveClient == null || waveClient.numberOfServers() <= 0) {
              System.err.println(
                  "Error HypoMag: no or bad waveservers defined in properties file. " );
              System.err.println("waveServerGroupList = " + wfProps.getProperty("waveServerGroupList"));

              return resultCode;
            }
    
            AbstractWaveform.setWaveDataSource(waveClient);  //'waveformReadMode' doesn't matter
    
          } catch (Exception ex) {
            System.err.println("Error HypoMag: creation of new wave client failed");
            System.err.println(ex.toString());
            ex.printStackTrace();
            return resultCode;
          }
        } else {
          System.err.println("Error HypoMag: you must define a legal value of 'waveformReadMode'");
          return resultCode;
        }
        if (ctwModel == null) {
          System.err.println("Error HypoMag: channel time window model required for picker is null");
          return resultCode;
        }
        // set time window model properties and candidate list to existing channel list
        ctwModel.setProperties(wfProps);
        //if (hasChannelList()) ctwModel.setCandidateList(MasterChannelList.get()); 
        if (hasChannelList()) ctwModel.setCandidateList(((AbstractChannelListProcessingController)getProcessingController()).getChannelList());
    
        addCnt = 0;
        pickCnt = 0;

        if (picker == null) {
            if (!setupPicker()) {
                System.err.println("Error HypoMag: failed to create new picker instance");
                return resultCode;
            }
        }

        //PhasePickerIF.P_ONLY PhasePickerIF.S_ONLY PhasePickerIF.P_AND_S PhasePickerIF.DEFAULT
        int pickFlag = wfProps.getInt("picker.flag", PhasePickerIF.DEFAULT);

        if (sol == null) {
            System.err.println("No selected input solution for picker " + picker.getName());
            return resultCode;
        }

        if (! sol.hasLatLonZ()) {
            System.out.println("Error HypoMag: input event has no location, skipping processing");
            return resultCode;
        }

        PhaseList solPhList = sol.getPhaseList();

        // First delete existing autopicks, since picker's parms may not re-pick same phases
        System.out.printf("INFO: HypoMag removed %d existing auto phases from solution phase list of size: %d%n",
                solPhList.eraseAllAutomatic(), solPhList.size());
        
        // Note: Not using solution's phaselist in construct() because add/removes to it cause events in SelectableReadingList
        // which invoke calcDistance sorts in EventQueue, but array changes by construct thread cause indexing exceptions sorting 
        // Instead in finished method update solution's phaselist in SelectableReadingList with new newSolPhList contents
        PhaseList newSolPhList = null; // Put picked phases into this list in worker thread

        try {
            // Lazy 1st time configuration of picker's station channel data initialize once unless changed
            boolean reloadParms = reloadParmsPerEvent;
            //if ( ! reloadParms ) {
            //  long parmsReadTime = System.currentTimeMillis();
            //  if ( (double) (parmsReadTime - lastParmsReadTime) >= reloadParmsSecs*1000. ) {
            //     reloadParms = true;
            //     lastParmsReadTime = parmsReadTime;
            //  }
            //}
            if (! picker.hasChannelParms() || reloadParms ) {
                if (! picker.loadChannelParms() ) {
                    System.err.println("Error HypoMag: picker error loading station parameters for picker " + picker.getName());
                } 
            }

            if (sol.hasLatLonZ()) {
                newSolPhList = pickEQ(sol, solPhList, pickFlag);
            }

            if ( newSolPhList == null || newSolPhList.size() == 0 ) {
                System.err.println("ERROR: Auto picking of waveforms failed with picker: " + picker.getName()); 
            }
            else {
                int oldSize = solPhList.size();
                solPhList.clear(false);
                solPhList.fastAddAll(newSolPhList);
                int newSize = solPhList.size();
                if (wfProps.getBoolean("dumpPhaseList")) {
                    System.out.println("---------------------------------------------------------------------------------------------");
                    System.out.println(solPhList.getNeatHeader());
                    solPhList.dumpToString();
                    System.out.println("---------------------------------------------------------------------------------------------");
                }

                int minPicks = wfProps.getInt("picker.minPicks", 5);
                System.out.printf("INFO: HypoMag %d picker total counts (phases at start: %d, at end: %d) (autopicked: %d, added/replaced: %d)%n", 
                            evid, oldSize, newSize, pickCnt, addCnt);
                if (newSize < minPicks ) { // 
                    System.out.printf("INFO: HypoMag %d disqualified new picks %d < %d minPicks required%n", evid, newSize, minPicks); 
                    resultCode = ProcessingResult.DISQUALIFIED.getIdCode();
                }
                else {
                    resultCode = ProcessingResult.UNIT_SUCCESS.getIdCode();
                }
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }

        //System.out.println("INFO HypoMag: doPicker for " +evid+ " end @ "+ new DateTime().toString()); // UTC time
        bm.printTimeStamp("INFO HypoMag: doPicker for " +evid+ " end");

        return resultCode;
    }

    private PhaseList pickEQ(Solution sol, PhaseList solPhList, int phasesToPickFlag) {
        return (picker instanceof TriaxialPickerIF) ? pickEQ_3(sol, solPhList, phasesToPickFlag) : pickEQ_1(sol, solPhList, phasesToPickFlag);
    }

    private PhaseList pickEQ_1(Solution sol, PhaseList solPhList, int phasesToPickFlag) {

        Waveform wf = null;
        PhaseList phList = null;  
        PhaseList newSolPhList = new PhaseList(solPhList); // should be only human, final picks here
        // Need wflist from channel time window model ?
        ctwModel.setSolution(sol); // do this after setting list, else it loads one from db, does distance sorts, too.
        ChannelableList wfList = (ChannelableList) ctwModel.getWaveformList();
        for (int idx = 0; idx < wfList.size(); idx++) {

            wf = (Waveform) wfList.get(idx);

            Channel ch = wf.getChannelObj();

            // assumes wf list is distance sorted
            if ( ! picker.withinPickerRange(ch, sol, "P") ) {  // out of P range
                System.out.println("Autopicking stopped");
                break;
            }

            int phFlag = phasesToPickFlag;
            if (phFlag == PhasePickerIF.DEFAULT) {
                phFlag = getDefaultPickFlag(picker, ch);
            }
            if (phFlag == PhasePickerIF.NONE) {
                if (picker.isDebug()) 
                    System.out.println("Autopicking NONE for channel: " + ch.toDelimitedSeedNameString("."));
                continue;
            }


            if (phFlag == PhasePickerIF.P_ONLY && !picker.pOnH() && ch.isHorizontal()) {
                if (picker.isDebug()) System.out.printf("Autopicking skipped, %s no P-picking on horz%n", ch.toDelimitedSeedNameString("."));
                continue; // skip
            }
            else if (phFlag == PhasePickerIF.S_ONLY && !picker.sOnV() && ch.isVertical()) {
                if (picker.isDebug()) System.out.printf("Autopicking skipped, %s no S-picking on vert%n", ch.toDelimitedSeedNameString("."));
                continue; // skip
            }
            // For group picking only phases allowed by property flags
            if (phFlag == PhasePickerIF.P_AND_S && !picker.pOnH() && ch.isHorizontal()) phFlag = PhasePickerIF.S_ONLY;
            else if (phFlag == PhasePickerIF.P_AND_S && !picker.sOnV() && ch.isVertical()) phFlag = PhasePickerIF.P_ONLY;

            if (phFlag == PhasePickerIF.S_ONLY) {
                if ( ! picker.withinPickerRange(ch, sol, "S") ) { 
                    continue; // assumes panel list is distance sorted
                }
            }

            phList = picker.pick(wf, sol, phFlag, true, false); // do velocity filter, but not distance filter (which is done above)

            phList = stripByWeight(phList);

            int cnt = phList.size();
            if (cnt > 0) {
                pickCnt += cnt;
                addCnt += addOrReplaceAutoPicks(sol, phList, newSolPhList);
            }
            else if (picker.isDebug()) System.out.printf("INFO: HypoMag picker found NO picks for %s%n", ch.toDelimitedSeedNameString("."));

        } //end of for loop over waveforms in collection

        return newSolPhList;
    }

    // TriaxialPicker code uses 3-component waveforms 
    private PhaseList pickEQ_3(Solution sol, PhaseList solPhList, int phasesToPickFlag) {

        PhaseList newSolPhList = new PhaseList(solPhList); // should be only human, final picks here

        // Need wflist from channel time window model
        ctwModel.setSolution(sol); // do this after setting list, else it loads one from db, does distance sorts, too.
        ChannelableList wfList = (ChannelableList) ctwModel.getWaveformList();

        PhaseList phList = picker.pick(wfList, sol, phasesToPickFlag, true, true);
        //System.out.println("DEBUG HypoMagSolutionProcessor pickEQ_3 returned phList:\n" + phList.toNeatString(true));

        phList = stripByWeight(phList);

        int cnt = phList.size();
        if (cnt > 0) {
            pickCnt += cnt;
            addCnt += addOrReplaceAutoPicks(sol, phList, newSolPhList);
        }
        else if (picker.isDebug()) System.out.printf("INFO: HypoMag picker found NO picks%n");

        return newSolPhList;
    }

    private int getDefaultPickFlag(PhasePickerIF picker, Channel ch) {
        int flag = PhasePickerIF.NONE;
        PickerParmIF parm = picker.getChannelParm(ch.toDelimitedSeedNameString("."));
        if (parm != null) return parm.getPickFlag();
        else if (picker.isDebug()) {
            System.out.println("Autopicking found no default parameters specified in parms file for " + 
                ch.toDelimitedSeedNameString("."));
        }
        return flag;
    }

    private PhaseList stripByWeight(PhaseList inList) {

        if ( maxPwt >= 4 && maxSwt >= 4) return inList; // no-op

        Phase ph = null;
        PhaseList newList = new PhaseList(2);
        for ( int idx=0; idx < inList.size(); idx++) {
            ph = (Phase) inList.get(idx);
            if ( ph.isP()  && maxPwt < 4) {
                if ( ph.description.getWeight() <= maxPwt ) newList.add(ph); 
                else if (debug) System.out.printf("%s P-pick wgt: %d > %d maxPwt, rejected\n",
                        ph.getChannelObj().toDelimitedSeedNameString(), ph.description.getWeight(), maxPwt);
            }
            else if ( ph.isS() && maxSwt < 4 ) {
                if ( ph.description.getWeight() <= maxSwt ) newList.add(ph); 
                else if (debug) System.out.printf("%s S-pick wgt: %d > %d maxSwt, rejected\n",
                        ph.getChannelObj().toDelimitedSeedNameString(), ph.description.getWeight(), maxSwt);
            }
        }
        return newList;
    }

    private int addOrReplaceAutoPicks(Solution sol, PhaseList pickList, PhaseList newSolPhList) {
        int addCnt = 0;
        // Now for each picker phase, check for allowable replacement
        for (int ii = 0; ii < pickList.size(); ii++) {

             boolean doitFlag = true;
             Phase newPhase = (Phase) pickList.get(ii);
             newPhase.assign(sol);

             Phase oldPhase = null;

             PhaseList staPhList = (PhaseList) newSolPhList.getAllOfSameStaType(newPhase);  // P or S
             String reason = "";
             String addrep = "ADD";
             if (staPhList.size() > 0) { // if others from same station, check before add or replace
                 String type = ((String) newPhase.getTypeQualifier()).trim();
                 // Loop over matching station types
                 Channel oldChan = null;
                 Channel newChan = null;
                 addrep = "REPLACE";
                 for (int jj=0; jj<staPhList.size(); jj++) {

                     oldPhase = staPhList.getPhase(jj);
                     if (oldPhase.isDeleted()) continue; // skip over existing deleted, assume they are replaceable

                     // Check for AUTO versus NON-auto pick on same station, don't replace non-auto picks
                     if ( newPhase.isAuto() && !oldPhase.isAuto() ) {
                       reason = "Auto";
                       doitFlag = false;
                       break;
                     }


                     newChan = newPhase.getChannelObj();
                     oldChan = oldPhase.getChannelObj();
                     // Don't replace VEL with ACC picks
                     if ( newChan.isAcceleration() && oldChan.isVelocity() ) {
                       reason = "Vel";
                       doitFlag = false; 
                       break;
                     }
                     else if (newChan.isVertical()) {
                         if (type.equals("S")) { 
                            if (oldChan.isHorizontal()) { // don't replace horz S with vert S
                                reason = "SonV";
                                doitFlag = false;
                                break;
                            }
                         }
                     }
                     else if (newChan.isHorizontal()) {
                         if (type.equals("P")) {
                             if (oldChan.isVertical()) { // don't replace vert P with horz P
                                reason = "PonH";
                                 doitFlag = false;
                                 break;
                             }
                         }
                         else if (oldChan.isHorizontal()) { // don't replace existing Horizontal of higher weight
                             if (oldPhase.getQuality() > newPhase.getQuality()) {
                                 reason = "Qual";
                                 doitFlag = false;
                                 break;
                             }
                         }
                     }

                     // Check seedchan codes
                       String oldseed = oldPhase.getChannelObj().getSeedchan(); 
                       String newseed = newPhase.getChannelObj().getSeedchan(); 

                       if ( newChan.isBorehole() && !oldChan.isBorehole() ) {
                          reason = "downH";
                          doitFlag = false;
                          break;
                       }

                       // Check freq band
                       char rateOld = oldseed.charAt(0);
                       char rateNew = newseed.charAt(0);
                       // only replace HH with HH 
                       if (rateOld == 'H' && rateNew != 'H') {
                          reason = "HH~";
                          doitFlag = false;
                          break;
                       }
                       // only replace EH with HH, EH
                       else if (rateOld == 'E' && !(rateNew == 'H' || rateNew == 'E')) {
                          reason = "EH~";
                          doitFlag = false;
                          break;
                       }
                       // Only replace SH with HH, EH, SH
                       else if (rateOld == 'S' && !(rateNew == 'H' || rateNew =='E' || rateNew == 'S')) {
                          reason = "SH~";
                          doitFlag = false;
                          break;
                       }
                       // If same freq band, next check high vs low gain type
                       else if (rateOld == rateNew) {
                           char typeOld = oldseed.charAt(1);
                           char typeNew = newseed.charAt(1);
                           // Only replace HH,EH,SH with like HH, EH, SH, skip low gain G L N  type
                           if (typeOld == 'H' && typeNew != 'H') {
                              reason = "H2L";
                              doitFlag = false;
                              break;
                           }
                       }

                     // Log quality check, else we won't know if the picker parms changes made poorer pick than old one
                     if ( newPhase.getQuality() < oldPhase.getQuality() ) {
                           System.out.printf("INFO: Hypomag picker quality %s new %3.2f < old %3.2f %s%n",
                               newChan.toDelimitedSeedNameString("."), newPhase.getQuality(),
                               oldPhase.getQuality(), oldChan.toDelimitedSeedNameString(".") // bugfix replace newChan with oldChan ref -aww 2011/10/06
                           );
                       //doitFlag = false; //don't replace lower quality ?
                       //break; 
                     }

                 } // end of loop over matching existing sta picks

             } // end of matching existing sta pick checking block

             if (doitFlag) {
                 if (picker.isDebug()) {
                     System.out.printf("INFO: Hypomag picker new phase %s %s%n", addrep,
                         newPhase.getChannelObj().toDelimitedSeedNameString("." ));
                     System.out.printf(" new: %s%n", newPhase.toNeatString());
                 }
                 newSolPhList.addOrReplaceAllOfSameStaType(newPhase, false); // onlyAuto=false, ok since we checked auto flag above
                 addCnt++;
             }
             else {
                 System.out.printf("INFO: Hypomag picker new phase SKIPPED (%s) %s%n", reason,
                         newPhase.getChannelObj().toDelimitedSeedNameString("." ));
                 if (picker.isDebug()) {
                     System.out.printf(" new: %s%n old: %s%n", newPhase.toNeatString(), oldPhase.toNeatString());
                 }
             }
        } // end of loop over all autopicks made
        return addCnt;
    } // end of addOrReplace method

} // end of HypoMagSolutionProcessor class
