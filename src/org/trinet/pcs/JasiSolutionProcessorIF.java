package org.trinet.pcs;
import org.trinet.jasi.*;

public interface JasiSolutionProcessorIF {

    void setAppName(String appName);
    String getAppName();

    int processSolution(long evid);
    int processSolution(Solution sol);

    SolutionProcessingMode getProcessingMode();
    int getProcessingModeId();

    boolean setProcessingMode(int modeId);
    boolean setProcessingMode(String modeName);
    boolean setProcessingMode(SolutionProcessingMode spm);

    // lookup match to known map mode from input property values
    boolean hasProcessingModeFor(int modeId);
    boolean hasProcessingModeFor(String name);
    boolean hasProcessingModeFor(SolutionProcessingMode spm);

    SolutionProcessingMode getProcessingModeFor(String name);
    SolutionProcessingMode getProcessingModeFor(int modeId);

    void setProcessingController( JasiSolutionProcessingControllerIF controller);
    JasiSolutionProcessingControllerIF getProcessingController();

    boolean setProperties(SolutionEditorPropertyList props);
    SolutionEditorPropertyList getProperties();
    void dumpProperties();
}
