package org.trinet.pcs;


/**
 * Transition ALL Process Table rows with state/results defined in the
 * Transition table
 */
class Transition
{

  public static void main (String args[])
    {
      ProcessControl.doTransitions();
    }

}
