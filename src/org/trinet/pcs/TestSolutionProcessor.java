package org.trinet.pcs;
import java.util.*;
import org.trinet.jasi.*;
import org.trinet.jasi.engines.*;

public class TestSolutionProcessor extends HypoMagSolutionProcessor {

    public boolean doModeTest = true;

    /** No-op Constructor. */
    public TestSolutionProcessor() { }

    /**
     * Constructor initializes HypoMagEngineDelegate with property list and
     * sets UNKNOWN as the mode (default) for processing.
     */
    public TestSolutionProcessor(SolutionWfEditorPropertyList props) {
        super(props);
    }

    /**
     * Constructor initializes HypoMagEngineDelegate with input property list and
     * sets mode (default) for processing to the specified input mode.
     */
    public TestSolutionProcessor(SolutionWfEditorPropertyList props, SolutionProcessingMode defaultMode) {
        super(props, defaultMode);
    }

    //IF
    /** Process input Solution with the HypoMagEngineDelegate as configured
     *  if <i>doModeTest=true</i>, otherwise does no-op and returns success code.*/
    public int processSolution(Solution sol)  {
        //return ProcessingResult.FAILURE.getIdCode();
        return (doModeTest) ?
            super.processSolution(sol) : ProcessingResult.UNIT_SUCCESS.getIdCode();
    }
}
