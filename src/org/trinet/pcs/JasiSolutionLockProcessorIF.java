package org.trinet.pcs;

public interface JasiSolutionLockProcessorIF {
    void setEnableLocking(boolean tf);
    boolean isLockingEnabled();
    void releaseAllSolutionLocks();
    boolean handleLock(long id);
}
