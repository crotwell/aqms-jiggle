package org.trinet.storedprocs.eventlock;

import org.trinet.jasi.DataSource;
import org.trinet.jasi.TestDataSource;
import org.trinet.jasi.SolutionLock;
import java.util.ArrayList;

/**
 * List all currently locked events on the default data source.
 *
 * Output has the following form:<p>
<tt>

----- Current locks = 1 ------
Event-ID    Host                          Username  Application    Date/Time
1234001     serverbo.gps.caltech.edu         doug      LockEvent      2000-08-16 17:56:09
1234003     serverbt.gps.caltech.edu          stan      Jiggle         2000-08-16 21:47:53
</tt>
*/

public class ListLockedEvents  {

    static String host = "";
    static String dbasename= "";

    public ListLockedEvents() { }

    public static void main (String args[]) {

        if (args.length > 0) {
            host = args[0];
            dbasename = args[1];
        }

        //System.err.println ("Making DataSource connection...");
        DataSource ds =  TestDataSource.create(host, dbasename); // new DataSource(url, driver, user, passwd);

        //DataSource ds =  new DataSource();        // make connection

        //System.out.println (ds.toString());

        SolutionLock solLock = SolutionLock.create();

        if (!solLock.checkLockingWorks()) {
          System.out.println ("Locking not enabled for this data source.");
          System.exit(-1);
        }

        ArrayList list = (ArrayList) solLock.getAllLocks();

        if (list == null) {
          System.out.println("No locks.");
        } else {
          System.out.println("----- Current locks = "+ list.size() +" ------");
          System.out.println (SolutionLock.getHeaderString());
          for (int i = 0; i< list.size(); i++) {
            System.out.println (((SolutionLock)list.get(i)).toFormattedString());
          }

        }

    }

} // ListLockedEvents
