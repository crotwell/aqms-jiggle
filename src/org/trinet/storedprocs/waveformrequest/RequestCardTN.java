package org.trinet.storedprocs.waveformrequest;

import java.util.*;
import java.sql.*;
import org.trinet.jasi.*;
import org.trinet.jdbc.*;
import org.trinet.jdbc.table.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.util.Format;


/**
 *  <pre>
 SQL> describe request_card
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
 EVID                                               NUMBER(15)
 AUTH                                      NOT NULL VARCHAR2(15)
 SUBSOURCE                                 NOT NULL VARCHAR2(8)
 NET                                       NOT NULL VARCHAR2(8)
 STA                                       NOT NULL VARCHAR2(8)
 SEEDCHAN                                  NOT NULL VARCHAR2(8)
 STAAUTH                                   NOT NULL VARCHAR2(15)
 CHANNEL                                   NOT NULL VARCHAR2(8)
 DATETIME_ON                               NOT NULL NUMBER(25,10)
 DATETIME_OFF                              NOT NULL NUMBER(25,10)
 REQUEST_TYPE                              NOT NULL VARCHAR2(1)
 LDDATE                                             DATE
 RCID                                               NUMBER(15)
 LOCATION                                  NOT NULL VARCHAR2(2)
 RETRY                                              NUMBER(38)
 LASTRETRY                                          DATE
 PRIORITY                                           NUMBER

 CONSTRAINT_NAME                SEARCH_CONDITION
------------------------------ --------------------------------------------------------------------------------
SYS_C008252                    "AUTH" IS NOT NULL
SYS_C008253                    "SUBSOURCE" IS NOT NULL
SYS_C008254                    "NET" IS NOT NULL
SYS_C008255                    "STA" IS NOT NULL
SYS_C008256                    "SEEDCHAN" IS NOT NULL
SYS_C008257                    "STAAUTH" IS NOT NULL
SYS_C008258                    "CHANNEL" IS NOT NULL
SYS_C008259                    "DATETIME_ON" IS NOT NULL
SYS_C008260                    "DATETIME_OFF" IS NOT NULL
SYS_C008261                    "REQUEST_TYPE" IS NOT NULL
SYS_C008262                    "LOCATION" IS NOT NULL
REQ01                          request_type in('T','C')
REQ02                          retry >=0
REQUEST_CARDKEY01
 </pre>
 *
 * @see: RequestCard
 * @see: TabelRowREquestCard
 */

// THIS SHOULD EXTEND A GENERIC RequestCard class
public class RequestCardTN {

  static Connection conn;
  static boolean addChan2src = false;

// Attributes
  DataString auth      = new DataString();   // usually the local network code
  DataString subsource = new DataString();   // 
  DataString staAuth   = new DataString();   // don't know what this is, always seems equal to subsource
  DataLong evid        = new DataLong(0);    // none if "continuous"
  DataInteger retry    = new DataInteger(0);
  DataInteger priority = new DataInteger(1);
  DataLong wfid        = new DataLong();
  DataDouble pcstored  = new DataDouble();

  // These are "read-only"
  /** Date card is inserted. Done automatically by Oracle. */
  DataDate lddate    = new DataDate();     // SYS_EXTRACT_UTC(CURRENT_TIMESTAMP)
  /** Unique number from a sequencer that is NOT part of the key. It is used
   to speed up bulk table operations because the KEY  involves several columns.*/
  DataLong rcid      = new DataLong();
  DataDate lastRetry = new DataDate();     // null

  ChannelTimeWindow ctw = new ChannelTimeWindow(Channel.create());

  String DEFAULT_TYPE = "T";
  DataString type = new DataString(DEFAULT_TYPE);

  public RequestCardTN() {
  }

  public RequestCardTN(ChannelTimeWindow ctw) {
    setup(ctw, DEFAULT_TYPE);
  }

  public RequestCardTN(Connection conn) {
    setConnection (conn);
  }

  public void setup (ChannelTimeWindow ctw, String type) {
    this.ctw = ctw;
    this.type.setValue(type);
  }

  /** Given a collection of ChannelTimeWindow's, return a matching list of RequestCardTN's.
   The values of type, retry, and priority are all set according to the prototype-RequestCardTN passed.*/
  public static ActiveArrayList makeList (ChannelableList channelTimeWindowList,
                                     RequestCardTN prototype) {
      return makeList(channelTimeWindowList,
                      prototype.auth.toString(),
                      prototype.subsource.toString(),
                      prototype.evid.longValue(),
                      prototype.staAuth.toString(),
                      prototype.type.toString(),
                      prototype.retry.intValue(),
                      prototype.priority.intValue()
                      );
    }
/** Given a collection of ChannelTimeWindow's, return a matching list of RequestCardTN's.
 The values of evid, type, retry, and priority are all set according to the prototype-RequestCardTN passed.*/
  public static ActiveArrayList makeList (ChannelableList channelTimeWindowList,
                                     String auth,  String subsource, long evid,
                                     String staAuth, String type, int retry, int priority ) {
    int sz = channelTimeWindowList.size();
    ChannelTimeWindow ctwList[] =
        (ChannelTimeWindow[]) channelTimeWindowList.toArray(new ChannelTimeWindow[sz]);
    ActiveArrayList rcList = new ActiveArrayList(sz);
    RequestCardTN rc;

    for (int i = 0; i<sz; i++) {
      rc = new RequestCardTN(ctwList[i]);
      rc.auth.setValue(auth);
      if ( addChan2src ) rc.subsource.setValue(subsource + "-" + rc.ctw.getChannelObj().getSeedchan().substring(0,2));
      else rc.subsource.setValue(subsource);
      rc.evid.setValue(evid);
      rc.staAuth.setValue(staAuth);
      rc.type.setValue(type);
      rc.retry.setValue(retry);
      rc.priority.setValue(priority);

      rcList.add(rc);
    }
    return rcList;
  }
  /**
   * Parse a resultset row that contains the results of a join + some querry
   */

    private static RequestCardTN parseOneRow (ResultSet rs){

      return parseOneRow(new ResultSetDb(rs));

    }
    /** Returns true if the attributes required for insertion in the dbase are set.*/
    public boolean isValid() {
      return !( !channelIsValid(ctw.getChannelObj()) ||
                ctw.getTimeSpan().isNull() ||
                auth.isNull() ||
                subsource.isNull() ||
                type.isNull()
                );
    }
/** This class has more stringent requirements for channels than most tables. */
    private static boolean channelIsValid(Channel ch) {
      return !(ch.getNet() == null  ||
               ch.getSta() == null ||
               ch.getSeedchan() == null ||
               ch.getChannel() == null
               ) ;
    }

    /** Parse one RequestCard row from the given ResultSetDb. */
    private static RequestCardTN parseOneRow (ResultSetDb rsdb) {

      if (rsdb == null) return null;

      RequestCard rc = new RequestCard();
      rc.parseOneRow(rsdb, 0);
      return parseOneRow(rc);
    }
    /** Parse the given RequestCard. */
    private static RequestCardTN parseOneRow (RequestCard rc) {

      RequestCardTN ths = new RequestCardTN();

// Suck all channel attributes from the row
      ths.ctw.setChannelObj(parseNameFromDataTableRow(rc));

      // RequestCard rc = RequestCard.create();
      ths.evid = (DataLong) rc.getDataObject(TableRowRequestCard.EVID);
      ths.auth = (DataString) rc.getDataObject(TableRowRequestCard.AUTH);
      ths.subsource = (DataString) rc.getDataObject(TableRowRequestCard.SUBSOURCE);

      ths.staAuth = (DataString) rc.getDataObject(TableRowRequestCard.STAAUTH);

      DataDouble dt_on = (DataDouble) rc.getDataObject(TableRowRequestCard.DATETIME_ON);
      DataDouble dt_off = (DataDouble) rc.getDataObject(TableRowRequestCard.DATETIME_OFF);
      ths.ctw.setLimits(dt_on.doubleValue(), dt_off.doubleValue());

      ths.type = (DataString) rc.getDataObject(TableRowRequestCard.REQUEST_TYPE);
      ths.lddate =(DataDate) rc.getDataObject(TableRowRequestCard.LDDATE);
      ths.rcid = (DataLong) rc.getDataObject(TableRowRequestCard.RCID);
      ths.retry = (DataInteger) rc.getDataObject(TableRowRequestCard.RETRY);
      ths.lastRetry = (DataDate) rc.getDataObject(TableRowRequestCard.LASTRETRY);
      ths.priority = (DataInteger) rc.getDataObject(TableRowRequestCard.PRIORITY);
      ths.wfid = (DataLong) rc.getDataObject(TableRowRequestCard.WFID);
      ths.pcstored = (DataDouble) rc.getDataObject(TableRowRequestCard.PCSTORED);

      return ths;
    }
    /** Create the given RequestCard. Returns null if a valid RequestCard
     cannot be made. */
    private RequestCard toRow () {

      if (!isValid()) return null;

      RequestCard rc = new RequestCard();
      RequestCardTN ths = new RequestCardTN();

      // get and set sequence #
      // This is probably sloooow. Should be set by dbase like SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) is.
      long newId = SeqIds.getNextSeq(getConnection(), RequestCard.SEQUENCE_NAME);
      rc.setValue(TableRowRequestCard.RCID, newId);

      // These can't be null if isValid()
      setRowChannelId(rc, ctw.getChannelObj());

      rc.setValue( TableRowRequestCard.DATETIME_ON, ctw.getStart());
      rc.setValue( TableRowRequestCard.DATETIME_OFF, ctw.getEnd());
      rc.setValue( TableRowRequestCard.REQUEST_TYPE, type);
      // subsource & staauth seem to be the same for "T"'s but diff for "C" in the dbase
      rc.setValue( TableRowRequestCard.AUTH, auth);
      rc.setValue( TableRowRequestCard.SUBSOURCE, subsource);
      rc.setValue( TableRowRequestCard.STAAUTH, staAuth);

// optional attributes
      if (!evid.isNull())          rc.setValue( TableRowRequestCard.EVID, evid);
      //if (!lddate.isNull())        rc.setValue( TableRowRequestCard.LDDATE, lddate); // leave null, uses SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) <- 02/22/2007 removed -aww
      if (!rcid.isNull())          rc.setValue( TableRowRequestCard.RCID, rcid);
      if (!retry.isNull())         rc.setValue( TableRowRequestCard.RETRY, retry);
      if (!lastRetry.isNull())     rc.setValue( TableRowRequestCard.LASTRETRY, lastRetry);
      if (!priority.isNull())      rc.setValue( TableRowRequestCard.PRIORITY, priority);
      if (!wfid.isNull())          rc.setValue( TableRowRequestCard.WFID, wfid);
      if (!pcstored.isNull())      rc.setValue( TableRowRequestCard.PCSTORED, pcstored);

      return rc;
    }
    /** Put the channel name attributes in the DataTableRow */
    protected void setRowChannelId(DataTableRow aRow, Channel chan) {
        ChannelIdIF cn = chan.getChannelId();
        // by default all fields are "" rather then null strings: see ChannelName
        aRow.setValue(RequestCard.STA, cn.getSta()); // this attribute can't be null
        String str = cn.getNet();
        if (str.length() > 0) aRow.setValue(RequestCard.NET, str);
        str = cn.getSeedchan();
        if (str.length() > 0) aRow.setValue(RequestCard.SEEDCHAN, str);
        str = cn.getLocation();
        if (str.length() > 0) aRow.setValue(RequestCard.LOCATION, str);
        str = cn.getChannel();
        if (str.length() > 0) aRow.setValue(RequestCard.CHANNEL, str);
    }

    /** Insert in dbase if connection and RequestCard is valid.
     You must commit to the connection to do a final commit of the insert. */
    public boolean commit() {
      // other checking here
      return dbaseInsert();
    }
    /**
     * Insert a new row in the dbase.
     * You must commit to the connection to do a final commit of the insert.
     */
    protected boolean dbaseInsert () {

      if (getConnection() == null) return false;
      if (!isValid()) return false;

      boolean status = true;

// make a dbase RC object
      RequestCard rc = toRow();

      rc.setProcessing(DataTableRowStates.INSERT);

      // write arrival row
      status = (rc.insertRow(getConnection()) > 0);

      return status;
    }
    /**
     * Insert a row in the dbase or each RequestCardTN in this List.
     * You must commit to the connection to do a final commit of the insert.
     * Returns the number of RequestCards inserted in the dbase.
     */
    public static int dbaseInsertList (Collection requestCardTNlist) {

      Connection conn = getConnection();
      if (conn == null) return 0;
      if (requestCardTNlist == null) return 0;

      int knt = 0;
      RequestCardTN rc;
      RequestCard rcdb;
      Iterator iter = requestCardTNlist.iterator ();
      while (iter.hasNext ()) {
        rc = (RequestCardTN) iter.next ();
        if (rc.isValid()) {
          rcdb = rc.toRow();
          rcdb.setProcessing(DataTableRowStates.INSERT);
          if (rcdb.insertRow(conn) > 0) knt++; // will return -1 on error - no exception thrown
        }
      }
      return knt;
    }

    /**
     * Return a collection of RequestCardTN objects from the dbase using this
     *  SQL call.
     * @param conn
     * @param sql an SQL string
     * @return a collection of RequestCardTN objects
     */
    protected Collection getBySql(Connection conn, String sql)
    {

      //ArrayList chanList = new ArrayList();
      ArrayList list = new ArrayList();

      Statement sm = null;
      ResultSet rs = null;

      try {
        if ( conn.isClosed() ) // check that valid connection exists
        {
          System.err.println ("* Connection is closed");
          return null;
        }

        sm = conn.createStatement();

        rs = org.trinet.jdbc.table.ExecuteSQL.rowQuery(sm, sql);
        if (rs != null) {
          // parse and add rows one-at-a-time
          while ( rs.next() ) {
            list.add(parseOneRow(rs));
          }
        }
      }
      catch (SQLException ex) {
        System.err.println(ex);
        ex.printStackTrace();
      }
      finally {
        try {
            if (rs != null) rs.close();
            if (sm != null) sm.close();
        }
        catch (SQLException ex) {}
      }

      // convert vector to array
      // if (list.size() == 0) return null;

      return list;
    }
    /** 
     * Get an array of this object given an SQL query.
     */
    public RequestCardTN[] getByRowType(String rowType)
    {
      return getByRowType(getConnection(), rowType);
    }

    /** * Get an array of this object given an SQL query.
     */
    public RequestCardTN[] getByRowType(Connection conn, String rowType)
    {
      return getByWhereClause(conn, "WHERE Request_Type like " + StringSQL.valueOf(rowType) +
                              " Order by Priority, Retry");
    }
    /** * Get an array of this object given an SQL query.
     */
      private static RequestCardTN[] getByWhereClause(Connection conn, String whereClause)
        {
          //setConnection(conn);
 //         ArrayList list = new ArrayList();

          RequestCard rc = new RequestCard(conn);
          RequestCard rcList[] = (RequestCard[]) rc.getRowsEquals(whereClause);
          if (rcList == null || rcList.length == 0) return new RequestCardTN[0]; // nothing found

          RequestCardTN[] rctnList = new RequestCardTN[rcList.length];

          // parse and add rows one-at-a-time
          for (int i = 0; i< rcList.length;i++) {
            rctnList[i] = parseOneRow(rcList[i]);
          }

          return rctnList;
    }
/*
    public static RequestCardTN[] getRowsByType(String type) {
      if (NullValueDb.isEmpty(type)) return null;

      RequestCard rc = new RequestCard();
      Object list = rc.getRowsEquals("WHERE Request_Type like " + StringSQL.valueOf(type) +
                           " Order by Priority, Retry");
      return
    }

  */

    /** Return request_card row count for evid */
    public static int countRequests(long evid)
    {

        RequestCard rc = new RequestCard(getConnection());
        return rc.getRowCount("*","WHERE evid="+evid);
    }

/**
 * Fetch request card for this event and channel 
 * from the Request_Card table.
 */
    public static RequestCardTN[] fetchRequests (long evid, Channel ch)
    {
        String tableName = "Request_Card";
        
        ChannelIdIF chid = ch.getChannelId();
        
        StringBuffer sb = new StringBuffer("WHERE evid = "+evid+" and ");
        // Build the SQL for SNCL matching in the dbase
        sb.append(DataTableRowUtil.toChannelIdSQLString(tableName, chid, 
                                                   DataTableRowUtil.LOCATION));
        
        return getByWhereClause( getConnection(), sb.toString());
    }
/**
 * Fetch request card for this event 
 * from the Request_Card table.
 */
    public static RequestCardTN[] fetchRequests (long evid)
    {
             return getByWhereClause( getConnection(), ("WHERE evid = "+evid));
    }    

    // PCSTORED,RETRY 
    public static RequestCardTN[] fetchRequestsOrderBy(long evid, String columnList)
    {
             return getByWhereClause( getConnection(), ("WHERE evid = "+evid) + " ORDER BY " + columnList);
    }    
/**
 * Returns 'true' if a request card for this event and channel exists
 * in the Request_Card table.
 */
    public static boolean requestAlreadyInDbase (long evid, Channel ch)
    {
        RequestCardTN[] list = fetchRequests(evid, ch);
        
        return (list.length > 0);
    }
        
    protected static Connection getConnection() {
      return (conn == null) ? DataSource.getConnection() : conn;
    }

    public static void setConnection (Connection connection) {
      conn = connection;
    }

    public static String getStringHeader() {
        return "Evid            NET.STA.CHN.LOC    Time Window Start       -> End                     Source   Auth     Sauth    T  Prior    PCS  Retry LastRetry           Rcid            Wfid            LdDate\n";
    
    }
    public String toString() {

      Channel ch = ctw.getChannelObj();
      Format s15 = new Format("%-15.15s");
      Format s8 = new Format("%-8.8s");
      Format d6 = new Format("%6d");
      Format s18 = new Format("%-18.18s");
      return ( s15.form(evid.isNull() ? "NULL" : String.valueOf(evid.longValue()))  +" "+
               s18.form(ch.toDelimitedSeedNameString()) +" "+
               ctw.getTimeSpan().toString() +" "+
               s8.form(subsource.toString())  +" "+
               s8.form(auth.toString())  +" "+
               s8.form(staAuth.toString()) +" "+
               type +" "+
               d6.form(priority.isNull() ? 0 : priority.intValue()) +" "+
               d6.form(pcstored.isNull() ? 0 : pcstored.intValue()) +" "+
               d6.form(retry.isNull() ? 0 : retry.intValue()) +" "+
               (lastRetry.isNull() ? "NULL            " : lastRetry.toString().substring(0,19)) +" "+
               s15.form(rcid.toStringSQL()) +" "+
               s15.form(wfid.toStringSQL()) +" "+
               (lddate.isNull() ? "NULL               " : lddate.toString().substring(0,19))
             );
    }
// The following is required because the freeking request_card table doesn't have
// a CHANNELSRC attribute like every other Channel-related table in the schema.
// So the ChannelTN parseNameFromDataTableRow() method dies with java.lang.NoSuchFieldException
    /**
     * Get a Channelname description from a DataTableRow object.
     * Use a DataTableRow because we an use this to parse ANY row type with a
     * channel in it.
     */
        // Must use Column names and not indexes here because every DataTableRow type
        // has different indexes.
        public static Channel parseNameFromDataTableRow (DataTableRow dtr) {
          Channel chan = Channel.create();
          try {
            chan.setNet(dtr.getStringValue("NET"));
            chan.setSta(dtr.getStringValue("STA"));
            chan.setSeedchan(dtr.getStringValue("SEEDCHAN"));
            chan.setLocation(dtr.getStringValue("LOCATION"));
            chan.setChannel(dtr.getStringValue("CHANNEL"));
            //!// chan.setChannelsrc(dtr.getStringValue("CHANNELSRC"));
            } catch (NoSuchFieldException ex)
            {
              System.err.println(ex);
              ex.printStackTrace();
            }

            return chan;
    }

/* Remove static main methods from classes 
    public static void main (String args[])
    {
      //Connection conn = org.trinet.storedprocs.DbaseConnection.create();
      DataSource ds = TestDataSource.create(args[0],args[1]);
      Connection conn = ds.getConnection();
      //RequestCardTN.setConnection(conn);
      
      // Load the Solution
      long evid = Long.parseLong(args[2]);
      //Solution sol = (Solution) Solution.create().getById(evid);
      //ChannelList chanList = ChannelList.readListByName("RCG-TRINET", null);
      //ChannelTimeWindowModel ctwModel = new PowerLawTimeWindowModel(sol, chanList);
      //ChannelableList ctwList = ctwModel.getChannelTimeWindowList();
      //System.out.println("Found "+ctwList.size()+" ctw's");

      System.out.println("Request count: " + RequestCardTN.countRequests(evid));

      // make request cards
      //ActiveArrayList rcList = RequestCardTN.makeList(ctwList, "CI", "test", 12345, "test", "T", 0, 1) ;
      //for (int i = 0; i< rcList.size();i++) {
      //  System.out.println(((RequestCardTN)rcList.get(i)).toString());
      //}
      // Write on to the dbase
      //RequestCardTN rc = (RequestCardTN)rcList.get(0);

    }
*/
}
