package org.trinet.storedprocs;

/*
import sqlj.runtime.ref.DefaultContext;
import oracle.sqlj.runtime.Oracle;
import oracle.jdbc.driver.*;
import oracle.sql.*;
*/

import java.sql.*;
import java.util.*;
import org.trinet.util.gazetteer.LatLonZ;

/* CHANGE HISTORY:

9/18/2000 - added quality, mag and magtype to support eventpriority function.
      Also added check for 'bogusflag'. Made members 'public' so other classes
      outside this package could see them.

*/

/* ******************************************************************
 * THIS WILL BE LOADED INTO ORACLE
 * MUST COMPILE UNDER JDK1.1.X AND USE ONLY LIBRARIES LOADED IN ORACLE
 *******************************************************************/

/**
 * Extract catalog info from the dbase for use by stored procedures. Can't
 * use jasi interface because Oracle 8.0.1 won't support Collections etc. that
 * are used by jasi and upgrading causes problems with SmartSockets, etc.
 */

public class CatEntry {

    public LatLonZ latlonz;
    public double ot;
    public long evid;
    public long locevid;
    public long orid;
    public String source = "";
    public double quality;
    public double mag;
    public String magType;

  // These persist
  /** The dbase connection */
   static public Connection conn;

    /** The SQL prefix used by most CatEntry queries */
    /*
    static String sqlPrefix =
    " Select Event.evid, Event.prefor, Event.selectflag, Origin.subsource, "+
    " Origin.orid, Origin.lat, Origin.lon, Origin.depth, truetime.getEpoch(Origin.datetime,'UTC'), "+ // truetime for UTC -aww 2008/02/08
    " Origin.locevid, Origin.Quality" +
    " from Event, Origin where (Event.prefor = Origin.orid) ";
    */
    static String sqlPrefix =
    " Select Event.evid, Event.prefor, Event.prefmag, Event.selectflag, Origin.subsource," +
    " Origin.orid, Origin.lat, Origin.lon, Origin.depth, truetime.getEpoch(Origin.datetime,'UTC'), "+ // truetime for UTC -aww 2008/02/08
    " Origin.locevid, Origin.Quality, Origin.bogusflag,"+
    " NetMag.magnitude, NetMag.magtype " +
    " from Event, Origin, NetMag"+
    " WHERE (Event.prefor = Origin.orid(+)) and (Event.prefmag = NetMag.magid(+))";
    /*
     static String sqlPrefix =
    "select Event.*, origin.*, netmag.* from Event, Origin, NetMag"+
    " WHERE (Event.prefor = Origin.orid(+)) and (Event.prefmag = NetMag.magid(+))";
    */

    static String whereValid = " and (Event.selectflag = 1) and (Origin.bogusflag = 0) ";

    public CatEntry() {}

    public CatEntry(LatLonZ llz, double ot) {
        latlonz = llz;
        this.ot = ot;
    }

    public CatEntry(LatLonZ llz, double ot, String src) {
        latlonz = llz;
        this.ot = ot;
        this.source = src;
    }
    public CatEntry(LatLonZ llz, double ot, String src, long evid) {
       latlonz = llz;
        this.ot = ot;
        this.source = src;
        this.evid = evid;
    }

    /*
    public CatEntry(LatLonZ llz, double ot, long evid, long orid) {
        latlonz = llz;
        this.ot = ot;
        this.evid = evid;
        this.orid = orid;
    }

    public CatEntry(LatLonZ llz, double ot, long evid, long orid, String src) {
        latlonz = llz;
        this.ot = ot;
        this.evid = evid;
        this.orid = orid;
        this.source = src;
    }
    */


/** Get valid events in this time window */
   public static CatEntry[] getEventsInWindow(double startTime, double endTime) {

       String sql = sqlPrefix + whereValid +
           " and Origin.datetime between truetime.putEpoch(" + startTime +
           ",'UTC') and truetime.putEpoch(" + endTime + ",'UTC')"; // truetime for UTC -aww 2008/02/08
   /*
    " Select Event.evid, Event.prefor, Event.selectflag, Origin.subsource, "+
    " Origin.orid, Origin.lat, Origin.lon, Origin.depth, Origin.datetime "+
    " from Event, Origin "+
    " where Origin.datetime between truetime.putEpoch(" + startTime +
       ",'UTC') and truetime.putEpoch(" + endTime + ",'UTC')" + // truetime for UTC -aww 2008/02/08
    " and (Event.prefor = Origin.orid) and Event.selectflag = 1 ";
   */
       //System.out.println("SQL: \n"+sql);

       return doQuery(sql);
   }

  /**
   * Make a connection. If we are running in the context of the Oracle server,
   * make the "default" connection to the local environment. Otherwise, use
   * the hardwired info which is used for testing outside the server.
   */

    public static void makeConnection() {
        conn = DbaseConnection.getConnection();
    }

    /**
     * Return a cat entry with this evid. Returns null if no entry. Does not
     * require that the select flag for either event be true.  */

    public static CatEntry getCatEntryByEvid(long evid) {

        String where = sqlPrefix +" and Event.evid = "+evid;

        //System.out.println("SQL: "+where);
    
        CatEntry[] ce = doQuery(where);

        if (ce != null && ce.length > 0) {
            return ce[0];
        } else {
            return null;
        }

    }

  /**
   * Return all Entries that satisfy this 'sql' where clause
   */
    public static CatEntry[] getWhere(String where) {
        String sql = sqlPrefix + where;
        return doQuery(sql);
    }

  /**
   * Return all Entries that satisfy this 'sql' query
   */
  public static CatEntry[] doQuery(String sql) {

    makeConnection(); // use existing or make new connection
    if (conn == null) return null;

    CatEntry cat;
    // Note: Oracle VM only does JDK 1.1.x :. can't use ArrayList which is JDK1.2
    Vector list = new Vector();
    //      ArrayList list = new ArrayList();

    Statement sm = null;
    ResultSet rs = null;
    boolean status = true;
    try {

        sm = conn.createStatement();

        rs = sm.executeQuery(sql);

        while ( rs.next() ) {
            cat = CatEntry.parse(rs);
            if (cat == null) break;
            list.addElement(cat);
        }

    } catch (SQLException ex) {
        ex.printStackTrace(System.out);
        status = false;
    }
    finally {
      try {
        if (rs != null) rs.close();
        if (sm != null) sm.close();
      }
      catch (SQLException ex) { }
    }

    CatEntry catArray[] = new CatEntry[list.size()];

    if ( list.size() > 0) list.copyInto(catArray);
    else status = false;

    return (status) ? catArray : null;
  }
  
  /**
   * Execute a 'sql' function
   */
    private static boolean doCallableSQL(String sql) {

        makeConnection(); // use existing or make new connection

        boolean status = true;
        CallableStatement sm = null;
        try {
            sm = conn.prepareCall(sql);
            sm.registerOutParameter(1, java.sql.Types.INTEGER);
            sm.executeUpdate();
            status  = (sm.getInt(1) > 0);
        } catch (SQLException ex) {
            status = false;
            System.err.println("CatEntry doCallableSQL:\n"+sql);
            System.err.println(ex);
            ex.printStackTrace();
        }
        finally {
          try {
            if (sm != null) sm.close();
          }
          catch (SQLException ex) { }
        }
        return status;
    }

   /** Parse resultset into this CatEntry*/
    public static CatEntry parse(ResultSet rs) {
        CatEntry cat = new CatEntry();

        try {

            // !!!! REMEMBER: jdbc doesn't understand fully qualified column name
            //  So you can't use Origin.quality, etc.
            cat.evid    = rs.getLong("evid");

            cat.orid    = rs.getLong("orid");
            double lat  = rs.getDouble("lat");
            double lon  = rs.getDouble("lon");
            double z    = rs.getDouble("depth");

            cat.latlonz = new LatLonZ(lat, lon, z);
            cat.ot      = rs.getDouble("datetime");
            cat.source  = getStringSafe(rs, "subsource");
            cat.quality = rs.getDouble("quality");

            String locstr  = getStringSafe(rs, "locevid");
            if (!locstr.equals("")) {
                cat.locevid = Long.valueOf(locstr).longValue(); //string->long
            }
            cat.mag     = rs.getDouble("magnitude");
            cat.magType = getStringSafe(rs, "magtype");

        } catch ( NullPointerException ex) {
            System.err.println("CatEntry: NullPointerException in parseResults");
            ex.printStackTrace(System.err);
            return null;
        } catch ( SQLException ex) {
            System.err.println("SQLException");
            ex.printStackTrace(System.err);
            return null;
        }

        return cat;
    }

    /*Write and commit this CatEntry to the dbase. 
    public void commit() {
        if (evid == 0) get one;
        String sql = "Insert
    }
    */

    /**
     * Merge two event, preserving the masterEvid's evid.
     * Set the prefor and prefmec of the masterEvid equal to the orid and
     * magid of the slaveEvid. Then set the selectflag of the slaveEvid
     * = 0. Uses the stored procedure EPREF.merge(e1, e2)
     */
    public boolean merge(long masterEvid, long slaveEvid) {
        //String sql = "call JASI.merge("+ masterEvid +", "+slaveEvid+")";
        String sql = "{ ?=call EPREF.merge("+ masterEvid +","+slaveEvid+") }";
        return doCallableSQL(sql);
    }

    /** for safe parsing */
    static String getStringSafe(ResultSet rs, String column) {
        try{
            String str = rs.getString(column);
            if (str == null) return "";
            return str;
        } catch ( NullPointerException ex) {
            System.err.println("NullPointerException");
            ex.printStackTrace(System.err);
            return "";
        } catch ( SQLException ex) {
            System.err.println("SQLException");
            ex.printStackTrace(System.out);
            return "";
        }
    }

    public String toString() {
        return evid +" " +orid+" "+ ot+" " +latlonz.toString()+ " "+source+" "+locevid +" "+ quality + " M"+ magType+ " "+mag;
    }

/*
    public static void main(String args[]) {
      CatEntry ce = CatEntry.getCatEntryByEvid(13973904);
      System.out.println(ce.toString());
    }
*/
} // end of CatEntry class


