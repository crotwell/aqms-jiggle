package org.trinet.waveserver.rt;
public class TCPConnException extends RuntimeException {
    TCPConnException() {
        super();
    }

    TCPConnException(String message) {
        super(message);
    }
}
