package org.trinet.waveserver.rt;
import java.io.*;

/** Abstract base class for objects implementing the WaveClient API serialization protocol
* Concrete subclasses must implement both the readDataMembers(DataInputStream) and
* writeDataMembers(DataOutputStream) methods.
* Implementations of these methods, together with those found in this base class,
* should enable any data member values to be read/written from/into byte arrays.
* For concrete subclasses serialized byte array representations of the data member
* values are used to construct the Packet objects created for a WaveClient generated
* TCPMessage request/reply. One or more Packet objects created by a TCPConn object
* are "serialized" as input/output via the associated TCPConn socket stream.
*
* @see TCPConn
* @see TCPMessage
*/

public abstract class TrinetSerial {

/** Maximum bytes in the serialized output stream of data member values */
    int defaultBytesInOutputBuffer;

/** Default constructor does no initialization. */
    TrinetSerial () { } 

/** Constructor initializes the output byte buffer size. */
    TrinetSerial (int bufferSize) {
        defaultBytesInOutputBuffer = bufferSize;
    } 

/** Implementation parses serialized values from input stream to set data member values*/
    abstract void readDataMembers(DataInputStream dataIn) throws IOException;

/** Implementation writes the serialized byte representation of the data member values to output stream */
    abstract void writeDataMembers(DataOutputStream dataOut) throws IOException;

/** Parses the values of the data members from the specified input stream.
* Requires an implementation of readDataMembers() by a subclass.
* @exception java.io.IOException error occurred parsing the data member values from the stream.
*/ 
    public void fromInputStream(InputStream inputStream) throws IOException {
        DataInputStream dataIn = new DataInputStream(inputStream);
        readDataMembers(dataIn);
    }

/** Parses the values of the data members from the specified byte array. 
* @exception java.io.IOException error occurred parsing the data member values from the byte array.
* @see #fromByteArray(byte[], int, int)
* @exception java.io.IOException error occurred parsing the data member values from the byte array.
*/
    public void fromByteArray(byte [] byteInput) throws IOException {
        fromByteArray(byteInput, 0, byteInput.length);
    }

/** Parses the values of the data members from the specified byte array,
* starting at the specified offset for the specified length.
* Requires an implementation of readDataMembers() by a subclass.
* @see #fromByteArray(byte[])
* @exception java.io.IOException error occurred parsing the data member values from the byte array.
*/ 
    public void fromByteArray(byte [] byteInput, int offset, int len) throws IOException {
        DataInputStream dataIn = new DataInputStream(new ByteArrayInputStream(byteInput, offset, len));
        try {
            readDataMembers(dataIn);
        }
        finally {
            try {
                dataIn.close();
            }
            catch (IOException ex) { ex.printStackTrace();}
        }
    }

/** Writes the byte array representation of all data members into the specified output stream.
* Requires an implementation of writeDataMembers() by a subclass.
* @exception java.io.IOException error occurred writing data member values to OutputStream
*/
    public void toOutputStream(OutputStream outStream) throws IOException {
        ByteArrayOutputStream bytesOutStream = new ByteArrayOutputStream(defaultBytesInOutputBuffer);
        DataOutputStream dataOut = new DataOutputStream(bytesOutStream);
        try {
            writeDataMembers(dataOut);
            bytesOutStream.writeTo(outStream);
        }
        finally {
            try {
                dataOut.close();
            }
            catch (IOException ex) { ex.printStackTrace();}
        }
    }

/** Writes the byte array representation of all data members into the input byte array argument at the specified offset.
* Requires an implementation of writeDataMembers() by a subclass.
* @exception java.io.IOException error occurred writing data member values to outputStream
* @see #toByteArray()
*/
    public void toByteArray(byte [] bytesOutArray, int offset) throws IOException {
        byte [] btmp = toByteArray();
        if (btmp.length > bytesOutArray.length - offset)
                 throw new IndexOutOfBoundsException(" toByteArray() input array too small.");
        else System.arraycopy(btmp, 0, bytesOutArray, offset, btmp.length);
    }

/** Writes the byte array representation of all data members into a new byte array object which it returns.
* Requires an implementation of writeDataMembers() by a subclass.
* @exception java.io.IOException error occurred writing data member values to outputStream
* @see #toByteArray(byte[], int)
*/
    public byte [] toByteArray() throws IOException {
        byte [] retVal = null;
        ByteArrayOutputStream bytesOutStream = new ByteArrayOutputStream(defaultBytesInOutputBuffer);
        DataOutputStream dataOut = new DataOutputStream(bytesOutStream);
        try {
            writeDataMembers(dataOut);
            retVal = bytesOutStream.toByteArray();
        }
        finally {
            try {
                dataOut.close();
            }
            catch (IOException ex) { ex.printStackTrace();}
        }
        return retVal;
    }

/** Compares two byte arrays. Can be utilized in subclasses to test serialized data member values 
* in implementations of equals() and compareTo().
* @return <pre>
*          0 if the byte arrays have same length and equivalent elements.
*         -1 if b[i] < b2[i] or (b1==b2 && b1.length < b2.length)
*          1 if b[i] > b2[i]
*         </pre>
*/
    public static int compareTo(byte [] b1, byte [] b2) {
        int len = Math.min(b1.length, b2.length);
        for (int index = 0; index < len; index++) {
            if (b1[index] > b2[index]) {
                return 1;
            }
            else if (b1[index] < b2[index]) {
                return -1;
            }
        }
        if (b1.length == b2.length) return 0;
        return (b1.length < b2.length) ? -1 : 1;
    }

/** Compares two byte arrays up to either the offset of the first zero value
* or the end of the shorter input buffer.
* Can be utilized in subclasses to test serialized data member values
* in implementations of equals() and compareTo().
* @return <pre>
*          0 if the termination is at same offset and array values are equivalent before it.
*         -1 if b[i] < b2[i] before termination (or second buffer is longer)
*          1 if b[i] > b2[i] before termination (or first buffer is longer)
*         </pre>
*/
    public static int compareToZero(byte [] b1, byte [] b2) {
        int len = Math.min(b1.length, b2.length);
        for (int index = 0; index < len; index++) {
            if (b1[index] == 0) {
              return (b2[index] == 0) ? 0 : -1;
            }
            else if (b2[index] == 0) {
              return (b1[index] == 0) ? 0 : 1;
            }
            else if (b1[index] > b2[index]) {
                return 1;
            }
            else if (b1[index] < b2[index]) {
                return -1;
            }
        }
        if (b1.length == b2.length) return 0;
        return (b1.length < b2.length) ? -1 : 1;
    }

/** Returns true only if the input object is an instance of this class and
*   its data list element values are all equivalent to this object's values.
*/
    public boolean equals(Object object) {
        if (this == object) return true;
        return (object == null || getClass() != object.getClass()) ?  false : true;
    }
}
