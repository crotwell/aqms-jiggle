package org.trinet.waveserver.rt;
public class WaveClientIdException extends WaveClientException {
    WaveClientIdException() {
        super();
    }
 
    WaveClientIdException(String message) {
        super(message);
    }
}
