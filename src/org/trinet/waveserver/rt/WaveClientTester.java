//Test of WaveClient.
package org.trinet.waveserver.rt;
import java.util.*;
import java.io.*;
//import org.trinet.waveserver.rt.*;
import org.trinet.jasi.seed.*;
import org.trinet.util.DateTime;
import org.trinet.util.TimeSpan;
import org.trinet.util.LeapSeconds;
//import org.trinet.jdbc.datatypes.*;

public class WaveClientTester {

    public static void main(String args []) {

        String datePattern = "yyyy-MM-dd:HH:mm:ss.SSS";

        ArrayList theList = new ArrayList(2048);
        int listSize = 0;
        String type = "new";

        int numberOfArgs = args.length;
        if (numberOfArgs < 2) {
            System.err.println("Error in syntax: WaveClientTester <propertyFileName>  <cmdfileName>");
            System.err.println("WaveClient configuration properties:\n"+
                "    waveclient.DEFAULT.debug=true | false verbose output messages\n"+
                "    waveclient.DEFAULT.servers= server-url-address1:port1  server-url-address2:port2  server-url-address3:port3\n"+
                "    waveclient.DEFAULT.verifyWaveforms=true | false confirm seed header packet times and sample counts (slows performance)\n"+
                "    waveclient.DEFAULT.truncateAtTimeGap=true | false don't read data past gap\n"+
                "    waveclient.DEFAULT.connectTimeout= millisecs to connect to server\n"+
                "    waveclient.DEFAULT.maxTimeout= millisecs for socket read\n"+
                "    waveclient.DEFAULT.maxRetries=1 times to try reconnect to server and re-request upon socket error or timeout"
            );
                    
            System.err.println("Command strings sent to waveserver are put in file \"cmdFileName\", one command per line (case-insensitive).\n"+
                "CHANNELS\n" +
                "    Print list of all channels available on waveserver\n"+
                "\nCommands listed below can be added for as many channels as desired for your test:\n"+
                "RATE NET STA SEEDCHAN LOC\n"+
                "    Print sample rate for the specified channel\n"+
                "TIMES NET STA SEEDCHAN LOC\n"+
                "    Print data times for the specified channel\n"+
                "DATA NET STA SEEDCHAN LOC <start yyyy-MM-dd:HH:mm:ss.sss> <duration_secs>\n"+
                "    Print data sample count for the specified channel and time range\n"+
                "PACKETS NET STA SEEDCHAN LOC <start yyyy-MM-dd:HH:mm:ss.sss> <duration_secs>\n"+
                "    Print list of packet seed header info for the specified channel and time range\n" +
                "JASIWAVE NET STA SEEDCHAN LOC <start yyyy-MM-dd:HH:mm:ss.sss> <duration_secs>\n"+
                "    Convert to Jiggle jasi waveform for the specified channel and time range, print waveform summary\n"
            );

            System.exit(-1);
        }

        // Save the command line data file name
        String propFileName  = args[0];

        WaveClient waveClient = null;
        double startTime = (double)System.currentTimeMillis()/1000.;
        int ncmd = 0;
        try {
            // Make a WaveClient
            if (numberOfArgs > 2) type = args[2]; 
            if (type.equalsIgnoreCase("new")) {
               waveClient = new WaveClientNew(propFileName); // property file name 
            }
            else {
               waveClient = new WaveClientOld(propFileName); // property file name 
            }
            int nservers = waveClient.numberOfServers();
            if (nservers <= 0) {
               System.err.println("WaveClientTester error: no data servers specified in property file: " + propFileName);
               System.exit(-1);
            }

           // Dump default attributes
            System.err.println("WCT waveClient.getMaxSequenceNumber     : " + waveClient.getMaxSequenceNumber() );
            System.err.println("WCT waveClient.getMaxRetries()          : " + waveClient.getMaxRetries() ); 
            System.err.println("WCT waveClient.getMaxTimeoutMilliSecs() : " + waveClient.getMaxTimeoutMilliSecs() );
            System.err.println("WCT waveClient.isVerifyWaveforms() : " + waveClient.isVerifyWaveforms() );
            System.err.println("WCT waveClient.isTruncateAtTimeGap() : " + waveClient.isTruncateAtTimeGap() );
            String [] servers = waveClient.listServers();
            System.err.println("WCT waveClient.listServers     : " + servers.length);
            for (int idx=0; idx<servers.length; idx++) {
                System.err.println("    " + servers[idx]);
            }
            System.err.println();

            // Save the command line data file name
            String cmdFileName  = args[1];
            BufferedReader inputReader = new BufferedReader(  new InputStreamReader( new FileInputStream(cmdFileName) )  );
            String command = getInputLine(inputReader);
            if (command == null) {
                System.err.println("Empty input file exiting.");
                System.exit(0);
            }
            boolean haveInput = true;
            while (haveInput) {
                StringTokenizer st = new StringTokenizer(command);
                int icmd = 0;
                command = st.nextToken();

                if (command.startsWith("#") || command.length() == 0) continue;

                if (command.equalsIgnoreCase("channels")) {
                   icmd = 0;
                }
                else if (command.equalsIgnoreCase("rate")) {
                   icmd = 2;
                }
                else if (command.equalsIgnoreCase("times")) {
                   icmd = 4;
                }
                else if (command.equalsIgnoreCase("data")) {
                    icmd = 6;
                }
                else if (command.equalsIgnoreCase("packets")) {
                    icmd = 8;
                }
                else if (command.equalsIgnoreCase("jasiwave")) {
                    icmd = 9;
                }
                else {
                  System.err.println("WCT Unknown input test command: " + command + " skipping ...");
                  continue;
                }

                int status = 0;
                switch (icmd) {
                    case 0: // Test: get list of channels
                        System.err.println("WCT GETCHANNELS START");
                        theList.clear();
                        status = waveClient.getChannels(theList);
                        System.err.println("    WCT waveClient.getChannels(theList) status: " + waveClient.getStatusMessage(status));
                        listSize = theList.size();
                        System.err.println("    WCT Channel list size:" + listSize);
                        if (listSize > 0) {
                           System.err.println("    WCT ChannelList:");
                           for (int index = 0; index < listSize; index++) {
                              //System.err.println("\"" + ((Channel) theList.get(index)).toIdString() + "\"");
                              System.err.println("\"" + ((Channel) theList.get(index)).toString() + "\"");
                           }
                        }
                        System.err.println("    WCT Channel list size:" + listSize);
                        System.err.println("WCT GETCHANNELS DONE");
                        break;
  
                    case 2: // Test: get channel sample rate as double
                    case 4:
                    case 6:
                    case 8:
                    case 9:
                        // Create Channel for method i/o get command string line
                        if (! st.hasMoreTokens()) throw new ChannelDataInputException("Channel input missing net,sta,chn,loc ");
                        String net = st.nextToken();
                        if (! st.hasMoreTokens()) throw new ChannelDataInputException("Channel input missing sta,chn,loc ");
                        String sta = st.nextToken();
                        if (! st.hasMoreTokens()) throw new ChannelDataInputException("Channel input missing chn,loc ");
                        String chn = st.nextToken();
                        String loc = "  ";
                        // added loc token for complete channel specification below  -aww 5/30/2003
                        if (st.hasMoreTokens()) {
                          loc = st.nextToken();
                          if ( loc.equals("\"\"") || loc.equals("--")) loc = "  ";
                        }
                        Channel chan = new Channel(net, sta, chn, loc);
    
                        System.err.println("WCT Requested channel id: " + chan.toIdString());
                        //System.err.println();
      
                        if (icmd == 2) { // Test request sample rate, put result in Channel
                            System.err.println("WCT GETSAMPLERATE AS DOUBLE START");
                            System.err.println("    WCT waveClient.getSampleRate(String...) : " + waveClient.getSampleRate(net, sta, chn, loc) );
                            status = waveClient.getSampleRate(chan);
                            System.err.println("    WCT waveClient.getSampleRate(Channel) status: " +
                                    waveClient.getStatusMessage(status) + "\n" + chan.toString());
                            System.err.println("WCT GETSAMPLERATE DONE");
                            break ;
                        }
                        if (icmd == 4) { // Test get list of time ranges
                            System.err.println("WCT GETTIMES START");
                            theList.clear();
                            status = waveClient.getTimes(net, sta, chn, loc, theList);
                            System.err.println("    WCT waveClient.getTimes(String... List) status: " + waveClient.getStatusMessage(status));
                            listSize = theList.size();
                            System.err.println("    WCT TimeRange list size:" + listSize);
                            System.err.println("    WCT TimeRangeList:");
                            for (int index = 0; index < listSize; index++) {
                                System.err.println(((TimeRange) theList.get(index)).toString());
                            }
                            // System.err.println("WCT WaveClient.getTimes(net, sta, chn, loc, list) Done.");
                            System.err.println("WCT GETTIMES DONE");
                            break;
                        }
                        // get time request data 
                        if (! st.hasMoreTokens()) throw new ChannelDataInputException("missing request start time, duration data");
                        DateTime beginTime = new DateTime(LeapSeconds.stringToTrue(st.nextToken(), datePattern), true); // for UTC -aww 2008/04/01
                        if (! st.hasMoreTokens()) throw new ChannelDataInputException("missing request duration data");
                        int duration = Integer.parseInt(st.nextToken());
                        DateTime endTime = new DateTime(beginTime.getTrueSeconds() + (double)duration, true); // for UTC -aww 2008/04/01
                        // Create TimeWindow for method i/o
                        TimeWindow timeWindow = new TimeWindow(beginTime, endTime);
                        //System.err.println("WCT Requested TimeWindow: " + timeWindow.toString());
                        //System.err.println();
    
                        if (icmd == 6) { // Test: get time series sample array 
                            System.err.println("WCT GETDATA START");
                            theList.clear();
                            status = waveClient.getData(net, sta, chn, loc, beginTime, duration, theList);
                            System.err.println("    WCT waveClient.getData(net, sta, chn, loc...) status: " + waveClient.getStatusMessage(status));
                            listSize = theList.size();
                            System.err.println("    WCT TimeSeries list size:" + listSize);
                            if (listSize > 0) {
                                 System.err.println("    WCT TimeSeriesList:");
                                 for (int index = 0; index < listSize; index++) {
                                      System.err.println(theList.get(index).toString());
                                 }
                            }
                            System.err.println("WCT GETDATA DONE");
                            break;
                        }
                        if (icmd == 8) { // Test: get miniSEEDpackets
                            System.err.println("WCT GETPACKETDATA START");
                            List packet = waveClient.getPacketData(net, sta, chn, loc, beginTime, duration);
                            if (packet != null) listSize = packet.size();
                            else listSize = -1;
                            System.err.println(net+" "+sta+" "+chn+" "+loc+"   WCT Packet list size:" + listSize);
                                 for (int index = 0; index < listSize; index++) {
                                      System.err.println(SeedHeader.parseSeedHeader((byte []) packet.get(index)).toString());
                                 }
                            //System.err.println("WCT WaveClient.getPacketData(net, sta, chn, loc ...) Done.");
                            System.err.println("WCT GETPACKETDATA DONE");
                            break;
                       }
    //
                       if (icmd == 9) {
                            org.trinet.jasi.Waveform jasiwave = org.trinet.jasi.AbstractWaveform.createDefaultWaveform();
                            //org.trinet.jasi.ChannelName chanName = new org.trinet.jasi.ChannelName(net, sta, chn);
                            org.trinet.jasi.ChannelName chanName = jasiwave.getChannelObj().getChannelName();
                            chanName.setNet(net);
                            chanName.setSta(sta);
                            chanName.setSeedchan(chn);
                            chanName.setChannel(chn);
                            chanName.setLocation(loc);
                            //long endms = System.currentTimeMillis();
                            //endms -= 120000;
                            //long startms = endms - 60000l;
                            //jasiwave.timeStart = new DataDouble((double)startms/1000.);
                            //jasiwave.timeEnd = new DataDouble((double)endms/1000.);
                            jasiwave.setTimeSpan(new org.trinet.util.TimeSpan(beginTime.getTrueSeconds(), endTime.getTrueSeconds()));
                            System.err.println("WCT GETJASIDATARAW START");
                                status = waveClient.getJasiWaveformDataRaw(jasiwave);
                            System.err.println("    WCT waveClient.getJasiWaveformDataRaw(Waveform) status: " + status + " : " +
                                    waveClient.getStatusMessage(status) + "\n" + chan.toString());
                            System.err.println("    WCT JASIWAVE info:");
                            jasiwave.scanForBias();
                            jasiwave.scanForAmps();
                            System.err.println(jasiwave.toString()); 
                            System.err.println("WCT GETJASIDATARAW DONE");
                            break ;

                      }
    //
                }
                command = getInputLine(inputReader);
                if (command == null) haveInput = false;
                ncmd++;
            }
            inputReader.close();
        }
        catch (Exception ex) {
            System.err.println(ex.toString());
            ex.printStackTrace();
        }
        finally {
            if (waveClient != null) waveClient.close();
            startTime = (double) System.currentTimeMillis()/1000. - startTime;
            ncmd = (ncmd == 0) ? 1 : ncmd; 
            System.err.println("Total run time: " + startTime + " cmd cnt: " + ncmd + " cmd/sec: " + startTime/ncmd);
        }
    }

    static final class ChannelDataInputException extends RuntimeException {
        ChannelDataInputException() {
            super();
        }
 
        ChannelDataInputException(String message) {
            super(message);
        }
    }

    static final class ServerDataInputException extends RuntimeException {
        ServerDataInputException() {
            super();
        }
 
        ServerDataInputException(String message) {
            super(message);
        }
    }

    public static String getInputLine(BufferedReader reader) {
        String retVal = null;
        try {
            while (reader.ready()) {                     // input text is available
                reader.mark(132);                        // mark beginning of line
                char c = (char) reader.read();           // read 1st char
                if (c == '#' || c == '!') {              // skip lines starting with '#' or '!'
                    reader.readLine();
                }
                else {                                   // may have data line
                    reader.reset();                      // read entire line
                    retVal = reader.readLine();
                    if (retVal.length() == 0) continue; // skip any empty lines
                    reader.mark(132);                   // reset mark to start of next line
                    break;                              // have data line of text done.
                }
            }
        }
        catch(IOException ex) {
            ex.printStackTrace();
        }
        return retVal;
    }
}
