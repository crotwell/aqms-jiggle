package org.trinet.waveserver.rt;
import java.util.*;
import org.trinet.util.DateTime;

/** Boolean methods with arguments should return the result of a comparing the input parameter(s) relative to invoking instance. */
public interface TimeRange {
/** Return the TimeRange reference of this instance. */
    public TimeRange getTimeRange() ;

/** Return the start Date of the range. */
    public DateTime getStartTimestamp() ; // for UTC -aww 2008/04/01

/** Return the end Date of the range. */
    public DateTime getEndTimestamp() ; // for UTC -aww 2008/04/01

/** Return time in leap seconds since Date(0). */
    public double getStartTimeSecs() ;

/** Return time in leap seconds since Date(0). */
    public double getEndTimeSecs() ;

/** Return time difference in seconds between the start time of the input TimeRange and the end time of this TimeRange instance. */
    public double timeGapSeconds(TimeRange nextTimeRange) ;

/** Return time difference in seconds between the input time and the end time of this TimeRange instance. */
    public double timeGapSeconds(DateTime nextStartTimestamp) ;

    public boolean after(Date date) ;
    public boolean after(TimeRange timeRange) ;
    public boolean before(Date date) ;
    public boolean before(TimeRange timeRange) ;
    public boolean excludes(Date date) ;
    public boolean excludes(TimeRange timeRange) ;
    public boolean excludes(Date start, Date end) ;
    public boolean includes(TimeRange timeRange) ;
    public boolean includes(Date start, Date end) ;
    public boolean includes(Date date) ;
    public boolean overlaps(TimeRange timeRange) ;
    public boolean overlaps(Date start, Date end) ;
    public boolean within(TimeRange timeRange) ;
    public boolean within(Date start, Date end) ;

/** Returns true if startTime > endTime for this instance. */
    public boolean isReversed() ;

/** Returns false == times are not defined || isReversed() == true for this instance. */
    public boolean isValid() ;
}
