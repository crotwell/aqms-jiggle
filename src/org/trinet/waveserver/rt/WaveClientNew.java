package org.trinet.waveserver.rt;

public class WaveClientNew extends org.trinet.waveserver.rt.WaveClient {

    static {
        org.trinet.waveserver.rt.Channel.new_wire_format = true;
    }

    public WaveClientNew() {
        super();
    }

    public WaveClientNew(int maxRetries, int maxTimeoutMilliSecs, boolean verifyWaveforms, boolean truncateAtTimeGap) {
        super(maxRetries, maxTimeoutMilliSecs, verifyWaveforms, truncateAtTimeGap);
    }

    public WaveClientNew(String propertyFileName) throws WaveClientException {
        super(propertyFileName);
    }

}

