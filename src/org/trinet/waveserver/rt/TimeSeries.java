package org.trinet.waveserver.rt;
import java.util.*;
import org.trinet.util.DateTime; // for UTC -aww 2008/04/01

/** Implementation of the WaveClient API TimeSeries class.
* This class contains an array of contiguous time series sample values and their starting and ending timestamps.
* The number of data samples is assumed equivalent to the length of the array and
* uniform sampling rate is assumed, thus the data sampling rate can be estimated from the timestamps.
* The data source is not known to this class, that association must be managed externally. 
* The WaveClient.getData(...) method returns the requested time series for a seismic channel and time span 
* as a collection of instances of this class (the WaveClient converts a DataCounts collection
* into a TimeSeries collection).
*<p>
*<b> Note public constructor and getXXX methods alias the data member references,
*    thus the member data reflect any external modification to the aliases.
*    If preservation of the original data is required, first clone the external alias
*    before modifying its contents.
*</b>
*
* @see DataCounts
* @see WaveClient
*/
public class TimeSeries {

/** Time of the first data sample. */
    DateTime startTimestamp = null; // for UTC -aww 2008/04/01

/** Time of the last data sample. */
    DateTime endTimestamp = null; // for UTC -aww 2008/04/01

/** The data sample values. */
    int [] samples = null;

/** Default constructor, null members.*/
    TimeSeries() {}

/** Constructor aliases data member references to the input values. */
    public TimeSeries(DateTime startTimestamp, DateTime endTimestamp, int [] samples) { // for UTC -aww 2008/04/01
        this.startTimestamp = startTimestamp;
        this.endTimestamp = endTimestamp;
        this.samples = samples;
    }

/** Returns an alias reference to the first sample timestamp. */
    public DateTime getStartTimestamp() { // for UTC -aww 2008/04/01
        return startTimestamp;
    }

/** Returns an alias reference to the last sample timestamp. */
    public DateTime getEndTimestamp() { // for UTC -aww 2008/04/01
        return endTimestamp;
    }

/** Returns an alias reference to the the data sample array. */
    public int [] getSamples() {
        return samples;
    }

/** Returns the calculated uniform sampling rate. 
* @return Double.NaN if timestamp values are null, zero, or undefined or samples.length == 0.
*/
    public double getSampleRate() {
        if (startTimestamp == null || endTimestamp == null || samples == null || samples.length == 0) return Double.NaN;
        double divisor =  endTimestamp.getTrueSeconds() - startTimestamp.getTrueSeconds(); // for UTC -aww 2008/04/01
        if (divisor <= 0.) return Double.NaN;
        return (double) samples.length / divisor;
    }

/** A deep copy */
    public Object clone() {
        TimeSeries timeSeries = null;
        try {
            timeSeries = (TimeSeries) super.clone();
        }
        catch (CloneNotSupportedException ex) {
            ex.printStackTrace();
        }

        timeSeries.startTimestamp = (DateTime) startTimestamp.clone(); // for UTC -aww 2008/04/01
        timeSeries.endTimestamp = (DateTime) endTimestamp.clone(); // for UTC -aww 2008/04/01
        timeSeries.samples = (int []) samples.clone();
        return timeSeries;
    }

/** Input object must be an instance of TimeSeries.
* @return <pre>
* -1 this object's starting timestamp is less than the input object's value,
*    or it is equivalent and its ending timestamp is less than the input object's value.<br>
*  0 this object's starting and ending timestamps equal the input object's values<br>
*  1 this object's starting time is greater than the input object's value,
*    or it is equivalent and its ending timestamp is greater than the input object's value.
* </pre>
* @exception java.lang.ClassCastException input object is not an instance of this class. 
*/
    public int compareTo(Object object) {
        TimeSeries timeSeries = (TimeSeries) object;
        if (this.startTimestamp.before(timeSeries.startTimestamp)) return -1;
        else if (this.startTimestamp.equals(timeSeries.startTimestamp)) {
           if (this.endTimestamp.equals(timeSeries.endTimestamp)) return 0;
           else if (this.startTimestamp.before(timeSeries.startTimestamp)) return -1;
        }
        return 1;
    }

/** Returns true only if input object is an instance of this class and
* its data member values are equal to this object's. 
*/
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        TimeSeries timeSeries = (TimeSeries) object;
        return (this.startTimestamp.equals(timeSeries.startTimestamp) &&
            this.endTimestamp.equals(timeSeries.endTimestamp) &&
            Arrays.equals(this.samples, timeSeries.samples) ) ? true : false;
    }

/** Returns the String concatenation of the labeled start and end times and the numberOfSamples.
*/
    public String toString() {
        StringBuffer sb = new StringBuffer(128);
        sb.append("TimeSeries : ");
        sb.append(startTimestamp);
        sb.append(" => " );
        sb.append(endTimestamp);
        sb.append(" samples: ");
        if (samples != null) sb.append(samples.length);
        else sb.append("null array");
        return sb.toString();
    }

/** Convenience wrapper of System.out.println(toString()).*/
    public void print() { 
        System.out.println(toString());
    }
}
