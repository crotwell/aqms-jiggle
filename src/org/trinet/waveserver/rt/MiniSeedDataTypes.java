package org.trinet.waveserver.rt;
/** Constants defining codes for the TRINET WaveClient miniSEED data types.
*/

public interface MiniSeedDataTypes {

    public static final int UNKNOWN_DATATYPE             = 0;
    public static final int INT_16                       = 1;
    public static final int INT_24                       = 2;
    public static final int INT_32                       = 3;
    public static final int IEEE_FP_SP                   = 4;
    public static final int IEEE_FP_DP                   = 5;

//  FDSN Network codes
    public static final int STEIM1                       = 10;
    public static final int STEIM2                       = 11;
    public static final int GEOSCOPE_MULTIPLEX_24        = 12;
    public static final int GEOSCOPE_MULTIPLEX_16_GR_3   = 13;
    public static final int GEOSCOPE_MULTIPLEX_16_GR_4   = 14;
    public static final int USNN                         = 15;
    public static final int CDSN                         = 16;
    public static final int GRAEFENBERG_16               = 17;
    public static final int IPG_STRASBOURG_16            = 18;

//  Older Network codes
    public static final int SRO                          = 30;
    public static final int HGLP                         = 31;
    public static final int DWWSSN_GR                    = 32;
    public static final int RSTN_16_GR                   = 33;

//  Definitions for blockette 1000
    public static final int SEED_LITTLE_ENDIAN           = 0;
    public static final int SEED_BIG_ENDIAN              = 1;

// MiniSEED data frame size in bytes 
    public static final int MINISEED_REC_512             = 512;
    public static final int MINISEED_REC_4096            = 4096;

}
