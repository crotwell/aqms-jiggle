package org.trinet.waveserver.rt;
/** Class provides simple integer repeating numeric count (i.e. modulo remainder).
* It is used by the WaveClient class to provide TCPMessage identifier tags.
* @see TCPMessage
* @see WaveClient
*/
public class Counter implements Cloneable, java.io.Serializable {

/** Current value of the sequence counter */
    int value;

/** Maximum units in the sequence (sequence count starts at zero).
* The counter rolls over to zero upon reaching this value.
*/
    int rolloverValue;

/** Default constructor sets the initial value to 0 and the roll-over value to Integer.MAX_VALUE.*/
    public Counter() {
        this(0);
    }

/** Constructor sets the counter's initial value to the specified input value.
* and sets its roll-over value to Integer.MAX_VALUE */
    public Counter(int value) {
        this(value, Integer.MAX_VALUE);
    }

/** Constructor sets the initial value and the roll-over value to the specified input values. */
    public Counter(int value, int rolloverValue) {
        if(rolloverValue >= 0) this.rolloverValue = rolloverValue;
        else this.rolloverValue = Integer.MAX_VALUE;
        setValue(value);
    }

/** Resets this counter's value to the specified input value. */
    public int setValue(int value) {
        this.value = value % rolloverValue;
        if (this.value < 0) this.value += rolloverValue;
        return value;
    }

/** Returns the current counter sequence value. */
    public int getValue() { return value;}

/** Adds 1 to the current sequence value: nextValue=(currentValue+1)%rolloverValue */
    public int plus() {
        setValue(this.value + 1);
        return value;
    }

/** Adds the specified input amount to current value. */
    public int plus(int amount) {
        setValue(this.value + amount);
        return value;
    }

/** Substracts 1 from the current sequence value. */
    public int minus() {
        setValue(this.value - 1);
        return value;
    }

/** Substracts the specified input amount to current value. */
    public int minus(int amount) {
        setValue(this.value - amount);
        return value;
    }

/** A deep copy. */
    public Object clone() {
        Counter cnt = null;
        try {
            cnt = (Counter) super.clone();
        }
        catch (CloneNotSupportedException ex) {
           ex.printStackTrace();
        }
        return cnt;
    }

/** Returns true only if the input object is an instance of this class and
* its data member values are identical to this object's values.
*/
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        Counter cnt = (Counter) object;
        return ( (this.value == cnt.value) && (this.rolloverValue == cnt.rolloverValue) ) ? true : false;
    }

/** Returns the String concatenation of the labeled current value and the sequence roll-over value. */
    public String toString() {
        return "Counter.value: " + value + " rolloverValue: " + rolloverValue;
    }

/** Convenience wrapper of System.out.println(getValue()).*/
    public void printValue() {
        System.out.println(getValue());
    }
}
