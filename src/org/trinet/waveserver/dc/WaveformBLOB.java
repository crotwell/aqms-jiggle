package org.trinet.waveserver.dc;
import java.io.*;
import java.security.*;
import java.sql.*;
import java.util.*;
//import oracle.sql.*;
import org.trinet.util.*;
import org.trinet.jasi.seed.SeedHeader;

public class WaveformBLOB {
  public static final int MIN_PACKET_SIZE=512;
  private static int packetSize = MIN_PACKET_SIZE;
  private static byte [] packetHeader = new byte[MIN_PACKET_SIZE]; 
  private static int bytesRead;

  WaveformBLOB() {}

  private static byte [] getWaveformBuffer(String filename, int traceoff, int nbytes) {
    byte[] packetBuffer = new byte [0];
    try {
      if (nbytes < 0) throw new IOException("requested negative nbytes");

      FileInputStream fis = new FileInputStream(filename);
      int bytesAvailable = fis.available();
      if (traceoff > bytesAvailable) throw new IOException("traceoff > available bytes");
      BufferedInputStream bis = new BufferedInputStream(fis);
      packetBuffer = new byte [Math.min(nbytes, bytesAvailable-traceoff)];
      long bytesSkipped = bis.skip((long) traceoff);
      if (bytesSkipped != (long) traceoff) throw new IOException("bytes skipped < traceoff");
      // read only min since if requested nbytes too big below throws IndexOutOfBounds
      bytesRead = bis.read(packetBuffer, 0, Math.min(nbytes, packetBuffer.length));
      bis.close();
    } catch (FileNotFoundException ex) {
      System.err.println ("WaveformBLOB getWaveformBuffer File not found: " + filename);
    } catch (IOException ex) {
      System.err.println ("WaveformBLOB getWaveformBuffer IO error: " + ex.toString());
    }
    return packetBuffer;
  }

  // STATIC METHOD CALLED BY ORACLE STORED PROCEDURE !
  public final static int isVersionUTC() {
      return 1;
  }

  // STATIC METHOD CALLED BY ORACLE STORED PROCEDURE !
  //public static int fillWaveformBLOB(oracle.sql.BLOB blob, String filename, int traceoff, int nbytes) {
  public static int fillWaveformBLOB(java.sql.Blob blob, String filename, int traceoff, int nbytes) {
    try {
      byte [] packetBuffer = getWaveformBuffer(filename, traceoff, nbytes);
      //OutputStream bos = blob.getBinaryOutputStream();
      OutputStream bos = blob.setBinaryStream(0l);
      bos.write(packetBuffer, 0, bytesRead);
      bos.flush();
      bos.close();
    } catch (IOException ex) {
      System.err.println ("WaveformBLOB fillWaveformBuffer IO error: " + ex.toString());
    } catch (SQLException ex) {
      System.err.println ("WaveformBLOB fillWaveformBuffer SQL error: " + ex.toString());
    }
    return bytesRead;
  }
  // STATIC METHOD CALLED BY ORACLE STORED PROCEDURE !
  //public static int fillWaveformBLOB(oracle.sql.BLOB blob, String filename, int traceoff, int nbytes,
  public static int fillWaveformBLOB(java.sql.Blob blob, String filename, int traceoff, int nbytes,
           double waveformStartTime, double waveformEndTime) {
    byte[] outputBuffer = new byte[0];
    try {
      byte [] packetBuffer = getWaveformBuffer(filename, traceoff, nbytes);
      if (bytesRead == 0 || packetBuffer.length == 0) return 0;
      outputBuffer = extractTimeRange(waveformStartTime, waveformEndTime, packetBuffer, 0, bytesRead);
      //OutputStream bos = blob.getBinaryOutputStream();
      OutputStream bos = blob.setBinaryStream(0l);
      bos.write(outputBuffer, 0, outputBuffer.length);
      bos.flush();
      bos.close();
    } catch (IOException ex) {
      System.err.println ("WaveformBLOB fillWaveformBuffer IO error: " + ex.toString());
    } catch (SQLException ex) {
      System.err.println ("WaveformBLOB fillWaveformBuffer SQL error: " + ex.toString());
    }
    return outputBuffer.length;
  }

  private static int getPacketSize(ByteArrayInputStream bis) throws IOException {
      if (bis.available() > 0) {
        int packetBytesRead = bis.read(packetHeader, 0, MIN_PACKET_SIZE);
        if (packetBytesRead < MIN_PACKET_SIZE) throw new IOException("header bytes read < " + MIN_PACKET_SIZE);
      }
      SeedHeader h = SeedHeader.parseSeedHeader(packetHeader);
      return (h != null) ? h.getSeedBlockSize() : MIN_PACKET_SIZE;
  }
  private static byte[] extractTimeRange(double waveformStartTime, double waveformEndTime,
      byte[] dataBuffer, int offset, int length) {
    ArrayList list = null;
    byte [] outputBuffer = new byte[0];
    try {
      if (dataBuffer == null || dataBuffer.length == 0 || length+offset > dataBuffer.length || length < 512)
         throw new InvalidParameterException( "input arg out of range or null");
      list = new ArrayList(length/MIN_PACKET_SIZE);
      ByteArrayInputStream bis = new ByteArrayInputStream (dataBuffer,offset,length);
      packetSize = getPacketSize(bis);
      bis.reset(); // start over
      int bisOffset = 0;
      while (bis.available() > 0) {
        byte[] packet = new byte[packetSize]; 
        int packetBytesRead = bis.read(packet, 0, packetSize);
        if (packetBytesRead != packetSize) break;
        bisOffset += packetSize;
        SeedHeader h = SeedHeader.parseSeedHeader(packet);
        if ( h.getSeedBlockSize() != packetSize)  // throws exception if assumes all packets same size
            throw new IOException("data packet size change old:"+packetSize+"new:"+h.getSeedBlockSize());
        if ( h.isData() ) {
            // NCEDC tables data epoch times include leap seconds; 
            // internally, the jasi java code uses nominal epoch seconds.
            // Leap seconds should be substracted/added when data is 
            // read/written from/to datasource by the concrete subtypes,
            // Use "nominal" time in the data request to the db for
            // timeseries, the nominal times are used to filter the
            // miniseed packet header times.
            // DataSource:
            // isDbTimeBaseLeap(), isDbTimeBaseNominal() 
           double packetStartTime = h.getDatetime(); // should be UTC (leap) seconds - aww 2008/01/24
           if (packetStartTime >= waveformEndTime) break;
           double packetEndTime = DateTime.trimToMicros(packetStartTime + (h.getSampleCount()-1)/h.getSamplesPerSecond());
           if (packetEndTime < waveformStartTime) continue;
           list.add(packet);
           if (packetEndTime >= waveformEndTime) break;
        }
      }
      bis.close();
      outputBuffer = new byte[list.size()*packetSize];
      bisOffset = 0;
      for (int idx=0; idx < list.size(); idx++) {
        System.arraycopy((byte []) list.get(idx),0,outputBuffer,bisOffset,packetSize);
        bisOffset += packetSize;
      }
    }
    catch (IOException ex) {
      System.err.println ("WaveformBLOB extractTimeRange IO error: " + ex.toString());
    }
    catch (Exception ex) {
      System.err.println ("WaveformBLOB extractTimeRange general exception: " + ex.toString());
    }
    return outputBuffer;
  }
/*
  public static final void main(String [] args) {
     WaveformBLOB wb = new WaveformBLOB();
     //oracle.sql.BLOB blob = new oracle.sql.BLOB();
     //wb.fillWaveformBLOB(blob, './701559', traceoff, nbytes) ;
     byte [] packetBuffer = wb.getWaveformBuffer("./701559", 0, 331776);
     if (wb.bytesRead == 0 || packetBuffer.length == 0) System.out.println("zero bytes read");
     byte[] outputBuffer = wb.extractTimeRange(0., 999999999999., packetBuffer, 0, wb.bytesRead);
     System.out.println("bytes in output buffer: " + outputBuffer.length);
  }
*/
}
