package org.trinet.web;

import java.text.*;
import java.awt.*;
import java.util.*;
import java.io.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

import org.trinet.jasi.*;
import org.trinet.formats.*;
import org.trinet.jiggle.JiggleProperties;
import org.trinet.util.GenericPropertyList;

//import Acme.JPM.Encoders.GifEncoder;

/**
 * Create gif format snapshot of an event showing an arbitrary
 * list of channels. Places no limit on the number of channels.
 * Sets beginning of window to origin of event,
 * distance sorts the channels.
 */

public class SnapGifFromList extends SnapShot {

    public static void main(String args[]) {

        if (args.length < 3) { // not enough args
          System.out.println("Usage: SnapGifFromList <evid> <property-file> <chanfile> [secsPerPage] [out-file]");
          System.out.println( "Set the secsPerPage command line value <= 0, to instead use the value of");
          System.out.println( "property 'secsPerPage' in the properties file, else defaults to: " + maxSecs);
          System.exit(-1);
        }

        // event ID
        long evid = Long.parseLong(args[0]);

        // Read in the properties file
        String propfile = (!args[1].equals("*")) ? args[1] : propertyFileName;
        // NOTE: User's Jiggle home path
        setProperties(new JiggleProperties(propfile));
        System.out.println("Read properties from "+ getProperties().getUserFilePath()+"/"+propfile);

        String inFilename = args[2];

        // Override properties with values specified on command line
        // seconds to plot
        if (args.length > 3) {
          int ival = Integer.parseInt(args[3]);
          if (ival > 0 ) maxSecs = ival;
        }

        // destination .gif file
        String gifFile = System.getProperty("user.dir") + GenericPropertyList.FILE_SEP + evid + ".gif";  //default
        if (args.length > 3) {
          gifFile = args[4];
        }

        org.trinet.jiggle.WaveServerGroup waveClientGrp = null;
        try {
          // Make a WaveClient
          waveClientGrp = getProperties().getWaveServerGroup();  // note: need JiggleProperties not Generic

          if (waveClientGrp == null || waveClientGrp.numberOfServers() <= 0) {
            System.err.println(
               "getDataFromWaveServer Error: no or bad waveservers in properties file. " );
            System.err.println("waveServerGroupList = " + getProperties().getProperty("waveServerGroupList"));
            System.exit(-1);
          }
          System.err.println("waveServerGroupList = " + getProperties().getProperty("waveServerGroupList"));
          AbstractWaveform.setWaveDataSource(waveClientGrp);
        } catch (Exception ex) {
          System.err.println(ex.toString());
          ex.printStackTrace();
        }

        System.out.println("Making connection...");
        DataSource init = new DataSource();
        DataSource.set(getProperties().getDbaseDescription()); // aww 2008/06/04
        if (debug) System.out.println(init.toString());

        // Makes the .gif file, returns the grapical component in case you want to display it below
        Component view = makeGif(evid, inFilename, maxSecs, gifFile);

        // Return codes:
        if (view == null) {
            System.err.println("View is null: exit status = -1");
            System.exit(-1);    // failure
        } else {
            System.err.println("exit status = 0");

            // make a frame
            if (debug) {

              JFrame frame = new JFrame("SnapGifFromList of "+evid);

              frame.addWindowListener(new WindowAdapter() {
                  public void windowClosing(WindowEvent e) {System.exit(-3);}
              });

              frame.getContentPane().add( new JScrollPane(view) );       // add scroller to frame
              frame.pack();
              frame.setVisible(true);

            } else {
              System.exit(0);   // success
            }
        }
   }

    // //////////////////////////////////////////////////////////////////

    public static Component makeGif(long id, String inFilename, int maxSecs, String outFile) {

        // must make instance because makeViewWithPhases() is not static
        SnapShot snapShot =  new SnapShot();
        Component view = null;

        try {
          ChannelableList clist = NSCLformat.readInFile(inFilename);
          view = snapShot.makeViewFromList(id, clist, maxSecs);
        } catch (Exception ex) {
          ex.printStackTrace();
        }

        //encodeGifFile(view, outFile);
        return encodeImageFile(view, outFile) ? view : null;
    }

    /*
    public static boolean encodeGifFile(Component view, String gifFile) {

        if (view == null) return false;

        try {
            // component must be in a frame for this to work
            JFrame frame =  new JFrame();
            frame.getContentPane().add( view );     // add scroller to frame
            frame.pack();

            // Creates an off-screen drawable image to be used for double buffering. (it will be blank)
            if (debug) System.out.println("making image...");

            Image image = view.createImage(view.getWidth(), view.getHeight());

            // get the graphics context of the image
            if (debug) System.out.println("getting graphics object...");
            Graphics g = image.getGraphics();

            // copy the frame into the image's graphics context
            if (debug) System.out.println("printing graphics object...");
           // view.print(g);   // changed DDG 6/5/03
            view.printAll(g);  // walk down whole component tree

            // open the output file
            if (debug) System.out.println("opening file...");
            File file = new File(gifFile);
             FileOutputStream out = new FileOutputStream(file);

            // create the encoder
            if (debug) System.out.println("encoding...");
            GifEncoder encoder = new GifEncoder(image, out);

            // encode it
            encoder.encode();

            if (debug) System.out.println("Flushing...");

            out.flush();
            if (debug) System.out.println("Close file...");
            out.close();

        } catch (FileNotFoundException exc) {
            System.err.println("File not found: " + gifFile);
            return false;
        } catch (IOException exc) {
            System.err.println("IO error: "+ exc.toString());
            return false;
        } catch (OutOfMemoryError err) {
            System.err.println("Out of memory error: "+ err.toString());
            return false;
        }

        return true;
    }
    */

} // end of class

