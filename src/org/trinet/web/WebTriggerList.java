package org.trinet.web;

import java.text.*;
import java.util.*;
import java.net.URL;
import java.sql.*;
import java.io.*;

import org.trinet.jdbc.*;
import org.trinet.util.*;
import org.trinet.jasi.*;
import org.trinet.util.Format;      // CoreJava printf-like Format class

/**
 * Make the files necessary to support the TriNet trigger and waveform review
 * web pages.  */


public class WebTriggerList extends WebCatList
{

    static String thisAppName = "WebTriggerList";

/** Make an empty WebCat */
public  WebTriggerList ()
{

}

/**
 * Main for testing: % WebCat [hours-back]  (default = 1)
 */
    public static void main (String args[]) {

    final int secondsPerHour = 60*60;

    // ?
    /* Java WIERDNESS note:
        <boron>% java WebCat ?
        args[0] = /j/
        :. must use equalsIgnoreCase("?") else it looks like "j"!!!
    */
    if (args.length == 0 || args[0].equalsIgnoreCase("?")) {    // no args on command-line value
        showUsage();
        System.exit (0);
    }

    // days back
    if (args.length > 0) {  // translate epoch second on command-line value
        try {
        Double val = Double.valueOf(args[0]);       // convert arg String to 'double'
        hoursback = (double) val.doubleValue();
        } catch (NumberFormatException ex) {
        showUsage();
        System.exit (0);
        }
    }

    // check path
    if (args.length > 1) {
        htmlOut = args[1];
    }

    host = defaulthost; // default
    if (args.length > 2) {
        host = args[2];
    }

    String dbasename = host+"db";   // default
    if (args.length > 3) {
        dbasename = args[3];
    }

        System.out.println ("Making connection...");
    DataSource init = TestDataSource.create(host, dbasename);  // make connection

    EventSelectionProperties props = new EventSelectionProperties();
        //props.setFiletype("jiggle"); // test file i/o in the user's home "jiggle" subdir

    // set time properties
    props.setTimeWindow(hoursback, TimeUnits.getUnitsName(TimeUnits.HOUR));

    // set flags that will result in only triggers
    props.setEventTypesIncluded("trigger");
    props.setEventValidFlag(true);
    props.setOriginDummyFlag(true);

    //  System.out.println ("Fetching: "+props.getTimeSpan().toString());

    Solution sol[] = (Solution[])Solution.create().getByProperties(props).toArray(new Solution[0]);

// dump the index html file
    try {

        if (htmlOut.equals("stdout")) {

        System.out.println (writeCatBody(sol));

        } else {
        FileOutputStream out = new FileOutputStream("catBody.html");

        PrintStream prt = new PrintStream(out);

        prt.println (writeCatBody(sol));
        //  System.out.println (writeCatBody(sol));

        prt.close();
        }

    } catch (FileNotFoundException ex) {
          ex.printStackTrace();
    } catch (Exception ex) {
          ex.printStackTrace();
    }

    System.exit(0);
    }

} // end of class


