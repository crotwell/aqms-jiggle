package org.trinet.apps;

// //////////////////////////////////////////////////////////////////////////
//
//  THIS CLASS IS DEPRECATED - it has been replaced by a Perl script
//
// ////////////////////////////////////////////////////////////////////////
import java.io.*;
import java.net.*;
import java.util.*;

import org.trinet.util.ExternalCommand;
import org.trinet.util.DateTime;

/**
 *
  Socket based HYPOINVERSE2000 solution server. <p>

  Accept connections at well known port address, accept an .ARC format stream over a socket,
  write it to a file, run a script to locate the event and pass back .ARC format result that
  come via StdOut.<p>

  SYNTAX: SolServer <port#> <commandScript>

 * @author Doug Given
 */

/*
 The basic server design is from the book _Java in a Nutshell_ by David Flanagan.
 Written by David Flanagan.  Copyright (c) 1996 O'Reilly & Associates.
 http://www.oreilly.com/catalog/javanut/examples/section7/Server.java
*/
/*
 basalt (Solaris)
<15> Socket info:
<15>  local port    = 6660
<15>  remote port   = 3940
<15>  in buff size  = 25200
<15>  out buff size = 16384
<15>  timout        = 0
<15>  linger        = -1
*/
//public class SolServer extends Thread {
public class SolServer {

    public final static int DEFAULT_PORT = 6610;
    protected static int listenerPort = DEFAULT_PORT;
    protected static final int MinPort = 1028;   // don't allow ports < this
    protected static ServerSocket listen_socket;

/** The external command or script used to run Hypoinverse. Should take the event
 * ID as its first and only argument and be of the form:
 * 'command <evid>'*/
    protected static String commandString = "locate";

    /** Max. number of thread that are allowed to be active at one time. */
//    protected final static int MaxThreads = 10;

    /** Counts the total number of thread created in a run. */
    protected static int threadCounter = 0;

    /** Counts the total number of active threads. */
    protected static int activeThreads = 0;

    /** List of active connections. */
    ArrayList connectionList = new ArrayList();

    public SolServer() {
      this(DEFAULT_PORT);
    }

    // Create a ServerSocket to listen for connections on;  start the thread.
    public SolServer(int port) {

        if (!setPort(port)) {
          System.err.println("* Bad port #: must be > "+ MinPort);
        } else {
          try {
              // defaults to default InetAddress and an
              // incoming connection indications (a request to connect) set to 50
            listen_socket = new ServerSocket(port);
          }
          catch (IOException e) {
            fail(e, "Exception creating server socket");
          }
        }

    // The body of the server thread.  Loop forever, listening for and
    // accepting connections from clients.  For each connection,
    // create a Connection object to handle communication through the
    // new Socket.

        try {
          // Dump some startup info for logging
          System.out.println("SolServer listening: host = "+ InetAddress.getLocalHost().getHostAddress()+
                             "  listener port = " + listen_socket.getLocalPort());
          //System.out.println("Socket timeout = "+listen_socket.getSoTimeout());
          System.out.println("  External command  = '" + commandString + " 99999'");
          System.out.println("  Example file name = z99999_12.arcin") ;

          // run forever, accept infinite connections
            while(true) {
                // returns a NEW TCP client socket once a connection is accepted
                Socket client_socket = listen_socket.accept();  // blocks on this listen
                threadCounter++;
                System.out.println("Server: listener creating ConnectionHandler # "+threadCounter+
                " at "+ new DateTime().toString());

                ConnectionHandler c = new ConnectionHandler(client_socket, threadCounter);
 //todo               connectionList.add(c);
            }
        }
        catch (IOException e) {
            fail(e, "Exception while listening for connections");
        }
    }

/** Set the listener port number. */
    boolean setPort(int port) {
      if (port > MinPort) {
        this.listenerPort = port;
        return true;
      }
      return false;
    }

/**  Exit with an error message, when an exception occurs.*/
    public static void fail(Exception e, String msg) {
        System.err.println(msg + ": " +  e);
        System.exit(1);
    }

/** Start the server up, listening on the specified port.
 * SYNTAX: SolServer <port#> <commandScript>
 *  */
    public static void main(String[] args) {

        if (args.length < 2) {
          System.out.println("SYNTAX: SolServer <port#> <commandScript>");
          System.exit(0);
        }

        if (args.length >= 1) {
            try { listenerPort = Integer.parseInt(args[0]);  }
            catch (NumberFormatException e) {
              System.err.println("Bad port number argument /"+args[0]+"/") ;
              System.err.println(e.getMessage());
              System.exit(-1);
            }
        }
        if (args.length >= 2) {
            commandString = args[1];
        }

        new SolServer(listenerPort);
    }
}
// //////////////////////////////////////////////////////////////////////////////////////////
// Inner class

/** This class is the thread that handles all communication with a client.
 * It expects to get a well formatted Hypoinverse .arc file image which it
 * writes to a file. It then runs an external script, which is defined as
 * the 2nd commandline argument. When the external script is done, this thread
 * reads the resulting output file and passes it to the client over the socket. */
class ConnectionHandler extends Thread {
    protected Socket socket;

    protected BufferedReader in;
    protected PrintStream out;

    protected int threadNo = 0;
    protected long evid = 0;

    // Hypoinverse
    StringBuffer stdErr;
    StringBuffer stdOut;
    int exitStatus = 0;

    /** Prefix of file written as Hypo input.*/
    static final String FilePrefix = "z";
    /** Suffix of file written as Hypo input.*/
    static final String InFileSuffix = ".arcin";
    /** Suffix of file written as Hypo output.*/
    static final String ResultFileSuffix = ".arcout";

    // Initialize the streams and start the thread
    public ConnectionHandler(Socket client_socket, int threadNumber) {
        socket = client_socket;
        threadNo = threadNumber;
        logit(" Connection from: "+client_socket.getInetAddress());

// debug //        dumpSocketInfo(socket);

        this.start();  // start the handler thread
    }

    /** Dump socket characteristics info. For debugging. */
    public void dumpSocketInfo (Socket s) {
        try {
            logit("Socket info:");
            logit(" local port    = "+s.getLocalPort());
            logit(" remote port   = "+s.getPort());
            logit(" in buff size  = "+s.getReceiveBufferSize());
            logit(" out buff size = "+s.getSendBufferSize());
            logit(" timout        = "+s.getSoTimeout());
            logit(" linger        = "+s.getSoLinger());
        } catch (IOException e) {
            logit(e.getMessage());
        }
    }
    /** Write logging info. Prefix all output with the thread number because multiple
     Threads may run at once and their output may be interleaved in the log. */
    private void logit (String str) {
        System.out.println("<"+threadNo+"> "+str);
    }

    // Provide the service.
    public void run() {
        String line;
        StringBuffer message = new StringBuffer();
        int len;

        try {
          // create the socket in/out streams
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new PrintStream(socket.getOutputStream());
        }
        catch (IOException e) {
            try { socket.close(); } catch (IOException e2) { ; }
            logit("Exception while getting socket streams: " + e);
            return;
        }

        // read input from the socket
        try {
            for(;;) {
                // read in a line
                line = in.readLine();
                if (line == null) { // test for null first to avoid null pointer exception -aww 07/26/2007
                  logit("Abnormal end of socket input.");
                  break;   // no more input
                }
                else if (line.startsWith("|FILE|")) {    // format "|FILE| xxxxxxxx"
                  try { evid = Long.parseLong(line.substring(7).trim());  }
                  catch (NumberFormatException e) {
                    logit("Bad event ID on |FILE| line. "+line);
                    evid = 0;   // todo: fail here?
                  }
                  logit("Evid = "+evid);
                }
                else if (line.equals("|EOF|")) {
                  logit("End of input");
                  break;
                }
                else {
                  message.append(line+"\n");       // build up a complete message
                  logit(line);   // dump it
                }
            }
        }
        catch (IOException e) {
            logit(e.getMessage());
        }

        // Write the .arcin file
        dumpToCommandInputFile(evid, message.toString());

        // run Hypoinverse code here, doesn't return until external command is done.
        runExternalCommand();

        // Return results to client via the socket. Close socket when done.
//        returnResult(out);
        returnResultFromFile(out);

        try {
            in.close();
            out.close();
        } catch (Exception ex) {
            logit(ex.getMessage());
        } finally {
            try {
                socket.close();
                } catch (IOException e) {;}
        };

    }
    /** produce the cannonical output data file name. It is <evid>_<threadNo>
     * Example: "38877234_213"
     * */
    protected String  getUniqueString () {
      return evid + "_" + threadNo;
    }
    /** Produce the cannonical file name for the input to the External Command.
     * It is <prefix><evid>_<threadNo>.<suffix>
     * Example: "z38877234.arcin". Note that it is called ".arcin" because it is input
     * to the external command, although it is output to us.*/
    protected String  getOutFilename () {
      return FilePrefix + getUniqueString() + InFileSuffix;
    }
    /** produce the cannonical file name for the results from the External Command.
     * It is <prefix><evid>_<threadNo>.<suffix>
     * Example: "z38877234.arcout". Note that it is called ".arcout" because it is output
     * from the external command, although it is input to us.*/
    protected String  getResultFilename () {
      return FilePrefix + getUniqueString() + ResultFileSuffix;
    }
    /**
     * Write the string to a file named "z"+evid+".arcin".
     */
    boolean dumpToCommandInputFile(long evid, String str) {

      FileWriter fw = null;
      PrintWriter pw = null;

      File file = new File(getOutFilename());

      try {
        fw = new FileWriter(file);
        pw = new PrintWriter(fw);

        // Hypoinverse requires that 1st line be blank
        pw.print("\n");

        // Now write the phase data
        pw.print(str);

      } catch (Exception ex) {
        logit(ex.getMessage());
        logit("Error creating: "+file.getName());
        return false;
      } finally {
        try {
          fw.close();    // flush and close
          pw.close();
        }
        catch (IOException ex) {
          logit(ex.getMessage());
          logit("Error closing file writer: " +file);
        }
        fw = null;   // GC
        pw = null;
      }

      return true;
    }

    /** Results of running the script should be in the file getResultFilename().
     * Just write that file's contents over the socket. */
    private boolean returnResultFromFile (PrintStream out) {

        String filename = getResultFilename();
        BufferedReader in = null;

        try {

          in = new BufferedReader(new FileReader(filename));
          String instr;
          // Continue to write lines while there are still some left to read
          while ((instr = in.readLine()) != null) {
              out.println(instr);
              logit(">"+instr);
          }

        } catch (FileNotFoundException ex) {
            logit ("* No such file: "+getResultFilename());
            logit(ex.getMessage());
        } catch (Exception ex) {
            logit ("* Error reading result file: "+getResultFilename());
            logit(ex.getMessage());
            return false;

        } finally {
            try {
                out.flush();
                in.close();             // close the file
            } catch (Exception ex2) {
                logit(ex2.getMessage());
            }
        }
        return true;
  }


/** If the results of running the script are in the runExternalCommand() stdOut buffer
 *  following the token "|DONE|", dump that to the outgoing socket.
 * NOTE: this has been problematic - sometimes the full results are not sent.
 *       so this method was abandoned in favor of reading and sent the result file.
 *       DDG 6/20/05 */
    private boolean returnResult(PrintStream out) {

      BufferedReader result = new BufferedReader(new StringReader(stdOut.toString()));
      String line;
      boolean flagSeen = false;

//      System.err.println("-----");
//      System.err.println(stdOut.toString());
//      System.err.println("-----");

// Pass stdout text from external command to the client over the socket
// line-by-line.
// Ignore everything before the "-DONE-" flag

      logit ("-- Pass back to client -- buff size = "+stdOut.length());
      int knt = 0;

      try {
        while ((line = result.readLine()) != null) {

            knt += line.length();

            if (flagSeen) {
                out.println(line);
                logit(line);   // write to log
            } else {
                logit("<> "+line);
                if ( line.startsWith("|DONE|") ||
                     line.startsWith("-DONE-") ) flagSeen = true;   // "-DONE-" a kludge, Windoz can't echo "|" (pipe)
            }
        }
      }
      catch (IOException e) {
          logit(e.getMessage());
      }
      logit ("-- Pass back done: total chars = "+knt);
      out.flush();
      out.close();  // terminating the socket signals client that we're finished

      return true;
    }

    private Socket getSocket() {
      return socket;
    }

    /** Run the external command to process the event. Blocks until completed. */
    void runExternalCommand () {

        // name and args of the external script are passed as an array
      String command[] = {SolServer.commandString , getUniqueString()};
//      String command[] = {"T:\\CodeSpace\\solserver\\Test\\FakeHypo.bat", "99999_123"};

      // blocks until external process completes
      ExternalCommand exComm = new ExternalCommand(command);

      // capture the output from the command so we can pass it back to the caller
      stdErr = exComm.getStdErr();
      stdOut = exComm.getStdOut();
      exitStatus = exComm.exitStatus();
    }
}

