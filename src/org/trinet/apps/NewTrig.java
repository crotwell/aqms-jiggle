package org.trinet.apps;
import org.trinet.jasi.*;
import org.trinet.storedprocs.waveformrequest.RequestGenerator;
import org.trinet.util.*;
import org.trinet.util.gazetteer.LatLonZ;
import java.io.File;
import java.text.*;
import org.trinet.jdbc.datasources.*;

/**
 *  Generate waveform requests for a fixed time slice for candidate channels in a named list.
 *  
 *  There are two main modes:
 *  1) Create a new trigger event (st) from scratch based on the specified options. 
 *  2) Use an existing event in the dbase (-i switch), but this may result in DUPLICATE
 *     requests and waveform rows since no checking for such is performed.
 *  
 *  Most of the configuration values come from the properties file, but some of the
 *  property values can be overridden with command line arguments.
 *  
 *  Be careful specifying the channel time window model in the properties file. 
 *  NewTriggerChannelTimeWindowModel and PowerLawTimeWindowModel have different
 *  requirements (e.g. PowerLaw requires an event with a location).
 *  
 *  <code>
 *
 *  Usage: org.trinet.apps.NewTrig -[switches] [<lat> <lon> [z] [magnitude] [bogusFlag] [selectFlag] [etype]]
 *  Option switches:
 *  -h              this help info
 *  -v              verbose output
 *  -P <props-file> file with the waveform request generator properties (default: newtrig.props)
 *  -i <evid>       associate requests with an existing event.evid
 *  -X              do not write a new event or its requests to db (test mode)
 *
 *  Db connection options (to override property file values):
 *  -U <URL>        override URL of dbase host property
 *  -d <name>       override dbaseName property
 *  -u <username>   override dbaseUser property
 *  -p <password>   override dbasePasswd property
 *
 *  Options to specify new event's time, and/or the window used for request timespans:
 *  -s <start-time> date-time string format either quoted "date time" string:
 *                  "yyyy-MM-dd HH:mm:ss.SSS",
 *                  or instead use ',' a comma delimiter:
 *                  yyyy-MM-dd,HH:mm:ss.SSS
 *
 *  -e <end-time>   date-time string formated same as start-time.
 *  -l <seconds>    length of waveform request window from start-time"
 *
 *  Using the NewTriggerChannelTimeWindowModel, specify either the starting and ending times
 *  or the starting time and a length seconds, if you enter all three, the length is ignored.
 *  When no -e or -l option, is specified the duration defaults to the model's 'maxWindowSize'.
 *
 *  Using the -i option without the -s start-time option sets the model's window start to the
 *  datetime of the event's prefor minus the model's preEventSize seconds.
 *
 *  Alternatively, you can use the PowerLawTimeWindowModel with the -i <id> option, or by
 *  specifying the -s start-time option followed by an argument list (not options) of 
 *  the new event lat and lon, and optionally its depth, magnitude, bogusFlag, selectFlag, and etype,
 *  where the defaults are: 0. deg, 0. deg, 0. km, 2.0 Mh, 1, 1 and 'st'.
 *
 *  When using the -i switch option, the existing db values are used instead of trailing arguments.
 *  For either model, the input properties file should specify the request generator's properties.
 *
 *  Input properties: 'auth' and 'subsource' are used for the new event's auth and subsource values.
 *  </code>
 */
public class NewTrig {

    static String TimeFormat = EpochTime.YYYY_NO_ZONE_FORMAT;
    static String DefaultPropFileName = "newtrig.props";
        
    public static void main(String[] args) {

        EnvironmentInfo.setApplicationName("NewTrig");
                  
        boolean noWrite = false;
        boolean verbose = false;
                  
        // shuttle sonic:  2009/9/12 00:45:00 UTC
        double startTime = 0.0;
        double endTime   = 0.0;
        double duration  = 0.0;
                    
        String dbaseHost = null;
        String dbase = null;
        String user = null;
        String pass = null;
        String propFileName = DefaultPropFileName;
        long eventId = 0; 
        String timeStrStart = "";
        String timeStrEnd = "";
                  
        // process options in command line arguments                  
        GetOpt getopt = new GetOpt(args, "hXvU:d:u:p:P:i:s:e:l:");
        getopt.optErr = true;
        int ch = -1;

        while ((ch = getopt.getopt()) != GetOpt.optEOF) {
           if ((char)ch == 'h') {
               printCommandOptions();
               System.exit(0);
           } else if ((char)ch == 'U'){
               dbaseHost = getopt.optArgGet(); 
           } else if ((char)ch == 'd'){
               dbase = getopt.optArgGet();                            
           } else if ((char)ch == 'u'){
               user = getopt.optArgGet(); 
           } else if ((char)ch == 'p'){
               pass = getopt.optArgGet(); 
           } else if ((char)ch == 'P'){
               propFileName = getopt.optArgGet(); 
           } else if ((char)ch == 'i'){
               eventId = getopt.processArg(getopt.optArgGet(), eventId);
           } else if ((char)ch == 'X'){
               noWrite = true;                   
           } else if ((char)ch == 'v'){
               verbose = true; 
           } else if ((char)ch == 's'){
               timeStrStart = getopt.optArgGet();
               timeStrStart = timeStrStart.replace(',',' '); // allow ',' delimiter between the date and time fields of arg string -aww 2014/05/21
               if ( timeStrStart.indexOf('.') < 0 ) timeStrStart += ".000";
                   
           } else if ((char)ch == 'e'){
               timeStrEnd = getopt.optArgGet();
               timeStrEnd = timeStrEnd.replace(',',' '); // allow ',' delimiter between the date and time fields of arg string -aww 2014/05/21
               if ( timeStrEnd.indexOf('.') < 0 ) timeStrEnd += ".000";

           } else if ((char)ch == 'l'){
               // No good handling of malformed strings here
               duration = getopt.processArg(getopt.optArgGet(), duration); 
           } else {
               // illegal option on line
               printCommandOptions();
               System.exit(1);
           }        
        } // end of option parsing while loop

        // parse the properties file
        File file = new File(propFileName);
        String parent = file.getParent(); 
        JasiDatabasePropertyList newProps = null;
        System.out.println("Reading properties from file: " + propFileName);
        if ( parent != null ) { // a fully specified path+file
            newProps = new JasiDatabasePropertyList(propFileName, null); 
        }
        else {
            newProps = new JasiDatabasePropertyList(propFileName); 
        }
        if (verbose) System.out.println("Properties:\n" + newProps.toDumpString());

        // setup the db connection info
        DbaseConnectionDescription dbDescription = newProps.getDbaseDescription();
        // override properties with any command line arg values
        if (user      != null) dbDescription.setUserName(user);
        if (pass      != null) dbDescription.setPassword(pass);
        if (dbase     != null) dbDescription.setDbaseName(dbase);
        if (dbaseHost != null) dbDescription.setHostName(dbaseHost);

        if (! dbDescription.isValid()) {
            System.err.println("Invalid dbase spec: "+dbDescription.toString());
            System.exit(1);                    
        }
            
        // Connect to the dbase 
        DataSource.createDefaultDataSource();
        if (!DataSource.set(dbDescription)) {
            System.err.println("Invalid dbase: "+dbDescription.toString());
            System.exit(1);                        
        }
        else System.out.println("Connected to dbase: "+dbDescription.toString());
                
        Solution sol = Solution.create();        

        // Read Sol from dbase if ID was given
        if (eventId > 0) {
            sol = sol.getById(eventId);    // get existing sol from dbase
            if (sol == null) {
                System.err.println("Error: no such solution in dbase - evid = "+eventId);
                System.exit(1);
            }
        }
          
        // Create a request generator
        RequestGenerator rg = new RequestGenerator();
        rg.setProperties(newProps);    
        ChannelTimeWindowModel model = rg.getModel();
        
        // override 'writeReqs' property if commandline -X is specified
        // Note: this can only stop writting, you can't force writing if
        // the property is set not to write (is this a problem?)
        if (noWrite) rg.setWriteReqs(false);
        if (verbose) rg.setVerbose(verbose);
          
        // Sanity checks of time window
        if (timeStrStart.length() > 0) {
          if (validateDateTime(timeStrStart)) {     // Validate date/time string format
            startTime = LeapSeconds.stringToTrue(timeStrStart); // -aww 2010/02/16
          } else {
            System.exit(1);
          }
        }
        if (timeStrEnd.length() > 0) {
          if (validateDateTime(timeStrEnd)) {     // Validate date/time string format
            endTime = LeapSeconds.stringToTrue(timeStrEnd); // -aww 2010/02/16
          } else {
            System.exit(1);
          }
        }
        // no starttime?
        if (startTime == 0.0) {
            if (eventId == 0) {
                System.err.println("ERROR: You must specify a start time.");
                System.exit(1);
            } else {
                startTime = sol.getTime() - model.getPreEventSize();
                System.out.println ("Warning: No start time given. ");
                System.err.println ("         Setting start time to OT - Model.PreEventSize.");
            }
        }
        // if endtime is not given, use duration to calculate it
        if (endTime == 0.0) {
            // if duration not given use Model's MaxWindowSize
            if (duration == 0.0) {
                duration = model.getMaxWindowSize();
                System.out.println ("Warning: No end time or window length given.");
                System.out.println ("         Setting length to Model.MaxWindowSize "+duration + " secs.");
            }
            model.setMaxWindowSize(duration);          // make sure we don't get truncated
            endTime = startTime + duration;
        } else {
            duration = endTime - startTime; // final duration
        }
        
        //inverted times?
        if (duration < 0) {
            System.err.println("Error: start time is later than end time.");
            System.err.println("Start: "+LeapSeconds.trueToString(startTime)); // -aww 2010/02/16
            System.err.println("End  : "+LeapSeconds.trueToString(endTime)); // -aww 2010/02/16
            System.exit(1);                      
        }
        // duration too large?
        if (duration > model.getMaxWindowSize()) {
            System.err.println("Warning: Window size exceeds the Model's maximum window size of "
                                +  model.getMaxWindowSize()+ " secs");
            System.err.println("   Either change the window length you requested or");
            System.err.println("   change the model's 'maxWindowSize' propery.");
            System.exit(1);                              
        }
        // duration too small?
        if (duration < model.getMinWindowSize()) {
            System.err.println("Warning: Window size is smaller than the Model's minimum window size of "
                                +  model.getMinWindowSize()+ " secs");
            System.err.println("   Either change the window length you requested or");
            System.err.println("   change the model's 'minWindowSize' propery.");
            System.exit(1);                              
        }

        // Event OT outside the window?
        // (Perhaps this should be loosened)
        if (eventId != 0) {         
            double ot = sol.getTime();
            if (ot < startTime || ot > endTime) {
                System.err.println("ERROR: Existing event "+eventId+" origin time is outside the time window.");
                System.err.println("OT   : "+LeapSeconds.trueToString(ot)); // -aww 2010/02/16
                System.err.println("Start: "+LeapSeconds.trueToString(startTime)); // -aww 2010/02/16
                System.err.println("End  : "+LeapSeconds.trueToString(endTime)); // -aww 2010/02/16
                System.exit(1);                                      
            }
        }
        else  { // If needed, create a solution from scratch - using properties & args

            System.out.println("New event create ... at " + new DateTime());

            // Defaults for new event, a bogus trigger:
            int numLocPh = 0;
            int numMagSta = 0;
            double locRMS = 0.;
            double magRMS = 0.;
            // 
            int bogusFlag = 1;
            int selectFlag = 1;
            String etype = EventTypeMap2.getMap().toJasiType(EventTypeMap2.TRIGGER);
            double lat = 0.;  //bogus
            double lon = 0.;
            double z = 0.;
            double prefMag = 2.0; // default event preferred magnitude
  
            int nextArgIdx = getopt.optIndexGet();
            if (args.length > nextArgIdx) {
  
              if ( args.length < nextArgIdx+2 ) {
                System.err.println("Error: NewTrig missing a lat,lon pair value");
                System.exit(1);
              }
  
              try {
                lat = Double.parseDouble(args[nextArgIdx++]);
                lon = Double.parseDouble(args[nextArgIdx++]);
  
                if (args.length > nextArgIdx) {
                  z = Double.parseDouble(args[nextArgIdx++]);
                }
  
                if (args.length > nextArgIdx) {
                  prefMag = Double.parseDouble(args[nextArgIdx++]);
                }
  
                if (args.length > nextArgIdx) {
                  bogusFlag = Integer.parseInt(args[nextArgIdx++]);
                }
  
                if (args.length > nextArgIdx) {
                  selectFlag = Integer.parseInt(args[nextArgIdx++]);
                }
  
                if (args.length > nextArgIdx) {
                  etype = args[nextArgIdx++];
                }
                //NumLocPhases    10
                if (args.length > nextArgIdx) {
                  numLocPh = Integer.parseInt(args[nextArgIdx++]);
                }
                //NumMagStations  10
                if (args.length > nextArgIdx) {
                  numMagSta = Integer.parseInt(args[nextArgIdx++]);
                }
                //LocRMS          .5
                if (args.length > nextArgIdx) {
                  locRMS = Double.parseDouble(args[nextArgIdx++]);
                }
                //MagRMS          .25
                if (args.length > nextArgIdx) {
                  magRMS = Double.parseDouble(args[nextArgIdx++]);
                }
              }
              catch (Exception ex) {
                  ex.printStackTrace();
                  System.exit(1);
              }
            }

            // Assign new db id
            sol.setUniqueId();   // assign an evid
            sol.setParentId(sol.getId().longValue());  // do this or it looks like a clone

            // Override defaults with properties or command line values
            sol.setTime(startTime);

            sol.setLatLonZ(new LatLonZ(lat, lon, z));
            sol.validFlag.setValue(selectFlag);
            sol.dummyFlag.setValue(bogusFlag);
            sol.eventType.setValue(EventTypeMap2.getMap().toJasiType(etype));

            String auth = rg.getProperties().getProperty("auth", "UNK");
            sol.setAuthRegion(auth);
            sol.setAuthority(auth);
            String src = rg.getProperties().getProperty("subsource", EnvironmentInfo.getApplicationName());
            sol.eventSource.setValue(src);
            sol.setSource(src);
            sol.setWho( EnvironmentInfo.getUsername() );
            if ( numLocPh > 0 ) {
                sol.usedReadings.setValue(numLocPh);
                sol.totalReadings.setValue(numLocPh);
            }
            if ( locRMS > 0. ) {
                sol.rms.setValue(locRMS);
            }
                            
            Magnitude myMag = Magnitude.create();
            myMag.algorithm.setValue("HAND");
            myMag.value.setValue(prefMag);
            myMag.setType("h");
            if ( numMagSta > 0 ) {
                myMag.usedStations.setValue(numMagSta);
                myMag.usedChnls.setValue(numMagSta*2);
            }
            if ( magRMS > 0. ) {
                myMag.error.setValue(magRMS);
            }
            myMag.assign(sol);
            sol.setPreferredMagnitude(myMag);

            if (noWrite) {
                  if (verbose) System.out.println("No write mode: evid not committed at " + new DateTime());
            }
            else {
              // Commit the new solution
              try {
                  if (verbose) {
                      System.out.println("Committing evid = "+sol.getId() + " at " + new DateTime()) ;
                  }
                  sol.commit();
              }
              catch (JasiCommitException ex) {
                  System.err.println(ex.toString());
                  System.exit(1);
              }
            }
        }

        // Set the fixed time window in the model
        TimeSpan ts = new TimeSpan(startTime, endTime);
        model.setFixedTimeWindow(ts);

        System.out.println(sol.getNeatStringHeader()) ;
        System.out.println(sol.toNeatString()) ;
        if (verbose){ 
          System.out.println("Start : "+LeapSeconds.trueToString(startTime)); // -aww 2010/02/16
          System.out.println("End   : "+LeapSeconds.trueToString(endTime)); // -aww 2010/02/16
          System.out.println("Length: "+duration);
          System.out.println("Model : "+model.getClassname());
          System.out.println("List  : "+model.getCandidateListName());
        }
                               
        // generate request cards        
        int numCreated = 0;
        try {
            numCreated = rg.generateRequests(sol);
        }
        catch (Exception ex) {
            System.err.println(ex.toString());
            System.exit(1);
        }
        
        if (noWrite){
            numCreated = rg.getRequestCardList().size();
            System.out.println ("NewTrig would have created "+ numCreated + " requests at " + new DateTime());
        } else {
            System.out.println ("NewTrig created "+ numCreated + " requests at " + new DateTime());
        }
        //if (verbose) rg.dumpRequestCardList(sol.getId().longValue());

        System.exit(0); // success

    }

    /**
    * Check a date/time string for validity.
    * @param timeStr
    * @return
    */
    private static boolean validateDateTime(String timeStr) {
       
        SimpleDateFormat dateFormat = EpochTime.getDateFormat(TimeFormat);
       
        try {
            dateFormat.parse(timeStr);
        } catch (ParseException e) {
            //e.printStackTrace();
            System.err.println ("ERROR  : unparseable date/time string |"+timeStr+"|");
            System.err.println ("Example: \""+TimeFormat+"\"");
            return false;
        }

        return true;
    }
           
    /**
    * Print out command help.
    *
    */
    public static void printCommandOptions() {
        System.out.println("\nUsage:  -[options] [ <lat> <lon> [z] [magValue] [bogusFlag] [selectFlag] [etype] [#locPh] [#magSta] [locRMS] [magRMS] ]"); 
        System.out.println("\noptions:"); 
        System.out.println(" -h              this help info");
        System.out.println(" -v              verbose output");
        System.out.println(" -P <props-file> file with the waveform request generator properties; default: "+DefaultPropFileName);
        System.out.println(" -i <evid>       associate requests with an existing event.evid (does not create new event)");
        System.out.println(" -X              do not write new event or its requests to dbase (test mode)");
        System.out.println(" --- Optional db connection values (override property file values) ---");
        System.out.println(" -U <URL>        override URL of dbase host property");
        System.out.println(" -d <name>       override dbaseName property");                   
        System.out.println(" -u <username>   override dbaseUser property");
        System.out.println(" -p <password>   override dbasePasswd property");
        System.out.println(" --- Options for specifying the timespan of request cards ---");                   
        System.out.println(" -s <start-time> a quoted \"date time\" string of format: \""+TimeFormat+"\"");
        System.out.println("                 or to avoid quotes, put a ',' between the date and time fields");
        System.out.println(" -e <end-time>   date-time string with same format as the start-time");
        System.out.println(" -l <seconds>    length of waveform request window from start-time\n");
        System.out.println("  Using the NewTriggerChannelTimeWindowModel, you specify either the starting and ending times");
        System.out.println("  or the starting time and a length seconds, if you enter all three, the length is ignored.");
        System.out.println("  When no -e or -l option, is specified, the duration defaults to the model's 'maxWindowSize'.");
        System.out.println("  Using the -i option without the -s start-time option sets the model's window start to the");
        System.out.println("  datetime of the event's prefor minus the model's preEventSize seconds.\n");
        System.out.println("  Alternatively, you can use the PowerLawTimeWindowModel with the -i <id> option, or by instead");
        System.out.println("  specifying the -s start-time option followed by an argument list (not options) consisting of "); 
        System.out.println("  the new event lat and lon, and optionally its depth, magnitude, bogusFlag, selectFlag, and etype,");  
        System.out.println("  the respective defaults are: 0. deg, 0. deg, 0. km, 2.0 Mh, 1, 1 and 'st'.\n");
        System.out.println("  When using the -i switch option, the existing db values are used instead of trailing arguments.\n");
        System.out.println("  For either model, the input properties file should specify the request generator's properties;"); 
        System.out.println("  properties 'auth' and 'subsource' specify the new event's auth and subsource values, respectively.");
                   

    }
}
