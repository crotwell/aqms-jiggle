package org.trinet.apps;
import java.util.*;

import org.trinet.jasi.*;
import org.trinet.jasi.engines.*;
import org.trinet.jasi.magmethods.*;
import org.trinet.jiggle.*;
import org.trinet.util.*;

/**
   Stand-alone program for calculating the ML of one event by scanning
   the waveforms for peak amps.
*/
public class CalcMlFromWaveforms {

    static boolean debug = true;
    static boolean useWaveserver = false;

    public CalcMlFromWaveforms() { }


    public static void main(String args[]) {

      BenchMark bm = new BenchMark();

      int evid = 13997460;  // aww why is this hard-wired?

// Doug commented this block out // aww 11/24
//    if (args.length <= 0) {   // no args
//      System.out.println
//      ("Usage: CalcMLFromWaveforms <evid> ");
//      System.exit(0);
//    }

      if (args.length > 0) {
        Integer val = Integer.valueOf(args[0]);
        evid = (int) val.intValue();
      }

      EnvironmentInfo.setApplicationName("CalcMlFromWaveforms");
      EnvironmentInfo.setNetworkCode("CI");
      // will mark data as "human" not automatic.
      EnvironmentInfo.setAutomatic(false);

      System.out.println ("Making connection...");
      DataSource init = TestDataSource.create();  // make connection
      init.setWriteBackEnabled(true);

      WaveClient waveClient = null;

      if (useWaveserver) {
        // cheating
        String propFile = "/tpp/www/waveserver.cfg";

        try { // Make a WaveClient

          System.out.println("Creating WaveClient using: "+propFile);

          waveClient = WaveClient.createWaveClient().configureWaveClient(propFile); // property file name

    //      System.out.println(waveClient.toString());

          int nservers = waveClient.numberOfServers();
          if (nservers <= 0) {
              System.err.println("getDataFromWaveServer Error:"+
                 " no data servers specified in input file: " +
                 propFile);
              System.exit(-1);
          }

        }
        catch (Exception ex) {
          System.err.println(ex.toString());
          ex.printStackTrace();
        }
        finally {
    //      if (waveClient != null) waveClient.close();
        }
        AbstractWaveform.setWaveDataSource(waveClient);
      }

// ///////

     if (debug) System.out.println("Making MasterView for evid = "+evid);

     // Make the "superset" MasterView
     MasterView mv = new MasterView();

     int i = 0;

     // wave scanner will load them
     mv.setWaveformLoadMode(MasterView.LoadNone);

 // mv.setTimeAlign(true);
     bm.print("BenchMark: startup done ");
     bm.reset();

     mv.defineByDataSource(evid);

     bm.print("BenchMark: event parameters loaded ");
     bm.reset();

  // get the first solution in the list
  //    Solution sol = (Solution) mv.solList.solList.get(0);
     Solution sol = null;
     if (mv.solList.size() > 0) {
       sol = (Solution) mv.solList.get(0);

       System.out.println("SOL: "+sol.toString());
       System.out.println("There are " + mv.getPhaseCount() +
          " phases, "+ mv.getWFViewCount() + " time series"+
                   " and "+mv.getAmpCount() + " amps");
     } else {
        System.out.println("No dbase entry found for evid = "+evid);
        System.exit(0);
     }

     // load channel info
     if (sol.waveformList.size() > 100) {
       MasterChannelList.set(ChannelList.readCurrentList());
       bm.print("BenchMark: MasterChannelList loaded ");
       bm.reset();
     }

     /****************
      * Use create() instead of SoCalML constructor.
      * Configuration Params are hardcoded in SoCalML.setDefaultParams()
      * Call configure() to setup method and default params.
      // ML
      SoCalML ml = new SoCalML();

     ml.setTrimResidual(1.0);
     ml.setMinSNR(8.0);    //per Kate Hutton 4/2002
     ml.setRequireCorrection(true);
     **********************************/

     MagnitudeMethodIF ml = AbstractMagnitudeMethod.create("org.trinet.jasi.magmethods.TN.SoCalMlMagMethod");
     ml.configure();

     MagnitudeEngineIF magEng = AbstractMagnitudeEngine.create();
     magEng.setMagMethod(ml);
     Magnitude newMag = (magEng.solveFromWaveforms(sol, (List)mv.getWaveformList())) ?
         magEng.getSummaryMagnitude() : null;
     if (newMag != null) sol.setPreferredMagnitude(newMag);

     // dump result
     System.out.println("Amps calc'ed = "+sol.ampList.size());
     System.out.println("Amps in sol  = "+sol.ampList.getAssociatedWith(sol).size());
     System.out.println("Amps in mag  = "+sol.magnitude.ampList.size());
     System.out.println("Total used   = "+sol.magnitude.getReadingsUsed());

     System.out.println(sol.ampList.toNeatString());
     System.out.println(sol.magnitude.neatDump());

     bm.print("BenchMark: ");
     System.out.println("Avg. time per reading = "+
                        bm.getSeconds()/mv.wfvList.size() + " sec");

// You must commit the sol not just the mag, so that prefmag etc. gets set.

// set false for testing!!
     if (false) {
      try {
       sol.commit();
      } catch ( JasiCommitException ex) {}
     }

     System.exit(0);
    } // end of main

} // end of class
