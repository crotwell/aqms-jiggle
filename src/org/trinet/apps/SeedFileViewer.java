package org.trinet.apps;

import org.trinet.jiggle.*;
import java.awt.*;
import java.io.File;

import org.trinet.jasi.*;
import javax.swing.*;
import java.awt.event.*;
import java.awt.Dimension;
import org.trinet.jasi.seed.*;

import org.trinet.util.WaveClient;

/**
 * SeedFileViewer.java
 * @author Doug Given
 * @version
 */

public class SeedFileViewer {

    static JFrame frame;
    static JPanel graphicsPanel = new JPanel();
    static JSplitPane wfSplit = new JSplitPane();

    static MasterView mv;
    static JiggleFileChooser chooser = new JiggleFileChooser("c:/");
//    static WFScroller wfScroller;    // make scroller
//    static ZoomPanel zpanel;

    //    static boolean debug = true;
    static boolean debug = false;


    static int height = 900;
    static int width  = 640;

    public SeedFileViewer() {

    }

public static void resetGUI(MasterView mv) {

  System.out.println ("Creating GUI...");

  // VirtScroller wfScroller = new VirtScroller(mv, channelsPerPage, true);
     WFScroller wfScroller = new WFScroller(mv, true) ;    // make scroller

  // make an empth Zoomable panel
  ZoomPanel zpanel = new ZoomPanel(mv);

  // appears in bottom of splitPane
  wfSplit.setTopComponent(zpanel);

  wfSplit.setBottomComponent(wfScroller);

  //wfScroller.validate();
  wfSplit.validate();

 /*
  // Retain previously selected WFPanel if there is one,
  // if none default to the first WFPanel in the list
     WFView wfvSel = mv.masterWFViewModel.get();
     // none selected, use the 1st one in the scroller
     if (wfvSel == null && mv.getWFViewCount() > 0) {
      wfvSel = (WFView) mv.wfvList.get(0);
     }

  // Must reset selected WFPanel because PickingPanel and WFScroller are
  // new and must be notified (via listeners) of the selected WFPanel.  It might
  // be null if no data is loaded.
     if (wfvSel != null ) {
       ((ActiveWFPanel)wfScroller.groupPanel.getWFPanel(wfvSel)).setSelected(true);
       mv.masterWFViewModel.set(wfvSel);
       mv.masterWFWindowModel.setFullView(wfvSel);
     }
*/

    }

    public static void main (String args[])  {

      int evid = 0;
      int channelsPerPage = 10;

      mv = new MasterView();
/*
      //String filename = "E:/Data/9634141";
      String filename = "E:/Data/9636285/CI.ALP.HHE";

      if (debug) System.out.println ("Making MasterView for file = "+filename);

      ChannelableList wfList = SeedReader.getDataFromFile(filename, 0);

      // make views
      Waveform wf[] = (Waveform[]) wfList.toArray(new Waveform[0]);
      for (int i = 0; i < wf.length; i++ ){
        mv.addWFView(new WFView(wf[i]));
      }
*/

      JButton clearButton = new JButton ("Clear");
      clearButton.addActionListener( new ActionListener () {
          public void actionPerformed (ActionEvent evt) {
             mv = new MasterView();
             resetGUI(mv);
          }
        }
      );


      JButton fileButton = new JButton ("Choose File");
      fileButton.addActionListener( new ActionListener () {
        public void actionPerformed (ActionEvent evt) {
           int status = chooser.showDialog();
           File file[] = chooser.getSelectedFiles();
           if (file.length > 0  && status == JFileChooser.APPROVE_OPTION) {

              ChannelableList wfList = new ChannelableList();
              SeedReader seedreader = new SeedReader();

              // read the files, create wf's and add to list
              for (int i=0;i<file.length;i++) {
                 System.out.println ("Loading "+file[i].getPath());
                 // returns a Waveform and adds it to the list
                 wfList.addAll( seedreader.getDataFromFile(file[i].getPath(), 0) );
                 System.out.println(seedreader.getLastParsedHeader().toString()); // changed methodname in class -aww  2008/04/03
              }

              // make views
              Waveform wf[] = (Waveform[]) wfList.toArray(new Waveform[0]);
              for (int i = 0; i < wf.length; i++ ) {
                  mv.addWFView(new WFView(wf[i]));
                  System.out.println(wf[i].toString());
              }
              resetGUI(mv);
           } // end of approve
        } // end of actionPerformed
      });  // end of new ActionListener 

      // make a main frame
      //        JFrame frame = new JFrame("SnapShot of "+evid);
      //VirtScroller wfScroller = new VirtScroller(mv, channelsPerPage, true);
      WFScroller wfScroller = new WFScroller(mv, true) ;    // make scroller

      // make an empth Zoomable panel
      ZoomPanel zpanel = new ZoomPanel(mv);

      zpanel.setMinimumSize(new Dimension(300, 100) );
      wfScroller.setMinimumSize(new Dimension(300, 100) );

      // make a split pane with the zpanel and wfScroller
      wfSplit = new JSplitPane(JSplitPane.VERTICAL_SPLIT,
             false,       // don't repaint until resizing is done
             zpanel,      // top component
             wfScroller); // bottom component

      // put divider at 25/75% position
      wfSplit.setOneTouchExpandable(true);
      wfSplit.setDividerLocation(0.25);



      frame = new JFrame("Seed File Viewer");

      frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {System.exit(0);}
      });


      JPanel buttonPanel = new JPanel();
      buttonPanel.add(fileButton);
      buttonPanel.add(clearButton);
      frame.getContentPane().add(buttonPanel, BorderLayout.SOUTH);

      frame.getContentPane().add(wfSplit, BorderLayout.CENTER);

      frame.pack();
      frame.setVisible(true);

      frame.setSize(width, height); // must be done AFTER setVisible

      //debug

      System.out.println ("++++++++++++++++++++++++++++++++++++++++++++++++++++");
      System.out.println ("WFView count        = "+ mv.getWFViewCount());
      System.out.println ("masterWFPanelModel  = "+ mv.masterWFViewModel.countListeners());
      System.out.println ("masterWFWindowModel = "+ mv.masterWFWindowModel.countListeners());
      System.out.println ("++++++++++++++++++++++++++++++++++++++++++++++++++++");

    }
}

