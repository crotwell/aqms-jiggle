package org.trinet.apps;

import java.awt.*;

import org.trinet.jiggle.*;
import org.trinet.jasi.*;
import javax.swing.*;
import java.awt.event.*;
import java.awt.Dimension;

import org.trinet.util.WaveClient;
import org.trinet.util.graphics.*;

/**
 * EventViewer.java
 * *
 * GUI for interactive viewing of waveforms and parameters for a single event.
 * @author Doug Given
 */

public class ModelViewer {

  static boolean debug = true;
  static boolean useWaveserver = true;

  public ModelViewer() { }


  public static void main(String args[]) {
    // event 13307456 is a good test because it has no WF in 1st panel
    // 3113276 is an event from 1993
    // event 13692644 has PASA with Steim2 compression

    int evid = 9924385;
    int channelsPerPage = 10;

    if (args.length <= 0) {
      System.out.println("Usage: EventViewer <evid> [<channels/page>] (default= "+ channelsPerPage+")");
      System.exit(0);
    }

    if (args.length > 0) {
      Integer val = Integer.valueOf(args[0]);
      evid = (int) val.intValue();
    }

    if (args.length > 1) {

      Integer val = Integer.valueOf(args[0]);
      channelsPerPage = (int) val.intValue();

      if (channelsPerPage < 0 || channelsPerPage > 100) {
        System.out.println(" ! channels/page must be between 0 & 100.");
        //debug// System.exit(0);
      }
    }

    // read props file to get waveserver filename
    JiggleProperties props = new JiggleProperties("properties");
    String dbasename = props.getProperty("dbaseName");
    String host = props.getProperty("dbaseHost");

    System.out.println("Making connection to "+host+" "+ dbasename);
    DataSource init = TestDataSource.create(host, dbasename);  // make connection

    if (useWaveserver) {

        try {
          // Make a WaveClient
          WaveServerGroup waveClient = props.getWaveServerGroup();

          if (waveClient == null || waveClient.numberOfServers() <= 0) {
            System.err.println(
                "getDataFromWaveServer Error: no or bad waveservers in properties file. " );
            System.err.println("waveServerGroupList = " + props.getProperty("waveServerGroupList"));
            System.exit(-1);
          }
          AbstractWaveform.setWaveDataSource(waveClient);
        } catch (Exception ex) {
          System.err.println(ex.toString());
          ex.printStackTrace();
       }
    }

    if (debug) System.out.println("Making MasterView for evid = "+evid);

    // Make the "superset" MasterView
    MasterView mv = new MasterView();

    int i = 0;
//        if (debug) System.out.println("got to "+ i++);

    mv.setWaveformLoadMode(MasterView.Cache);
    //mv.setWaveformLoadMode(MasterView.LoadAllInForeground);

    int above = 50;
    int below = 50;
    mv.setCacheSize(above, below);

    //mv.setTimeAlign(true);
//     mv.setAlignmentMode(MasterView.AlignOnTime);
    mv.setAlignmentMode(MasterView.AlignOnTime);

    ////////  mv.defineByDataSource(evid);

    // ////////////////////////////
    // Pre-select trigger channels
    System.out.println("Reading in current channel info...");
    String compList[] = {"EH_", "HH_", "HL_", "AS_"};
    ChannelList chanList = ChannelList.getByComponent(compList);
    // Load the Solution
    Solution sol = Solution.create().getById(evid);
    ChannelTimeWindowModel ctwModel = new JBChannelTimeWindowModel(sol, chanList);
    mv.addSolution(sol);
    mv.setSelectedSolution(sol);
    mv.defineByChannelTimeWindowModel(ctwModel);

    // ///////////////////////////////

    // get the first solution in the list
    // Solution sol = (Solution) mv.solList.solList.get(0);
    if (mv.solList.size() > 0) {
      sol = (Solution) mv.solList.get(0);

      System.out.println("SOL: "+sol.toString());
      System.out.println("There are " + mv.getPhaseCount() +
                          " phases, "+ mv.getWFViewCount() + " time series"+
                          " and "+mv.getAmpCount() + " amps");
    } else {
      System.out.println("No dbase entry found for evid = "+evid);
      System.exit(0);
    }
// Make graphics components

    if (debug) System.out.println("Creating GUI...");

    int height = 900;
    int width  = 640;

    // VirtScroller wfScroller = new VirtScroller(mv, channelsPerPage, true);
    final WFScroller wfScroller = new WFScroller(mv, true) ;     // make scroller

// test this
    //wfScroller.getWFGroupPanel().setShowSegments(true);

    // make an empth Zoomable panel
    ZoomPanel zpanel = new ZoomPanel(mv);

    // enable filtering, default will be Butterworth. See: ZoomPanel
    // Use zpanel.zwfp.setFilter(FilterIF) to change the filter
    zpanel.setFilterEnabled(true);

    // Retain previously selected WFPanel if there is one,
    // if none default to the first WFPanel in the list
    WFView wfvSel = mv.masterWFViewModel.get();
    // none selected, use the 1st one in the scroller
    if (wfvSel == null && mv.getWFViewCount() > 0) {
      wfvSel = (WFView) mv.wfvList.get(0);

      //////
      wfvSel.wf = null;     // crash
      //((WFView) mv.wfvList.get(3)).wf = null;  // no crash
      //////
    }

    // Must reset selected WFPanel because PickingPanel and WFScroller are
    // new and must be notified (via listeners) of the selected WFPanel.  It might
    // be null if no data is loaded.
    if (wfvSel != null ) {
      ((ActiveWFPanel)wfScroller.groupPanel.getWFPanel(wfvSel)).setSelected(true);
      mv.masterWFViewModel.set(wfvSel);
      mv.masterWFWindowModel.setFullView(wfvSel.getWaveform());
    }

    zpanel.setMinimumSize(new Dimension(300, 100) );
    wfScroller.setMinimumSize(new Dimension(300, 100) );

    ///////////    TEST //////////////////
    wfScroller.setSecondsInViewport(60.0);

    // make a split pane with the zpanel and wfScroller
    JSplitPane split =
        new JSplitPane(JSplitPane.VERTICAL_SPLIT,
        false,       // don't repaint until resizing is done
        zpanel,      // top component
        wfScroller); // bottom component

// make a main frame
    //        JFrame frame = new JFrame("SnapShot of "+evid);
    JFrame frame = new JFrame(sol.toSummaryString());

    String fileName = "JiggleLogo.gif";
    IconImage.setDebug(true);
    Image image = IconImage.getImage(fileName);
    frame.setIconImage(image);

    frame.addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent e) {System.exit(0);}
    });
    frame.getContentPane().add(split, BorderLayout.CENTER);     // add splitPane to frame

// Add the channelname finder
    JPanel finderPanel = new JPanel();
    finderPanel.add(new JLabel("Find: "));
    finderPanel.add(new ChannelFinderTextField(mv));
    frame.getContentPane().add(finderPanel, BorderLayout.SOUTH);

//        SelectablePhaseList list = new SelectablePhaseList(mv);
//        frame.getContentPane().add(list, BorderLayout.SOUTH);



    frame.pack();
    frame.setVisible(true);

    frame.setSize(width, height); // must be done AFTER setVisible

    // put divider at 25/75% position
    split.setOneTouchExpandable(true);
    split.setDividerLocation(0.25);

    //debug

    System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++");
    System.out.println("WFView count        = "+ mv.getWFViewCount());
    System.out.println("masterWFPanelModel  = "+ mv.masterWFViewModel.countListeners());
    System.out.println("masterWFWindowModel = "+ mv.masterWFWindowModel.countListeners());
    System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++");

  }

} // EventViewer
