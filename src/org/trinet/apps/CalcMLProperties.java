package org.trinet.apps;

import java.text.*;
import java.util.*;
import java.io.*;
import java.net.InetAddress;

import org.trinet.jasi.*;
import org.trinet.jdbc.datasources.*;
import org.trinet.jiggle.*;
import org.trinet.util.*;

/**
 * @see: PropertyList
 */
public class CalcMLProperties extends JasiPropertyList {

 // Waveform read options
  /** Read waveforms from local files or FTP*/
//    static final int WavesFromLocalFile  = AbstractWaveform.LoadFromDataSource;
  /** Read waveforms from WaveServerGroup */
//    static final int WavesFromWaveServer = AbstractWaveform.LoadFromWaveServer;

  /** */
//    static final int WavesFromFTP        = 2;
  /** Contains current waveform source, local files or wave servers*/
//    static int waveformReadMode = WavesFromLocalFile;

    /* Set waveform cache size */
    static int cacheAbove = 50;
    static int cacheBelow = 50;

    /** Description of the dbase connection. */
    DbaseConnectionDescription dbConn;

  /** Catalog view event selection properties */
//     static EventSelectionProperties eventProps;

    /** True to turn on debug messages. */
    static boolean debug = false;

    static final String DEFAULT_FILENAME = "CalcMLProperties";

/**
 *  Construtor: reads default property file and System properties
 */
    public CalcMLProperties() {
        super(DEFAULT_FILENAME);
        setDefaultProperties();
    }

/**
 *  Construtor: Makes a COPY of a property list. Doesn't read the files.
 */
    public CalcMLProperties(JasiPropertyList props) {
        super(props);
        setFiletype("calcml");
        setFilename(DEFAULT_FILENAME);
        setDefaultProperties();
    }

/**
 *  Construtor: Makes a COPY of a property list. Doesn't read the files.
 */
    public CalcMLProperties(String file) {
        super(file);
        setFiletype("calcml");
        setFilename(file);
        setDefaultProperties();
    }

    /** Set true to reuse old amps rather then rescan the time-series. */
    public void setReuseOldAmps(boolean tf) {
        setProperty("reuseOldAmps", ""+tf);
    }
    public boolean getReuseOldAmps() {
        return this.getBoolean("reuseOldAmps") ;
    }

    /** Set true to use low gain channels. */
    public void setUseLowGains(boolean tf) {
        setProperty("useLowGains", ""+tf);
    }

    /**Returns true if low gain channels will contribute to the magnitude. */
    public boolean getUseLowGains() {
        return getBoolean("useLowGains");
    }

    /**
    * If 'true' the waveform window scanned begins 1 sec before expected
    * P-wave onset and ends at P-wave onset plus 2x the S-P time.
    * If 'false' the whole available waveform is scanned.
    */
    public void setScanEnergyWindow(boolean tf) {
        setProperty("scanEnergyWindow", ""+tf);
    }

    public boolean getScanEnergyWindow() {
        return getBoolean("scanEnergyWindow");
    }

    /** Don't calculate amps greater then this far away (in km).*/
    public void setMaxDistance(double distKm ) {
        setProperty("maxDistance", distKm);
    }

    /** Return the maximum distance value. Returns  Double.MAX_VALUE if it
    * was not explicitly set, so it is safe to use.*/
    public double getMaxDistance() {
        return this.getDouble("maxDistance");
    }

    /** Don't calculate amps greater then this far away (in km).*/
    public void setMaxChannels(int maxChannels ) {
        setProperty("maxChannels", maxChannels);
    }

    /** Return the maximum distance value. Returns  Int.MAX_VALUE if it
    * was not explicitly set, so it is safe to use.*/
    public int getMaxChannels() {
        return this.getInt("maxChannels");
    }

    /** Set the minimum SNR value for which amplitudes will contribute to the
    * magnitude.*/
    public void setMinSNR(double value ) {
        setProperty("minSNR", value);
    }

    /** Return the minimum SNR value for which    amplitudes will contribute to the
    * magnitude. Returns Double.NaN if not set.
    * Returns Double.MIN_VALUE if not set.*/
    public double getMinSNR() {
        return this.getDouble("minSNR");
    }

    /** If value is non-zero trim amps with residuals greater then this value and
    * recalc the median.*/
    public void setTrimResidual(double value ) {
        setProperty("trimResidual", value);
    }
    /** Return the value of the trimResidual. Returns Double.MAX_VALUE if not set. */
    public double getTrimResidual() {
        return this.getDouble("trimResidual");
    }

    /** Set the value of requireCorrections. */
    public void setRequireCorrection(boolean tf ) {
        setProperty("requireCorrection", ""+tf);
    }
    /** Return the value of requireCorrections. */
    public boolean  setRequireCorrection() {
        return getBoolean("requireCorrection");
    }

/**
 * Set values for certain essential properties so the program will work in the absence of
 * a default 'properties' file.
 */
    public void setDefaultProperties() {
        EnvironmentInfo.setApplicationName("??");
        EnvironmentInfo.setNetworkCode("??");
        setReuseOldAmps(false) ;
        setUseLowGains(false) ;
        setScanEnergyWindow(true) ;
        setMaxDistance(600.0) ;
        setMaxChannels(999999 );
        setMinSNR(8.0) ;
        setTrimResidual( 1.0) ;
        setRequireCorrection( false) ;
    }

    /** Return the fully specified name of the JDBC URL for the dbase. It is composed
     * three properties: dbaseHost, dbasePort and dbaseName. It has a form like:
     * jdbc:oracle:thin:@serverq.gps.caltech.edu:1521:databaseq*/
    public String getDbaseURL() {
        return dbConn.getURL();
    }

    /** Return the DbaseConnectionDescription object described by this property list. */
    public DbaseConnectionDescription getDbaseDescription() {
        // must remake it because we don't know what has changed lately
        dbConn =  new DbaseConnectionDescription(getProperty("dbaseHost"),
               getProperty("dbaseDomain"),
               getProperty("dbaseName"),
               getProperty("dbasePort"),
               getProperty("dbaseDriver"),
               getProperty("dbaseUser"),
               getProperty("dbasePasswd") );
        return dbConn;
    }

/**
* Save all the environment's properties.
*/
    public void save() {
        // WRITE THEM OUT TO A FILE
        super.saveProperties("--- CalcML Properties --- [" + getUserPropertiesFileName()+"] ");      // save user properties
    }

//
    public static void main(String args[]) {
        // System properties
        System.out.println("-- SYSTEM PROPERTIES --");

        System.getProperties().list(System.out);

        // Private properties
        CalcMLProperties test = new CalcMLProperties();

        System.out.println("-- CalcML PROPERTIES --");

        //test.dumpProperties();
        System.out.println(test.listToString());

        // examples of local 'get' methods
        System.out.println( "......... Test some stuff ...............");
        System.out.println("\n minSNR = " + test.getDouble("minSNR") );

        try {
            System.out.println("IP address: "+ InetAddress.getLocalHost().getHostName());
        } catch (Exception ex) {
            System.out.println("Bad IP address.");
        }

        System.out.println(test.getDbaseDescription().toString());
        System.out.println("user.home= "+System.getProperty("user.home", "."));
        System.out.println("user.timezone= "+System.getProperty("user.timezone", "."));
        System.out.println("user.name= "+System.getProperty("user.name", "."));

        // write out to file
        test.save();
    }
//
} // end of class
