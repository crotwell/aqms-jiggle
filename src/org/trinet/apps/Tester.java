package org.trinet.apps;

import javax.swing.UIManager;
import java.awt.*;
import java.util.*;
import java.io.*;

import org.trinet.jasi.*;
import org.trinet.util.*;
import org.trinet.formats.*;
/**
 * @author Doug Given
 */
public class Tester {
  private boolean packFrame = false;

  //Construct the application
  public Tester() {

    TesterFrame frame = new TesterFrame();
    //Validate frames that have preset sizes
    //Pack frames that have useful preferred size info, e.g. from their layout
    if (packFrame) {
      frame.pack();
    }
    else {
      frame.validate();
    }
    //Center the window
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    Dimension frameSize = frame.getSize();
    if (frameSize.height > screenSize.height) {
      frameSize.height = screenSize.height;
    }
    if (frameSize.width > screenSize.width) {
      frameSize.width = screenSize.width;
    }
    frame.setLocation((screenSize.width - frameSize.width) / 2, (screenSize.height - frameSize.height) / 2);
    frame.setVisible(true);
  }
  //Main method
  public static void main(String[] args) {

	  Gmp2Db gmp2db = new Gmp2Db();
	  int goodknt = 0;
	  
	  ArrayList files = new ArrayList();
	  files.add(new File("C:\\temp\\GMP\\0630660.smMP"));
	  files.add(new File("C:\\temp\\GMP\\0630661.smMP"));
	  files.add(new File("C:\\temp\\GMP\\0630662.smMP"));	 
	  
	  //File files[] = (File[]) newList.toArray(new File[0]);
	     
      for (int i = 0; i< files.size(); i++) {    // for each file
          if (gmp2db.processOneFile((File) files.get(i))) {
            goodknt++;
          }
        }
	  
  }
}
