package org.trinet.apps;

import org.trinet.jasi.*;
//import org.trinet.util.*;
import org.trinet.waveserver.rt.WaveClient;

/**
 * GUI to allow viewing of current data for a set of channels. <p>
 *
 * Usage: Scope <seconds> "<nt.sta.chan nt.sta.chan ...>"
 */

public class WavepoolList {

    final int maxRetries = 3;
    final int maxTimeoutMills = 30000;
    final boolean verify = true;
    final boolean truncate = false;

    WaveClient waveClient  =  new WaveClient(maxRetries, maxTimeoutMills, verify, truncate);

    public ChannelableList getWavepoolList(String url, int port) {
        addWaveClient(url, port);
        return waveClient.getJasiChannels();
    }

    public WaveClient addWaveClient(String url, int port) {
        waveClient.addServer(url, port);

        if (waveClient == null || waveClient.numberOfServers() <= 0) {
          System.err.println("getDataFromWaveServer Error: no or bad waveservers in properties file. ");
          System.err.println("waveServer = " + url +" : "+port);
          return null;
        }
        return waveClient;
    }


    public static void main(String args[]) {

        if (args.length < 2) {
          System.out.println("Usage: WavepoolList <host.ip.address> <port#>");
          System.exit(0);
        }

        String host  = "serverpa.gps.caltech.edu";
        int port  = 6501;

        System.err.println("host = "+host+ "  port = "+ port);
        WavepoolList wpl = new WavepoolList();

        ChannelableList chanList = wpl.getWavepoolList(host, port);

        String str="";

        Channel[] ch = ((ChannelList)chanList).getArray();

        if (ch.length == 0) {
          str = ("No channels returned.");
        } else {
          for (int i = 0; i < ch.length; i++) {
            str += ch[i].toDelimitedSeedNameString() + "\n";
          }
        }
        System.out.println(str);
    }
}

