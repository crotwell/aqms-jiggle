package org.trinet.util;

//import javax.swing.UIManager;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
//import java.io.*;
import java.lang.reflect.*;

/**
 * Schedule some job to run in a separate thread at regular intervals.
 * Only one active job is allowed at a time.
 * Note the time interval set here is the time that will elapse between the
 * END of the previous run and the START of the next. It is NOT the interval between
 * starts.
 * You can force a job to run outside the schedule with runNow() or runASAP().
 */
public class ThreadScheduler extends javax.swing.Timer implements ActionListener {

  // removed "static" declarations for field: - aww 2011/02/22
  private ThreadWrapper thread = null;
  private Runnable target = null;
  private int standardInterval = 60 * 30;  // 30 minutes

  private boolean verbose = false;

  private boolean isDaemon = false;  // set true if you want sudden death when main (java machine exits), else thread is user thread, keeps going

  public ThreadScheduler (Runnable runnable, int intervalSecs, boolean isDaemon) {
    // millisecs
    super(intervalSecs*1000, null);  // can't pass 'this' to super :. must set ActionListener later
    setTarget(runnable);             // thing to run at end of interval
    setStandardInterval(intervalSecs);
    setRepeats(false);               // don't auto-repeat at interval forever
    setCoalesce(true);               // lump multiple actionEvents
    //setInitialDelay(getDelay());   // delay before 1st run is same as regular delay (this is the default)
    addActionListener(this);         // add self as a listener
    this.isDaemon = isDaemon;        // if true, not a user thread, continues on main exit 
    start();                         // a real self-starter, clock is ticking...
  }

  protected void setTarget(Runnable target) {
    this.target = target;
  }

  protected Runnable getTarget() {
    return target;
  }

  /** ***********************************************************
   *   This runs the target job in a thread every timer interval.
   * NOTE: the Timer class runs this in the EventDispatchThread.
   */

  public void kill() {
      try {
        if (thread != null) thread.interrupt();
      }
      catch (Exception ex) {
      }
      thread = null;
  }

  public void actionPerformed(ActionEvent evt) {

//    dump("Thread starting.");
//    dump(evt.toString());
//    dump(evt.getActionCommand());

    // create thread 1st, but does not run it
    if (thread == null) {
        thread = new ThreadWrapper(getTarget());
        thread.setDaemon(isDaemon);
    }

    // disallow multiple thread instances
    if (thread.isAlive()) {
      dump("Start request ignored: Thread already running. "+getTarget().getClass().getName());
    } else {
      thread = new ThreadWrapper(getTarget());
      thread.setDaemon(isDaemon);
      thread.start();  // executes the thread's run() method
      dump("Thread started... "+getTarget().getClass().getName());
    }
  }
  /**
   * Set the Job run interval in seconds.
   * */
  public void setStandardInterval (int secs) {
    standardInterval = secs;
    setInitialDelay(standardInterval * 1000);
  }

  public int getStandardInterval() {
    return standardInterval;
  }

  /**
   * Try to run the scheduled job now.
   * If it the job is currently running this call has no effect and returns false.
   * It will not schedule the job to run when the current instance completes. To
   * do that use runASAP().
   */
  public boolean runNow() {
    if (thread == null || !thread.isAlive()) {
      actionPerformed(new ActionEvent(this, 1, "Forced run"));
      return true;
    } else {
      return false;
    }
  }
  /**
   * Run the scheduled job now.
   */
  public void runASAP() {
    if (runNow()) {
      // noop
    } else {
      if (thread != null) thread.setRedoImmediately(true);
    }
  }

  public void setVerbose (boolean tf) {
    verbose = tf;
  }
  public boolean getVerbose() {
    return verbose;
  }
  /** Debug output */
  public void dump(String str) {
    if (getVerbose()) System.out.println ("ThreadScheduler: "+str);
  }

// ////////////////////////////////////////////////////////////////////////////////
/** This wrapper allows use to run the ThreadScheduler.threadFinished() method
 * when the thread completes. */
private class ThreadWrapper extends Thread {

  private Runnable target;
  private boolean redoImmediately = false;

  ThreadWrapper (Runnable runnable) {

    target = runnable;
  }

  public void run() {
    if (target != null) {
      // make sure this always happens in the same thread, otherwise you might
      // run more than one instance
      try {
        //dump(" ++ Starting thread: " + EpochTime.dateToString(new Date()));
        //EventQueue.invokeAndWait(target);   // executes in EventQueue thread
        target.run();   // execute in this instance thread -aww test alternative 2011/01/03 
      //} catch (InvocationTargetException ex) {
      //} catch (InterruptedException ex) {
      } catch (Exception ex) {
        ex.printStackTrace();
      } finally {
        threadFinished();
      }
    }
  }

  public void setRedoImmediately(boolean tf) {
    redoImmediately = tf;
  }
  public boolean getRedoImmediately() {
    return redoImmediately ;
  }
  //*************************************************************

  /**
   * Do anything that needs to be done upon thread completion.
   * This is where the delay to the next run is set.
   */
  public void threadFinished() {

    int secs = getStandardInterval();  // normally repeat at this interval

    if (getRedoImmediately()) {
      secs = 2;
      //actionPerformed(new ActionEvent(this, 1, "redoImmediately run"));
      redoImmediately = false;  // reset flag
    }

    dump("Thread finished will restart in "+secs+" secs.");

    setInitialDelay(secs * 1000);
    restart();
  }
}

/*
public static void main(String[] args) {

  //new Tester();

  ThreadScheduler sched = new ThreadScheduler(new SimpleTarget(), 5);

// hang while timer has time to do its thing
  try {
    System.out.println("--- runNow() "+ sched.runNow());

    Thread.sleep(15 * 1000);

    System.out.println("--- runNow() "+ sched.runNow());
    System.out.println("--- runNow() "+ sched.runNow());
    System.out.println("--- runNow() "+ sched.runNow());

    System.out.println("--- runASAP() ");

    Thread.sleep(32 * 1000);
     sched.runASAP();
    System.out.println("-32 runASAP() ");

    Thread.sleep(32 * 1000);
     sched.runASAP();
    System.out.println("-32 runASAP() ");

    Thread.sleep(18 * 1000);
    System.out.println("-18 runNow() "+ sched.runNow());

    Thread.sleep(10 * 1000);
    System.out.println("-10 runNow() "+ sched.runNow());

    Thread.sleep(50000 * 1000);
  }
  catch (InterruptedException ex) {
  }
}
// /////////////////////////////////////////
class SimpleTarget implements Runnable {
    SimpleTarget () { }

    public void run () {
      int rnd = (int) (Math.random()*10); // unique thread label
      System.out.println ("SimpleTarget starting...["+rnd+"] "+ Thread.currentThread().toString());
      try {
        for (int i = 1; i< 16; i++) {
          System.out.println(rnd+"> "+i);
          Thread.sleep(1 * 1000);
        }
      }
      catch (InterruptedException ex) {
      }
      System.out.println ("SimpleTarget done.");
    }
}
*/

}
