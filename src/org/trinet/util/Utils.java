package org.trinet.util;

/**
 * Class Utils contains various static utility methods.
 */
public class Utils {
  /**
   * Close the resource quietly ignoring any errors.
   * 
   * @param resource the resource or null if none.
   */
  public static void closeQuietly(AutoCloseable resource) {
    try {
      if (resource != null) {
        resource.close();
      }
    } catch (Exception ex) {
    }
  }

  /**
   * Close the resources quietly ignoring any errors.
   * 
   * @param resources the resources or null if none.
   */
  public static void closeQuietly(AutoCloseable... resources) {
    if (resources != null) {
      for (AutoCloseable resource : resources) {
        closeQuietly(resource);
      }
    }
  }
}
