package org.trinet.util.locationengines;
import java.beans.*;
import org.trinet.jasi.*;
import org.trinet.jasi.engines.*;
import org.trinet.util.*;
import org.trinet.util.gazetteer.*;

public interface LocationEngineIF extends SolutionDataEngineIF {

    public static final int DEFAULT_MIN_SOLUTION_PHASES  = 3;

    public void setLocatorName(String str);
    public String getLocatorName();

    public void setUseTrialLocation(boolean tf) ;
    public boolean getUseTrialLocation() ;

    public void setUseTrialOriginTime(boolean tf) ;
    public boolean getUseTrialOriginTime() ;

    public boolean locate(Solution sol);
    public boolean solve(Solution sol, PhaseList phaseList);
    public boolean solve(PhaseList phaseList);

    public int getMinPhaseCount();
    public void setTrialLocation(LatLonZ latLonZ);
    public void setTrialLocation(double lat, double lon, double z);
    public LatLonZ getTrialLocation();
    // could add more methods for setting fixed depth etc.

    public String getResultsHeaderString(); // hypo summary lat,lon,etc.
    public String getResultsPhaseString();  // phase summary time,motion,weights etc.
}
