package org.trinet.util.locationengines;
import org.trinet.util.*;

public interface LocationEngineServiceIF extends LocationEngineIF, LocationServiceDescIF, GenericServiceIF {}
