package org.trinet.util.locationengines;
/**
 * Calculate a location using the remote location server.
 */
import java.beans.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.lang.*;
import java.net.*;
import javax.swing.*;       // JFC "swing" library

import org.trinet.hypoinv.*;
import org.trinet.jasi.*;
import org.trinet.jdbc.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.jdbc.datasources.*;
//import org.trinet.jiggle.JiggleProperties; // for test?
//import org.trinet.jiggle.MasterView; // for test?
import org.trinet.util.*;
import org.trinet.util.gazetteer.LatLonZ;

public class LocationEngineHypoInverse extends AbstractLocationEngineService {

    public static final int DEFAULT_LINE_TERMINATOR = (int) '\n';

    //Don't have Swing graphics component here, better if interested component
    //was a listener for output text updates? -aww

    //public static final String datumHorizontal = "NAD27"; // is this right -aww
    public static final String datumHorizontal = "WGS84"; // by GPS of stations
    public static final String datumVertical   = "AVERAGE";

/** The socket thru which we communicate with the remote location server. */
    private Socket socket;
    private InputStream  streamIn;
    private OutputStream streamOut;

    private ArcSummary arcsum = new ArcSummary(); // aww added new class for formatted raw ARC formats
    private ArcStation arcstn = new ArcStation(); // aww added new class for formatted raw ARC formats

    private String resultPhaseStr;
    private String resultHeaderStr;
    private int phasesSent = 0;

    { serverTimeoutMillis = 45000; } // in super set arbitrary value

    /** Uses the HYPOINVERSE 2000 service */
    public LocationEngineHypoInverse() {
      statusString = "No solution.\n";
      resultsString = statusString;
    }

    /**
     * ServerIPAddress has the form: "XXX.XXX.XX.154" or "splat.gps.caltech.edu"
     */
    public LocationEngineHypoInverse(String serverIPAddress, int serverPort) {
      this();
      setServer(serverIPAddress, serverPort);
    }

    /**
     * Connect to the solution server. If connection exists this is a noop.
     * To do a hard reconnect you must call disconnect() first.
     * @see #disconnect()
     */
    public boolean connect() {
      // NOTE in jdk1.4 see methods:
      // socket.isClosed() socket.isConnected() socket.isInputShutdown() socket.isOutputShutdown() 
      engineIsValid = true;

      if (socket == null || socket.isClosed() || socket.getInetAddress() == null ) {
        if (debug || verbose)
         System.out.println("INFO: LocationEngine attempting connection to " + getServiceName() +
                 " addr: " + serverAddress + "  port "+port + " timeout: " + serverTimeoutMillis);
        try {
          if (socket != null) disconnect(false); // probably not needed here
          socket = new Socket (serverAddress, port); //stream (i.e. TCP) socket
          socket.setSoTimeout(serverTimeoutMillis);
          streamIn  = socket.getInputStream();
          streamOut = socket.getOutputStream();
        } catch (Exception ex) {
          System.err.println("ERROR: LocationEngine service connection exception.");
          System.err.println(ex.getMessage());
          engineIsValid = false;
        }
      }
      return engineIsValid;
    }

    /** Break the current service connection. */
    public boolean disconnect() {
        return disconnect(true);
    }

    private boolean disconnect(boolean resetEOT) {
      // NOTE in jdk1.4 see methods:
      // socket.isClosed() socket.isConnected() socket.isInputShutdown() socket.isOutputShutdown() 
      engineIsValid = false;

      if (resetEOT) resetEOT(); // try notifying solserver to tidy up by writing EOT 

      boolean status = true;
      try {
        if (socket != null && !socket.isClosed()) {
            System.err.println("INFO: LocationEngine service closing socket");
            socket.close();
        }
      } catch (Exception ex) {
        ex.printStackTrace();
        status = false;
      }
      finally {
        socket = null;  // force GC
        streamIn = null; // added -aww 2010/05/24 
        streamOut = null; // added -aww 2010/05/24 
      }
      return status;

    }

    private void resetEOT() {
        try {
          if (socket != null && !socket.isClosed()) { // added test for closed socket -aww 2010/05/24
              System.out.println("INFO: LocationEngine sending EOT to solserver now ...");
              writeMessage("|EOT|"); // signal solserver to tidy up, close open socket
          }
        } catch (Exception ex) {
            System.err.println("ERROR: LocationEngine resetEOT: " + ex.getMessage());
        }
    }

    public void reset() {
        // reset instance result data
        resultHeaderStr = "";
        resultPhaseStr  = "";
        //System.out.println("INFO: (solvedSolution==null):"+(solvedSolution==null)+" (currentSol==null):"+(currentSol==null)+
        //        " (solvedSolution==currentSol):" + (solvedSolution==currentSol));
        if (solvedSolution != null && solvedSolution != currentSol) {
            System.out.println("INFO: LocationEngine reset, disconnecting solserver now ...");
            disconnect(); // insurance for client side
        }
        // do this last since below nulls the solvedSolution checked above
        super.reset();
    }

    // Added for cleanup just in case dangling connection -aww 2010/05/24
    protected void finalize() throws Throwable {
      disconnect();
      super.finalize();
    }

    /** Return Raw(HypoInverse) style output for this location run. It will be multi-string
     * output with "\n" line separators. */
    public String getResultsString() {
        return super.getResultsString();
    }

    public String getResultsHeaderString() { return resultHeaderStr;}
    public String getResultsPhaseString() { return resultPhaseStr;}

    // artifact of SolutionDataEngineIF:
    public boolean solveFromWaveforms(Solution sol, List wfList) {
        // Could rescan Solution waveforms for new phase arrival times
        // and update the PhaseList with results before locating.
        setSolution(sol); // assumes method does reset()
        solveSuccess = false; // insurance here, reset() does this too.
        return solveSuccess;
    }

    /**
     * Calculate a solution given an input PhaseList (arrivals).
     * Returns true on success and false if PhaseList is empty.
     * NOTE: public access needed by IF implementation.
     * End-users should not invoke this method directly unless
     * they first invoked setSolution(phaseAssocSol);
     */
    public boolean solve(PhaseList phaseList) {

      // if old result, force reset which closes connection,
      // only solution per connection session to allow the use
      // of the getXXX file data methods
      //reset();  // reset is done by setSolution

      // NOTE: input phaseList (may have readings not associated with currentSol)
      // we don't force assignment of all readings to current sol here...
      // sendPhaseList() method extracts only those associated with currentSol
      // using phaseList.getAssociatedWith(currentSol)
      //

      solveSuccess = false; // insurance, setSolution does a reset too

      currentPhaseList = phaseList;
      if (currentPhaseList.isEmpty()) {
        statusString = "LocationEngine: cannot locate; no phases in phase list.\n";
        resultsString = statusString;
        System.err.println(statusString);
        return false;
      }

// Set LAT, LON, Z Channel_Data in input PhaseList for b4 distanceSort of phaseList.
// Otherwise must be done externally before invoking solution solve. See next comment.

// NOTE: Removing channelLookup invocation below results in Mung table distances being 
// unknown after interactive insert of NEW readings in multiple GUI classes. So either 
// do the Channel matching in those classes or force a check here to copy lat,lon,z data 
// from the ChannelList ref to the matching channel of input phaseList. - 06/27/2006 aww
      if (currentSol.hasValidDateTime() && ! currentSol.isDummy()) { // not null or NaN
        channelLookup(currentPhaseList, currentSol.getDateTime());
      }
      else {
        // get earliest phase date from db
        TimeSpan ts = currentPhaseList.getMinMaxTimeSpan();
        //looks up channels in db for those where latLonZ is not known
        channelLookup(currentPhaseList, new DateTime(ts.getStart(), true)); // for UTC seconds time -aww 2008/02/11
      }

      boolean useTrialTime = getUseTrialOriginTime() || currentSol.timeFixed.booleanValue() || currentSol.useTrialTime(); // added timeFixed to force option 2011/05/12 -aww
      if (!currentSol.hasValidDateTime() && useTrialTime) { // if invalid and use trial time
        statusString = "WARNING: LocationEngine TRIAL origin time is INVALID, attempting location without using trial time.\n";
        useTrialTime = false;
        resultsString = statusString;
        System.err.println(statusString);
      }

      // don't do distances here, stale? must determined by new hypoinv origin using returned arc phases

      if (debug || verbose) {
        StringBuffer sb = new StringBuffer(132);
        sb.append("INFO: LocationEngine attempting to locate ").append(currentSol.getId().longValue());
        sb.append(" with ").append(currentPhaseList.getReadingCount(false,true)).append(" phases.");
        statusString = sb.toString();
        System.out.println(statusString);
      }

      // connect to the server
      if (! connectWithStatus() ) return false;

      // set a line telling the solution server what to call the remote file (this was
      // done in case we want to keep and examine the remote files)
      // Input and output are Hypoinverse .ARC file format
      // |FILE| <evid> - begin ARC input format send data for this event
      // |LOC|  - new style end ARC input send data for this event
      // |EOF|  - old style end of ARC input data, calculate the solution and return results
      try { // added try-catch exception logic to disconnect server upon exception -aww 2010/02/25
        boolean status = writeMessage("|FILE| " + currentSol.id.longValue());
        if (!status) return false;

        // write HYPO format phase data to the solserver socket
        status &= sendPhaseList(currentPhaseList);
        if (! status)  return false;

        // send a terminator line (Hypoinverse needs this)
        // Set appropriate fixed flags and if the engine's
        // trial location is currently set not null and differs
        // from that of the currentSol reset currentSol's location:
        if ( getUseTrialLocation() || currentSol.useTrialLocation() ) setTrialLocationOf(currentSol); // aww 2014/03/26 overrides with engine trial loc?
        // Test to see if anything is fixed, if so, we need to use pass info in terminator 
        // added locationFixed condition to force -aww 2011/05/12
        boolean fixZok = !(props.getBoolean("unfixEqDepth") &&
                 EventTypeMap3.getEventTypeByJasiType(currentSol.eventType.toString()).group.equals("earthquake")); 
        //System.out.println("DEBUG fixZok: " + fixZok + " " + !props.getBoolean("unfixEqDepth")); 
        status &= writeMessage( HypoFormat.toTerminatorString(
                       currentSol,
                       currentSol.locationFixed.booleanValue(),
                       currentSol.depthFixed.booleanValue() && fixZok, // do not fix if unfix prop is set true and it's an eq -aww 2015/04/01
                       currentSol.timeFixed.booleanValue(),
                       ( getUseTrialLocation() || currentSol.locationFixed.booleanValue() || currentSol.useTrialLocation() ),
                       useTrialTime // if false, uses earliest channel picked
                    )
                  );
        if (!status) return false;

        //status &= writeMessage("|EOF|"); // old style signal solserver that its the end of data
        status &= writeMessage("|LOC|"); // new style signal solserver that its the end of data
        if (!status) return false;

        // <SOLUTION> read the returned results from the server
        if (debug) System.out.println("DEBUG: ** LocationEngine readingResults() ** ");
        // not always reliable, sometimes a bogus parse of ascii
        solveSuccess = (status && readLocationResults());
        if (solveSuccess) {
          solvedSolution = currentSol; // save the currentSol as result -aww

          if ( props.getBoolean("locationEnginePRT") ) {
            resultsString = getPrtFileContents();
          }

        }
      }
      catch (Exception ex) {
          ex.printStackTrace();
          disconnect(false); // try to reset solserver
      }

      // if (debug) System.out.println("DEBUG:  ** LocationEngine disconnect() **");
      // Don't signal solserver you are done, may want to invoke getXXX file data method
      // disconnect(); // release the server

      return solveSuccess;
    }

    public boolean connectWithStatus() { 
      if ( ! connect() )         {
        statusString = "INFO: LocationEngine FAILED to connect to "+serverAddress+" port "+port + "\n";
        resultsString = statusString;
        System.err.println(statusString);
        return false;
      } else if (debug || verbose) {
        statusString = "INFO: LocationEngine connected to "+serverAddress+" port "+port + "\n";
      }
      return true;

    }

    /**
     * Format and send a phase list to the solution server.
     */
    private boolean sendPhaseList(PhaseList phaseList) {
      boolean status = true;
      int count = phaseList.size();
      if (debug) System.out.println("DEBUG: LocationEngine sendPhaseList oldPhases:\n"+ phaseList.toNeatString());
      // for reset of deleted phase attributes (those not from this current solve for solution)
      //Phase blankPhase = (count > 0) ? Phase.create() : null; // removed 2008/01/10 -aww
      phasesSent=0;
      for (int i = 0; i < count; i++) {
        Phase aPhase = (Phase) phaseList.get(i);
        aPhase.initSolDependentData(); // reset its AssocArO data (except in_wgt) added 2008/01/10 -aww  
        if (aPhase.isDeleted()) {
          // removed below code block, just do init above 2008/01/10 -aww
          /*
          // reset its AssocArO data to "null"
          Channel chan = (Channel) aPhase.getChannelObj();
          chan.setDistanceBearingNull();
          // don't need to do chan.clone() above, if setChannelObj below justs sets values; - aww
          blankPhase.setChannelObj(chan); // copy channel data into "virgin" phase
          aPhase.copySolutionDependentData(blankPhase); // now reset rest of origin assoc data
          */
          if (debug) System.out.println("DEBUG: LocationEngine sendPhaseList deleted:"+ aPhase.toNeatString());
          continue;    // skip deleted phases after reset, don't propagate to server
        }
        if (aPhase.getAssociatedSolution() == currentSol) {
          String str = HypoFormat.toArchiveString(aPhase);
          status &= writeMessage(str); // write phase data to the solserver socket
          if (!status) {
             //System.out.println("ERROR sendPhaseList writeMessage failed");
             break; // failure bail
          }
          phasesSent++; // save count sent to check againste count returned from server
        }
      }
      return status;
    }

    /**
     * Send String request over the service socket. 
     */
    public boolean setServiceResponseFor(String inputCommandStr) {
      // Commands sent by the client over the socket are tags delimited by the pipe character. 
      resultsString = inputCommandStr + "\n";

      boolean status = connectWithStatus(); // only connects if existing socket is closed.
      if (!status) return false;

      status = writeMessage(inputCommandStr); // send tag to server
      resultsString += statusString;

      return status;
    }

    /**
     * Send String request over, and return reply String from, the service socket. 
     */
    private String getServiceResponseFor(String inputCommandStr, boolean multiline) {
        if (debug) System.out.println("DEBUG Location Engine getServiceResponseFor input: " + inputCommandStr);
        if ( ! setServiceResponseFor(inputCommandStr) ) return resultsString + " FAILED!";
        String msg = null;
        List aList = new ArrayList(1024);
        int length = 0;
        do { 
            msg = getResponse(DEFAULT_LINE_TERMINATOR);
            if (msg == null || msg.startsWith("|EOT|") || msg.endsWith("|EOT|") || msg.startsWith("|EOF|") || msg.endsWith("|EOF|") ) {
                break; // What about using ascii char EOT int value 4 ?
            }
            if (msg == "") continue;
            aList.add(msg);
            length += msg.length();
        } while (multiline); // loop read until end of data
        //disconnect(); // no, keep connection open until solution changed, to allow ARCIN ARCOUT PRT file data return from server

        StringBuffer sb = new StringBuffer(length+aList.size()+1);
        for (int idx = 0; idx < aList.size(); idx++) {
            sb.append((String)aList.get(idx));
            sb.append("\n");
        }
        resultsString = sb.toString();
        return resultsString;
    }

    private String getResponse(int terminator) {
      String response = null;
      boolean ioExcepted = false; // exception error flag
      try {
        response = readMessage(terminator);
      }
      catch (InterruptedIOException ex) {
        statusString = "LocationEngine: service timed out after "+ serverTimeoutMillis/1000+" seconds.\n";
        resultsString += statusString;
        ioExcepted = true;
      }
      catch (IOException ex) {
        statusString = "LocationEngine: service socket read error.\n";
        resultsString += statusString + ex.getMessage() ;
        ioExcepted = true;
      }

      if (response == null || response.equals("?") ) {
        statusString = "LocationEngine: service did not return data.\n";
        resultsString += statusString;
        System.out.println(resultsString);
        System.out.println("ERROR: LocationEngine no data returned. Check input.");
      }

      // if IOException, reset socket
      if (ioExcepted) {
          disconnect(); // added disconnect - 2015/06/03 aww
      }

      return response;
    }

/*
SolServer Protocol
Commands sent by the client over the socket are tags delimited by the pipe character and terminated by \n. 

Invalid strings are acknowledged with ? followed by \n. 

Getting information about the Hypoinverse configuration: 
FYI: ascii char EOT = 4 (also STX=2 and ETX=3)
|GETINSTFILE| - return the contents of the Hypoinverse instruction file, terminated by |EOT|
|GETSTANAME| - return the name of the Hypoinverse station file
|GETSTAFILE| - return the contents of the Hypoinverse station file, terminated by |EOT|
|GETCRHNAME| - return the name of the Hypoinverse crustal model file
|GETCRHFILE| - return the contents of the Hypoinverse crustal model file, terminated by |EOT|

Computing an event location: 
Old style finishes by closing socket connection (for backward compatibility)
|FILE| <evid> = Beginning of the .arc format input. Each line terminated by \n.
|EOF| = End-Of-File: locate the event, return result in ARC format and close the socket.

New style finishes by sending |EOT|
|LOC| <evid> = Beginning of the .arc format input. Each line terminated by \n.
|EOF| = locate the event and send |EOT| when done. Socket remains open. User must ask for results (see below).

 After new style location, ask for result 
 |PRT| = return the PRT formatted results (from last event this session), terminated by |EOT|
 |ARCOUT| = return the ARC format results (from last event this session), terminated by |EOT|
 |ARCIN| = return the ARC format input file , terminated by |EOT|

 |EOT| = end of transmission, disconnect the new style socket connection
*/
    public String getArcInFileContents() {
        return getServiceResponseFor("|ARCIN|", true); // return text contents of Hypoinverse .ARCIN file
    }

    public String getArcOutFileContents() {
        return getServiceResponseFor("|ARCOUT|", true); // return text contents of Hypoinverse .ARCOUT file
    }

    public String getPrtFileContents() {
        return getServiceResponseFor("|PRT|", true); // return text contents of Hypoinverse .PRT file
    }

    // |GetInstFileList| - return list of available instruction files, returns one filename per line terminated by |EOT|
    public String getInstFileList() {
        return getServiceResponseFor("|GetInstFileList|", true);
    }

    // |GetInstName| - returns the name of the current instruction file
    public String getInstName() {
        //String msg = getServiceResponseFor("|GetInstName|", false); // no, this leaves appends |EOT| line in buffer
        String msg = getServiceResponseFor("|GetInstName|", true); // return text contents of hypinst. instruction file
        if (msg != null) {
            int idx = msg.lastIndexOf("\n");
            if (idx > 0) msg = msg.substring(0,idx); 
        }
        return msg;
    }

    // |GetInstFile| <filename> - return contents of specified instruction file
    public String getInstFileContents(String filename) {
        return getServiceResponseFor("|GetInstFile| " + filename, true); // return text contents of specified instruction file
    }

    // |GetInstFile| - return contents of currently set instruction file
    public String getInstFileContents() {
        return getServiceResponseFor("|GetInstFile|", true); // return text contents of hypinst. instruction file
    }

    // |PutInstFile| <filename> - write text lines to server "filename" instruction file, sends |EOT| 
    public boolean putInstFile(String instrTextLines, String filename) {
        boolean status = setServiceResponseFor("|PutInstFile| " + filename);
        if (! status) return false;
        status = writeMessage(instrTextLines); // each line terminated by "\n"
        if (!status) return status;
        // no matter what send the terminator
        status &= writeMessage("|EOT|"); // signal end of file data
        return status;
    }

    // |SetInstFile| - set instruction file back to default hypinst.
    public boolean setInstFile() {
        return setServiceResponseFor("|SetInstFile|");
    }

    // |SetInstFile| <filename> - set use of named instruction file for the duration of the connection (from JList or field)
    public boolean setInstFile(String filename) {
        return setServiceResponseFor("|SetInstFile| " + filename);
    }

    public String getStaFile() {
        String msg = getServiceResponseFor("|GETSTANAME|", true);  // return name of Hypoinverse station file
        if (msg != null) {
            int idx = msg.lastIndexOf("\n");
            if (idx > 0) msg = msg.substring(0,idx); 
        }
        return msg;
    }
    public String getStaFileContents() {
        return getServiceResponseFor("|GETSTAFILE|", true);  // return text contents of Hypoinverse station file
    }

    public String getCrhFile() {
        String msg = getServiceResponseFor("|GETCRHNAME|", true); // return name of Hypoinverse crustal model file
        if (msg != null) {
            int idx = msg.lastIndexOf("\n");
            if (idx > 0) msg = msg.substring(0,idx); 
        }
        return msg;
    }
    public String getCrhFileContents() {
        return getServiceResponseFor("|GETCRHFILE|", true); // return text contents of Hypoinverse crustal model file
    }

    private boolean readLocationResults() {
      // reset engine instance string descriptions
      resultsString = "LocationEngine: no results.\n";
      resultHeaderStr = "";
      resultPhaseStr  = "";

      // After locating server sends EOT
      String header = getResponse(DEFAULT_LINE_TERMINATOR);
      if (header == null || ! header.equals("|EOT|")) {
        System.err.println("FYI: LocationEngine service LOC request followed by: " + header);
        return false;
      }

      // Signal solserver you want data
      if (! setServiceResponseFor("|ARCOUT|")) {
        System.err.println("Error: LocationEngine service for ARCOUT request failed");
        return false;
      }

      // Get the summary header
      header = getResponse(DEFAULT_LINE_TERMINATOR);
      if ( header == null || header.equals("?") || header.equals("|EOT|") ) {
        System.err.println("Error: LocationEngine service header msg: \"" +header+ "\"");
        return false;
      }
      if (debug) System.out.println("DEBUG: LocationEngine service header msg: \"" +header+ "\"");

      // reset engine instance string descriptions
      resultHeaderStr = "";
      resultPhaseStr  = "";
      // Make an arc formatted string for output (put in textarea if there is one.

      resultsString = "HYPOINVERSE SERVER ARC RESULTS ";
      if (arcsum.parseArcSummary(header)) {
        // id not formatted correctly patch for arc summary parse
        if (!currentSol.getId().isNull() && currentSol.getId().longValue() > 0 )
                arcsum.evid = currentSol.getId().longValue();
        resultsString += (arcsum.getFormattedErrorEllipseString() + "\n");
        resultsString += (ArcSummary.getTitle() + "\n");
        resultsString += (arcsum.getFormattedOriginString() + "\n");
      } else {
        resultsString += ("Bad Arc Summary Header parseStatus" + "\n");
        return false; // added short-circuit here -aww 2010/08/24
      }

      resultHeaderStr = resultsString;
      // Reset result string for phase list parse
      resultsString = (ArcStation.getTitle() + "\n");

      // Copy location dependent info from newSol to old Solution
      // the orid, evid, commid, Event and Magnitude objects are unchanged
      // Parse header into existing solution.
      // Save event type (e.g. local, quarry) and restore it later
      // because it is cleared by call to clearLocationAttributes()
      // DataString doesn't "clone" the String (it's uncloneable) but a reference to it.
      DataString savedEventType = (DataString) currentSol.eventType.clone(); // aww change
      DataString savedGType = (DataString) currentSol.gtype.clone();
      LatLonZ oldLatLonZ = currentSol.getLatLonZ(); // bugfix: line moved here, was below clearLocation -aww 2008/03/11
      currentSol.clearLocationAttributes(); // reset all solution dependent fields
      currentSol.eventType = savedEventType; // restore the event type
      currentSol.gtype = savedGType; // restore the origin gtype

      // read new solution dependent fields, method returns input
      // WARNING! Hypoformat method doesn't throw exception or
      // set any status after a bad parse so junk could be returned.
      boolean parseSuccess = true; // dont' know any thing here
      Solution sol = HypoFormat.parseArcSummary(currentSol, header);
      if (sol == null) return false; // returns null solution for exception, otherwise the input currentSol

      // set all the fields that HypoFormat doesn't have
      currentSol.getEventAuthority(); // resets the authority if region changed -aww 2011/09/21 
      //
      // NOTE: regional eventAuthority or solution's origin authority could be different
      //if ( !currentSol.authority.equalsValue(currentSol.getEventAuthority()) ) // -aww 2011/08/17, removed 2011/09/21
      //    currentSol.authority.setValue(currentSol.getEventAuthority()); // -aww 2011/08/17, removed 2011/09/21
      if ( !currentSol.authority.equalsValue(EnvironmentInfo.getNetworkCode()) ) // reverted back -aww 2011/09/21
          currentSol.authority.setValue(EnvironmentInfo.getNetworkCode());

      if ( !currentSol.source.equalsValue(EnvironmentInfo.getApplicationName()) )
          currentSol.source.setValue(EnvironmentInfo.getApplicationName());

      if ( !currentSol.algorithm.equalsValue(getLocatorName()) )
          currentSol.algorithm.setValue(getLocatorName());

      if ( !currentSol.type.equalsValue(Solution.HYPOCENTER) )
          currentSol.type.setValue(Solution.HYPOCENTER); // a Hypocenter

      // saved state effected by solCommitResetsRflag property which sets currentSol.commitResetsRflag -aww 2015/04/03
      if ( currentSol.commitResetsRflag && !currentSol.processingState.equalsValue(EnvironmentInfo.getAutoString()) ) {
          currentSol.processingState.setValue(EnvironmentInfo.getAutoString()); //automatic or human?
      }

      if (! currentSol.validFlag.equalsValue(1)) {
          System.out.println("INFO: LocationEngine currentSol (deleted?) selectflag (validFlag) = 0");
          //currentSol.validFlag.setValue(1);  // valid? - remove 2014/10/30 why is it changing the "delete" status? -aww
      }

      // however could have phase parsing error try block exception below 
      // or could have too few phases for valid solution
      if ( !currentSol.dummyFlag.equalsValue(0) )
          currentSol.dummyFlag.setValue(0); // - added 07/28/2006 aww  

      if ( !currentSol.horizDatum.equalsValue(datumHorizontal))
          currentSol.horizDatum.setValue(datumHorizontal);

      if ( !currentSol.vertDatum.equalsValue(datumVertical))
          currentSol.vertDatum.setValue(datumVertical);

      // parse the per-station results into the existing currentPhaseList
      String msg = "";
      Phase phNew = null;
      int knt = 0;
      boolean ioExcepted = false; // exception error flag
      try {
        while (true) {
           msg = readMessage(DEFAULT_LINE_TERMINATOR);
           if (msg == null || msg.startsWith("|EOT|")) break; // What about using ascii char EOT int value 4 ?

          // skip blank lines and last summary line
          if (msg.length() == 0 || msg.startsWith("     ")) continue;   // skip blank line
          // check line's validity and build up output string
          if (debug) System.out.println("DEBUG: LocationEngine service phase msg: \"" +msg+ "\"");
          if (arcstn.parseArcStation(msg)) {
            // could make this the raw return string
            resultsString += (arcstn.getTableString() + "\n");
          } else {
            resultsString += ("Bad Arc Station parseStatus" + "\n");
            parseSuccess = false; // flag questionable parse result ? -aww
          }
          // re-parse message into a NEW phase object
          // NOTE: parse above and this parse could be combined (DDG)
          phNew = HypoFormat.parseArcToPhase(msg);
          // find matching phase in currentPhaseList & transfer 'result' part of newly
          // parsed phase (ie the 'assoc' object) into original input phase.
          if (phNew != null) {
              knt += copyDependentData(phNew, currentPhaseList);
          } else {
              resultsString += ("HypoFormat phase line parse failure.\n");
          }
        } // end of while loop
      } // end of try block
      catch (InterruptedIOException ex) {
        statusString = "LocationEngine service socket timed out after "+ serverTimeoutMillis/1000+" seconds.\n";
        resultsString += statusString + ex.getMessage();
        System.err.println(statusString + ex.getMessage());
        ioExcepted = true;
      }
      catch (IOException ex) {
        statusString = "LocationEngine service socket read error:\n";
        resultsString += statusString + ex.getMessage() + "\n";
        System.err.println(statusString + ex.getMessage());
        ioExcepted = true;
      }
      finally { // added this block to save the text results up to this point - aww 01/18/2005
        resultPhaseStr = resultsString;
        resultsString = resultHeaderStr + resultPhaseStr; // combine results 08/18/2004 -aww test
        if (debug) {
          System.out.println("DEBUG: LocationEngine resultsString:");
          System.out.println(resultsString);
        }

        // if exception, don't flag success, reset solution state, or fire solved.
        if (ioExcepted) {
            disconnect(); // added disconnect - 2015/06/03 aww
            return false;
        }
      }

      statusString = ((parseSuccess && knt > 0) ?
          "Location success for " + knt + " phases.\n" : "Location ERROR? " + knt + " phases found, check for parse ERROR.\n");
      if (knt <= 0) System.err.println(statusString);
      if (knt != phasesSent) System.err.println("LocationEngine INFO: Phases returned by server fewer than number sent.");

      currentSol.gap.setValue(currentSol.getPhaseList().getMaximumAzimuthalGap());
      //Old way overrode the HypoFormat HYP2000 parsed epicentral dist and ESaz
      //with distance set to slant (true) distance and azimuth SEaz
      //Should use HYP2000 ESaz convention for AssocAro, AssocAmO, and AssocCoO
      //
      //Below call to calcDistances overwrites the parsed dist, az results and
      //requires LatLonZ in Channel objects else dist, az are set to null values.
      //also does the calc for deleted phases not "sent" over to the server.
      //calcDistances(currentPhaseList, currentSol.getLatLonZ()); // aww 6/11/04
      calcDistances(currentPhaseList, currentSol); // aww 6/11/04

      currentSol.totalReadings.setValue(currentSol.phaseList.getChannelWithInWgtCount()); // all with inWgt> 0 -aww 2011/05/11
      currentSol.usedReadings.setValue(currentSol.phaseList.getChannelUsedCount());
      currentSol.sReadings.setValue(sol.phaseList.getChannelUsedCount("S")); // all S with weight > 0 -aww 2011/05/11

      //System.out.println("DEBUG LEHI totalReadings: " + sol.totalReadings);
      //System.out.println("DEBUG LEHI usedReadings : " + currentSol.usedReadings); // all with weight > 0 -aww 2011/05/11
      //System.out.println("DEBUG LEHI s readings   : " + sol.sReadings);

      currentSol.setStale(false); // reset flag since solution is now "fresh"
      currentSol.setSolveDate(); // set current time

      //Check for location change from previous, if so, set "stale" state for dependent magnitudes -aww
      double deltaKm = oldLatLonZ.distanceFrom(currentSol.getLatLonZ());
      // Flag a commit if its origin change > 10 meters
      if ( deltaKm > 0.01) {
          if (verbose || debug)
              System.out.println("INFO: LocationEngine Solution LatLonZ changed by a delta km of : "+ Math.round(deltaKm*1000.)/1000.);
          //currrentSol.setNeedsCommit(true);
          // Only redo its magnitudes if origin change > 100 meters
          if (deltaKm >= 0.10) { 
              currentSol.setMagnitudeStatusStale(); // force new magnitude calculation
          }
          else {
              if (verbose || debug) System.out.println("      small location delta km change: magnitude not set stale.");
              currentSol.setMagnitudeNeedsCommit(); // force synch orid update - aww 2008/03/11
          }
      }

      firePropertyChange(((knt > 0) ? EngineIF.SOLVED : EngineIF.FAILED), (Solution) null, currentSol);

      return (knt > 0) ? parseSuccess : false; // not always valid, some parse errors could slip through, corruption of data
    }

    /** Find the original phase that returned in the Location output and copy all
    the Solution dependent values to the original phase. */
    private int copyDependentData(Phase newPhase, PhaseList phaseList) {
      //
      //if (debug) System.out.println("DEBUG "+getClass().getName()+"\n copyDependentData input  Phase= "+newPhase.toString());
      //
      //boolean saveLocSetting =  ChannelName.useLoc; // save state of location matching see toggle below -aww removed 12/02/2005
      // Flag set to not use the site location, it's not known in newly parsed phase from hypoinv
      // until server hypoinverse code is updated
      //ChannelName.useLoc = false; // Temporary patch here, remove when location codes implemented! -aww removed 12/02/2005
      //Note: phaselist method below does not check for a sol association match:
      Phase oldPhase = (Phase) phaseList.getLikeWithSameTime(newPhase);
      //ChannelName.useLoc = saveLocSetting; // Temporary patch to handle channel location matching! -aww removed 12/02/2005
      //
      if (oldPhase == null)  {
        if (debug) {
          System.out.println( "ERROR " + getClass().getName() +
            " copyDependentData code/data corrupt, no matching phase found in input phaseList size: "
            + phaseList.size()
          );
        }
        return 0;   // no match
      }
      // copy stuff that depends on the solution (dist, rms, etc.)
      oldPhase.copySolutionDependentData(newPhase);
      //if (debug) System.out.println(" copyDependentData output Phase= "+ oldPhase.toString());
      if (! oldPhase.getChannelObj().hasLatLonZ() ) {
          System.out.println(" WARNING! LocationEngine phase missing LatLonZ for dist,az data: "+ oldPhase.toString());
          //oldPhase.matchChannelWithDataSource();
      }
      return 1;
    }

    /*
      // NOTE DIFFERENCE below with MUNG required IMPLEMENATION above
      // which takes into account 2 factors regarding matching input/output phases.
      // 1) Db ChannelName LOCATION field mismatch with data returned for channel by HypoInverse server.
      // 2) Discriminating two phases of same phase type but with different times for a channel.
      //    e.g. one phase is "down weighted" to determine its residual compared to the other.
      //    Jiggle doesn't allow "multiple" per P or S picks per channel but Mung editor does allow it.
    private void copyDependentData(Phase newPhase, PhaseList phaseList) {
      if (debug) System.out.println("DEBUG loc engine copyDependentData copy: newPhase= "+ newPhase.toString());
      // find a matching phase
      Phase oldPhase = (Phase) phaseList.getLikeChanType(newPhase); // NO required assoc?
      if (oldPhase == null) {
        System.out.println(getClass().getName()+
            " ERROR copyDependentData code/data corrupt, no matching phase found in input phaseList");
        return;  // no match shouldn't happen corruption?
      }
      // if (debug) System.out.println("copy: oldPhase= "+ oldPhase.toString());
      // copy stuff that depends on the solution (dist, rms, etc.)
      oldPhase.copySolutionDependentData(newPhase);
      if (debug) System.out.println("DEBUG                                  : oldPhase= "+ oldPhase.toString());
      if (! oldPhase.getChannelObj().hasLatLonZ() ) { // && verbose) {
        System.out.println(" WARNING! LocationEngine phase has no LatLonZ: Missing dist,az commit data:\n  "+ oldPhase.toString());
        //oldPhase.matchChannelWithDataSource();
      }
    }
    */

    /**
     * Write string to service socket as a message. A message is variable length terminated
     * with '\n'. Used by client to send command strings to service. */
    private boolean writeMessage(String msg) { // should this be protected ?? -aww
        return writeMessage(msg, "\n");
    }

    private boolean writeMessage(String msg, String terminator) { // should this be protected ?? -aww
      String strOut = msg + terminator;   // append the terminator
      boolean status = true;
      // try reconnect on error
      try {
        streamOut.write(strOut.getBytes());
        statusString = strOut;
        if (debug) System.out.println("DEBUG LocationEngine writeMessage (to solserver):"+msg);
      } catch (IOException ex) {
        statusString = "LocationEngine writeMessage socket error for msg: " + msg + "\n";
        System.err.println(statusString + ex.getMessage());
        status = false;
        try {
          disconnect(false); // force socket closed, no EOT write needed
        }
        catch (Exception ex2) {
          statusString = "LocationEngine writeMessage disconnect failed\n";
          System.err.println(statusString + ex.getMessage());
        }
      }
      return status;
    }

   /**
    * Read message from socket. A message is variable length terminated with '\n'.
    * Pass IOExeceptions back up to the caller.
    */
    private String readMessage(int terminator) throws IOException {
      byte[] bytes = new byte[4096] ;
      int    intRead = 0;
      int    byteCount = -1;
      // byte[] b = new byte[1];

      // TCP is a stream protocol :. must read one byte at a time and look for
      // terminator Note that the terminator is NOT include in the message
      // returned to the caller
        while ( (intRead = streamIn.read()) != -1) {
          if (intRead == terminator) break;      // terminator - bail
          bytes[++byteCount] = (byte) intRead;
          //System.out.println(intRead + " " + new String(bytes, 0, byteCount+1) );
          if (byteCount == bytes.length ) break;  // buffer is full
         }

      // socket closed :. broken message(?)
      if (intRead == -1) return (String) null ;
      else if (byteCount == -1) return "";
      //if (debug) System.out.println("DEBUG readMessage bytes:" +byteCount+ ":\"" + new String(bytes,0,byteCount+1) + "\"");
      return (new String(bytes, 0, byteCount+1) );  // convert byte[] -> String
    }


/*
    public static final class Tester {
      public static void main(String args[]) {
        int evid;
        // switch for debugging
        boolean commitIt = false;

        if (args.length <= 0)        // no args
        {
          System.out.println("Usage: java LocationEngineHypoInverse [evid])");

          //evid = 9526852;

          evid = 13998604;

          System.out.println("Using evid "+ evid+" as a test...");
        } else {
          Integer val = Integer.valueOf(args[0]);            // convert arg String
          evid = (int) val.intValue();
        }

        JiggleProperties props = new JiggleProperties("properties");
        DbaseConnectionDescription dbConn = props.getDbaseDescription();
        // The data source is static
        //        DataSource ds = new DataSource(url, driver, user, passwd, writeBackEnabled);
        DataSource ds = new DataSource(dbConn);

        String locEngAddr = props.getProperty("LocationEngineAddress");
        int locEngPort    = props.getInt("LocationEnginePort");
//        String locEngAddr = props.getProperty("LocationEngineHypoInverseAddress");
//        int locEngPort    = props.getInt("LocationEngineHypoInversePort");

//        boolean writeEnable = true;
//        DataSource init = new DataSource (url, driver, user, passwd);

        System.out.println("Making MasterView for evid = "+evid);

        MasterView mv = new MasterView();            // make MasterView
        mv.setWaveformLoadMode(MasterView.LoadNone);
        mv.defineByDataSource(evid);

        Solution sol = (Solution) mv.solList.getSelected();

        System.out.println("--- Before location ------------------------------");
        System.out.println(sol.fullDump());
        System.out.println("--------------------------------------------------");

         // Set location engine explicitely for testing. Overrides properties file
         // locEngAddr = "serverdi.gps.caltech.edu";
         //  locEngPort = 6550;
         //  locEngAddr = "serverjo.gps.caltech.edu";
         //  locEngPort = 6630;

        LocationEngineHypoInverse locEng =
            new LocationEngineHypoInverse(locEngAddr, locEngPort);

        //        boolean status = locEng.solve(sol, mv.phaseList);
        boolean status = locEng.solve(sol);
        sol.processingState.setValue(JasiProcessingConstants.STATE_HUMAN_TAG);

        if (status) {
            System.out.println("--- After Location -------------------------------");
            System.out.println(sol.fullDump());
            System.out.println("--------------------------------------------------");
            //        sol.phaseList.dump();
            if (commitIt) {
                System.out.println("Committing solution...");
                System.out.println("Message = "+locEng.getStatusString());
                //                sol.commit();
            } else {
                System.out.println("NOT Committing solution...");
            }
        } else {
            System.out.println("Test solution failed.");
            System.out.println(locEng.getStatusString());
        }

        sol.depthFixed.setValue(true);
        sol.locationFixed.setValue(true);
        sol.depth.setValue(6.0);

        status = locEng.solve(sol);

        if (status) {
            System.out.println("--- After Location () fixed Z -------------------------------");
            System.out.println(sol.fullDump());
            System.out.println("--------------------------------------------------");
        } else {
            System.out.println("Test solution failed.");
            System.out.println(locEng.getStatusString());
        }
        // Examine the input phase data:
        System.out.println(sol.getPhaseList().toNeatString());

      } // end of main
    } // end of Tester class
*/
} // end of Location engine class

