
package org.trinet.util;
public class PropertyFormatException extends RuntimeException {
    public PropertyFormatException() {
  super();
    }

    public PropertyFormatException(String message) {
  super(message);
    }
}
