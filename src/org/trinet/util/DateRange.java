package org.trinet.util;
import java.util.*;
import org.trinet.jdbc.*;
import org.trinet.jdbc.datatypes.*;
//
// Reworked entire class to convert all time into true millisecs
// for both java.util.Date and org.trinet.util.DateTime inputs - aww 2008/02/08
//
/** Class implements a java.util.Date range. Stores bounds as number representing long true millisecs of time. */
public class DateRange extends NumberRange implements DateRangeIF, DateStringFormatter,
        Cloneable, Comparable {

/** Default constructor, null bounds. */
    public DateRange() {}

/** Constructor creates minimum and maximum bounds both input parameters should be true millisecs time values.
* @exception java.lang.IllegalArgumentException max < min
*/
    public DateRange(long min, long max) {
        super(new Long(min), new Long(max));
    }

/** Constructor converts input true seconds time values to rounded long millisecs time values.*/
    public DateRange(double minTrueSecs, double maxTrueSecs) {
        this(Math.round(minTrueSecs*1000.), Math.round(maxTrueSecs*1000.));
    }

/** Constructor creates minimum and maximum bounds from the input converting nominal to true time values.
* @exception java.lang.IllegalArgumentException max < min
*/
    public DateRange(java.util.Date min, java.util.Date max) {
        super();
        if ( (min != null && max != null) && (min.getClass() != max.getClass()) )
            throw new IllegalArgumentException("Input date subclass mismatch: " + min.getClass().getName() + "<>" +
               max.getClass().getName()); 
        setMin(min);
        setMax(max);
    }

/** Constructor uses LeapSeconds.stringToTrue(String date, String pattern) to construct time bounds. */
    public DateRange(String minDate, String maxDate, String pattern) {
        this(LeapSeconds.stringToTrue(minDate, pattern), LeapSeconds.stringToTrue(maxDate, pattern));
    }

/** Returns true milliseconds value of lower bound.  Returns Long.MIN_VALUE if no bound defined. */
    public long getMinTime() {
        return (min == null) ? Long.MIN_VALUE : min.longValue(); // default for null used to 0l - aww
    }

/** Returns true milliseconds value of upper bound.  Returns Long.MAX_VALUE if no bound defined. */
    public long getMaxTime() {
        return (max == null) ? Long.MAX_VALUE : max.longValue(); // default for null used to 0l - aww
    }

/** Returns true seconds value of lower bound. Returns -Double.MAX_VALUE if no bound defined*/
    public double getMinTrueSecs() {
        return (min == null) ? -Double.MAX_VALUE : min.doubleValue()/1000.; // default for null used to 0. - aww
    }
/** Sets the lower bound, converting from input true seconds to true millisecs. */
    public void setMinTrueSecs(double trueSecs) {
      if (trueSecs == -Double.MAX_VALUE) setMin( (Number) null);
      else setMin( new Long(Math.round(trueSecs*1000.)) ); // hopefully won't overflow, but no check
    }

/** Returns true seconds value of upper bound. Returns Double.MAX_VALUE if no bound defined*/
    public double getMaxTrueSecs() {
        return (max == null) ? Double.MAX_VALUE : max.doubleValue()/1000.; // default null used to 0.
    }

/** Sets the upper bound, converting from input true seconds to true millisecs. */
    public void setMaxTrueSecs(double trueSecs) {
      if (trueSecs == Double.MAX_VALUE) setMax((Number) null);
      else setMax( new Long(Math.round(trueSecs*1000.)) ); // hopefully won't overflow, but no check
    }

/** Returns nominal seconds value of lower bound. Returns -Double.MAX_VALUE if no bound defined*/
    public double getMinNominalSecs() {
        return (min == null) ? -Double.MAX_VALUE : LeapSeconds.trueToNominal(min.doubleValue()/1000.); // default for null used to 0. - aww
    }
/** Sets the lower bound, converting from input nominal seconds to true millisecs. */
    public void setMinNominalSecs(double nominalSecs) {
      if (nominalSecs == -Double.MAX_VALUE) setMin( (Number) null);
      else setMin( new Long(Math.round(LeapSeconds.nominalToTrue(nominalSecs)*1000.)) ); // hopefully won't overflow, but no check
    }

/** Returns nominal seconds value of upper bound. Returns Double.MAX_VALUE if no bound defined*/
    public double getMaxNominalSecs() {
        return (max == null) ? Double.MAX_VALUE : LeapSeconds.trueToNominal(max.doubleValue()/1000.); // default for null used to 0. - aww
    }

/** Sets the upper bound, converting from input nominal seconds to true millisecs. */
    public void setMaxNominalSecs(double nominalSecs) {
      if (nominalSecs == Double.MAX_VALUE) setMax((Number) null);
      else setMax( new Long(Math.round(LeapSeconds.nominalToTrue(nominalSecs)*1000.)) ); // hopefully won't overflow, but no check
    }

/** Returns minimum bound as nominal time Date. Returns null if no bound is defined.*/
    public java.util.Date getMinDate() {
        return (min == null) ? null : new java.util.Date(Math.round(LeapSeconds.trueToNominal(min.longValue()/1000.)*1000.));
    }
/** Returns maximum bound as nominal time Date. Returns null if no bound is defined.*/
    public java.util.Date getMaxDate() {
        return (max == null) ? null : new java.util.Date(Math.round(LeapSeconds.trueToNominal(max.longValue()/1000.)*1000.));
    }

/** Returns minimum bound as DateTime (with leap seconds). Returns null if no bound is defined.*/
    public DateTime getMinDateTime() {
        return (min == null) ? null : new DateTime(min.doubleValue()/1000., true); // for UTC seconds
    }
/** Returns maximum bound as DateTime (with leap seconds). Returns null if no bound is defined.*/
    public DateTime getMaxDateTime() {
        return (max == null) ? null : new DateTime(max.doubleValue()/1000., true); // for UTC seconds
    }

/** Returns the size of the range as a double seconds. */
    public double seconds() {
        return doubleExtent()/1000.;
    }

/** Returns the size of the range as a double milliseconds. */
    public double doubleExtent() {
        return  (double) longExtent();
    }

/** Returns the size of the range as a long milliseconds. */
    public long longExtent() {
        return  (min == null || max == null) ? Long.MAX_VALUE : (max.longValue() - min.longValue());
    }

/** True if minimum bound of this instance is after the input date. */
    public boolean after(java.util.Date date) {
        if (date == null) return true;
        if (date instanceof DateTime) {
            return super.after(new Long(((DateTime)date).getTrueMillis()));
        }
        else {
            return super.after(new Long(Math.round(LeapSeconds.nominalToTrue(date.getTime()/1000.)*1000.)));
        }
    }

/** True if maximum bound of this instance is before the input date. */
    public boolean before(java.util.Date date) {
        if (date == null) return true;
        if (date instanceof DateTime) {
            return super.before(new Long(((DateTime)date).getTrueMillis()));
        }
        else {
            return super.before(new Long(Math.round(LeapSeconds.nominalToTrue(date.getTime()/1000.)*1000.)));
        }
    }

/** True if this object's range excludes input date.
 *  Returns true if input is null.
 */
    public boolean excludes(java.util.Date date) {
        if (date == null) return true;
        if (date instanceof DateTime) {
            return super.excludes(new Long(((DateTime)date).getTrueMillis()));
        }
        else {
            return super.excludes(new Long(Math.round(LeapSeconds.nominalToTrue(date.getTime()/1000.)*1000.)));
        }
    }

/** True if this object's range contains the input true millisecs time.
 *  Returns false otherwise.
 */
    public boolean contains(long ms) {
      return super.contains(new Long(ms));
    }
/** True if this object's range excludes the input true millisecs time.
 *  Returns false otherwise.
 */
    public boolean excludes(long ms) {
      return super.excludes(new Long(ms));
    }
/** True if this object's max range is before the true input millisecs time.
 *  Returns false otherwise.
 */
    public boolean before(long ms) {
        return super.before(new Long(ms));
    }
/** True if this object's min range is after the input true millisecs time.
 *  Returns false otherwise.
 */
    public boolean after(long ms) {
        return super.after(new Long(ms));
    }

/** True if this object's range contains the input true seconds time.
 *  Returns false otherwise.
 */
    public boolean contains(double trueSecs) {
      return super.contains(new Long(Math.round(trueSecs*1000.)));
    }
/** True if this object's range excludes the input true seconds time.
 *  Returns false otherwise.
 */
    public boolean excludes(double trueSecs) {
      return super.excludes(new Long(Math.round(trueSecs*1000.)));
    }
/** True if this object's max range is before the input true seconds time.
 *  Returns false otherwise.
 */
    public boolean before(double trueSecs) {
      return super.before(new Long(Math.round(trueSecs*1000.)));
    }
/** True if this object's min range is after the input true seconds time.
 *  Returns false otherwise.
 */
    public boolean after(double trueSecs) {
        return super.after(new Long(Math.round(trueSecs*1000.)));
    }

/** True if this object's range contains input date.
 *  Returns false if input is null.
 */
    public boolean contains(java.util.Date date) {
        if (date == null) return false;
        if (date instanceof DateTime) {
            return super.contains(new Long(((DateTime)date).getTrueMillis()));
        }
        else {
            return super.contains(new Long(Math.round(LeapSeconds.nominalToTrue(date.getTime()/1000.)*1000.)));
        }

    }

    private LongRange getLongRange(java.util.Date minDate, java.util.Date maxDate) {
        Long mind = null;
        Long maxd = null;

        if (minDate != null) {
          if (minDate instanceof DateTime) {
              mind = new Long(((DateTime)minDate).getTrueMillis());
          }
          else {
              mind = new Long(Math.round(LeapSeconds.nominalToTrue(minDate.getTime()/1000.)*1000.));
          }
        }

        if (maxDate != null) {
          if (maxDate instanceof DateTime) {
              maxd = new Long(((DateTime)maxDate).getTrueMillis());
          }
          else {
              maxd = new Long(Math.round(LeapSeconds.nominalToTrue(maxDate.getTime()/1000.)*1000.));
          }
        }
        return new LongRange(mind, maxd);
    }

/** True if this object's range excludes input range. */
    public boolean excludes(java.util.Date minDate, java.util.Date maxDate) {
        return super.excludes(getLongRange(minDate, maxDate));
    }

/** True if this object's range contains input range. */
    public boolean contains(java.util.Date minDate, java.util.Date maxDate) {
        return super.contains(getLongRange(minDate, maxDate));
    }

/** True if this object's range overlaps input range. */
    public boolean overlaps(java.util.Date minDate, java.util.Date maxDate) {
        return super.overlaps(getLongRange(minDate, maxDate));
    }

/** True if this object's range lies within input range. */
    public boolean within(java.util.Date minDate, java.util.Date maxDate) {
        return super.within(getLongRange(minDate, maxDate));
    }

/** Returns String concatenation of minimum and maximum bounds values
* formatted as specified by the input pattern relative to UTC time zone.
* The min, max dates are separated by " ".
*/
    public String toDateString(String pattern) {
        StringBuffer sb = new StringBuffer(80);
        if (min != null) {
            sb.append(LeapSeconds.trueToString(min.doubleValue()/1000., pattern));
        }
        else sb.append("NULL");
        sb.append(" ");
        if (max != null) {
            sb.append(LeapSeconds.trueToString(max.doubleValue()/1000., pattern));
        }
        else sb.append("NULL");
        return sb.toString();
    }

/** Returns String concatenation of minimum and maximum bounds as
* GMT date values separated by " ".
*/
    public String toString() {
        return toDateString("yyyy-MM-dd:HH:mm:ss.fff"); // changed from yyyy/MM/dd -aww 2008/02/13
    }

/** Convenience wrapper System.out.println(toString()). */
    public void print() {
        System.out.println(toString());
    }

/**
* Sets the minimum bound of the range to the input.
*/
    public void setMin(java.util.Date minDate) {
        if (minDate == null) {
            setMin((Number) null);
        }
        else if (minDate instanceof DateTime) {
          super.setMin(new Long(((DateTime)minDate).getTrueMillis()));
        }
        else {
          super.setMin(new Long(Math.round(LeapSeconds.nominalToTrue(minDate.getTime()/1000.)*1000.)));
        }
    }

/**
* Sets the maximum bound of the range to the input.
*/
    public void setMax(java.util.Date maxDate) {
        if (maxDate == null) {
            setMax((Number)null);
        }
        else if (maxDate instanceof DateTime) {
          super.setMax(new Long(((DateTime)maxDate).getTrueMillis()));
        }
        else {
          super.setMax(new Long(Math.round(LeapSeconds.nominalToTrue(maxDate.getTime()/1000.)*1000.)));
        }
    }

/**
* Sets the minimum, maximum bounds of the range to the input values.
*/
    public void setLimits(java.util.Date minDate, java.util.Date maxDate) {
        if (minDate != null && maxDate != null) {
            if (maxDate.before(minDate))
                throw new IllegalArgumentException("DateRange min-max bounds reversed.");
        }
        setMin(minDate);
        setMax(maxDate);
    }

/**
* Sets the appropiate minimum or maximum bound to extend the range to include the input value.
* Does a no-op if contains(Date) == true.
*/
    public void include(java.util.Date date) {
        if (date instanceof DateTime) super.include(new Long(((DateTime)date).getTrueMillis()));
        else super.include(new Long(Math.round(LeapSeconds.nominalToTrue(date.getTime()/1000.)*1000.)));
    }

/*
    public static void main(String [] args) {

         System.out.println("++++++++ BEGIN TEST ++++++++++");
         long ms = System.currentTimeMillis();
         Date now = new Date(ms);

         Date ontime = new Date(ms + 360000);
         Date later = new Date(ms + 3600000);
         Date laterstill = new Date(ms + 7200000);
         Date yesterday = new Date(ms - 86400000);
         Date tomorrow =  new Date(ms + 86400000);

         DateRange drGMT = new DateRange(0., 1.);
         System.out.println("Test drGMT (0,1) :" +drGMT.toString());
         drGMT = new DateRange("1977-01-01:00:00:00.000", "1977-01-02:00:00:00.000", "yyyy-MM-dd:HH:mm:ss.SSS");
         System.out.println("Test drGMT (Jan 01, 1977 00:00):" +drGMT.toString());

         DateRange dr = new DateRange(now, later);
         DateRange dr2 = new DateRange(yesterday, tomorrow);

         dr.dump1(yesterday);
         dr.dump1(ontime); dr.dump1(later); dr.dump1(tomorrow);

         dr.dump2(dr2);

         System.out.println("Test dr.setLimits(ontime, laterstill)");
         dr.setLimits(ontime, laterstill);
         dr.dump2(dr2);

         System.out.println("Test dr.include(now)");
         dr.include(now);
         dr.dump2(dr2);

         System.out.println("Test dr.include(dr2)");
         dr.include(dr2);
         dr.dump2(dr2);

         System.out.println("Test dr.setMax(laterstill); dr2.setMin(laterstill)");
         dr.setMax(laterstill);
         dr2.setMin(laterstill);
         dr.dump2(dr2);

         System.out.println("Test dr.setMax(later); dr2.setMin(laterstill)");
         dr.setMax(later);
         dr2.setMin(laterstill);
         dr.dump2(dr2);
//
        if (args.length < 4) {
            System.out.println("Database timestamp test requires input args: [user] [pwd] [host.domain] [dbName]
            System.exit(0);
        }

        String user   = args[0];
        String passwd = args[1];
        String host   = args[2];
        String dbname = args[3];

        String url= "jdbc:" + org.trinet.jdbc.datasources.AbstractSQLDataSource.DEFAULT_DS_SUBPROTOCOL +
        ":@"+ host +":"+ org.trinet.jdbc.datasources.AbstractSQLDataSource.DEFAULT_DS_PORT +":"+ dbname;

        System.out.println("DateRange: making connection... ");
        org.trinet.jasi.DataSource ds = new DataSource (url, org.trinet.jdbc.datasources.AbstractSQLDataSource.DEFAULT_DS_DRIVER, user, passwd);
        System.out.println("DateRange dataSource : " + ds.describeConnection());
        oracle.jdbc.OracleConnection conn = (oracle.jdbc.OracleConnection) ds.getConnection();
        //Locale[] l = Locale.getAvailableLocales();
        //for (int i=0; i<l.length; i++) System.out.println(l[i].toString());
        try {
           conn.setSessionTimeZone("GMT");
           //
           java.sql.Statement sm = conn.createStatement();
           oracle.jdbc.OracleResultSet rs = (oracle.jdbc.OracleResultSet) sm.executeQuery(
           //"SELECT ondate FROM CHANNEL_DATA where sta='ADO' and seedchan='BHE' order by ondate"
           "select ondate from timestamp_data"
           );
           while (rs.next()) {
             //oracle.sql.TIMESTAMPLTZ dtz = rs.getTIMESTAMPLTZ(1);
             System.out.println(rs.getObject(1).getClass().getName());
             java.sql.Timestamp d0 = rs.getTimestamp(1);
             System.out.println("Timestamp Date: " + d0.toString());
             System.out.println("Timestamp EpochTime.epochToString: " + EpochTime.epochToString(d0.getTime()/1000.));
             //oracle.sql.TIMESTAMPTZ d = rs.getTIMESTAMPTZ(1);
             //String d2 = d.toString(conn, d.toBytes());
             //System.out.println("Orig Date: " + d2 + " " + d.toString());
             //long msoff = GregorianCalendar.getInstance().getTimeZone().getRawOffset();
             //System.out.println("offset ms = " + msoff/3600000l);
             //d.setTime(d.getTime() + msoff);
             //String d = rs.getString(1);
             //System.out.println("Date: " + " " + d);
             //System.out.println("After Offset Date: " + d.getTime() + " " + d.toString());
             //System.out.println("EpochTime.epochToString: " + EpochTime.epochToString(d.getTime()/1000.));
             //System.out.println("EpochTime.stringToDate time : " + EpochTime.stringToDate(d.toString()).getTime());
             //System.out.println("Epoch stringToDate time: " + EpochTime.stringToDate(d).getTime());
           }
//
           java.sql.Statement stmt = conn.createStatement ();
           String sql = "SELECT dbtimezone, sessiontimezone, current_timestamp, localtimestamp from dual ";
           java.sql.Timestamp myTime = null;
           java.sql.ResultSet rset = null;
           rset = stmt.executeQuery(sql);
           while (rset.next()) {
              System.out.println("-------------");
              System.out.println( "dbtimezone = " + rset.getString(1));
              System.out.println(" sessiontimezone " + rset.getString(2));
                           // myTime = rset.getTimestamp(1);
              System.out.println(" current_timestamp " + rset.getTimestamp(3));
              System.out.println(" localtimestamp " + rset.getTimestamp(4));
           }
           rset.close();
//


           if (rs != null) rs.close();
           if (sm != null) sm.close();
           if (conn != null) ds.close();
        }
        catch (Exception ex) {
             ex.printStackTrace();
        }
    }

    public void dump(DateRange dr2, DateRange dr3) {
         System.out.println("Test equalsRange: ");
         System.out.println("     dr2.toString(): " + dr2.toString());
         System.out.println("     dr3.toString(): " + dr3.toString());
         System.out.println("     dr2.equalsRange(dr3) : " +  dr2.equalsRange(dr3));
         System.out.println("------------------\n");
    }

     public void dump1(Date val) {
         System.out.println("Dump Range bounds: " + min.toString() + ", " + max.toString() + " size: " +
                             EpochTime.elapsedTimeToText(seconds()) );
         System.out.println(" Test input val: " + val);
         System.out.println(" after(val)    : " + after(val));
         System.out.println(" before(val)   : " + before(val));
         System.out.println(" excludes(val) : " + excludes(val));
         System.out.println(" contains(val) : " + contains(val));
         System.out.println("------------------\n");
    }

    public void dump2(DateRange dr2) {
         System.out.println("Dump Range bounds: " + min.toString() + ", " + max.toString() + " size: " +
                             EpochTime.elapsedTimeToText(seconds()) );
         System.out.println(" Test Range2 Min,max: " + dr2.min.toString() + ", " + dr2.max.toString() );

         System.out.println(" within(dr2)       : " + within(dr2));
         System.out.println(" dr2.within(this)  : " + dr2.within(this));

         System.out.println(" overlaps(dr2)     : " + overlaps(dr2));
         System.out.println(" dr2.overlaps(this): " + dr2.overlaps(this));

         System.out.println(" dr2.after(this)   : " + dr2.after(this));
         System.out.println(" after(dr2)        : " + after(dr2));

         System.out.println(" dr2.before(this)  : " + dr2.before(this));
         System.out.println(" before(dr2)       : " + before(dr2));

         System.out.println(" dr2.excludes(this): " + dr2.excludes(this));
         System.out.println(" excludes(dr2)     : " + excludes(dr2));

         System.out.println(" dr2.contains(this): " + dr2.contains(this));
         System.out.println(" contains(dr2)     : " + contains(dr2));
         System.out.println("------------------\n");

    }
*/
}

