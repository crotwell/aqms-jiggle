package org.trinet.util;
import java.awt.*;
import java.beans.*;
import java.util.*;
import org.trinet.jasi.*;
import org.trinet.jasi.engines.*;
import org.trinet.jasi.magmethods.*;
import org.trinet.util.*;
import org.trinet.util.locationengines.*;
import org.trinet.util.gazetteer.*;

public abstract class AbstractEngineDelegate implements EngineDelegateIF, PropertyChangeListener {

    protected boolean debug = false;
    protected boolean verbose  = false;
  
    protected boolean solveSuccess = false;

    protected String resultsString;
    protected String statusString;

    //Implement new named event types if necessary here or EngineDelegateIF
    //or separate class for named properties (e.g. solSolved or magSolved)
    
    //EventListenerList could be used instead to discriminate different types
    //would need to implement add/remove xxxEventListener and firexxxEvent methods
    //for the "types" of interest
    private PropertyChangeSupport propChangeSupport;

    protected GenericPropertyList myProps;

    protected HashMap engMap = new HashMap(11);

    protected AbstractEngineDelegate() { 
        propChangeSupport = new PropertyChangeSupport(this);
    }
    protected AbstractEngineDelegate(GenericPropertyList props) { 
        this();
        myProps = props;
    }

    public void setDebug(boolean tf) {
        debug = tf;
        if (debug) setVerbose(true);
        setDebugForEngines(debug);
    }

    public void setVerbose(boolean tf) {
        verbose = tf;
        setVerboseForEngines(verbose);
    }

    private void setVerboseForEngines(boolean tf) {
        Collection values = engMap.values();
        Iterator iter = values.iterator();
        while (iter.hasNext()) {
          ((EngineIF) iter.next()).setVerbose(tf);
        }
    }

    private void setDebugForEngines(boolean tf) {
        Collection values = engMap.values();
        Iterator iter = values.iterator();
        while (iter.hasNext()) {
          ((EngineIF) iter.next()).setDebug(tf);
        }
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propChangeSupport.addPropertyChangeListener(listener);
    }
    public void addPropertyChangeListener(String propName, PropertyChangeListener listener) {
        propChangeSupport.addPropertyChangeListener(propName, listener);
    }
    public void firePropertyChange(PropertyChangeEvent evt) {
        propChangeSupport.firePropertyChange(evt);
    }
    public void firePropertyChange(String propName, boolean oldValue, boolean newValue) {
        propChangeSupport.firePropertyChange(propName, oldValue, newValue);
    }
    public void firePropertyChange(String propName, int oldValue, int newValue) {
        propChangeSupport.firePropertyChange(propName, oldValue, newValue);
    }
    public void firePropertyChange(String propName, Object oldValue, Object newValue) {
        propChangeSupport.firePropertyChange(propName, oldValue, newValue);
    }
    //1.4public PropertyChangeListener[] getPropertyChangeListeners() {
        //1.4return propChangeSupport.getPropertyChangeListeners();
    //1.4}
    //1.4public PropertyChangeListener[] getPropertyChangeListeners(String propName) {
        //1.4return propChangeSupport.getPropertyChangeListeners(propName);
    //1.4}
    public boolean hasListeners(String propName) {
        return propChangeSupport.hasListeners(propName);
    }
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propChangeSupport.removePropertyChangeListener(listener);
    }
    public void removePropertyChangeListener(String propName, PropertyChangeListener listener) {
        propChangeSupport.removePropertyChangeListener(propName, listener);
    }

    // This default PropertyChangeListener method handles events fired
    // by the added engines as a no-op. Method is overriden in subclasses
    // to notify any classes that are registered as PropertyChangeListeners
    // of this instance about the fired engine property changes.
    public void propertyChange(PropertyChangeEvent e) {
        /*
        StringBuffer sb = new StringBuffer(512);
          sb.append("DEBUG").append(getClass().getName()).append(" propertyChange(e):\n");
          sb.append("  source:").append(e.getSource().getClass().getName()).append("\n");
          sb.append("  propName:").append(e.getPropertyName().append("\n");
          sb.append("  oldValue:").append(e.getOldValue()).append("\n");
          sb.append("  newValue:").append(e.getNewValue())
        Debug.println(sb.toString());
        */
    }

    public GenericPropertyList getDelegateProperties() {
        return myProps;
    }

    public boolean setDelegateProperties(GenericPropertyList props) {
        this.myProps = props;
        return (props == null) ? false : initializeEngineDelegate();
    }

    public boolean setDelegateProperties(String propFileName) {
        return (loadProperties(propFileName)) ?  initializeEngineDelegate() : false;
    }
    protected boolean loadProperties(String propFileName) {
        myProps =  new GenericPropertyList(); // empty list
        return myProps.readPropertiesFile(propFileName);
    }

    public boolean initializeEngineDelegate() {
        if (myProps == null) throw
          new NullPointerException("Init Failure; Input propertylist null: " + getClass().getName());
        return initialize();
    }
    protected boolean initialize() {
      initIO();          // do first to enable flags
      initEngines();
      return true; // TODO: change initXXX method return to boolean here and in subclasses - aww
    }

    abstract protected void initEngines();

    protected void initIO() {
      debug = myProps.getBoolean("debug");
      if (! debug)  debug = myProps.getBoolean("delegateDebug");
      if (debug) setVerbose(true);
      else setVerbose(myProps.getBoolean("verbose"));
      setDebugForEngines(debug);

      if (verbose)
         System.out.println(getClass().getName()+" property file: "+myProps.getUserPropertiesFileName());
    }

    public String getStatusString() {
        return statusString;
    }
    public String getResultsString() {
        return resultsString;
    }
    public void reset() {
        resultsString = "";
        statusString = "";
        solveSuccess = false;
        Collection values = engMap.values();
        Iterator iter = values.iterator();
        while (iter.hasNext()) {
          ((EngineIF) iter.next()).reset();
        }
    }

    abstract public boolean solve(Object data);

    public boolean success() {
        return solveSuccess;
    }

    public void addEngine(String keyName, EngineIF engine) {
        if (engine != null) {
          engine.removePropertyChangeListener(this); // remove old listener, if already there
          engine.addPropertyChangeListener(this);    // add listener to engine
          // aww - 11/09/2004 added logic below to remove listener from any prior engine instance
          EngineIF oldEngine = (EngineIF) engMap.put(keyName, engine); // return old engine instance or null
          if (oldEngine != null && oldEngine != engine) 
              oldEngine.removePropertyChangeListener(this); // remove listener from old instance 
        }
    }
    public boolean hasEngine(String type) {
        return engMap.containsKey(type);
    }
    protected EngineIF getEngine(String type) {
        return (EngineIF) engMap.get(type);
    }
    public String getResultsString(String type) {
        EngineIF engine = getEngine(type);
        if (engine != null) {
          return engine.getResultsString();
        }
        else {
          return getClass().getName()+" getResultsString(), unknown engine type: " + type;
        }
    }
}
