package org.trinet.util.velocitymodel;
public interface UniformFlatLayerVelocityModelIF extends VelocityModelIF {
    int getLayerCount() ;
    double [] getVelocities() ;
    double [] getBoundaryDepths() ;
    double getVelocity(int layerNumber) ;
    double getBoundaryDepth(int layerNumber) ;
    double getPSRatio() ;
    void setPSRatio(double psRatio) ;
}

