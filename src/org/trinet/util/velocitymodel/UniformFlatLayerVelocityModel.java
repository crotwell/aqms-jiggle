package org.trinet.util.velocitymodel;
import java.util.*;
import org.trinet.util.*;
import org.trinet.util.gazetteer.*;

public class UniformFlatLayerVelocityModel implements UniformFlatLayerVelocityModelIF {
    private static UniformFlatLayerVelocityModel DEFAULT = null; // added 2008/03/19 -aww
    public static UniformFlatLayerVelocityModel SOCAL_DEFAULT = null;
    public static UniformFlatLayerVelocityModel NOCAL_DEFAULT = null;
    public static UniformFlatLayerVelocityModel TWO_LAYER_DEFAULT = null;

    protected ArrayList layerList = null;
    protected double psRatio = 1.78;
    protected String name = null;

    class ConstantVelocityLayer {
         double depth;
         double velocity;
         ConstantVelocityLayer(double depth, double velocity) {
             this.depth = depth;
             this.velocity = velocity;
         }
         public boolean equals(Object obj) {
             ConstantVelocityLayer layer =  (ConstantVelocityLayer) obj; 
             return (layer.depth == depth && layer.velocity == velocity);
         }
    }

    //static { createDefaultModels(); } // causes null pointer exception -aww

    protected UniformFlatLayerVelocityModel(String name, Collection layerList) {
        this.layerList = new ArrayList(layerList);
    }
    protected UniformFlatLayerVelocityModel(String name, double [] depths, double [] velocities, double psRatio) {
        this(name, depths.length, depths, velocities, psRatio);
    }
    protected UniformFlatLayerVelocityModel(String name, int layerCount, double [] depths, double [] velocities, double psRatio) {
        if (layerCount > depths.length || layerCount > velocities.length)
            throw new IllegalArgumentException("layerCount > length of input velocity/depth array.");
        layerList = new ArrayList(layerCount);
        for (int idx = 0; idx < layerCount; idx++) {
            layerList.add(new ConstantVelocityLayer(depths[idx],velocities[idx]));
        }
        this.name = name;
        this.psRatio = psRatio;
    }

    public static final void createDefaultModels() {
      if (SOCAL_DEFAULT == null)
          SOCAL_DEFAULT = new HK_SoCalVelocityModel();
      if (NOCAL_DEFAULT == null)
          NOCAL_DEFAULT = new USGS_NC_VelocityModel();
      if (TWO_LAYER_DEFAULT == null)
          TWO_LAYER_DEFAULT = new UniformFlatLayerVelocityModel("2LAYER", new double [] {0., 25.0}, new double [] {6., 8.}, 1.78);
    }

    public double [] getVelocities() {
        if (layerList == null) return null;
        int size = layerList.size();
        double [] velocities = new double[size];
        if (size == 0) return velocities;
        for (int idx = 0; idx < size; idx++) {
            velocities[idx] = ((ConstantVelocityLayer) layerList.get(idx)).velocity;
        }
        return velocities;
    }

    public double [] getBoundaryDepths() {
        if (layerList == null) return null;
        int size = layerList.size();
        double [] depths = new double[size];
        if (size == 0) return depths;
        for (int idx = 0; idx < size; idx++) {
            depths[idx] = ((ConstantVelocityLayer) layerList.get(idx)).depth;
        }
        return depths;
    }

    public double getVelocityAt(Geoidal geoidal) {
        return getVelocityAt(geoidal.getLat(), geoidal.getLon(), geoidal.getZ()) ;
    }
    public double getVelocityAt(double lat, double lon, double z) {
        return getVelocityAt(z);
    }
    public double getVelocityAt(double z) {
        double velocity = 0.d;
        int nLayers = layerList.size();
        if ( nLayers ==  0) return velocity;
        for (int idx = 0; idx < nLayers; idx++) {
            ConstantVelocityLayer cvl = (ConstantVelocityLayer) layerList.get(idx);
            if (z < cvl.depth) break;
            velocity = cvl.velocity;
        }
        return velocity;
    }

    public double getVelocity(int layerNumber) {
        if (layerNumber < 1 || layerNumber > layerList.size())
            throw new IllegalArgumentException("layerNumber > layers in model");
        return ((ConstantVelocityLayer) layerList.get(layerNumber - 1)).velocity;
    }

    public double getBoundaryDepth(int layerNumber) {
        if (layerNumber < 1 || layerNumber > layerList.size())
            throw new IllegalArgumentException("layerNumber > layers in model");
        return ((ConstantVelocityLayer) layerList.get(layerNumber - 1)).depth;
    }

    public int getLayerCount() {
        return layerList.size();
    }

    public double getPSRatio() {
        return psRatio;
    }

    public void setPSRatio(double psRatio) {
        this.psRatio = psRatio;
    }
    public String toString() {
        StringBuffer sb = new StringBuffer(128);
        sb.append("Model name: ").append(name).append("\n");
        sb.append("P/S velocity ratio: ").append(psRatio).append("\n");
        int count = layerList.size();
        if (count == 0) {
          sb.append("No Layers defined!");
          return sb.toString();
        }

        sb.append("Depth Velocity (at layer top)\n");
        List aList = (List) layerList;
        Format ff = new Format("%5.2f");
        ConstantVelocityLayer cvl = null;
        for (int idx = 0; idx < count; idx++) {
          cvl = (ConstantVelocityLayer) aList.get(idx);
          sb.append(ff.form(cvl.depth)).append("   ");
          sb.append(ff.form(cvl.velocity)).append("\n");
        }
        return sb.toString();
    }

    public String getName() {
        return name;
    }

    /** Does not change class default model , if input property not defined. */
    public static void setDefaultModelFromProps(GenericPropertyList gpl) {
        UniformFlatLayerVelocityModel model = getDefaultModelFromProps(gpl);
        if (model != null) DEFAULT = model;
    }

    /** Return class default model. */
    public static UniformFlatLayerVelocityModel getDefaultModel() {
        return DEFAULT;
    }

    /** Set class default model. */
    public static void setDefaultModel(UniformFlatLayerVelocityModel model) {
        DEFAULT = model;
    }

    /** Return instance of default model. */
    public static UniformFlatLayerVelocityModel getDefaultModelFromProps(GenericPropertyList gpl) {
        return getNamedModelFromProps(gpl.getProperty("velocityModel.DEFAULT.modelName"), gpl);
    }

    /** Return instance of named model. */
    public static UniformFlatLayerVelocityModel getNamedModelFromProps(String modelName, GenericPropertyList gpl) {

        if (modelName == null || gpl == null) return null;

        double psR = gpl.getDouble("velocityModel."+modelName+".psRatio");
        if (Double.isNaN(psR)) psR = 1.78;

        if (psR < 1.5)  //  || psR > 2.5)   removed upper limit -aww 2009/04/21
            throw new IllegalArgumentException("Invalid Velocity model property parsed psRatio : " + psR);

        double [] depths = gpl.getDoubleArray("velocityModel."+modelName+".depths");
        int layerCount = depths.length;

        double [] velocities = gpl.getDoubleArray("velocityModel."+modelName+".velocities");
        if (layerCount != velocities.length)
            throw new IllegalArgumentException("Invalid Velocity model property value parsed depths " +layerCount+ " !=  velocities " +velocities.length);

        return (layerCount > 0) ? new UniformFlatLayerVelocityModel(modelName, layerCount, depths, velocities, psR) : null;
    }

    /** Set model properties. */
    public void toNamedModelProps(GenericPropertyList gpl) {
        if (gpl == null || this.name == null) return;

        gpl.setProperty("velocityModel."+name+".psRatio", psRatio);

        double [] depths = new double [layerList.size()];
        double [] velocities = new double [layerList.size()];
        ConstantVelocityLayer layer = null;

        for (int idx = 0; idx < layerList.size(); idx++) {
            ConstantVelocityLayer cvl = (ConstantVelocityLayer) layerList.get(idx);
            depths[idx] =  cvl.depth;
            velocities[idx] = cvl.velocity;  // bugfix 2012/10/30 -aww was depth
        }
        gpl.setProperty("velocityModel."+name+".depths", depths);
        gpl.setProperty("velocityModel."+name+".velocities", velocities);
    }

    public boolean equals(Object obj) {
        if (! (obj instanceof UniformFlatLayerVelocityModel)) return false;
        UniformFlatLayerVelocityModel model = (UniformFlatLayerVelocityModel) obj;
        if (model.psRatio != this.psRatio) return false;
        if (! model.getName().equals(this.name)) return false;
        if (model.layerList.size() != this.layerList.size()) return false;
        for (int idx = 0; idx < this.layerList.size(); idx++) {
            if (! this.layerList.get(idx).equals(model.layerList.get(idx))) return false;
        }
        return true;
    }

}// end of class UniformFlatLayerVelocityModel 
