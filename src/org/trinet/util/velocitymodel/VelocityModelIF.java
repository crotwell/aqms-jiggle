package org.trinet.util.velocitymodel;
import org.trinet.util.gazetteer.*;

public interface VelocityModelIF {
    double getVelocityAt(double lat, double lon, double z);
    double getVelocityAt(Geoidal geoidal);
    public String getName();
}

