package org.trinet.util.velocitymodel;

/** Modified Cusp CALNET model used to locate NC earthquakes. */
public class Cusp_NoCalVelocityModel extends UniformFlatLayerVelocityModel {
    public static final double [] BOUNDARY_DEPTHS   = {0., 3.5, 15., 25.0 };
    public static final double [] LAYER_VELOCITIES  = {4., 5.9, 6.8, 8.05 };
    public static final double DEFAULT_PS_RATIO = 1.78;

    public Cusp_NoCalVelocityModel()  {
        super("CUSP_NOCAL", new double [] {0., 3.5, 15., 25.0 }, new double [] {4., 5.9, 6.8, 8.05 }, 1.78);
        //super("CUSP_NOCAL", BOUNDARY_DEPTHS, LAYER_VELOCITIES, DEFAULT_PS_RATIO);
    }

}
