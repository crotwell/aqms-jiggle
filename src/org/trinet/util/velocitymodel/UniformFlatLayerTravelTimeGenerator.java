package org.trinet.util.velocitymodel;
import org.trinet.util.gazetteer.*;

public abstract class UniformFlatLayerTravelTimeGenerator implements PrimarySecondaryWaveTravelTimeGeneratorIF {
    protected Geoidal source;
    protected UniformFlatLayerVelocityModel velocityModel;

    public void setModel(VelocityModelIF velocityModel) {
        this.velocityModel = (UniformFlatLayerVelocityModel) velocityModel;
    }

    public VelocityModelIF getModel() {
        return velocityModel;
    }

    public Geoidal getSource() {
        return source;
    }

    public void setSource(GeoidalLatLonZ source) {
        setSource((Geoidal)source.getLatLonZ());
    }

    public void setSource(Geoidal source) {
        this.source = source;
    }

    public void setSource(double lat, double lon, double z) {
        setSource(new LatLonZ(lat, lon, z));
    }

    public abstract double pTravelTime(double range) ;

    public double pTravelTime(double lat, double lon) {
        return pTravelTime(GeoidalConvert.horizontalDistanceKmBetween(source.getLat(), source.getLon(), lat, lon));
    }

    public double pTravelTime(Geoidal receiver) {
        return pTravelTime(receiver.getLat(), receiver.getLon()) ;
    }

    public double sTravelTime(double range) {
        return ((UniformFlatLayerVelocityModel) getModel()).getPSRatio()*pTravelTime(range);
    }

    public double sTravelTime(double lat, double lon) {
        return ((UniformFlatLayerVelocityModel) getModel()).getPSRatio()*pTravelTime(lat, lon);
    }

    public double sTravelTime(Geoidal receiver) {
        return sTravelTime(receiver.getLat(), receiver.getLon());
    }

    public String toString() {
        StringBuffer sb = new StringBuffer(256);
        sb.append("Source: ");
        sb.append(((source != null) ? source.toString() : "null"));
        sb.append(" Model: ");
        sb.append(((velocityModel != null) ? velocityModel.toString() : "null"));
        return sb.toString();
    }

} // end of class UniformFlatLayerTravelTimeGenerator 
