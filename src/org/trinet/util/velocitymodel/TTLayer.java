package org.trinet.util.velocitymodel;
import org.trinet.util.gazetteer.*;

/**
* Derived from TTLAYR J.P. Eaton's FORTRAN algoritim for computing traveltime and derivatives
* for a uniform flat layer crustal model.
* The source depth is in km with a positive value down. Receiver depth is ALWAYS
* considered to be 0.0km.
* Class main(String []) parses the input line a source depth
* for which to calculate traveltime to ranges of 0 to 250 km.<p>
* The main method uses the HadleyKanamori Southern California flat layer velocity model.
* The ranges and calculated travel times are output to System.out.
*/
public class TTLayer extends UniformFlatLayerTravelTimeGenerator implements java.io.Serializable {
    //private static boolean debug = false;
    private static final int FIRST_LAYER_IDX = 0;

// value derived from input
    private double range;
    private double minHeadWaveTravelTime = 99999.99;
    private double maxCriticalAngleRange;

// derived values calculated for return
    private double calcTT;
    private double dTdZ;
    private double dTdR;
    private double incAngleDeg;

// buffer loop index bounds
    private int nLayers;
    private int maxLayerIdx;
    private int srcLayerIdx;
    private int layerBelowSrcIdx;
    private int fastestLayerIdx;

// source depths (down is positive)
    private double srcZ;
    private double srcLayerZ;
    private double srcLayerZSquared;

// buffers for calculations
    private double []   srcZCorrLayerUpDownPathTime;
    private double []   srcZCorrLayerUpDownHorizDist;

    private double []   layerHeadWaveTravelTime;
    private double []   vSquared;
    private double []   thickness;
    private double [][] pathLegsThruLayer;
    private double [][] layerUpDownPathTime;
    private double [][] layerUpDownHorizDist;

// set by constructor input parameters
    private double [] velocities;
    private double [] depths;

    public TTLayer(UniformFlatLayerVelocityModelIF vm) {
        initModel(vm);  // must initModel first
    }

    public TTLayer(UniformFlatLayerVelocityModelIF vm, Geoidal source) {
        initModel(vm);  // order dependent, must initModel first
        setSource(source);
    }

    private synchronized void initModel(UniformFlatLayerVelocityModelIF vm) {
        super.setModel(vm);
        velocities = vm.getVelocities();
        depths = vm.getBoundaryDepths();
        initModelCalculationBuffers();
    }

    public void setModel(VelocityModelIF vm) {
        initModel((UniformFlatLayerVelocityModelIF) vm);
        if (getSource() != null) initCalcDataForSource();
    }

    private void createModelCalculationBuffers() {
        this.nLayers = ((UniformFlatLayerVelocityModelIF) getModel()).getLayerCount();
        this.maxLayerIdx = nLayers - 1;
        srcZCorrLayerUpDownPathTime = new double [nLayers];
        srcZCorrLayerUpDownHorizDist = new double [nLayers];
        layerHeadWaveTravelTime = new double [nLayers];
        vSquared = new double [nLayers];
        layerUpDownPathTime = new double [nLayers][nLayers];
        layerUpDownHorizDist  = new double [nLayers][nLayers];
        thickness = new double [nLayers];
        pathLegsThruLayer = new double [nLayers][nLayers]; 
    }

    public void setSource(Geoidal source) {
        if (this.source != null && this.source.equals(source)) return; // already set
        this.source = source;
        if (getModel() == null) throw new NullPointerException("VelocityModel null, invoke setVelocity() first.");
        initCalcDataForSource();
    }

    private synchronized void initCalcDataForSource() {
        srcZ = Math.abs(getSource().getZ());    // positive depth downwards into model
        srcLayerIdx = getSourceLayerIndex();

        srcLayerZ = srcZ - depths[srcLayerIdx]; // depth of source in source layer: srcLayerZ 
        srcLayerZSquared = srcLayerZ*srcLayerZ + 0.000001;

        layerBelowSrcIdx = srcLayerIdx;
        if (srcLayerIdx <  maxLayerIdx) {      // srcZ < maxLayerZ: get maxCriticalAngleRange, minRefractTime, minRefractDist
            layerBelowSrcIdx++;
            if (getModel() == null) throw new NullPointerException("VelocityModel must be set not null before setting source.");
            initSourceDepthCorrectedUpDownPathData();
        }
    }

    private int getSourceLayerIndex() {
        int index = nLayers - 1;
        for (int idx = 0; idx < nLayers; idx++) {
            if (depths[idx] > srcZ) {
                index = idx-1;
                break;
            }
        }
        return index;
    }

    private void initSourceDepthCorrectedUpDownPathData() {
        for (int idx = layerBelowSrcIdx; idx < nLayers; idx++) {
                double velocityDiffSqrt = Math.sqrt(vSquared[idx] - vSquared[srcLayerIdx]);
                srcZCorrLayerUpDownPathTime[idx] =
                    layerUpDownPathTime[srcLayerIdx][idx] - srcLayerZ*velocityDiffSqrt/(velocities[idx]*velocities[srcLayerIdx]);
                srcZCorrLayerUpDownHorizDist[idx] =
                    layerUpDownHorizDist[srcLayerIdx][idx] - srcLayerZ*velocities[srcLayerIdx]/velocityDiffSqrt;
        }
        maxCriticalAngleRange = ( velocities[layerBelowSrcIdx] * velocities[srcLayerIdx] ) *
                 ( srcZCorrLayerUpDownPathTime[layerBelowSrcIdx] - layerUpDownPathTime[srcLayerIdx][srcLayerIdx] ) /
                 ( velocities[layerBelowSrcIdx] - velocities[srcLayerIdx] ) ;
    }

//  Direct wave if focus in last layer find smallest possible refracted arrival time 
    private void calculateResults(double distanceFromSource) {
        this.range = distanceFromSource;
        fastestLayerIdx = findFastestHeadWaveLayerIndex(range);
        //String mode = null;
        if ( (srcLayerIdx == maxLayerIdx) || (range < maxCriticalAngleRange)) {
            if (srcLayerIdx == FIRST_LAYER_IDX) {
                    doFirstLayerSrc();
                    //mode = "1stLayerSrc";
            }
            else {
                    doDirectWave();
                    //mode = "doDirect";
            }
        }
        else {
            setHeadWaveResults(fastestLayerIdx);
            //mode = "setHeadWave";
        }
        //System.out.println("TTLayer Mode: "+mode+" range: "+range+" HeadIdx: "+fastestLayerIdx+" srcZ:"+srcZ);
        return;
    }

    private int findFastestHeadWaveLayerIndex(double range) {
        for (int idx = layerBelowSrcIdx; idx < nLayers; idx++) {
            layerHeadWaveTravelTime[idx]  =  srcZCorrLayerUpDownPathTime[idx] + range/velocities[idx];
        }

        int fastestLayerIdx = 0;
        minHeadWaveTravelTime = 99999.99;
        for ( int idx = layerBelowSrcIdx; idx < nLayers; idx++) {
            if ( (layerHeadWaveTravelTime[idx] > minHeadWaveTravelTime) || (srcZCorrLayerUpDownHorizDist[idx] > range) ) continue;
            fastestLayerIdx = idx;
            minHeadWaveTravelTime = layerHeadWaveTravelTime[idx];
        }
        return fastestLayerIdx;
    }

//  Calculation for a direct wave from source located in the first layer 
    private void doFirstLayerSrc() {
        double dist = Math.sqrt(srcZ*srcZ + range*range);
        double directTT = dist/velocities[FIRST_LAYER_IDX];
        if (directTT >= minHeadWaveTravelTime) {
            setHeadWaveResults(fastestLayerIdx);   // set results here
        }
        calcTT = directTT; // set result here 
        dTdR = range/(velocities[FIRST_LAYER_IDX] * dist);
        dTdZ = srcZ/(velocities[FIRST_LAYER_IDX] * dist);
        incAngleDeg = toAngleOfIncidenceDegrees(range/dist);
    }

//  Find a direct wave that WILL emerge at the current range
    private void doDirectWave() {
        double XBIG = range;
        double XLIT = range * srcLayerZ/srcZ;
        double UB = XBIG/Math.sqrt(XBIG*XBIG + srcLayerZSquared);
        double UL = XLIT/Math.sqrt(XLIT*XLIT + srcLayerZSquared);

        double deltaBig = srcLayerZ * UB/Math.sqrt(1.000001 - UB*UB);
        double deltaSmall = srcLayerZ * UL/Math.sqrt(1.000001 - UL*UL);

        for (int idx = 0; idx < srcLayerIdx; idx++) {
            deltaBig   +=  (thickness[idx] * UB)/Math.sqrt(vSquared[srcLayerIdx]/vSquared[idx] - UB*UB);
            deltaSmall +=  (thickness[idx] * UL)/Math.sqrt(vSquared[srcLayerIdx]/vSquared[idx] - UL*UL);
        }

        double sineOfAngle = extrapolateSineOfAngleForDirectWave(XBIG, XLIT, deltaBig, deltaSmall);
        if (1.0 - sineOfAngle > 0.0002) {
            calcDirectWaveResults(sineOfAngle);
            return;
        }

        //  If sineOfAngle is close to 1.0, compute traveltime for head wave along the top of layer srcLayerIdx
        double tt = layerUpDownPathTime[srcLayerIdx][srcLayerIdx] + range/velocities[srcLayerIdx];

        if ( (srcLayerIdx == maxLayerIdx) || (tt < minHeadWaveTravelTime) ) { // results for head wave at top of src layer
            calcTT = tt;
            dTdR = 1.0/velocities[srcLayerIdx];
            dTdZ = 0.;
            incAngleDeg = toAngleOfIncidenceDegrees(0.9999999);
        }
        else {
            setHeadWaveResults(fastestLayerIdx);  // use fastest head wave traveltime
        }
        return; 
    }

    private double extrapolateSineOfAngleForDirectWave(double XBIG, double XLIT, double deltaBig, double deltaSmall) {
            double XTR = 0.;
            double sineOfAngle = 0.;
            for (int iteration = 1; iteration <= 26; iteration++) {

                if ( (iteration > 10) && ( (1.0 - sineOfAngle) < 0.0002) ) {
                     return sineOfAngle;
                }
                if (deltaBig - deltaSmall < 0.02) break;

                XTR = XLIT + (range - deltaSmall) * (XBIG - XLIT)/(deltaBig - deltaSmall);
                sineOfAngle = XTR/Math.sqrt(XTR*XTR + srcLayerZSquared);

                double estRange = srcLayerZ * sineOfAngle/Math.sqrt(1.000001 - sineOfAngle*sineOfAngle);
                for (int idx = 0; idx < srcLayerIdx; idx++) {
                    estRange +=
                           (thickness[idx]*sineOfAngle)/Math.sqrt(vSquared[srcLayerIdx]/vSquared[idx] - sineOfAngle*sineOfAngle);
                }

                double rangeErr  = range - estRange;
                if (Math.abs(rangeErr) <= 0.02) {
                    return sineOfAngle;
                }

                if (rangeErr < 0.) {
                    XBIG = XTR;
                    deltaBig = estRange;
                }
                else {
                    XLIT = XTR;
                    deltaSmall = estRange;
                }
            } // end of iteration loop

            XTR = 0.5 * (XBIG + XLIT);
            sineOfAngle = XTR/Math.sqrt(XTR*XTR + srcLayerZSquared);
            return sineOfAngle;
    }

//  Travel time & derivatives for direct wave below first layer
    private void calcDirectWaveResults(double sineOfAngle) {
        double directTT = srcLayerZ/(velocities[srcLayerIdx] * Math.sqrt(1.0 - sineOfAngle*sineOfAngle));
        for (int idx = 0; idx < srcLayerIdx; idx ++) {
            directTT += (thickness[idx] * velocities[srcLayerIdx]) /
                        (vSquared[idx] * Math.sqrt(vSquared[srcLayerIdx]/vSquared[idx] - sineOfAngle*sineOfAngle) );
        }
        if ( (srcLayerIdx == maxLayerIdx) || (directTT < minHeadWaveTravelTime) ) {
            double SRR = Math.sqrt(1. - sineOfAngle*sineOfAngle);
            double SRT = SRR * SRR * SRR;
            double ALPHA = srcLayerZ/SRT;
            double BETA = srcLayerZ * sineOfAngle/(velocities[srcLayerIdx] * SRT);

            for (int idx = 0; idx < srcLayerIdx; idx++) {
                double STK = Math.sqrt(vSquared[srcLayerIdx]/vSquared[idx] - sineOfAngle*sineOfAngle);
                STK = STK * STK * STK;
                double VTK = thickness[idx] / ( vSquared[idx] * STK );
                ALPHA += VTK * vSquared[srcLayerIdx];
                BETA += VTK * velocities[srcLayerIdx] * sineOfAngle;
            }

            calcTT = directTT;
            dTdR = BETA/ALPHA;
            dTdZ = (1.0 - velocities[srcLayerIdx] * sineOfAngle * dTdR)/(velocities[srcLayerIdx] * SRR);
            incAngleDeg = toAngleOfIncidenceDegrees(sineOfAngle);
        }
        else {
            setHeadWaveResults(fastestLayerIdx);
        }
        return;
    }


    private void setHeadWaveResults(int layerIdx) {
        calcTT = layerHeadWaveTravelTime[layerIdx];
        dTdR = 1.0/velocities[layerIdx];
        dTdZ =  - Math.sqrt(vSquared[layerIdx] - vSquared[srcLayerIdx])/(velocities[layerIdx] * velocities[srcLayerIdx]);
        incAngleDeg = toAngleOfIncidenceDegrees(- velocities[srcLayerIdx]/velocities[layerIdx]);
    }

    private double toAngleOfIncidenceDegrees(double sineOfAngle) {
        if ( Math.abs(sineOfAngle) > 1.0 ) sineOfAngle = 1.0;
        double angleDeg = Math.toDegrees(Math.asin(sineOfAngle)); // * 57.29578
        if (angleDeg < 0.) angleDeg += 180.;
        angleDeg = 180. - angleDeg;
        return angleDeg;
    }

/**
*  Setup travel time matrices, matrices are layerUpDownPathTime[srcIdx][headIdx] AND layerUpDownHorizDist[srcIdx][headIdx].
*  Source is at top of srcIdx, ray is critically refracted at top of headIdx (with velocity=velocities[headIdx]
*  layerUpDownPathTime is travel time for that path, layerUpDownHorizDist is surface distance traveled from source along path.
*/
    private void initModelCalculationBuffers() {

        createModelCalculationBuffers();

        // Cache velocities squared for calculations
        for (int idx = 0; idx < nLayers; idx++) {
            vSquared[idx] = velocities[idx] * velocities[idx];
        }

        // Save layer thicknesses for calculations
        int maxIdx = nLayers - 1;
        for (int idx = 0; idx < maxIdx; idx++) {
            thickness[idx] = depths[idx+1] - depths[idx];
        }

        initModelHeadWaveUpDownPathDataBuffers() ;
    }

    // Compute layerUpDownPathTime and layerUpDownHorizDist for src at top of source layer=srcIdx
    private void initModelHeadWaveUpDownPathDataBuffers() {
        // For each src idx set the up-down path leg contribution 
        for (int srcIdx = 0; srcIdx < nLayers; srcIdx++) {
            // Ray refracted along underlying layer identified by headIdx
            for (int headIdx = 0; headIdx < nLayers; headIdx++) {
                pathLegsThruLayer[srcIdx][headIdx] = 1.0;                        // headIdx <  srcIdx up leg only
                if (headIdx >= srcIdx) pathLegsThruLayer[srcIdx][headIdx] = 2.0; // headIdx >= srcIdx up & down legs
            }
        }

        // Zero-out traveltime and distance calculation buffers for current velocity model.
        for (int srcIdx = 0; srcIdx < nLayers; srcIdx++) {
            for (int headIdx = 0; headIdx < nLayers; headIdx++) {
                layerUpDownPathTime[srcIdx][headIdx] = 0.;
                layerUpDownHorizDist[srcIdx][headIdx] = 0.;
            }
        }

        // Src layer reference index
        for (int srcIdx = 0; srcIdx < nLayers; srcIdx++) {
            HeadWaveLayerLoop:
            for (int headIdx = srcIdx; headIdx < nLayers; headIdx++) {  // do underlying layer headwaves
                if (headIdx == 0) continue HeadWaveLayerLoop;
                int lastIdx = headIdx; 
                for (int idx = 0; idx < lastIdx; idx++) {  // calc tt and dist from src thru layers above refracting layer.
                    double velocityDiffSqrt = Math.sqrt(vSquared[headIdx] - vSquared[idx]);
                    double time = thickness[idx] * velocityDiffSqrt/(velocities[idx] * velocities[headIdx]);
                    double dist = thickness[idx] * velocities[idx]/velocityDiffSqrt;
                    layerUpDownPathTime[srcIdx][headIdx] += pathLegsThruLayer[srcIdx][idx] * time;
                    layerUpDownHorizDist[srcIdx][headIdx] += pathLegsThruLayer[srcIdx][idx] * dist;
                } // end of lastIdx loop
            } // end of headIdx inner loop
        } // end of outer srcIdx loop

    } // end of method

    /** Return travel-time (secs) through this model for a receiver at distance
     * 'range' in km. Note that adjustment for receiver elevation is not possible. */
    public double pTravelTime(double range) {
        calculateResults(range);
        return calcTT;
    }

    public double getDtDz() { return dTdZ; }
    public double getDtDr() { return dTdR; }

    public double getSlowness() { return dTdR; }
    public double getAngleOfIncidence() { return incAngleDeg; }

    public static void outputSoCalTable( double startRangeKm,    double rangeInc, int rangeElements,
                                    double startSrcDepthKm, double depthInc, int depthElements) {
        UniformFlatLayerVelocityModel.createDefaultModels();
        outputTable( UniformFlatLayerVelocityModel.SOCAL_DEFAULT,
                     startRangeKm,    rangeInc, rangeElements,
                     startSrcDepthKm, depthInc, depthElements) ;
    }

    public static void outputTable(UniformFlatLayerVelocityModelIF vm,
                               double startRangeKm,    double rangeInc, int rangeElements,
                               double startSrcDepthKm, double depthInc, int depthElements) {
        TTLayer ttlayer = new TTLayer(vm);
        ttlayer.outputTable(startRangeKm, rangeInc, rangeElements,
                            startSrcDepthKm, depthInc, depthElements);
    }

    public static void outputSTable(UniformFlatLayerVelocityModelIF vm,
                               double startRangeKm,    double rangeInc, int rangeElements,
                               double startSrcDepthKm, double depthInc, int depthElements, double vpvs) {
        TTLayer ttlayer = new TTLayer(vm);
        ttlayer.outputTable(startRangeKm, rangeInc, rangeElements,
                            startSrcDepthKm, depthInc, depthElements, vpvs);
    }

    protected void outputTable(double startRangeKm, double rangeInc, int rangeElements,
              double startSrcDepthKm, double depthInc, int depthElements) {
        outputTable(startRangeKm, rangeInc, rangeElements, startSrcDepthKm, depthInc, depthElements, 1.);
    }

    protected void outputTable(double startRangeKm, double rangeInc, int rangeElements,
              double startSrcDepthKm, double depthInc, int depthElements, double vpvs) {
        double [] depthKm = new double[depthElements];
        double depth = startSrcDepthKm;
        for (int depthIdx = 0; depthIdx < depthElements; depthIdx++) {
            depthKm[depthIdx] = depth;
            depth += depthInc;
        }
        StringBuffer depthStringBuffer = new StringBuffer(132);
        depthStringBuffer.append("  V  ");
        Concat cc  = new Concat();
        for (int depthIdx = 0; depthIdx < depthElements; depthIdx++) {
            cc.format(depthStringBuffer,depthKm[depthIdx], 3, 2);
//          depthStringBuffer.append(" ");
        }
        double dist = startRangeKm;
        double [] range = new double [rangeElements];
        for (int rangeIdx = 0; rangeIdx < rangeElements; rangeIdx++) {
             range[rangeIdx] = dist;
             dist += rangeInc;
        }
        
        double [][] tt = new double [depthElements][rangeElements];
        for (int depthIdx = 0; depthIdx < depthElements; depthIdx++) {
            this.setSource(new LatLonZ(0.,0., depthKm[depthIdx]));
            for (int rangeIdx = 0; rangeIdx < rangeElements; rangeIdx++) {
                tt[depthIdx][rangeIdx] = ( vpvs > 1. ) ? vpvs * pTravelTime(range[rangeIdx]) : pTravelTime(range[rangeIdx]);
            }
        }
        //if (! debug) {
            System.out.println("Range");
            System.out.println("  |  Depth ->");
            System.out.println(depthStringBuffer.toString());
            for (int rangeIdx = 0; rangeIdx < rangeElements; rangeIdx++) {
                StringBuffer sb = new StringBuffer(132);
//                cc.format(sb, (long) range[rangeIdx],3);
                cc.format(sb, range[rangeIdx], 3, 1);
                for (int depthIdx = 0; depthIdx < depthElements; depthIdx++) {
                    cc.format(sb,tt[depthIdx][rangeIdx],3,2);
//                  sb.append(" ");
                }
                System.out.println(sb.toString());
            }
        //}
    }

//
    public static void main(String [] args) {
        UniformFlatLayerVelocityModel.createDefaultModels();
        TTLayer ttlayer = new TTLayer(UniformFlatLayerVelocityModel.SOCAL_DEFAULT);
        double depthKm = 0.;
        double vpvs = 1.;
        int argCnt = args.length;
        if (argCnt > 0) {
            depthKm = Double.parseDouble(args[0]);
            if (argCnt > 1) {
              vpvs = Double.parseDouble(args[1]);
            }
            ttlayer.outputTable(0., 5., 101, depthKm, 0., 1, vpvs);
        }
        else {
            ttlayer.outputTable(0., 5., 101, 0., 2., 18);
        }
        //LatLonZ receiver = new LatLonZ(34.2, -121., 0.);
        //range = GeoidalConvert.horizontalDistanceKmBetween(ttlayer.getSource(), receiver);
        //tt = ttlayer.pTravelTime(receiver);
        //System.out.println("LatLonZ Range: " + range + " time: " + tt);
    }
//

} // end of class
