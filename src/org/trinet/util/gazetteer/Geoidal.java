package org.trinet.util.gazetteer;
public interface Geoidal extends GeoidalLatLonZ {
    double getLat();
    double getLon();
    double getZ();
    Geoidal getGeoidal();
    GeoidalUnits getZUnits();
    void setZUnits(GeoidalUnits units);
    //boolean hasLatLonZ();
    //LatLonZ getLatLonZ();
    // maintain directional convention in coda base
    // or you will need to add methods like:
    //boolean positiveDepths()
    //boolean positiveElevs() // return ! positiveDepths
    //void setZType(int mode) // is it a ELEV +up, or DEPTH +down axis
}

