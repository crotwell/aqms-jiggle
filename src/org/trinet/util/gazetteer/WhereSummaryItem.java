package org.trinet.util.gazetteer;
/** Class holds the summary data representing distance, azimuth, and elevation to a gazetteer reference and its description.
* The equals(...), compareTo(...) methods are provided for distance comparison to enable sorting capability.
* fromWhereString(...) returns the distance, azimuth and elevation as string data concatenated with the place description .
* Includes methods to have distances reported as kilometers or miles units.
*/
public class WhereSummaryItem implements WhereSummary {
    DistanceAzimuthElevation distAzElev;
    String fromString;
    String remark;

    public WhereSummaryItem()
    {
    }

    public WhereSummaryItem(WhereItem wi) {
        this.distAzElev = wi.getDistanceAzimuthElevation();
        this.fromString = wi.fromPlaceString(false);
        this.remark = wi.getRemark();
    }

    public WhereSummaryItem(double distance, double azimuth, double z, String fromString, String remark) {
        this.distAzElev = DistanceAzimuthElevation.create(GeoidalUnits.DEGREES);
        this.distAzElev.setDistanceAzimuthElevation(distance, azimuth, z);
        this.fromString = fromString;
        this.remark = remark;
    }

    public WhereSummaryItem(DistanceAzimuthElevation distAzElev,  String fromString, String remark) {
        this.distAzElev = distAzElev;
        this.fromString = fromString;
        this.remark = remark;
    }

    public void SetParams(DistanceAzimuthElevation distAzElev,  String fromString, String remark) {
        this.distAzElev = distAzElev;
        this.fromString = fromString;
        this.remark = remark;
    }

/*
    public void setDistanceAzimuthElevation(DistanceAzimuthElevation distAzElev) {
        this.distAzElev = distAzElev;
    }

    public void setFromString(String fromString) {
        this.fromString = fromString;
    }

*/

/** Set a remark data member to further identify this object.
*   This remark string is concatenated to returned String value for methods where input parameter includeRemark == true.
*/
    public void setRemark(String remark) {
        this.remark = remark;
    }

/** Returns the horizontal datum distance to the target from this gazetteer location, in the specified distance units.
*/
    public double getDistance(GeoidalUnits distanceUnits) {
        return distAzElev.getDistance(distanceUnits);
    }

/** Returns the horizontal datum distance to the target from this gazetteer location, in the default units.
*/
    public double getDistance() {
        return distAzElev.getDistance(null);
    }

/** Returns the DistanceAzimuthElevation of the target relative to this gazetteer location.
*/
    public DistanceAzimuthElevation getDistanceAzimuthElevation() {
        return distAzElev;
    }

/** Uses distance comparision */
    public boolean equals(Object object) {
        if (object == null) return false;
        try {
            if (((WhereSummaryItem) object).getDistance() == this.getDistance()) return true;
        }
        catch (ClassCastException ex) {
        }
        return false;
    }

/** Uses distance comparision */
    public int compareTo(Object object) throws ClassCastException {
        if (! (object instanceof WhereSummaryItem)) throw new ClassCastException ("WhereSummaryItem compareto()");
        if (((WhereSummaryItem) object).getDistance() == this.getDistance()) return 0;
        else if (((WhereSummaryItem) object).getDistance() >= this.getDistance()) return -1;
        else return 1;
    }

    public String fromPlaceString(boolean includeRemark) {
        if (! includeRemark) return fromString;
        StringBuffer sb = new StringBuffer(256);
        sb.append(fromString).append(fromPlaceRemarkString());
        return sb.toString();
    }

/** Returns String reporting distance, azimuth, elevation to gazetteer placename.
*/
    public String fromWhereString(GeoidalUnits distanceUnits, boolean includeRemark) {
        StringBuffer sb = new StringBuffer(256);
        sb.append(distAzElev.toLabeledStringWithUnits(distanceUnits));
        sb.append(" from "); // aww 06/15/2006
        sb.append(fromPlaceString(includeRemark));
        return sb.toString();
    }

/** Convenience wrapper of fromWhereString(...). */
    public String fromWhereStringKm(boolean includeRemark) {
        StringBuffer sb = new StringBuffer(256);
        sb.append(distAzElev.toLabeledStringWithKm());
        sb.append(" from "); // aww 06/15/2006
        sb.append(fromPlaceString(includeRemark));
        return sb.toString();
    }

    public String fromWhereStringMiles(boolean includeRemark) {
        StringBuffer sb = new StringBuffer(256);
        sb.append(distAzElev.toLabeledStringWithMiles());
        sb.append(" from "); // aww 06/15/2006
        sb.append(fromPlaceString(includeRemark));
        return sb.toString();
    }

/** Returns the REMARK column string found in the database table row object.
*/
    private String fromPlaceRemarkString() {
        if ( (remark == null) || remark.equalsIgnoreCase("NULL") ) return "";
        StringBuffer sb = new StringBuffer(256);
        sb.append(" ").append(remark.trim());
        return sb.toString();
    }

/** String resulting from calling toString() on data member objects.
* Uses default distance units.
*/
    public String toString() {
        StringBuffer sb = new StringBuffer(256);
        sb.append(distAzElev.toString()).append(" ");
        sb.append(fromString).append( " ").append(remark);
        return sb.toString();
    }

/** String resulting from calling toString() on data member objects.
* Uses specified distance units.
*/
    public String toString(GeoidalUnits distanceUnits) {
        StringBuffer sb = new StringBuffer(256);
        sb.append(distAzElev.toString(distanceUnits)).append(" ");
        sb.append(fromString).append(" ").append(remark);
        return sb.toString();
    }
}
