package org.trinet.util.gazetteer;
public interface WhereSummary {
    public double getDistance(GeoidalUnits gu);
    public DistanceAzimuthElevation getDistanceAzimuthElevation();
    public String fromPlaceString(boolean includeRemark);
    public String fromWhereStringKm(boolean includeRemark);
    public String fromWhereStringMiles(boolean includeRemark);
    public String fromWhereString(GeoidalUnits gu, boolean includeRemark);
}
