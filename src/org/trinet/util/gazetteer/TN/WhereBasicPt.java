package org.trinet.util.gazetteer.TN;

import java.sql.*;
import java.util.*;

import org.trinet.jasi.JasiDatabasePropertyList;
import org.trinet.util.gazetteer.*;

public class WhereBasicPt extends WhereAmI {

    protected static long typeId = 0l;
    //protected static final String SQL_QUERY_STRING = "SELECT p.lat,p.lon,p.z,p.name,p.state FROM GAZETTEERPT p WHERE p.TYPE=?";
    protected static final String SQL_QUERY_STRING = "SELECT lat, lon, z, name, state FROM GAZETTEERPT WHERE TYPE=?";

/** Default constructor does not set a reference point or a database connection */
    public WhereBasicPt() {
        super();
    }
/** Constructor sets the database connection, does not set a reference point. */
    public WhereBasicPt(Connection conn) {
        super(conn);
    }
/** Constructor sets the database connection and reference point. */
    public WhereBasicPt(Connection conn, Geoidal reference) {
        super(conn, reference);
    }
/** Constructor sets the reference point, does not set a database connection. */
    public WhereBasicPt(Geoidal reference) {
        super(reference);
    }
/** Constructor sets the reference point, does not set a database connection. 
* Elevation/depth input parameter is assumed to be in km units.
*/
    public WhereBasicPt(double lat, double lon, double z) {
        super(lat, lon, z);
    }
    
    public long getTypeId() {
        return typeId ;
    }

    public void setTypeId(long id) {
        typeId = id;
    }

    protected Vector getDatabaseData() {
        return getDatabaseData(SQL_QUERY_STRING, typeId);
    }

    protected Vector getDatabaseData(String sql, long typeId) {
        if (items != null) return items; // don't requery database just return cached data
    
        Vector items = new Vector(256);
        ResultSet rs = null;
        PreparedStatement sm = null;
        try {
          if (JasiDatabasePropertyList.debugSQL) System.out.println(sql + " typeId: " + typeId);
          sm =  getConnection().prepareStatement(sql);
          if (sm != null) {
            sm.setLong(1, typeId);
            rs = sm.executeQuery(); 
            while (rs.next()) {
              BasicGazetteerPt bgp = new BasicGazetteerPt();
              bgp.lat = rs.getDouble("LAT");  
              bgp.lon = rs.getDouble("LON");  
              bgp.z = rs.getDouble("Z");  
              bgp.name = rs.getString("NAME");    
              bgp.state = rs.getString("STATE");  
              if (rs.wasNull()) bgp.state = "";
              //bgp.remark = rs.getString("REMARK");    // not a column in gazetteerpt table, in outer join table like gazetteertown
              //if (rs.wasNull()) bgp.remark = "NULL";
              WhereItem whereItem = new WhereItemBasicPt(reference, bgp);
              items.addElement(whereItem);
            }
          }
        }
        catch (SQLException ex) {
            ex.printStackTrace();
            //return null;
        }
        finally {
          try {
            if (rs != null) rs.close();
            if (sm != null) sm.close();
          }
          catch (SQLException ex) {}
        }

        return items;
    }

    /*
    //Returns Vector of WhereItems constructed using gazetteer database data for generic type
    protected Vector getDatabaseData(String sql) {
        if (items != null) return items; // don't requery database just return cached data
    
        Vector items = new Vector(256);
        ResultSet rs = null;
        Statement sm = null;
        try {
          sm =  getConnection().createStatement();
          if (JasiDatabasePropertyList.debugSQL) System.out.println(sql);
          if (sm != null) {
            rs = sm.executeQuery(sql); 
            while (rs.next()) {
              BasicGazetteerPt bgp = new BasicGazetteerPt();
              bgp.lat = rs.getDouble("LAT");  
              bgp.lon = rs.getDouble("LON");  
              bgp.z = rs.getDouble("Z");  
              bgp.name = rs.getString("NAME");    
              bgp.state = rs.getString("STATE");  
              if (rs.wasNull()) bgp.state = "";
              bgp.remark = rs.getString("REMARK");    
              if (rs.wasNull()) bgp.remark = "NULL";
              WhereItem whereItem = new WhereItemBasicPt(reference, bgp);
              items.addElement(whereItem);
            }
          }
        }
        catch (SQLException ex) {
            ex.printStackTrace();
            //return null;
        }
        finally {
          try {
            if (rs != null) rs.close();
            if (sm != null) sm.close();
          }
          catch (SQLException ex) {}
        }
        return items;
    }
    */
}
