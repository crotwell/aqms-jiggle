package org.trinet.util.gazetteer.TN;
import org.trinet.util.gazetteer.*;
public interface MapLocation {
    double getLat();
    double getLon();
    MapLocation getMapLocation();
}

