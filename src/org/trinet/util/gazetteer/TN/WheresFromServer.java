// 
// NOTE: PLSQL functions are mapped to methods in this class any changes in
// method signature requires editing the PLSQL stored procedure and recompilation 
// on the database servers !
//
package org.trinet.util.gazetteer.TN;

import java.sql.*;

import org.trinet.jiggle.common.DbConstant;
import org.trinet.util.gazetteer.*;

import org.trinet.jdbc.datasources.*;
import org.trinet.jasi.*;
import org.postgresql.pljava.annotation.Function;

// NOTE can't extend WheresFrom, static class methods can't override instance IF methods
// Thus this class provides static wrappers around a static engine instance.
public class WheresFromServer {

    // delegate to type we need to create concrete instance of engine class -aww
    // for previous old code use this:
    //private static WheresFrom wf = new WheresFrom();

    // new code use this:
    private static WheresFrom wf = (WheresFrom) WhereIsEngine.create();
    static {
        try {
          if (wf.getConnection() == null || wf.getConnection().isClosed()) {
              // JDBConn.createInternalDbServerConnection();
              Connection conn = DataSource.createDefaultDataSource().createInternalDbServerConnection();
              GazetteerType.init(conn);
          }
        }
        catch (SQLException ex) { ex.printStackTrace(); }
    }

//Method invoked by plsql functions
    public static void setDistanceUnits(GeoidalUnits units) {
        wf.setDistanceUnits(units);
    }

//NAME 'org.trinet.util.gazetteer.TN.WheresFromServer.setDistanceUnitsKm()';
    @Function(schema=DbConstant.SCHEMA_WHERES, name="wf_du_km")
    public static void setDistanceUnitsKm() {
        setDistanceUnits(GeoidalUnits.KILOMETERS);
    }

//NAME 'org.trinet.util.gazetteer.TN.WheresFromServer.setDistanceUnitsMiles()';
    @Function(schema=DbConstant.SCHEMA_WHERES, name="wf_du_mi")
    public static void setDistanceUnitsMiles() {
        setDistanceUnits(GeoidalUnits.MILES);
    }

    @Function(schema=DbConstant.SCHEMA_WHERES, name="km_to_fault")
    public static double kmToFault(double lat, double lon, String name) {
        return wf.kmToFault(lat, lon, name);
    }

/** Returns String describing where the specified location is relative to closest
database entry for all known database types. */
//NAME 'org.trinet.util.gazetteer.TN.WheresFromServer.where(double, double) return java.lang.String';
    @Function(schema=DbConstant.SCHEMA_WHERES, name="locale")
    public static String where(double lat, double lon) {
        return wf.where(lat, lon);
    }

/** Returns String describing where the specified location is relative to input
number of database entries for all known database types. Uses z to determine elevation.
*/
//NAME 'org.trinet.util.gazetteer.TN.WheresFromServer.where(double, double, double, int) return java.lang.String';
    @Function(schema=DbConstant.SCHEMA_WHERES, name="locale")
    public static String where(double lat, double lon, double z, int number) {
        return wf.where(lat, lon, z, number);
    }

/** Returns String describing where the specified location (int degrees and decimal minutes)
is relative to the input number of database entries for all known types.  Uses z to determine elevation.
*/
//NAME 'org.trinet.util.gazetteer.TN.WheresFromServer.where(int, double, int, double, double, int) return java.lang.String';
    @Function(schema=DbConstant.SCHEMA_WHERES, name="locale")
    public static String where(int lat, double latmin, int lon, double lonmin, double  z, int number) {
      return wf.where(GeoidalConvert.toDecimalDegrees(lat, latmin), GeoidalConvert.toDecimalDegrees(lon, lonmin),
        z, number);
    }

//NAME 'org.trinet.util.gazetteer.TN.WheresFromServer.whereType(double, double, double, int, java.lang.String) return java.lang.String';
    /** Returns String describing where the specified location is relative to the input number of database entries
     * for the input database type and uses z to determine elevation.
     */
    @Function(schema= DbConstant.SCHEMA_WHERES, name="where_from_type")
    public static String whereType(double lat, double lon, double z, int number, String type) {
        return wf.whereType(lat, lon, z, number, type);
    }

//NAME 'org.trinet.util.gazetteer.TN.WheresFromServer.getClosestTown
    public static final void getClosestPoint(String name, double lat, double lon, double z,
         double [] dist, double [] az, double [] elev, String [] place) {
        wf.getClosestPoint(name, lat, lon, z, dist, az, elev, place);
    }

    //NAME 'org.trinet.util.gazetteer.TN.WheresFromServer.getClosestTown - used by PL/Java
    @Function(schema= DbConstant.SCHEMA_WHERES, name="town", type="RECORD")
    public static final boolean getClosestTown(double lat, double lon, double z, ResultSet out) {
        wf.getClosestTown(lat, lon, z, out);
        return true;
    }

    //NAME 'org.trinet.util.gazetteer.TN.WheresFromServer.getClosestTown
    public static final void getClosestTown(double lat, double lon, double z,
         double [] dist, double [] az, double [] elev, String [] place) {
        wf.getClosestTown(lat, lon, z, dist, az, elev, place);
    }

    //NAME 'org.trinet.util.gazetteer.TN.WheresFromServer.getClosestBigTown - used by PL/Java
    @Function(schema= DbConstant.SCHEMA_WHERES, name="bigtown", type="RECORD")
    public static final boolean getClosestBigTown(double lat, double lon, double z, ResultSet out) {
        wf.getClosestBigTown(lat, lon, z, out);
        return true;
    }

    //NAME 'org.trinet.util.gazetteer.TN.WheresFromServer.getClosestBigTown
    public static final void getClosestBigTown(double lat, double lon, double z,
         double [] dist, double [] az, double [] elev, String [] place) {
        wf.getClosestBigTown(lat, lon, z, dist, az, elev, place);
    }

    //NAME 'org.trinet.util.gazetteer.TN.WheresFromServer.getClosestEqPOI - used by PL/Java
    @Function(schema=DbConstant.SCHEMA_WHERES, name="eqpoi", type="RECORD")
    public static final boolean getClosestEqPOI(double lat, double lon, double z, ResultSet out) {
        wf.getClosestEqPOI(lat, lon, z, out);
        return true;
    }

    //NAME 'org.trinet.util.gazetteer.TN.WheresFromServer.getClosestEqPOI
    public static final void getClosestEqPOI(double lat, double lon, double z,
                                             double [] dist, double [] az, double [] elev, String [] place) {
        wf.getClosestEqPOI(lat, lon, z, dist, az, elev, place);
    }

    //NAME 'org.trinet.util.gazetteer.TN.WheresFromServer.getClosestQuarry - used by PL/Java
    @Function(schema=DbConstant.SCHEMA_WHERES, name="quarry", type="RECORD")
    public static final boolean getClosestQuarry(double lat, double lon, double z, ResultSet out) {
        wf.getClosestQuarry(lat, lon, z, out);
        return true;
    }

    //NAME 'org.trinet.util.gazetteer.TN.WheresFromServer.getClosestQuarry
    public static final void getClosestQuarry(double lat, double lon, double z,
                                              double [] dist, double [] az, double [] elev, String [] place) {
        wf.getClosestQuarry(lat, lon, z, dist, az, elev, place);
    }

    //NAME 'org.trinet.util.gazetteer.TN.WheresFromServer.getClosestQuake - used by PL/Java
    @Function(schema=DbConstant.SCHEMA_WHERES, name="quake", type="RECORD")
    public static final boolean getClosestQuake(double lat, double lon, double z, ResultSet out) {
        wf.getClosestQuake(lat, lon, z, out);
        return true;
    }

    //NAME 'org.trinet.util.gazetteer.TN.WheresFromServer.getClosestQuake
    public static final void getClosestQuake(double lat, double lon, double z,
                                             double [] dist, double [] az, double [] elev, String [] place) {
        wf.getClosestQuake(lat, lon, z, dist, az, elev, place);
    }

    //NAME 'org.trinet.util.gazetteer.TN.WheresFromServer.getClosestStation - used by PL/Java
    @Function(schema=DbConstant.SCHEMA_WHERES, name="station", type="RECORD")
    public static final boolean getClosestStation(double lat, double lon, double z, ResultSet out) {
        wf.getClosestStation(lat, lon, z, out);
        return true;
    }

    //NAME 'org.trinet.util.gazetteer.TN.WheresFromServer.getClosestStation
    public static final void getClosestStation(double lat, double lon, double z,
                                               double [] dist, double [] az, double [] elev, String [] place) {
        wf.getClosestStation(lat, lon, z, dist, az, elev, place);
    }

    //NAME 'org.trinet.util.gazetteer.TN.WheresFromServer.getClosestFault - used by PL/Java
    @Function(schema=DbConstant.SCHEMA_WHERES, name="fault", type="RECORD")
    public static final boolean getClosestFault(double lat, double lon, double z, ResultSet out) {
        wf.getClosestFault(lat, lon, z, out);
        return true;
    }

    //NAME 'org.trinet.util.gazetteer.TN.WheresFromServer.getClosestFault
    public static final void getClosestFault(double lat, double lon, double z,
                                             double [] dist, double [] az, double [] elev, String [] place) {
        wf.getClosestFault(lat, lon, z, dist, az, elev, place);
    }

    /*
    public static class Tester {
      public static void main(String [] args) {
         double [] dist = new double[1];
         double [] az = new double[1];
         double [] elev = new double[1];
         String [] place = new String[1];
         JasiDatabasePropertyList.debugSQL = true;
         System.err.println("TESTER: Creating default data source");
         DataSource ds = TestDataSource.create("host","db","user","pw");
         WheresFrom.CONNECTION = ds.getConnection();
         System.err.println("TESTER: Created default data source");
         WheresFromServer.getClosestTown(32., -116., 0., dist, az, elev, place);
         System.out.println("TESTER: Town: " + place[0]);
         WheresFromServer.getClosestTown(33., -117., 0., dist, az, elev, place);
         System.out.println("TESTER: new Town: " + place[0]);
         System.out.println(WheresFromServer.where(33., -118.));
         //System.out.println(WheresFromServer.where(33, 0., -118, 0., 1., 1));
         System.out.println(WheresFromServer.whereType(35.7, -117.7, 1., 1, "bigtown"));
         System.out.println(WheresFromServer.whereType(35.7, -117.7, 1., 1, "town"));
         System.out.println(WheresFromServer.whereType(35.7, -117.7, 1., 1, "station"));
         System.out.println(WheresFromServer.whereType(35.7, -117.7, 1., 1, "quarry"));
         System.out.println(WheresFromServer.whereType(35.7, -117.7, 1., 1, "quake"));
         System.out.println(WheresFromServer.whereType(35.7, -117.7, 1., 1, "fault"));
         System.out.println(WheresFromServer.whereType(35.7, -117.7, 1., 1, "volcano"));
         WheresFromServer.getClosestPoint("town", 35.7, -117.7, 1., dist, az, elev, place);
         System.out.println("getClosestPoint town place: " + place[0]);
         System.out.println(GazetteerType.dumpName2CodeMap());
         System.out.println(GazetteerType.dumpCode2NameMap());
         System.out.println("GazetteerType volcano code: " + GazetteerType.getCode("volcano"));
      }
    } // end of Tester
    */
}
