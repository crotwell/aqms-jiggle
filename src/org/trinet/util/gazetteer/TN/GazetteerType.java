package org.trinet.util.gazetteer.TN;
import java.sql.*;
import java.util.HashMap;
import org.trinet.util.gazetteer.*;
import org.trinet.jasi.DataSource;
import org.trinet.jasi.JasiDatabasePropertyList;

public class GazetteerType {

    // Note SQL implementation of round(code, -1) is used to group bigtown,town so city subtype gazetteer code must be between 10 and 19
    public static final String TOWN = "town";         // 10
    public static final String BIG_TOWN = "bigTown";  // 12
    public static final String QUARRY = "quarry";     // 30
    public static final String QUAKE = "quake";       // 40
    public static final String VOLCANO = "volcano";   // 60
    public static final String EQ_POI = "eq_poi";     // 70
    public static final String PLACE = "place";       // 500
    public static final String STATION = "station";   // 501
    public static final String LANDFORM = "landform"; // 600
    public static final String FAULT = "fault";       // 601

    private static final HashMap codeMap = new HashMap(11);
    private static final HashMap nameMap = new HashMap(11);

    static {

        nameMap.put(TOWN, new Long(10l));
        nameMap.put(BIG_TOWN, new Long(12l));
        nameMap.put(QUARRY, new Long(30l));
        nameMap.put(QUAKE, new Long(40l));
        nameMap.put(VOLCANO, new Long(60l));
        nameMap.put(EQ_POI, new Long(70l));
        nameMap.put(STATION, new Long(501l));
        nameMap.put(LANDFORM, new Long(600l));
        nameMap.put(FAULT, new Long(601l));

        nameMap.put(new Long(10l), TOWN);
        nameMap.put(new Long(12l), BIG_TOWN);
        nameMap.put(new Long(30l), QUARRY);
        nameMap.put(new Long(40l), QUAKE);
        nameMap.put(new Long(60l), VOLCANO);
        nameMap.put(new Long(70l), EQ_POI);
        nameMap.put(new Long(501l), STATION);
        nameMap.put(new Long(600l), LANDFORM);
        nameMap.put(new Long(601l), FAULT);

        init(null);
    }

    public static long getCode(String name) {
        Long code = (Long) nameMap.get(name);
        return (code == null) ? 0l : code.longValue();
    }

    public static String getName(long code) {
        String name = (String) codeMap.get(new Long(code));
        return name;
    }

    public static String dumpName2CodeMap() {
        return nameMap.toString();
    }

    public static String dumpCode2NameMap() {
        return nameMap.toString();
    }

    public String toString() {
        StringBuffer sb = new StringBuffer(1024);
        sb.append(nameMap.toString());
        sb.append("\n");
        sb.append(codeMap.toString());
        return sb.toString();
    }

    protected static void init(Connection dbConn) {

        ResultSet rs = null;
        PreparedStatement sm = null;

        try {

          String sql = "select code, name from gazetteertype";
          if (JasiDatabasePropertyList.debugSQL) System.out.println(sql);
          Connection conn = (dbConn == null) ? DataSource.getConnection() : dbConn;
          if (conn == null || conn.isClosed()) {
              //System.err.println("GazetteerType init failed - No valid Db connection");
              return;
          }
          sm =  conn.prepareStatement(sql);
          if (sm != null) {
            rs = sm.executeQuery(); 
            String name = null;
            Long code = null;
            nameMap.clear();
            codeMap.clear();
            while (rs.next()) {
                code = new Long(rs.getLong(1)); 
                name = rs.getString(2);
                codeMap.put(code, name);
                nameMap.put(name, code);
            }
          }
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        }
        finally {
          try {
            if (rs != null) rs.close();
            if (sm != null) sm.close();
          }
          catch (SQLException ex) {}
        }
    }
 
}
