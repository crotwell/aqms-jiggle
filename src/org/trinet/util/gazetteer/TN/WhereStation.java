package org.trinet.util.gazetteer.TN;
import java.sql.*;
import java.util.*;

import org.trinet.jasi.EnvironmentInfo;
import org.trinet.jasi.JasiDatabasePropertyList;
import org.trinet.util.gazetteer.*;

public class WhereStation extends WhereAmI {
    private static String STATION_DATA_TABLE = "JASI_STATION_VIEW";
    //static {
        //STATION_DATA_TABLE = (EnvironmentInfo.getNetworkCode().equals("CI")) ? "SIS_STATION_DATA" : "STATION_DATA";
    //}

    { prettyName = "station"; }

/** Default constructor does not set a reference point or a database connection */
    public WhereStation() {
        super();
    }
/** Constructor sets the database connection, does not set a reference point. */
    public WhereStation(Connection conn) {
        super(conn);
    }
/** Constructor sets the database connection and reference point. */
    public WhereStation(Connection conn, Geoidal reference) {
        super(conn, reference);
    }
/** Constructor sets the reference point, does not set a database connection. */
    public WhereStation(Geoidal reference) {
        super(reference);
    }
/** Constructor sets the reference point, does not set a database connection. 
* Elevation/depth input parameter is assumed to be in km units.
*/
    public WhereStation(double lat, double lon, double z) {
        super(lat, lon, z);
    }

/** Returns Vector of WhereItems constructed using station database data. */
    protected Vector getDatabaseData() {
        String sql = "SELECT STA,STANAME,LAT,LON,ELEV FROM " + STATION_DATA_TABLE +
           " WHERE ONDATE<=SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) AND (OFFDATE>SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) OR OFFDATE IS NULL)";
        Vector items = new Vector(2048);
        Statement sm = null;
        ResultSet rs = null;
        try {
            sm = getConnection().createStatement();
            if (JasiDatabasePropertyList.debugSQL) System.out.println(sql);
            rs = sm.executeQuery(sql); 
            while (rs.next()) {
                BasicGazetteerPt bgp = new BasicGazetteerPt();
                bgp.name = rs.getString("STA") + " station";
                bgp.lat = rs.getDouble("LAT");        
                bgp.lon = rs.getDouble("LON");        
                bgp.z = rs.getDouble("ELEV") / 1000.;        
                bgp.state = "";
                StringBuffer sb = new StringBuffer(24);
                sb.append(" (").append(rs.getString("STANAME")).append(")");
                bgp.remark = sb.toString();
                WhereItem whereItem = new WhereItemBasicPt(bgp);
                whereItem.setDistanceAzimuthElevation(reference);
                items.addElement(whereItem);
            }
        }
        catch (SQLException ex) {
            ex.printStackTrace();
            //return null;
        }
        finally {
          try {
            if (rs != null) rs.close();
            if (sm != null) sm.close();
          }
          catch (SQLException ex) {}
        }
        return items;
    }
}
