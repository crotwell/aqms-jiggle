package org.trinet.util.gazetteer.TN;
import org.trinet.util.gazetteer.*;
import java.sql.*;
import java.util.*;
/** Polymorphic type extension of WhereBasicPt */
public class WhereBasicTown extends WhereBasicPt {
    { prettyName = "town"; }

    public WhereBasicTown() {
        super();
    }
    public WhereBasicTown(Connection conn) {
        super(conn);
    }
    public WhereBasicTown(Connection conn, Geoidal reference) {
        super(conn, reference);
    }
    public WhereBasicTown(Geoidal reference) {
        super(reference);
    }
    public WhereBasicTown(double lat, double lon, double z) {
        super(lat, lon, z);
    }

/** Returns Vector of WhereItems constructed using gazetteer database data for town type. */
    protected Vector getDatabaseData() {
        return super.getDatabaseData(
                    "SELECT lat,lon,z,name,state FROM GAZETTEERPT WHERE ROUND(type,-1)=?",
                    GazetteerType.getCode(GazetteerType.TOWN)
                );
    }
}
