package org.trinet.util.gazetteer.TN;
import org.trinet.util.gazetteer.*;
import java.sql.*;
import java.util.*;
/** Polymorphic type extension of WhereBasicPt */
public class WhereBasicQuake extends WhereBasicPt {
    { prettyName = "quake"; }

    public WhereBasicQuake() {
        super();
    }
    public WhereBasicQuake(Connection conn) {
        super(conn);
    }
    public WhereBasicQuake(Connection conn, Geoidal reference) {
        super(conn, reference);
    }
    public WhereBasicQuake(Geoidal reference) {
        super(reference);
    }
    public WhereBasicQuake(double lat, double lon, double z) {
        super(lat, lon, z);
    }

/** Returns Vector of WhereItems constructed using gazetteer database data for quake type. */
    protected Vector getDatabaseData() {
        return super.getDatabaseData(SQL_QUERY_STRING, GazetteerType.getCode(GazetteerType.QUAKE)); 
    }
}
