package org.trinet.util.gazetteer.TN;
import org.trinet.util.gazetteer.*;
import java.sql.*;
import java.util.*;
public class WhereIsClosestQuarry extends WhereIsClosest {

    public WhereIsClosestQuarry() {
    }
    public WhereIsClosestQuarry(Connection conn) {
	super(conn);
    }
    public WhereIsClosestQuarry(Connection conn, Geoidal reference) {
	super(conn, reference);
    }
    public WhereIsClosestQuarry(Geoidal reference) {
	super(reference);
    }
    public WhereIsClosestQuarry(double lat, double lon, double z) {
	super(lat, lon, z);
    }

/** Return Vector of WhereSummary for closest town constructed using gazetteer database data for town type. */
    protected Vector getDatabaseData() {
	String sql = "{ call WHERES.QUARRY(?,?,?,?,?,?,?) }";
	return super.getDatabaseData(sql);
    }
}
