package org.trinet.util.gazetteer.TN;
import org.trinet.util.gazetteer.*;
import java.sql.*;
import java.util.*;
/** Polymorphic type extension of WhereBasicPt */
public class WhereBasicQuarry extends WhereBasicPt {
    { prettyName = "quarry"; }

    public WhereBasicQuarry() {
        super();
    }
    public WhereBasicQuarry(Connection conn) {
        super(conn);
    }
    public WhereBasicQuarry(Connection conn, Geoidal reference) {
        super(conn, reference);
    }
    public WhereBasicQuarry(Geoidal reference) {
        super(reference);
    }
    public WhereBasicQuarry(double lat, double lon, double z) {
        super(lat, lon, z);
    }

/** Returns Vector of WhereItems constructed using gazetteer database data for quarry type. */
    protected Vector getDatabaseData() {
        return super.getDatabaseData(SQL_QUERY_STRING, GazetteerType.getCode(GazetteerType.QUARRY)); 
    }
}
