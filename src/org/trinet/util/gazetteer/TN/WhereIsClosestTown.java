package org.trinet.util.gazetteer.TN;
import org.trinet.util.gazetteer.*;
import java.sql.*;
import java.util.*;
public class WhereIsClosestTown extends WhereIsClosest {

    public WhereIsClosestTown() {
    }
    public WhereIsClosestTown(Connection conn) {
	super(conn);
    }
    public WhereIsClosestTown(Connection conn, Geoidal reference) {
	super(conn, reference);
    }
    public WhereIsClosestTown(Geoidal reference) {
	super(reference);
    }
    public WhereIsClosestTown(double lat, double lon, double z) {
	super(lat, lon, z);
    }

/** Return Vector of WhereSummary for closest town constructed using gazetteer database data for town type. */
    protected Vector getDatabaseData() {
	String sql = "{ call WHERES.TOWN(?,?,?,?,?,?,?) }";
	return super.getDatabaseData(sql);
    }
}
