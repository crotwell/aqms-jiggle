package org.trinet.util.gazetteer.TN;
import org.trinet.util.gazetteer.*;
import java.sql.*;
import java.util.*;
public class WhereBasicBigTown extends WhereBasicPt {
    { prettyName = "town"; }

    public WhereBasicBigTown() {
        super();
    }
    public WhereBasicBigTown(Connection conn) {
        super(conn);
    }
    public WhereBasicBigTown(Connection conn, Geoidal reference) {
        super(conn, reference);
    }
    public WhereBasicBigTown(Geoidal reference) {
        super(reference);
    }
    public WhereBasicBigTown(double lat, double lon, double z) {
        super(lat, lon, z);
    }

/** Returns Vector of WhereItems constructed using gazetteer database data for big town type. */
    protected Vector getDatabaseData() {
        return super.getDatabaseData(SQL_QUERY_STRING, GazetteerType.getCode(GazetteerType.BIG_TOWN)); 
    }
}
