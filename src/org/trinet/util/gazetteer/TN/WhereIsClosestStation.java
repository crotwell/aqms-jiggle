package org.trinet.util.gazetteer.TN;
import org.trinet.util.gazetteer.*;
import java.sql.*;
import java.util.*;
public class WhereIsClosestStation extends WhereIsClosest {

    public WhereIsClosestStation() {
    }
    public WhereIsClosestStation(Connection conn) {
	super(conn);
    }
    public WhereIsClosestStation(Connection conn, Geoidal reference) {
	super(conn, reference);
    }
    public WhereIsClosestStation(Geoidal reference) {
	super(reference);
    }
    public WhereIsClosestStation(double lat, double lon, double z) {
	super(lat, lon, z);
    }

/** Return Vector of WhereSummary for closest town constructed using gazetteer database data for town type. */
    protected Vector getDatabaseData() {
	String sql = "{ call WHERES.STATION(?,?,?,?,?,?,?) }";
	return super.getDatabaseData(sql);
    }
}
