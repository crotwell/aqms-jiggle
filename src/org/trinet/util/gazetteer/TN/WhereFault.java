package org.trinet.util.gazetteer.TN;
import java.sql.*;
import java.util.*;
import org.trinet.util.gazetteer.*;
import org.trinet.jasi.JasiDatabasePropertyList;

/** Polymorphic type extension of WhereLine */
public class WhereFault extends WhereLine {
    public WhereFault() {
        super();
    }
    public WhereFault(Connection conn) {
        super(conn);
    }
    public WhereFault(Connection conn, Geoidal reference) {
        super(conn, reference);
    }
    public WhereFault(Geoidal reference) {
        super(reference);
    }
    public WhereFault(double lat, double lon, double z) {
        super(lat, lon, z);
    }

/** Return String name of the geographic entity represented by this class */
    public String getLineTypeName() {
        return "Fault";
    }

/** Returns Vector of WhereItems constructed using gazetteer database data for town type. */
    protected Vector getDatabaseData() {
        return super.getDatabaseData( "SELECT NAME,LINE,FORMAT,POINTS FROM GAZETTEERLINE" );
    }

    // Local hashmap of blob line data for specific fault (if name changes requery);
    HashMap faultMap = new HashMap(1);
    ArrayList flineDataList = null;
    protected double kmToFault(String faultName) {
        if (faultName == null) {
            System.err.println("WhereFault: kmToFault faultName input is null");
            return Double.NaN;
        }
        if (reference == null) throw new NullPointerException("WhereFault: kmToFault null reference point object");

        LineData ld = null;
        BasicGazetteerPt bgp = null;
        flineDataList = (ArrayList) faultMap.get(faultName);
        Vector fitems = new Vector(10); // assume length is previous size

        if (flineDataList == null) {
          flineDataList = new ArrayList(5);
          // No don't do super here we need to implement all code logic here since the "cache" must be bypassed !!!!
          ResultSet rs = null;
          Statement sm = null;
          String sql = "SELECT NAME,LINE,FORMAT,POINTS FROM GAZETTEERLINE WHERE NAME='"+faultName+"'";
          try {
            faultMap.clear();
            sm = getConnection().createStatement();
            if (JasiDatabasePropertyList.debugSQL) System.out.println(sql);
            rs = sm.executeQuery(sql); 
            while (rs.next()) {
                String name = rs.getString("NAME");
                int line = rs.getInt("LINE");
                String format = rs.getString("FORMAT");
                int bytesPerPoint = getBytesPerPoint(format);
                String remark = "NULL";
                //oracle.sql.BLOB points = (oracle.sql.BLOB) rs.getObject("POINTS");
                java.sql.Blob points = (java.sql.Blob ) rs.getObject("POINTS");
                if (points == null) throw new NullPointerException("WhereFault: kmToFault null points object");
                int blobPts = ((int) points.length())/bytesPerPoint;
                //System.out.println(name + " " + line + " " + format + " " + bytesPerPoint + " " + remark + " Pts: " + blobPts);
                ld = new LineData(name, line, format, remark, blobPts);
                flineDataList.add(ld);
                bgp = getClosestPoint(ld, reference.getLat(), reference.getLon(), points);
                bgp.setName(ld.name);
                //bgp.setRemark(getLineTypeName());
                fitems.addElement(new WhereItem(reference, bgp));
            }
            ArrayList aList = (ArrayList) faultMap.put(faultName, flineDataList);
            if (aList != null) aList.clear();
          }
          catch (SQLException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
          }
          finally {
            try {
              if (rs != null) rs.close();
              if (sm != null) sm.close();
            }
            catch (SQLException ex) {}
          }
        } // end of new fault line data lookup from db
        else { // use existing fault line data
          for (int index=0; index < flineDataList.size(); index++) {
             ld = (LineData) flineDataList.get(index);
             bgp = getClosestPoint(ld);
             if (bgp == null) continue;
             bgp.setName(ld.name);
             //bgp.setRemark(getLineTypeName());
             fitems.addElement(new WhereItem(reference, bgp));
          }
        }

        if (fitems.size() == 0) return Double.MAX_VALUE;

        double [] dist = new double [fitems.size()];
        for (int index = 0; index < fitems.size(); index++) {
           dist[index] = ((WhereSummary) fitems.elementAt(index)).getDistance(distanceUnits);
           if (Double.isNaN(dist[index])) dist[index] = Double.MAX_VALUE;
        }

        int [] sortOrder = IndexSort.getSortedIndexes(dist);
        return ((WhereSummary)fitems.elementAt(sortOrder[0])).getDistance(GeoidalUnits.KILOMETERS);
    }

}
