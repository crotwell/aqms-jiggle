package org.trinet.util.gazetteer.TN;
import org.trinet.util.gazetteer.*;
import java.sql.*;
import java.util.*;
public class WhereIsClosestBigTown extends WhereIsClosest {

    public WhereIsClosestBigTown() {
    }
    public WhereIsClosestBigTown(Connection conn) {
	super(conn);
    }
    public WhereIsClosestBigTown(Connection conn, Geoidal reference) {
	super(conn, reference);
    }
    public WhereIsClosestBigTown(Geoidal reference) {
	super(reference);
    }
    public WhereIsClosestBigTown(double lat, double lon, double z) {
	super(lat, lon, z);
    }

/** Return Vector of WhereSummary for closest town constructed using gazetteer database data for town type. */
    protected Vector getDatabaseData() {
	String sql = "{ call WHERES.BIGTOWN(?,?,?,?,?,?,?) }";
	return super.getDatabaseData(sql);
    }
}
