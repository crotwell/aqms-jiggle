package org.trinet.util.gazetteer.TN;

import java.sql.SQLException;
import java.sql.Connection;
import java.sql.SQLData;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import oracle.jdbc.OracleConnection;
import oracle.jdbc.OracleTypes;
//import oracle.sql.STRUCT;
//import oracle.jpub.runtime.MutableStruct;

public class OracleRegion implements SQLData, java.io.Serializable
{
  // Oracle jdbc qualifies with user's schema if not fully qualified -aww
  public static final String _SQL_NAME = "PUBLIC.REGION"; // used to be CODE. -aww 2008/04/22
  public static final int _SQL_TYPECODE = OracleTypes.STRUCT;

  private String m_Name;
  private java.sql.Array m_Border;

  /* constructor */
  public OracleRegion()
  {
  }

  public OracleRegion(String Name, java.sql.Array Border) throws SQLException
  {
    setName(Name);
    setBorder(Border);
  }
  public void readSQL(SQLInput stream, String type)
  throws SQLException
  {
      setName(stream.readString());
      setBorder(stream.readArray());
  }

  public void writeSQL(SQLOutput stream)
  throws SQLException
  {
      stream.writeString(getName());
      stream.writeArray(getBorder());
  }

  public String getSQLTypeName() throws SQLException
  {
    return _SQL_NAME;
  }

  /* Serialization interface */
  public void restoreConnection(java.sql.Connection conn) throws SQLException
  { }
  /* accessor methods */
  public String getName()
  { return m_Name; }

  public void setName(String Name)
  { m_Name = Name; }


  public java.sql.Array getBorder()
  { return m_Border; }

  public void setBorder(java.sql.Array Border)
  { m_Border = Border; }

  public String toString()
  { try {
     return "REGION" + "(" +
       ((getName()==null)?"null": "'" + getName()+"'" ) + "," +
       getBorder() +
     ")";
    } catch (Exception e) { return e.toString(); }
  }

}
