package org.trinet.util;

public interface FilterIF {

/** Return reference to input object parameter which contains a representation
 *  of a waveform time series.
 * Object could be an array or more complex object such as a float[],
 * jasi.Waveform or jasi.WFSegment.
 * Specific filter implementations can define the types of input and output
 * objects but should check for valid Object types.
 */
    /** Return a filtered COPY of the passed object. */
    Object filter(Object wave);

    /** Create a filter of the type described by String 'type'. The valid values
     *  and meanings of 'type' is filter dependent.
     *  For example: "BANDPASS", "HIGHPASS", "LOWPASS", etc.*/
    FilterIF getFilter(String type);

    /** Create a filter of the type id corresponding to input value.*/ 
    FilterIF getFilter(int type, int sampleRate);

    /** Create a filter for timeseries of the given sample rate. This assumes
     *  that sample rate is the only significant parameter for the filter. */
    FilterIF getFilter(int sampleRate);

    /** Short string describing the filter. */
    void setDescription(String str) ;
    /** Short string describing the filter. */
    String getDescription( );

    int getType();
    void setType(int type);

    int getSampleRate();
    void setSampleRate(int rate);

    double getSps();
    
    int getOrder();
    void setOrder(int norder);

    boolean isReversed();
    void setReversed(boolean tf);
}
