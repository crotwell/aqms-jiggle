package org.trinet.util;

import org.trinet.jasi.Waveform;

public interface WaveformFilterIF extends FilterIF {
/** Return reference to input object parameter which contains a Waveform time series.
*/
    public Waveform filter(Waveform wf);
    public void copyInputWaveform(boolean tf);
    public boolean unitsAreLegal(Waveform wf);
    public boolean unitsAreLegal(int units);
}
