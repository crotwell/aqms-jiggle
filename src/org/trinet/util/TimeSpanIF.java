package org.trinet.util;
public interface TimeSpanIF {

     public boolean isNull() ;
     public boolean after(double datetime) ;
     public boolean before(double datetime) ;

     public boolean excludes(double datetime) ;
     public boolean excludes(double startDatetime, double endDatetime) ;
     public boolean excludes(TimeSpanIF timeSpan);

     public boolean contains(double startDatetime, double endDatetime) ;
     public boolean contains(double datetime) ;
     public boolean contains(TimeSpanIF timeSpan) ;

     public boolean overlaps(double startDatetime, double endDatetime) ;
     public boolean within(double startDatetime, double endDatetime) ;

     public void setStart(double datetime) ;
     public void setEnd(double datetime) ;
     public double getStart() ;
     double getEnd() ;

     public void setLimits(double startDatetime, double endDatetime) ;

     public void include(double datetime) ;

     public double getDuration();
     public double getCenter();

}
