package org.trinet.util.graphics;

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.lang.*;
import java.net.URL;
/**
 * Retrieve an image file in a general way. This is necessary for platform
 * independence. This method searches the classpath for the file.
 *
 *
 * Created: Wed May 10 08:53:05 2000
 *
 * @author Doug Given
 * @version
 */

public class IconImage  {

    protected static boolean debug = false;

    // the root path for images
    protected static final String imageRoot = "/images/";
//    static final String imageRoot = "";

    // Can't instantiate
    public IconImage() {

    }
    /**
     * Uses a Class object's ClassLoader to chase down the classpath looking for
   the resource. getClass() returns a URL object. Will also find the
   resource in the a jar file from which the class is run or that is in
   the classpath. To find the image file in a jar the name must be
   relative to the "root" of the jar.  <p>

   Note that unless the filename begins with a "/" the Package name of
   THIS class will be prepended to the filename.  <p>

Examples:<p>
filename = "/images/xyz.gif"  -> "<classpath>/images/xyz.gif"<br>
filename = "images/xyz.gif" -> "<classpath>/<package-path>/images/xyz.gif"<br>

@see: java.lang.Class.getResource() */

    public static Image getImage (String filename) {
        Image image = null;
        if (filename == null ) return image;
 //     Image image = null;
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        try {
            // Because method is static, must get an instance of THIS class
            Class c = Class.forName("org.trinet.util.graphics.IconImage");
            //        URL url =  c.getClass().getResource(filename);
            URL url =  c.getResource(imageRoot+filename);

            if (url == null ) {
              if (debug) System.out.println("URL is null.");
              return null;
            }
            else {
              if (debug) System.out.println("URL: "+url.toString());
            }
            image = toolkit.getImage( url );
        }
        catch (Exception ex) {
            return null;
        }
        return image;
    }

    public static void setDebug(boolean bDebug) {
        debug = bDebug;
    }

/*
  public static final class Tester {
    public static void main(String args[]) {
//      String file = "/images/bullseye.gif";
//      String file = "c:\\tpp\\lib\\jdk1.2.2\\v1.5\\images\\bullseye.gif";
        String userHome   = System.getProperty("JIGGLE_USER_HOMEDIR", "");
        if(userHome.equals(""))
        userHome = System.getProperty("user.home", ".");

  // get system specific file separator so we can build a path string
        String fileSep  = System.getProperty("file.separator");  // "/" on UNIX, "\" on PC's

//        String fileName =
//        userHome + fileSep + ".jiggle" +"/images/bullseye.gif";

//        String fileName = "bullseye.gif";
        String fileName = "JiggleLogo.gif";
        IconImage.debug = true;
        Image image = IconImage.getImage(fileName);

        if (image == null ) {
          System.out.println ("Image = NULL");
          System.exit(0);
        }
        else {
          System.out.println ("Image = "+fileName);
        }

        JFrame frame = new JFrame(image.toString());
        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {System.exit(0);}
        });
        frame.setIconImage(image);
        frame.pack();
        frame.setVisible(true);

  //      System.exit(0);
    }
  } // end of Tester
*/
} // IconImage
