package org.trinet.util.graphics;

import java.awt.*;
import java.awt.event.*;
import java.beans.*;
import java.io.Serializable;

import javax.swing.*;
import javax.swing.border.*;
import javax.accessibility.*;
import javax.swing.event.*;

import org.trinet.util.gazetteer.LatLonZ;
import org.trinet.util.Format; // CoreJava printf-like Format class

/**
 *  Text boxes and labels for latitude, longitude.
 */
public class LatLonChooser extends JPanel implements Serializable, ActionListener {

    private Format df9 = new Format("%9.4f");
    private Format df7 = new Format("%7.4f");
    private Format di5 = new Format("%5i");

    public static final int DEC_MODE = 0;
    public static final int DM_MODE = 1;

    // Decimal or DegMin mode
    private static int mode = DEC_MODE;

    private LatLonZ llz = new LatLonZ();

    // graphics stuff
    private JPanel mainPanel = null; // DegMin or Decimal panel
    private JPanel buttonPanel = new JPanel();

    private JLabel latLabel = new JLabel("Lat");
    private JLabel lonLabel = new JLabel("Lon");

    // radio buttons for Decimal <-> DegMin
    private JRadioButton radioButtonDM = new JRadioButton("Deg Min");
    private JRadioButton radioButtonDec = new JRadioButton("Decimal");

    private JTextField lonTextField = new JTextField();
    private JTextField latTextField = new JTextField();
    private JTextField lonDTextField = new JTextField();
    private JTextField lonMTextField = new JTextField();
    private JTextField latDTextField = new JTextField();
    private JTextField latMTextField = new JTextField();

    {
        Dimension d = new Dimension(100,20);
        latTextField.setMaximumSize(d);
        lonTextField.setMaximumSize(d);
        d = new Dimension(60,20);
        latLabel.setMaximumSize(d);
        lonLabel.setMaximumSize(d);
        latDTextField.setMaximumSize(d);
        lonDTextField.setMaximumSize(d);
        d = new Dimension(80,20);
        latMTextField.setMaximumSize(d);
        lonMTextField.setMaximumSize(d);
    }

    public LatLonChooser() {
        this(0.,0.);
    }

    public LatLonChooser(double lat, double lon) {
        this(new LatLonZ(lat, lon, 0.));
    }

    public LatLonChooser(LatLonZ llz) {
        this(llz, DEC_MODE);
    }

    public LatLonChooser (LatLonZ llz, int mode) {

        this.mode = mode;

        setLatLonZ(llz);

        try {
            jbInit();
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    public LatLonZ getLatLonZ() {
        llz = parseLatLonZ();
        return llz;
    }

    public void setLatLonZ(LatLonZ llz) {
        this.llz = llz;
        setLatLonFields(llz);
    }

    public void setLatLon(double lat, double lon) {
        setLatLonZ(new LatLonZ(lat,lon,0.));
    }
  
    private void setLatLonFields(LatLonZ llz) {
        if (llz == null) llz = new LatLonZ();
        latTextField.setText(df9.form(llz.getLat()));
        lonTextField.setText(df9.form(llz.getLon()));
  
        latDTextField.setText(di5.form(llz.getLatDeg()));
        latMTextField.setText(df7.form(llz.getLatMin()));
        lonDTextField.setText(di5.form(llz.getLonDeg()));
        lonMTextField.setText(df7.form(llz.getLonMin()));
    }
  
    private LatLonZ parseLatLonZ() {
        LatLonZ llz = new LatLonZ();
  
        double val = Double.NaN;
        double val2 = Double.NaN;
  
        if (mode == DEC_MODE) {
            val = parseTextField(latTextField);
            if (!Double.isNaN(val)) llz.setLat(val);
            val = parseTextField(lonTextField);
            if (!Double.isNaN(val)) llz.setLon(val);
        } else {
            val = parseTextField(latDTextField);
            val2 = parseTextField(latMTextField);
            if (!Double.isNaN(val) && !Double.isNaN(val2)) llz.setLat((int)val, val2);
            val = parseTextField(lonTextField);
            val2 = parseTextField(lonMTextField);
            if (!Double.isNaN(val) && !Double.isNaN(val2)) llz.setLon((int)val, val2);
        }
        return llz;
    }
  
    private double parseTextField(JTextField textField) {
        String str = textField.getText();
        double val = Double.NaN;
        try {
            val = Double.valueOf(str).doubleValue();
        } catch (NumberFormatException ex) {
            //textField.setText("Bad Value");  // illegal - recursive
            //textField.setBackground(Color.red);
        }
        return val;
    }
  
    private JPanel getDecimalPanel() {
        setLatLonFields(llz);
        JPanel decPanel = new JPanel();
        Box vbox = Box.createVerticalBox();
        //decPanel.setLayout(new GridLayout(2,1));
        Box hbox = Box.createHorizontalBox();
        hbox.add(Box.createHorizontalGlue());
        hbox.add(latLabel);
        hbox.add(latTextField);
        hbox.add(Box.createHorizontalStrut(10));
        hbox.add(lonLabel);
        hbox.add(lonTextField);
        hbox.add(Box.createHorizontalGlue());

        hbox.setMaximumSize(new Dimension(250,25));
        vbox.add(hbox);
        buttonPanel.setMaximumSize(new Dimension(250,25));
        vbox.add(buttonPanel);
        decPanel.add(vbox);

        return decPanel;
    }
  
    private JPanel getDegMinPanel() {
        setLatLonFields(llz);
        JPanel dmPanel = new JPanel();
        Box vbox = Box.createVerticalBox();
        //dmPanel.setLayout(new GridLayout(2,1));
        Box hbox = Box.createHorizontalBox();
        hbox.add(Box.createHorizontalGlue());
        hbox.add(latLabel);
        hbox.add(latDTextField);
        hbox.add(latMTextField);
        hbox.add(Box.createHorizontalStrut(10));
        hbox.add(lonLabel);
        hbox.add(lonDTextField);
        hbox.add(lonMTextField);
        hbox.add(Box.createHorizontalGlue());

        hbox.setMaximumSize(new Dimension(250,25));
        vbox.add(hbox);
        buttonPanel.setMaximumSize(new Dimension(250,25));
        vbox.add(buttonPanel);
        dmPanel.add(vbox);

        return dmPanel;
    }
  
    /** Build the component. */
    private void jbInit() throws Exception {
        setLayout(new BorderLayout());

        setMode(mode);
  
        radioButtonDM.addActionListener(this);
        radioButtonDec.addActionListener(this);

        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(radioButtonDM);
        buttonGroup.add(radioButtonDec);
  
        buttonPanel.add(radioButtonDec);
        buttonPanel.add(radioButtonDM);
        /*
        JButton testButton = new JButton("Test");
        testButton.addActionListener( new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    System.out.println("LatLonChooser : "+getLatLonZ());
                }
        });
        buttonPanel.add(testButton);
        */
  
        //this.add(buttonPanel, BorderLayout.SOUTH);
    }
  
    /** Set Decimal or DegMin mode. */
    private void setMode(int mode) {
        this.mode = mode;
        if (mode == DEC_MODE) {
            setMainPanel(getDecimalPanel());
            radioButtonDec.setSelected(true);
        } else {
            setMainPanel(getDegMinPanel());
            radioButtonDM.setSelected(true);
        }
    }
  
  /** Handle radio button events. */
    public void actionPerformed(ActionEvent e) {
        getLatLonZ();
        //System.out.println("Radio LatLonChooser : "+this.llz.toString());
        setLatLonStyleDecimal(radioButtonDec.isSelected()) ;
    }
  
    public void setLatLonStyleDecimal(boolean tf) {
        if (tf) {
            setMode(DEC_MODE);
        } else {
            setMode(DM_MODE);
        }
    }

    /** Return true if the decimal style of editing Lat/Lon is selected. */
    public boolean latLonDecimalStyleSelected() {
        return (mode == DEC_MODE) ;
    }
  
    private void setMainPanel(JPanel panel) {
        if (panel != null) {
            if (mainPanel != null) remove(mainPanel);
            mainPanel = panel;
            add(mainPanel, BorderLayout.CENTER);
            revalidate();
        }
    }
  
    /*
    public final static void main(String[] args) {
      JFrame frame = new JFrame("LatLonZChooser");
      frame.setSize(400,200);
      LatLonChooser cp = new LatLonChooser(32.75, -116.25);
      frame.getContentPane().add(cp);
      frame.setVisible(true);
    }
    */
}
