package org.trinet.util.graphics;

import java.awt.*;
import java.awt.event.*;
import java.beans.*;
import java.io.Serializable;

import javax.swing.*;
import javax.swing.border.*;
import javax.accessibility.*;
import javax.swing.event.*;

import org.trinet.util.graphics.text.JTextClipboardPopupMenu;
import org.trinet.util.gazetteer.LatLonZ;
import org.trinet.util.Format; // CoreJava printf-like Format class

/**
 *  Text boxes and labels for latitude, longitude and depth.
 */

public class LatLonZChooser extends JPanel implements Serializable, ActionListener {

  static LatLonZ defllz = new LatLonZ(33.0, -118.0, 0.0);

  LatLonZ llz = new LatLonZ(defllz);

  boolean fixLatLon = false;
  boolean fixZ = false;
  boolean bogusLLZ = false;

  Format df9 = new Format("%9.4f");
  Format df7 = new Format("%7.4f");
  Format di5 = new Format("%5i");

  public static final int DEC_MODE = 0;
  public static final int DM_MODE = 1;

// Decimal or DegMin mode
  static int mode = DM_MODE;

// graphics stuff
  private JPanel mainPanel = new JPanel();
  private GridLayout gridLayout1 = new GridLayout();
  private JTextField lonTextField = new JTextField(8);
  private JTextField depthTextField = new JTextField(8);
  private JTextField latTextField = new JTextField(8);
  private JPanel llzDecPanel = new JPanel();
  private JPanel llzDMPanel = new JPanel();
  private JLabel depthLabel = new JLabel();
  private JLabel latLabel = new JLabel();
  private JLabel lonLabel = new JLabel();
  private BorderLayout borderLayout1 = new BorderLayout();
  private BorderLayout borderLayout2 = new BorderLayout();
  private JPanel buttonPanel = new JPanel();
  private JRadioButton radioButtonDM = new JRadioButton();
  private JRadioButton radioButtonDec = new JRadioButton();

  private JTextField lonDTextField = new JTextField(4);
  private JTextField lonMTextField = new JTextField(6);
  private JTextField latDTextField = new JTextField(3);
  private JTextField latMTextField = new JTextField(6);

  MyDocListener docListener = new MyDocListener(this);
  private JPanel fixCheckBoxPanel = new JPanel();
  private JCheckBox fixZcheckBox = new JCheckBox();
  private JCheckBox fixLatLonCheckBox = new JCheckBox();
  private JCheckBox bogusCheckBox = new JCheckBox();  // added option -aww 08/04/2006

  /**
   *  Creates the panel.
   */
  public LatLonZChooser() {
    this(defllz);
  }

  public LatLonZChooser(LatLonZ llz) {
    this(llz, false, false, false, DEC_MODE);
  }

  /**
   *  Creates the panel.
   */
  public LatLonZChooser(LatLonZ llz, boolean latLonFixed, boolean depthFixed, boolean bogus, int mode) {

    fixLatLon = latLonFixed;
    fixZ = depthFixed;
    bogusLLZ = bogus;

    this.mode = mode;

    setLatLonZ(llz);

    try {
      jbInit();
    }
    catch(Exception e) {
      e.printStackTrace();
    }

  }

    /**
     *  @return the LatLonPoint represented by contents of the entry boxes
     */
    public LatLonZ getLatLonZ() {
      return llz;
    }

    public boolean getFixDepth() {
      return fixZ;
    }

    public boolean getFixLatLon() {
      return fixLatLon;
    }

    public boolean getBogus() {
      return bogusLLZ;
    }
    /**
     *  Sets the contents of the latitude and longitude entry boxes
     *  @param llpoint the object containt the coordinates that should go in the boxes
     */
    public void setLatLonZ(LatLonZ llz) {
      this.llz = llz;
      setLLZfields(llz);
    }
    public void setLatLonZ(double lat, double lon, double z) {
      llz.set(lat, lon, z);
      setLLZfields(llz);
    }

    private void setLLZfields(LatLonZ llz) {
      if (llz == null) llz = defllz;

      latTextField.setText(df9.form(llz.getLat()));
      lonTextField.setText(df9.form(llz.getLon()));
      depthTextField.setText(df9.form(llz.getZ()));

      latDTextField.setText(di5.form(llz.getLatDeg()));
      latMTextField.setText(df7.form(llz.getLatMin()));
      lonDTextField.setText(di5.form(llz.getLonDeg()));
      lonMTextField.setText(df7.form(llz.getLonMin()));

    }
    private LatLonZ parseLatLonZ() {
      LatLonZ llz = new LatLonZ();
      double val, val2 ;
      val = val2 = Double.NaN;

      if (mode == DEC_MODE) {
        val = parseTextField(latTextField);
        if (!Double.isNaN(val)) llz.setLat(val);
        val = parseTextField(lonTextField);
        if (!Double.isNaN(val)) llz.setLon(val);
        val = parseTextField(depthTextField);
        if (!Double.isNaN(val)) llz.setZ(val);
      } else {
        val = parseTextField(latDTextField);
        val2 = parseTextField(latMTextField);
        // modified second condition to be val2  01/06/06 -aww
        if (!Double.isNaN(val) && !Double.isNaN(val2)) llz.setLat((int)val, val2);
        val = parseTextField(lonDTextField);
        val2 = parseTextField(lonMTextField); // assignment missing, added line 01/06/06 -aww
        if (!Double.isNaN(val) && !Double.isNaN(val2)) llz.setLon((int)val, val2);
        val = parseTextField(depthTextField);
        if (!Double.isNaN(val)) llz.setZ(val);
      }
      return llz;
    }

    /** */
    private double parseTextField(JTextField textField) {
      String str = textField.getText();

      double val = Double.NaN;

      try {
        val = Double.valueOf(str).doubleValue();
      } catch (NumberFormatException ex) {
        //textField.setText("Bad Value");  // illegal - recursive
        //textField.setBackground(Color.red);
      }
      return val;

    }

  private JPanel getDecimalPanel() {
    llzDecPanel = new JPanel();

    gridLayout1.setColumns(2);
    gridLayout1.setRows(3);
    llzDecPanel.setLayout(gridLayout1);
    llzDecPanel.setSize(220,150);
    depthLabel.setText("Depth");
    latLabel.setText("Latitude");
    lonLabel.setText("Longitude");
    this.setLayout(borderLayout2);

    setLLZfields(getLatLonZ());

    latTextField.getDocument().addDocumentListener(docListener);
    lonTextField.getDocument().addDocumentListener(docListener);
    depthTextField.getDocument().addDocumentListener(docListener);

    llzDecPanel.add(latLabel);
    llzDecPanel.add(latTextField);
    llzDecPanel.add(lonLabel);
    llzDecPanel.add(lonTextField);
    llzDecPanel.add(depthLabel);
    llzDecPanel.add(depthTextField);

    return llzDecPanel;
  }

  private JPanel getDegMinPanel() {
    llzDMPanel = new JPanel();
    gridLayout1.setColumns(2);
    gridLayout1.setRows(3);
    llzDMPanel.setLayout(gridLayout1);
    llzDMPanel.setSize(220,150);
    depthLabel.setText("Depth");
    latLabel.setText("Latitude");
    lonLabel.setText("Longitude");
    this.setLayout(borderLayout2);

    JPanel jp1 = new JPanel();
    JPanel jp2 = new JPanel();
    JPanel jp3 = new JPanel();

    jp1.add(latDTextField);
    jp1.add(latMTextField);
    jp2.add(lonDTextField);
    jp2.add(lonMTextField);   // not really needed but makes all the same the same size
    jp3.add(depthTextField);

    setLLZfields(getLatLonZ());

    latDTextField.getDocument().addDocumentListener(docListener);
    latMTextField.getDocument().addDocumentListener(docListener);
    lonDTextField.getDocument().addDocumentListener(docListener);
    lonMTextField.getDocument().addDocumentListener(docListener);
    depthTextField.getDocument().addDocumentListener(docListener);

    llzDMPanel.add(latLabel);
    llzDMPanel.add(jp1);
    llzDMPanel.add(lonLabel);
    llzDMPanel.add(jp2);
    llzDMPanel.add(depthLabel);
    llzDMPanel.add(jp3);

    return llzDMPanel;
  }

  /** Build the component. */
  private void jbInit() throws Exception {

    new JTextClipboardPopupMenu(latTextField);
    new JTextClipboardPopupMenu(lonTextField);
    new JTextClipboardPopupMenu(depthTextField);
    new JTextClipboardPopupMenu(latDTextField);
    new JTextClipboardPopupMenu(latMTextField);
    new JTextClipboardPopupMenu(lonDTextField);
    new JTextClipboardPopupMenu(lonMTextField);

    mainPanel.setLayout(borderLayout1);

    // radio buttons for Decimal <-> DegMin
    radioButtonDM.setText("Deg Min");
    radioButtonDec.setText("Decimal");

    setMode(mode);

    radioButtonDM.addActionListener(this);
    radioButtonDec.addActionListener(this);
    ButtonGroup buttonGroup = new ButtonGroup();
    buttonGroup.add(radioButtonDM);
    buttonGroup.add(radioButtonDec);

    buttonPanel.add(radioButtonDec);
    buttonPanel.add(radioButtonDM);

    fixZcheckBox.setText("Fix Depth");
    fixZcheckBox.setSelected(getFixDepth());  // set current state
    fixZcheckBox.addActionListener(
        new java.awt.event.ActionListener() {
          public void actionPerformed(ActionEvent e) {
            fixZ = fixZcheckBox.isSelected();
          }
    });
    fixCheckBoxPanel.add(fixZcheckBox);

    fixLatLonCheckBox.setText("Fix Lat/Lon");
    fixLatLonCheckBox.setSelected(getFixLatLon());  // set current state
    fixLatLonCheckBox.addActionListener(
        new java.awt.event.ActionListener() {
          public void actionPerformed(ActionEvent e) {
            fixLatLon = fixLatLonCheckBox.isSelected();
          }
    });
    fixCheckBoxPanel.add(fixLatLonCheckBox);

    bogusCheckBox.setText("Bogus Lat/Lon");
    bogusCheckBox.setSelected(getBogus());  // set current state
    bogusCheckBox.addActionListener(
        new java.awt.event.ActionListener() {
          public void actionPerformed(ActionEvent e) {
            bogusLLZ = bogusCheckBox.isSelected();
          }
    });
    fixCheckBoxPanel.add(bogusCheckBox);

    JButton clearButton = new JButton();
    clearButton.setText("Clear");
    clearButton.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(ActionEvent e) {
            clear();
        }
    });
    fixCheckBoxPanel.add(clearButton);


    JPanel southPanel = new JPanel();
    southPanel.setLayout(new ColumnLayout());
    southPanel.add(fixCheckBoxPanel);
    southPanel.add(buttonPanel);

    this.add(southPanel, BorderLayout.SOUTH);
    this.add(mainPanel, BorderLayout.CENTER);

  }
  /** Set Decimal or DegMin mode. */
  void setMode(int mode) {
    this.mode = mode;
    if (mode == DEC_MODE) {
      setMainPanel(getDecimalPanel());
      radioButtonDec.setSelected(true);
    } else {
      setMainPanel(getDegMinPanel());
      radioButtonDM.setSelected(true);
    }
  }

  /** Handle radio button events. */
  public void actionPerformed(ActionEvent e) {
    setLatLonStyleDecimal (radioButtonDec.isSelected()) ;
  }

  public void setLatLonStyleDecimal(boolean tf) {
    if (tf) {
      setMode(DEC_MODE);
    } else {
      setMode(DM_MODE);
    }
  }
  /** Return true if the decimal style of editing Lat/Lon is selected. */
  public boolean latLonDecimalStyleSelected() {
    return (mode == DEC_MODE) ;
  }

  private void setMainPanel(JPanel panel) {
    if (panel != null) {
      this.remove(mainPanel);
      mainPanel = panel;
      this.add(mainPanel, BorderLayout.CENTER);
      validate();
    }
  }

  public void clear() {
      fixZcheckBox.setSelected(false);
      fixLatLonCheckBox.setSelected(false);
      bogusCheckBox.setSelected(true); // force bogus -aww 2011/08/15
      this.llz = new LatLonZ();
      lonTextField.setText("0");
      depthTextField.setText("0");
      latTextField.setText("0");
      lonDTextField.setText("0");
      lonMTextField.setText("0");
      latDTextField.setText("0");
      latMTextField.setText("0");
  }

  /*
  public static void main(String[] args) {
      JFrame frame = new JFrame("LatLonZChooser");
      frame.setSize(220,150);

      org.trinet.jasi.Solution sol = org.trinet.jasi.Solution.create();
      sol.setLatLonZ(32.54, -116.23, 6.34);

      LatLonZChooser cp = new LatLonZChooser(sol.getLatLonZ());
      frame.getContentPane().add(cp);
      frame.setVisible(true);
      //frame.validate();
  }
  */

  /**
   * This is better then an actionListener because it updates
   * the values as they are edited and does not require that the user
   * hit a <CR> to trigger the listener.
   */
  class MyDocListener implements DocumentListener {

      LatLonZChooser src;

      // Passes reference to the caller so we can see and change some stuff
      public MyDocListener(LatLonZChooser llzChooser)
      {
          src = llzChooser;  // need reference to see other variables
      }
       public void insertUpdate(DocumentEvent e) {
           setValues(e);
       }
       public void removeUpdate(DocumentEvent e) {
           setValues(e);
       }
       public void changedUpdate(DocumentEvent e) {
           // we won't ever get this with a PlainDocument
       }

       private void setValues(DocumentEvent e) {
           llz.copy(parseLatLonZ());
       }
    }
}

