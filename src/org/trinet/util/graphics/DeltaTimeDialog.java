package org.trinet.util.graphics;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

/** A dialog panel for selecting delta times. */
public class DeltaTimeDialog extends JDialog {

     JPanel mainPanel = new JPanel();
     DeltaTimePanel deltaPanel = new DeltaTimePanel();

     BorderLayout borderLayout1 = new BorderLayout();
     JPanel buttonPanel = new JPanel();
     JButton cancelButton = new JButton();
     JButton okButton = new JButton();

     /** Status for dialog when closed. */
     int returnValue = JOptionPane.CLOSED_OPTION;

     public DeltaTimeDialog(Frame frame, String title, boolean modal) {
          super(frame, title, modal);
          try  {
               jbInit();
               pack();
          }
          catch(Exception ex) {
               ex.printStackTrace();
          }
     }

     /** Create a modal dialog. */
     public DeltaTimeDialog() {
          this(null, "Delta Time Dialog", true);
     }

     void jbInit() throws Exception {
          mainPanel.setLayout(borderLayout1);
          cancelButton.setText("CANCEL");
          cancelButton.addActionListener(new java.awt.event.ActionListener() {

               public void actionPerformed(ActionEvent e) {
                    cancelButton_actionPerformed(e);
               }
          });
          okButton.setText("OK");
          okButton.addActionListener(new java.awt.event.ActionListener() {

               public void actionPerformed(ActionEvent e) {
                    okButton_actionPerformed(e);
               }
          });
          getContentPane().add(mainPanel);
          mainPanel.add(buttonPanel, BorderLayout.SOUTH);
          mainPanel.add(deltaPanel, BorderLayout.CENTER);

          buttonPanel.add(okButton, null);
          buttonPanel.add(cancelButton, null);

          centerDialog();
     }
/**
 * Center the dialog on the screen
 */
   public void centerDialog() {
     Dimension screenSize = this.getToolkit().getScreenSize();
	Dimension size = this.getSize();
	screenSize.height = screenSize.height/2;
	screenSize.width = screenSize.width/2;
	size.height = size.height/2;
	size.width = size.width/2;
	int y = screenSize.height - size.height;
	int x = screenSize.width - size.width;
	this.setLocation(x,y);
    }

     /** Return the results in seconds. */
     public double getSeconds () {
            return deltaPanel.getSeconds();
     }

     void okButton_actionPerformed(ActionEvent e) {

          returnValue = JOptionPane.OK_OPTION;

          this.setVisible(false);
     }

     void cancelButton_actionPerformed(ActionEvent e) {

          returnValue = JOptionPane.CANCEL_OPTION;
          this.setVisible(false);
     }

     public int getButtonStatus () {
        return returnValue;
     }

/*
     public static void main(String[] args) {

        JFrame frame = new JFrame("Delta Time Panel");

        frame.addWindowListener(new WindowAdapter()
	   {
            public void windowClosing(WindowEvent e) {System.exit(0);}
        });

        final DeltaTimeDialog dialog = new DeltaTimeDialog(null, "Choose a delta", true);

        System.out.println ("value is: "+dialog.getSeconds());
        
        frame.pack();
        frame.setVisible(true);

  }
*/
}

