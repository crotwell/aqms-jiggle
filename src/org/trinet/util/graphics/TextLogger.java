/** Sets the JTextArea to which output text information is appended.  */
package org.trinet.util.graphics;
import java.io.*;
import javax.swing.*;
import org.trinet.util.DateTime;
import org.trinet.util.graphics.text.JTextClipboardPopupMenu;
public class TextLogger {
    protected JTextArea textArea;
    private JTextClipboardPopupMenu clipBoardPopupMenu;
    protected boolean timeStamp = false;

    public TextLogger() { }
    public TextLogger(JTextArea textArea) {
        this(textArea, false);
    }
    public TextLogger(JTextArea textArea, boolean timeStamp) {
           this();
           setTextArea(textArea);
           setTimeStamp(timeStamp);
    }
    public void setTimeStamp(boolean tf) { timeStamp = tf; } 

    public void setTextArea(JTextArea textArea) {
        this.textArea = textArea;
        if (textArea != null)
          textArea.setToolTipText("Right-click for cut,paste,find functions.");
        if (clipBoardPopupMenu == null) clipBoardPopupMenu =
             new JTextClipboardPopupMenu(textArea);
    }
    public JTextArea getTextArea() {
        return textArea;
    }
    public void logText(String text) {
      logText(text, timeStamp);
    }
    public void logText(String text, boolean stamp) {
        String buffer = (stamp == true) ?
          new DateTime().toString() + " " + text : text; // for current UTC date - aww 2008/02/08
        if (textArea != null) textArea.append(buffer);
        else System.out.print(buffer);
    }
    public void logTextnl(String text) {
      logTextnl(text, timeStamp);
    }
    public void logTextnl(String text, boolean stamp) {
        logText(text + "\n", stamp);
    }
    public void clear() {
        if (textArea != null) textArea.setText("");
    }
    public void saveToFile(String fileNameString, boolean append) {
        JFileChooser fChooser = new JFileChooser();
        fChooser.setCurrentDirectory(new File(System.getProperty("user.dir")));
        fChooser.setDialogTitle( "Save text area content to file");
        if (fileNameString != null) fChooser.setSelectedFile(new File(fileNameString));
        if (JFileChooser.APPROVE_OPTION == fChooser.showSaveDialog(textArea.getTopLevelAncestor()) ) {
          fileNameString = fChooser.getCurrentDirectory().getAbsolutePath() +
              File.separator + fChooser.getName(fChooser.getSelectedFile());
        }
        else {
          System.out.println("No file name selected, canceling text area save");
          return;
        }
        BufferedWriter bw = null;
        try {
          bw = new BufferedWriter(new FileWriter(fileNameString, append));
          bw.write(textArea.getText());
          System.out.println(" Saving text area to file: "+fileNameString);
        }
        catch(IOException ex) {
          ex.printStackTrace();
        }
        finally {
          try { bw.close(); }
          catch (IOException ex2) { ex2.printStackTrace();}
        }
    }
}
