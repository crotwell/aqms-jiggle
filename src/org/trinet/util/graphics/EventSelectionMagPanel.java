package org.trinet.util.graphics;

import java.awt.*;
import java.awt.event.*;
import java.util.Enumeration;
import javax.swing.*;
import javax.swing.border.*;

import org.trinet.jasi.*;
import org.trinet.util.*;

/** A panel allows selection of various EventSelectionProperties attributes. */
public class EventSelectionMagPanel extends JPanel {

    private JTextField jtfAuth = new JTextField(8);
    private JTextField jtfSrc = new JTextField(8);
    private JTextField jtfAlgo = new JTextField(8);

    private MinMaxRangeChooser rcQual = null;
    private MinMaxRangeChooser rcSta = null;
    private MinMaxRangeChooser rcObs = null;
    private MinMaxRangeChooser rcDist = null;
    private MinMaxRangeChooser rcErr = null;
    private MinMaxRangeChooser rcGap = null;
    private MinMaxRangeChooser rcVal = null;

    private ButtonGroup bgPrefType = new ButtonGroup();
    private ButtonModel anyModel = null;
    private ButtonModel nullModel = null;

    // MAG PROPS
        //"magAuth"
        //"magSubsource"
        //"magAlgo"

        //"magQualityRange"
        //"magStaRange"
        //"magObsRange"
        //"magPrefType"
        //"magErrRange"
        //"magGapRange"
        //"magDistRange"
        //"magValueRange"

     // We will change the properties immediately, its up to the caller
     // to make sure it can recover from a cancellation.
     private EventSelectionProperties props;

     /** The EventSelectionProperties object that is passed is changed
     * immediately by user action in the panel, therefore, if the caller wants
     * to be able to undo user action and revert to the old properties (say in
     * response to clicking a [CANCEL] button in a dialog) the caller must either retain
     * a copy of the original properties or pass a copy to this class and only
     * use it when approriate (say when [OK] is clicked). */
     public EventSelectionMagPanel(EventSelectionProperties props) {
          this.props = props;
          try  {
               jbInit();
          }
          catch(Exception ex) {
               ex.printStackTrace();
          }
     }

     /** Return the modified EventSelectionProperties object. */
     public EventSelectionProperties getProperties () {
        return props;
     }

     private void jbInit() throws Exception {
        Box masterBox = Box.createVerticalBox();
// Authority Subsource panel
        TitledBorder border = new TitledBorder("Authority Subsource");
        border.setTitleColor(Color.black);
        JPanel authSrcPanel = new JPanel();
        authSrcPanel.setLayout(new BorderLayout());
        authSrcPanel.setBorder(border);

        Box box = Box.createHorizontalBox();
        JLabel jlabel = new JLabel("Mag Auth");
        box.add(jlabel);
        jtfAuth.setMaximumSize(new Dimension(120,20));
        jtfAuth.setText(props.getMagAuth());
        /*
        jtfAuth.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String str = ((JTextField) e.getSource()).getText();
                if (str != null && str.length() > 0) props.setMagAuth(str);
                else props.remove("magAuth");
            }
        });
        */
        box.add(jtfAuth);
        box.add(Box.createHorizontalStrut(4));

        jlabel = new JLabel("Mag Subsource");
        box.add(jlabel);
        jtfSrc.setMaximumSize(new Dimension(120,20));
        jtfSrc.setText(props.getMagSubsource());
        /*
        jtfSrc.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String str = ((JTextField) e.getSource()).getText();
                if (str != null && str.length() > 0) props.setMagSubsource(str);
                else props.remove("magSubsource");
            }
        });
        */
        box.add(jtfSrc);
        box.add(Box.createHorizontalStrut(4));
          
// Mag Algo
        jlabel = new JLabel("Mag Algo");
        box.add(jlabel);
        jtfAlgo.setMaximumSize(new Dimension(120,20));
        jtfAlgo.setText(props.getMagAlgo());
        /*
        jtfAlgo.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String str = ((JTextField) e.getSource()).getText();
                if (str != null && str.length() > 0) props.setMagAlgo(str);
                else props.remove("magAlgo");
            }
        });
        */
        box.add(jtfAlgo);
        box.add(Box.createHorizontalGlue());
        authSrcPanel.add(box, BorderLayout.CENTER);
        box = Box.createHorizontalBox();
        box.add(Box.createHorizontalGlue());
        JButton cb = new JButton("Clear");
        cb.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                jtfAuth.setText(null);
                jtfSrc.setText(null);
                jtfAlgo.setText(null);
            }
        });
        cb.setMaximumSize(new Dimension(90,20));
        box.add(cb);
        box.add(Box.createHorizontalGlue());
        authSrcPanel.add(box, BorderLayout.SOUTH);
        authSrcPanel.setMinimumSize(new Dimension(200,40));

        masterBox.add(authSrcPanel);

// preferred mag type panel
        border = new TitledBorder("Preferred Type");
        border.setTitleColor(Color.black);
        JPanel prefPanel = new JPanel();
        prefPanel.setBorder(border);
        prefPanel.setLayout(new GridLayout(1, 0));

        /*
        final ActionListener al = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String cmd = e.getActionCommand();
                if (cmd.equals("any")) props.remove("magPrefType");
                else props.setProperty("magPrefType", cmd);
            }
        };
        */

        JRadioButton jb = new JRadioButton("any"); 
        jb.setActionCommand("any");
        bgPrefType.add(jb);
        prefPanel.add(jb);
        anyModel = jb.getModel();

        jb = new JRadioButton("null"); 
        jb.setActionCommand("null");
        nullModel = jb.getModel();
        bgPrefType.add(jb);
        prefPanel.add(jb);
        
        for (int i = 0; i < MagTypeIdIF.TYPES.length; i++) {
            jb = new JRadioButton(MagTypeIdIF.TYPES[i]);
            jb.setActionCommand(MagTypeIdIF.TYPES[i]);
            //jb.addActionListener(al);
            bgPrefType.add(jb);
            prefPanel.add(jb);
        }
        String prefType = props.getProperty("magPrefType");
        if (prefType == null) {
            if ( props.getBoolean("magPrefNull") ) bgPrefType.setSelected(nullModel, true);
            else bgPrefType.setSelected(anyModel, true);
        }
        else {
            Enumeration e = bgPrefType.getElements();
            jb = null;
            while (e.hasMoreElements()) {
                jb = (JRadioButton) e.nextElement();
                if (jb.getActionCommand().equals(prefType)) {
                    bgPrefType.setSelected(jb.getModel(), true);
                    break;
                }
            }
        }
        prefPanel.setMinimumSize(new Dimension(200,40));
        masterBox.add(prefPanel);

        JPanel rangePanel = new JPanel();
        rangePanel.setLayout(new GridLayout(0,1));
        border = new TitledBorder("Calculated Ranges");
        border.setTitleColor(Color.black);
        rangePanel.setBorder(border);

        JPanel jp = new JPanel();
        box = Box.createHorizontalBox();
        JLabel label = new JLabel("Name", SwingConstants.CENTER);
        label.setPreferredSize(new Dimension(80,20));
        label.setMaximumSize(new Dimension(80,20));
        label.setMinimumSize(new Dimension(80,20));
        box.add(label);
        box.add(Box.createHorizontalStrut(10));
        label = new JLabel("Min", SwingConstants.CENTER);
        label.setPreferredSize(new Dimension(80,20));
        label.setMaximumSize(new Dimension(80,20));
        label.setMinimumSize(new Dimension(80,20));
        box.add(label);
        box.add(Box.createHorizontalStrut(10));
        label = new JLabel("Max", SwingConstants.CENTER);
        label.setPreferredSize(new Dimension(80,20));
        label.setMaximumSize(new Dimension(80,20));
        box.add(label);
        box.add(Box.createHorizontalGlue());
        jp.add(box);
        rangePanel.add(jp);

        // boolean value true/false must match Double/Integer range declared in EventSelectionProperties class
        // Double
        rcVal = new MinMaxRangeChooser("Mag Value", "magValueRange", true, -1., 10.);
        rcQual = new MinMaxRangeChooser("Quality", "magQualityRange", true, 0., 1.);
        rcDist = new MinMaxRangeChooser("Distance", "magDistRange", true, 0., 9999.);
        //Integer
        rcErr = new MinMaxRangeChooser("Err", "magErrRange", true, 0, 9999);
        rcSta = new MinMaxRangeChooser("Sta", "magStaRange", false, 0, 9999);
        rcObs = new MinMaxRangeChooser("Sta", "magObsRange", false, 0, 9999);
        rcGap = new MinMaxRangeChooser("Gap", "magGapRange", false, 0, 360);

        rangePanel.add(rcQual);
        rangePanel.add(rcSta);
        rangePanel.add(rcObs);
        rangePanel.add(rcDist);
        rangePanel.add(rcErr);
        rangePanel.add(rcGap);
        rangePanel.add(rcVal);

        rangePanel.setMinimumSize(new Dimension(200,230));
        masterBox.add(rangePanel);
        masterBox.add(Box.createVerticalStrut(5));

        jp = new JPanel();
        cb = new JButton("Clear All");
        cb.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                EventSelectionMagPanel.this.clear();
            }
        });
        cb.setMaximumSize(new Dimension(90,20));
        jp.add(cb);
        masterBox.add(jp);

        masterBox.add(Box.createVerticalGlue());
        this.add(masterBox);

        this.setMinimumSize(new Dimension(200,320));
    }
        

    private class MinMaxRangeChooser extends JPanel implements ActionListener {
        JTextField min = null;
        JTextField max = null;
        String propName = null;
        boolean fp = false;

        double defaultMinValue = 0.;
        double defaultMaxValue = 9999.;
      
        public MinMaxRangeChooser(String title, String propName, boolean fp, double defMin, double defMax) {
            this.propName = propName;
            this.fp = fp;

            defaultMinValue = defMin;
            defaultMaxValue = defMax;

            NumberRange nr = (fp) ?
                (NumberRange) props.getDoubleRange(propName) : (NumberRange) props.getIntegerRange(propName);

            //TitledBorder border = new TitledBorder(title);
            //border.setTitleColor(Color.black);
            //setBorder(border);
            //setLayout(new GridLayout(1,2));
            Box box = Box.createHorizontalBox();
            JLabel label = new JLabel(title, SwingConstants.TRAILING);
            label.setPreferredSize(new Dimension(80,20));
            label.setMaximumSize(new Dimension(80,20));
            label.setMinimumSize(new Dimension(80,20));
            box.add(label);

            box.add(Box.createHorizontalStrut(10));

            min = new JTextField(8);
            min.setMaximumSize(new Dimension(80,20));
            if (nr != null) min.setText(nr.getMin().toString());
            min.addActionListener(this);
            box.add(min);

            box.add(Box.createHorizontalStrut(10));

            max = new JTextField(8);
            max.setMaximumSize(new Dimension(80,20));
            if (nr != null) max.setText(nr.getMax().toString());
            max.addActionListener(this);

            box.add(max);
            box.add(Box.createHorizontalStrut(5));
            JButton cb = new JButton("C");
            cb.setMaximumSize(new Dimension(20,20));
            cb.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    MinMaxRangeChooser.this.clear();
                }
            });
            box.add(cb);
            box.add(Box.createHorizontalGlue());
            add(box);
        }
      
      
        public void actionPerformed(ActionEvent e) {
            //updateProperty(); // don't do it until update invoked externally
        }
        
        private IntegerRange getIntegerRange() {
            String minStr = min.getText();
            String maxStr = max.getText();
            if (minStr.length() < 1 && maxStr.length() < 1) return null;
            else if (minStr.length() < 1) minStr = String.valueOf(defaultMinValue);
            else if (maxStr.length() < 1) maxStr = String.valueOf(defaultMaxValue);
            // In case user entered decimal:
            return new IntegerRange(Math.round(Float.parseFloat(minStr)),Math.round(Float.parseFloat(maxStr)));
        }

        private DoubleRange getDoubleRange() {
            String minStr = min.getText();
            String maxStr = max.getText();
            if (minStr.length() < 1 && maxStr.length() < 1) return null;
            else if (minStr.length() < 1) minStr = String.valueOf(defaultMinValue);
            else if (maxStr.length() < 1) maxStr = String.valueOf(defaultMaxValue);
            return new DoubleRange(new Double(minStr), new Double(maxStr));
        }

        public void updateProperty() {
            NumberRange nr = ( fp ) ? (NumberRange) getDoubleRange() : (NumberRange) getIntegerRange();
            if (nr != null) props.setProperty(propName, nr);
            else props.remove(propName);
        }

        public void clear() {
            min.setText(null);
            max.setText(null);
        }

    }

    public void updateProperties() {
        String str = jtfAuth.getText();
        if (str != null && str.length() > 0) props.setMagAuth(str);
        else props.remove("magAuth");

        str = jtfSrc.getText();
        if (str != null && str.length() > 0) props.setMagSubsource(str);
        else props.remove("magSubsource");

        str = jtfAlgo.getText();
        if (str != null && str.length() > 0) props.setMagAlgo(str);
        else props.remove("magAlgo");

        str = bgPrefType.getSelection().getActionCommand();
        if (str.equals("any")) {
            props.remove("magPrefType");
            props.remove("magPrefNull");
        }
        else if (str.equals("null")) {
            props.remove("magPrefType");
            props.setProperty("magPrefNull", true);
        }
        else {
            props.remove("magPrefNull");
            props.setProperty("magPrefType", str);
        }


        rcQual.updateProperty();
        rcSta.updateProperty();
        rcObs.updateProperty();
        rcDist.updateProperty();
        rcErr.updateProperty();
        rcGap.updateProperty();
        rcVal.updateProperty();
    }

    public void clear() {
        jtfAuth.setText(null);
        jtfSrc.setText(null);
        jtfAlgo.setText(null);

        bgPrefType.setSelected(anyModel, true);

        rcQual.clear();
        rcSta.clear();
        rcObs.clear();
        rcDist.clear();
        rcErr.clear();
        rcGap.clear();
        rcVal.clear();
    }
}                                        
