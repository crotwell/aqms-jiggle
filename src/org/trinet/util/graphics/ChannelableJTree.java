package org.trinet.util.graphics;
/**
 * Based on: http://www.codeguru.com/java/articles/187.shtml
 */

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.*;
import javax.swing.border.*;

import org.trinet.jasi.*;
import org.trinet.jiggle.common.AlertUtil;
import org.trinet.jiggle.common.JiggleConstant;
import org.trinet.jiggle.common.LogUtil;

// makes your tree as CheckTree
//CheckTreeManager checkTreeManager = new CheckTreeManager(yourTree); 
// to get the paths that were checked
//TreePath checkedPaths[] = checkTreeManager.getSelectionModel().getSelectionPaths(); 

public class ChannelableJTree extends JPanel {

  public static final int NAME_MODE = 0;
  public static final int DATA_MODE = 1;
  public static final int CHANNEL_MODE = 2;

  private DefaultMutableTreeNode topNode = null;
  //private CheckTreeManager checkTreeManager = null;
  //private JTree tree;
  private CheckboxTree tree;
  private JScrollPane treePane;

  public ChannelableJTree(ChannelableList poolList) {
    this(null, poolList, true);
  }

  public ChannelableJTree(String title, ChannelableList poolList, boolean addControlButtons) {
    super();
    if (title != null) setBorder(BorderFactory.createTitledBorder(title));
    initGraphics(getChannelNodes(poolList), addControlButtons);
  }

  public java.util.List getSelectedNames() {
      return getSelected(NAME_MODE);
  }
  public java.util.List getSelectedData() {
      return getSelected(DATA_MODE);
  }
  public java.util.List getSelectedChannelables() {
      return getSelected(CHANNEL_MODE);
  }

  public java.util.List getSelected(int mode) {
      //System.out.println("Tree row count: " + tree.getRowCount());
      //TreePath checkedPaths[] = checkTreeManager.getSelectionModel().getSelectionPaths(); 
      //TreePath checkedPaths[] = tree.getCheckingPaths(); 
      //if (checkedPaths == null) return new ArrayList(0);
      TreePath rootPaths[] = tree.getCheckingRoots(); 
      if (rootPaths == null) return new ArrayList(0);
      ArrayList aList = new ArrayList(1000);
      DefaultMutableTreeNode node = null;
      for (int idx = 0; idx < rootPaths.length; idx++) {
        node = (DefaultMutableTreeNode) rootPaths[idx].getLastPathComponent();
        addToList(aList, node, mode); 
      }
      return aList;
  }

  private void addToList(java.util.List aList, DefaultMutableTreeNode node, int mode) { 
      DefaultMutableTreeNode nextNode = null;
      Enumeration enm = node.breadthFirstEnumeration();
      while (enm.hasMoreElements()) {
          nextNode = (DefaultMutableTreeNode) enm.nextElement();
          toList(nextNode, aList, mode);
      }
  }

  private void toList(DefaultMutableTreeNode userNode, java.util.List aList, int mode) {
          Object obj = userNode.getUserObject();
          if ( obj instanceof ChannelUserObj) {
             ChannelUserObj cuo = (ChannelUserObj) obj;
             Object element = null;
             if (mode == CHANNEL_MODE) element = cuo.chnl;
             else if (mode == NAME_MODE) element = cuo.toNameString();
             else if (mode == DATA_MODE) element = cuo.toDataString();
             if (element != null) {
                 //System.out.println("DEBUG CJT toList addElement");
                 aList.add(element);
             }
          }
  }

  public void setSelectedNames(String [] names) {
      if (names == null) return;
      Channel chan = Channel.create();
      ChannelableList cList = new ChannelableList(names.length);
      for (int idx = 0; idx < names.length; idx++) {

        try {
          cList.add( ((Channel) chan.clone()).setChannelName(ChannelName.fromDelimitedString(names[idx], " ./_,\t")) );
        }
        catch (Exception ex) {
          ex.printStackTrace();
        }
      }
      setSelectedNames(cList);
  }

  public void setSelectedNames(ChannelableListIF cList) {
      ChannelUserObj cuo = null;
      DefaultMutableTreeNode node = null;
      Object obj = null;
      Enumeration e = topNode.postorderEnumeration();
      TreePath tp = null;
      while (e.hasMoreElements()) {
          node = (DefaultMutableTreeNode)e.nextElement();
          if (! node.isLeaf()) continue;
          node = (DefaultMutableTreeNode) node.getParent();
          obj = node.getUserObject();
          if ( obj instanceof ChannelUserObj) {
              cuo = (ChannelUserObj) obj;
              if (cList.getByChannel(cuo.chnl) != null) {
                  tp = new TreePath(node.getPath());
                  //System.out.println("DEBUG CJT setSelectedNames tp: " + tp);
                  //checkTreeManager.getSelectionModel().addSelectionPath(tp);
                  tree.addCheckingPath(tp);
                  tree.expandPath(tp);
                  //if (checkTreeManager.getSelectionModel().isPathSelected(tp)) System.out.println("path selected: " + tp);
              }
          }
      }
      if (tp != null) tree.scrollPathToVisible(tp);
  }

  private void setAllSelections(boolean tf) {

      if (topNode.getChildCount() == 0) return;

      if (!tf) {
          //checkTreeManager.clearSelection();
          tree.clearChecking();
          //CheckTreeSelectionModel model = checkTreeManager.getSelectionModel();
          //model.clearSelection();
          //model.removeSelectionPaths(new TreePath [] {new TreePath(topNode)}); // model.getSelectionPaths());
      }
      else {
         //checkTreeManager.getSelectionModel().addSelectionPath(new TreePath(topNode));
         tree.addCheckingPath(new TreePath(topNode));
      }
  }

  public void selectAll() {
      setAllSelections(true);
  }

  public void unselectAll() {
      setAllSelections(false);
  }

  private DefaultMutableTreeNode [] getChannelNodes(ChannelableListIF list) {

    ArrayList nodeList = new ArrayList(list.size()+1);

    nodeList.add(topNode);

    ChannelableListIF netlist = null;
    ChannelableListIF stalist = null;
    ChannelableListIF chanlist = null;

    String net = null;
    String sta = null;
    String chan = null;

    DefaultMutableTreeNode netNode = null;
    DefaultMutableTreeNode staNode = null;
    DefaultMutableTreeNode chanNode = null;
    DefaultMutableTreeNode dataNode = null;

    topNode = new DefaultMutableTreeNode("Select all channels");

    NetworkGrouper netGrouper = new NetworkGrouper(list);
    //System.out.println("channels = "+ list.size() + " net grps = "+netGrouper.countGroups());

    // for each net
    while (netGrouper.hasMoreGroups()) {
      netlist = (ChannelableListIF) netGrouper.getNext();
      if (netlist == null) {
          System.out.println("New net node null");
          break;
      }
      if  (netlist.size() == 0) {
          System.out.println("New net node size=0");
          continue;
      }
      //System.out.println("New net group : " + netlist.size());
      net = ((Channelable) netlist.get(0)).getChannelObj().getNet();
      netNode = new DefaultMutableTreeNode(net);
      nodeList.add(netNode);
      topNode.add(netNode);   // network node

      StationGrouper staGrouper = new StationGrouper(netlist);

      // for each station
      while (staGrouper.hasMoreGroups()) {
        stalist = (ChannelableListIF) staGrouper.getNext();
        ChannelGrouper chanGrouper = new ChannelGrouper(stalist);
        sta = ((Channelable) stalist.get(0)).getChannelObj().getSta();
        //System.out.println("New sta group : " + sta + " " + stalist.size() + " channels cnt: " + chanGrouper.countGroups());
        staNode = new DefaultMutableTreeNode(sta);
        nodeList.add(staNode);
        netNode.add(staNode);

        // for each channel
        Channel chnl = null;
        while (chanGrouper.hasMoreGroups()) {
          chanlist = (ChannelableListIF) chanGrouper.getNext();

          // There is an issue here with iteration when there are duplicate channels.
          // The for loop below iterates on all duplicate channels, but the outer loop moves one at a time.
          // For now, it appears duplicate channels should not exist so check for null. May need to adjust the loop if not.
          if (chanlist != null) {
            Channelable chable[] = chanlist.getChannelableArray();

            for (int i = 0; i < chable.length; i++) {
              chanNode = new DefaultMutableTreeNode(new ChannelUserObj(chable[i]));
              nodeList.add(chanNode);

              chnl = chable[i].getChannelObj();

              StringBuffer sb = new StringBuffer(512);
              sb.append("<html>");
              sb.append(chnl.getDateRange().toDateString("yyyy/MM/dd:HH:mm:SS"));
              sb.append(" ").append(chnl.getLatLonZ().toString());
              sb.append(" ").append(String.valueOf(chnl.getSensorDepth()));
              sb.append(" ").append(String.valueOf(chnl.getDistance()));
              sb.append(" ").append(String.valueOf(chnl.getHorizontalDistance()));
              sb.append(" ").append(String.valueOf(chnl.getAzimuth()));
              sb.append(" ").append(String.valueOf(chnl.getSampleRate()));
              sb.append("<br>");
              if (chnl.getResponseMap().size() > 0) {
                sb.append(chnl.getResponseMap().getValuesString().replaceAll(chnl.getChannelId().toString(), "").replaceAll("\n", "<br>"));
                sb.append("<br>");
              }
              if (chnl.getCorrMap().size() > 0) {
                sb.append(chnl.getCorrMap().getValuesString().replaceAll(chnl.getChannelId().toString(), "").replaceAll("\n", "<br>"));
              }
              sb.append("</html>");
  /*
                  sb.append("<br>");
                  sb.append(chnl.clipAmpMap.getValuesString().replaceAll(chnl.getChannelId().toString(), "").replaceAll("\n","<br>"));
  */
              dataNode = new DefaultMutableTreeNode(sb.toString());
              //nodeList.add(dataNode);
              chanNode.add(dataNode);

              staNode.add(chanNode);
            }
          } else {
            String errTitle = "Error Reading Channels";
            String errMsg = "Error in ChannelableJTree reading channel list. Possible duplicate channels:" +
                    JiggleConstant.NEWLINE + stalist;

            LogUtil.error(errMsg);
            int response = AlertUtil.displayCrticialError(errTitle, errMsg);
            if (response == JOptionPane.YES_OPTION) {
              LogUtil.error("User wants to continue. Try the next channel in a group");
              continue;
            } else {
              // stop this iteration
              LogUtil.error("User wants to stop. Ignore current group and move to next group");
              break;
            }
          }
        }
      }
    }

    return (DefaultMutableTreeNode[]) nodeList.toArray(new DefaultMutableTreeNode[nodeList.size()]);
  }

  private class ChannelUserObj{

      String id = null;
      Channelable chnl = null;

      ChannelUserObj() { }

      ChannelUserObj(Channelable chan) {
          Channel chnl = chan.getChannelObj();
          this.id = new StringBuffer(6).append(chnl.getSeedchan()).append(" ").append(chnl.getLocation().replace(' ','-')).toString();
          this.chnl = chan;
      }

      public String toString() {
          return id;
      }

      public String toDataString() {
          return chnl.getChannelObj().toDumpString();
      }

      public String toNameString() {
          return chnl.getChannelObj().toDelimitedSeedNameString(".");
      }

      public boolean equals(Object obj) {
          if (!(obj instanceof ChannelUserObj)) return false; 
          ChannelUserObj cuo = (ChannelUserObj) obj;
          return ( id.equals(cuo.id) && chnl.equalsChannelId(cuo.chnl) );
      }

      public int hashCode() {
          return id.hashCode();
      }
  }

  public void setScrollPanePreferredSize(Dimension d) {
      treePane.setPreferredSize(d);
  }

  public void setCheckingMode(TreeCheckingModel.CheckingMode mode) {
      CheckboxTree jt = (CheckboxTree) getTree();
      if (jt != null) jt.getCheckingModel().setCheckingMode(mode);
  }

  private void initGraphics(final DefaultMutableTreeNode[] nodes, boolean addControlButtons) {

      //tree = new JTree(topNode);
      //tree.setVisibleRowCount(32);
      //checkTreeManager = new CheckTreeManager(tree); 
      tree = new CheckboxTree(topNode) { // enable tooltips to provide path info

        public String getToolTipText(MouseEvent e) {
            TreeNode node = null;
            TreePath path = getPathForLocation(e.getX(), e.getY());
            if (path != null) {
                node = (TreeNode) path.getLastPathComponent();
            }
            if (node == null) { 
              return super.getToolTipText(e);
            }


            Object [] obj = path.getPath();
            StringBuffer sb = new StringBuffer(32);
            int end = (node.isLeaf()) ? obj.length-1 : obj.length;
            for (int idx=1; idx<end; idx++) {
                sb.append(obj[idx].toString()).append(" ");
            }
            return sb.toString();
        }

      }; 
      //ToolTipManager.sharedInstance().registerComponent(tree);
      tree.setToolTipText("Mouse over channel node to see NET STA SEED LOCATION"); // registers component with ToolTipManger instance

      tree.getCheckingModel().setCheckingMode(TreeCheckingModel.CheckingMode.PROPAGATE_PRESERVING_CHECK); // or PropagateUpWhiteTreeCheckingMode ?
      //tree.getCheckingModel().setCheckingMode(TreeCheckingModel.CheckingMode.PROPAGATE_PRESERVING_UNCHECK);
      CheckboxTreeCellExceptLeafRenderer renderer = new CheckboxTreeCellExceptLeafRenderer();
      renderer.setOpenIcon(null);
      renderer.setClosedIcon(null);
      renderer.setLeafIcon(null);
      tree.setCellRenderer(renderer);
      treePane = new JScrollPane(tree);

      this.setLayout(new BorderLayout());
      this.add(treePane, BorderLayout.CENTER);

      if (! addControlButtons) return;

      Box hbox = Box.createHorizontalBox();

      JButton jb = new JButton("Clear Checks");
      jb.setToolTipText("Clear checked paths");
      jb.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    unselectAll();
                }
      });
      jb.setPreferredSize(new Dimension(80,20));
      hbox.add(jb);

      jb = new JButton("Collapse Tree");
      jb.setToolTipText("Collapse all expanded nodes");
      jb.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    JTree jt = getTree();
                    int row = jt.getRowCount() - 1; // row indexing starts at 0
                    while (row >= 0) {
                        jt.collapseRow(row);
                        row--;
                    }
                    jt.expandPath(new TreePath(jt.getModel().getRoot()));
                }
      });
      jb.setPreferredSize(new Dimension(80,20));
      hbox.add(jb);

      jb = new JButton("Collapse Node");
      jb.setToolTipText("Collapse selected node and its children");
      jb.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    JTree jt = getTree();
                    TreePath tp = jt.getSelectionPath();
                    if (tp == null) return;
                    Enumeration e = jt.getExpandedDescendants(tp);
                    if (e != null) {
                      while (e.hasMoreElements()) {
                        jt.collapsePath((TreePath)e.nextElement());
                      }
                    }
                    jt.collapsePath(tp);
                }
      });
      jb.setPreferredSize(new Dimension(80,20));
      hbox.add(jb);

      jb = new JButton("Expand Node");
      jb.setToolTipText("Expand selected node and its children");
      jb.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    JTree jt = getTree();
                    TreeModel tm = jt.getModel();
                    TreePath tp = jt.getSelectionPath();
                    if (tp == null) return;
                    Object parent =tp.getLastPathComponent();
                    int cnt = tm.getChildCount(parent);
                    TreePath cp = null;
                    //this expands only 1st tier children, expansion to leaf level requires recursive method call 
                    for (int idx = 0; idx < cnt; idx++) {
                      cp = tp.pathByAddingChild(tm.getChild(parent, idx));
                      jt.expandPath(cp);
                    }
                    jt.expandPath(tp);
                }
      });
      jb.setPreferredSize(new Dimension(80,20));
      hbox.add(jb);

      jb = new JButton("Expand Checked Nodes");
      jb.setToolTipText("Expand checked nodes and children");
      jb.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    CheckboxTree jt = (CheckboxTree) getTree();
                    TreePath [] tp = jt.getCheckingPaths(); // jt.getCheckingRoots();
                    if (tp == null) return;

                    for (int jdx = 0; jdx < tp.length; jdx++) {
                      jt.expandPath(tp[jdx]);
                    }
                }
      });
      jb.setPreferredSize(new Dimension(80,20));
      hbox.add(jb);

      this.add(hbox,BorderLayout.SOUTH);
  }

  public JTree getTree() {
      return tree;
  }

}

