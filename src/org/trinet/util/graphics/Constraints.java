package org.trinet.util.graphics;
public interface Constraints {
    boolean contains(Object data) ;
    boolean excludes(Object data) ;
    boolean isLimited();
    // override Object.class String toString() to derive description of limits;
}

