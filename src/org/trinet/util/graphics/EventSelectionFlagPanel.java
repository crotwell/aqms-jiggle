package org.trinet.util.graphics;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;

import org.trinet.jasi.*;
import org.trinet.util.*;

/** A panel allows selection of various EventSelectionProperties attributes. */
public class EventSelectionFlagPanel extends JPanel {
    //EVENT PROPS
        //"eventAuth"
        //"eventSubsource"
        //"eventValidFlag"
        //"eventTypesIncluded"
        //"eventTypesExcluded"
        //"originGTypesIncluded"
        //"originGTypesExcluded"
    //
    // ORIGIN PROPS
        //"originProcTypesIncluded"
        //"originProcTypesExcluded"
        //"originDummyFlag"
        //"originAuth"
        //"originSubsource"
        //"originType"
        //"originAlgo"
        //"originFixDepthFlag"
        //"originFixEpiFlag"
        //
    
     // Properties may change immediately, so its up to the caller
     // to implement recover from a cancellation.
    private EventSelectionProperties props;

    private BooleanFlagPanel selectFlagPanel = null;
    private BooleanFlagPanel dummyFlagPanel = null;
    private BooleanFlagPanel fixEpiFlagPanel = null;
    private BooleanFlagPanel fixDepthFlagPanel = null;

    private ArrayList eTypeCheckedList = new ArrayList();
    private ArrayList gTypeCheckedList = new ArrayList();
    private ArrayList pTypeCheckedList = new ArrayList();

    private JCheckBox jcbAnyEvent = new JCheckBox("Any");
    private JCheckBox jcbAnyGType = new JCheckBox("Any");
    private JCheckBox jcbAnyProc = new JCheckBox("Any");

    JTextField jtfEventAuth = new JTextField(8);
    JTextField jtfEventSrc = new JTextField(8);
    JTextField jtfOriginAuth = new JTextField(8);
    JTextField jtfOriginSrc = new JTextField(8);
    JTextField jtfOriginAlgo = new JTextField(8);
    JTextField jtfOriginType = new JTextField(8);

    /** The EventSelectionProperties object that is passed is changed
    * immediately by user action in the panel, therefore, if the caller wants
    * to be able to undo user action and revert to the old properties (say in
    * response to clicking a [CANCEL] button in a dialog) the caller must either retain
    * a copy of the original properties or pass a copy to this class and only
    * use it when approriate (say when [OK] is clicked). */
    public EventSelectionFlagPanel(EventSelectionProperties props) {
        this.props = props;
        try  {
            jbInit();
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    /** Return the modified EventSelectionProperties object. */
    public EventSelectionProperties getProperties () {
        return props;
    }

    private void jbInit() throws Exception {
        Box masterBox = Box.createVerticalBox();
    // event origin flag panel 
        JPanel flagPropPanel = new JPanel();
        //flagPropPanel.setMaximumSize(new Dimension(400,120));
        //flagPropPanel.setPreferredSize(new Dimension(400,120));
        TitledBorder border = new TitledBorder("Event-Origin Flags");
        border.setTitleColor(Color.black);
        flagPropPanel.setBorder(border);

        selectFlagPanel = new BooleanFlagPanel("EventValid", "eventValidFlag");
        dummyFlagPanel = new BooleanFlagPanel("OriginDummy", "originDummyFlag");
        fixDepthFlagPanel = new BooleanFlagPanel("OriginFixDepth", "originFixDepthFlag");
        fixEpiFlagPanel = new BooleanFlagPanel("OriginFixEpi", "originFixEpiFlag");

        flagPropPanel.add(selectFlagPanel);
        flagPropPanel.add(dummyFlagPanel);
        flagPropPanel.add(fixDepthFlagPanel);
        flagPropPanel.add(fixEpiFlagPanel);

        masterBox.add(flagPropPanel);

    // event type panel
        Box eventTypePanel = Box.createHorizontalBox();
        //eventTypePanel.setMaximumSize(new Dimension(400,100));
        //eventTypePanel.setPreferredSize(new Dimension(400,100));
        border = new TitledBorder("Event Type");
        border.setTitleColor(Color.black);
        eventTypePanel.setBorder( border );

        /** List of event type choices which is defined in org.trinet.jasi.EventTypeMap3. */
        String typeChoice[] = EventTypeMap3.getMap().getJasiTypeArray();

        StringList aListInc = props.getEventTypesIncludedList();
        StringList aListExc = props.getEventTypesExcludedList();

        int count = aListInc.size() + aListExc.size();
        jcbAnyEvent.setSelected( (count == 0) );
        jcbAnyEvent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (jcbAnyEvent.isSelected()) clearEventTypes();
            }
        });
        //eventTypePanel.add(jcbAnyEvent);

        Box vbox = Box.createVerticalBox();
        vbox.add(jcbAnyEvent);
        boolean addBox = true;
        for (int i = 0; i< typeChoice.length; i++) {
            JCheckBox jcb = new JCheckBox(typeChoice[i]);
            eTypeCheckedList.add(jcb);

            // Set initial state of check box
            // Declared include property list forces ignore of the exclude property list
            // otherwise would need a radio button group or "tristate" checkbox
            if (aListInc.size() > 0) {
                jcb.setSelected(aListInc.contains(typeChoice[i]));
            }
            else if (aListExc.size() > 0) { // none specified as included
                if (aListExc.contains(typeChoice[i])) { // reject it
                  jcb.setSelected(false);
                  aListExc.remove(typeChoice[i]); // checkbox state uncertainty, only allows "includes" -aww 2008/11/17
                }
                else { // not on list, so by default select it
                  jcb.setSelected(true);
                }
            }

            jcb.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    jcbAnyEvent.setSelected(false);
                }
            });

            vbox.add(jcb);
            addBox = true;

            if (i > 0 && (i+1)%10 == 0) {
              
              eventTypePanel.add(vbox);
              vbox = Box.createVerticalBox();

              vbox.add(Box.createRigidArea(jcb.getPreferredSize()));
              addBox = false;
            }
        }

        if (addBox) {
            vbox.add(Box.createVerticalGlue());
            eventTypePanel.add(vbox);
        }
        masterBox.add(eventTypePanel);

        // gtype panel
        JPanel gTypePanel = getGtypePanel();
        masterBox.add(gTypePanel);

        // processing state panel
        JPanel procStatePanel = new JPanel();
        //procStatePanel.setMaximumSize(new Dimension(400,60));
        //procStatePanel.setPreferredSize(new Dimension(400,60));
        border = new TitledBorder("Processing State");
        border.setTitleColor(Color.black);
        procStatePanel.setBorder(border);

        /** List of event type choices which is defined in org.trinet.jasi.EventTypeMap3. */
        String label[] = ProcessingState.getLabelArray();

        aListInc = props.getOriginProcTypesIncludedList();
        aListExc = props.getOriginProcTypesExcludedList();

        count = aListInc.size() + aListExc.size();
        jcbAnyProc.setSelected( (count == 0) );
        jcbAnyProc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (jcbAnyProc.isSelected()) clearProcTypes();
            }
        });
        procStatePanel.add(jcbAnyProc);

        for (int i = 0; i< label.length; i++) {
            JCheckBox jcb = new JCheckBox(label[i]);
            pTypeCheckedList.add(jcb);

            // Set initial state of check box
            // Declared include property list forces ignore of the exclude property list
            // otherwise would need a radio button group or "tristate" checkbox
            if (aListInc.size() > 0) {
                jcb.setSelected(aListInc.contains(label[i]));
            }
            else if (aListExc.size() > 0) { // none specified as included
                if (aListExc.contains(label[i])) { // reject it
                  jcb.setSelected(false);
                  aListExc.remove(label[i]); // checkbox state uncertainty, only allows "includes" -aww 2008/11/17
                }
                else { // not on list, so by default select it
                  jcb.setSelected(true);
                }
            }

            jcb.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    jcbAnyProc.setSelected(false);
                }
            });

            procStatePanel.add(jcb);
        }

        masterBox.add(procStatePanel);

        // Authority Subsource panel
        border = new TitledBorder("Authority Subsource");
        border.setTitleColor(Color.black);
        JPanel authSrcPanel = new JPanel();
        authSrcPanel.setBorder(border);
        //authSrcPanel.setMaximumSize(new Dimension(400,80));
        //authSrcPanel.setPreferredSize(new Dimension(400,80));

        vbox = Box.createVerticalBox();
        Box hbox = Box.createHorizontalBox();
        //box.setMaximumSize(new Dimension(400,30));
        //box.setPreferredSize(new Dimension(400,30));
        JLabel jlabel = new JLabel(" Event Auth");
        //jlabel.setMaximumSize(new Dimension(120,20));
        hbox.add(jlabel);
        jtfEventAuth.setMaximumSize(new Dimension(120,20));
        jtfEventAuth.setText(props.getEventAuth());
        /*
        jtfEventAuth.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String str = ((JTextField) e.getSource()).getText();
                if (str != null && str.length() > 0) props.setEventAuth(str);
                else props.remove("eventAuth");
            }
        });
        */
        hbox.add(jtfEventAuth);
        hbox.add(Box.createHorizontalStrut(4));

        jlabel = new JLabel("Subsource");
        //jlabel.setMaximumSize(new Dimension(120,20));
        hbox.add(jlabel);
        jtfEventSrc.setMaximumSize(new Dimension(120,20));
        jtfEventSrc.setText(props.getEventSubsource());
        /*
        jtfEventSrc.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String str = ((JTextField) e.getSource()).getText();
                if (str != null && str.length() > 0) props.setEventSubsource(str);
                else props.remove("eventSubsource");
            }
        });
        */
        hbox.add(jtfEventSrc);
        hbox.add(Box.createHorizontalGlue());
        vbox.add(hbox);
          
        hbox = Box.createHorizontalBox();
        //box.setMaximumSize(new Dimension(400,30));
        //box.setPreferredSize(new Dimension(400,30));
        jlabel = new JLabel("Origin Auth");
        //jlabel.setMaximumSize(new Dimension(120,20));
        hbox.add(jlabel);
        jtfOriginAuth.setMaximumSize(new Dimension(120,20));
        jtfOriginAuth.setText(props.getOriginAuth());
        /*
        jtfOriginAuth.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String str = ((JTextField) e.getSource()).getText();
                if (str != null && str.length() > 0) props.setOriginAuth(str);
                else props.remove("originAuth");
            }
        });
        */
        hbox.add(jtfOriginAuth);
        hbox.add(Box.createHorizontalStrut(4));

        jlabel = new JLabel("Subsource");
        //jlabel.setMaximumSize(new Dimension(120,20));
        hbox.add(jlabel);
        jtfOriginSrc.setMaximumSize(new Dimension(120,20));
        jtfOriginSrc.setText(props.getOriginSubsource());
        /*
        jtfOriginSrc.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String str = ((JTextField) e.getSource()).getText();
                if (str != null && str.length() > 0) props.setOriginSubsource(str);
                else props.remove("originSubsource");
            }
        });
        */
        hbox.add(jtfOriginSrc);
        hbox.add(Box.createHorizontalStrut(4));

        // Origin Algo
        //box.setMaximumSize(new Dimension(400,30));
        //box.setPreferredSize(new Dimension(400,30));
        jlabel = new JLabel("Algo");
        //jlabel.setMaximumSize(new Dimension(120,20));
        hbox.add(jlabel);
        jtfOriginAlgo.setMaximumSize(new Dimension(120,20));
        jtfOriginAlgo.setText(props.getOriginAlgo());
        /*
        jtfOriginAlgo.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String str = ((JTextField) e.getSource()).getText();
                if (str != null && str.length() > 0) props.setOriginAlgo(str);
                else props.remove("originAlgo");
            }
        });
        */
        hbox.add(jtfOriginAlgo);
        hbox.add(Box.createHorizontalStrut(4));

        jlabel = new JLabel("Type");
        //jlabel.setMaximumSize(new Dimension(120,20));
        hbox.add(jlabel);
        jtfOriginType.setMaximumSize(new Dimension(120,20));
        jtfOriginType.setText(props.getOriginType());
        /*
        jtfOriginType.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String str = ((JTextField) e.getSource()).getText();
                if (str != null && str.length() > 0) props.setOriginType(str);
                else props.remove("originType");
            }
        });
        */
        hbox.add(jtfOriginType);
        hbox.add(Box.createHorizontalGlue());
        vbox.add(hbox);

        hbox = Box.createHorizontalBox();
        hbox.add(Box.createHorizontalGlue());
        JButton cb = new JButton("Clear");
        cb.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                jtfEventAuth.setText(null);
                jtfEventSrc.setText(null);
                jtfOriginAuth.setText(null);
                jtfOriginSrc.setText(null);
                jtfOriginAlgo.setText(null);
                jtfOriginType.setText(null);
            }
        });
        cb.setMaximumSize(new Dimension(90,20));
        hbox.add(cb);
        hbox.add(Box.createHorizontalGlue());
        vbox.add(hbox);
        vbox.add(Box.createVerticalGlue());

        authSrcPanel.add(vbox);
        masterBox.add(authSrcPanel);

        JPanel jp = new JPanel();
        cb = new JButton("Clear All");
        cb.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                EventSelectionFlagPanel.this.clear();
            }
        });
        cb.setMaximumSize(new Dimension(90,20));
        jp.add(cb);
        masterBox.add(jp);

        masterBox.add(Box.createVerticalGlue());
        this.add(masterBox);

        this.setMinimumSize(new Dimension(440,640));

    }

    // Get GType panel with checkboxes
    private JPanel getGtypePanel() {
        JPanel gTypePanel = new JPanel(new BorderLayout());
        TitledBorder border = new TitledBorder("Origin Gtype");
        border.setTitleColor(Color.black);
        gTypePanel.setBorder(border);

        String[] typeChoice = GTypeMap.getJasiTypeArray();

        StringList aListInc = props.getOriginGTypesIncludedList();
        StringList aListExc = props.getOriginGTypesExcludedList();

        int count = aListInc.size() + aListExc.size();
        jcbAnyGType.setSelected( (count == 0) );
        jcbAnyGType.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (jcbAnyGType.isSelected()) {
                    clearGTypes();
                }
            }
        });

        Box hbox = Box.createHorizontalBox();
        hbox.add(jcbAnyGType);
        for (int i = 0; i< typeChoice.length; i++) {
            JCheckBox jcb = new JCheckBox(typeChoice[i]);
            gTypeCheckedList.add(jcb);

            // Set initial state of check box
            // Declared include property list forces ignore of the exclude property list
            // otherwise would need a radio button group or "tristate" checkbox
            if (aListInc.size() > 0) {
                jcb.setSelected(aListInc.contains(typeChoice[i]));
            }
            else if (aListExc.size() > 0) { // none specified as included
                if (aListExc.contains(typeChoice[i])) { // reject it
                    jcb.setSelected(false);
                    aListExc.remove(typeChoice[i]); // checkbox state uncertainty, only allows "includes"
                }
                else { // not on list, so by default select it
                    jcb.setSelected(true);
                }
            }

            jcb.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    jcbAnyGType.setSelected(false);
                }
            });
            hbox.add(Box.createHorizontalGlue());
            hbox.add(jcb);
        }
        hbox.add(Box.createRigidArea(new Dimension(10,20)));
        gTypePanel.add(hbox, BorderLayout.CENTER);
        //gTypePanel.add(Box.createRigidArea(new Dimension(30,20)));
        return gTypePanel;
    }

    /*
    private void eventType_actionPerformed(ActionEvent e) {
          JCheckBox box = (JCheckBox) e.getSource();
          String type = box.getText();
          StringList aListInc  = props.getEventTypesIncludedList();
          StringList aListExc  = props.getEventTypesExcludedList();
          if (box.isSelected()) {
            if (! aListInc.contains(type)) aListInc.add(type);
            if (aListExc.contains(type)) aListExc.remove(type);
          } else {
            if (aListInc.contains(type)) aListInc.remove(type);
            if (! aListExc.contains(type)) aListExc.add(type);
          }
          props.setEventTypesIncluded(aListInc.toArray());
          props.setEventTypesExcluded(aListExc.toArray());
    }

    private void procState_actionPerformed(ActionEvent e) {
          JCheckBox box = (JCheckBox) e.getSource();
          String type = box.getText();
          StringList aListInc  = props.getOriginProcTypesIncludedList();
          StringList aListExc  = props.getOriginProcTypesExcludedList();
          if (box.isSelected()) {
            if (! aListInc.contains(type)) aListInc.add(type);
            if (aListExc.contains(type)) aListExc.remove(type);
          } else {
            if (aListInc.contains(type)) aListInc.remove(type);
            if (! aListExc.contains(type)) aListExc.add(type);
          }
          props.setOriginProcTypesIncluded(aListInc.toArray());
          props.setOriginProcTypesExcluded(aListExc.toArray());
    }
    */

    private class BooleanFlagPanel extends JPanel implements ActionListener {

        private String propName = null;
        private ButtonGroup bg = null;

        private JRadioButton jbAny = null;
        private JRadioButton jbTrue = null;
        private JRadioButton jbFalse = null;


        public BooleanFlagPanel(String panelLabel, String propName) {

            super();

            this.propName = propName;

            JLabel jLabel = new JLabel(panelLabel);

            jbTrue  = new JRadioButton("true");
            jbTrue.setActionCommand("true");
            jbFalse = new JRadioButton("false");
            jbFalse.setActionCommand("false");
            jbAny = new JRadioButton("any");
            jbAny.setActionCommand("any");

            setLayout(new GridLayout(0, 1));
            add(jLabel);
            add(jbAny);
            add(jbTrue);
            add(jbFalse);

            bg = new ButtonGroup();
            bg.add(jbTrue);
            bg.add(jbFalse);
            bg.add(jbAny);
            setSelection();

            jbTrue.addActionListener(this);
            jbFalse.addActionListener(this);
            jbAny.addActionListener(this);

        }

        private void setSelection() { 
            String value = props.getProperty(propName);
            if (value == null) bg.setSelected(jbAny.getModel(), true);
            else if (value.equalsIgnoreCase("true")) bg.setSelected(jbTrue.getModel(), true);
            else if (value.equalsIgnoreCase("false")) bg.setSelected(jbFalse.getModel(), true);
            else bg.setSelected(jbAny.getModel(), true);
        }
        
        public void actionPerformed(ActionEvent e) {
            //updateProperty()
        }

        public void updateProperty() {
            String cmd = bg.getSelection().getActionCommand();
            if (cmd.equals("any")) props.remove(propName);
            else props.setProperty(propName, cmd);
        }

        public void clear() {
            bg.setSelected(jbAny.getModel(), true);
        }

    }

    private void updateEventTypes() {

        if (jcbAnyEvent.isSelected()) {
            //props.setEventTypesIncluded((String) null); // remove property
            //props.setEventTypesExcluded((String) null); // remove property
            props.setEventTypesIncluded(""); // empty string
            props.setEventTypesExcluded(""); // empty string
            return;
        }

        JCheckBox box = null;
        String type = null;
        StringList aListInc  = props.getEventTypesIncludedList();
        StringList aListExc  = props.getEventTypesExcludedList();
        for (int ii = 0; ii < eTypeCheckedList.size(); ii++) {
            box = (JCheckBox) eTypeCheckedList.get(ii);
            type = box.getText();
            if (box.isSelected()) {
                if (! aListInc.contains(type)) aListInc.add(type);
                if (aListExc.contains(type)) aListExc.remove(type);
            } else {
                if (aListInc.contains(type)) aListInc.remove(type);
                //if (! aListExc.contains(type)) aListExc.add(type); // checkbox state uncertainty, only allows "includes" -aww 2008/11/17
            }
        }
        if (aListInc.size() > 0) props.setEventTypesIncluded(aListInc.toArray());
        else props.setEventTypesIncluded(""); // empty string
        if (aListExc.size() > 0) props.setEventTypesExcluded(aListExc.toArray());
        else props.setEventTypesExcluded(""); // empty string
    }

    private void updateGtypes() {

        if (jcbAnyGType.isSelected()) {
            //props.setEventTypesIncluded((String) null); // remove property
            //props.setEventTypesExcluded((String) null); // remove property
            props.setOriginGTypesIncluded(""); // empty string
            props.setOriginGTypesExcluded(""); // empty string
            return;
        }

        JCheckBox box = null;
        String type = null;
        StringList aListInc  = props.getOriginGTypesIncludedList();
        StringList aListExc  = props.getOriginGTypesExcludedList();
        for (int ii = 0; ii < gTypeCheckedList.size(); ii++) {
            box = (JCheckBox) gTypeCheckedList.get(ii);
            type = box.getText();
            if (box.isSelected()) {
                if (! aListInc.contains(type)) aListInc.add(type);
                if (aListExc.contains(type)) aListExc.remove(type);
            } else {
                if (aListInc.contains(type)) aListInc.remove(type);
            }
        }
        if (aListInc.size() > 0) props.setOriginGTypesIncluded(aListInc.toArray());
        else props.setEventTypesIncluded(""); // empty string
        if (aListExc.size() > 0) props.setOriginGTypesExcluded(aListExc.toArray());
        else props.setEventTypesExcluded(""); // empty string
    }

    private void updateProcessingTypes() {

        if (jcbAnyProc.isSelected()) {
          //props.setOriginProcTypesIncluded((String) null); // remove property
          //props.setOriginProcTypesExcluded((String) null); // remove property
          props.setOriginProcTypesIncluded("");
          props.setOriginProcTypesExcluded("");
          return;
        }

        JCheckBox box = null;
        String type = null;
        StringList aListInc  = props.getOriginProcTypesIncludedList();
        StringList aListExc  = props.getOriginProcTypesExcludedList();
        for (int ii = 0; ii < pTypeCheckedList.size(); ii++) {
            box = (JCheckBox) pTypeCheckedList.get(ii);
            type = box.getText();
            if (box.isSelected()) {
                if (! aListInc.contains(type)) aListInc.add(type);
                if (aListExc.contains(type)) aListExc.remove(type);
            } else {
                if (aListInc.contains(type)) aListInc.remove(type);
                //if (! aListExc.contains(type)) aListExc.add(type); // checkbox state uncertainty, only allows "includes" -aww 2008/11/17
            }
        }
        if (aListInc.size() > 0) props.setOriginProcTypesIncluded(aListInc.toArray());
        else props.setOriginProcTypesIncluded("");
        if (aListExc.size() > 0) props.setOriginProcTypesExcluded(aListExc.toArray());
        else props.setOriginProcTypesExcluded("");
    }

    public void updateProperties() {
        selectFlagPanel.updateProperty();
        dummyFlagPanel.updateProperty();
        fixEpiFlagPanel.updateProperty();
        fixDepthFlagPanel.updateProperty();
        updateEventTypes();
        updateGtypes();
        updateProcessingTypes();
        updateTextProperties();
    }

    private void updateTextProperties() {
        String str = jtfEventAuth.getText();
        if (str != null && str.length() > 0) props.setEventAuth(str);
        else props.remove("eventAuth");
        str = jtfEventSrc.getText();
        if (str != null && str.length() > 0) props.setEventSubsource(str);
        else props.remove("eventSubsource");
        str = jtfOriginAuth.getText();
        if (str != null && str.length() > 0) props.setOriginAuth(str);
        else props.remove("originAuth");
        str = jtfOriginSrc.getText();
        if (str != null && str.length() > 0) props.setOriginSubsource(str);
        else props.remove("originSubsource");
        str = jtfOriginAlgo.getText();
        if (str != null && str.length() > 0) props.setOriginAlgo(str);
        else props.remove("originAlgo");
        str = jtfOriginType.getText();
        if (str != null && str.length() > 0) props.setOriginType(str);
        else props.remove("originType");
    }

    public void clear() {

        selectFlagPanel.clear();
        dummyFlagPanel.clear();
        fixEpiFlagPanel.clear();
        fixDepthFlagPanel.clear();

        jtfEventAuth.setText(null);
        jtfEventSrc.setText(null);
        jtfOriginAuth.setText(null);
        jtfOriginSrc.setText(null);
        jtfOriginAlgo.setText(null);
        jtfOriginType.setText(null);
        clearEventTypes();
        clearGTypes();
        clearProcTypes();

    }

    private void clearEventTypes() {
        int count = eTypeCheckedList.size();
        for (int ii = 0; ii < count; ii++) {
            ((JCheckBox) eTypeCheckedList.get(ii)).setSelected(false);
        }
    }

    private void clearGTypes() {
        int count = gTypeCheckedList.size();
        for (int ii = 0; ii < count; ii++) {
            ((JCheckBox) gTypeCheckedList.get(ii)).setSelected(false);
        }
    }

    private void clearProcTypes() {
        int count = pTypeCheckedList.size();
        for (int ii = 0; ii < count; ii++) {
            ((JCheckBox) pTypeCheckedList.get(ii)).setSelected(false);
        }
    }
/*
    public static void main(String s[])
    {
        JFrame frame = new JFrame("Main");
        frame.addWindowListener(new WindowAdapter()
        {
            public void windowClosing(WindowEvent e) {System.exit(0);}
        });

        // Private properties
        final EventSelectionProperties origProps = new EventSelectionProperties();
        origProps.setFiletype("jiggle"); // test file in the "jiggle" subdir
        origProps.setFilename("eventProperties"); // name of event selection props file
        origProps.reset(); // read the props from file
        origProps.setup(); // configure local environment from props
        origProps.dumpProperties();

        EventSelectionFlagPanel flagPanel =
                 new EventSelectionFlagPanel(origProps);

        JPanel panel = new JPanel(new BorderLayout());
        panel.add (flagPanel, BorderLayout.CENTER);
        JButton okButton = new JButton("OK");
        okButton.addActionListener(new ActionListener() {
           public void actionPerformed(ActionEvent e) {
               System.out.println ("-- Event Selection Properties AFTER --");

               origProps.dumpProperties();

               System.out.println ("-----------");

           }
        });

        panel.add(okButton, BorderLayout.SOUTH);
        frame.getContentPane().add(panel);

//      frame.setSize(200,30); // this has no effect!
        frame.pack();
        frame.setVisible(true);
    }
*/

}                                        
