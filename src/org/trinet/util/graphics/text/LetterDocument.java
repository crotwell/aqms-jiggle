package org.trinet.util.graphics.text;

import java.text.*;
import java.awt.Toolkit;
import javax.swing.text.*; 

public class LetterDocument extends PlainDocument {
    protected int strLength;

    public LetterDocument() {}

    public LetterDocument(int cols) {
	strLength = cols;
    }

    protected boolean hasNoMoreSpace(String inputString) {
        if (strLength <= 0 || inputString.length() <= strLength) return false;
        else {
	    System.err.println("Letter Document: text at maximum length.: " + strLength + " input: " + inputString.length());
            Toolkit.getDefaultToolkit().beep();
	    return true;
	}
    }

    protected boolean hasNonLetter(String inputString) {
	StringCharacterIterator charIter = new StringCharacterIterator(inputString);
	for (char result = charIter.first(); result != CharacterIterator.DONE; result = charIter.next()) {
	    if (! Character.isLetter(result)) {
                    Toolkit.getDefaultToolkit().beep();
		    System.err.println("LetterDocument - text not a letter");
		    return true;
	    }
	}
        return false;
    }

    protected final String getProposedResult(int offs, String inputString) throws BadLocationException {
        String currentText = getText(0, getLength());
        String beforeOffset = currentText.substring(0, offs);
        String afterOffset = currentText.substring(offs, currentText.length());
        return beforeOffset + inputString + afterOffset;
    }


    protected boolean isValid(int offs, String inputString) throws BadLocationException {
        String proposedResult = getProposedResult(offs, inputString);
	return ( proposedResult.equals("") || hasNonLetter(inputString) || hasNoMoreSpace(proposedResult)) ? false : true;
    }

    public void insertString(int offs, String str, AttributeSet a) 
        throws BadLocationException {
	if ( isValid(offs, str) ) super.insertString(offs, str, a);
    }

    public void remove(int offs, int len) throws BadLocationException {
        super.remove(offs, len);
    }
}
