package org.trinet.util.graphics.text;
import java.awt.*;
import java.awt.datatransfer.*;
import java.awt.event.*;
import javax.swing.*;
public class JTableClipboardPopupMenu extends AbstractClipboardPopupMenu {
    JMenuItem copyItem;
    JMenuItem pasteItem;
    JMenuItem selectItem;

    public JTableClipboardPopupMenu() { 
        this(null, null);
    }
    public JTableClipboardPopupMenu(String text) { 
        this(null, text);
    }
    public JTableClipboardPopupMenu(JTable aTable) { 
        this(aTable, null);
    }
    public JTableClipboardPopupMenu(JTable aTable, String label) { 
        super(aTable, label);
    }

    protected void configureMenu() {
        addSeparator();
        copyItem = new JMenuItem("Copy");
        copyItem.addActionListener(this);
        add(copyItem);
        pasteItem = new JMenuItem("Paste");
        pasteItem.addActionListener(this);
        add(pasteItem);
        addSeparator();
        selectItem = new JMenuItem("Unselect row");
        selectItem.addActionListener(this);
        add(selectItem);
        addSeparator();
    }

    public void actionPerformed(ActionEvent e) {
        JMenuItem source = (JMenuItem)(e.getSource());
        if (aComp instanceof JTable) {
          JTable aTable = (JTable) aComp;
          if (source.getText().equals("Paste")) {
            String data = getClipboardString(clip);
            if (data != null) {
              //System.out.println("DEBUG row,col: "+aTable.getSelectedRow()+" " +aTable.getSelectedColumn());
              aTable.setValueAt(data, aTable.getSelectedRow(), aTable.getSelectedColumn());
            }
            else toolkit.beep();
          }
          else if (source.getText().equals("Copy")) {
            //System.out.println( "Selected Table row, col: " + aTable.getSelectedRow()+" " + aTable.getSelectedColumn());
            try {
              StringSelection cellData =
                new StringSelection(
                       (aTable.getValueAt( aTable.getSelectedRow(),
                        aTable.getSelectedColumn())).toString());
              clip.setContents(cellData,cellData);
            } catch (Exception ex) {
               System.out.println(ex.toString());
               toolkit.beep();
            }
          }
          else if (source.getText().equals("Unselect row")) {
             aTable.clearSelection();
          }
        }
        return;
    }
    protected void configureMenuItemStates(Component aComp) {
        pasteItem.setEnabled((clip.getContents(this) != null));
    }
}
