package org.trinet.util.graphics.text;
import javax.swing.text.*; 

import java.awt.Toolkit;
import java.text.*;
import java.util.Locale;

public class FormattedNumberDocument extends PlainDocument {
    private java.text.DecimalFormat format;
    private int strLength;
    private boolean parseIntegerOnly;
    private boolean absoluteValueOnly;

    public FormattedNumberDocument(java.text.DecimalFormat format) {
	this(format, 1, false);
    }

    public FormattedNumberDocument(java.text.DecimalFormat format, int cols) {
	this(format, cols, false);
    }

    public FormattedNumberDocument(java.text.DecimalFormat format, int cols, boolean absoluteValueOnly) {
        this.format = format;
	strLength = cols;
	this.parseIntegerOnly = format.isParseIntegerOnly();
	this.absoluteValueOnly = absoluteValueOnly;
    }

    public boolean isAbsoluteValueOnly() {
	return absoluteValueOnly;
    }

    public void setAbsoluteValueOnly(boolean value) {
	absoluteValueOnly = value;
    }

    public java.text.Format getFormat() {
        return format;
    }

    public void insertString(int offs, String str, AttributeSet a) 
        throws BadLocationException {

	int textLength = getLength();

        String currentText = getText(0, textLength);

	int sign = str.indexOf('-');
	int signCurrent = currentText.indexOf('-');

	int maxLength = strLength;
	if ( (textLength != 0 && currentText.charAt(0) == '-') ||
	     (sign >= 0) ) maxLength++;

        boolean inputValid = false;
        int err = 0;
        do {
  
	    if (strLength > 0 && str.length()+textLength > maxLength) break;
            err++;
            if (str.indexOf('.') >= 0 && (parseIntegerOnly || currentText.lastIndexOf('.') >= 0) ) break;
            err++;
	    if (sign > 0 ) break;
            err++;
            if (sign == 0 && signCurrent >= 0) break;
            err++;
	    if (sign == 0 && ( isAbsoluteValueOnly() || offs > 0 ) ) break;
            err++;

            err = 20;
	    StringCharacterIterator charIter = new StringCharacterIterator(str);
	    for (char result = charIter.first(); result != CharacterIterator.DONE; result = charIter.next()) {
	        if (! Character.isDigit(result) && (result != '-' || result != '.') ) break;
	    }
            inputValid = true;
        } while (false);

        if (! inputValid ) {
//System.err.println("INVALID insertStr: " + str + " err: " + err);
            Toolkit.getDefaultToolkit().beep();
            return;
        }

        String beforeOffset = currentText.substring(0, offs);
        String afterOffset = currentText.substring(offs, currentText.length());
        String proposedResult = beforeOffset + str + afterOffset;

	if (proposedResult.equals("")) return;
//System.err.println("proposedResult: " + proposedResult.substring(0) + " sign: " + sign);
	try {
	    if (! (proposedResult.equals("-.") || proposedResult.equals("-")  || proposedResult.equals("."))) {
                ParsePosition ps = new ParsePosition(0);
//              format.parseObject(proposedResult.substring(sign+1), ps);
                format.parseObject(proposedResult, ps);
                int index = ps.getIndex();
                if (index != proposedResult.length()) throw new ParseException("non-numeric insert", index);
            }
	    super.insertString(offs, str, a);
	}
	catch (ParseException ex) {
            Toolkit.getDefaultToolkit().beep();
//          System.err.println("FormattedNumberDocument insertString- parse error: " + proposedResult);
        }
    }

    public void remove(int offs, int len) throws BadLocationException {
        String currentText = getText(0, getLength());
        String beforeOffset = currentText.substring(0, offs);
        String afterOffset = currentText.substring(len + offs,
                                                   currentText.length());
        String proposedResult = beforeOffset + afterOffset;

        try {
            if (proposedResult.length() != 0) {
	        if (proposedResult.equals("-.") || proposedResult.equals("-")  || proposedResult.equals(".")) {
//		    offs--;
//		    len++;
		}
		else format.parseObject(proposedResult);
	    }
            super.remove(offs, len);
	}
        catch (ParseException e) {
            Toolkit.getDefaultToolkit().beep();
//System.err.println("FormattedNumberDocument remove- parse error: " + proposedResult);
        }
    }
}
