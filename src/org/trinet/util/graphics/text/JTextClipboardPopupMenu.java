package org.trinet.util.graphics.text;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.text.*;
import javax.swing.undo.*;

public class JTextClipboardPopupMenu extends AbstractClipboardPopupMenu {
    boolean addUndo = false;
    protected UndoAction undoAction;
    protected RedoAction redoAction;
    protected UndoManager undo = new UndoManager();
    protected JMenuItem redoItem;
    protected JMenuItem undoItem;

    protected JMenuItem cutItem;
    protected JMenuItem pasteItem;
    protected JMenuItem copyItem;
    protected JMenuItem selectItem;
    protected JMenuItem findItem;
    protected FindStringOptionPanel findPanel;

    public JTextClipboardPopupMenu() { 
        this(null, null);
    }
    public JTextClipboardPopupMenu(String text) { 
        this(null, text);
    }
    public JTextClipboardPopupMenu(JTextComponent textComp) { 
        this(textComp, null);
    }
    public JTextClipboardPopupMenu(JTextComponent textComp, String label) { 
        super(textComp, label);
    }

    public void setAddUndo() {
        addUndo = true;
        createUndo();
    }

    protected void configureMenu() {
        addSeparator();
        copyItem = new JMenuItem("Copy");
        copyItem.addActionListener(this);
        //copyItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C,ActionEvent.ALT_MASK));
        add(copyItem);
        cutItem = new JMenuItem("Cut");
        cutItem.addActionListener(this);
        add(cutItem);
        pasteItem = new JMenuItem("Paste");
        pasteItem.addActionListener(this);
        add(pasteItem);
        addSeparator();
        selectItem = new JMenuItem("Select All");
        selectItem.addActionListener(this);
        add(selectItem);
        addSeparator();
        findItem = new JMenuItem("Find");
        findItem.addActionListener(this);
        add(findItem);
        addSeparator();
        if (addUndo) createUndo();
    }

    public void actionPerformed(ActionEvent e) {
        JMenuItem source = (JMenuItem)(e.getSource());
        if (aComp instanceof JTextComponent) {
          JTextComponent jtextComp = (JTextComponent) aComp;
          if (source.getText().equals("Cut")) {
            jtextComp.cut();
          }
          else if (source.getText().equals("Paste")) {
            jtextComp.paste();
          }
          else if (source.getText().equals("Copy")) {
            jtextComp.copy();
          }
          else if (source.getText().equals("Select All")) {
            jtextComp.requestFocus();
            jtextComp.selectAll();
          }
          else if (source.getText().equals("Find")) {
            if (findPanel == null) findPanel = new FindStringOptionPanel(jtextComp);
            else findPanel.setTextComponent(jtextComp);

            if (findPanel.isSearchBackwards())
                    findPanel.reverseSearchDirection();
            findPanel.createDialog();
          }
        }
    }
    protected class FindStringOptionPanel extends JPanel {
      boolean backSearch = false;
      boolean ignoreCase = false;
      JCheckBox searchCheckBox;
      JCheckBox caseCheckBox;
      JButton findButton;
      JTextField textField;
      JTextComponent jtextComp;
      JOptionPane jop;
      JDialog dialog;

      FindStringOptionPanel() {
        super();
      }
      FindStringOptionPanel(JTextComponent comp) {
        this();
        setTextComponent(comp);
      }

      private void initGraphics() {
        searchCheckBox = new JCheckBox("Backwards", false);
        searchCheckBox.addItemListener(new ItemListener() {
          public void itemStateChanged(ItemEvent evt) {
            backSearch = (evt.getStateChange() == ItemEvent.SELECTED) ?
            true : false;
          }
        });
        caseCheckBox = new JCheckBox("Ignore case", false);
        caseCheckBox.addItemListener(new ItemListener() {
          public void itemStateChanged(ItemEvent evt) {
            ignoreCase = (evt.getStateChange() == ItemEvent.SELECTED) ?
            true : false;
          }
        });
        findButton = new JButton("Find");
        findButton.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent evt) {
            if (jtextComp == null) {
              JOptionPane.showMessageDialog(null, "No text to search");
              return;
            }
            String findStr = textField.getText();
            if (findStr != null && findStr.length() > 0) {
              findText(jtextComp, findStr, true);
            }
          }
        });
        textField = new JTextField(24);
        textField.setFocusable(true);
        textField.addActionListener( new ActionListener() {
          public void actionPerformed(ActionEvent evt) {
            findButton.doClick();
          }
        });
        setLayout(new BorderLayout());
        add(new JLabel("Enter string to find"), BorderLayout.NORTH);
        add(textField, BorderLayout.CENTER);
        add(findButton, BorderLayout.EAST);
        Box hbox = Box.createHorizontalBox();
        hbox.add(searchCheckBox);
        hbox.add(caseCheckBox);
        add(hbox, BorderLayout.SOUTH);
        initOptionPane();
      }

      private void initOptionPane() {
        if (jop == null)  {
          jop = new JOptionPane(
            this,
            JOptionPane.DEFAULT_OPTION,
            JOptionPane.PLAIN_MESSAGE,
            null, new Object[] {"Done"}, "Done");
        }
      }
      public void setTextComponent(JTextComponent comp) {
         jtextComp = comp;
      }
      public boolean caseInsensitive() { return ignoreCase; }
      public boolean isSearchBackwards() { return backSearch; }
      public boolean isSearchForwards() { return ! backSearch; }
      public void reverseSearchDirection() { searchCheckBox.doClick(); }
      
      public void createDialog() {

        if (dialog != null  && dialog.isVisible()) {
            dialog.requestFocus();
            return; // already exists
        }
        if (jop == null) initGraphics();  // make just once
        if (dialog != null) dialog.dispose(); // for insurance

        dialog = jop.createDialog(jtextComp.getTopLevelAncestor(), "Find String");
        dialog.setModal(false);
        dialog.addWindowFocusListener(new WindowAdapter() {
          public void windowGainedFocus(WindowEvent e) {
              textField.requestFocusInWindow();
          }
        });
        dialog.show();
        textField.selectAll();
        textField.repaint();
      }
      private void findText(JTextComponent jtextComp, String selectStr, boolean reverseSearch) {

        String source = jtextComp.getText();
        String findStr = selectStr;

        if (caseInsensitive()) {
            source = source.toLowerCase();
            findStr = findStr.toLowerCase();
        }

        int srcLen = source.length();
        int findStrLen = findStr.length();
        int start = jtextComp.getSelectionStart();
        int end = jtextComp.getSelectionEnd();

        int offset = -1;
        if (isSearchForwards()) {
          start += findStrLen;
          if (start > srcLen) start = srcLen;
          offset = source.indexOf(findStr, start);
        }
        else {
          start -= (findStrLen-1);
          if (start > srcLen) start = srcLen;
          start = Math.max(0, start);
          offset = source.lastIndexOf(findStr, start);
        }

        if (offset >= 0) {
          start = offset; 
          end = Math.min(srcLen, offset+findStrLen);
          jtextComp.getCaret().setDot(start);
          jtextComp.getCaret().moveDot(end);
          jtextComp.repaint();
        }
        else if (reverseSearch) {
          int status = JOptionPane.showConfirmDialog(dialog,
              "String not found, reverse search direction?",
              "Select Option",
              JOptionPane.OK_CANCEL_OPTION);
          if (status == JOptionPane.OK_OPTION) {
           reverseSearchDirection();
           findText(jtextComp, findStr, false);
          }
        }
      }
    }
    protected void createUndo() {
        ((JTextComponent) aComp).getDocument().addUndoableEditListener(new
                        UndoableTextEditListener());
        undoAction = new UndoAction();
        undoItem = new JMenuItem(undoAction);
        undoItem.setMnemonic(KeyEvent.VK_U);
        add(undoItem);
        redoAction = new RedoAction();
        redoItem = new JMenuItem(redoAction);
        redoItem.setMnemonic(KeyEvent.VK_R);
        add(redoItem);
        addSeparator();
        addComponentKeymap();
    }
    protected void addComponentKeymap() {
        //Add a new key map to the keymap hierarchy.
        JTextComponent jtext = (JTextComponent) aComp;
        Keymap keymap = jtext.addKeymap("menuTextKeyBindings", jtext.getKeymap());
        keymap.addActionForKeyStroke(KeyStroke.getKeyStroke(KeyEvent.VK_Z, Event.CTRL_MASK), undoAction);
        keymap.addActionForKeyStroke(KeyStroke.getKeyStroke(KeyEvent.VK_Y, Event.CTRL_MASK), redoAction);
        jtext.setKeymap(keymap);
    }

    protected class UndoableTextEditListener implements UndoableEditListener {
        public void undoableEditHappened(UndoableEditEvent e) {
            undo.addEdit(e.getEdit());
            undoAction.updateUndoState();
            redoAction.updateRedoState();
        }
    }

    class UndoAction extends AbstractAction {
        public UndoAction() {
            super("Undo");
            setEnabled(false);
        }
        public void actionPerformed(ActionEvent e) {
            try {
                undo.undo();
            } catch (CannotUndoException ex) {
                System.out.println("Unable to undo: " + ex);
                ex.printStackTrace();
            }
            updateUndoState();
            redoAction.updateRedoState();
        }
        protected void updateUndoState() {
            if (undo.canUndo()) {
                setEnabled(true);
                putValue(Action.NAME, undo.getUndoPresentationName());
            } else {
                setEnabled(false);
                putValue(Action.NAME, "Undo");
            }
        }      
    }    

    class RedoAction extends AbstractAction {
        public RedoAction() {
            super("Redo");
            setEnabled(false);
        }
        public void actionPerformed(ActionEvent e) {
            try {
                undo.redo();
            } catch (CannotRedoException ex) {
                System.out.println("Unable to redo: " + ex);
                ex.printStackTrace();
            }
            updateRedoState();
            undoAction.updateUndoState();
        }
        protected void updateRedoState() {
            if (undo.canRedo()) {
                setEnabled(true);
                putValue(Action.NAME, undo.getRedoPresentationName());
            } else {
                setEnabled(false);
                putValue(Action.NAME, "Redo");
            }
        }
    } 

    protected void configureMenuItemStates(Component aComp) {
        JTextComponent jText = (JTextComponent) aComp;
        boolean hasText = (jText.getSelectedText() != null);
        boolean isEditable = jText.isEditable();

        cutItem.setEnabled(hasText && isEditable);
        cutItem.setVisible(isEditable);

        copyItem.setEnabled(hasText);

        selectItem.setEnabled(! hasText);

        findItem.setEnabled((jText.getDocument().getLength() > 0));
        hasText = (getClipboardString(clip) != null);
        pasteItem.setEnabled(hasText && isEditable);
        pasteItem.setVisible(isEditable);

        if(addUndo) {
          undoAction.updateUndoState();
          redoAction.updateRedoState();
        }
    }
}
