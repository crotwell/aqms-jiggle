package org.trinet.util.graphics.text;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import org.trinet.util.graphics.*;
// Modify to add key action binding for breaking out of a JComponent focusCycleRoot
// then invoking a master verify up chain of parents.
// Requires method mods/additions

public class VerifyingFocusManager extends DefaultFocusManager {
    public void processKeyEvent(Component focusedComponent,KeyEvent anEvent) {
        if(anEvent.getKeyCode() == KeyEvent.VK_TAB || anEvent.getKeyChar() == '\t') {
            /** If the focused component manages focus, let it do so
             *  if control is not pressed
             */
            if(focusedComponent instanceof JComponent) {
                JComponent fc = (JComponent) focusedComponent;
                if(fc.isFocusCycleRoot()) { // jdk1.4 isFocusCycleRoot, isManagingFocus deprecated
                    if(!((anEvent.getModifiers() & ActionEvent.CTRL_MASK) ==
                       ActionEvent.CTRL_MASK))
                        return;
                }
                if (fc instanceof InputVerification) {
                    if (! ((InputVerification) fc).getInputVerifier().shouldYieldFocus(fc)) {
                        anEvent.consume();
                        return;
                    }
                }
            }

            /** If this is not a key press, consume and return **/
            if(anEvent.getID() != KeyEvent.KEY_PRESSED){
                anEvent.consume();
                return;
            }

//	    System.out.println("\n\n******TAB pressed in processKeyEvent");
            if((anEvent.getModifiers() & ActionEvent.SHIFT_MASK) == ActionEvent.SHIFT_MASK)
                focusPreviousComponent(focusedComponent);
            else
                focusNextComponent(focusedComponent);
            anEvent.consume();
        }
    }
}
