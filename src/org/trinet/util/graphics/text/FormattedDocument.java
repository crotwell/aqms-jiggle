package org.trinet.util.graphics.text;
import javax.swing.text.*; 

import java.awt.Toolkit;
import java.text.*;
import java.util.Locale;

public class FormattedDocument extends PlainDocument {
    private java.text.Format format;
    private int strLength;

    public FormattedDocument() {}

    public FormattedDocument(java.text.Format f) {
        format = f;
	strLength = 0;
    }

    public FormattedDocument(java.text.Format f, int cols) {
        format = f;
	strLength = cols;
    }

    public java.text.Format getFormat() {
        return format;
    }

    public void insertString(int offs, String str, AttributeSet a) 
        throws BadLocationException {

        String currentText = getText(0, getLength());
//	System.out.println("strLength:" + strLength + " " + currentText.length());
	if (strLength > 0 && currentText.length() >= strLength) {
          Toolkit.getDefaultToolkit().beep();
	  return;
	}
        String beforeOffset = currentText.substring(0, offs);
        String afterOffset = currentText.substring(offs, currentText.length());
        String proposedResult = beforeOffset + str + afterOffset;

        try {
	    if (proposedResult.equals("")) return;
            format.parseObject(proposedResult);
            super.insertString(offs, str, a);
        } catch (ParseException e) {
            Toolkit.getDefaultToolkit().beep();
            System.err.println("Formatted Document insertString- parse error: " + proposedResult);
        }
    }

    public void remove(int offs, int len) throws BadLocationException {
        String currentText = getText(0, getLength());
        String beforeOffset = currentText.substring(0, offs);
        String afterOffset = currentText.substring(len + offs,
                                                   currentText.length());
        String proposedResult = beforeOffset + afterOffset;

        try {
            if (proposedResult.length() != 0)
                format.parseObject(proposedResult);
            super.remove(offs, len);
        } catch (ParseException e) {
            Toolkit.getDefaultToolkit().beep();
            System.err.println("Formatted Document remove- parse error: " + proposedResult);
        }
    }
}
