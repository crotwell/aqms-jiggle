package org.trinet.util.graphics;
import org.trinet.util.graphics.QuadristateButtonModel.State;

import java.awt.Component;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.tree.TreePath;

public class CheckboxTreeCellExceptLeafRenderer extends DefaultCheckboxTreeCellRenderer {

    public CheckboxTreeCellExceptLeafRenderer () { super(); }

    public Component getTreeCellRendererComponent(JTree tree, Object object, boolean selected, boolean expanded,
                               boolean leaf, int row, boolean hasFocus) {


        this.label.getTreeCellRendererComponent(tree, object, selected, expanded, leaf, row, hasFocus);

        if (tree instanceof CheckboxTree) {
            TreeCheckingModel checkingModel = ((CheckboxTree) tree).getCheckingModel();
            TreePath path = tree.getPathForRow(row);
            this.checkBox.setEnabled(checkingModel.isPathEnabled(path));
            boolean checked = checkingModel.isPathChecked(path);
            boolean greyed = checkingModel.isPathGreyed(path);
            if (checked && !greyed) {
                this.checkBox.setState(State.CHECKED);
	        this.checkBox.setBackground(UIManager.getColor("Tree.selectionBackground"));
            }
            if (!checked && greyed) {
                this.checkBox.setState(State.GREY_UNCHECKED);
	        this.checkBox.setBackground(UIManager.getColor("Tree.selectionBackground"));
            }
            if (checked && greyed) {
                this.checkBox.setState(State.GREY_CHECKED);
	        this.checkBox.setBackground(UIManager.getColor("Tree.selectionBackground"));
            }
            if (!checked && !greyed) {
                this.checkBox.setState(State.UNCHECKED);
	        this.checkBox.setBackground(UIManager.getColor("Tree.textBackground"));
            }
            this.checkBox.setVisible(! leaf);
        }
        return this;
    }
}
