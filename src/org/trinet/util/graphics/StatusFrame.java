package org.trinet.util.graphics;
import java.awt.*;
import javax.swing.*;

/** A centered informational box with no buttons. */
public class StatusFrame extends JDialog {

    boolean showProgress = true;

    JLabel textLabel = new JLabel();
    JLabel graphicLabel = new JLabel();
    JProgressBar progressBar = new JProgressBar(0, 100);

    public StatusFrame() {
        this((Dialog) null, "", "", null, false);
    }

    public StatusFrame(String title, String text, Icon icon) {
        this((Dialog) null, title, text, icon, false);
    }
 
    public StatusFrame(String title, String text, Icon icon, boolean showProgress) {
        this((Dialog) null, title, text, icon, showProgress);
    }

    public StatusFrame(Dialog owner, String title, String text, Icon icon, boolean showProgress) {
        super((Dialog) owner);
        makeGUI(title, text, icon, showProgress);
    }
    public StatusFrame(Frame owner, String title, String text, Icon icon, boolean showProgress) {
        super((Frame) owner);
        makeGUI(title, text, icon, showProgress);
    }

    private void makeGUI(String title, String text, Icon icon, boolean showProgress) {
          this.showProgress = showProgress;
          try  {
               jbInit();
          }
          catch(Exception e) {
               e.printStackTrace();
          }
          set(title, text, icon);
    }

    public void set(String title, String text,  Icon icon) {
        setTitle(title);
        setText(text);
        setIcon(icon);
        if (showProgress) setProgress(0);
    }

     /** Set the text in the text panel. */
    public void setText(String text) {
        textLabel.setText(text);
        pack();
    }

     public void setIcon(Icon icon) {
        if (icon != null) {
            graphicLabel.setIcon(icon);
            pack();
        }
     }

     public void setProgress(int percent) {
         if (showProgress) {
             progressBar.setValue(percent);
             progressBar.setString(percent+"%");
         }
     }

     public void setProgress(double percent) {
        setProgress( (int) percent);
     }

     private void jbInit() throws Exception {
          setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
          JPanel mainPanel = new JPanel();
          mainPanel.setLayout(new BorderLayout());
          mainPanel.add(graphicLabel, BorderLayout.WEST);
          mainPanel.add(textLabel, BorderLayout.CENTER);
          mainPanel.setMinimumSize(new Dimension(200, 150));
          mainPanel.setPreferredSize(new Dimension(200, 150));
          if (showProgress) {
            mainPanel.add(progressBar, BorderLayout.SOUTH);
            progressBar.setStringPainted(true);
          }
          this.getContentPane().add(mainPanel);
          pack();
          centerDialog();
     }

    /** Center the dialog on the screen */
    protected void centerDialog() {
        Dimension screenSize = this.getToolkit().getScreenSize();
        Dimension size = this.getSize();
        screenSize.height = screenSize.height/2;
        screenSize.width = screenSize.width/2;
        size.height = size.height/2;
        size.width = size.width/2;
        int y = screenSize.height - size.height;
        int x = screenSize.width - size.width;
        this.setLocation(x,y);
    }

  /*
     public static void main(String[] args) {
          ImageIcon defIcon = new ImageIcon ("images/swing-64.gif");
          StatusFrame statusFrame = new StatusFrame("Test frame", "Something's happening", defIcon);
          statusFrame.setVisible(true);
          statusFrame.setProgress(43);
     }
  */
}
