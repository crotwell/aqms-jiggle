package org.trinet.util.graphics;

import javax.swing.JFrame;

/**
 * Generic help window that displays HTML documents.
 */
public class HTMLHelp extends JFrame {

    public HTMLHelp(String helpFile, String title) {
        super("Help");

        if (title != null) {
            setTitle(title);        // set title to caller's
        }

        setBounds( 200, 25, 400, 400);
        HtmlPane html = new HtmlPane(helpFile);

        getContentPane().add(html);

        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        setVisible(true);
    }
} // end of class
