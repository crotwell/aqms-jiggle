package org.trinet.util.graphics.table;
import javax.swing.event.*;
import javax.swing.table.*;
public class JasiListTableModelEvent extends TableModelEvent {

      protected boolean listElementOrderChanged = false;

      public JasiListTableModelEvent(TableModel tableModel) {
          this(tableModel, false);
      }

      public JasiListTableModelEvent(TableModel tableModel, boolean listElementOrderChanged) {
          super(tableModel);
          this.listElementOrderChanged = listElementOrderChanged;
      }

      public boolean wasOrderChanged() { return listElementOrderChanged; }
}
