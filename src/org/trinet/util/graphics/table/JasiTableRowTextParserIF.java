package org.trinet.util.graphics.table;
import javax.swing.*;
public interface JasiTableRowTextParserIF extends TableRowTextParserIF {
  public void setRowModel(JasiListTableModelIF model);
  public void createInputTextDialog(JComponent parserOwningComponent);
}
