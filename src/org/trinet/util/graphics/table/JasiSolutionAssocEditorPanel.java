// TODO: property/menu settable option to reset default "stale" commit state after fresh load
// 
// Usually association relation tree
// ph.sol
// amp.sol amp.mag
// mag.sol amp.sol amp.mag
//
//? enable/disable of button actions related to modified state.
package org.trinet.util.graphics.table;
import java.beans.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;
import org.trinet.jasi.*;
import org.trinet.jasi.engines.*;
import org.trinet.jdbc.*;
import org.trinet.util.*;

public class JasiSolutionAssocEditorPanel extends JasiMagnitudeAssocEditorPanel {
    protected boolean loadAltSol = true; // a test aww
    protected boolean loadWf = false; // "true "won't work since WaveletGroup class is not a JasiCommitableIF - aww
    protected JasiPhaseListPanel phasePanel;
    protected JasiWaveformListPanel wfPanel;
    protected JasiMagListPanel   magPanel;
    protected JasiSolutionListPanel   altSolPanel;
    //private MouseAdapter aMouseAdapter; // removed so kate can edit solution readings 07/15/2004 -aww

    private boolean loadAmpsByOrigin = true;

    public JasiSolutionAssocEditorPanel() {super();}
    public JasiSolutionAssocEditorPanel(Solution sol) {
        this(sol,null);
    }
    public JasiSolutionAssocEditorPanel(Solution sol, Magnitude mag) {
        super(sol,mag);
    }

    protected void setSplitPaneBorders(JSplitPane jsp) { 
        initSplitPaneBorders(jsp, new Color(130,160,100));
    }

    protected boolean hasModifiedTable() {
        // Loading mag readings lists synchs through listeners also updating the
        // respective sol reading lists thus reseting sol state as needing commit,
        // if we don't ever edit these mag readings (hasUpdateEnabled() == false) here
        // we can finesse below the modified check so that after a load of the JMAEPanel
        // data this check here doesn't cause the commit modified dialog to popup if the
        // after data has just been read from the db and no edits have been made before 
        // user exits.
        //if (super.hasModifiedTable()) return true; // see above note for below kludge:
        if (
            (ampPanel != null && ampPanel.hasUpdateEnabled() && ampPanel.hasModifiedTable())
            || 
            (codaPanel != null && codaPanel.hasUpdateEnabled() && codaPanel.hasModifiedTable())
           ) return true; 
        return
          ((phasePanel != null) ? phasePanel.hasModifiedTable() : false)  ||
          ((magPanel != null) ? magPanel.hasModifiedTable() : false);
          // altSolPanel not editable so user can't change state of this table
          // || ((altSolPanel != null) ? altSolPanel.hasModifiedTable() : false);
    }
 
    protected void addTableModelListener(TableModelListener tml) {
        super.addTableModelListener(tml);
        if (phasePanel != null) phasePanel.addTableModelListener(tml);
        if (magPanel != null) magPanel.addTableModelListener(tml);
        if (altSolPanel != null) altSolPanel.addTableModelListener(tml);
    }
    protected boolean reloadDataLists(boolean reloadLists) {
        if (reloadLists) {
          loadOption = JOptionPane.YES_OPTION;
          // override checks for existing list data in panel Solution, in any data in its list, prompt to verify load
          Solution sol = getSelectedSolution(); // aww test 07/13/2004
          if (
              sol.getPhaseList().size() > 0 ||
              sol.getAmpList().size() > 0   || 
              sol.getCodaList().size() > 0
              //|| sol.getMagList().size() > 0  
             )
                  loadOption = getDataListLoadOption();
        }
        else {
          loadOption = JOptionPane.NO_OPTION;
        }
        return (loadOption == JOptionPane.YES_OPTION) ? true : false;
    }
    protected void loadData(boolean reloadLists) {
        Solution sol = getSelectedSolution();
        if (sol == null) {
          aLog.logTextnl("FYI - No selected solution for: " +
                          getClass().getName());
          return;
        }
        boolean queryDB = reloadDataLists(reloadLists);
        aLog.logText("Loading lists for event id: " + StringSQL.valueOf(getSolutionId()) +
                        " prefor : " + StringSQL.valueOf(getSolutionOriginId())
                    );
        //
        // GET stale and commit states here
        boolean needsCommit = sol.getNeedsCommit();
        boolean isStale = sol.isStale();
        if (queryDB) sol.resetStatusFlags(); // not stale or needing commit

        if (loadWf) {
          if (queryDB) loadWaveformList();
          //wfPanel.setAssocSolution(sol);
          wfPanel.setList(sol);
        }
        //
        if (queryDB) loadPhaseList();
        phasePanel.setAssocSolution(sol);

        if (queryDB) loadMagList();
        magPanel.setAssocSolution(sol);

        if (loadAmps) {
          if (queryDB) loadAmpList();  // load prefor assoc
          ampPanel.setAssocSolution(sol); 
        }

        if (loadCoda) {
          if (queryDB) loadCodaList(); // load prefor assoc
          codaPanel.setAssocSolution(sol);
        }

        if (loadAltSol) {
          if (queryDB) loadAltSolList();
          // instead of below extend SolutionPanel as AlternatePanel
          // with implementation of setAssocSolution method 
          altSolPanel.setList((ActiveArrayListIF) sol.getAlternateSolutions());
        }

        // RESET original stale and commit states here after the load methods above!!!
        if (queryDB) {
          sol.setStale(isStale);
          sol.setNeedsCommit(needsCommit);
        }
        Debug.println(" End loadData() sol: " + sol.getId() +" " + sol.getSummaryStateString(sol,sol.magnitude));
        
        aLog.logText(" phases: " + sol.getPhaseList().size());
        aLog.logText(" mags: " + sol.getMagList().size());
        aLog.logText(" amps: " + sol.getAmpList().size());
        aLog.logText(" codas: " + sol.getCodaList().size());
        aLog.logText(" wf: " + sol.getWaveformList().size());
        aLog.logTextnl(" AltSols: " + sol.getAlternateSolutions().size());
        if (sol.magnitude != null) {
          aLog.logTextnl("Prefmag " + sol.magnitude.toDumpString());
        }
        //reportSummaryState(); // DEBUG
    }

    // Invoke clear() to avoid duplicating readings with new object instances.
    // Invoking clear() may not be needed if load association uses addOrReplace
    // Loading lists, may effect stale state, whether or not db changed
    // We have assume always stale, since can't depend on knowlege of db edits 
    // unless data was known to be in a db archival, only modified by user.
    // Else db table change would have to trigger a flagged "modified" on Event
    // Need a user option for this assumption to toggle stale or not, 
    // in the load methods, perhaps a setting static attribute (global)
    // based on application settings (properties).
    //
    // NOTE: associate as implemented in getBySolution methods appends to
    // input Solution list. If not, we need to implement an add append to
    // the list here using addorReplaceAll(getBy...) logic.
    // Also if the modelList were split "cloned" from input solution list,
    // to preserve its autonomy with getBy doing the association add to the
    // input argument sol, we would need logic like:
    //   modelList.addAll(Phase.create().getBySolution(sol.getId.longValue()));
    //   modelList.assignAllTo(sol);
    // thus uses "ghost" Solution to load, to avoid primary sol changes
    // affiliates parent sol with modelList but does not add to parent sol list 
    //
    protected void loadPhaseList() {
        Solution sol = getSelectedSolution();
        //if (sol.getPhaseList().size() > 0) sol.getPhaseList().clear();
        // adding phases made origin stale and any prefmag is stale if origin dependent thus don't listen to load
        sol.listenToPhaseListChange(false);
        sol.loadPhaseList(true); // arg true clears list b4 reload
        sol.listenToPhaseListChange(true);
    }
    protected void loadMagList() {
        Solution sol = getSelectedSolution();
        // below resets prefmag, thus sol.hasChanged() set so => needsCommit == true
        //sol.loadMagList(true); // mag associate does add to list
        if (sol.getMagList().size() > 0) sol.getMagList().clear();
        sol.loadPrefMags(); // mag associate adds event preferred to list
    }
    // see notes above loadPhaseList about behavior alternatives
    protected void loadAmpList() {
        Solution sol = getSelectedSolution();
        //! below line clears all Mag amps if coupled with sol list listeners
        // using clear(false) won't notify mags, but then list instances are different
        //if (sol.getAmpList().size() > 0) sol.getAmpList().clear();
        
        if (loadAmpsByPrefmag()) sol.loadAmpList(true);  // amps in list may not related to prefmag 
        else sol.loadPrefMagAmpList(true);  // amps in list must be related to prefmag of type
    }

    private boolean loadAmpsByPrefmag() {

      loadAmpsByOrigin = editorProps.getBoolean("loadOriginAmpsByOriginAssoc"); 

      JRadioButton byOriginButton = new JRadioButton("Load By Origin Association");
      byOriginButton.setSelected(loadAmpsByOrigin);
      byOriginButton.addActionListener(
        new ActionListener() {
          public void actionPerformed(ActionEvent evt) {
            loadAmpsByOrigin = true;
          }
      });
      JRadioButton byPrefmagButton = new JRadioButton("Load By Prefmag Association");
      byPrefmagButton.setSelected(! loadAmpsByOrigin);
      byPrefmagButton.addActionListener(
        new ActionListener() {
          public void actionPerformed(ActionEvent evt) {
            loadAmpsByOrigin = false;
          }
      });
      ButtonGroup bgroup = new ButtonGroup(); 
      bgroup.add(byOriginButton);
      bgroup.add(byPrefmagButton);
      JPanel jp = new JPanel();
      jp.add(byPrefmagButton);
      jp.add(byOriginButton);
      JOptionPane.showMessageDialog(null, jp, "Select amp loading option", JOptionPane.QUESTION_MESSAGE);

      return loadAmpsByOrigin;

    }

    // see notes above loadPhaseList about behavior alternatives
    protected void loadCodaList() {
        Solution sol = getSelectedSolution();
        //! below line clears all Mag coda if coupled with sol list listeners
        // using clear(false) won't notify mags, but then list instances are different
        //if (sol.getCodaList().size() > 0) sol.getCodaList().clear();
        //sol.loadCodaList(true);  // possibly list input may not related to prefmag 
        sol.loadPrefMagCodaList(true);  // possibly list input may not related to prefmag 
    }
    protected void loadAltSolList() {
        Solution sol = getSelectedSolution();
        //if (sol.getAlternateSolutions().size() > 0) sol.getAlternateSolutions().clear();
        sol.loadAltSolList(true);
    }

    protected void loadWaveformList() {
        Solution sol = getSelectedSolution();
        sol.loadWaveformList(true);
    }

    protected JComponent createDataComponent() {
        phasePanel = createPhasePanel();
        magPanel = createMagPanel();
        JTabbedPane jtb = new JTabbedPane();
        jtb.addTab(phasePanel.getPanelName(), null, phasePanel, "arrival table rows");
        jtb.addTab(magPanel.getPanelName(), null, magPanel, "magnitude table rows");
        if (loadAmps) {
             ampPanel = createAmpPanel();
             jtb.addTab(ampPanel.getPanelName(), null, ampPanel, "amplitude table rows");
        }
        if (loadCoda) {
             codaPanel = createCodaPanel();
             jtb.addTab(codaPanel.getPanelName(), null, codaPanel, "coda table rows");
        }
        if (loadAltSol) {
             altSolPanel = createAltSolutionPanel();
             jtb.addTab(altSolPanel.getPanelName(), null, altSolPanel, "alt sol table rows");
        }
        if (loadWf) {
             wfPanel = createWfPanel();
             jtb.addTab(wfPanel.getPanelName(), null, wfPanel, "waveform table rows");
        }
        JPanel topPanel = new JPanel();
        topPanel.setPreferredSize(
                  new Dimension(PREFERRED_PANEL_WIDTH,PREFERRED_PANEL_HEIGHT));
        topPanel.setLayout(new BorderLayout());
        topPanel.add(jtb, BorderLayout.CENTER);
        addLowerControlsToPanel(topPanel);
        return topPanel;
    }
    /*
    private MouseAdapter createMouseAdapter() {
      if (aMouseAdapter == null) {
        aMouseAdapter = new MouseAdapter() {
          public void mousePressed(MouseEvent evt) {
            int clickCount = evt.getClickCount();
            if (clickCount >= 2) {
              JOptionPane.showMessageDialog(getTopLevelAncestor(),
                  "Edit via associated magnitude panel data.");
            }
          }
        };
      }
      return aMouseAdapter;
    }
    */
    protected void setComponentUpdateState(boolean tf) {
        super.setComponentUpdateState(tf);
        if (ampPanel != null) { // don't data to be edited here defer to mag association
          //ampPanel.setUpdateDB(false);
          ampPanel.setUpdateDB(tf); // aww 07/14/2004 enable edits, do they propagate?
          //ampPanel.dataTable.addMouseListener(createMouseAdapter());
        }
        if (codaPanel != null) {// don't data to be edited here defer to mag association
          //codaPanel.setUpdateDB(false);
          codaPanel.setUpdateDB(tf);
          //codaPanel.dataTable.addMouseListener(createMouseAdapter());
        }
        if (phasePanel != null) phasePanel.setUpdateDB(tf);
        if (magPanel   != null) magPanel.setUpdateDB(tf);
        if (altSolPanel != null) altSolPanel.setUpdateDB(false);
    }
    protected void setComponentCommitState(boolean tf) {
        super.setComponentCommitState(tf);
        if (phasePanel != null) phasePanel.setCommitEnabled(tf);
        if (magPanel   != null) magPanel.setCommitEnabled(tf);
        if (altSolPanel != null) altSolPanel.setCommitEnabled(false);
    }
    protected void enableComponentCommitButton(boolean tf) {
        super.enableComponentCommitButton(tf);
        if (phasePanel != null) phasePanel.setCommitButtonEnabled(tf);
        if (magPanel   != null) magPanel.setCommitButtonEnabled(tf);
        if (altSolPanel != null) magPanel.setCommitButtonEnabled(false);
    }
    protected void commit() {
        Debug.println(getClass().getName() + " DEBUG commit");
        if (phasePanel != null) phasePanel.commit();
        if (magPanel != null) magPanel.commit();
        //if (altSolPanel != null) altSolPanel.commit();
        super.commit();  // does the coda and amp lists
    }
    protected void updateLists() {
      updateLists(true);
    }
    protected void updateLists(boolean solveWhenStale) {
        Debug.println(getClass().getName() + " DEBUG update lists");
        //wait don't solve upon updating the solution coda and amp lists:
        super.updateLists(false);
        if (phasePanel != null) phasePanel.updateList();
        //System.out.println("DEBUG JSAEP ampList size: " + getSelectedSolution().getAmpList().size());
        //System.out.println("DEBUG JSAEP codaList size: " + getSelectedSolution().getCodaList().size());
        //System.out.println("DEBUG JSAEP phaseList size: " + getSelectedSolution().getPhaseList().size());
        if (magPanel != null) {
          // wait don't solve upon updating mag assocLists:
          magPanel.updateLists(false);
          //magPanel.updateList(); // this is done by above method
        }
        if (altSolPanel != null) altSolPanel.updateList();
        if (wfPanel != null) wfPanel.updateList();
        // recalculate if stale? calcmag: pref or selected mag?
        //if (solveWhenStale) doEngineSolve(); // aww removed 06/29/2004 
        if (autoSolve && solveWhenStale) doEngineSolve(); // require autoSolve property 06/29/2004 aww ?
    }
    protected void doEngineSolve() {
        Debug.println("DEBUG JSAEP doEngineSolve stale sol,mag: " + hasStaleSol() + " " + hasStaleMag());
        if (hasStaleSol() || hasStaleMag()) { // added hasStaleMag() 03/03 aww
          if (! isAutoSolve() &&
              ! confirm("Solve for solution using updated list data for id: " +
                      StringSQL.valueOf(getSolutionId()) )  ) {
                  return;
          }
          //Note: beware of run-away recursive list updating
          if (delegate != null) delegate.solve(getSelectedSolution(),getSelectedMagnitude()); // try this out
          //delegate.locate();
          //delegate.calcMag();  // since mag may be stale as well 12/11/02 aww
        }
    }
    protected JasiPhaseListPanel createPhasePanel() {
        JasiPhaseListPanel aPanel = new JasiPhaseListPanel();
        aPanel.setPanelName("Phase");
        aPanel.setTextLogger(getTextLogger());
        this.addPropertyChangeListener(EngineIF.SOLVED, aPanel);
        //aPanel.addAncestorListener(this);
        return aPanel;
    }
    protected JasiMagListPanel createMagPanel() {
        JasiMagListPanel aPanel = new JasiMagListPanel();
        aPanel.setPanelName("Mag");
        aPanel.setTextLogger(getTextLogger());
        aPanel.addEngineDelegate(delegate);
        this.addPropertyChangeListener(EngineIF.SOLVED, aPanel);
        //aPanel.addAncestorListener(this);
        aPanel.addPropertyChangeListener(this); // until mess fixed, kludge deals with sol amp,coda list synch
        aPanel.setAutoSolve(isAutoSolve());
        return aPanel;
    }
    protected JasiSolutionListPanel createAltSolutionPanel() {
        JasiSolutionListPanel aPanel = new JasiSolutionListPanel();
        aPanel.setPanelName("AltSolution"); // 01/06/03 changed name aww
        aPanel.setTextLogger(getTextLogger());
        // list length may change or prefSol so listen
        this.addPropertyChangeListener(EngineIF.SOLVED, aPanel);
        //aPanel.addAncestorListener(this);
        //aPanel.disableParsing();
        return aPanel;
    }
    protected JasiWaveformListPanel createWfPanel() {
        JasiWaveformListPanel aPanel = new JasiWaveformListPanel();
        aPanel.setPanelName("Waveform"); // 01/06/03 changed name aww
        aPanel.setTextLogger(getTextLogger());
        return aPanel;
    }

    public void propertyChange(PropertyChangeEvent e) {
        String propName = e.getPropertyName();
        if (propName.equals(EngineIF.SOLVED)) {
           Object value = e.getNewValue();
           scrollTextPane();
           firePropertyChange(propName, e.getOldValue(),e.getNewValue());
        }
        // below code not needed if model lists not cloned
        else if (propName.equals("magReadings")) {
          // problem: synch mag and sol amplists if different objects for same db data?
          // could invoke addOrReplaceWithSameTime() on sol list for each mag reading?
          Solution sol = getSelectedSolution();
          Magnitude mag = (Magnitude) e.getNewValue();
          if ( mag.isCodaMag() ) {
            if (codaPanel != null) {
              codaPanel.setList(sol);
            }
          }
          else if ( mag.isAmpMag() ) {
            if (ampPanel != null) {
              ampPanel.setList(sol);
            }
          }
        }
        else if (propName.equals("magTableChanged")) {  // propagate selected magnitude
           firePropertyChange(propName, e.getOldValue(), e.getNewValue());
        }
    }
    //override superclass method to used panel selected mag
    public boolean locate() {
      boolean status  = super.locate();
      if (phasePanel != null) phasePanel.repaint();  // changed 2008/01/10 aww
      return status;
    }
    /*
    // Do we need fire event to all panels listening to updateLists (after inserts) before engines revise loc or mag?
    public boolean locate() {
      if (phasePanel != null) phasePanel.updateList();  // changed 12/06/2002 aww as update list 
      return super.locate();
    }
    public boolean calcMag() {
        if (magPanel != null) magPanel.updateAssocLists(false);  // changed 10/08/2002 aww as update list before load from db
        Magnitude mag = (magPanel == null) ? null : (Magnitude) magPanel.getSelectedRow();
        setSelectedMagnitude(mag);
        if (mag != null) {
          if ( mag.isCodaMag() ) {
            if (mag.getCodaList().size() <= 0) {
                getSelectedSolution().getCodaList().clear(); // add does not discrimate duplicate
                mag.loadCodaList(); // from db, adds to sol list auto as side effect
              }
            }
          }
          else if ( mag.isAmpMag() ) {
            if (mag.getAmpList().size() <= 0) {
              getSelectedSolution().getAmpList().clear(); // add does not discrimate duplicate
              mag.loadAmpList(); // from db, adds to sol list auto as side effect
              //System.out.println("JSAEP DEBUG sol amplist size: " + getSelectedSolution().getAmpList().size());
            }
          }
        }
        return super.calcMag();
    }
*/
}
