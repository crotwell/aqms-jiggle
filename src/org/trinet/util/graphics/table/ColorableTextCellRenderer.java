package org.trinet.util.graphics.table;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;
public class ColorableTextCellRenderer extends TextCellRenderer
    implements ColorableRendererIF {

  private Border focusBorder = BorderFactory.createLineBorder(Color.black,2);
  private Border noFocusBorder = BorderFactory.createEmptyBorder(1,1,1,1);

  protected Color defaultBackground = getBackground();
  protected Color defaultForeground = getForeground();

  public ColorableTextCellRenderer() { 
    this(-1, null);
  }
  public ColorableTextCellRenderer(int fontSize) { 
    this(fontSize, null);
  }
  public ColorableTextCellRenderer(Font font) { 
    this(-1, font);
  }
  public ColorableTextCellRenderer(int fontSize, Font font) { 
    super(fontSize, font);
  }
  public void setBackground(Color c) {
    super.setBackground(c);
  }
  public void setForeground(Color c) {
    super.setForeground(c);
  }

  public void setDefaultForeground(Color c) {
    defaultForeground = c;
    setForeground(c);
  }
  public void setDefaultBackground(Color c) {
    defaultBackground = c;
    setBackground(c);
  }

  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
                 int row, int col) {
    Component comp = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, col);

    if (hasFocus) {
      setBorder(focusBorder);
      //setBackground(table.getSelectionBackground());
    }
    else setBorder(noFocusBorder);

    if (isSelected) {
      setBackground(table.getSelectionBackground());
      setForeground(table.getSelectionForeground());
    }
    else  {
      setForeground(table.getForeground());
      setBackground(defaultBackground);
    }
    return comp;
  }
}
