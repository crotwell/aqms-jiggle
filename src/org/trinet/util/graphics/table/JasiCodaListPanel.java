//
//TODO: Refactor to super Class the methods generic to abstract getReadingList type
//
package org.trinet.util.graphics.table;
import javax.swing.*;
import org.trinet.jasi.*;
import org.trinet.jasi.coda.*;
import org.trinet.util.*;
public class JasiCodaListPanel extends AbstractJasiMagnitudeAssocPanel {
    public JasiCodaListPanel() {
       this(null, false);
    }
    public JasiCodaListPanel(JTextArea textArea, boolean updateDB) {
        super(textArea, updateDB);
        initPanel();
    }
    protected void initPanel() { 
        dataTable = new JasiCodaListTable(new JasiCodaListTableModel());
        super.initPanel();
    }
    protected void setList(Solution sol) {
        aMag = null;    // avoids appending values to prior mag in update of solution
        aSol = sol;
        setList(sol.getCodaList());
    }
    protected void setList(Magnitude mag) {
        if (aMag.isCodaMag()) setList(aMag.getReadingList());
        else
          System.err.println(panelName +
               " Error setting model CodaList from Magnitude input not a CodaMag (perhaps AmpMag)"); 
    }
 
    /*
    protected int updateList() {
      if (! confirmUpdate()) return 0;
      return synchInputList();
    }
    */
    /* See notes on Amp List Panel analog
    protected int synchInputList() {
      Debug.println("DEBUG updating panel list for " + panelName);
      if (! isListUpdateNeeded()) return 0;
      CodaList aList = (CodaList) getList();
      if (inputList == aList) { // identical list object no-op
        setListUpdateNeeded(false);
        return 0;
      }
      boolean added = false;
      for (int idx=0; idx < aList.size(); idx++){
        Coda coda = (Coda) aList.get(idx);
        // always update, but coda may not be related to prefmag:
        // test trial of  "replacement" in lieu of "add" 02/03 aww: 
        added = aSol.getCodaList().add(coda);
        // "set" state code below redundant with list listeners:
        if (added) aSol.setNeedsCommit(true);
        if (aMag != null && aMag.isCodaMag()) {
          added = aMag.getCodaList().add(coda);
          if (added) aMag.setStale(true);
        }
      }
      setListUpdateNeeded(false);
      return aList.size();
    }
   */
  
    protected boolean insertRow() { 
        Coda newCoda = Coda.create();
        getModel().initRowValues(newCoda);
        return insertRow(newCoda);
    }
    protected boolean canParseInput() {
      boolean status = super.canParseInput();
      if (!status) return status;
      // changed logic to allow Sol input and generic amp mag - aww 02/08/2005
      return ( aMag != null && aMag.isCodaMag() ) || aSol != null;
    }
    protected void enableInsertButtons() {
      // changed logic to allow Sol input and generic amp mag - aww 02/08/2005
      if ( (aMag != null && aMag.isCodaMag()) || aSol != null ) {
        super.enableInsertButtons();
      }
      else {
        super.disableInsertButtons();
      }
    }
}
