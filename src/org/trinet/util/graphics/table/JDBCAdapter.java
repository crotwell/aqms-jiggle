package org.trinet.util.graphics.table;
/**
 * An adaptor, transforming the JDBC interface to the TableModel interface.
 * @version 1.0 09/15/98
 * @author Philip Milne original SUNSOFT version
 * @author highly modified by AWalter USGS 9/98
 */
 
import java.math.*;
import java.util.*;
import java.sql.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import org.trinet.jdbc.*;
import org.trinet.util.*;

public class JDBCAdapter extends AbstractTableModel {
    private static final String DEFAULT_SCHEMA = "TRINETDB";
    private int maxRows = 100;
    private Connection  conn;
    private Statement   stmt;
    private Statement   lockstmt;
    private Statement   updstmt;
    private ResultSet   queryResultSet;
    private ResultSet   resultSet;
    private ResultSetMetaData rsMD;
    private DatabaseMetaData  dbMD;
    private int               connID = -1;
    private int               updateCount = -1;
    private static final String strLock1 = "LOCK TABLE ";
    private static final String strLock2 = " IN ROW EXCLUSIVE MODE NOWAIT";
    private String []         dbTableNames;
    private ColumnData []     columnDataArray;

    private String[]  queryTables;
    private String[]  colNames;
    private String[]  colTbls;
    private String[]  colLabel;
    private int[]     colTypes;
    private int[]     colWidth;
    private int[]     colPrecision;
    private int[]     colScale;
    private boolean[] colAKey;

    private boolean updateDBflag = false;
    private boolean insertDBflag = false;
    private int insertedRowIndex = -1;

    private ArrayList rows = new ArrayList();
    private String lastUpdateQuery = "";
    private static final String catalog = null;
    private static String schemaLogin = "";
     
    public JDBCAdapter() {}
    
    public int connect(String url, String driverName,
                       String user, String passwd) {
        schemaLogin = user.toString().toUpperCase();
        System.out.println("JDBCAdapter: Table connect() username/schemaLogin: " + schemaLogin);
        try {
            conn = JDBCConnectionPool.getConnection(url, driverName, user, passwd);
            if (conn != null) {
                stmt = conn.createStatement();
                updstmt = conn.createStatement();
                lockstmt = conn.createStatement();
                conn.setAutoCommit(false);
                dbMD = conn.getMetaData();
                connID = JDBCConnectionPool.getConnectionId(url, driverName, user, passwd);
            }
        }
        catch (SQLException ex) {
            System.err.println("JDBCAdapter: Connect failure; SQLException");
            SQLExceptionHandler.prtSQLException(ex);
            connID = -1;
        }
        return connID;
    }

    public int connect(int id) {
        conn = JDBCConnectionPool.getConnection(id);
        connID = -1;
        if (conn != null) {
            try {
                stmt = conn.createStatement();
                updstmt = conn.createStatement();
                lockstmt = conn.createStatement();
                dbMD = conn.getMetaData();
                conn.setAutoCommit(false);
                queryResultSet = null;
                rsMD = null;
                connID = id;
            }
            catch (SQLException ex) {
                System.err.println("JDBCAdapter: Connect with integer id failure; SQLException");
                SQLExceptionHandler.prtSQLException(ex);
            }
        }
        else {
            System.err.println("JDBCAdapter: Connect invalid connection id");
        }
        return connID;
    }

    public boolean execute(String query) {
        boolean rval = false;
        resultSet = null;
        updateCount = -1;        
        try {
            if(stmt == null) stmt = conn.createStatement();
            System.out.println("JDBCAdapter: execute SQL statement:\n" + query);
            rval =  stmt.execute(query);
            if (rval) resultSet = stmt.getResultSet();
            else updateCount = stmt.getUpdateCount();
        }
        catch (SQLException ex) {
            System.err.println("JDBCAdapter: execute statement failure.");
            ex.printStackTrace();
        }
        return rval;
    }

    public int executeQuery(String query) {
      return executeQuery(query, maxRows);
    }

    public int executeQuery(String query, int maxRows) {
        System.out.println("JDBCAdapter: executeQuery SQL statement:\n" + query);
        if (conn == null) {
            System.err.println("JDBCAdapter: executeQuery null connection failure; must connect to database to execute query.");
            return 0;
        }
        String schema;
        try {
            if(stmt == null) stmt = conn.createStatement();
            int idx = query.indexOf("FROM");
            int idx2 = query.indexOf("WHERE");
            String tblString = null;
            if (idx > 0 && idx2 > 0) {
                if (idx+5 < idx2-2) {
                    tblString = query.substring(idx+5,idx2-1);
                    findQueryTables(tblString);
                    }
            }
            else if (idx > 0) {
                int ifor = query.indexOf("FOR UPDATE");
                String tmp = null;
                if (ifor >= 0) tblString = query.substring(idx+5,ifor);
                else tblString = query.substring(idx+5);
//                Debug.println("JDBCAdapter tblString: " + tblString);
                findQueryTables(tblString);
            }
            if (queryTables.length == 0 || idx == -1) {
//                Debug.println("JDBCAdapter: QUERY TABLES LENGTH = 0 " );
                return 0;
            }
            else {
                if (query.indexOf("UPDATE") >= 0) updateDBflag = true;
                else updateDBflag = false;
                First: for (int i=0; i<queryTables.length; i++) {
                    int indexSchema = queryTables[i].indexOf('.');
                    if (indexSchema >= 0) schema = queryTables[i].substring(0, indexSchema);
                    else schema = schemaLogin;
                    dbTableNames = getTableNames(schema);

                    for (int j=0; j<dbTableNames.length; j++) {
//                        Debug.println("dbTableNames:" + dbTableNames[j]);
                        if(queryTables[i].substring(indexSchema+1).equals(dbTableNames[j])) {
//                            Debug.println("JDBCAdapter: Found table: " + queryTables[i]);
                            columnDataArray = getColumnData(queryTables[i]);
/*
                            for (int k = 0 ; k < columnDataArray.length ; k++) {
                              Debug.println(k + " " + columnDataArray[k].toString());
                            }
*/
                            continue First;
                        }
                    }
                    System.out.println("JDBCAdapter: executeQuery requested table: " + queryTables[i] + " not in schemaLogin");
                    continue First;
//                    return 0;
                }
            }    
            if (maxRows > 0) {
                stmt.setMaxRows(maxRows);
            }
            else {
              System.out.println("JDBCAdapter: No MAXROWS specified ... truncating table to 100 rows");
              stmt.setMaxRows(100);  // temporary limit set here
//              java.awt.Toolkit.getDefaultToolkit().beep();
            }
//                Debug.println("EXECUTING QUERY");
            queryResultSet = stmt.executeQuery(query);
            rsMD = queryResultSet.getMetaData();
            int numberOfColumns =  rsMD.getColumnCount();
//                Debug.println("JDBCAdapter: Table column count:" + numberOfColumns);
            // Get the column names and types; cache them for later use.
            colNames = new String[numberOfColumns];
            colTypes = new int[numberOfColumns];
            colWidth = new int[numberOfColumns];
            colPrecision = new int[numberOfColumns];
            colScale = new int[numberOfColumns];
            colTbls = new String[numberOfColumns];
            colLabel = new String[numberOfColumns];
            colAKey = new boolean[numberOfColumns];
//aww            String keys[] = getPrimaryKeys(queryTables[0]);
            findColumnTables(query);
            for(int colIndex = 0; colIndex < numberOfColumns; colIndex++) {
                colNames[colIndex] = rsMD.getColumnName(colIndex+1);
                colLabel[colIndex] = rsMD.getColumnLabel(colIndex+1);
                colTypes[colIndex] = rsMD.getColumnType(colIndex+1);
                colWidth[colIndex] = rsMD.getColumnDisplaySize(colIndex+1);
                colPrecision[colIndex] = rsMD.getPrecision(colIndex+1);
                colScale[colIndex] = rsMD.getScale(colIndex+1);
                if (colTbls[colIndex] == null) {
                    colTbls[colIndex] = rsMD.getTableName(colIndex+1);
                }
                if(colTbls[colIndex].equals("")) {
                    if(queryTables.length == 1) colTbls[colIndex] = queryTables[0]; // rsMD.getTableName(colIndex+1);
                }

                String keys[] = getPrimaryKeys(colTbls[colIndex]);
//                Debug.println("JDBCAdapter: table keys:" + keys.length);
                colAKey[colIndex] = false;
                for ( int i = 0; i<keys.length; i++) {
                    if (colNames[colIndex].equals(keys[i])) colAKey[colIndex] = true;
                }
/* comment out the print
                Debug.print(" ColumnName:" + colNames[colIndex]);
//                Debug.print("\n");
                Debug.print(" Label:" + colLabel[colIndex]);
//                Debug.print("\n");
                Debug.print(" Type:" + colTypes[colIndex]);
//                Debug.print("\n");
                Debug.print(" Width:" + colWidth[colIndex]);
//                Debug.print("\n");
                Debug.print(" Precision:" + colPrecision[colIndex]);
//                Debug.print("\n");
                Debug.print(" Scale:" + colScale[colIndex]);
//                Debug.print("\n");
                Debug.print(" Table:" + colTbls[colIndex]);
//                Debug.print("\n");
                Debug.print(" AKey:" + colAKey[colIndex]);
                Debug.print("\n");
// end of comment  */
            }

            // Get all rows.
            rows = new ArrayList(128);
            int j = 0;
//            Object obj;
            while (queryResultSet.next()) {
                j++;
//                Debug.println("JDBCAdapter: executeQuery row count:" + j);
                ArrayList newRow = new ArrayList(64);
                for (int i = 1; i <= numberOfColumns; i++) {
//                    obj = queryResultSet.getObject(i);
//                    if (obj == null) Debug.println("JDBCAdapter: executequery object null");
//                    else Debug.println("JDBCAdapter: execute query Objectname:" + obj.getClass().getName());
//                    newRow.add(obj);

                    newRow.add(queryResultSet.getObject(i));
                }
                rows.add(newRow);
            }
//            Debug.println("JDBCAdapter: Found table.");
//            for (int i = 0; i < queryTables.length; i++) { Debug.println( queryTables[i] + " "); }
//            Debug.println(" column count: " + numberOfColumns);
//            Debug.println("JDBCAdapter: row count: " + rows.size());
            fireTableChanged(null); // Tell the listeners a new table has arrived.
            return rows.size();
        }
        catch (SQLException ex) {
            System.err.println("JDBCAdapter: executeQuery failure.");
            SQLExceptionHandler.prtSQLException(ex);
            return -1;
        }
    }

    private void findColumnTables(String queryString) {
        int iend = queryString.indexOf("FROM");
        String colNames = queryString.substring(7,iend);
        StringTokenizer st = new StringTokenizer(colNames, ", \t\n\r\f");
        int itoke = st.countTokens();
        if (itoke != getColumnCount()) {
            if (queryString.indexOf('*') < 0) {
                System.out.println("JDBCAdapter: findColumnTables: column count token mismatch");
                return;
            }
            else return;
        }

        String colname;
        for (int i = 0; i<itoke; i++) {
            colname = st.nextToken();
            if (colname.indexOf('.') > 0) colTbls[i] = colname.substring(0,colname.indexOf('.'));
            else colTbls[i] = null;
//            Debug.println("JDBCAdapter: findColumnTables:" + colname + " Table: " + colTbls[i]);
        }
    }

    private void findQueryTables(String str) {
        StringTokenizer st = new StringTokenizer(str, ", \t\n\r\f");
        int itoke = st.countTokens();
        queryTables = new String[itoke];
        for (int i = 0; i<itoke; i++) {
            queryTables[i] = st.nextToken();
//            Debug.println("JDBCAdapter: findQueryTables:" + queryTables[i]);
        }
    }

    public void resetStatement() {
        try {
            stmt.cancel();
            stmt = null;
        }
        catch (SQLException ex) {
            System.err.println("SQLException for JDBCAdapter resetStatement()");
            System.err.println(ex.getMessage());
            ex.printStackTrace();
        }
    }

    public void close() throws SQLException {
        System.out.println("JDBCAdapter: Closing database connection");
        if (queryResultSet != null) {
            queryResultSet.close();
            queryResultSet = null;
        }
        if (resultSet != null) {
            resultSet.close();
            resultSet = null;
        }
        if (stmt != null) {
            stmt.close();
            stmt = null;
        }
        if (updstmt != null) {
            updstmt.close();
            updstmt = null;
        }
        if (lockstmt != null) {
            lockstmt.close();
            lockstmt = null;
        }
        if (conn != null) {
          JDBCConnectionPool.close(connID);
          conn = null;
          connID = -1;
        }
        rsMD  = null;
        dbMD  = null;
    }

    protected void finalize() throws Throwable {
        System.out.println("JDBCAdapter: Finalize JDBCAdapter");
 //       close();
        super.finalize();
    }
    
// Begin added methods - AWW
    public DatabaseMetaData getDBMetaData() {
        return dbMD;
    }

    public String [] getPrimaryKeys(String tableName) {
        ArrayList kn = new ArrayList(16);
        int [] ks = new int[8];
        int ncomp=0;
        ResultSet rs = null;
        String schema;
        try {
//            Debug.println("JDBCAdapter: getPrimaryStrings tableName:" + tableName);
            int indexSchema = tableName.indexOf('.');
            if (indexSchema >= 0) schema = tableName.substring(0, indexSchema);
            else schema = schemaLogin;
                dbMD = conn.getMetaData();
            rs = dbMD.getPrimaryKeys(catalog, schema, tableName.substring(indexSchema+1));
            while (rs.next()) {
                try {
                    kn.add(rs.getString("COLUMN_NAME"));
                    ks[ncomp] = rs.getInt("KEY_SEQ") - 1;
                    ncomp++;
                }
                catch (SQLException ex) {
                    System.err.println("JDBCAdapter: getPrimaryKeys rs.getResults SQLException");
                    SQLExceptionHandler.prtSQLException(ex);
                }
            }
        }
        catch (SQLException ex) {
            System.err.println("JDBCAdapter: dbMD.getPrimaryKeys SQLException");
            SQLExceptionHandler.prtSQLException(ex);
        }
        finally {
            try {
                rs.close();
            }
            catch (SQLException ex) {
                System.err.println("JDBCAdapter: getPrimaryKeys rs.close SQLException");
                SQLExceptionHandler.prtSQLException(ex);
            }
        }
        
        String[] keyNames = new String[kn.size()];
//        Debug.println("JDBCAdapter: ncomp:" + ncomp + " kn.size:" + kn.size());
        int j;
        for (int i = 0; i < ncomp; i++) {
            j = ks[i];
            keyNames[i] = (String) kn.get(j);   
        }
        return keyNames;
    }

    public String [] getTableNames(String schema) {
        String[] tableNames = null;
        ArrayList vtr = new ArrayList(128);
        try {
            dbMD = conn.getMetaData();
        }
        catch (SQLException ex) {
            System.err.println("JDBCAdapter: getTableNames getMetaData SQLException");
            SQLExceptionHandler.prtSQLException(ex);
        }
        vtr.addAll(getMetaDataTableNames(DEFAULT_SCHEMA));
//        vtr.addAll(getMetaDataTableNames(null));
        vtr.addAll(getMetaDataTableNames(schema));
        tableNames = new String[vtr.size()];
        vtr.toArray(tableNames);
        return tableNames;
    }
    
    private Collection getMetaDataTableNames(String schema) {
        String [] types = {"TABLE"};        // types[0] = "TABLE";
        ResultSet rs = null;
        ArrayList vtr = new ArrayList(128);
        try {
            rs =  dbMD.getTables(catalog, schema, "%", types);
            while (rs.next()) {
                try {
//                String tmpStr = rs.getString("TABLE_NAME");
//                Debug.println("result table:" + tmpStr);
//                vtr.add(tmpStr);
                    vtr.add(rs.getString("TABLE_NAME"));
                }
                catch (SQLException ex) {
                    System.err.println("JDBCAdapter: getTableNames rs.getString SQLException");
                    SQLExceptionHandler.prtSQLException(ex);
                }
            }
        }
        catch (SQLException ex) {
            System.err.println("JDBCAdapter: getTableNames dbMD.getTables SQLException");
            SQLExceptionHandler.prtSQLException(ex);
        }
        finally {
            try {
                rs.close();
            }
            catch (SQLException ex) {
                System.err.println("JDBCAdapter: getTableNames rs.close SQLException");
                SQLExceptionHandler.prtSQLException(ex);
            }
        }
        return vtr;
    }

    public ResultSet getColumnMetaData(String tableName){
        ResultSet rs = null;
        String schema;
        try {
            int indexSchema = tableName.indexOf('.');
            if (indexSchema >= 0) schema = tableName.substring(0, indexSchema);
            else schema = schemaLogin;
                dbMD = conn.getMetaData();
            rs = dbMD.getColumns(catalog, schema, tableName.substring(indexSchema+1), null);
        }
        catch (SQLException ex) {
            System.err.println("JDBCAdapter: getColumnMetaData dbMD.getColumns SQLException");
            SQLExceptionHandler.prtSQLException(ex);
        }
        return rs;
    }
   
    public String [] getColumnNames(String tableName) {
        String[] colNames = null;
        ArrayList tn = new ArrayList(64);
        ResultSet rs = null;
        String schema;
        try {
            int indexSchema = tableName.indexOf('.');
            if (indexSchema >= 0) schema = tableName.substring(0, indexSchema);
            else schema = schemaLogin;
                dbMD = conn.getMetaData();
            rs = dbMD.getColumns(catalog, null, tableName.substring(indexSchema+1), null);
            while (rs.next()){
                try {
                    tn.add(rs.getString("COLUMN_NAME"));
                }
                catch (SQLException ex) {
                    System.err.println("JDBCAdapter: getColumnNames rs.getString SQLException");
                    SQLExceptionHandler.prtSQLException(ex);
                }
            }
        }
        catch (SQLException ex) {
            System.err.println("JDBCAdapter: getColumnNames dbMD.getColumns SQLException");
            SQLExceptionHandler.prtSQLException(ex);
        }
        finally {
            try {
                rs.close();
            }
            catch (SQLException ex) {
                System.err.println("JDBCAdapter: getColumnNames rs.close SQLException");
                SQLExceptionHandler.prtSQLException(ex);
            }
        }

        colNames = new String[tn.size()];
        tn.toArray(colNames);
        return colNames;
    }
    
    public int getColumnType(int icol) {
      if (icol >= 0 && icol <= colTypes.length) return colTypes[icol];
      else return -1;
    }

    public int getColumnScale(int icol) {
      if (icol >= 0 && icol <= colTypes.length) return colScale[icol];
      else return -1;
    }

    public ColumnData [] getColumnData(String tableName) {
        String schema;
        String tablestr = tableName;
        int indexSchema = tablestr.indexOf('.');
        if (indexSchema >= 0) {
            schema = tableName.substring(0, indexSchema);
            tablestr = tableName.substring(indexSchema+1);
        }
        else schema = schemaLogin;
//        Debug.println("JDBCAdapter: getColumnData dbMD schema:" + schema + " table:" + tablestr);
        return ColumnData.getColumnData(conn, schema.toUpperCase(), tablestr.toUpperCase());
    }
 
    public boolean isColumnAKey(int colIndex) {
        if (colIndex < 0 || colIndex > colAKey.length) return false;
        return colAKey[colIndex];
    }

    public boolean isColumnNullable(int colIndex) {
        if (columnDataArray == null) return false;
        if (colIndex < 0 || colIndex > columnDataArray.length || columnDataArray.length == 0) return false;
        return columnDataArray[colIndex].isNullable;
    }

    public int getQueryTableCount() {
        if (queryTables == null) return 0;
        return queryTables.length;
    }

    public String getQueryTableName(int index) {
        if (index < 0 || index > queryTables.length) return "";
        return queryTables[index];
    }

    public int getKeyColumnCount() {
        return colAKey.length;
    }

    public void setInsertedRowIndex(int index) {
        if (index > getRowCount()) return;
        insertedRowIndex = index;
    }

    public int getInsertedRowIndex() {
        return insertedRowIndex;
    }

    public boolean isInsertable() {
        return insertDBflag;
    }

    public void disableInsert() {
        insertDBflag = false;
    }

    public void enableInsert() {
        insertDBflag = true;
    }

    public boolean isUpdatable() {
        return updateDBflag;
    }

    public void disableUpdate() {
        updateDBflag = false;
    }

    public void enableUpdate() {
        updateDBflag = true;
    }

    public int getUpdateCount() {
        return updateCount;
    }

    public ResultSet getResultSet() {
        return resultSet;
    }

    public int getTableKeyColumnIndex(String colName) {
        if (colName == null) return -1;
        for (int i = 0 ; i < colNames.length; i++) {
            if (colName.trim().equals(colNames[i]) ) return i;
        }
        return -1;
    }

// End of added methods - AWW
//////////////////////////////////////////////////////////////////////////
//
//             Implementation of the TableModel Interface
//
//////////////////////////////////////////////////////////////////////////

    public String getColumnName(int colIndex) {
        if (colNames == null) return "";
        if (colIndex < 0 || colIndex > colNames.length) return "";
        return colNames[colIndex];
    }

    public int findColumn(String name) {
        for (int i = 0; i < colNames.length; i++) {
            if (colNames[i].equals(name)) return i;
        }
        return -1;
    }

    public Class getColumnClass(int colIndex) {
      Class rval = null;
      if (columnDataArray == null) columnDataArray = getColumnData(colTbls[colIndex]);
//      Debug.println("JDBCAdapter: getColumnClass table:" +  colTbls[colIndex] + " columnDataArray.length:" + columnDataArray.length);
      for ( int i=0 ; i < columnDataArray.length; i++) {
        if (columnDataArray[i].columnName.equals(colNames[colIndex]) ) {
          String type  =  columnDataArray[i].typeName;
          if (type.equals("VARCHAR2")) {
            rval = String.class;
          }
          else if (type.equals("NUMBER")) {
            if (columnDataArray[i].decimalDigits == 0)  rval = Integer.class; // colScale[colIndex];
            else  rval = Double.class;
          }
          else if (type.equals("FLOAT")) {
            rval = Double.class;
          }
          else if (type.equals("DATE")) {
            rval = java.sql.Date.class;
//          Debug.println("JDBCAdapter DATE: col:" + colIndex + " name:" + colNames[colIndex] + " className:" + type + " numberType:" + columnDataArray[i].dataType);
          }
          else if (type.equals("TIMESTAMP")) {
            rval = java.sql.Timestamp.class;
//          Debug.println("JDBCAdapter TIMESTAMP: col:" + colIndex + " name:" + colNames[colIndex] + " className:" + type + " numberType:" + columnDataArray[i].dataType);
          }
//          Debug.println("JDBCAdapter: col:" + colIndex + " name:" + colNames[colIndex] +
//                        " className:" + type + " numberType:" + columnDataArray[i].dataType);
          break;
        }
      }
      if (rval == null) {
//        Debug.println("JDBCAdapter: getColumnClass: DEBUG Default Object.class type for col: " + colIndex  + " colName:" + colNames[colIndex]);
//        Debug.println("JDBCAdapter: getColumnClass: DEBUG columnDataArray.length:" + columnDataArray.length);
        if (columnDataArray.length > 0) Debug.println("JDBCAdapter: getColumnClass: DEBUG class not found for cda:\n" +
                 columnDataArray[colIndex].toString());
        rval = Object.class;
      }
      return rval;
    }

    public boolean isCellEditable(int rowIndex, int colIndex) {
        if (insertDBflag && insertedRowIndex == rowIndex) {
          return true;
        }
        else if (insertDBflag) return false;
        else if (! updateDBflag) return false;

// cell editing based on primary keys discrimination;
        boolean retVal = true;
        try {
            String tableName = rsMD.getTableName(colIndex+1);
            if (tableName == null) {
                System.out.println("JDBAdapter isCellEditable: Table name returned null.");
            }
            if (queryTables.length == 1) tableName = colTbls[colIndex];
//            Debug.println("JDBCAdapter: isCellEditable at getPrimaryKeys table:" + tableName);
            if (tableName.toUpperCase().indexOf("ASSOC") >= 0) return true;
            String [] pkeys = getPrimaryKeys(tableName);
            for (int ikey = 0; ikey < pkeys.length; ikey++) {
//                Debug.println("JDBCAdapter: irow:" + rowIndex + " column:" + colIndex + " ikey:" + ikey + " pkey[ikey]:" + pkeys[ikey]);
//                Debug.println("JDBCAdapter: findcolumn:" + findColumn(pkeys[ikey]));
                if (findColumn(pkeys[ikey]) == colIndex) {
                    retVal = false;
                    break;
                }
            }
        }
        catch (SQLException ex) {
            System.err.println("JDBCAdapter: isCellEditable rdMD.getTableName SQLException");
            SQLExceptionHandler.prtSQLException(ex);
            retVal = false;
        }
//        Debug.println("JDBCAdapter: isCellEditable:" + retVal);
        return retVal;
    }

    public int getColumnCount() {
        return colNames.length;
    }

    // Data cell methods

    public int getRowCount() {
        return rows.size();
    }

    public Object getValueAt(int aRow, int aColumn) {
        if (rows.get(aRow) == null) System.out.println("JDBCAdapter: getValueAt rows null");
        ArrayList row = (ArrayList)rows.get(aRow);
        return row.get(aColumn);
    }

    public String dbRepresentation(int colIndex, Object value) {
      if (value == null) return "null" ;
      int type = -1;
      if (columnDataArray == null) columnDataArray = getColumnData(colTbls[colIndex]);
      for ( int i=0 ; i < columnDataArray.length; i++) {
        if (columnDataArray[i].columnName.equals(colNames[colIndex]) ) {
          String typeName  =  columnDataArray[i].typeName;
          if (typeName.equals("VARCHAR2")) {
            type = Types.VARCHAR; 
          }
          else if (typeName.equals("NUMBER")) {
            if (columnDataArray[i].decimalDigits == 0)  type = Types.INTEGER; // colScale[colIndex];
            else  type = Types.DOUBLE;
          }
          else if (typeName.equals("FLOAT")) {
            type =  Types.FLOAT;
          }
          else if (typeName.equals("DATE")) {
            type = Types.DATE;
          }
          else if (typeName.equals("TIMESTAMP")) {
            type = Types.TIMESTAMP;
          }
          else if (typeName.equals("OTHER")) {
            System.out.println("JDBCAdapter dbRepresentation DEBUG at colIndex:" + colIndex + " getType typeName == OTHER");
            type = Types.OTHER;
          }
        }
      }
//        int type = colTypes[colIndex];
//        Debug.println("JDBCAdapter: index: " + colIndex + " type:" + type + " class: " + value.getClass().getName());
        switch(type) {
        case Types.DATE:
        case Types.TIME:
        case Types.TIMESTAMP:
//                Debug.println("dbRepresentation date value: " + StringSQL.valueOf(value));
            String tmp =  StringSQL.valueOf(value);
            if (tmp.length() > 21) tmp =  tmp.substring(0,20) + "'";
//                Debug.println("dbRep datestring: " + tmp);
            return tmp;
        case Types.INTEGER:
        case Types.DOUBLE:
        case Types.FLOAT:
        case Types.REAL:
        case Types.NUMERIC:
        case Types.DECIMAL:
        case Types.CHAR:
        case Types.VARCHAR:
        case Types.LONGVARCHAR:
        case Types.OTHER:
//            return value.toString() ;
//                Debug.println("dbRepresentation of value: " + StringSQL.valueOf(value));
            return StringSQL.valueOf(value);
        case Types.BIT:
            return ((Boolean)value).booleanValue() ? "1" : "0";
        default:
            return "'" + value.toString() + "'";
        }
    }

    public void setValueAt(Object value, int rowIndex, int colIndex) {
        try {
//            Debug.println("JDBCAdapter: setValueAt row,col:" + rowIndex + "," + colIndex);
            if (rsMD == null) System.out.println("JDBCAdapter: setValueAt rsMD is null");
            int nullable = rsMD.isNullable(colIndex+1);
            if (nullable != 1) {
              if (value == null) {
                System.out.println("JDBCAdapter: setValueAt NULL value not allowed");
                return;
              }
              else if (value instanceof String) {
                if ( ((String) value).trim().toUpperCase().equals("NULL")) {
                  System.out.println("JDBCAdapter: setValueAt NULL string value not allowed");
                  return;
                }
              }
            }
        }
        catch (SQLException ex) {
            System.err.println("JDBCAdapter: setValueAt rsMD.isNullable SQLException");
            SQLExceptionHandler.prtSQLException(ex);
        }
        if (updateDBflag) updateRowValueDB(value, rowIndex, colIndex) ;

        ArrayList dataRow = (ArrayList)rows.get(rowIndex);
//        dataRow.set(value, colIndex);
        dataRow.add(colIndex, value);
//        rows.set(dataRow, rowIndex);
        fireTableCellUpdated(rowIndex, colIndex);
    }

// UPDATE OF DATABASE 
    public int updateRowValueDB(Object value, int rowIndex, int colIndex) {
//        if (value == null) Debug.println ("updateRowValueDB value == null");
//        else Debug.println("updateRowValueDB value:" + value.getClass().getName() + " : " + value.toString());
        int rval = -1;
        String tableName;
        try {
            if (queryTables.length == 1) {
                tableName = queryTables[0];
                if (tableName.length() == 0) {
                    System.out.println("JDBCAdapter: updateRowValueDB tableName empty string.");
                    return rval;
                }
            }
            else {
                tableName = rsMD.getTableName(colIndex+1); // data may be from more than one table 
                if (tableName == null) {
                    System.out.println("JDBCAdapter: updateRowValueDB rsMD.getTableName returned null.");
                    return rval;
                }
            }
//            Debug.println("updateRowValueDB at getPrimaryKeys");
            String [] pkeys = getPrimaryKeys(tableName);
            String columnName = getColumnName(colIndex);
            String query =
                "UPDATE " + tableName +
                " SET " + columnName + " = " +
                dbRepresentation(colIndex, value) +
                "  WHERE  " ;
            int ikeycol;
            for (int ikey = 0; ikey < pkeys.length; ikey++) {
                if (ikey > 0) query = query + " AND ";
                ikeycol = findColumn(pkeys[ikey]);
                query = query + pkeys[ikey] + " = " +
                    dbRepresentation(ikeycol,getValueAt(rowIndex, ikeycol)); 
            }
            if (conn == null) {
                System.out.println("JDBCAdapter: updateRowValueDB: connection null");
                return rval;
            }
            else {
                if (! lastUpdateQuery.equals(query)) {
                    System.out.println("JDBCAdapter: updateRowValueDB: Old value:" + dbRepresentation(colIndex,getValueAt(rowIndex, colIndex))); 
                    System.out.println("JDBCAdapter: updateRowDB SQL statement:\n" + query);
                    try {
                        if (lockstmt == null) lockstmt = conn.createStatement();
                        lockstmt.execute(strLock1 + tableName + strLock2);
                    }
                    catch (SQLException ex) {
                        System.err.println("JDBCAdapter: updateRowDB execute lock statement SQLException");
                        SQLExceptionHandler.prtSQLException(ex);
                        return rval;
                    }
                    if (updstmt == null) updstmt = conn.createStatement();
                    rval = updstmt.executeUpdate(query);
                    conn.commit();
//                    Debug.println("JDBCAdapter: updateRowDB executeUpdate after commit");
                    lastUpdateQuery = query;
                }
            }
        }
        catch (SQLException ex) {
            System.err.println("JDBCAdapter: updateRowDB executeUpdate statement SQLException");
            SQLExceptionHandler.prtSQLException(ex);
        }
        return rval;
    }
// end of UPDATE 

    public int addRow(int rowIndex) {
            ArrayList newRow = (ArrayList) makeNullRow();
        rows.add(rowIndex, newRow);
//        int rowIndex = rows.size() - 1;
        fireTableRowsInserted(rowIndex,rowIndex); 
        return rows.size();
    }
    
    public int addRow(int rowIndex, Object [] values) {
        if (values.length != getColumnCount()) {
            System.err.println("JDBCAdapter: addRow values length not equal to getColumnCount.");
            return -1;
        }
/* must have correspondence between column names and values expected
        for (int i = 0; i < getColumnCount(); i++) {
            if (values[i] != null)  {
                if (! values[i].getClass().getName().equals(getColumnClass(i).getName()) ) {
                    System.err.println("JDBCAdapter: addRow mismatched class for column:" + i +
                        "  ColumnClass:" + getColumnClass(i).getName() + " ValueClass:" + values[i].getClass().getName());
                    return -1;
                }
            }
            else {
// or leave as null object - noop
            } 
        }
*/
            ArrayList newRow = (ArrayList) makeNewRow(values);
        rows.add(rowIndex, newRow);
//        int rowIndex = rows.size() - 1;
        fireTableRowsInserted(rowIndex,rowIndex); 
        return rows.size();
    }
    
    List makeNewRow(Object[] values) {
        ArrayList newRow = new ArrayList(64);
        for (int i = 0; i < getColumnCount(); i++) {
                newRow.add(values[i]);
        }
        return newRow;
    }
    
    List makeNullRow() {
        ArrayList nullRow = new ArrayList(64);
        Object nullObject = null;
//        Class className;
        for (int i = 0; i < getColumnCount(); i++) {
            nullRow.add(nullObject);
        }
        return nullRow;
    }

// DELETE row from TableModel and DB
    public int deleteRow(int rowIndex) {
        int rval = -1;
          if (rowIndex > -1 && rowIndex < rows.size()) {
            if (insertedRowIndex != rowIndex) {
                rval = deleteRowFromDB(rowIndex);
                if (rval > 0) {
//                    Debug.println("JDBCAdapter: deleteRowFromDB dropped " + rval + " rows from database");
                }
                else {
                    System.err.println("JDBCAdapter: delrow unable to drop table row at index " + rowIndex + " from database");
                }
            }
            rows.remove(rowIndex);
            fireTableRowsDeleted(rowIndex,rowIndex);
//            Debug.println("JDBCAdapter: delrow dropped row " + rowIndex + " from table");
            if (insertedRowIndex == rowIndex) {
                insertDBflag = false;
                insertedRowIndex = -1;
            }
        }
        return rval;
    }
    
// DELETE row from DB
    public int deleteRowFromDB(int rowIndex) {
        int rval = -1;
        if (queryTables.length != 1) {
          System.err.println("JDBCAdapter: deleteRowFromDB SQL query includes columns from more than 1 table.");
          return rval;
        }
        try {
            String [] pkeys = getPrimaryKeys(queryTables[0]);
            String query =
                "DELETE FROM " + queryTables[0] + " WHERE " ;
            int ikeycol;
            for (int ikey = 0; ikey < pkeys.length; ikey++) {
                if (ikey > 0) query = query + " AND ";
                ikeycol = findColumn(pkeys[ikey]);
                query = query + pkeys[ikey] + " = " +
                    dbRepresentation(ikeycol, getValueAt(rowIndex, ikeycol));
            }
            System.out.println("JDBCAdapter: deleteRowFromDB SQL statement:\n" + query);
            try {
                if (lockstmt == null) lockstmt = conn.createStatement();
                lockstmt.execute(strLock1 + queryTables[0] + strLock2);
            }
            catch (SQLException ex) {
                System.err.println("JDBCAdapter: updateRowDB execute lock statement SQLException");
                SQLExceptionHandler.prtSQLException(ex);
                return rval;
            }
            if (updstmt == null) updstmt = conn.createStatement();
            rval = updstmt.executeUpdate(query);
            conn.commit();
        }
        catch (SQLException ex) {
            System.err.println("JDBCAdapter: deleteRowFromDB SQL executeUpdate statement SQLException");
            SQLExceptionHandler.prtSQLException(ex);
        }
        return rval;
    }

// INSERT table row into DB
    public int insertIntoDB(JTable tbl) {
        if (! (tbl.getModel() instanceof JDBCAdapter) ) return -1;
        int rowIndex = tbl.getSelectedRow();
        int rval = insertIntoDB(rowIndex);
//        if (rval > 0) tbl.clearSelection();
        return rval;
    }

    public int insertIntoDB () {
        int rval = insertIntoDB(insertedRowIndex);
        if (rval > 0) insertedRowIndex = -1;
        return rval;
    }

    public int insertIntoDB (int rowid) {
        int rval = -1;

        if (queryTables.length != 1) {
          System.err.println("JDBCAdapter: insertIntoDB: Only allowing inserts for single table, not join views.");
//          java.awt.Toolkit.getDefaultToolkit().beep();
          return rval;
        }

//        int rowid = insertedRowIndex;

        if (rowid < 0) {
          System.err.println("JDBCAdapter: insertIntoDB: Invalid row index; index < 0.");
          return rval;
        }
        else if (rowid > getRowCount()) {
          System.err.println("JDBCAdapter: insertIntoDB: Invalid row index; index greater than table row count.");
          return rval;
        }
        else {
          StringBuffer strSQL = new StringBuffer(255);
          strSQL.append("INSERT INTO " + queryTables[0].toString() + "(" );
          for (int i = 0; i < getColumnCount(); i++) {
            strSQL.append(colNames[i]);
            strSQL.append(", ");
          }
          strSQL.delete(strSQL.length()-2, strSQL.length());
          strSQL.append(") VALUES (" );
          boolean doTrueTime = false; // for UTC -aww 2008/02/15
          for (int i = 0; i < getColumnCount(); i++) {
//            strSQL.append(StringSQL.valueOf(getValueAt(rowid,i))); 
            if (colNames[i].equalsIgnoreCase("LDDATE")) strSQL.append("SYS_EXTRACT_UTC(CURRENT_TIMESTAMP)");
            else {
                doTrueTime = (colNames[i].toUpperCase().indexOf("DATETIME") >= 0) ; // for UTC -aww 2008/02/15
                if (doTrueTime) strSQL.append("truetime.putEpoch("); // for UTC -aww 2008/02/15
                strSQL.append(dbRepresentation(i, getValueAt(rowid, i)));
                if (doTrueTime) strSQL.append(",'UTC')"); // for UTC -aww 2008/02/15
            }
            strSQL.append(", ");
          }
          strSQL.delete(strSQL.length()-2, strSQL.length());
          strSQL.append(")");
          System.out.println("JDBCAdapter: insertToDB SQL statement:\n" + strSQL);

          try {
            try {
              if (lockstmt == null) lockstmt = conn.createStatement();
              lockstmt.execute(strLock1 + queryTables[0] + strLock2);
            }
            catch (SQLException ex) {
              System.err.println("JDBCAdapter: updateRowDB execute lock statement SQLException");
              SQLExceptionHandler.prtSQLException(ex);
              return rval;
            }
            if (updstmt == null) updstmt = conn.createStatement();
            rval = updstmt.executeUpdate(strSQL.toString());
            conn.commit();
          }
          catch (SQLException ex) {
            System.err.println("JDBCAdapter: insertToDB executeUpdate SQLException");
            SQLExceptionHandler.prtSQLException(ex);
          }
          return rval;
        }
    }
}
