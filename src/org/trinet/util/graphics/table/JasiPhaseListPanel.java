package org.trinet.util.graphics.table;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;
import org.trinet.jasi.*;
import org.trinet.util.*;
    /*
    // WARNING: Location engine fires event to update table and resets
    // solution to unstale state, thus DON'T set stale by 
    // below method, because sol.isStale() should be false
    // after location updates values in table.
    public void tableChanged(TableModelEvent evt) {
            super.tableChanged(evt);
            if (aSol != null) aSol.setStale(true);
    }
    */
public class JasiPhaseListPanel extends AbstractJasiSolutionAssocPanel {
// table data model is aliased phaseList reference in the class's constructor.
    public JasiPhaseListPanel() {this(null,false);}
    public JasiPhaseListPanel(JTextArea textArea, boolean updateDB) {
        super(textArea,updateDB);
        initPanel();
    }
    protected void initPanel() { 
        //if (tableColumnOrderByName == null) this.tableColumnOrderByName = JasiPhaseListTableConstants.columnNames;
        this.dataTable = new JasiPhaseListTable(new JasiPhaseListTableModel());
        super.initPanel();
    }
    protected void setList(Solution sol) { 
        aSol = sol;
        setList(sol.getPhaseList());
    }
 
    /*
    protected int updateList() {
      if (! confirmUpdate()) return 0;
      return synchInputList();
    }
    */

    /* Could replace the method below by generic abstraction in superclass
    // Need application USER OPTION to select replacement strategy here:
    // Without list listerners the above replacement methods 
    // return phOut == phIn if phIn already in list, not replaced 
    // way to test for add would be to first find its index then do
    // the list set method here to decide if stale or not.
    //Phase phOut = (Phase) aSol.phaseList.addOrReplace(phIn);
    //Phase phOut = (Phase) aSol.phaseList.addOrReplaceWithSameTime(phIn);
    protected int synchInputList() {
      Debug.println("DEBUG updating panel list for " + panelName);
      if (! isListUpdateNeeded()) return 0;
      PhaseList aList = (PhaseList) getList();
      if (inputList == aList) {
          setListUpdateNeeded(false);
          return 0;
      }
      boolean added = false;
      for (int idx=0; idx < aList.size(); idx++) {
        Phase ph = aList.getPhase(idx);
        added = (Phase) aSol.phaseList.add(ph);
      }
      //below redundant, if done by list listeners, or model
      if (added) aSol.setStale(true); // only if added
      setListUpdateNeeded(false);
      Debug.println("DEBUG updated phasepanel input list size " +
                      aList.size() +
                      " output list size: " +
                      aSol.getPhaseList().size());
      //aSol.getPhaseList().dump(); //DEBUG
      return aList.size();
    }
   */

    protected boolean insertRow() { 
        Phase newPhase = Phase.create();
        getModel().initRowValues(newPhase);
        return insertRow(newPhase);
    }
}
