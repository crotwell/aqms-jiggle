package org.trinet.util.graphics.table;
import javax.swing.*;
import javax.swing.table.*;
import org.trinet.util.graphics.text.*;
import org.trinet.jdbc.datatypes.*;
public class JasiCodaListTable extends AbstractJasiListTable {
    public JasiCodaListTable(JasiCodaListTableModel tm) {
            super(tm);
            loadTableProperties("CodaListTable.props");
    }
    protected boolean configureColEditorRendererByName(TableCellEditorRenderer tcer,
              int modelIndex, String colName) {
          boolean setValues = super.configureColEditorRendererByName(tcer, modelIndex, colName);
          if (setValues) return true;
          if (colName.equals("UNITS")) {
            tcer.tcEditor = new ComboBoxCellEditor("UNITS",
              new String [] { "counts","unknown" });
            tcer.tcRenderer = new ColorableTextCellRenderer(font);
            setValues = true;
          }
          else if (colName.equals("PH")) {
            tcer.tcEditor = new ComboBoxCellEditor("PH", new String [] {"P","S","?"});
            tcer.tcRenderer = new ColorableTextCellRenderer(font);
            setValues = true;
          }
          else if (colName.equals("TYPE")) {
            tcer.tcEditor = new ComboBoxCellEditor("TYPE", new String [] {"a","d","h","?"});
            //((JComponent)((ComboBoxCellEditor)tcer.tcEditor).getComponent()).setToolTipText("h=humanEst,a=mc,d=md,?=unknown");
            tcer.tcRenderer = new ColorableTextCellRenderer(font);
            ((DefaultTableCellRenderer)tcer.tcRenderer).setToolTipText("a=mc,d=md,h=human estimate,?=unknown");
            setValues = true;
          }
          else if (colName.equals("DESC")) {
            tcer.tcEditor = new ComboBoxCellEditor("DESC", new String [] {
              "A","C","L","N","W","T"
            }); //"N", "R","T","NRT","RT","NT", "NR"
            tcer.tcRenderer = new ColorableTextCellRenderer(font);
            ((DefaultTableCellRenderer)tcer.tcRenderer).setToolTipText("A=atCutoff,C=clipped,L=lowAmp,N=noisy,W=windows,T=truncated");
            setValues = true;
          }
          else if (colName.equals("IWT")) {
            tcer.tcEditor = new ComboBoxCellEditor("IWT", new String [] {"0","1","2","3","4"});
            tcer.tcRenderer = new ColorableTextCellRenderer(font);
            //tcer.tcRenderer = new ComboBoxCellRenderer("Q", new String [] {"0","1","2","3","4"});
            setValues = true;
          }
          return setValues;
    }
}
