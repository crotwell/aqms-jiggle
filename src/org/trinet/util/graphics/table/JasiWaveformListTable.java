package org.trinet.util.graphics.table;
import org.trinet.jasi.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.table.*;
import java.util.Arrays;
import org.trinet.util.*;
import org.trinet.util.graphics.text.*;
import org.trinet.jdbc.datatypes.*;
public class JasiWaveformListTable extends AbstractJasiListTable {
    public JasiWaveformListTable(JasiWaveformListTableModel tm) {
            super(tm);
            loadTableProperties("WaveformListTable.props");
    }
    protected boolean configureColEditorRendererByName(TableCellEditorRenderer tcer,
            int modelIndex, String colName) {
          boolean setValues = super.configureColEditorRendererByName(tcer,modelIndex,colName);
          if (setValues)  return setValues;
          // add custom col render here
          return setValues;
    }
}
