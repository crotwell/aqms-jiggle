package org.trinet.util.graphics.table;
import java.awt.Font;
import java.text.*;
import javax.swing.*;
import org.trinet.jasi.*;
import org.trinet.util.graphics.text.*;
public abstract class AbstractJasiMagnitudeAssocPanel extends AbstractJasiSolutionAssocPanel {
    protected Magnitude aMag;

    protected AbstractJasiMagnitudeAssocPanel() {this(null,false);}

    protected AbstractJasiMagnitudeAssocPanel(JTextArea textArea, boolean updateDB) {
        super(textArea,updateDB);
    }

    protected void setAssocMagnitude(Magnitude mag) {
        aMag = mag;
        aSol = mag.getAssociatedSolution();
        //System.out.println("DEBUG AJMAP aMag.aSol: " + aSol.getId().longValue());
        ((AbstractJasiAssociationTableModel) getModel()).setAssocSolution(aSol); // aww 10/7/2002
        // below line needed to associate data with input magnitude (such as hand-entered input)
        ((AbstractJasiMagAssocTableModel) getModel()).setAssocMagnitude(mag); // 2007/05/29 - aww 
        setList(aMag);
    }
    protected Magnitude getAssocMagnitude() {
        return aMag;
    }
 
    protected abstract void setList(Magnitude mag);

    // the associated magnitude is stale
    protected boolean isStale() {
      return (aMag == null) ? false : aMag.isStale();
    }
    protected void setStale(boolean tf) {
      if (aMag != null) aMag.setStale(tf);
    }

/* override propertyChange method in parent
    public void propertyChange(java.beans.PropertyChangeEvent e) {
        debugProperty(e);
        if (e.getPropertyName().equals(EngineIF.SOLVED)) {
          if (dataTable.getRowCount() <= 0) return;
          Object newValue = e.getNewValue();
          Solution solvedSol = null;
          Magnitude solvedMag = null;
          if (newValue instanceof Solution) {
            solvedSol = (Solution) newValue;
            solvedMag = solvedSol.getPreferredMagnitude();
            if (solvedSol == aSol) informModel(); // just update fields  
          }
          else if (newValue instanceof Magnitude) {
            solvedMag = (Magnitude) newValue;
            solvedSol = solvedMag.getAssociatedSolution();
            // KLUDGE assumes sol and mag configured to have identical lists:
            //System.out.println("AJMAP debug solvedMag.getReadingList().size(): " + solvedMag.getReadingList().size());
            if (solvedSol == aSol) setList(solvedMag);
          }
        }
    }
*/
} // end of AbstractJasiMagntiudeAssocPanel class
