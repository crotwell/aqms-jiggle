package org.trinet.util.graphics.table;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import java.util.Arrays;
import org.trinet.util.*;

public class DBTable {
    static final int DEFAULT_FONT_SIZE = 12;
    static final Font DEFAULT_TABLE_FONT = new Font("Monospaced", Font.PLAIN, DEFAULT_FONT_SIZE);
    Font font = DEFAULT_TABLE_FONT;

    static final int DEFAULT_BLOCK_ROWS = 9;

    public JTable  tableRowHead;
    public JTable  tableView;
    TableSorterAbs  sorter = new TableSorterAbs();
    public JDBCAdapter  tableModel;
    JScrollPane  tableDB;

    Color defaultBgColor = new Color(1.f, 1.f, 0.7f); // light yellow;
    Color keyBgColor = new Color (1.f, 0.8f, 0.35f); // orange;
    Color nonNullBgColor = Color.green;
    Color selectedBgColor = Color.yellow;

//    int [] displayedColumns;

// DBTable constructor
    public DBTable(JDBCAdapter tableModel) {
	initTableModel(tableModel);
    }

    public void setTableCellBackgroundColors(Color defaultColor, Color keyColor, Color nonNullColor, Color selectedColor) { 
	if (defaultColor != null) defaultBgColor = defaultColor;
	if (keyColor != null) keyBgColor = keyColor;
	if (nonNullColor != null) nonNullBgColor = nonNullColor;
	if (selectedColor != null) selectedBgColor = selectedColor;
    }

/*
    public void setDisplayColumnIndices(int [] order) {
	displayedColumns = order;
    }

    void setTableDisplayColumns(JTable dataTable, int [] orderIndex) {
	if (orderIndex == null) return;
	if (orderIndex.length == 0) return;
	int colCount = dataTable.getColumnCount();
	if (orderIndex.length > colCount) return;

	TableColumnModel tcm = dataTable.getColumnModel();
	TableColumnModel tcm2 = new DefaultTableColumnModel();
	TableColumn col;
	for (int i = 0; i<colCount; i++) {
		col = tcm.getColumn(orderIndex[i]));
		if (col != null) tcm2.addColumn(col);
	}
	dataTable.setColumnModel(tcm2); 
    }
*/

    public JScrollPane createTable(JDBCAdapter tableModel) {
	initTableModel(tableModel);
	return createTable();
    }

    public JScrollPane createTable() {
//	Debug.println("DBTable setting up JTable scrollpane ...");
	tableView = new JTable(sorter);
	setTableAppearance(tableView);
        setTableSelectionState(tableView);
//	Debug.println("DBTable setting up RowHeader cell renderers/editors...");
	setTableColumnRenderersEditors(tableView, defaultBgColor, nonNullBgColor, selectedBgColor);
	initTableColumnSizes(tableView);
//	setTableDisplayColumns(tableview, displayedColumns);

	tableRowHead = createRowHeaderTable(tableView);
	setTableAppearance(tableRowHead);
        setTableSelectionState(tableRowHead);
	setTableColumnRenderersEditors(tableRowHead, keyBgColor, keyBgColor, selectedBgColor);
	initTableColumnSizes(tableRowHead);

	addListeners();

	tableDB = createTableScrollPane();

        return tableDB;
    }					// end of createTable() method

    private JScrollPane createTableScrollPane() {
	tableRowHead.setPreferredScrollableViewportSize(tableRowHead.getPreferredSize());
	tableView.setPreferredScrollableViewportSize(tableView.getPreferredSize());
	JScrollPane scrollpane = null;
        if (tableView.getColumnCount() <= 0) {
	  System.out.println("DBTable: tableView has no non-key columns");
	  scrollpane = new JScrollPane(tableRowHead);
	}
        else scrollpane = new JScrollPane(tableView);

	scrollpane.setRowHeaderView(tableRowHead);
	scrollpane.setCorner(JScrollPane.UPPER_LEFT_CORNER, tableRowHead.getTableHeader());

	int incr = tableView.getRowHeight() + tableView.getRowMargin();
	scrollpane.getVerticalScrollBar().setUnitIncrement(incr);
	scrollpane.getVerticalScrollBar().setBlockIncrement(incr*DEFAULT_BLOCK_ROWS);

	JButton jbDown = new JButton();
	jbDown.setActionCommand("D");
	ScrollingActionListener scrollingActionListener = new ScrollingActionListener();
	jbDown.addActionListener(scrollingActionListener);
	scrollpane.setCorner(JScrollPane.LOWER_RIGHT_CORNER, jbDown);

	JButton jbUp = new JButton();
	jbUp.setActionCommand("U");
	jbUp.addActionListener(scrollingActionListener);
	scrollpane.setCorner(JScrollPane.UPPER_RIGHT_CORNER, jbUp);

	return scrollpane;
    }

    public void setScrollIncrements(int unit, int block) {
	if (tableDB == null) return;
	tableDB.getVerticalScrollBar().setUnitIncrement(unit);
	tableDB.getVerticalScrollBar().setBlockIncrement(block);
    }

    private JTable createRowHeaderTable(JTable dataTable) {	// Make second table for row header
//	Debug.println("DBTable setting up RowHeader JTable scrollpane ...");
	TableColumnModel tcm = dataTable.getColumnModel();
	TableColumnModel tcm2 = new DefaultTableColumnModel();
	int nkeyCols = 0;
	TableColumn col;
	int count = tableModel.getColumnCount();
	for (int i = 0; i < count; i++) {
	    if (tableModel.isColumnAKey(i)) {
		col = tcm.getColumn(tcm.getColumnIndex(tableModel.getColumnName(i)));
		tcm.removeColumn(col);
		tcm2.addColumn(col);
		nkeyCols++;
	    }
	}
	JTable rowHeaderTable = new JTable(sorter); 
	rowHeaderTable.setColumnModel(tcm2);
	return rowHeaderTable;
    }

    private void initTableModel(JDBCAdapter model) {
	this.tableModel = model;
	sorter.setModel(model);
    }

    public void setTableFont(JTable table, Font font) {
	if (font == null) font = DEFAULT_TABLE_FONT;
	table.getTableHeader().setFont(font);
	table.setFont(font);
    }

    private void setTableAppearance(JTable table) {
	table.setDoubleBuffered(true);
	table.setToolTipText(null); // "double-click edits data field only if SELECT FOR UPDATE query"
	table.setAutoResizeMode(tableView.AUTO_RESIZE_OFF );
	table.getTableHeader().setForeground(Color.red);
	table.getTableHeader().setBackground(new Color(.85f,.85f,.85f));
	table.getTableHeader().setToolTipText("double-click sorts ascending; shift-double-click sorts descending");
	table.setShowGrid(false);
	setTableFont(table, font);
    }

// setup rowheader selection/color attributes
    private void setTableSelectionState(JTable table) {
	table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	table.setRowSelectionAllowed(true);
	table.setCellSelectionEnabled(false);
	table.setColumnSelectionAllowed(false);
    }

    public void clearSelection() {
	tableView.clearSelection();
	tableRowHead.clearSelection();
    }

// Setup format patterns and number renderers
    private void setTableColumnRenderersEditors(JTable table, Color defaultColor, Color nonNullColor, Color selectedColor) {
//	Debug.println("DBTable setting up MainTable cell renderers/editors...");
	String pattern0 = "#0";
	FormattedNumberEditor_FW numberEditor0 = new FormattedNumberEditor_FW(pattern0);
	EditorMouseListener editorListener = new EditorMouseListener();
	numberEditor0.getComponent().addMouseListener(editorListener);

	String pattern9 = "#0.000000"; // generic four byte float
	FormattedNumberRenderer numberRenderer9 = new FormattedNumberRenderer(pattern9);
	FormattedNumberEditor_FW numberEditor9 = new FormattedNumberEditor_FW(pattern9);
	numberEditor9.getComponent().addMouseListener(editorListener);

// Assign editor/renderers to table columns
	Font tableFont = table.getFont();
	FontMetrics fm = table.getFontMetrics(tableFont);

	GraphicTimestampTextCellRenderer timestampRenderer = 
		new GraphicTimestampTextCellRenderer(fm, defaultColor, selectedColor);
	GraphicTimestampTextCellRenderer timestampNonNullRenderer =
		 new GraphicTimestampTextCellRenderer(fm, nonNullColor, selectedColor);

	GraphicTextCellRenderer defaultRenderer = new GraphicTextCellRenderer(fm, defaultColor, selectedColor);
	GraphicTextCellRenderer defaultNonNullRenderer  = new GraphicTextCellRenderer(fm, nonNullColor, selectedColor);

	GraphicDateTimeTextCellRenderer datetimeRenderer = new GraphicDateTimeTextCellRenderer(fm, defaultColor, selectedColor);
	GraphicDateTimeTextCellRenderer datetimeNonNullRenderer  = new GraphicDateTimeTextCellRenderer(fm, nonNullColor, selectedColor);
	CalendarDateEditor cdEditor = new CalendarDateEditor(tableFont);
	cdEditor.getComponent().addMouseListener(editorListener);

	DateTimeEditor dtEditor = new DateTimeEditor(tableFont);
	dtEditor.getComponent().addMouseListener(editorListener);
	
	TextCellEditor textEditor = new TextCellEditor(tableFont);
	textEditor.getComponent().addMouseListener(editorListener);

	GraphicDateTimeTextCellRenderer dtRenderer = null;
	GraphicTimestampTextCellRenderer tsRenderer = null;
	GraphicTextCellRenderer renderer = null;

	TableColumnModel tcm = table.getColumnModel();
	for (int i = 0; i<table.getColumnCount(); i++) {
	  TableColumn column = tcm.getColumn(i);
	  int index = table.convertColumnIndexToModel(i);
	  if (tableModel.isColumnNullable(index)) {
		renderer   = defaultRenderer;
//		tsRenderer = timestampNonNullRenderer;
		tsRenderer = timestampRenderer;
//		dtRenderer = datetimeNonNullRenderer;
		dtRenderer = datetimeRenderer;
	  }
	  else {
		renderer   = defaultNonNullRenderer;
		tsRenderer = timestampNonNullRenderer;
		dtRenderer = datetimeNonNullRenderer;
	  }

	  String className = table.getColumnClass(i).getName();
	  if (table.getColumnName(i).equalsIgnoreCase("DATETIME") ) {
		column.setCellRenderer(dtRenderer);
		column.setCellEditor(dtEditor);
	  }
	  else if (className.equals("java.lang.Number") || className.equals("java.lang.Long") ||
		className.equals("java.lang.Integer") ) {
		column.setCellRenderer(renderer);
		column.setCellEditor(numberEditor0);
	  }
	  else if (className.equals("java.lang.Double")) {
		column.setCellRenderer(renderer);
		column.setCellEditor(numberEditor9);
	  }
	  if (
                className.equals("java.util.Date") ||
                className.equals("org.trinet.util.DateTime") ||
	        className.equals("java.sql.Date") || 
                className.equals("java.sql.Timestamp")
              )  {
		column.setCellRenderer(tsRenderer);
		column.setCellEditor(cdEditor);
	  }
	  else {
		column.setCellRenderer(renderer);
		column.setCellEditor(textEditor);
	  }
	}
//	Debug.println("DBTable finished with renderer setup; final table setup");	
    }

    private void initTableColumnSizes(JTable table) {
        TableColumn column = null;
        Component comp = null;
        int headerWidth = 0;
        int cellWidth = 0;

	int colCount = table.getColumnCount();
	int checkRows;
	int rowCount = table.getRowCount(); // perhaps would be better to used subset for speed?
	if (rowCount > 20) checkRows = 20;  
	else checkRows = rowCount;

        for (int i = 0; i < colCount; i++) {
          column = table.getColumnModel().getColumn(i);
          comp = column.getHeaderRenderer().
                             getTableCellRendererComponent(
                                 table, column.getHeaderValue(), 
                                 false, false, 0, 0);
          headerWidth = comp.getPreferredSize().width;
	  int maxCellWidth = 0;
	  for (int j = rowCount-checkRows; j < rowCount; j++) {
            comp = table.getCellRenderer(j,i).
                             getTableCellRendererComponent(
                                 table, table.getValueAt(j,i),
                                 false, false, j, i);
            cellWidth = comp.getPreferredSize().width;
	    if (cellWidth > maxCellWidth) maxCellWidth = cellWidth;

	  }
          column.setPreferredWidth(Math.max(headerWidth, maxCellWidth));
        }
    } 


// add main table models event listeners
    private void addListeners() {
//	tableRowHead.getColumnModel().addColumnModelListener(tableView); 
//	tableView.getColumnModel().addColumnModelListener(tableRowHead);
//	sorter.addTableModelListener(tableView);
//	sorter.addTableModelListener(tableRowHead);

// Install a mouse listener in the TableHeader as the sorter UI.
	sorter.addMouseListenerToHeaderInTable(tableView);
// Install a mouse listener in the TableHeader as the sorter UI.
	sorter.addMouseListenerToHeaderInTable(tableRowHead);

	tableView.getSelectionModel().addListSelectionListener(new TableEditListener());

// sync scrolling table selection with any row header cell selection change.
	tableRowHead.getSelectionModel().addListSelectionListener(new TableRowHeadSelectionListener());
// sync row header selection with any scrolling table cell selection change.
	tableView.getSelectionModel().addListSelectionListener(new TableViewSelectionListener());

// click mouse button 1 >= 4 times over tableView cell to cancel cell editing
	tableView.addMouseListener(new MouseAdapter() {
		public void mousePressed(MouseEvent evt) {
			int clickCount = evt.getClickCount();
			int mask = evt.getModifiers();
//			Debug.println("TV ClickCount:"+clickCount+" TV Editing? "+tableView.isEditing()+" Button: "+mask);
			if ( mask == MouseEvent.BUTTON1_MASK && clickCount >= 4) {
//			    Debug.println("Canceling edit");
			    tableView.editingCanceled(new ChangeEvent(tableView));
			    tableView.repaint();
			}
		}
	 });

// shift click mouse button 1 on any tableRowHead cell to resize table row header columns
	tableRowHead.addMouseListener(new MouseAdapter() {
		public void mousePressed(MouseEvent evt) {
//			Debug.println("TRH ClickCount:" + evt.getClickCount() + " TRH Editing? " + tableRowHead.isEditing());
			if ( (evt.getModifiers() == (MouseEvent.BUTTON1_MASK | InputEvent.SHIFT_MASK)) 
				&& evt.getClickCount() >= 1) {
//			Debug.println("TRH reset RowHeaderColumnSizes");
    			    resetRowHeader();
			}
//			else System.out.println("Shift failed");
		}
	 });

    }

    public boolean resetRowHeader() {
	if (tableDB == null) return false;
	JTable table = (JTable) tableDB.getRowHeader().getView();
	resetRowHeaderColumnSizes(table.getSelectedRow());
	table.setPreferredScrollableViewportSize(table.getPreferredSize());
	tableDB.setRowHeaderView(table);
	tableDB.setCorner(JScrollPane.UPPER_LEFT_CORNER, table.getTableHeader());
	tableDB.revalidate();
	return true;
    }
/*
    public boolean resetRowHeader(JTable table, JScrollPane scrollpane) {
	if (table == null || scrollpane == null) return false;
	table.setPreferredScrollableViewportSize(table.getPreferredSize());
	scrollpane.setRowHeaderView(table);
	scrollpane.setCorner(JScrollPane.UPPER_LEFT_CORNER, table.getTableHeader());
	return true;
    }
*/
    private boolean resetScrollingTableColumnSizes(int irow) {
	return TableCell.resetRowColumnSizes(tableView, irow);
    }
    private boolean resetRowHeaderColumnSizes(int irow) {
	return TableCell.resetRowColumnSizes(tableRowHead, irow);
    }

    public void stopAllCellEditing() {
	if (tableView == null) return;
	if (tableView.isEditing()) {
	     tableView.getCellEditor().stopCellEditing();
	}
	if (tableRowHead == null) return;
	if (tableRowHead.isEditing()) {
	     tableRowHead.getCellEditor().stopCellEditing();
	}
    }

// Implementation of Listener interface
// Double click mouse button 2 to stop (end) cell edit
// Double click mouse button 3 to cancel cell edit

    private class EditorMouseListener extends MouseAdapter {
	public void mousePressed(MouseEvent evt) {
//	Debug.println("EML ClickCount:" + evt.getClickCount());
	    if ( evt.getModifiers() == MouseEvent.BUTTON3_MASK && evt.getClickCount() == 2) {
//		Debug.println("EML Canceling edit");
		tableView.editingCanceled(new ChangeEvent(tableView));
		tableView.repaint();
	    }
	    else if ( evt.getModifiers() == MouseEvent.BUTTON2_MASK && evt.getClickCount() == 2) { 
//		Debug.println("EML Stopping edit");
		tableView.editingStopped(new ChangeEvent(tableView));
//		tableView.repaint();
	    }
	}
    }

    private class TableEditListener implements ListSelectionListener {
      public void valueChanged(ListSelectionEvent e) {
/*
	Object obj = e.getSource();
//	Debug.println("Source: " + e.getSource() );
	Component [] cmps = tableView.getComponents();
	for (int i  = 0; i<cmps.length; i++) {
//	    Debug.println(cmps[i].getClass().getName());
	    Component [] cmps2 = ((CellRendererPane)cmps[i]).getComponents();
	    for (int j  = 0; j<cmps2.length; j++) {
//	        Debug.println(cmps2[i].getClass().getName());
	    }
	}
*/
//	if (tableView.isEditing()) tableView.editingCanceled(new ChangeEvent(tableView));
	if (tableView.isEditing()) tableView.editingStopped(new ChangeEvent(tableView));
      }
    }

    private class TableRowHeadSelectionListener implements ListSelectionListener {
      public void valueChanged(ListSelectionEvent e) {
	int irow1 = tableRowHead.getSelectedRow();
	int irow2 = tableView.getSelectedRow();
	if (irow1 != irow2 && irow1 >= 0) {
//	    tableView.clearSelection(); 
	    tableView.setRowSelectionInterval(irow1, irow1); 
	}
      }
    }

    private class TableViewSelectionListener implements ListSelectionListener {
      public void valueChanged(ListSelectionEvent e) {
	int irow1 = tableView.getSelectedRow();
	int irow2 = tableRowHead.getSelectedRow();
	if (irow1 != irow2 && irow1 >= 0) {
//	    tableRowHead.clearSelection(); 
	    tableRowHead.setRowSelectionInterval(irow1, irow1); 
	}
      }
    }

    private class ScrollingActionListener implements ActionListener  {
	public void actionPerformed (ActionEvent evt) {
	    if (tableDB == null) return;
	    String str = evt.getActionCommand();
	    JScrollBar bar = tableDB.getVerticalScrollBar();
	    if (str.equalsIgnoreCase("U")) {
		bar.setValue(bar.getMinimum());
	    }
	    else if (str.equalsIgnoreCase("D")) {
		bar.setValue(bar.getMaximum());
	    }
	}
    }

}
