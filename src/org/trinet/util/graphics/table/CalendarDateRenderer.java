package org.trinet.util.graphics.table;
import java.awt.*;
import java.util.*;
import java.text.*;

public class CalendarDateRenderer extends ColorableTextCellRenderer {
  protected DateFormat dateFormatter = DateFormat.getDateInstance();

  public CalendarDateRenderer() { super(); }
  
  public CalendarDateRenderer(int fontSize) { super(fontSize); }
  
  public CalendarDateRenderer(Font font) { super(font); }
  
  public void setValue(Object value) {
    if (value != null && value instanceof GregorianCalendar) {
        GregorianCalendar cal = (GregorianCalendar)value;
        super.setValue(dateFormatter.format(cal.getTime()));        
    }
    else {
        super.setValue(value); // default for java.sql.Timestamp
    }
  }
}
