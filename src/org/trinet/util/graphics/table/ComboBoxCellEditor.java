package org.trinet.util.graphics.table;
import javax.swing.*;
import javax.swing.text.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import org.trinet.util.graphics.text.*;

public class ComboBoxCellEditor extends AbstractCellEditor {
  private JComboBox component;
  private String colName;
  private JTextClipboardPopupMenu clipboard;
  protected int fontSize = 12;

  public ComboBoxCellEditor(String colName, Object [] values) {
    this(colName, values, null);
  }
  public ComboBoxCellEditor(String colName, Object [] values, Font font) {
    super();
    initEditorComponent(colName, values, font);
  }
  protected void initEditorComponent(String colName, Object values, Font font) {
    this.colName = colName;
    component = new JComboBox((Object []) values);  // add list model choices;
    component.addItemListener(this);
    component.addActionListener(this);
    Component editor = component.getEditor().getEditorComponent();
    if (font == null) 
        editor.setFont(new Font("Monospaced", Font.PLAIN, this.fontSize));
    if (editor instanceof JTextComponent) {
       clipboard = new JTextClipboardPopupMenu((JTextComponent)editor);
    }
  }
  
  public int getFontSize() {
    return component.getFont().getSize();
  }
  public void setFontSize(int fontSize) {
    if (fontSize > 0 && this.fontSize != fontSize) {
      this.fontSize = fontSize;
      component.setFont(component.getFont().deriveFont((float)fontSize));
    }
  }

// Methods to implement DefaultCellEditor 
  public boolean startCellEditing(EventObject anEvent) {
    if(anEvent == null) component.requestFocus();
    else if (anEvent instanceof MouseEvent) {
      if (((MouseEvent)anEvent).getClickCount() < clickCountToStart)
        return false;
    }
    return true;
  }
  public Component getComponent() {
    return this.component;
  }

  public Object getCellEditorValue() {
    return component.getSelectedItem();
  }

  public void setCellEditorValue(Object value) {
    component.setSelectedItem(value);
  }

  public Component getTableCellEditorComponent( JTable table, Object value,
        boolean isSelected,int row,int column) {
    if (table.getColumnName(column).equals(colName)) {
      setCellEditorValue(value);
      return component;
    }
    else return super.getTableCellEditorComponent(table, value, isSelected, row, column);
  }
}
