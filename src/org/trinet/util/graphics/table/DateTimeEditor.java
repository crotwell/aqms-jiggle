package org.trinet.util.graphics.table;
import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.text.*;
import java.math.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.util.*;
import org.trinet.util.graphics.text.*;
public class DateTimeEditor extends TextCellEditor {

  public DateTimeEditor() { 
    this(-1, null);
  }
  public DateTimeEditor(int fontSize) { 
    this(fontSize, null);
  }
  public DateTimeEditor(Font font) { 
     this(-1, font);
  }
  protected DateTimeEditor(int fontSize, Font font) { 
    super(fontSize, font);
    component.setColumns(14);
    component.setHorizontalAlignment(JTextField.RIGHT);
  }
  
// override Methods of DefaultCellEditor 
  public Object getCellEditorValue() {
    String editStr = component.getText();
    StringTokenizer stoke = new StringTokenizer(editStr, "-/:, \t");
    //if (stoke.countTokens() != 6 ) return value;
    StringBuffer sb = new StringBuffer(28); 
    while (stoke.hasMoreTokens()) {
       sb.append(stoke.nextToken());
       editStr = sb.toString();
    }
    sb = new StringBuffer(28);
    try {
      sb = Concatenate.format(sb,Double.parseDouble(editStr),14,3);
      editStr = sb.toString();
    }
    catch (NumberFormatException ex) {
      ex.printStackTrace();
      return value;
    }
    // Do not use ss.fff fractional seconds format, Java SimpleDateFormat class is used to decode pattern and it does not recognize the 'f' code 
    return new DataDouble(LeapSeconds.stringToTrue(editStr,"yyyyMMddHHmmss.SSS")); // for UTC leap secs -aww 2008/02/07
  }

  public void setCellEditorValue(Object value) {
    String str = "";
    if (value == null) component.setText(str);
    else {
      //component.setText(LeapSeconds.trueToString(((BigDecimal) value).doubleValue()).substring(0,24));
      if (value instanceof Number) {
        str = LeapSeconds.trueToString( ((Number) value).doubleValue() ); // for UTC leap secs -aww 2008/02/07
      }
      else if (value instanceof DataObject) {
        str = LeapSeconds.trueToString( ((DataObject) value).doubleValue() ); // for UTC leap secs -aww 2008/02/07
      }
      else if (value instanceof String) {
        str = (String) value;
      }
      component.setText(str); // ? or .substring(0,Math.min(23),str.length());?
    }
    this.value = value;
  }
}
