package org.trinet.util.graphics.table;
import java.awt.*;
import javax.swing.*;
import javax.swing.table.*;
import org.trinet.util.*;

    public class GraphicTimestampTextCellRenderer extends GraphicTextCellRenderer {
	static final int LENGTH_OF_ORACLE_TIMESTAMP = 19;

	public GraphicTimestampTextCellRenderer(FontMetrics metrics, Color defaultBackgroundColor, Color selectedBackgroundColor) {
	   super(metrics, defaultBackgroundColor, selectedBackgroundColor);
	}

	public void setValue(Object value) {
	    if (value == null) {
		super.setValue("");
	    }
	    else {
		String tmp = value.toString();
		if (tmp.length() > LENGTH_OF_ORACLE_TIMESTAMP) tmp = tmp.substring(0, LENGTH_OF_ORACLE_TIMESTAMP);
		super.setValue(tmp);
	    }
	}
    }
