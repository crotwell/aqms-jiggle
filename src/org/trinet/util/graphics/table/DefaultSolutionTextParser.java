package org.trinet.util.graphics.table;
import java.io.*;
import java.util.*;
import org.trinet.jasi.*;
import org.trinet.util.*;
public class DefaultSolutionTextParser extends AbstractJasiTableRowTextParser {
  protected String eventType; 
  protected double lat;
  protected double lon;
  protected double z;

  public DefaultSolutionTextParser() {
    super();
    parserName = "Solution";
    dialogTitle = "<yyyy-MM-dd:HH:mm:ss.s> [lat] [lon] [z] [etype]>";
  }

  public void parseInputTableRows(String text) {
    hasParseErrors = false;
    if (jrList == null) jrList = new GenericSolutionList();
    else jrList.clear();
    if (text == null || text.length() == 0) return;
    BufferedReader buffReader = null;
    try {
      buffReader = new BufferedReader(new StringReader(text));
      String aLine = null;
      while ((aLine = buffReader.readLine()) != null) {
        if (aLine.trim().equals("")) continue;
        StringTokenizer strToke = new StringTokenizer(aLine);
        lat = 0.;
        lon = 0.;
        z = 0.;
        eventType = EventTypeMap3.EARTHQUAKE;
        if (strToke.countTokens() < 1) {
          System.err.println("Error: Input solution has too few field tokens");
          System.err.println(aLine);
          hasParseErrors = true;
          continue;
        }
        if (! parseDateTime(strToke.nextToken())) {
          System.err.println("Error: Unable to parse solution datetime string");
          System.err.println(aLine);
          hasParseErrors = true;
          continue;
        }
        if (strToke.hasMoreTokens()) {
          try {
            lat = Double.parseDouble(strToke.nextToken());
          }
          catch (NumberFormatException ex) {
            System.err.println("Error: Unable to parse solution lat");
            System.err.println(aLine);
            hasParseErrors = true;
            continue;
          }
        }
        if (strToke.hasMoreTokens()) {
          try {
            lon = Double.parseDouble(strToke.nextToken());
          }
          catch (NumberFormatException ex) {
            System.err.println("Error: Unable to parse solution lon");
            System.err.println(aLine);
            hasParseErrors = true;
            continue;
          }
        }
        if (strToke.hasMoreTokens()) {
          try {
            z = Double.parseDouble(strToke.nextToken());
          }
          catch (NumberFormatException ex) {
            System.err.println("Error: Unable to parse solution z");
            System.err.println(aLine);
            hasParseErrors = true;
            continue;
          }
        }
        if (strToke.hasMoreTokens()) {
          eventType = strToke.nextToken();
        }
        Debug.println(
          LeapSeconds.trueToString(datetime) //for UTC - aww 2008/02/07 
           + " " + lat + " " + lon + " " + z
        ); 
        Solution aRow = (Solution) createRow();
        aRow.setNeedsCommit(false); // why false?, may want this removed
        if (aRow != null) jrList.add(aRow);
      }
    }
    catch (IOException ex) {
       ex.printStackTrace();
       hasParseErrors = true;
    }
    finally {
      try {
        if (buffReader != null) buffReader.close();
      }
      catch (IOException ex2) {
        ex2.toString();
      }
    }
  }
  public JasiCommitableIF createRow() {
      if (tableModel == null) return null;
      Solution aSolution = (Solution) tableModel.initRowValues(Solution.create());
      aSolution.setTime(datetime);
      aSolution.setLatLonZ(lat, lon, z);
      if ( ! aSolution.getLatLonZ().isNull() ) {
          // valid, not bogus dummy
          aSolution.validFlag.setValue(1);
          aSolution.dummyFlag.setValue(0);
      }
      //to not bump version
      aSolution.eventType.setValue( EventTypeMap3.getMap().toJasiType(eventType) );
      aSolution.eventAuthority.setValue(defaultAuth);
      aSolution.eventSource.setValue(defaultSource);
      Debug.println("input sol: " + aSolution.toString());
      return aSolution;
  }
}
