package org.trinet.util.graphics.table;

import java.util.*;
import org.trinet.util.Format;

public class SlashDegMinSolutionTextParser extends DegMinSolutionTextParser {
    private int oldYr;
    private int oldMo;
    private int oldDy;

    Format i2 = new Format("%02i");

  public SlashDegMinSolutionTextParser() {
    super();
    parserName = "Solution";
    dialogTitle = "<yyyy/MM/dd/HH/mm/ss.s> [latd] [latm] [lond] [lonm] [z] [etype]>";
  }

  public boolean parseDateTime(String dateTime) {
    return parseDateTime(dateTime, isDefaultReversed); 
  }

  private boolean parseDateTime(String dateTime, boolean reversed) {
    return (reversed) ? parseDateTimeReversed(dateTime) :
          parseDateTimeForward(dateTime);
  }

//  yyyy/mo/dy/hr/mm/s.s  || /hr/mm/s.s || //mm/s.s  || ///ss.s 
  private boolean parseDateTimeForward(String dateTime) {
    StringTokenizer dateToke = new StringTokenizer(dateTime, "/ ");
    int cnt = dateToke.countTokens(); 
    String token = null;
    try {
      switch (cnt) {
        case 6:
          if ((token = dateToke.nextToken()) != null) {
            oldYr = Integer.parseInt(token);
          }
        case 5:
          if ((token = dateToke.nextToken()) != null) {
            oldMo = Integer.parseInt(token);
          }
        case 4:
          if ((token = dateToke.nextToken()) != null) {
            oldDy = Integer.parseInt(token);
          }
          oldDate = oldYr + "-" + i2.form(oldMo) + "-" + i2.form(oldDy);
        case 3:
          if ((token = dateToke.nextToken()) != null) {
            oldHr = Integer.parseInt(token);
          }
        case 2:
          if ((token = dateToke.nextToken()) != null) {
            oldMin = Integer.parseInt(token);
          }
        case 1:
          if ((token = dateToke.nextToken()) != null) {
            oldSec = Double.parseDouble(token);
          }
          break;
        default:
           System.err.println("Error date time token count: " + cnt);
           return false;
        }
      }
      catch (Exception ex) {
        System.err.println(ex.toString());
        return false;
      }
      setDateTimeValues();
      return true;
    }
//  ss/mn/hr/dy/mo/yr  || /ss/mn/hr || ss/mn  || ss 
    private boolean parseDateTimeReversed(String dateTime) {
      StringTokenizer dateToke = new StringTokenizer(dateTime, "/ ");
      int cnt = dateToke.countTokens(); 
      String token = null;
      try {
        switch (cnt) {
          case 6:
            oldSec  = Double.parseDouble(dateToke.nextToken());
            oldMin  = Integer.parseInt(dateToke.nextToken());
            oldHr   = Integer.parseInt(dateToke.nextToken());
            oldDy   = Integer.parseInt(dateToke.nextToken());
            oldMo   = Integer.parseInt(dateToke.nextToken());
            oldYr   = Integer.parseInt(dateToke.nextToken());
            oldDate = oldYr + "-" + i2.form(oldMo) + "-" + i2.form(oldDy);
            break;
          case 5:
            oldSec  = Double.parseDouble(dateToke.nextToken());
            oldMin  = Integer.parseInt(dateToke.nextToken());
            oldHr   = Integer.parseInt(dateToke.nextToken());
            oldDy   = Integer.parseInt(dateToke.nextToken());
            oldMo   = Integer.parseInt(dateToke.nextToken());
            oldDate = oldYr + "-" + i2.form(oldMo) + "-" + i2.form(oldDy);
            break;
          case 4:
            oldSec  = Double.parseDouble(dateToke.nextToken());
            oldMin  = Integer.parseInt(dateToke.nextToken());
            oldHr   = Integer.parseInt(dateToke.nextToken());
            oldDy   = Integer.parseInt(dateToke.nextToken());
            oldDate = oldYr + "-" + i2.form(oldMo) + "-" + i2.form(oldDy);
            break;
          case 3:
            oldSec  = Double.parseDouble(dateToke.nextToken());
            oldMin  = Integer.parseInt(dateToke.nextToken());
            oldHr   = Integer.parseInt(dateToke.nextToken());
            break;
          case 2:
            oldSec  = Double.parseDouble(dateToke.nextToken());
            oldMin  = Integer.parseInt(dateToke.nextToken());
            break;
          case 1:
            oldSec  = Double.parseDouble(dateToke.nextToken());
            break;
          default:
            System.err.println("Error date time token count: " + cnt);
            return false;
        }
      }
      catch (Exception ex) {
        System.err.println(ex.toString());
        return false;
      }
      setDateTimeValues();
      return true;
  }
}
