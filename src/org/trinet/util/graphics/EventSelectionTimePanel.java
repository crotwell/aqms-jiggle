package org.trinet.util.graphics;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

import org.trinet.jasi.*;
import org.trinet.jiggle.ui.panel.config.EventAssocTimeRangePanel;
import org.trinet.util.*;
import org.trinet.util.graphics.text.*;

    //"catalogTimeMode"            // ComboBox, RadioButtonGroup
    //"catalogOriginStartTime"     // DateTimeChooser
    //"catalogOriginEndTime"       // DateTimeChooser
    //"catalogRelativeTimeValue"   // NumberTextField or ComboBox
    //"catalogRelativeTimeUnits"   // ComboBox or RadioButtonGroup
    //"catalogMaxRows"             // NumberTextField

public class EventSelectionTimePanel extends JPanel {

    /** Date range component */
    private DateRangeChooser dateRangeChooser = new DateRangeChooser();
    /** Delta time component */
    private DeltaTimePanel deltaTimePanel   = new DeltaTimePanel();
    private DeltaTimePanel deltaTimeIdPanel = new DeltaTimePanel();

    /** Properties passed as an argument to the constructor */
    private EventSelectionProperties props;

    private JRadioButton rbuttonRelative = new JRadioButton();
    private JRadioButton rbuttonAbsolute = new JRadioButton();
    private JRadioButton rbuttonId = new JRadioButton();
    private NumberTextField idField = NumberTextFieldFactory.createInputField(15,true);
    private NumberTextField maxRowsField = NumberTextFieldFactory.createInputField(6,true);
    private EventAssocTimeRangePanel eventAssocTimeRangePanel = null;

    private final int ROW_DETAL_TIME = 0;
    private final int ROW_ABSOLUTE_TIME = 1;
    private final int ROW_RELATIVE_ID = 2;
    private final int ROW_MAX_CATALOG = 3;
    private final int ROW_EVENT_ASSOC = 4;


    public EventSelectionTimePanel(EventSelectionProperties props) {

         this.props = props;

         try  {
              jbInit();
         }
         catch(Exception ex) {
              ex.printStackTrace();
         }
    }

    /** GUI builder constructed by JBuilder design tool. */
    private void jbInit() throws Exception {

        this.setLayout(new GridBagLayout());
        idField.setToolTipText("Set absolute time span the input event origin time -/+ the selected delta time");
        ActionListener al = new java.awt.event.ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (rbuttonRelative.isSelected()) {
                    props.setTimeModeRelative();
                    dateRangeChooser.setEnabled(false);
                    deltaTimePanel.setEnabled(true);
                    deltaTimeIdPanel.setEnabled(false);
                    deltaTimePanel.requestFocus();
                    idField.setEnabled(false);
                    idField.setValue(props.getRelativeTimeEvid());
                }
                else if (rbuttonAbsolute.isSelected()) {
                    props.setTimeModeAbsolute();
                    dateRangeChooser.setEnabled(true);
                    deltaTimePanel.setEnabled(false);
                    deltaTimeIdPanel.setEnabled(false);
                    dateRangeChooser.requestFocus();
                    idField.setEnabled(false);
                    idField.setValue(props.getRelativeTimeEvid());
                }
                else if (rbuttonId.isSelected()) {
                    //props.setTimeModeAbsolute();
                    props.setTimeModeEvid();
                    dateRangeChooser.setEnabled(false);
                    deltaTimePanel.setEnabled(false);
                    deltaTimeIdPanel.setEnabled(true);
                    idField.setValue(props.getRelativeTimeEvid());
                    idField.setEnabled(true);
                    idField.requestFocus();
                }
                repaint();
            }
        };

        idField.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                long evid = idField.getLongValue();
                if (evid > 0l)  {
                   Solution sol = Solution.create().getById(evid);
                   if (sol != null && sol.hasValidDateTime()) {
                       double ot = sol.getTime();
                       double delta = deltaTimeIdPanel.getSeconds(); 
                       dateRangeChooser.setTimeSpan(new TimeSpan(ot-delta, ot+delta));
                   }
                }
            }
        });

        rbuttonRelative.setHorizontalTextPosition(SwingConstants.CENTER);
        rbuttonRelative.setVerticalTextPosition(SwingConstants.TOP);
        rbuttonRelative.setText("Relative Time");
        rbuttonRelative.setActionCommand("Relative");
        rbuttonRelative.setSelected(props.modeIsRelative());
        rbuttonRelative.addActionListener(al);

        rbuttonAbsolute.setHorizontalTextPosition(SwingConstants.CENTER);
        rbuttonAbsolute.setVerticalTextPosition(SwingConstants.TOP);
        rbuttonAbsolute.setText("Absolute Time");
        rbuttonAbsolute.setActionCommand("Absolute");
        rbuttonAbsolute.setSelected(props.modeIsAbsolute());
        rbuttonAbsolute.addActionListener(al);

        rbuttonId.setHorizontalTextPosition(SwingConstants.CENTER);
        rbuttonId.setVerticalTextPosition(SwingConstants.TOP);
        rbuttonId.setText("Relative to Id");
        rbuttonId.setActionCommand("Id");
        rbuttonId.setSelected(props.modeIsEvid());
        rbuttonId.addActionListener(al);

        GridBagConstraints constraint = getLayoutConstraint();

        // Delta time combo
        JPanel jp = new JPanel(new FlowLayout(FlowLayout.LEFT));
        jp.setBorder(BorderFactory.createLineBorder(Color.black) );
        jp.add(rbuttonRelative);
        jp.add(Box.createHorizontalStrut(10));  // spacer
        jp.add(deltaTimePanel);
        constraint.gridy = this.ROW_DETAL_TIME;
        this.add(jp, constraint);

        // DateRange combo
        jp = new JPanel(new FlowLayout(FlowLayout.LEFT));
        jp.setBorder(BorderFactory.createLineBorder(Color.black) );
        jp.add(rbuttonAbsolute);
        jp.add(dateRangeChooser);
        constraint.gridy = this.ROW_ABSOLUTE_TIME;
        this.add(jp, constraint);

        // set range times to values in props
        dateRangeChooser.setTimeSpan(props.getTimeSpanAbsolute()); // 02/22/2006 -aww used to show relative time
        String units = props.getRelativeTimeUnits();
        double val = props.getRelativeTimeValue();
        dateRangeChooser.defaultUnits = units;
        dateRangeChooser.defaultValue = val;
        dateRangeChooser.addItemListeners(); // add listeners only after the timespan is set above else chooser times may be wrong 

        // set delta times to values in props
        deltaTimePanel.setUnits(units);
        deltaTimePanel.setValue(val);

        // default is hour
        deltaTimeIdPanel.setUnits(units);
        deltaTimeIdPanel.setValue(val);

        jp = new JPanel(new FlowLayout(FlowLayout.LEFT));
        jp.setBorder(BorderFactory.createLineBorder(Color.black) );
        jp.add(rbuttonId);
        jp.add(Box.createHorizontalStrut(10));  // spacer
        jp.add(new JLabel("Id "));
        jp.add(idField);
        jp.add(Box.createHorizontalStrut(10));  // spacer
        jp.add(deltaTimeIdPanel);
        constraint.gridy = this.ROW_RELATIVE_ID;
        this.add(jp, constraint);

        // group radio buttons so only one can be chosen at a time
        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(rbuttonRelative);
        buttonGroup.add(rbuttonAbsolute);
        buttonGroup.add(rbuttonId);

        // enable initial mode as defined in the properties
        if ( props.modeIsRelative()) {
            rbuttonRelative.doClick();
        }
        else if (props.modeIsAbsolute()) {
            rbuttonAbsolute.doClick();
        }
        else if ( props.modeIsEvid()) {
            rbuttonId.doClick();
        }

        jp = new JPanel();
        jp.setBorder(BorderFactory.createLineBorder(Color.black) );
        jp.add(new JLabel("Maximum # of catalog rows (-1 -> ALL): "));
        maxRowsField.setValue(props.getMaxCatalogRows());
        jp.add(maxRowsField);
        constraint.gridy = this.ROW_MAX_CATALOG;
        this.add(jp, constraint);
        
        this.eventAssocTimeRangePanel = new EventAssocTimeRangePanel(this.props.getEventAssociationTimeRange());
        this.eventAssocTimeRangePanel.setBorder(BorderFactory.createLineBorder(Color.black) );
        constraint.gridy = this.ROW_EVENT_ASSOC;
        this.add(this.eventAssocTimeRangePanel, constraint);
    }

     /** Set the time span for the date range chooser. If the 'catalogTimeMode'
     * is "relative" this will be a timeSpan from now back to the length of
     * of the relative time interval. */
    public TimeSpan getTimeSpan() {
        return props.modeIsAbsolute() ?  dateRangeChooser.getTimeSpan() : deltaTimePanel.getTimeSpan();
    }

    private GridBagConstraints getLayoutConstraint() {
        GridBagConstraints constraint = new GridBagConstraints();
        constraint.fill = GridBagConstraints.BOTH;
        constraint.weightx = 0.5;
        constraint.weighty = 0.5;
        constraint.gridx = 0;
        constraint.anchor = GridBagConstraints.NORTH;
        return constraint;
    }

/** The value specified  for the relative time*/
    public double getValue() {
        return deltaTimePanel.getValue();
    }
/** The units specified for the relative time */
    public String getUnits() {
        return deltaTimePanel.getUnits();
    }

    /** Return the modified EventSelectionProperties */
    public EventSelectionProperties getProperties() {
        return props;
    }

    /** Returns a string describing the mode selected, relative or absolute.
     * @return empty string if undefined.
     * */
    public String getTimeMode() {
        return props.getTimeMode();
    }

     public void updateProperties() {
         updateProperties(props);
     }

     public EventSelectionProperties updateProperties(EventSelectionProperties propsIn) {

       if (props.modeIsAbsolute()) {
         propsIn.setTimeModeAbsolute();
         propsIn.setTimeSpan(dateRangeChooser.getTimeSpan());
       }
       else if (props.modeIsEvid()) {
         long evid = idField.getLongValue();
         if (evid > 0l) {
             Solution sol = Solution.create().getById(evid);
             if (sol != null && sol.hasValidDateTime()) {
                 propsIn.setRelativeTimeEvid(evid);
                 double ot = sol.getTime();
                 double delta = deltaTimeIdPanel.getSeconds(); 
                 TimeSpan ts = new TimeSpan(ot-delta, ot+delta);
                 dateRangeChooser.setTimeSpan(ts);
                 propsIn.setTimeSpan(ts);
                 propsIn.setTimeWindow(deltaTimeIdPanel.getValue(), deltaTimeIdPanel.getUnits());
                 propsIn.setTimeModeEvid();
             }
             else {
               JOptionPane.showMessageDialog(getTopLevelAncestor(), "Event id: " + evid + " not in database!\n"+
                           "defaulting to span relative to current time",
                           "Event Selection Relative to Event", JOptionPane.ERROR_MESSAGE);
               propsIn.setTimeModeRelative();
               propsIn.setTimeWindow(deltaTimePanel.getValue(), deltaTimePanel.getUnits());
             }
         }
         else {
           JOptionPane.showMessageDialog(getTopLevelAncestor(), "Event id invalid, " + evid + " <=0\n"+
                           "defaulting to span relative to current time",
                           "Event Selection Relative to Event", JOptionPane.ERROR_MESSAGE);
           propsIn.setTimeModeRelative();
           propsIn.setTimeWindow(deltaTimePanel.getValue(), deltaTimePanel.getUnits());
         }
       }
       else  {
         propsIn.setTimeModeRelative();
         propsIn.setTimeWindow(deltaTimePanel.getValue(), deltaTimePanel.getUnits());
       }
       propsIn.setMaxCatalogRows(maxRowsField.getIntValue());
       propsIn.setEventAssociationTimeRange(this.eventAssocTimeRangePanel.getEventAssocTimeRange());
       return propsIn;

     }

///////
   /*
    public static void main(String s[])
   {

       JFrame frame = new JFrame("Main");
       frame.addWindowListener(new WindowAdapter()
          {
           public void windowClosing(WindowEvent e) {System.exit(0);}
       });
// Private properties
       EventSelectionProperties origProps = new EventSelectionProperties();
       origProps.setFiletype("jiggle"); // test file in the "jiggle" subdir
       origProps.setFilename("eventProperties"); // name of event selection props file
       origProps.reset(); // read the props from file
       origProps.setup(); // configure local environment from props

       origProps.dumpProperties();

       final EventSelectionTimePanel timePanel = new EventSelectionTimePanel(origProps);

       JPanel panel = new JPanel(new BorderLayout());
       panel.add (timePanel, BorderLayout.CENTER);
       JButton okButton = new JButton("OK");
       okButton.addActionListener(new ActionListener() {

          public void actionPerformed(ActionEvent e) {

                System.out.println ("-- Event Selection Properties AFTER --");

              timePanel.updateProperties(origProps).dumpProperties();

              System.out.println ("-----------");

          }
       });

       panel.add(okButton, BorderLayout.SOUTH);
       frame.getContentPane().add(panel);

//      dialog.getEventSelectionProperties().dumpProperties();

//           frame.setSize(200,30);        // this has no effect!
       frame.pack();
       frame.setVisible(true);

   }
   */
}   
