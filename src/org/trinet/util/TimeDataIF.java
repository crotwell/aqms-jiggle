package org.trinet.util;
import java.util.Collection;
import java.util.Date;
public interface TimeDataIF {
    public void setTime(double t);
    public double getTime();
    public DateTime getDateTime(); // NOTE able to get UTC leap seconds -aww 2008/02/08
    //public java.util.Date getDate(); // NOTE generic date has no leap seconds -aww 2008/02/08
    public void setTime(Date d);  // NOTE input should be DateTime to get leap seconds -aww 2008/02/08
    public Collection getByTime(double start, double end); // epochseconds?
}
