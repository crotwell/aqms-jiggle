package org.trinet.util;

/**
  Static class that provides time unit information.
*/
public class TimeUnits {
     // LOGIC IN THIS CLASS DEPENDS ON ALL THE ARRAYS BEING CORRECTLY ALIGNED
     private static int k = 0;
     public static final int SECOND = k++;
     public static final int MINUTE = k++;
     public static final int HOUR   = k++;
     public static final int DAY    = k++;
     public static final int WEEK   = k++;
     // no months, too variable
     public static final int YEAR   = k++;

     // don't do months because I don't want to deal with the complexity now.
     // Besides for a delta we don't know WHICH month we're measuring from.
     private static final String unitName[] = {"Second", "Minute", "Hour", "Day", "Week", "Year"};
     private static final String pluralUnitName[] = {"Seconds", "Minutes", "Hours", "Days", "Weeks", "Years"};

     // second in each time unit (NOTE: year assumes 365 days/yr, :. leap year
     // are not handled correctly).
     private static final double secondsIn[] = {1.0, 60., 3600., 86400., 604800., 31536000.};

     TimeUnits() { }

     /** Return the number of seconds in the time unit described by the String
     * 'unit'. String comparison is NOT case sensitive. <p>
     *
     * Allowed units are "Second", "Minute", "Hour", "Day", "Week", "Year" and plural
     * forms "Seconds", "Minutes", "Hours", "Days", "Weeks", "Years" <br>
     * Note that 'month' is not supported because they are variable. Years is
     * assumed to be 365 days.
     *
     * If no match is found or 'unit' is null, 0.0 is returned.
      */
     public static double getSecondsIn(String unit) {
         if (unit == null || unit == "") return 0.0;

         if (unit.endsWith("s")) {
             for (int i = 0; i < pluralUnitName.length; i++) {
               if (unit.equalsIgnoreCase(pluralUnitName[i]))
                 return secondsIn[i];
             }
         }
         else {
            for (int i = 0; i < unitName.length; i++) {
               if (unit.equalsIgnoreCase(unitName[i]))
                  return secondsIn[i];
            }
         }
         return 0.0;
     }

     public static int getUnitIndex(String unit) {
         if (unit == null || unit == "") return -1;

         if (unit.endsWith("s")) {
             for (int i = 0; i < pluralUnitName.length; i++) {
               if (unit.equalsIgnoreCase(pluralUnitName[i]))
                 return i;
             }
         }
         else {
            for (int i = 0; i < unitName.length; i++) {
               if (unit.equalsIgnoreCase(unitName[i]))
                  return i;
            }
         }

         return -1;
     }

     public static String getUnitName(int type) {
         return unitName[type];
     }
     public static String getUnitsName(int type) {
         return pluralUnitName[type];
     }
     public static String [] getKnownUnitNames() {
         String [] names = new String[unitName.length];
         System.arraycopy(unitName,0, names, 0, unitName.length);
         return names;
     }
     public static String [] getKnownUnitsNames() {
         String [] names = new String[pluralUnitName.length];
         System.arraycopy(pluralUnitName,0, names, 0, pluralUnitName.length);
         return names;
     }

     /** Return the number of seconds in the time unit matching the 'unit' enumeration
     * value given.
     *
     * Allowed units are SECOND, MINUTE, HOUR, DAY, WEEK, YEAR
     * Note that 'MONTH' is not supported because they are variable. YEAR is
     * assumed to be 365 days.
     *
     * If no match is found, zero is returned.
      */
     public static double getSecondsIn(int value) {

            if (value > -1 && value <= secondsIn.length) {
               return secondsIn[value];
            }

            return 0.0;
     }
}
