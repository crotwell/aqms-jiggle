//TODO - change method parameters to generic Object type and integrate with DataObjectRangeIF.
package org.trinet.util;
public interface NumberRangeIF {

/** Returns a reference to the object representing the range minimum. */
    Number getMin() ;

/** Returns a reference to the object representing the range maximum. */
    Number getMax() ;

/** Returns true if input object bound's doubleValue() equal those of this instance. */
    boolean equalsRange(NumberRange range) ;

/** Returns true if input object's value is less than this instance's minimum bound. */
    boolean after(Number number) ;

/** Returns true if input range's maximum bound is less than this instance's minimum bound.*/
    boolean after(NumberRange range) ;

/** Returns true if input object's value is greater than this instance's maximum bound.*/
    boolean before(Number number) ;

/** Returns true if input range's minimum bound is greater than this instance's maximum bound.*/
    boolean before(NumberRange range) ;

/** Returns true if input object's value is outside the bounds of this instance. */
    boolean excludes(Number number) ;

/** Returns true if input range is outside the bounds of this instance. */
    boolean excludes(NumberRange range) ;

/** Returns true if input range is outside the bounds of this instance. */
    boolean excludes(Number min, Number max) ;

/** Returns true if input range is within the bounds of this instance. */
    boolean contains(NumberRange range) ;

/** Returns true if input range is within the bounds of this instance. */
    boolean contains(Number min, Number max) ;

/** Returns true if input object's value is inside the bounds of this instance. */
    boolean contains(Number number) ;

/** Returns true if input range overlaps the bounds of this instance. */
    boolean overlaps(NumberRange range) ;

/** Returns true if input range overlaps the bounds of this instance. */
    boolean overlaps(Number min, Number max) ;

/** Returns true if input range encloses the bounds of this instance. */
    boolean within(NumberRange range) ;

/** Returns true if input range encloses the bounds of this instance. */
    boolean within(Number min, Number max) ;

/** Returns (long) (maxValue - minValue) for this instance. */
    long longExtent() ;

/** Returns (double) (maxValue - minValue) for this instance. */
    double doubleExtent() ;

/** Returns true if min > max for this instance. */
    boolean isReversed() ;

/** Returns false if (min == null || max == null || isReversed()) for this instance. */
    boolean isValid() ;

/** 
* Sets the minimum bound of the range to the input.
*/
    void setMin(Number min);

/** 
* Sets the maximum bound of the range to the input.
*/
    void setMax(Number max);

/** 
* Sets the minimum, maximum bounds of the range to the input range.
*/
    void setLimits(Number min, Number max);

/** 
* Sets the range bounds to those of the input object.
*/
    void setLimits(NumberRange range);

/**
* Sets the appropiate minimum or maximum bound to extend the range to include the input value.
* Does a no-op if contains(Number) == true.
*/
    void include(Number dataObject);

/**
* Sets the appropiate range bounds to include the input range extent.
* Does a no-op if contains(NumberRange) == true.
*/
    void include(NumberRange range);

/**
* Returns String of values of min and max bounds separated by ", ".
* Inserts "NULL" if bound == null or its value is equivalent to an SQL null, undefined.
*/
    String toStringSQL();
}
