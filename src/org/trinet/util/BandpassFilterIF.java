package org.trinet.util;

public interface BandpassFilterIF extends FilterIF {
/** Return reference to input object parameter which contains a representation of a waveform time series.
* of input and output objects.
*/
    void setPassBand(double loHz, double hiHz);
    double getLoHz();
    double getHiHz();
    void setLoHz(double hz);
    void setHiHz(double hz);
    public boolean setFilter(int sampleRate, double lf, double hf, int norder, boolean reverse);
}
