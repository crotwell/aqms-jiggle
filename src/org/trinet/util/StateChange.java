package org.trinet.util;
import org.trinet.jasi.*;
public class StateChange {
     public static final String ADDED      = "added";
     public static final String CHANGED    = "changed";
     public static final String DELETED    = "deleted";
     public static final String ORDER      = "order";
     public static final String REMOVED    = "removed";
     public static final String REPLACED   = "replaced";
     public static final String SELECTED   = "selected";
     public static final String UNCHANGED  = "unchanged";
     public static final String UPDATED    = "updated";

     private final String stateName;
     private final Object source;
     private final Object value;
     public StateChange(Object source, String stateName, Object value) {
       this.source    = source;
       this.stateName = stateName;
       this.value     = value;
     }
     public Object getSource() { return source; }
     public Object getValue() { return value; }
     public String getStateName() { return stateName; }
}
