package org.trinet.util;

/**
 *   Create a "deep" clone of an object.
 *   Submitted by John Dumas to:
 *   http://www.faqs.org/faqs/computer-lang/java/programmers/faq/
     It uses serialization to write an object into a byte array, and reads
     it back to reconstitute a fresh copy.
 */
     import java.io.ByteArrayOutputStream;
     import java.io.ByteArrayInputStream;
     import java.io.ObjectOutputStream;
     import java.io.ObjectInputStream;

     public class Cloner {
        private Cloner() {}

        public static Object cloneObject(Object o) throws Exception {
           ByteArrayOutputStream bOut = new ByteArrayOutputStream();
           ObjectOutputStream out     = new ObjectOutputStream(bOut);

           out.writeObject(o);

           ByteArrayInputStream bIn =
                    new ByteArrayInputStream(bOut.toByteArray());
           ObjectInputStream in     = new ObjectInputStream(bIn);

           return(in.readObject());
        }

        /*
        public static void main(String args[]) throws Exception {
           java.util.Vector v = new java.util.Vector();
           v.addElement(new StringBuffer("Hello"));

           java.util.Vector vClone =
                      (java.util.Vector)Cloner.cloneObject(v);

           // Changing the StringBuffer int the cloned vector has no
           // effect on the original StringBuffer object --
           // demonstrating that we have indeed done a deep copy

           ((StringBuffer)vClone.elementAt(0)).append(" world");

           StringBuffer sb = (StringBuffer)v.elementAt(0);
           System.out.println(sb.toString());

           sb = (StringBuffer)vClone.elementAt(0);
           System.out.println(sb.toString());

           int array[] = { 1, 2, 3, 4, 5 };

           int arrayClone[] = (int [])Cloner.cloneObject(array);

           // Again, changes to an element in the cloned array do not
           // have any effect on the original

           arrayClone[0]++;

           System.out.println(array[0]);
           System.out.println(arrayClone[0]);
        }
        */
     }
