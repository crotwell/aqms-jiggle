package org.trinet.util;

/** Coda duration calculation. This is based on documentation for HYPOINVERSE:
 *  Fred Klein Open-File Report pg. 41 (attributed to Lee, et al (1972)
 *
 <tt>
MD = -0.87 + 2.0*log(F*c) +0.0035*d
Where:
  F = secs from P-arrival to when coda amp < 1cm on Geotech viewer
  c = station correction (multiplier)
  d = epicenteral distance
</tt>
This model does not apply any station or gain corrections.
*/
public class CodaModelMD implements CodaModelIF{
   public CodaModelMD() { }

   // magic numbers from Lee, et al
   static double fac1 = 0.87;
   static double fac2 = 2.00;
   static double fac3 = 0.0035;

   public void setFactors (double factor1, double factor2, double factor3) {
     fac1 = factor1;
     fac2 = factor2;
     fac3 = factor3;
   }

   /** Given a mag return the expected coda duration (tau). */
   public double getDuration (double distance, double mag) {
     double stuff = (mag + fac1 - (fac3*distance)) / fac2;
     return  Math.pow(10.0, stuff);
   }

   /** Given a duration (tau) return the coda mag. */
   public double getMag (double distance, double duration) {
     return (-fac1 + (fac2*MathTN.log10(duration)) + (fac3*distance));
   }

}  // end of CodaModel

