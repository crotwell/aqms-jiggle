package org.trinet.util;
import org.postgresql.pljava.annotation.Function;
import org.trinet.jiggle.common.DbConstant;

import java.io.*;

public class FileUtil {

    // STATIC METHOD CALLED BY ORACLE STORED PROCEDURE !
    @Function(schema=DbConstant.SCHEMA_FILEUTIL, name="delete_file")
    public static int delete(String path) {
        try {
            File file = new File(path);
            boolean exists = file.exists();
            if (exists) return (file.delete()) ? 1 : 0;
            else return 0;
        }
        catch (SecurityException  ex) {
             ex.printStackTrace();
             System.err.println("Check file delete access permissions for: " + path);
             return -1;
        }
    }

    // STATIC METHOD CALLED BY ORACLE STORED PROCEDURE !
    @Function(schema=DbConstant.SCHEMA_FILEUTIL, name="create_file")
    public static int create(String path) {
        try {
            File file = new File(path);
            return (file.createNewFile()) ? 1 : 0;
        }

        catch (SecurityException  ex) {
             ex.printStackTrace();
             System.err.println("Check file write access permissions for: " + path);
             return -1;
        }

        catch (IOException  ex) {
             ex.printStackTrace();
             return -2;
        }

    }
    // STATIC METHOD CALLED BY ORACLE STORED PROCEDURE !
    @Function(schema=DbConstant.SCHEMA_FILEUTIL, name="user_dir")
    public static String getUserDir() {
        try {
            return System.getProperty("user.dir");
        }
        catch (SecurityException  ex) {
             ex.printStackTrace();
             System.err.println("Check file write access permissions for system properties");
             return null;
        }
    }
    // STATIC METHOD CALLED BY ORACLE STORED PROCEDURE !
    @Function(schema=DbConstant.SCHEMA_FILEUTIL, name="user_home")
    public static String getUserHomeDir() {
        try {
            return System.getProperty("user.home");
        }
        catch (SecurityException  ex) {
             ex.printStackTrace();
             System.err.println("Check file write access permissions for system properties");
             return null;
        }
    }
  // STATIC METHOD CALLED BY ORACLE STORED PROCEDURE !
    public static final void main(String [] args) {
        int status = FileUtil.create(args[0]);
        System.out.println("create status: " + status);
        File file = new File(args[0]);
        System.out.println("file exists: " +  file.exists());
        try {
            FileOutputStream fos = new FileOutputStream(args[0]);
            fos.write("this is a test file".getBytes());
            fos.close();
        } catch (IOException ex) { ex.printStackTrace();}
        //FileUtil.delete(args[0]);
        //System.out.println("delete status: " + status);
        System.out.println("file exists: " + file.exists());
        System.out.println("user_dir: " + FileUtil.getUserDir());
        System.out.println("user_home: " + FileUtil.getUserHomeDir());
    }
} // end of FileUtil class
