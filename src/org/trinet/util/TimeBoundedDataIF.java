package org.trinet.util;
import org.trinet.jdbc.datatypes.*;

public interface TimeBoundedDataIF {

    public DataDouble getStart();
    public double getEpochStart();
    public void setStart(DataDouble start);
    public void setStart(double start);

    public DataDouble getEnd();
    public double getEpochEnd();
    public void setEnd(DataDouble end);
    public void setEnd(double end);

    public double getDuration();
    public TimeSpan getTimeSpan();
    public void setTimeSpan(TimeSpan ts);
    public void setTimeQuality(double qual);
    public void setTimeQuality(DataDouble qual);
    public DataDouble getTimeQuality();

}
