package org.trinet.util;
/**
 * General purpose range (pair) of float values
*/
public class FloatRange extends NumberRange {

    public FloatRange(float min, float max) {
	super(new Float(min), new Float(max));
    }

    public FloatRange(Float min, Float max) {
	super(min, max);
    }

/**
 * Returns the min value of this range.
*/
    public float getMinValue() {
	return getMin().floatValue();
    }

/** Returns true if both the minimum and maximum bound values == NullValueDb.NULL_FLOAT.
* @see org.trinet.jdbc.NullValueDb 
*/
    public boolean isNull() {
        return ( min.equals(new Float(org.trinet.jdbc.NullValueDb.NULL_FLOAT)) &&
                 max.equals(new Float(org.trinet.jdbc.NullValueDb.NULL_FLOAT)) );
    }

/** Sets the minimum and maximum bounds == NullValueDb.NULL_FLOAT.
* @see org.trinet.jdbc.NullValueDb 
*/

    public void setNull() {
        setMin(org.trinet.jdbc.NullValueDb.NULL_FLOAT);
        setMax(org.trinet.jdbc.NullValueDb.NULL_FLOAT);
    }

/**
 * Returns the max value of this range.
*/
    public float getMaxValue() {
	return getMax().floatValue();
    }

/**
 * Sets the min value of this range.
*/
    public void setMin(float min) {
	setMin(new Float(min));
    }

/**
 * Sets the max value of this range.
*/
    public void setMax(float max) {
	setMax(new Float(max));
    }

/**
 * Sets the max, min values of this range.
*/
    public void setLimits(float min, float max) {
	setLimits(new Float(min), new Float(max));
    }

    public void include(float val) {
	include(new Float(val));
    }

/**
 * Adjust current range to include range in argument
*/
    public void include(FloatRange range) {
        include((NumberRange) range);
    }

/**
* Return true if number is within specified bounds inclusive.
*/
    public boolean contains(float val) {
        return contains(new Float(val));
    }

/**
* Return true if range is within this range inclusive.
*/
    public boolean contains(FloatRange range) {
	return contains((NumberRange) range);
    }

/**
* Returns the difference between the upper and lower range bounds.
*/
    public float size() {
	return (float) doubleExtent();
    }

/**
* Returns min + "," + max.
*/
    public String toString() {
        return min + "," + max;
    }

/*
    public static void main(String [] args) {

         System.out.println("++++++++ BEGIN TEST ++++++++++");

         FloatRange dr = new FloatRange(-10.f, 10.f);
         FloatRange dr2 = new FloatRange(-20.f, 20.f);

         IntRange dr3 = new IntRange(20, 20);

         dr.dump(dr2, dr3);
         dr3.setLimits(1,21);
         dr.dump(dr2, dr3);

         dr.dump1(-11.f);
         dr.dump1(-1.f);
         dr.dump1(0.f);
         dr.dump1(1.f);
         dr.dump1(11.f);

         dr.dump2(dr2);

         System.out.println("Test dr.setLimits(-14, 14)");
         dr.setLimits(-14.f,14.f);
         dr.dump2(dr2);

         System.out.println("Test dr.include(-16, 16)");
         dr.include(-16);
         dr.include(16);
         dr.dump2(dr2);

         System.out.println("Test dr.include(dr2)");
         dr.include(dr2);
         dr.dump2(dr2);

         System.out.println("Test dr.setMax(0); dr2.setMin(0)");
         dr.setMax(0.f);
         dr2.setMin(0.f);
         dr.dump2(dr2);

         System.out.println("Test dr.setMax(0); dr2.setMin(1)");
         dr.setMax(0.f);
         dr2.setMin(1.f);
         dr.dump2(dr2);

    }

    public void dump(NumberRange dr2, NumberRange dr3) {
         System.out.println("Test equalsRange: ");
         System.out.println("     dr2.toString(): " + dr2.toString());
         System.out.println("     dr3.toString(): " + dr3.toString());
         System.out.println("     dr2.equalsRange(dr3) : " +  dr2.equalsRange(dr3));
         System.out.println("------------------\n");
    }

     public void dump1(float number) {
         Float val = new Float(number);
         System.out.println("Dump Range bounds, size: " + getMinValue() + ", " + getMaxValue() + " size(): " + size());
         System.out.println(" Test input val: " + val);
         System.out.println(" after(val)    : " + after(val));
         System.out.println(" before(val)   : " + before(val));
         System.out.println(" excludes(val) : " + excludes(val));
         System.out.println(" contains(val) : " + contains(val));
         System.out.println("------------------\n");
    }

    public void dump2(FloatRange dr2) {
         System.out.println("Dump Range, number : " + getMinValue() + ", " + getMaxValue() + " size(): " + size());
         System.out.println(" Test Range2 Min,max: " + dr2.getMinValue() + ", " + dr2.getMaxValue() );

         System.out.println(" within(dr2)       : " + within(dr2));
         System.out.println(" dr2.within(this)  : " + dr2.within(this)); 

         System.out.println(" overlaps(dr2)     : " + overlaps(dr2));
         System.out.println(" dr2.overlaps(this): " + dr2.overlaps(this));

         System.out.println(" dr2.after(this)   : " + dr2.after(this));
         System.out.println(" after(dr2)        : " + after(dr2));

         System.out.println(" dr2.before(this)  : " + dr2.before(this));
         System.out.println(" before(dr2)       : " + before(dr2));

         System.out.println(" dr2.excludes(this): " + dr2.excludes(this));
         System.out.println(" excludes(dr2)     : " + excludes(dr2));

         System.out.println(" dr2.contains(this): " + dr2.contains(this));
         System.out.println(" contains(dr2)     : " + contains(dr2));
         System.out.println("------------------\n");

    }
*/
}
