package org.trinet.util;
public interface ConfigurationSourceIF {
  public static final int CONFIG_SRC_UNDEFINED  = 0;
  public static final int CONFIG_SRC_DATABASE   = 1;
  public static final int CONFIG_SRC_FILE       = 2;
  public static final int CONFIG_SRC_SECTION    = 3;
  public static final int CONFIG_SRC_STRING     = 4;
  public void configure();
  public void configure(int source, String location, String section);
}
