package org.trinet.util;
//import java.util.regex.*;
import jregex.Pattern; // aww 02/15/2005 try this pkg
import jregex.Replacer; // aww 02/15/2005 try this pkg

public class DbStringMatcher extends StringMatcher {

    protected static String dbMatchOneChar = "_";
    protected static String dbMatchAllChar = "%";

    public DbStringMatcher() {}

    public DbStringMatcher(String [] dbString) {
      this(dbString, 0);
    }
    public DbStringMatcher(String [] dbString, int flags) {
      super(dbString, flags);
    }

    public void createPatternList(String [] dbString, int flags) {
      this.regex = dbString;
      this.size = dbString.length;
      this.flags = flags;
      patterns = new Pattern[size];
      for (int idx =0; idx < size; idx++) {
        patterns[idx] = (flags > 0) ?
           //Pattern.compile(dbWildCardsToRegex(dbString[idx]), flags) : Pattern.compile(dbWildCardsToRegex(dbString[idx])); //jdk1.4
           new Pattern(dbWildCardsToRegex(dbString[idx]), flags) : new Pattern(dbWildCardsToRegex(dbString[idx])); // jregex
      }
    }
    public String dbWildCardsToRegex(String dbString) {
      //return dbString.replaceAll(dbMatchOneChar,".").replaceAll(dbMatchAllChar,".*"); // jdk1.4
      StringBuffer sb = new StringBuffer(dbString);
      int idx = 0;
      while (true) {
        idx = dbString.indexOf("_", idx);
        if (idx == -1) break;
        sb.replace(idx, idx+1,".");
        idx++;
      }
      idx = 0;
      while (true) {
        idx = dbString.indexOf("%", idx);
        if (idx == -1) break;
        sb.replace(idx, idx+1,".");
        idx++;
      }
      idx = 0;
      int count = 0;
      while (true) {
        idx = dbString.indexOf("%", idx);
        if (idx == -1) break;
        count++;
        sb.insert(idx+count,"*");
        idx++;
      }
      return sb.toString();
    }
    public String regexToDbWildCards(String regex) {
      //return regex.replaceAll(".*",dbMatchAllChar).replaceAll(".",dbMatchOneChar); // jdk1.4
      StringBuffer sb = new StringBuffer(regex);
      int idx = 0;
      int count = 0;
      while (true) {
        idx = regex.indexOf(".*", idx);
        if (idx == -1) break;
        sb.replace(idx-count, idx-count+2, "%");
        count++;
        idx++;
      }
      String str = sb.toString();
      idx = 0;
      while (true) {
        idx = str.indexOf(".", idx);
        if (idx == -1) break;
        sb.replace(idx, idx+1, "_");
        idx++;
      }
      return sb.toString();
    }

/*
  static public final class Tester {
    public static final void main(String [] args) {
      DbStringMatcher sm = new DbStringMatcher(new String [] {"HHZ","HLZ","HLE"});
      System.out.println("HHZ sm.matches(HHZ) t: " + sm.matches("HHZ"));
      System.out.println("HLZ sm.matches(HLZ) t: " + sm.matches("HLZ"));
      System.out.println("HLE sm.matches(HLE) t: " + sm.matches("HLE"));
      System.out.println("above sm.matches(ABC) f: " + sm.matches("ABC"));
      sm.createPatternList(new String [] {"H_Z","BLZ","ABC"});
      System.out.println("H_Z sm.matches(HHZ) t: " + sm.matches("HHZ"));
      System.out.println("H_Z sm.matches(HLZ) t: " + sm.matches("HLZ"));
      System.out.println("H_Z sm.matches(HLE) f: " + sm.matches("HLE"));
      sm.createPatternList(new String [] {"DEF","BLZ","H%"});
      System.out.println("H%  sm.matches(HHZ) t: " + sm.matches("HHZ"));
      System.out.println("H%  sm.matches(HLZ) t: " + sm.matches("HLZ"));
      System.out.println("H%  sm.matches(HLE) t: " + sm.matches("HLE"));
      System.out.println("H%  sm.matches(ABC) f: " + sm.matches("ABC"));
      //sm.createPatternList(new String [] {"DEF","BLZ","H%"}, Pattern.CASE_INSENSITIVE); //jdk1.4
      sm.createPatternList(new String [] {"DEF","BLZ","H%"}, Pattern.IGNORE_CASE); // jregex
      System.out.println("H% nocase sm.matches(hhz) t: " + sm.matches("hhz"));
      System.out.println("H% nocase sm.matches(hlz) t: " + sm.matches("hlz"));
      System.out.println("H% nocase sm.matches(hle) t: " + sm.matches("hle"));
      System.out.println("H% nocase sm.matches(abc) f: " + sm.matches("abc"));
      System.out.println("sm.dbWildCardsToRegex('___ )  ...    = " + sm.dbWildCardsToRegex("___")); 
      System.out.println("sm.dbWildCardsToRegex(%HD%)   .*HD.* = " + sm.dbWildCardsToRegex("%HD%")); 
      System.out.println("sm.dbWildCardsToRegex(_%)     ..*    = " + sm.dbWildCardsToRegex("_%")); 
      System.out.println("sm.regexToDbWildCards(H.*D.*) H%D%   = " + sm.regexToDbWildCards("H.*D.*")); 
      System.out.println("sm.regexToDbWildCards(H.D.)   H_D_   = " + sm.regexToDbWildCards("H.D.")); 
      System.out.println("sm.regexToDbWildCards(...)    ___    = " + sm.regexToDbWildCards("...")); 
      Pattern p = new Pattern("00   00\\.00");
      Replacer r = p.replacer("0 0.0");
      System.out.println(r.replace("00   00.00abc00   00.00def00   00.00ghi"));
      r = p.replacer("\n   ");
      System.out.println(r.replace("00   00.00abc00   00.00def00   00.00ghi"));
    }
  }
*/
}
