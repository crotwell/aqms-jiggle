package org.trinet.util;

/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @author
 * @version 1.0
 */

public interface CodaModelIF {
   /** Given a mag return the expected coda duration. */
   abstract public double getDuration (double distance, double mag);
   /** Given a duration return the coda mag. */
   abstract public double getMag (double distance, double duration) ;
}