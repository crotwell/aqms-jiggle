// Derivative of WaveViewSettingsToolbar by Dan Cervelli in AVO Swarm distribution
package gov.usgs.swarm.wave;

import gov.usgs.swarm.Images;
import gov.usgs.swarm.SwarmUtil;
import gov.usgs.swarm.wave.JiggleSwarmWaveViewSettings.ViewType;
import gov.usgs.util.Util;

import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemListener;
import java.util.HashSet;
import java.util.Set;

import javax.swing.AbstractAction;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;


public class JiggleSwarmWaveViewSettingsToolbar {

    private ButtonGroup waveTypes;
    private JButton waveSet;
    private JToggleButton waveToggle;
    private JToggleButton spectraToggle;
    private JToggleButton spectrogramToggle;

    private JToggleButton logSettingsToggle;

    private JToggleButton filterToggle;
    private JButton resetMemoryButton;
    private JButton yScaleInButton;
    private JButton yScaleOutButton;
    
    private Set<JiggleSwarmWaveViewSettings> settingsSet;
//    private WaveViewSettings settings;
    
    public JiggleSwarmWaveViewSettingsToolbar(JiggleSwarmWaveViewSettings s, JToolBar dest, JComponent keyComp)
    {
        settingsSet = new HashSet<JiggleSwarmWaveViewSettings>();
        createUI(dest, keyComp);
        setSettings(s);
    }
    
    private void enableWaveButtons(boolean tf) {
        logSettingsToggle.setEnabled(!tf);

        resetMemoryButton.setEnabled(tf);
        yScaleInButton.setEnabled(tf);
        yScaleOutButton.setEnabled(tf);
    }

    public void createUI(JToolBar dest, JComponent keyComp)
    {
        waveSet = SwarmUtil.createToolBarButton(
                Images.getIcon("wavesettings"),
                "Wave view settings (?)",
                new ActionListener()
                {
                    public void actionPerformed(ActionEvent e)
                    {
                        JiggleSwarmWaveViewSettings s = null;
                        int count = settingsSet.size();
                        if (count <= 0) {
                            s = new JiggleSwarmWaveViewSettings();
                        }
                        else {
                            JiggleSwarmWaveViewSettings [] array = settingsSet.toArray(new JiggleSwarmWaveViewSettings [count]);
                            s = new JiggleSwarmWaveViewSettings(array[count-1]);
                        }
                        JFrame frame = (JFrame) waveSet.getTopLevelAncestor();
                        JiggleSwarmWaveViewSettingsDialog wvsd =
                                                    JiggleSwarmWaveViewSettingsDialog.getInstance(frame, s, settingsSet.size());
                        wvsd.setVisible(true); // user modified here

                        // Do all the panels in set, for standard view frame just one WaveViewPanel, but for the clipboard may be many panels
                        for (JiggleSwarmWaveViewSettings settings : settingsSet)
                        {
                            settings.copy(s);
                            //System.out.println("Toolbar new settings:\n" + settings.dumpSettings());
                            settings.notifyView();
                        }
                    }
                });
        Util.mapKeyStrokeToButton(keyComp, "shift SLASH", "settings", waveSet);
        dest.add(waveSet);
        
        waveToggle = SwarmUtil.createToolBarToggleButton(
                Images.getIcon("wave"),
                "Wave view (W or ,)",
                new ActionListener()
                {
                    public void actionPerformed(ActionEvent e)
                    {
//                        if (settings != null)
                        for (JiggleSwarmWaveViewSettings settings : settingsSet)
                            settings.setType(ViewType.WAVE);
                    }
                });
        waveToggle.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent evt) {
                if (evt.getSource() == waveToggle)
                  enableWaveButtons(waveToggle.isSelected());
            }
        });

        Util.mapKeyStrokeToButton(keyComp, "COMMA", "wave1", waveToggle);
        Util.mapKeyStrokeToButton(keyComp, "W", "wave2", waveToggle);
//        waveToggle.setSelected(true);
        dest.add(waveToggle);
        
        spectraToggle = SwarmUtil.createToolBarToggleButton(
                Images.getIcon("spectra"),
                "Spectra view (S or .)",
                new ActionListener()
                {
                    public void actionPerformed(ActionEvent e)
                    {
//                        if (settings != null)
                        for (JiggleSwarmWaveViewSettings settings : settingsSet)
                            settings.setType(ViewType.SPECTRA);
                    }
                });
        Util.mapKeyStrokeToButton(keyComp, "PERIOD", "spectra1", spectraToggle);
        Util.mapKeyStrokeToButton(keyComp, "S", "spectra2", spectraToggle);
        dest.add(spectraToggle);
        
        spectrogramToggle = SwarmUtil.createToolBarToggleButton(
                Images.getIcon("spectrogram"),
                "Spectrogram view (G or /)",
                new ActionListener()
                {
                    public void actionPerformed(ActionEvent e)
                    {
//                        if (settings != null)
                        for (JiggleSwarmWaveViewSettings settings : settingsSet)                            
                            settings.setType(ViewType.SPECTROGRAM);
                    }
                });
        Util.mapKeyStrokeToButton(keyComp, "SLASH", "spectrogram1", spectrogramToggle);
        Util.mapKeyStrokeToButton(keyComp, "G", "spectrogram2", spectrogramToggle);
        dest.add(spectrogramToggle);
        dest.addSeparator();
        
        logSettingsToggle = SwarmUtil.createToolBarToggleButton(
                Images.getIcon("graybullet"),
                "Toggle log settings (L)",
                new ActionListener()
        //keyComp.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke("L"), "cycleLogSettings");
        //keyComp.getActionMap().put("cycleLogSettings", new AbstractAction()
                {
                    public static final long serialVersionUID = -1;
                    public void actionPerformed(ActionEvent e)
                    {
//                        if (settings != null)
                        for (JiggleSwarmWaveViewSettings settings : settingsSet)
                            settings.cycleLogSettings();
                    }    
                });
        Util.mapKeyStrokeToButton(keyComp, "L", "cycleLogSettings", logSettingsToggle);
        dest.add(logSettingsToggle);
                
        filterToggle = SwarmUtil.createToolBarToggleButton(
                Images.getIcon("server"),
                "Toggle filter (F)",
                new ActionListener()
        //keyComp.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke("F"), "toggleFilter");
        //keyComp.getActionMap().put("toggleFilter", new AbstractAction()
                {
                    public static final long serialVersionUID = -1;
                    public void actionPerformed(ActionEvent e)
                    {
//                        if (settings != null)
                        for (JiggleSwarmWaveViewSettings settings : settingsSet)
                            settings.toggleFilter();
                    }    
                });                
        Util.mapKeyStrokeToButton(keyComp, "F", "toggleFilter", filterToggle);
        dest.add(filterToggle);
        dest.addSeparator();

        resetMemoryButton = SwarmUtil.createToolBarButton(
                Images.getIcon("ruler"),
                "Reset autoscale memory (R)",
                new ActionListener()
        //keyComp.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke("R"), "resetAutoScale");
        //keyComp.getActionMap().put("resetAutoScale", new AbstractAction()
                {
                    public static final long serialVersionUID = -1;
                    public void actionPerformed(ActionEvent e)
                    {
//                        if (settings != null)
                        for (JiggleSwarmWaveViewSettings settings : settingsSet) {
                            settings.resetAutoScaleMemory();
                        }
                    }    
                });
        dest.add(resetMemoryButton);
        Util.mapKeyStrokeToButton(keyComp, "R", "resetAutoScale", resetMemoryButton);

        yScaleInButton= SwarmUtil.createToolBarButton(
                Images.getIcon("yminus"),
                "Zoom amp axis in ( [ )",
                new ActionListener()
        //keyComp.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke("OPEN_BRACKET"), "yScaleIn");
        //keyComp.getActionMap().put("yScaleIn", new AbstractAction()
                {
                    public static final long serialVersionUID = -1;
                    public void actionPerformed(ActionEvent e)
                    {
//                        if (settings != null)
                        for (JiggleSwarmWaveViewSettings settings : settingsSet)
                            settings.adjustScale(1.0 / 1.25);
                    }    
                });
        dest.add(yScaleInButton);
        Util.mapKeyStrokeToButton(keyComp, "OPEN_BRACKET", "yScaleIn", yScaleInButton);
        
        yScaleOutButton = SwarmUtil.createToolBarButton(
                Images.getIcon("yplus"),
                "Zoom amp axis out ( ] )", 
                new ActionListener()
        //keyComp.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke("CLOSE_BRACKET"), "yScaleOut");
        //keyComp.getActionMap().put("yScaleOut", new AbstractAction()
                {
                    public static final long serialVersionUID = -1;
                    public void actionPerformed(ActionEvent e)
                    {
//                        if (settings != null)
                        for (JiggleSwarmWaveViewSettings settings : settingsSet)
                            settings.adjustScale(1.25);
                    }    
                });
                
        dest.add(yScaleOutButton);
        Util.mapKeyStrokeToButton(keyComp, "CLOSE_BRACKET", "yScaleOut", yScaleOutButton);
        //
        waveTypes = new ButtonGroup();
        waveTypes.add(waveToggle);
        waveTypes.add(spectraToggle);
        waveTypes.add(spectrogramToggle);
    }
    
    public void clearSettingsSet()
    {
        settingsSet.clear();
    }
    
    public void addSettings(JiggleSwarmWaveViewSettings s)
    {
        if (s != null)
        {
            settingsSet.add(s);
            s.toolbar = this;
            settingsChanged();
        }
//        System.out.println("addSettings " + s + " " + settingsSet.size());
    }
    
    public void removeSettings(JiggleSwarmWaveViewSettings s)
    {
        settingsSet.remove(s);
        if (s != null)
        {
            s.toolbar = null;
            settingsChanged();
        }
//        System.out.println("removeSettings " + s + " " + settingsSet.size());
    }
    
    public void setSettings(JiggleSwarmWaveViewSettings s)
    {
        clearSettingsSet();
        addSettings(s);
//        settingsSet.add(s);
//        settings = s;
    }
    
    public void settingsChanged()
    {
        boolean w = false;
        boolean s = false;
        boolean sg = false;
        for (JiggleSwarmWaveViewSettings set : settingsSet)
        {
            if (set.viewType == ViewType.WAVE)
                w = true;
            if (set.viewType == ViewType.SPECTRA)
                s = true;
            if (set.viewType == ViewType.SPECTROGRAM)
                sg = true;
        }
        // TODO: fix for Java 1.5, clearSelection was added in 1.6
        try {
                    //waveTypes.clearSelection();
                    waveTypes.setSelected(waveTypes.getSelection(),false);
                } catch (Throwable e) {}
                //System.out.println("JVWST debug settingsChanged: wave:" + w + " spectra:" + s + " sgram:" + sg);
        waveToggle.setSelected(w && !s && !sg);    
        spectraToggle.setSelected(!w && s && !sg);
        spectrogramToggle.setSelected(!w && !s && sg);
//        waveToggle.setSelected(settings.viewType == ViewType.WAVE);    
//        spectraToggle.setSelected(settings.viewType == ViewType.SPECTRA);
//        spectrogramToggle.setSelected(settings.viewType == ViewType.SPECTROGRAM);
    }
}
