// Derivative of WaveClipboardFrame by Dan Cervelli in AVO Swarm distribution
package gov.usgs.swarm.wave;

import gov.usgs.swarm.Images;
import gov.usgs.swarm.Metadata;
import gov.usgs.swarm.Swarm;
// import gov.usgs.swarm.SwarmFrame;
import gov.usgs.swarm.SwarmUtil;
import gov.usgs.swarm.SwingWorker;
import gov.usgs.swarm.Throbber;
import gov.usgs.swarm.TimeListener;
//import gov.usgs.swarm.chooser.DataChooser;
import gov.usgs.swarm.data.CachedDataSource;
import gov.usgs.swarm.data.SeismicDataSource;
//import gov.usgs.swarm.heli.HelicorderViewPanelListener;
import gov.usgs.util.Time;
import gov.usgs.util.Util;
import gov.usgs.util.png.PngEncoder;
import gov.usgs.util.png.PngEncoderB;
import gov.usgs.util.ui.ExtensionFileFilter;
import gov.usgs.vdx.data.wave.SAC;
import gov.usgs.vdx.data.wave.Wave;

import org.trinet.jiggle.Jiggle;
import org.trinet.jiggle.JiggleSwarmDataSource;
import org.trinet.util.graphics.IconImage;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.TimeZone;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
//import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollPane;
//import javax.swing.JSplitPane;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
//import javax.swing.event.InternalFrameAdapter;
//import javax.swing.event.InternalFrameEvent;
import javax.swing.event.EventListenerList;

/**
 * The wave clipboard internal frame.
 */
public class JiggleSwarmWaveClipboard extends JPanel // extends SwarmFrame
{
    public static final long serialVersionUID = -1;
    private static final Color SELECT_COLOR = new Color(200, 220, 241);
    private static final Color BACKGROUND_COLOR = new Color(0xf7, 0xf7, 0xf7);
        
    //private JSplitPane split;
    private JScrollPane scrollPane;
    private Box waveBox;
    private List<JiggleSwarmWaveViewPanel> waves;
    private Set<JiggleSwarmWaveViewPanel> selectedSet;
    private JToolBar toolbar;
    //private JPanel mainPanel;
    private JLabel statusLabel;
    private JToggleButton linkButton;
    private JButton sizeButton;
    private JButton syncButton;
    private JButton sortButton;
    private JButton titleAllButton;
    private JButton removeAllButton;
    private JButton clearCacheButton;
    private JButton saveButton;
    private JButton saveAllButton;
    private JButton openButton;
    private JButton captureButton;
    private JButton histButton; 
    private JButton selectAllButton; 
    private JButton deselectAllButton; 
    private DateFormat saveAllDateFormat;
    
    private JiggleSwarmWaveViewPanelListener selectListener;
    private JiggleSwarmWaveViewSettingsToolbar waveToolbar;
    
    private JButton upButton;
    private JButton downButton;
    private JButton removeButton;
    private JButton compXButton;
    private JButton expXButton;
    private JButton copyButton;
    private JButton forwardButton;
    private JButton backButton;
    private JButton gotoButton;
    
    private JPopupMenu popup;
    
    private Map<JiggleSwarmWaveViewPanel, Stack<double[]>> histories;
    
    //private HelicorderViewPanelListener linkListener;
    //private boolean heliLinked = true;
    
    private Throbber throbber;
    
    private int waveHeight = -1;
    
    private int lastClickedIndex = -1;
    
        protected JFileChooser chooser = new JFileChooser();
        protected CachedDataSource readFileCache = new CachedDataSource();
        //protected DataChooser dataChooser = new DataChooser();

    public JiggleSwarmWaveClipboard()
    {
        //super("Wave Clipboard", true, true, true, false);
        this.setFocusable(true); // ??
        selectedSet = new HashSet<JiggleSwarmWaveViewPanel>();
        saveAllDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        //saveAllDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        saveAllDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        waves = new ArrayList<JiggleSwarmWaveViewPanel>();
        histories = new HashMap<JiggleSwarmWaveViewPanel, Stack<double[]>>();
        createUI();
        /*
                 linkListener = new HelicorderViewPanelListener() 
                {
                    public void insetCreated(double st, double et)
                    {
                        if (heliLinked)
                            repositionWaves(st, et);
                    }
                };
                */
    }

        // public HelicorderViewPanelListener getLinkListener() { return linkListener; }

    private EventListenerList timeListeners = new EventListenerList();
    public void addTimeListener(TimeListener tl)
    {
        timeListeners.add(TimeListener.class, tl);
    }
    
    public void removeTimeListener(TimeListener tl)
    {
        timeListeners.remove(TimeListener.class, tl);
    }
    
    public void fireTimeChanged(double j2k)
    {
        Object[] ls = timeListeners.getListenerList();
        for (int i = ls.length - 2; i >= 0; i -= 2)
            if (ls[i] == TimeListener.class)
                ((TimeListener)ls[i + 1]).timeChanged(j2k);
    }
    

    private void createUI()
    {
        //this.setFrameIcon(Images.getIcon("clipboard"));
        //this.setLocation(JiggleSwarmDataSource.config.clipboardX, JiggleSwarmDataSource.config.clipboardY);
        //this.setDefaultCloseOperation(JInternalFrame.DO_NOTHING_ON_CLOSE);
        if (JiggleSwarmDataSource.config.clipboardWidth > 0 && JiggleSwarmDataSource.config.clipboardHeight > 0) {
          this.setPreferredSize(new Dimension(JiggleSwarmDataSource.config.clipboardWidth, JiggleSwarmDataSource.config.clipboardHeight));
        }
        this.setMinimumSize(new Dimension(150,100));
        
        toolbar = SwarmUtil.createToolBar();
        //mainPanel = new JPanel(new BorderLayout());
                JPanel mainPanel = this; // aww
                mainPanel.setLayout(new BorderLayout());
        
        createMainButtons();
        createWaveButtons();

        mainPanel.add(toolbar, BorderLayout.NORTH);
        
        waveBox = new Box(BoxLayout.Y_AXIS);
        scrollPane = new JScrollPane(waveBox);
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        scrollPane.getVerticalScrollBar().setUnitIncrement(40);
        //
    //split = SwarmUtil.createStrippedSplitPane(JSplitPane.HORIZONTAL_SPLIT, dataChooser, scrollPane);
    //split.setDividerLocation(JiggleSwarmDataSource.config.chooserDividerLocation);
    //split.setDividerSize(4);
    //setChooserVisible(JiggleSwarmDataSource.config.chooserVisible);
    //dataChooser.setDividerLocation(JiggleSwarmDataSource.config.nearestDividerLocation);
        //mainPanel.add(split, BorderLayout.CENTER);
        //
        mainPanel.add(scrollPane, BorderLayout.CENTER);
        
        statusLabel = new JLabel(" ");
        statusLabel.setBorder(BorderFactory.createEmptyBorder(0, 3, 0, 1));
        mainPanel.add(statusLabel, BorderLayout.SOUTH);
        
        mainPanel.setBorder(BorderFactory.createEmptyBorder(0, 2, 1, 2));
        //this.setContentPane(mainPanel);
        
        createListeners();
        doButtonEnables();
    }

        /*
    public void setChooserVisible(boolean vis) {
        if (vis) {
            split.setDividerLocation(JiggleSwarmDataSource.config.chooserDividerLocation);
        }
        else if (isChooserVisible()) {
            JiggleSwarmDataSource.config.chooserDividerLocation = split.getDividerLocation();
                        split.setDividerLocation(split.getMinimumDividerLocation());
        }
        if (SwingUtilities.isEventDispatchThread()) validate();
    }
    
    public boolean isChooserVisible() {
        return split.getDividerLocation() > split.getMinimumDividerLocation();
    }
        */

    private void createMainButtons()
    {
        openButton = SwarmUtil.createToolBarButton(
                Images.getIcon("open"),
                "Open a saved wave",
                new OpenActionListener());
        toolbar.add(openButton);

        saveButton = SwarmUtil.createToolBarButton(
                //Images.getIcon("save"),
                                new ImageIcon(IconImage.getImage("mini-save.gif")),
                "Save selected wave",
                new SaveActionListener());
        saveButton.setEnabled(false);
        toolbar.add(saveButton);

        saveAllButton = SwarmUtil.createToolBarButton(
                //Images.getIcon(saveall"),
                                new ImageIcon(IconImage.getImage("mini-saveall.gif")),
                "Save all waves",
                new SaveAllActionListener());
        saveAllButton.setEnabled(false);
        toolbar.add(saveAllButton);
        
        toolbar.addSeparator();
        
                /*
        linkButton = SwarmUtil.createToolBarToggleButton(
                Images.getIcon("helilink"),
                "Synchronize times with helicorder wave",
                new ActionListener()
                {
                        public void actionPerformed(ActionEvent e)
                        {
                            heliLinked = linkButton.isSelected();
                        }
                });
        linkButton.setSelected(heliLinked);
        toolbar.add(linkButton);
                */
        
        syncButton = SwarmUtil.createToolBarButton(
                Images.getIcon("clock"),
                "Synchronize times with selected wave",
                new ActionListener()
                {
                    public void actionPerformed(ActionEvent e)
                    {
//                        if (selected == null)
//                            return;
                        
                        syncChannels();
                    }
                }); 
        syncButton.setEnabled(false);
        toolbar.add(syncButton);
        
        sortButton = SwarmUtil.createToolBarButton(
                Images.getIcon("geosort"),
                "Sort waves by nearest to selected wave",
                new ActionListener()
                {
                    public void actionPerformed(ActionEvent e)
                    {
//                        if (selected == null)
//                            return;
                        
                        sortChannelsByNearest();
                    }
                });
        sortButton.setEnabled(false);
        toolbar.add(sortButton);
        
        toolbar.addSeparator();
        
        sizeButton = SwarmUtil.createToolBarButton(
                Images.getIcon("resize"),
                "Set clipboard wave size",
                new ActionListener()
                {
                    public void actionPerformed(ActionEvent e)
                    {
                        doSizePopup(sizeButton.getX(), sizeButton.getY() + 2 * sizeButton.getHeight());
                    }
                }); 
        toolbar.add(sizeButton);
        
        titleAllButton = SwarmUtil.createToolBarButton(
                Images.getIcon("label_all"),
                "Toggle selectd panel title labels",
                new ActionListener()
                {
                    public void actionPerformed(ActionEvent e)
                    {
                        setTitleAll();
                    }
                });
        titleAllButton.setEnabled(false);
        toolbar.add(titleAllButton);
        toolbar.addSeparator();
        
        removeAllButton = SwarmUtil.createToolBarButton(
                Images.getIcon("deleteall"),
                "Remove all waves from clipboard",
                new ActionListener()
                {
                    public void actionPerformed(ActionEvent e)
                    {
                        removeWaves();
                    }
                });
        removeAllButton.setEnabled(false);
        toolbar.add(removeAllButton);
        
        //  AWW do we need button to clear cache ???
        clearCacheButton = SwarmUtil.createToolBarButton(
                Images.getIcon("redbullet"),
                "Clear read file cache",
                        new ActionListener() {
                            public void actionPerformed(ActionEvent e) {
                            if (readFileCache != null) {
                                            readFileCache.output();
                                            readFileCache.flush();
                                            readFileCache.output();
                                            doButtonEnables();
                                        }
                            }
                                });
        clearCacheButton.setEnabled(false);
        toolbar.add(clearCacheButton);
        // // AWW ??

        toolbar.addSeparator();
        captureButton = SwarmUtil.createToolBarButton(
                Images.getIcon("camera"),
                "Save clipboard image (P)",
                new CaptureActionListener());
        toolbar.add(captureButton);
        Util.mapKeyStrokeToButton(this, "P", "capture", captureButton);
    }
    
    // TODO: don't write image on event thread
    // TODO: unify with MapFrame.CaptureActionListener
    class CaptureActionListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            if (waves == null || waves.size() == 0)
                return;
            
            //JFileChooser chooser = Swarm.getApplication().getFileChooser();
            File lastPath = new File(JiggleSwarmDataSource.config.lastPath);
            chooser.setCurrentDirectory(lastPath);
            chooser.setSelectedFile(new File("clipboard.png"));
            chooser.setDialogTitle("Save Clipboard Screen Capture");
            int result = chooser.showSaveDialog(JiggleSwarmWaveClipboard.this.getTopLevelAncestor());
            File f = null;
            if (result == JFileChooser.APPROVE_OPTION) 
            {                         
                f = chooser.getSelectedFile();

                if (f.exists()) 
                {
                    int choice = JOptionPane.showConfirmDialog(JiggleSwarmWaveClipboard.this.getTopLevelAncestor(),
                            "File exists, overwrite?", "Confirm", JOptionPane.YES_NO_OPTION);
                    if (choice != JOptionPane.YES_OPTION) 
                        return;
                }
                JiggleSwarmDataSource.config.lastPath = f.getParent();
            }
            if (f == null)
                return;

            int height = 0;
            int width = waves.get(0).getWidth();
            for (JiggleSwarmWaveViewPanel panel : waves)
                height += panel.getHeight();
            
            BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_4BYTE_ABGR);
            Graphics g = image.getGraphics();
            for (JiggleSwarmWaveViewPanel panel : waves)
            {
                panel.paint(g);
                g.translate(0, panel.getHeight());
            }
            try
            {
                PngEncoderB png = new PngEncoderB(image, false, PngEncoder.FILTER_NONE, 7);
                FileOutputStream out = new FileOutputStream(f);
                byte[] bytes = png.pngEncode();
                out.write(bytes);
                out.close();
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }
    }
    
    private void createWaveButtons()
    {
        toolbar.addSeparator();
        
        backButton = SwarmUtil.createToolBarButton(
                Images.getIcon("left"),
                "Scroll back time 20% (Left arrow)",
                new ActionListener()
                {
                    public void actionPerformed(ActionEvent e)
                    {
//                        if (selected != null)
                            shiftTime(-0.20);                        
                    }
                });
        toolbar.add(backButton);
        Util.mapKeyStrokeToButton(this, "LEFT", "backward1", backButton);
        
        forwardButton = SwarmUtil.createToolBarButton(
                Images.getIcon("right"),
                "Scroll forward time 20% (Right arrow)",
                new ActionListener()
                {
                    public void actionPerformed(ActionEvent e)
                    {
//                        if (selected != null)
                            shiftTime(0.20);
                    }
                });
        toolbar.add(forwardButton);
        Util.mapKeyStrokeToButton(this, "RIGHT", "forward1", forwardButton);
        
        gotoButton = SwarmUtil.createToolBarButton(
                Images.getIcon("gototime"),
                "Go to time (Ctrl-G)",
                new ActionListener()
                {
                    public void actionPerformed(ActionEvent e)
                    {
                        JiggleSwarmWaveViewPanel wvp = JiggleSwarmWaveClipboard.this.getSingleSelected();
                        String t = JOptionPane.showInputDialog(JiggleSwarmWaveClipboard.this.getTopLevelAncestor(),
                            "Input time to go to 'YYYYMMDDhhmm[ss]' format:",
                        ((wvp == null) ? "" : saveAllDateFormat.format(Util.j2KToDate(wvp.getStartTime())))
                            );
//                        if (selected != null && t != null)
                        if (t != null)
                            gotoTime(t);
                    }
                });
        toolbar.add(gotoButton);
        Util.mapKeyStrokeToButton(this, "ctrl G", "goto", gotoButton);
        
        compXButton = SwarmUtil.createToolBarButton(
                Images.getIcon("xminus"),
                "Shrink sample time 20% (Alt-left arrow, +)",
                new ActionListener()
                {
                    public void actionPerformed(ActionEvent e)
                    {
//                        if (selected != null)
                            scaleTime(0.20);
                    }
                });
        toolbar.add(compXButton);
        Util.mapKeyStrokeToButton(this, "alt LEFT", "compx", compXButton);
        Util.mapKeyStrokeToButton(this, "EQUALS", "compx2", compXButton);
        Util.mapKeyStrokeToButton(this, "shift EQUALS", "compx2", compXButton);
        
        expXButton = SwarmUtil.createToolBarButton(
                Images.getIcon("xplus"),
                "Expand sample time 20% (Alt-right arrow, -)",
                new ActionListener()
                {
                    public void actionPerformed(ActionEvent e)
                    {
//                        if (selected != null)
                            scaleTime(-0.20);
                    }
                });
        toolbar.add(expXButton);
        Util.mapKeyStrokeToButton(this, "alt RIGHT", "expx", expXButton);
        Util.mapKeyStrokeToButton(this, "MINUS", "expx", expXButton);

        histButton = SwarmUtil.createToolBarButton(
                Images.getIcon("timeback"),
                "Last time settings (Backspace)",
                new ActionListener()
                {
                    public void actionPerformed(ActionEvent e)
                    {
//                        if (selected != null)
                            back();
                    }
                });        
        toolbar.add(histButton);
        toolbar.addSeparator();
        Util.mapKeyStrokeToButton(this, "BACK_SPACE", "back", histButton);

        waveToolbar = new JiggleSwarmWaveViewSettingsToolbar(null, toolbar, this);
        
        copyButton = SwarmUtil.createToolBarButton(
                Images.getIcon("clipboard"),
                "Place another copy of selected wave on clipboard (C or Ctrl-C)",
                new ActionListener()
                {
                    public void actionPerformed(ActionEvent e)
                    {
                        // TODO: implement
                                                JiggleSwarmWaveViewPanel newPanel = null;
                        for (JiggleSwarmWaveViewPanel wvp : selectedSet) {
                            newPanel = new JiggleSwarmWaveViewPanel(wvp); 
                            newPanel.setBackgroundColor(BACKGROUND_COLOR);
                            addWave(newPanel);
                        }
                    }
                });
        toolbar.add(copyButton);
        Util.mapKeyStrokeToButton(this, "C", "clipboard1", copyButton);
        Util.mapKeyStrokeToButton(this, "control C", "clipboard2", copyButton);
        
        toolbar.addSeparator();
        
        upButton = SwarmUtil.createToolBarButton(
                Images.getIcon("up"),
                "Move wave up in clipboard (Up arrow)",
                new ActionListener()
                {
                    public void actionPerformed(ActionEvent e)
                    {
//                        if (selectedSet.size() == 1)
//                        if (selected != null)
                            moveUp();
                    }
                });
        toolbar.add(upButton);
        Util.mapKeyStrokeToButton(this, "UP", "up", upButton);
        
        downButton = SwarmUtil.createToolBarButton(
                Images.getIcon("down"),
                "Move wave down in clipboard (Down arrow)",
                new ActionListener()
                {
                    public void actionPerformed(ActionEvent e)
                    {
//                        if (selected != null)
//                        if (selectedSet)
                            moveDown();
                    }
                });
        toolbar.add(downButton);
        Util.mapKeyStrokeToButton(this, "DOWN", "down", downButton);
        
        removeButton = SwarmUtil.createToolBarButton(
                Images.getIcon("delete"),
                "Remove wave from clipboard (Delete)",
                new ActionListener()
                {
                    public void actionPerformed(ActionEvent e)
                    {
//                        if (selected != null)
                        remove();
                    }
                });
        toolbar.add(removeButton);
        Util.mapKeyStrokeToButton(this, "DELETE", "remove", removeButton);
        
        toolbar.add(Box.createHorizontalGlue());
        toolbar.addSeparator(); 

        selectAllButton = SwarmUtil.createToolBarButton(
                Images.getIcon("bluebullet"),
                "Select all panels (ctrl A)",
        //Util.mapKeyStrokeToAction(this, "control A", "selectAll", new AbstractAction()
                new ActionListener()
            {
                private static final long serialVersionUID = 1L;

                public void actionPerformed(ActionEvent e)
                {
                    for (JiggleSwarmWaveViewPanel wave : waves)
                        select(wave);
                }
            });
        toolbar.add(selectAllButton);
        Util.mapKeyStrokeToButton(this, "control A", "selectAll", selectAllButton);

        deselectAllButton = SwarmUtil.createToolBarButton(
                Images.getIcon("graybullet"),
                "Deselect all panels (alt A)",
        //Util.mapKeyStrokeToAction(this, "alt A", "deselectAll", new AbstractAction()
                new ActionListener()
            {
                private static final long serialVersionUID = 1L;

                public void actionPerformed(ActionEvent e)
                {
                    deselectAll();
                }
            });
        toolbar.add(deselectAllButton);
        Util.mapKeyStrokeToButton(this, "alt A", "deselectAll", selectAllButton);
        toolbar.addSeparator(); 

        throbber = new Throbber();
        toolbar.add(throbber);
    }
    
    private void createListeners()
    {
            /*
        this.addInternalFrameListener(new InternalFrameAdapter()
                {
                    public void internalFrameActivated(InternalFrameEvent e)
                    {
//                        if (selected != null)
//                        {
//                            selected.setBackgroundColor(BACKGROUND_COLOR);
//                            Swarm.getApplication().getDataChooser().setNearest(selected.getChannel());
//                            dataChooser.setNearest(selected.getChannel());
//                        }
                    }
                    
                    public void internalFrameDeiconified(InternalFrameEvent e)
                    {
                        resizeWaves();    
                    }
                    
                    public void internalFrameClosing(InternalFrameEvent e)
                    {
                        setVisible(false);
                    }
                    
                    public void internalFrameClosed(InternalFrameEvent e)
                    {}
                });
             */
        
        this.addComponentListener(new ComponentAdapter()
                {
                    public void componentResized(ComponentEvent e)
                    {
                        resizeWaves();
                    }
                });
        
//        this.addKeyListener(new KeyAdapter()
//                {
//                    public void keyPressed(KeyEvent e)
//                    {
//                        if (e.isShiftDown() && e.getKeyCode() == KeyEvent.VK_R)
//                        {
//                            resetAllAutoScaleMemory();
//                        }
//                    }
//                });
        
        //Swarm.getApplication().addTimeListener(new TimeListener()
        this.addTimeListener(new TimeListener()
                {
                    public void timeChanged(double j2k)
                    {
                        for (JiggleSwarmWaveViewPanel panel : waves)
                        {
                            if (panel != null)
                                panel.setCursorMark(j2k);
                        }
                    }
                });
               //
        
        selectListener = new JiggleSwarmWaveViewPanelAdapter()
                {
                    public void mousePressed(JiggleSwarmWaveViewPanel src, MouseEvent e, boolean dragging)
                    {
                        requestFocusInWindow();
                        int thisIndex = getWaveIndex(src);
//                        if (dragging)
//                        {
//                            if (!selectedSet.contains(src))
//                            {
//                                deselectAll();
//                                select(src);
//                            }
//                        }
//                        else
//                        {
                            if (!e.isControlDown() && !e.isShiftDown() && !e.isAltDown())
                            {
                                deselectAll();
                                select(src);
                            }
                            else if (e.isControlDown())
                            {
                                if (selectedSet.contains(src))
                                    deselect(src);
                                else
                                    select(src);
                            }
                            else if (e.isShiftDown())
                            {
                                if (lastClickedIndex == -1)
                                    select(src);
                                else
                                {
                                    deselectAll();
                                    int min = Math.min(lastClickedIndex, thisIndex);
                                    int max = Math.max(lastClickedIndex, thisIndex);
                                    for (int i = min; i <= max; i++)
                                    {
                                        JiggleSwarmWaveViewPanel ps = (JiggleSwarmWaveViewPanel)waveBox.getComponent(i);
                                        select(ps);
                                    }
                                }
                            }
//                        }
                        lastClickedIndex = thisIndex;
//                        setLastClickedIndex(thisIndex);
                    }
                    
                    public void waveZoomed(JiggleSwarmWaveViewPanel src, double st, double et, double nst, double net)
                    {
                        double[] t = new double[] {st, et};
                        addHistory(src, t);
                        for (JiggleSwarmWaveViewPanel wvp : selectedSet)
                        {
                            if (wvp != src)
                            {
                                addHistory(wvp, t);
                                wvp.zoom(nst, net);
                            }
                        }
                    }
                    
                    public void waveClosed(JiggleSwarmWaveViewPanel src)
                    {
                        remove(src);
                    }
                };
    }
    
    private int calculateWaveHeight()
    {
        if (waveHeight > 0)
            return waveHeight;
        
        int w = scrollPane.getViewport().getSize().width;
        int h = (int)Math.round((double)w * 60.0 / 300.0);
        h = Math.min(200, h);
        h = Math.max(h, 80);
        return h;
    }
    
    private void setWaveHeight(int s)
    {
        waveHeight = s;
        resizeWaves();
    }
    
    private void doSizePopup(int x, int y)
    {
        if (popup == null)
        {
            final String[] labels = new String[] { "Auto", null, "Tiny", "Small", "Medium", "Large" };
            final int[] sizes = new int[] { -1, -1, 50, 100, 160, 230 };
            popup = new JPopupMenu();
            ButtonGroup group = new ButtonGroup();
            for (int i = 0; i < labels.length; i++)
            {
                if (labels[i] != null)
                {
                    final int size = sizes[i];
                    JRadioButtonMenuItem mi = new JRadioButtonMenuItem(labels[i]);
                    mi.addActionListener(new ActionListener()
                            {
                                public void actionPerformed(ActionEvent e)
                                {
                                    setWaveHeight(size);
                                }
                            });
                    if (waveHeight == size)
                        mi.setSelected(true);
                    group.add(mi);
                    popup.add(mi);
                }
                else
                    popup.addSeparator();
            }
        }
        popup.show(this, x, y);
    }
    
    private class OpenActionListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            //JFileChooser chooser = Swarm.getApplication().getFileChooser();
            chooser.resetChoosableFileFilters();
            ExtensionFileFilter txtExt = new ExtensionFileFilter(".txt", "Matlab-readable text files");
            ExtensionFileFilter sacExt = new ExtensionFileFilter(".sac", "SAC files");
            chooser.addChoosableFileFilter(txtExt);
            chooser.addChoosableFileFilter(sacExt);
            chooser.setDialogTitle("Open Wave");
            chooser.setFileFilter(chooser.getAcceptAllFileFilter());
            File lastPath = new File(JiggleSwarmDataSource.config.lastPath);
            chooser.setCurrentDirectory(lastPath);
            chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
            chooser.setMultiSelectionEnabled(true);
            int result = chooser.showOpenDialog(JiggleSwarmWaveClipboard.this.getTopLevelAncestor());
            if (result == JFileChooser.APPROVE_OPTION) 
            {                                    
                File[] fs = chooser.getSelectedFiles();

                for (int i = 0; i < fs.length; i++)
                {
                    if (fs[i].isDirectory())
                    {
                        File[] dfs = fs[i].listFiles();
                        for (int j = 0; j < dfs.length; j++)
                            openFile(dfs[j]);
                        JiggleSwarmDataSource.config.lastPath = fs[i].getParent();
                    }
                    else
                    {
                        openFile(fs[i]);
                        JiggleSwarmDataSource.config.lastPath = fs[i].getParent();
                    }
                }
            }
        }
    }
    
    private class SaveActionListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            JiggleSwarmWaveViewPanel selected = getSingleSelected();
            if (selected == null)
                return;
            
            //JFileChooser chooser = Swarm.getApplication().getFileChooser();
            chooser.resetChoosableFileFilters();
            chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
            chooser.setMultiSelectionEnabled(false);
            chooser.setDialogTitle("Save Wave");
            
            ExtensionFileFilter txtExt = new ExtensionFileFilter(".txt", "Matlab-readable text files");
            ExtensionFileFilter sacExt = new ExtensionFileFilter(".sac", "SAC files");
            chooser.addChoosableFileFilter(txtExt);
            chooser.addChoosableFileFilter(sacExt);
            chooser.setFileFilter(chooser.getAcceptAllFileFilter());
            
            File lastPath = new File(JiggleSwarmDataSource.config.lastPath);
            chooser.setCurrentDirectory(lastPath);
            chooser.setSelectedFile(new File(selected.getChannel() + ".txt"));
            int result = chooser.showSaveDialog(JiggleSwarmWaveClipboard.this.getTopLevelAncestor());
            if (result == JFileChooser.APPROVE_OPTION) 
            {                                    
                File f = chooser.getSelectedFile();
                String path = f.getPath();
                if (!(path.endsWith(".txt") || path.endsWith(".sac")))
                {
                    if (chooser.getFileFilter() == sacExt)
                        f = new File(path + ".sac");
                    else
                        f = new File(path + ".txt");
                }
                boolean confirm = true;
                if (f.exists())
                {
                    if (f.isDirectory())
                    {
                        JOptionPane.showMessageDialog(JiggleSwarmWaveClipboard.this.getTopLevelAncestor(),
                                "You can not select an existing directory.", "Error", JOptionPane.ERROR_MESSAGE);
                        return;
                    }
                    confirm = false;
                    int choice = JOptionPane.showConfirmDialog(JiggleSwarmWaveClipboard.this.getTopLevelAncestor(),
                            "File exists, overwrite?", "Confirm", JOptionPane.YES_NO_OPTION);
                    if (choice == JOptionPane.YES_OPTION)
                        confirm = true;
                }
                
                if (confirm)
                {
                    try
                    {
                        JiggleSwarmDataSource.config.lastPath = f.getParent();
                        String fn = f.getPath().toLowerCase();
                        if (fn.endsWith(".sac"))
                        {
                            SAC sac = selected.getWave().toSAC();
                            String[] scn = selected.getChannel().split(" ");
                            sac.kstnm = scn[0];
                            sac.kcmpnm = scn[1];
                            sac.knetwk = scn[2];
                            sac.write(f);
                        }
                        else
                            selected.getWave().exportToText(f.getPath());
                    }
                    catch (FileNotFoundException ex)
                    {
                        JOptionPane.showMessageDialog(JiggleSwarmWaveClipboard.this.getTopLevelAncestor(), 
                                "Directory does not exist.", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                    catch (IOException ex)
                    {
                        JOptionPane.showMessageDialog(JiggleSwarmWaveClipboard.this.getTopLevelAncestor(), 
                                "Error writing file.", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        }
    }
    
    private class SaveAllActionListener implements ActionListener
    {
        public void actionPerformed(ActionEvent e)
        {
            if (waves.size() <= 0)
                return;
            
            //JFileChooser chooser = Swarm.getApplication().getFileChooser();
            chooser.resetChoosableFileFilters();
            chooser.setMultiSelectionEnabled(false);
            chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            chooser.setDialogTitle("Save SAC File Directory");
            
            File lastPath = new File(JiggleSwarmDataSource.config.lastPath);
            chooser.setCurrentDirectory(lastPath);
            int result = chooser.showSaveDialog(JiggleSwarmWaveClipboard.this.getTopLevelAncestor());
            if (result == JFileChooser.CANCEL_OPTION)
                return;
            File f = chooser.getSelectedFile();
            if (f == null)
            {
                JOptionPane.showMessageDialog(JiggleSwarmWaveClipboard.this.getTopLevelAncestor(),
                        "You must select a directory.", "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
            if (result == JFileChooser.APPROVE_OPTION) 
            {    
                try
                {
                    if (f.exists() && !f.isDirectory())
                        return;
                    if (!f.exists())
                        f.mkdir();
                    for (JiggleSwarmWaveViewPanel wvp : waves)
                    {
                        Wave sw = wvp.getWave();
                        
                        if (sw != null)
                        {
                            sw = sw.subset(wvp.getStartTime(), wvp.getEndTime());
                            String date = saveAllDateFormat.format(Util.j2KToDate(sw.getStartTime()));
                            File dir = new File(f.getPath() + File.separatorChar + date);
                            if (!dir.exists())
                                dir.mkdir();
                            
                            SAC sac = sw.toSAC();
                            String[] scn = wvp.getChannel().split(" ");
                            sac.kstnm = scn[0];
                            sac.kcmpnm = scn[1];
                            sac.knetwk = scn[2];
                            sac.write(new File(dir.getPath() + File.separatorChar + wvp.getChannel().replace(' ', '.')));
                        }
                    }
                    JiggleSwarmDataSource.config.lastPath = f.getPath();
                }
                catch (FileNotFoundException ex)
                {
                    JOptionPane.showMessageDialog(JiggleSwarmWaveClipboard.this.getTopLevelAncestor(), 
                            "Directory does not exist.", "Error", JOptionPane.ERROR_MESSAGE);
                }
                catch (IOException ex)
                {
                    JOptionPane.showMessageDialog(JiggleSwarmWaveClipboard.this.getTopLevelAncestor(), 
                            "Error writing file.", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    }
    
    private enum FileType 
    { 
        TEXT, SAC, UNKNOWN;
        
        public static FileType fromFile(File f)
        {
            if (f.getPath().endsWith(".sac"))
                return SAC;
            else if( f.getPath().endsWith(".txt"))
                return TEXT;
            else 
                return UNKNOWN;
        }
    }
    
    private SAC readSAC(File f)
    {
        SAC sac = new SAC();
        try
        {
            sac.read(f.getPath());
        }
        catch (Exception ex)
        {
            sac = null;
        }
        return sac;
    }
    
    public void openFile(File f)
    {
        SAC sac = null;
        Wave sw = null;
        String channel = f.getName();
        FileType ft = FileType.fromFile(f);
        switch (ft)
        {
            case SAC:
                sac = readSAC(f);
                break;
            case TEXT:
                sw = Wave.importFromText(f.getPath());
                break;
            case UNKNOWN:
                // try SAC
                sac = readSAC(f);
                // try text
                if (sac == null)
                    sw = Wave.importFromText(f.getPath());
                break;
        }
        
        if (sac != null)
        {
            sw = sac.toWave();
            channel = sac.getWinstonChannel().replace('$', ' ');
        }
        
        if (sw != null)
        {
            JiggleSwarmWaveViewPanel wvp = new JiggleSwarmWaveViewPanel();
            wvp.setChannel(channel);
            readFileCache.putWave(channel, sw);
            wvp.setDataSource(readFileCache);
                        clearCacheButton.setEnabled(! readFileCache.isEmpty());
            wvp.setWave(sw, sw.getStartTime(), sw.getEndTime());
            JiggleSwarmWaveClipboard.this.addWave(new JiggleSwarmWaveViewPanel(wvp));
        }
        else
            JOptionPane.showMessageDialog(JiggleSwarmWaveClipboard.this.getTopLevelAncestor(),
                    "There was an error opening the file, '" + f.getName() + "'.", "Error", JOptionPane.ERROR_MESSAGE);
    }
    
        /* Need to extend SDS with JiggleCachedSource since classes are private in CachedDataSource
        private void removeWaveFromCache(JiggleSwarmWaveViewPanel wavePanel) {
            List<CachedWave> chanCachedWaveList = readFileCache.get(wavePanel.getChannel());
            if (waves == null) return;
            else {
                for (CachedWave cw : waves) {
                    if (cw.wave == wavePanel.getWave()) {
                        System.out.println("DEBUG JSWClip removed wave from readFileCache.");
                        chanCachedWaveList.remove(cw);
                    }
                }
            }
        }
        */
    

    private void doButtonEnables()
    {
        boolean enable = (waves == null || waves.size() == 0);
        saveButton.setEnabled(!enable);
        sortButton.setEnabled(!enable);
        saveAllButton.setEnabled(!enable);
        syncButton.setEnabled(!enable);
        removeAllButton.setEnabled(!enable);
        saveAllButton.setEnabled(!enable);
        
        clearCacheButton.setEnabled(! readFileCache.isEmpty());

        boolean allowSingle = (selectedSet.size() == 1);
        upButton.setEnabled(allowSingle);
        downButton.setEnabled(allowSingle);
        sortButton.setEnabled(allowSingle);
        syncButton.setEnabled(allowSingle);
        saveButton.setEnabled(allowSingle);
        
        boolean allowMulti = (selectedSet.size() > 0);
        titleAllButton.setEnabled(allowMulti);
        backButton.setEnabled(allowMulti);
        expXButton.setEnabled(allowMulti);
        compXButton.setEnabled(allowMulti);
        backButton.setEnabled(allowMulti);
        forwardButton.setEnabled(allowMulti);
        histButton.setEnabled(allowMulti);
        removeButton.setEnabled(allowMulti);
        gotoButton.setEnabled(allowMulti);
    }
    
    public synchronized void sortChannelsByNearest()
    {
        JiggleSwarmWaveViewPanel p = getSingleSelected();
        if (p == null)
            return;
        
        ArrayList<JiggleSwarmWaveViewPanel> sorted = new ArrayList<JiggleSwarmWaveViewPanel>(waves.size());
        for (JiggleSwarmWaveViewPanel wave : waves)
            sorted.add(wave);
        
        final Metadata smd = JiggleSwarmDataSource.config.getMetadata(p.getChannel());
        if (smd == null || Double.isNaN(smd.getLongitude()) || Double.isNaN(smd.getLatitude()))
            return;
        
        Collections.sort(sorted, new Comparator<JiggleSwarmWaveViewPanel>()
                {
                    public int compare(JiggleSwarmWaveViewPanel wvp1, JiggleSwarmWaveViewPanel wvp2)
                    {
                        Metadata md = JiggleSwarmDataSource.config.getMetadata(wvp1.getChannel());
                        double d1 = smd.distanceTo(md);
                        md = JiggleSwarmDataSource.config.getMetadata(wvp2.getChannel());
                        double d2 = smd.distanceTo(md);
                        return Double.compare(d1, d2);
                    }
                });
        
        removeWaves();
        for (JiggleSwarmWaveViewPanel wave : sorted)
            addWave(wave);
        select(p);
    }
    
    public synchronized JiggleSwarmWaveViewPanel getSingleSelected()
    {
        if (selectedSet.size() != 1)
            return null;
        
        JiggleSwarmWaveViewPanel p = null;
        for (JiggleSwarmWaveViewPanel panel : selectedSet)
            p = panel;
        
        return p;
    }
    
    public synchronized void syncChannels()
    {
        final JiggleSwarmWaveViewPanel p = getSingleSelected();
        if (p == null)
            return;
        
        final double st = p.getStartTime();
        final double et = p.getEndTime();
        
        // TODO: thread bug here.  must synch iterator below
        final SwingWorker worker = new SwingWorker()
                {
                    public Object construct()
                    {
                        List<JiggleSwarmWaveViewPanel> copy = null;
                        synchronized (JiggleSwarmWaveClipboard.this)
                        {
                            copy = new ArrayList<JiggleSwarmWaveViewPanel>(waves);
                        }
                        for (JiggleSwarmWaveViewPanel wvp : copy)
                        {
                            if (wvp != p)
                            {
                                if (wvp.getDataSource() != null)
                                {
                                    addHistory(wvp, new double[] { wvp.getStartTime(), wvp.getEndTime() });
                                    Wave sw = wvp.getDataSource().getWave(wvp.getChannel(), st, et);
                                    wvp.setWave(sw, st, et);
                                }
                            }
                        }
                        return null;
                    }
                    
                    public void finished()
                    {
                        repaint();    
                    }
                };
        worker.start();    
    }
    
    public void setStatusText(final String s)
    {
        SwingUtilities.invokeLater(new Runnable()
        {
            public void run()
            {
                statusLabel.setText(s);
            }
        });
    }
    
    public JiggleSwarmWaveViewPanel getSelected()
    {
        return null;
//        return selected;
    }
    
    public synchronized void addWave(final JiggleSwarmWaveViewPanel p)
    {
        p.addListener(selectListener);
        p.setOffsets(54, 8, 21, 19);
        p.setAllowClose(true);
        p.setStatusLabel(statusLabel);
        p.setAllowDragging(true);
        p.setDisplayTitle(true);
//        p.setFrameDecorator(new ClipboardWaveDecorator(p));
        int w = scrollPane.getViewport().getSize().width;
        p.setSize(w, calculateWaveHeight());
        p.setBottomBorderColor(Color.GRAY);
        p.createImage();
        waveBox.add(p);    
        waves.add(p);
        doButtonEnables();
        waveBox.validate();
//        select(p);
    }
    
    private synchronized void deselect(final JiggleSwarmWaveViewPanel p)
    {
        selectedSet.remove(p);
        waveToolbar.removeSettings(p.getSettings());
        p.setBackgroundColor(BACKGROUND_COLOR);
        p.createImage();
        doButtonEnables();
    }
    
    private synchronized void setTitleAll()
    {
        JiggleSwarmWaveViewPanel[] panels = selectedSet.toArray(new JiggleSwarmWaveViewPanel[0]);
        for (JiggleSwarmWaveViewPanel p : panels) {
            p.displayTitle = ! p.displayTitle;
            p.createImage();
        }
    }
    
    private synchronized void deselectAll()
    {
        JiggleSwarmWaveViewPanel[] panels = selectedSet.toArray(new JiggleSwarmWaveViewPanel[0]);
        for (JiggleSwarmWaveViewPanel p : panels)
            deselect(p);
    }
    
    private synchronized void select(final JiggleSwarmWaveViewPanel p)
    {
        if (p == null || selectedSet.contains(p))
            return;
        
        selectedSet.add(p);
        doButtonEnables();
//        if (selected == p)
//            return;
        
//        if (selected != null)
//        {
//            selected.setBackgroundColor(BACKGROUND_COLOR);
//            selected.createImage();
//        }
//        selected = p;
        p.setBackgroundColor(SELECT_COLOR);
        //dataChooser.setNearest(p.getChannel());
//        selected.setBackgroundColor(SELECT_COLOR);
        p.createImage();
//        waveToolbar.setSettings(selected.getSettings());
        waveToolbar.addSettings(p.getSettings());
    }
    
    private synchronized void remove(JiggleSwarmWaveViewPanel p)
    {
        int i = 0;
        for (i = 0; i < waveBox.getComponentCount(); i++)
        {
            if (p == waveBox.getComponent(i))
                break;
        }
        
        p.removeListener(selectListener);
        p.getDataSource().close();
        setStatusText(" ");
        waveBox.remove(i);
        waves.remove(p);
        histories.remove(p);
//        if (selected == null && waves.size() > 0)
//        {
//            if (i >= waves.size())
//                i = waves.size() - 1;
//            select(waves.get(i));
//        }
        doButtonEnables();
        waveBox.validate();
        selectedSet.remove(p);
        lastClickedIndex = Math.min(lastClickedIndex, waveBox.getComponentCount() - 1);
        waveToolbar.removeSettings(p.getSettings());
                //removeWaveFromCache(p); // added - aww 
        repaint();
    }
    
    protected int getWaveIndex(JiggleSwarmWaveViewPanel p)
    {
        int i = 0;
        for (i = 0; i < waveBox.getComponentCount(); i++)
        {
            if (p == waveBox.getComponent(i))
                break;
        }
        return i;
    }
    
    public synchronized void remove()
    {
        JiggleSwarmWaveViewPanel[] panels = selectedSet.toArray(new JiggleSwarmWaveViewPanel[0]);
        for (JiggleSwarmWaveViewPanel p : panels)
            remove(p);
//        if (selected == p)
//        {
//            selected = null;
//            waveToolbar.setSettings(null);
//        }
//        int i = 0;
//        for (i = 0; i < waveBox.getComponentCount(); i++)
//        {
//            if (p == waveBox.getComponent(i))
//                break;
//        }
//        
//        p.getDataSource().close();
//        setStatusText(" ");
//        waveBox.remove(i);
//        waves.remove(p);
//        histories.remove(p);
//        if (selected == null && waves.size() > 0)
//        {
//            if (i >= waves.size())
//                i = waves.size() - 1;
//            select(waves.get(i));
//        }
//        doButtonEnables();
//        waveBox.validate();
//        repaint();
    }
    
    public synchronized void moveDown()
    {
        JiggleSwarmWaveViewPanel p = getSingleSelected();
        if (p == null)
            return;
        
        int i = waves.indexOf(p);
        if (i == waves.size() - 1)
            return;
            
        waves.remove(i);
        waves.add(i + 1, p);
        waveBox.remove(p);
        waveBox.add(p, i + 1);
        waveBox.validate();
//        if (selected != null)
//            selected.requestFocus();
        repaint();
    }
    
    public synchronized void moveUp()
    {
        JiggleSwarmWaveViewPanel p = getSingleSelected();
        if (p == null)
            return;
        
        int i = waves.indexOf(p);
        if (i == 0)
            return;
            
        waves.remove(i);
        waves.add(i - 1, p);
        waveBox.remove(p);
        waveBox.add(p, i - 1);
        waveBox.validate();
//        if (selected != null)
//            selected.requestFocus();
        repaint();
    }
    
    public void resizeWaves()
    {
        SwingWorker worker = new SwingWorker()
                {
                    public Object construct()
                    {
                        int w = scrollPane.getViewport().getSize().width;
                        for (JiggleSwarmWaveViewPanel wave : waves)
                        {
                            wave.setSize(w, calculateWaveHeight());
                            wave.createImage();
                        }
                        return null;
                    }
                    
                    public void finished()
                    {
                        waveBox.validate();
                        validate();
                        repaint();
                    }
                };
        worker.start();
    }

     
    public void removeWaves()
    {
        while (waves.size() > 0)
            remove(waves.get(0));

        waveBox.validate();
        scrollPane.validate();
        doButtonEnables();
        repaint();
    }
    
    private void addHistory(JiggleSwarmWaveViewPanel wvp, double[] t)
    {
        Stack<double[]> history = histories.get(wvp);
        if (history == null)
        {
            history = new Stack<double[]>();
            histories.put(wvp, history);
        }
        history.push(t);
    }

    public void gotoTime(JiggleSwarmWaveViewPanel wvp, String t)
    {
        double j2k = Double.NaN;
        try
        {
            if (t.length() == 12)
                t = t + "30";
                
            j2k = Time.parse("yyyyMMddHHmmss", t);
        }    
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(getTopLevelAncestor(),
                    "Illegal time value.", "Error", JOptionPane.ERROR_MESSAGE);
        }    
        
        if (!Double.isNaN(j2k))
        {
            double dt = 60;
            if (wvp.getWave() != null)
            {
                double st = wvp.getStartTime();    
                double et = wvp.getEndTime();
                double[] ts = new double[] {st, et};
                addHistory(wvp, ts);
                dt = (et - st);    
            }
            
            //double tzo = Time.getTimeZoneOffset(JiggleSwarmDataSource.config.getTimeZone(wvp.getChannel())); // seems to give bogus number ?
            //double nst = j2k - tzo - dt / 2;
            double nst = j2k - dt / 2;
            double net = nst + dt;

            fetchNewWave(wvp, nst, net);
        }    
    }
    
    public void gotoTime(String t)
    {
        for (JiggleSwarmWaveViewPanel p : selectedSet)
            gotoTime(p, t);
    }
    
    public void scaleTime(JiggleSwarmWaveViewPanel wvp, double pct)
    {
        double st = wvp.getStartTime();    
        double et = wvp.getEndTime();
        double[] t = new double[] {st, et};
        addHistory(wvp, t);
        double dt = (et - st) * (1 - pct);
        double mt = (et - st) / 2 + st;
        double nst = mt - dt / 2;
        double net = mt + dt / 2;
        fetchNewWave(wvp, nst, net);
    }

    public void scaleTime(double pct)
    {
        for (JiggleSwarmWaveViewPanel p : selectedSet)
            scaleTime(p, pct);
    }
    
    public void back(JiggleSwarmWaveViewPanel wvp)
    {
        Stack<double[]> history = histories.get(wvp);
        if (history == null || history.empty())
            return;
            
        final double[] t = history.pop();
        fetchNewWave(wvp, t[0], t[1]);
    }

    public void back()
    {
        for (JiggleSwarmWaveViewPanel p : selectedSet)
            back(p);
    }
    
    private void shiftTime(JiggleSwarmWaveViewPanel wvp, double pct)
    {
        double st = wvp.getStartTime();    
        double et = wvp.getEndTime();
        double[] t = new double[] {st, et};
        addHistory(wvp, t);
        double dt = (et - st) * pct;
        double nst = st + dt;
        double net = et + dt;
        fetchNewWave(wvp, nst, net);
    }
    
    public void shiftTime(double pct)
    {
        for (JiggleSwarmWaveViewPanel p : selectedSet)
            shiftTime(p, pct);
    }
    
    public void repositionWaves(double st, double et)
    {
        for (JiggleSwarmWaveViewPanel wave : waves)
        {
            fetchNewWave(wave, st, et);
        }
    }
    
    public Throbber getThrobber()
    {
        return throbber;
    }
    
    // This isn't right, this should be a method of waveviewpanel 
    private void fetchNewWave(final JiggleSwarmWaveViewPanel wvp, final double nst, final double net)
    {
        final SwingWorker worker = new SwingWorker()
                {
                    public Object construct()
                    {
                      try {
                        throbber.increment();
//                        disableNavigationButtons();
//                        System.out.println(waveViewPanel.getDataSource().getClass());
                        SeismicDataSource sds = wvp.getDataSource();
                        // Hacky fix for bug #84
                        Wave sw = null;
                        if (sds instanceof CachedDataSource)
                            sw = ((CachedDataSource)sds).getBestWave(wvp.getChannel(), nst, net);
                        else
                            sw = sds.getWave(wvp.getChannel(), nst, net);
                        wvp.setWave(sw, nst, net);
                        wvp.repaint();
                      }
                      catch ( ArrayIndexOutOfBoundsException ex ) {
                          ex.printStackTrace();
                      }
                      return null;
                    }
                    
                    public void finished()
                    {
                        throbber.decrement();
                        repaint();    
                    }
                };
        worker.start();    
    }

        /*
    public void setMaximum(boolean max) throws PropertyVetoException
    {
        if (max)
        {
            JiggleSwarmDataSource.config.clipboardX = getX();
            JiggleSwarmDataSource.config.clipboardY = getY();
        }
        super.setMaximum(max);
    }
        */
    
    public void paint(Graphics g)
    {
        super.paint(g);
        if (waves.size() == 0)
        {
            Dimension dim = this.getSize();
            g.setColor(Color.black);
            g.drawString("Clipboard empty.", dim.width / 2 - 40, dim.height / 2);    
        }    
    }
}
