These files include changes needed to start minimal Redhat 8 / Postgres 14 docker container from https://gitlab.com/aqms-swg/docker-playground/-/tree/master/postgres14.
Check the latest state of the docker image and if needed, take the files from the `docker-playground` and replace original files with these. 

